/*
 Copyright 2015

 Alfred Wassermann
 Michael Gerhaeuser,
 Carsten Miller,
 Matthias Ehmann

 This file is part of the JSXGraph GUI project.
 This code isn't licensed yet.
 */

/**
 * Declare Options for sketchometry
 */
var GUI = GUI || {};

// ensure GUI is really a global symbol
window.GUI = GUI;

JXG.extend(GUI, {
    Options: {
        /* Enable import button */
        allowImport: true,

        /* Cloud storage settings */

        Cloud : { // if nothing is defined here default settings are used
        	defaultStorageProxyBaseURI : "https://sketchometry.uni-bayreuth.de/",

        	//individual settigs for each storage provider
        	//only provided attributes overwrite standard settings
        	/*
        	Webdav : {
        		storageProxyBaseURI : "http://"+location.host+"/sketchometryproxy/", //individual proxy only for webdav
        		enabled : true, //show service in export and import dialog
        		DefaultLoginData : {uri : "http://"+location.host+"/webdav", user : "test", pass : "test"}, // define login data here if the user should always be authenticated by these
        		loginDataEditable : false, //prevent user form editing system provided login data
        		ajaxMethod : "CORS" // use CORS
        	},

            Dropbox : {
        		enabled : true
        	},

            Skydrive : {
        		enabled : false,
        		ajaxMethod : "JSONP" // use CORS
        	},

            Googledrive : {
        		enabled : true
        	},

            Download: {
                enabled: false
            },
        	*/

        	defaultAjaxMethod : "JSONP" //possible values "CORS" or "JSONP"
        },


        /* needs some testing ... */
        finderEnabled: false,

        /* conflicts with zoom and pinch gesture ... */
        multiConstructEnabled: false,

        allowedGestures: ['angle',
                   'bisector',
                   'circle',
                   'circle2points',
                   'line',
                   'midpoint',
                   'normal',
                   'parallel',
                   'perpendicularBisector',
                   'polygon',
                   'quadrilateral',
                   'ray',
                   'reflection',
                   'sector',
                   'segment',
                   'slopetriangle',
                   'tangent',
                   'triangle',
                   'vector'
                  ]
    }
});

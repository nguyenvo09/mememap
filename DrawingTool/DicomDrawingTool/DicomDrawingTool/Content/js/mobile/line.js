﻿var Line = {
    line: undefined,
    currMousePosition: undefined,
    lines: {},
    activeLineId: undefined,
    options: {
        straightFirst: false,
        straightLast: false,
        strokeWidth: 2,
        dash: 0,
        strokeColor: JSXMaster.fakePoint.getAttributes().fillcolor
    },
    removeShape: function (lineId) {
        //TODO
        var line = Line.lines[lineId];
        var p1 = line.point1,
			p2 = line.point2;
        gui.deleteObj(line);
        gui.deleteObj(p1);
        gui.deleteObj(p2);
        delete Line.lines[lineId];

    },
    /**@shapeType: solid, dash, dot of the boundary of each shape**/
    setShapeType: function (lineType) {
        if (Line.activeLineId !== undefined) {
            var activeLine = JSXMaster.board.objects[Line.activeLineId];
            activeLine.setAttribute({ dash: lineType });
        } else {
            Line.options.dash = lineType;
        }
    },
    setStrokeWidth: function (width) {
        if (Line.activeLineId !== undefined) {
            var activeLine = JSXMaster.board.objects[Line.activeLineId];
            //var w = parseInt(width);
            activeLine.setAttribute({ strokeWidth: width });
        } else {
            //Line.options.strokeWidth = parseInt(gui.LineStrokeWidth());
            Line.options.strokeWidth = width;
        }
    },
    setShapeColor: function (color) {
        if (Line.activeLineId !== undefined) {
            var activeLine = JSXMaster.board.objects[Line.activeLineId];
            activeLine.setAttribute({ strokeColor: color });
        } else {
            Line.options.strokeColor = color;
        }
    },
    down: function (e) {
        var first, coords = JSXMaster.getMouseCoords(e, 0);
        //console.log('down');

        for (el in JSXMaster.board.objects) {

            if (el !== JSXMaster.fakePoint.id && JSXMaster.board.objects[el].type
				&& JSXMaster.board.objects[el].hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {

                //shouldCreate = false;
                //first = JSXMaster.board.objects[el];
                return;
                //break;
            }
        }

        //console.log("mouse down");

        if (Line.line !== undefined &&
			Line.line.point1.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
            return;
        }
        first = JSXMaster.board.create('point', JSXMaster.currentMouseCoords, JSXMaster.pointStyle);
        JSXMaster.getMouseCoords(e, 0);
        JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, JSXMaster.currentMouseCoords);
        //JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
        JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
        /*
		if(Line.line !== undefined){
			Line.line.point2 = first;
			//Line.hideVertices(Line.line.id);
			Line.bindEventForLine(Line.line);
			Line.activateShape(Line.line.id);
			Line.line = undefined;
			Line.activeLineId = undefined;
			return;
		}
		*/
        var options = Line.options;
        Line.line = JSXMaster.board.createElement('line', [first, JSXMaster.fakePoint], options);

        //Line.bindEventForLine(Line.line);
        JSXMaster.deactivateExcept(Line.line.id);
        JSXMaster.showToolbar();
    },
    bindEventForLine: function (line) {
        line.on('over', Line.overLine);
        line.on('out', Line.outLine);
        Line.lines[line.id] = line;
        line.on('drag', Line.draggingLine);
        line.on('down', Line.clickingLine);
    },
    bindEventForLine2: function (line) {
        line.on('over', Line.overLine);
        line.on('out', Line.outLine);
        Line.lines[line.id] = line;
        //line.on('drag', Line.draggingLine);
        line.on('down', Line.clickingLine);
    },
    draggingLine: function (e) {
        //http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html#event:drag
        var self = this;

        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;

        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = this.point1,
			second = this.point2;
        //first.prepareUpdate().update().updateRenderer();
        second.prepareUpdate().update().updateRenderer();

        second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
        var mid = [(first.X() + second.X()) / 2 + vector.x, (first.Y() + second.Y()) / 2 + vector.y];
        JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
        JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
        //JSXMaster.fakePoint.showElement();
        JSXMaster.fakeText.hideElement();
    },
    clickingLine: function (e) {
        //console.log("click edge");
        Line.activeLineId = this.id;
        //console.log(Line.activeLineId);
        JSXMaster.getMouseCoords(e, 0);
        JSXMaster.deactivateExcept(this.id);

        var attrs = this.getAttributes();


        gui.ActivateItem('line');
        Line.activateShape(this.id);
        gui.LineStrokeWidth(attrs.strokewidth);
        gui.LineColorValue(attrs.strokecolor);
        document.getElementById(gui.LineColorInputId()).color.fromString(gui.LineColorValue());
        switch (attrs.dash) {
            case 0:

                gui.DashActive("");
                gui.DotActive("");
                gui.SolidActive("toolbar_item_click");
                break;
            case 1:
                gui.DashActive("");
                gui.DotActive("toolbar_item_click");
                gui.SolidActive("");
                break;
            case 2:
                gui.DashActive("toolbar_item_click");
                gui.DotActive("");
                gui.SolidActive("");
                break;
            default:
                break;
        }

    },
    overLine: function (e) {
        //console.log("on element");
        var self = this;
        JSXMaster.disableBoardEvent();
        //JSXMaster.board.off('down', Line.down);
        //console.log(JSON.stringify(self.attrs));
        //self.withLabel = true;
        JSXMaster.board.containerObj.style.cursor = 'move';
        //JSXMaster.fakePoint.hideElement();
        JSXMaster.hideFakePnt();
        var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X()) / 2, (p1.Y() + p2.Y()) / 2];
        sum = v[0] * v[0] + v[1] * v[1];
        var dist = Math.sqrt(sum);
        dist = Math.round(dist * 100) / 100;
        //alert(dist);
        JSXMaster.fakeText.setText("<strong>" + dist + "</strong>");
        //console.log(JSON.stringify(mid));
        JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
        JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
        JSXMaster.fakeText.showElement();
        JSXMaster.board.fullUpdate();
    },
    outLine: function (e) {
        //console.log("out element");
        var self = this;
        //if(JSXMaster.SHAPE_MODE === JSXMaster.SHAPING_MODES.LINE && JSXMaster.board.eventHandlers.down === undefined)
        //	JSXMaster.board.on('down', Line.down);
        JSXMaster.enableBoardEvent();
        //self.withLabel = false;
        //JSXMaster.fakePoint.showElement();
        JSXMaster.showFakePnt();
        JSXMaster.fakeText.hideElement();
        //JSXMaster.board.containerObj.style.cursor = 'default';
    },
    restoreStyle: function () {
        console.log('restore');
        JSXMaster.board.containerObj.style.cursor = 'default';
    },
    move: function (e) {

        console.log('mouse move Line');
        if (Line.line === undefined) {
            JSXMaster.hideFakePnt();
            return;
        }

        JSXMaster.getMouseCoords(e, 0);
        JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, JSXMaster.currentMouseCoords);
        //JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
        JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
        Line.line.prepareUpdate().update().updateRenderer();




        //getTextAnchor

    },
    showVertices: function (cId) {
        var l = Line.lines[cId];
        l.point1.showElement();
        l.point2.showElement();
    },
    hideVertices: function (cId) {
        var l = Line.lines[cId];
        l.point1.hideElement();
        l.point2.hideElement();
    },
    activateShape: function (lineId) {
        JSXMaster.setActiveShape(Line, lineId, 'line');
        Line.showVertices(lineId);
    },
    deactivateShape: function (lineId) {
        Line.hideVertices(lineId);
    },
    showHideToolbar: function (action, type) {
        if (action === 'hide') {
            gui.ActivateItem('');
        } else {
            (type === undefined) ? type = 'line' : type = type;
            gui.ActivateItem(type)
        }
    },
    up: function (e) {
        if (Line.line === undefined) {
            return;
        }
        var first, coords = JSXMaster.getMouseCoords(e, 0);
        if (Line.line !== undefined &&
			Line.line.point1.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
            //return;
        }
        first = JSXMaster.board.create('point', JSXMaster.currentMouseCoords, JSXMaster.pointStyle);
        JSXMaster.getMouseCoords(e, 0);
        JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, JSXMaster.currentMouseCoords);
        //JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
        JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();

        if (Line.line !== undefined) {
            Line.line.point2 = first;
            //Line.hideVertices(Line.line.id);
            Line.bindEventForLine(Line.line);
            Line.activateShape(Line.line.id);
            Line.line = undefined;
            Line.activeLineId = undefined;
            return;
        }

        //Circle.unbinding();
    },
    binding: function () {
        console.log("Binding Line");
        JSXMaster.board.on('down', Line.down);
        JSXMaster.board.on('move', Line.move);
        JSXMaster.board.on('up', Line.up);
    },
    unbinding: function () {
        JSXMaster.board.off('down', Line.down);
        JSXMaster.board.off('move', Line.move);
        JSXMaster.board.off('up', Line.up);
    }
};
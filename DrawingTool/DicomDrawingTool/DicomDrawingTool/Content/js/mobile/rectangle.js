﻿var Rectangle = {
    rectangle: undefined,
    //lines:[],
    //points:[],
    prefix: "rect",
    counter: 0,
    currRectId: undefined,
    /**
	@field: rects
	@type: dictionary of rectangles drawn on the screen.
	@structure: key: rectangleId, value: an array of vertices belonging to @rectangleId 
	@aim: many things. 
	@example: {rect1: [v1, v2,...,vn], rect2: [], ....}
	*/
    rects: {},

    /**
	@field: lines
	@structure: key: rectangleId, value: an array of edges belonging to @rectangleId
	@type: dictionary of edges of a rectangle.
	@aim: the set of lines belonging to the current rect. This set is used only for binding events.
	*/
    lines: {},

    /**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: rectangleID. 
	@aim: This dictionary is used for lookup a rectangle that a vertex belongs to.
	*/
    mappingPoints: {},
    init: function () {
        Rectangle.mappingPoints = {};
        Rectangle.lines = {};
        Rectangle.rects = {};
        Rectangle.counter = 0;
        Rectangle.currRectId = undefined;
    },
    removeShape: function (rectId) {
        //TODO
        var pts = Rectangle.rects[rectId];
        var lines = Rectangle.lines[rectId];
        for (var i = 0; i < lines.length; i++) {
            gui.deleteObj(lines[i]);
        }
        for (var i = 0; i < pts.length; i++) {
            delete Rectangle.mappingPoints[pts[i].id];
            gui.deleteObj(pts[i]);
        }
        delete Rectangle.lines[rectId];
        delete Rectangle.rects[rectId];
    },
    /**@shapeType: solid, dash, dot of the boundary of each shape**/
    setShapeType: function (shapeType) {
        //TODO
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ dash: shapeType });
        }

    },
    /**@width: width of lines of rect**/
    setStrokeWidth: function (width) {
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ strokeWidth: width });
        }
    },
    /**@color: color of shape**/
    setShapeColor: function (color) {
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ strokeColor: color });
        }
    },
    down: function (e) {
        var coords, first;

        coords = JSXMaster.getMouseCoords(e, 0);
        first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);

        Rectangle.currRectId = Rectangle.prefix + "_" + Rectangle.counter;
        var rectId = Rectangle.currRectId;
        //alert(rectId);
        JSXMaster.deactivateExcept(rectId);
        JSXMaster.showToolbar();

        if (Rectangle.rectangle !== undefined) {

            Rectangle.completeRect(rectId, first);
            //Rectangle.hideVertices(rectId);
            Rectangle.activateShape(rectId);
            Rectangle.initHoverBinding(rectId);
            Rectangle.initDraggingListener(rectId, true);
            return;
        }

        var p3 = { x: first.X(), y: first.Y() };
        var secondPoint = JSXMaster.board.create('point', [function () { return JSXMaster.fakePoint.X(); }, p3.y], JSXMaster.pointStyle);

        var fourthPoint = JSXMaster.board.create('point', [p3.x, function () { return JSXMaster.fakePoint.Y(); }], JSXMaster.pointStyle);

        Rectangle.rects[rectId] = Rectangle.rects[rectId] || [];
        Rectangle.rects[rectId].push(first);
        Rectangle.rects[rectId].push(secondPoint);
        Rectangle.rects[rectId].push(JSXMaster.fakePoint);
        Rectangle.rects[rectId].push(fourthPoint);

        var points = Rectangle.rects[rectId];
        var options = { straightFirst: false, straightLast: false, strokeWidth: 2 };
        var firstLine = JSXMaster.board.createElement('line', [points[0], points[1]], options);

        var secondLine = JSXMaster.board.createElement('line', [points[1], points[2]], options);

        var thirdLine = JSXMaster.board.createElement('line', [points[3], points[2]], options);

        var fourthLine = JSXMaster.board.createElement('line', [points[3], points[0]], options);

        Rectangle.lines[rectId] = Rectangle.lines[rectId] || [];
        Rectangle.lines[rectId].push(firstLine);
        Rectangle.lines[rectId].push(secondLine);
        Rectangle.lines[rectId].push(thirdLine);
        Rectangle.lines[rectId].push(fourthLine);

        Rectangle.rectangle = "created";

    },
    initHoverBinding: function (rectId) {
        var lines = Rectangle.lines[rectId] || [];
        for (var i = 0; i < lines.length; i++) {
            lines[i].on('over', Rectangle.overLine);
            lines[i].on('out', Rectangle.outLine);
        }

    },
    overLine: function (e) {
        //console.log("on element");
        var self = this;
        //console.log(JSON.stringify(self.attrs));
        //self.withLabel = true;
        JSXMaster.disableBoardEvent();
        JSXMaster.board.containerObj.style.cursor = 'move';
        //JSXMaster.fakePoint.hideElement();
        JSXMaster.hideFakePnt();
        var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X()) / 2, (p1.Y() + p2.Y()) / 2];
        sum = v[0] * v[0] + v[1] * v[1];
        var dist = Math.sqrt(sum);
        dist = Math.round(dist * 100) / 100;
        //alert(dist);
        JSXMaster.fakeText.setText("<strong>" + dist + "</strong>");
        //console.log(JSON.stringify(mid));
        JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
        JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
        JSXMaster.fakeText.showElement();
        JSXMaster.board.fullUpdate();
    },
    outLine: function (e) {
        //console.log("out element");
        var self = this;
        //self.withLabel = false;
        JSXMaster.enableBoardEvent();
        //JSXMaster.board.containerObj.style.cursor = 'default';
        //JSXMaster.fakePoint.showElement();
        JSXMaster.showFakePnt();
        JSXMaster.fakeText.hideElement();
    },
    restoreStyle: function () {
        console.log('restore');
        JSXMaster.board.containerObj.style.cursor = 'default';
    },
    completeRect: function (rectId, lastpoint) {

        //reset, very important
        Rectangle.rectangle = undefined;
        //Rectangle.secondPoint.addConstraint([x, y]);
        var points = Rectangle.rects[rectId] || [];
        points[1].free();
        points[3].free();
        points[2] = lastpoint;
        for (var i = 0; i < points.length; i++) {
            Rectangle.mappingPoints[points[i].id] = rectId;
        }
        var lines = Rectangle.lines[rectId] || [];
        lines[1].point2 = lastpoint;
        lines[2].point2 = lastpoint;
        Rectangle.counter++; //increase id of distinct rect
    },
    initDraggingListener: function (rectId, realTime) {
        /*
		var down = function(e){
			JSXMaster.getMouseCoords(e, 0);
			gui.ActivateItem('rectangle');
		}*/
        var lines = Rectangle.lines[rectId] || [];
        for (var i = 0; i < lines.length; i++) {
            lines[i].on('down', Rectangle.mouseDownEdge);
        }
        lines[0].on('drag', Rectangle.dragging1);
        if (realTime === undefined || realTime === false) {
            lines[1].on('drag', Rectangle.dragging3);
            lines[2].on('drag', Rectangle.dragging3);
        } else {
            //real time
            lines[1].on('drag', Rectangle.dragging2);
            lines[2].on('drag', Rectangle.dragging2);
        }
        lines[3].on('drag', Rectangle.dragging1);
    },
    showVertices: function (polyId) {
        //show all vertices when being active
        var pts = Rectangle.rects[polyId];
        for (var i = 0; i < pts.length; i++) {
            var p = pts[i];
            p.showElement();
        }
    },
    hideVertices: function (polyId) {
        //hide all vertices, when being inactive
        //show all vertices when being active
        var pts = Rectangle.rects[polyId];
        for (var i = 0; i < pts.length; i++) {
            var p = pts[i];
            p.hideElement();
        }
    },
    mouseDownEdge: function (e) {
        gui.ActivateItem('rectangle');
        JSXMaster.getMouseCoords(e, 0);
        var self = this,
			rectID = Rectangle.mappingPoints[self.point1.id];
        Rectangle.setToolBar(rectID);
        JSXMaster.deactivateExcept(rectID);
        Rectangle.activateShape(rectID);
    },
    setToolBar: function (polyId) {
        var attrs = Rectangle.lines[polyId][0].getAttributes();
        gui.DotActive("");
        gui.SolidActive("");
        gui.DashActive("");
        if (attrs.dash === 2) {
            gui.DashActive("toolbar_item_click");
        } else if (attrs.dash === 1) {
            gui.DotActive("toolbar_item_click");
        } else if (attrs.dash === 0) {
            gui.SolidActive("toolbar_item_click");
        }
        gui.RectStrokeWidth(attrs.strokewidth);
        gui.RectColorValue(attrs.strokecolor);
        document.getElementById(gui.RectColorInputId()).color.fromString(gui.RectColorValue());
    },
    showHideToolbar: function (action, type) {
        if (action === 'hide') {
            gui.ActivateItem('');
        } else {
            (type === undefined) ? type = 'rectangle' : type = type;
            gui.ActivateItem(type)
        }
    },
    activateShape: function (rectId) {
        JSXMaster.setActiveShape(Rectangle, rectId, 'rectangle');
        Rectangle.showVertices(rectId);
    },
    deactivateShape: function (rectId) {
        Rectangle.hideVertices(rectId);
    },
    dragging3: function (e) {
        //for un-real time
        var self = this;
        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;

        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = self.point1, second = self.point2;
        JSXMaster.fakeText.hideElement();
        //second.prepareUpdate().update().updateRenderer();
        //second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

        //first.prepareUpdate().update().updateRenderer();
        var rectId = Rectangle.mappingPoints[first.id];
        var points = Rectangle.rects[rectId] || [];
        for (var i = 0; i < points.length; i++) {
            var p = points[i];
            if (p.id !== first.id && p.id !== second.id) {
                p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
                p.prepareUpdate().update().updateRenderer();
            }
        }
    },
    dragging2: function (e) {
        var self = this;
        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;

        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = self.point1, second = self.point2;
        JSXMaster.fakeText.hideElement();
        second.prepareUpdate().update().updateRenderer();
        second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

        //first.prepareUpdate().update().updateRenderer();
        var rectId = Rectangle.mappingPoints[first.id];
        var points = Rectangle.rects[rectId] || [];
        for (var i = 0; i < points.length; i++) {
            var p = points[i];
            if (p.id !== first.id && p.id !== second.id) {
                p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
                p.prepareUpdate().update().updateRenderer();
            }
        }
    },
    dragging1: function (e) {
        var self = this;
        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;
        JSXMaster.fakeText.hideElement();
        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = self.point1, second = self.point2;

        var rectId = Rectangle.mappingPoints[first.id];
        var points = Rectangle.rects[rectId] || [];
        for (var i = 0; i < points.length; i++) {
            var p = points[i];
            if (p.id !== first.id && p.id !== second.id) {

                p.prepareUpdate().update().updateRenderer();
                p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
            }
        }
    },
    move: function (e) {
        var coords = JSXMaster.getMouseCoords(e, 0);
        JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));

        JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
        var rectId = Rectangle.currRectId;
        if (rectId === undefined) {
            return;
        }

        var points = Rectangle.rects[rectId] || [];
        var lines = Rectangle.lines[rectId] || [];

        for (var i = 0; i < points.length; i++) {
            var point = points[i];
            if (point !== undefined)
                point.prepareUpdate().update().updateRenderer();
        }
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];
            line.prepareUpdate().update().updateRenderer();
        }

    },
    up: function (e) {
        //Circle.unbinding();
    },
    binding: function () {
        JSXMaster.board.on('down', Rectangle.down);
        JSXMaster.board.on('move', Rectangle.move);
        JSXMaster.board.on('up', Rectangle.up);
    },
    unbinding: function () {
        JSXMaster.board.off('down', Rectangle.down);
        JSXMaster.board.off('move', Rectangle.move);
        JSXMaster.board.off('up', Rectangle.up);
    }
};
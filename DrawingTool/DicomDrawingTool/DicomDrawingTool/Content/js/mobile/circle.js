﻿var Circle = {
	circle: undefined,
	circles: {},
	//isBinding: false,
	circleStyle: { strokeColor: '#0000ff', strokeWidth: 2 },
	removeShape: function (cId) {
		var c = Circle.circles[cId];
		var center = c.center,
			p2 = c.point2;
		gui.deleteObj(c);
		gui.deleteObj(center);
		gui.deleteObj(p2);
		delete Circle.circles[cId];
	},
	down: function (e) {
		console.log("what");
		var coords = JSXMaster.getMouseCoords(e, 0);

		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();

		for (el in JSXMaster.board.objects) {

			if (el !== JSXMaster.fakePoint.id
				&& JSXMaster.board.objects[el].hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {

				//shouldCreate = false;
				//first = JSXMaster.board.objects[el];
				JSXMaster.hideFakePnt();
				return;
				//break;
			}
		}

		if (Circle.circle !== undefined && Circle.circle.center.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
			return;
		}
		var center = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
		JSXMaster.deactivateExcept(center.id);
		JSXMaster.showToolbar();
		/*
		if(Circle.circle !== undefined){
			Circle.circle.point2 = center;
			Circle.bindEventForCircle(Circle.circle);
			
			Circle.circle = undefined;
			return;
		}
		*/
		Circle.circle = JSXMaster.board.createElement('circle', [center, JSXMaster.fakePoint], Circle.circleStyle);
	},
	bindEventForCircle: function (c) {
		Circle.circles[c.center.id] = c;
		//Circle.hideVertices(c.center.id);
		Circle.activateShape(c.center.id);
		c.on('down', Circle.clickCircle);
		c.on('drag', Circle.draggingCirle);
		c.on('over', Circle.hoverOn);
		c.on('out', Circle.hoverOff);
		c.center.on('down', Circle.clickCenter);
		c.center.on('drag', Circle.draggingCenter);
		c.center.on('over', Circle.hoverOn);
		c.center.on('out', Circle.hoverOff);
	},
	bindEventForCircle2: function (c) {
		//This function is preserved for circle drawn from file.
		Circle.circles[c.center.id] = c;
		Circle.hideVertices(c.center.id);
		c.on('down', Circle.clickCircle);
		//c.on('drag', Circle.draggingCirle);
		c.on('over', Circle.hoverOn);
		c.on('out', Circle.hoverOff);
		c.center.on('down', Circle.clickCenter);
		c.center.on('drag', Circle.draggingCenter);
		c.center.on('over', Circle.hoverOn);
		c.center.on('out', Circle.hoverOff);
	},
	hoverOn: function (e) {
		//JSXMaster.board.off('down', Circle.down);
		JSXMaster.disableBoardEvent();
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.hideFakePnt();
		JSXMaster.board.containerObj.style.cursor = 'move';
		var self = this;
		var p1 = self.center, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X()) / 2, (p1.Y() + p2.Y()) / 2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist * 100) / 100;
		//alert(dist);
		JSXMaster.fakeText.setText("<strong>" + dist + "</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, [self.point2.X(), self.point2.Y()]);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();

	},
	hoverOff: function (e) {
		JSXMaster.enableBoardEvent();
		//JSXMaster.fakePoint.showElement();
		JSXMaster.showFakePnt();
		//JSXMaster.board.containerObj.style.cursor = 'default';
		JSXMaster.fakeText.hideElement();
	},
	restoreStyle: function () {
		console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	draggingCirle: function (e) {
		JSXMaster.fakeText.hideElement();
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;

		var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
		var first = this.center,
			second = this.point2;

		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
	},
	clickCircle: function (e) {
		//alert('fuck');
		//Circle.unbinding();
		JSXMaster.getMouseCoords(e, 0);
		gui.ActivateItem('circle');
		Circle.setToolBar(this.center.id);
		JSXMaster.deactivateExcept(this.center.id);
		Circle.activateShape(this.center.id);
	},
	setToolBar: function (polyId) {
		var attrs = Circle.circles[polyId].getAttributes();
		gui.DotActive("");
		gui.SolidActive("");
		gui.DashActive("");
		if (attrs.dash === 2) {
			gui.DashActive("toolbar_item_click");
		} else if (attrs.dash === 1) {
			gui.DotActive("toolbar_item_click");
		} else if (attrs.dash === 0) {
			gui.SolidActive("toolbar_item_click");
		}
		gui.CircleStrokeWidth(attrs.strokewidth);
		gui.CircleColorValue(attrs.strokecolor);
		document.getElementById(gui.CircleColorInputId()).color.fromString(gui.CircleColorValue());
	},
	clickCenter: function (e) {
		JSXMaster.getMouseCoords(e, 0);
		gui.ActivateItem('circle');
		JSXMaster.deactivateExcept(this.id);
		Circle.activateShape(this.id);
	},
	showVertices: function (cId) {
		var c = Circle.circles[cId];
		c.center.showElement();
		c.point2.showElement();
	},
	hideVertices: function (cId) {
		var c = Circle.circles[cId];
		c.center.hideElement();
		c.point2.hideElement();
	},
	activateShape: function (circleId) {
		//alert('fuck');
		JSXMaster.setActiveShape(Circle, circleId, 'circle');
		Circle.showVertices(circleId);
	},
	deactivateShape: function (circleId) {
		Circle.hideVertices(circleId);
	},
	draggingCenter: function (e) {
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;

		//console.log("begin point: " + JSON.stringify(begin));
		//console.log("end point: " + JSON.stringify(end));
		var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
		var main_circle = Circle.circles[self.id],
			second = main_circle.point2;
		//var second = this;
		//first.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
	},
	move: function (e) {


		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		//JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if (Circle.circle !== undefined) {
			Circle.circle.prepareUpdate().update().updateRenderer();
		}

		//getTextAnchor

	},
	showHideToolbar: function (action, type) {
		if (action === 'hide') {
			gui.ActivateItem('');
		} else {
			(type === undefined) ? type = 'circle' : type = type;
			gui.ActivateItem(type)
		}
	},
	up: function (e) {
		//Circle.circle = JSXMaster.board.createElement('circle',[center, JSXMaster.fakePoint], Circle.circleStyle);
		//Circle.unbinding();
		var coords = JSXMaster.getMouseCoords(e, 0);

		if (Circle.circle !== undefined && Circle.circle.center.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
			//return;
		}
		var center = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
		JSXMaster.deactivateExcept(center.id);
		JSXMaster.showToolbar();
		if (Circle.circle !== undefined) {
			Circle.circle.point2 = center;
			Circle.bindEventForCircle(Circle.circle);

			Circle.circle = undefined;
			return;
		}
	},
	binding: function () {
		//Circle.isBinding = true;
		JSXMaster.board.on('down', Circle.down);
		JSXMaster.board.on('move', Circle.move);
		JSXMaster.board.on('up', Circle.up);
	},
	unbinding: function () {
		//Circle.isBinding = false;
		JSXMaster.board.off('down', Circle.down);
		JSXMaster.board.off('move', Circle.move);
		JSXMaster.board.off('up', Circle.up);
	},
	/**@shapeType: solid, dash, dot of the boundary of each shape**/
	setShapeType: function (boundaryType) {

		var activeShape = JSXMaster.getActiveShape();
		if (activeShape !== undefined) {
			var activeShapeId = activeShape.activeShapeId;
			//if(Line.activeLineId !== undefined){
			//if(activeShape.cls.hasOwnProperty('circles'))
			var c = Circle.circles[activeShapeId];
			//var activeLine = JSXMaster.board.objects[Line.activeLineId];
			c.setAttribute({ dash: boundaryType });
		} else {
			Circle.circleStyle.dash = boundaryType;
		}
	},
	/**@width: strokeWidth of shape**/
	setStrokeWidth: function (width) {
		var id = JSXMaster.getActiveShape().activeShapeId,
			c = Circle.circles[id];
		c.setAttribute({ strokeWidth: width });
	},
	/**@color: color of shape**/
	setShapeColor: function (color) {
		var id = JSXMaster.getActiveShape().activeShapeId,
			c = Circle.circles[id];
		c.setAttribute({ strokeColor: color });
	}
};
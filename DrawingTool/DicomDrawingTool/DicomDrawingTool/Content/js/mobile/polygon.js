﻿var Polygon = {
    currLine: undefined,
    prefix: "poly",
    counter: 0,

    /**
	@field: polygons
	@type: dictionary of polygons drawn on the screen.
	@structure: key: polygonId, value: an array of vertices belonging to @polygonId 
	@aim: many things. 
	@example: {poly1: [v1, v2,...,vn], poly2: [], ....}
	*/
    polygons: {},

    /**
	@field: lines
	@structure: key: polygonId, value: an array of edges belonging to @polygonId
	@type: dictionary of edges of a polygon.
	@aim: the set of lines belonging to the current polygon. This set is used only for binding events.
	*/
    lines: {},

    /**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: polygonID. 
	@aim: This dictionary is used for lookup a polygon that a vertex belongs to.
	*/
    mappingPoints: {},

    init: function () {
        Polygon.mappingPoints = {};
        Polygon.lines = {};
        Polygon.polygons = {};
        Polygon.counter = 0;
        Polygon.currLine = undefined;
    },
    removeShape: function (polyId) {
        //TODO
        //alert(polyId);
        var pts = Polygon.polygons[polyId];
        var lines = Polygon.lines[polyId];
        for (var i = 0; i < lines.length; i++) {
            gui.deleteObj(lines[i]);
        }
        for (var i = 0; i < pts.length; i++) {
            delete Polygon.mappingPoints[pts[i].id];
            gui.deleteObj(pts[i]);
        }
        delete Polygon.lines[polyId];
        delete Polygon.polygons[polyId];
    },
    down: function (e) {
        //handler when we press the mouse on the screen.
        var canCreate = true,
			coords,
			polygonID,
			points;


        coords = JSXMaster.getMouseCoords(e, 0);
        polygonID = Polygon.prefix + "_" + Polygon.counter;
        //if(JSXMaster.activeShape.activeShapeId !== polygonID)
        //	JSXMaster.deactivateAll();
        JSXMaster.deactivateExcept(polygonID);
        JSXMaster.showToolbar();
        //alert(polygonID);
        if (Polygon.ableToCreate(polygonID, e)) {

            console.log("Can create");
            //var first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
            var first = undefined;
            JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
            JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();

            if (Polygon.currLine === undefined) {
                //complete the previous line. 
                //Polygon.currLine.point2 = first;
                //Polygon.currLine = JSXMaster.board.create('line', [first, Polygon.currLine.point2], 
                //						{straightFirst:false, straightLast:false, strokeWidth:2});
                first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
                //Polygon.currLine = JSXMaster.board.create('line', [first, JSXMaster.fakePoint], 
                //						{straightFirst:false, straightLast:false, strokeWidth:2});
            } else {
                //Polygon.currLine = JSXMaster.board.create('line', [first, JSXMaster.fakePoint], 
                //						{straightFirst:false, straightLast:false, strokeWidth:2});
                //var first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
                //Polygon.currLine = JSXMaster.board.create('line', [Polygon.currLine.point2, JSXMaster.fakePoint], 
                //						{straightFirst:false, straightLast:false, strokeWidth:2});
                first = Polygon.currLine.point2;
            }


            Polygon.currLine = JSXMaster.board.create('line', [first, JSXMaster.fakePoint],
									{ straightFirst: false, straightLast: false, strokeWidth: 2 });

            //set of lines of current polygon.
            Polygon.lines[polygonID] = Polygon.lines[polygonID] || [];
            Polygon.lines[polygonID].push(Polygon.currLine);
            //Polygon.lines[polygonID] = list;

            //add the new vertex to the polygon @polygonID
            Polygon.polygons[polygonID] = Polygon.polygons[polygonID] || [];
            Polygon.polygons[polygonID].push(first);
            //console.log(Polygon.polygons[polygonID].length);
            //this vertex belongs to @polygonID
            Polygon.mappingPoints[first.id] = polygonID;

        }
    },
    initHoverBinding: function (polyId) {
        var lines = Polygon.lines[polyId] || [];
        for (var i = 0; i < lines.length; i++) {
            lines[i].on('over', Polygon.overLine);
            lines[i].on('out', Polygon.outLine);
        }

    },
    overLine: function (e) {
        console.log("on element");
        var self = this;
        //console.log(JSON.stringify(self.attrs));
        //self.withLabel = true;
        //JSXMaster.fakePoint.hideElement();
        JSXMaster.disableBoardEvent();
        //JSXMaster.fakePoint.hideElement();
        JSXMaster.hideFakePnt();
        //JSXMaster.fakeText.hideElement();
        JSXMaster.board.containerObj.style.cursor = 'move';

        var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X()) / 2, (p1.Y() + p2.Y()) / 2];
        sum = v[0] * v[0] + v[1] * v[1];
        var dist = Math.sqrt(sum);
        dist = Math.round(dist * 100) / 100;
        //alert(dist);
        JSXMaster.fakeText.setText("<strong>" + dist + "</strong>");
        //console.log(JSON.stringify(mid));
        JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
        JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
        JSXMaster.fakeText.showElement();
        JSXMaster.board.fullUpdate();
    },
    outLine: function (e) {
        //console.log("out element");
        var self = this;
        //self.withLabel = false;
        JSXMaster.enableBoardEvent();
        //JSXMaster.fakePoint.showElement();
        JSXMaster.showFakePnt();
        JSXMaster.fakeText.hideElement();
        //JSXMaster.board.containerObj.style.cursor = 'default';
    },
    restoreStyle: function () {
        console.log('restore');
        JSXMaster.board.containerObj.style.cursor = 'default';
    },
    /**
		Check if we can create new point and new line.
		@polygonID
		@event e: down event object
	*/
    ableToCreate: function (polygonID, e) {
        var coords, points, canCreate = true;

        coords = JSXMaster.getMouseCoords(e, 0);
        points = Polygon.polygons[polygonID] || [];
        for (var i = 0; i < points.length; i++) {

            var point = points[i];

            //The condition that inform us that a cycle (a polygon) is created.
            if (point.id !== JSXMaster.fakePoint.id && point.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
                Polygon.completePolygon(point, polygonID);
                //Polygon.hideVertices(polygonID);
                Polygon.activateShape(polygonID);
                Polygon.initHoverBinding(polygonID, true);
                Polygon.initDraggingListener(polygonID, true);
                canCreate = false;
                break;
            }
        }
        return canCreate;
    },
    /**a cycle of points was created. 
	we need to complete the current polygon and reset data for the new one*/
    completePolygon: function (lastPoint, polyId) {
        //Polygon.currLine.point2 = lastPoint;
        Polygon.currLine = JSXMaster.board.create('line', [Polygon.currLine.point2, lastPoint],
									{ straightFirst: false, straightLast: false, strokeWidth: 2 });

        Polygon.lines[polyId] = Polygon.lines[polyId] || [];
        Polygon.lines[polyId].push(Polygon.currLine);

        Polygon.polygons[polyId] = Polygon.polygons[polyId] || [];
        Polygon.polygons[polyId].push(Polygon.currLine.point1);

        Polygon.mappingPoints[Polygon.currLine.point1.id] = polyId;
        Polygon.mappingPoints[Polygon.currLine.point2.id] = polyId;

        //for (var i = 0; i < Polygon.polygons[polyId].length; i++) {
        //    console.log(Polygon.polygons[polyId][i].id);
        //}

        //alert("Complete polygon: " + "so line: " + Polygon.lines[polyId].length + " + so dinh: " + Polygon.polygons[polyId].length)
        Polygon.currLine = undefined;
        Polygon.counter++; //increase the id of polygon
    },
    showVertices: function (polyId) {
        //show all vertices when being active
        var pts = Polygon.polygons[polyId];
        for (var i = 0; i < pts.length; i++) {
            var p = pts[i];
            p.showElement();
        }
    },
    hideVertices: function (polyId) {
        //hide all vertices, when being inactive
        //show all vertices when being active
        var pts = Polygon.polygons[polyId];
        for (var i = 0; i < pts.length; i++) {
            var p = pts[i];
            p.hideElement();
        }
    },
    mouseDownEdge: function (e) {
        console.log('click edge of polygon');
        gui.ActivateItem('polygon');

        JSXMaster.getMouseCoords(e, 0);
        Polygon.overLine(e);
        var self = this,
			polygonID = Polygon.mappingPoints[self.point1.id];
        console("Edege dc click thuoc ve: " + polygonID);
        //Polygon.lines[polygonID][0].getAttributes();
        //if(JSXMaster.activeShape.activeShapeId !== polygonID)
        //	JSXMaster.deactivateAll();
        Polygon.setToolBar(polygonID);
        JSXMaster.deactivateExcept(polygonID);
        Polygon.activateShape(polygonID);
    },
    setToolBar: function (polyId) {
        var attrs = Polygon.lines[polyId][0].getAttributes();
        gui.DotActive("");
        gui.SolidActive("");
        gui.DashActive("");
        if (attrs.dash === 2) {
            gui.DashActive("toolbar_item_click");
        } else if (attrs.dash === 1) {
            gui.DotActive("toolbar_item_click");
        } else if (attrs.dash === 0) {
            gui.SolidActive("toolbar_item_click");
        }
        //TODO
        //gui.PolygonStrokeWidth PolygonColorValue
        //alert(attrs.strokewidth);
        gui.PolygonStrokeWidth(attrs.strokewidth);
        //alert(attrs.strokecolor);
        gui.PolygonColorValue(attrs.strokecolor);
        document.getElementById(gui.PolygonColorInputId()).color.fromString(gui.PolygonColorValue());
    },
    activateShape: function (polyId) {
        JSXMaster.setActiveShape(Polygon, polyId, 'polygon');
        Polygon.showVertices(polyId);
    },
    deactivateShape: function (polyId) {
        //console.log('start');
        Polygon.hideVertices(polyId);
    },
    showHideToolbar: function (action, type) {
        if (action === 'hide') {
            gui.ActivateItem('');
        } else {
            (type === undefined) ? type = 'polygon' : type = type;
            gui.ActivateItem(type)
        }
    },
    
	mouseUpEdge: function(e){
		//gui.ActivateItem('');
		//JSXMaster.getMouseCoords(e, 0);
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id];
	    //Polygon.hideVertices(polygonID);
		Polygon.outLine(e);
	},
	/**
		{boolean} @realTime: determine whether binding real-time or loading from file
		{string} @polygonID
		Binding dragging event for each edge
	*/
    initDraggingListener: function (polygonID, realTime) {
        var lines = Polygon.lines[polygonID] || [];

        for (var j = 0; j < lines.length; j++) {
            var line = lines[j];
            line.on('down', Polygon.mouseDownEdge);
            line.on('up', Polygon.mouseUpEdge);
            if (realTime === undefined || realTime === false)
                //not real-time
                line.on('drag', Polygon.dragging2);
            else
                line.on('drag', Polygon.dragging);
        }
    },
    dragging2: function (e) {
        //function is used for binding when loading from file
        var self = this,
			polygonID = Polygon.mappingPoints[self.point1.id],
			points = Polygon.polygons[polygonID] || [];
        //alert("polygonID: " + polygonID);
        //alert(JSON.stringify(points));
        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;
        JSXMaster.fakeText.hideElement();
        //console.log("begin point: " + JSON.stringify(begin));
        //console.log("end point: " + JSON.stringify(end));
        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = this.point1,
			second = this.point2;
        //first.prepareUpdate().update().updateRenderer();
        //second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
        //second.prepareUpdate().update().updateRenderer();

        //first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
        for (var k = 0; k < points.length; k++) {
            var nextPts = points[k];
            if (nextPts.id != first.id && nextPts.id != second.id) {
                nextPts.setPosition(JXG.COORDS_BY_USER, [nextPts.X() + vector.x, nextPts.Y() + vector.y]);
                nextPts.prepareUpdate().update().updateRenderer();
            }
        }
    },
    dragging: function (e) {
        //this: the line that we drag
        var self = this,
			polygonID = Polygon.mappingPoints[self.point1.id],
			points = Polygon.polygons[polygonID] || [];
        //alert("polygonID: " + polygonID);
        //alert(JSON.stringify(points));
        var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
        JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
        var end = JSXMaster.currentMouseCoords;
        JSXMaster.fakeText.hideElement();
        //console.log("begin point: " + JSON.stringify(begin));
        //console.log("end point: " + JSON.stringify(end));
        var vector = { x: end[0] - begin[0], y: end[1] - begin[1] };
        var first = this.point1,
			second = this.point2;
        //first.prepareUpdate().update().updateRenderer();
        second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
        second.prepareUpdate().update().updateRenderer();

        //first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
        for (var k = 0; k < points.length; k++) {
            var nextPts = points[k];
            if (nextPts.id != first.id && nextPts.id != second.id) {
                nextPts.setPosition(JXG.COORDS_BY_USER, [nextPts.X() + vector.x, nextPts.Y() + vector.y]);
                nextPts.prepareUpdate().update().updateRenderer();
            }
        }
    },
    binding: function () {
        console.log("Binding Polygon");
        JSXMaster.board.on('move', Polygon.mousemove);
        JSXMaster.board.on('down', Polygon.down);

        JSXMaster.board.on('up', Polygon.mouseup);
    },
    unbinding: function () {
        JSXMaster.board.off('down', Polygon.down);
        JSXMaster.board.off('move', Polygon.mousemove);
        JSXMaster.board.off('up', Polygon.mouseup);
    },
    mousemove: function (e) {
        //console.log("mouse move polygon");
        var coords = JSXMaster.getMouseCoords(e, 0);
        JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
        JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
        if (Polygon.currLine !== undefined) {
            Polygon.currLine.prepareUpdate().update().updateRenderer();
        }
        //http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html
    },
    mouseup: function (e) {
        //JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
        //JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
        var coords = JSXMaster.getMouseCoords(e, 0);

        //polygonID = Polygon.prefix + "_" + Polygon.counter;

        //JSXMaster.deactivateExcept(polygonID);
        //JSXMaster.showToolbar();


        if (Polygon.currLine !== undefined) {
            //complete the previous line. 
            var first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
            Polygon.currLine.point2 = first;
        }
    },
    /**@shapeType: solid, dash, dot of the boundary of each shape**/
    setShapeType: function (shapeType) {
        //TODO
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ dash: shapeType });
        }

    },
    /**@width: width of lines of rect**/
    setStrokeWidth: function (width) {
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ strokeWidth: width });
        }
    },
    /**@color: color of shape**/
    setShapeColor: function (color) {
        var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
        for (var i = 0; i < shape.length; i++) {
            var line = shape[i];
            line.setAttribute({ strokeColor: color });
        }
    }
};
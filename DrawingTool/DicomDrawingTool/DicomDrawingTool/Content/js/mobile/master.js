﻿//JXG.Options.renderer = 'canvas';
var JSXMaster = {
	fakePoint: undefined, //this point is the position of mouse on the screen (always)
	fakeText: undefined,
	board: undefined,
	scale: 100,
	mapWidth: undefined,
	mapHeight: undefined,

	/** style of points on the map **/
	pointStyle: { fillColor: '#00ff00', strokeWidth: 0, size: 3, name: '', showInfobox: false },

	/** style of text on the map **/
	textStyle: { fontSize: 10, display: 'html', fixed: true },

	/** the current drawing mode on the map, possible values: 1,2,3,4,5 **/
	SHAPE_MODE: undefined,
	/** all possible modes on the map **/
	SHAPING_MODES: { 'line': 1, 'circle': 2, 'polygon': 3, 'rectangle': 4, 'dragging': 5, 'text': 6 },

	/** structure: {cls: ..., type:.....}
		@cls: Polygon, Line, Circle, Rectangle, Text
		@type: possible values: 'polygon', 'line', 'circle', 'rectangle', 'text' 
	**/
	activeDrawingMode: undefined,

	/** structure: {cls:....., activeId:....., type:......}
		@cls: Polygon, Line, Circle, Rectangle, Text
		@activeShapeId: string.
		@type: possible values: 'polygon', 'line', 'circle', 'rectangle', 'text' **/
	activeShape: undefined,
	setActiveShape: function (cls, id, type) {
		console.log('setActiveShape: ' + id + " " + type);
		JSXMaster.activeShape = { cls: cls, activeShapeId: id, type: type };
	},
	getActiveShape: function () {
		var self = JSXMaster;
		return self.activeShape;
	},
	showToolbar: function () {
		if (JSXMaster.activeDrawingMode !== undefined) {
			JSXMaster.activeDrawingMode.cls.showHideToolbar('show', JSXMaster.activeDrawingMode.type);
		} else {
			gui.Drag();
		}
	},
	deleteActiveShape: function () {
		//TODO
		var active = JSXMaster.activeShape;
		if (active !== undefined) {
			active.cls.removeShape(active.activeShapeId);
			JSXMaster.activeShape = undefined;
			//gui.ActivateItem('');
		}
	},
	deactivateAll: function () {
		var record = JSXMaster.activeShape;
		//console.log('JSXMaster.deactivateAll: ' + JSON.stringify(record));

		if (record !== undefined) {
			console.log('JSXMaster.deactivateAll: ' + record.activeShapeId + " " + record.type);
			var cls = record['cls'],
				activeId = record['activeShapeId'];
			//alert("ton tai thuoc tinh ko: " + cls.hasOwnProperty('deactivateShape'));
			if (cls !== undefined && cls.hasOwnProperty('deactivateShape')) {
				//alert('dkm');
				cls.deactivateShape(activeId);
				//gui.ActivateItem('');
				JSXMaster.activeShape = undefined;
			}
		}
	},
	deactivateExcept: function (currActive) {
		var r = JSXMaster.activeShape;
		if (r !== undefined && r.activeShapeId !== currActive) {
			JSXMaster.deactivateAll();

		}
	},
	activateAll: function () {
		var record = JSXMaster.activeShape;
		if (record !== undefined) {
			var cls = record['cls'],
				activeId = record['activeShapeId'];
			if (cls !== undefined && cls.hasOwnProperty('activeShape')) {
				cls.activateShape(activeId);
				//gui.ActivateItem(cls.type);
			}
		}
	},
	setDrawingMode: function (cls, type) {
		(cls === undefined) ?
			JSXMaster.activeDrawingMode = undefined :
			JSXMaster.activeDrawingMode = { cls: cls, type: type };
		//JSXMaster.SHAPE_MODE = JSXMaster.SHAPING_MODES[type];
	},
	init: function () {
		JSXMaster.mapWidth = window.innerWidth / JSXMaster.scale;
		JSXMaster.mapHeight = window.innerHeight / JSXMaster.scale;
		var w = JSXMaster.mapWidth, h = JSXMaster.mapHeight;
		//var dim = (w > h) ? h : w;
		//dim = dim/100;

		JSXMaster.board = JXG.JSXGraph.initBoard('jxgbox', {
			boundingbox: [-w / 2, h / 2, w / 2, -h / 2],
			axis: false,
			grid: true,
			showCopyright: false,
			showNavigation: true,
			zoom: {
				wheel: true
			}
		});
		JSXMaster.fakePoint = JSXMaster.board.create('point', [0, 0], JSXMaster.pointStyle);
		JSXMaster.fakeText = JSXMaster.board.create('text', [0, 0, ''], JSXMaster.textStyle);
		JSXMaster.hideFakePnt();
	},
	getMouseCoords: function (e, i) {
		var cPos = JSXMaster.board.getCoordsTopLeftCorner(e, i),
			absPos = JXG.getPosition(e, i),
			dx = absPos[0] - cPos[0],
			dy = absPos[1] - cPos[1];
		//console.log("update mouse coord");
		var res = new JXG.Coords(JXG.COORDS_BY_SCREEN, [dx, dy], JSXMaster.board);
		JSXMaster.currentMouseCoords = res.usrCoords.slice(1);
		return res;
	},
	currentMouseCoords: undefined,
	hideFakePnt: function () {
		if (JSXMaster.fakePoint !== undefined)
			JSXMaster.fakePoint.hideElement();
	},
	showFakePnt: function () {
		var shouldShow = false;
		var activeDrawingMode = JSXMaster.activeDrawingMode;
		if (activeDrawingMode !== undefined) {
			var type = activeDrawingMode.type;
			if (type === 'line' ||
				type === 'circle' ||
				type === 'polygon' ||
			    type === 'rectangle') {
				shouldShow = true;
			}

		}


		if (shouldShow) {
			JSXMaster.fakePoint.showElement();
		}
	},
	mousewheel: function (evt) {
		console.log("wheeling");
	},

	disableBoardEvent: function () {
		JSXMaster.board.off('down', Circle.down);
		JSXMaster.board.off('down', Line.down);
		JSXMaster.board.off('down', Rectangle.down);
		JSXMaster.board.off('down', Polygon.down);
		JSXMaster.board.off('down', TextOBJ.down);
	},
	enableBoardEvent: function () {
		//There is a 'down' event associated with the board.
		if (JSXMaster.board.eventHandlers.down !== undefined) {
			return;
		}
		if (JSXMaster.activeDrawingMode === undefined) {
			return;
		}
		var activeDraw = JSXMaster.activeDrawingMode;
		//if(activeDraw.type === 'line' || activeDraw.type === 'circle' || 
		//	activeDraw.type === 'polygon')
		if (activeDraw.cls.hasOwnProperty('down')) {
			JSXMaster.board.on('down', activeDraw.cls.down);
		}
		if (activeDraw.cls.hasOwnProperty('restoreStyle')) {
			activeDraw.cls.restoreStyle();
		}

	}

};
JSXMaster.init();
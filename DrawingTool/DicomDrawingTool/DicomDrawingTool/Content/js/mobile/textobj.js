﻿var TextOBJ = {
	defaultStyle: {
		fontSize: 20,
		strokeColor: '#ff0000',
		display: 'html',
		isLabel: true,
		highlightCssClass: 'fonthighlight',
		cssClass: 'myFont'
	},
	texts: {},
	/**@structure: key: @textId, value: html input object that associates with @textId**/
	textInputs: {},
	/** the input html that is active currently **/
	activeBlankInput: undefined,
	/** active text object id **/
	activeTextId: undefined,
	/** the text object this is editing **/
	editingText: undefined,
	removeShape: function (textId) {
		var self = TextOBJ;
		var txt = self.texts[textId];
		gui.deleteObj(txt);
		delete self.texts[textId];

	},
	addText: function (text, options, position) {
		var self = TextOBJ;
		options = options || self.defaultStyle;
		//console.log("dkm:" + TextOBJ.defaultStyle);
		for (var p in self.defaultStyle) {
			if (options[p] === undefined) {
				//console.log(p);
				options[p] = self.defaultStyle[p];
			}
		}
		//console.log(options);
		(position === undefined) ? position = [0, 0, text] : position = [position[0], position[1], text];
		var txt = JSXMaster.board.create('text', position, options);
		//JSXMaster.setDrawingMode(self, 'text');
		self.bindEventForText(txt);
		TextOBJ.activateShape(txt.id);
		JSXMaster.deactivateExcept(txt.id);
		JSXMaster.showToolbar();
		self.showVertices(txt.id);
		return txt;
	},
	init: function () {
		var self = TextOBJ;
		self.activeTextId = undefined;
		JSXMaster.board.containerObj.style.cursor = 'text';
		self.binding();
	},
	restoreStyle: function () {
		console.log('restore properties of text');
		JSXMaster.board.containerObj.style.cursor = 'text';
	},
	down: function (e) {

		//console.log('fuck');
		//JSXMaster.board.containerObj.style.cursor = 'text';
		var self = TextOBJ;
		if (self.activeBlankInput !== undefined) {
			//There is one input is still active
			if (self.editingText !== undefined) self.editingText.showElement();
			self.activeBlankInput.remove();
			self.activeBlankInput = undefined;
		}
		JSXMaster.deactivateAll();
		var end = JSXMaster.board.getMousePosition(e, 0);
		end[0] = e.clientX;
		end[1] = e.clientY;
		var coords = JSXMaster.getMouseCoords(e, 0);
		//var c = 
		//for(var key in JSXMaster.board.objects)
		for (var key in self.texts) {
			var txt = self.texts[key];
			if (txt.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
				return;
			}
		}
		//var changedE = e;
		//var end = JSXMaster.currentMouseCoords;
		//console.log(end[0] + " xxx " + end[1]);
		//console.log(window.innerWidth + " vs " + window.innerHeight);
		var ENTER_KEY_CODE = 13,
			ESC_KEY_CODE = 27;
		self.activeBlankInput =

			jQuery('<input type="text" wrap="soft" class="fonthighlight"></input>')
			.css({
				top: (end[1] - 10) + 'px',
				left: end[0] + 'px',
				width: '100px',
				height: '20px',
				'font-size': self.defaultStyle.fontSize + 'px',
				'line-height': '150%',
				'color': self.defaultStyle.strokeColor,
				'background-color': '#ffffff',
				//'margin': '100px',
				'border-radius': 5 + 'px',
				'overflow': 'hidden',
				'position': 'absolute'

			})
			.appendTo("body")
			.keydown(function (e) {
				if (e.shiftKey && e.which === ENTER_KEY_CODE) {
					return; // allow shift+enter to break lines
				}
				else if (e.which === ENTER_KEY_CODE) {
					//onCommit();
					//add text 
					console.log('it run');
					var options = { 'fontSize': self.activeBlankInput.css('font-size'), 'strokeColor': self.activeBlankInput.css('color') };
					//	var pos = [ideaInput.css('left'), ideaInput.css('top')];

					var pos = [coords.usrCoords[1], coords.usrCoords[2]];
					//console.log(JSON.stringify(e));

					var txt = self.addText(self.activeBlankInput.val(), options, pos);
					self.activeBlankInput.css('display', 'none');
					self.textInputs[txt.id] = self.activeBlankInput;
					//self.activeBlankInput.remove();
					self.activeBlankInput = undefined;
				} else if (e.which === ESC_KEY_CODE) {
					//onCancelEdit();
				} else if (e.which === 9) {
					//onCommit();
					e.preventDefault();
					//self.fire(':request', { type: 'addSubIdea', source: 'keyboard' });
					return;
				} else if (e.which === 83 && (e.metaKey || e.ctrlKey)) {
					e.preventDefault();
					//onCommit();
					return; /* propagate to let the environment handle ctrl+s */
				} else if (!e.shiftKey && e.which === 90 && (e.metaKey || e.ctrlKey)) {
					if (ideaInput.val() === self.unformattedText) {
						//	onCancelEdit();
					}
				}
				e.stopPropagation();
			})
				.focus(function () {
					//alert('fuck');
				}).dblclick(function () {
					//alert('hello');
					//this.css('display', 'block'); 
				});

	},
	clickText: function (e) {
		TextOBJ.activeTextId = this.id;
		//console.log('strokecolor:' + )
		gui.TextColorValue(this.getAttributes().strokecolor);
		gui.TextStrokeWidth(this.getAttributes().fontsize);
		document.getElementById(gui.TextColorInputId()).color.fromString(gui.TextColorValue());
		gui.ActivateItem('text');
		JSXMaster.deactivateExcept(this.id);
		TextOBJ.activateShape(this.id);
	},
	overText: function (e) {
		JSXMaster.disableBoardEvent();
		gui.showHideFakePoint('hide');
		JSXMaster.board.containerObj.style.cursor = 'move';
	},
	outText: function (e) {
		JSXMaster.enableBoardEvent();
		gui.showHideFakePoint('show');
		//JSXMaster.board.containerObj.style.cursor = 'default';
	},
	dblClick: function (txt) {
		JSXMaster.board.containerObj.addEventListener('dblclick', function (e) {
			console.log('dblClick:' + JSON.stringify(e));
			var self = TextOBJ,
				shape = JSXMaster.getActiveShape(),
				ENTER_KEY_CODE = 13,
				ESC_KEY_CODE = 27;
			if (self.editingText !== undefined) {
				var id = self.editingText.id,
					input = self.textInputs[id];
				input.css('display', 'none');
				self.editingText.showElement();
			}
			if (shape.type === 'text') {
				var txtInput = TextOBJ.textInputs[shape.activeShapeId];
				if (txtInput !== undefined) {
					var end = JSXMaster.board.getMousePosition(e, 0);
					var coords = JSXMaster.getMouseCoords(e, 0);
					txtInput.css('top', (end[1] - 15) + 'px');
					txtInput.css('left', (end[0] - 30) + 'px');
					//console.log('it works');
					txtInput.css('display', 'block');
					var textObj = shape.cls.texts[shape.activeShapeId];
					textObj.hideElement();
					//if(self.activeBlankInput !== undefined){
					//console.log('it is really good');
					//self.activeBlankInput.remove(); 
					//self.activeBlankInput = undefined;
					//}
					//self.activeBlankInput = txtInput;
					self.editingText = textObj;
					txtInput.unbind('keydown');
					txtInput.keydown(function (e) {
						if (e.shiftKey && e.which === ENTER_KEY_CODE) {
							return; // allow shift+enter to break lines
						}
						else if (e.which === ENTER_KEY_CODE) {
							var options = { 'fontSize': txtInput.css('font-size'), 'strokeColor': txtInput.css('color') };

							var pos = [coords.usrCoords[1], coords.usrCoords[2]];

							textObj.showElement();
							textObj.setText(txtInput.val());

							textObj.prepareUpdate().update().updateRenderer();
							textObj.setAttribute(options);
							txtInput.css('display', 'none');
							//self.activeBlankInput = undefined;
							self.editingText = undefined;
						} else if (e.which === ESC_KEY_CODE) {
							//onCancelEdit();
						} else if (e.which === 9) {
							//onCommit();
							e.preventDefault();
							//self.fire(':request', { type: 'addSubIdea', source: 'keyboard' });
							return;
						} else if (e.which === 83 && (e.metaKey || e.ctrlKey)) {
							e.preventDefault();
							//onCommit();
							return; /* propagate to let the environment handle ctrl+s */
						} else if (!e.shiftKey && e.which === 90 && (e.metaKey || e.ctrlKey)) {
							if (ideaInput.val() === self.unformattedText) {
								//	onCancelEdit();
							}
						}
						e.stopPropagation();
					});

				}
			}
		});
	},
	bindEventForText: function (text) {
		TextOBJ.texts[text.id] = text;
		text.on('down', TextOBJ.clickText);
		text.on('over', TextOBJ.overText);
		text.on('out', TextOBJ.outText);
		TextOBJ.dblClick();
	},
	binding: function () {
		JSXMaster.board.on('down', TextOBJ.down);
	},
	unbinding: function () {
		var self = TextOBJ;
		JSXMaster.board.off('down', TextOBJ.down);
		if (self.activeBlankInput !== undefined) {
			self.activeBlankInput.remove();
			self.activeBlankInput = undefined;
		}
	},
	showHideToolbar: function (action, type) {
		if (action === 'hide') {
			gui.ActivateItem('');
		} else {
			(type === undefined) ? type = 'text' : type = type;
			gui.ActivateItem(type)
		}
	},
	showVertices: function (cId) {
		var text = TextOBJ.texts[cId]
		text.setAttribute({ cssClass: 'fonthighlight' });
		//text.showElement('cssClass', );
	},
	hideVertices: function (cId) {
		var text = TextOBJ.texts[cId]
		//text.hideElement();
		text.setAttribute({ cssClass: 'myFont' });

	},
	activateShape: function (tId) {
		JSXMaster.setActiveShape(TextOBJ, tId, 'text');
		TextOBJ.showVertices(tId);
	},
	deactivateShape: function (tId) {
		TextOBJ.hideVertices(tId);
	}
};
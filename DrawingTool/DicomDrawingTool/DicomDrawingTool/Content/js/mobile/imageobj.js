﻿var ImageOBJ = {
    styles: {},
    images: {},
    activeImageId: undefined,
    removeShape: function (imageId) {
        //TODO
    },
    addImage: function (img, options) {
        options = options || ImageOBJ.styles;
        var w = img.width / JSXMaster.scale,
			h = img.height / JSXMaster.scale;

        var im = JSXMaster.board.create('image', [img.src, [-w / 2, -h / 2], [w, h]], options);
        ImageOBJ.bindEventForImage(im);
    },
    setBackground: function (img, options) {
        options = options || ImageOBJ.styles;
        options['fixed'] = true;
        var w = img.width / JSXMaster.scale,
			h = img.height / JSXMaster.scale;
        //w = JSXMaster.mapWidth;
        //h = JSXMaster.mapHeight;
        var im = JSXMaster.board.create('image', [img.src, [-w / 2, -h / 2], [w, h]], options);
        ImageOBJ.bindEventForImage(im);
    },
    addImage2: function (params, options) {
        options = options || ImageOBJ.styles;
        var im = JSXMaster.board.create('image', params, options);
        ImageOBJ.bindEventForImage(im);
    },
    init: function () {
        ImageOBJ.styles = {};
        //TextOBJ.activeTextId = undefined;
    },
    imageClicking: function (e) {

    },
    bindEventForImage: function (img) {
        ImageOBJ.images[img.id] = img;
        img.noHighlight = function () { } //override to disable highlight
        img.highlight = function () { }
    },
    binding: function () {

    },
    showVertices: function (cId) {
        var img = JSXMaster.board.objects[cId];
        img.showElement();
    },
    hideVertices: function (cId) {
        var img = JSXMaster.board.objects[cId];
        img.hideElement();
    },
    showHideToolbar: function (action, type) {
        if (action === 'hide') {
            gui.ActivateItem('');
        } else {
            (type === undefined) ? type = 'image' : type = type;
            gui.ActivateItem(type)
        }
    },
    activateShape: function (imgId) {
        JSXMaster.setActiveShape(ImageOBJ, imgId, 'image');
        ImageOBJ.showVertices(imgId);
    },
    deactivateShape: function (imgId) {
        ImageOBJ.hideVertices(imgId);
    }
};
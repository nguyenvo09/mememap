
DRAG = {
	down: function(e){
		var self = DRAG;
		var coords = JSXMaster.getMouseCoords(e);
		var shouldDisable = true;
		for(var id in JSXMaster.board.objects){
			var ele = JSXMaster.board.objects[id];
			if(id !== JSXMaster.fakePoint.id && ele.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
				shouldDisable = false;
				break;
			}
		}
		if(shouldDisable){
			console.log('should do it');
			JSXMaster.deactivateAll();
			gui.ActivateItem('');
			self.restoreStyle();
		}
	},
	restoreStyle: function(){
		//console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	binding: function(){
		JSXMaster.board.on('down', DRAG.down);
	},
	unbinding: function(){
		JSXMaster.board.off('down', DRAG.down);
	}
}
var GUI = function(){
	var self = this;
	self.Shapes = {};
	
	var inputIdname = "inputId";
	//var tempF = ko.observable();
	self.InputId = ko.observable(inputIdname);
	self.MapJSON = ko.observable("");
	self.map = undefined;
	self.init = function(){
		//alert("no");
		$(document).ready(function(){
			$('div.toolbar_item').hover(function(){
				//alert('shit');
				$(this).addClass('toolbar_item_hover');
			}, function(){
				$(this).removeClass('toolbar_item_hover');
			});
			self.InitImageEvent();
			self.InitImageEvent2();
			self.InitLoadFile();
			self.InitColorInput();
			self.InitTextColor();
			self.Shapes['line'] = Line;
			self.Shapes['polygon'] = Polygon;
			self.Shapes['rectangle'] = Rectangle;
			self.Shapes['circle'] = Circle;
			self.Shapes['text'] = TextOBJ;
			self.Shapes['image'] = ImageOBJ;
		});
		var keyDown = function (Evt) {
			var code;
			if (!Evt) Evt = window.event;
			if (Evt.which) {
				code = Evt.which;
			} else if (Evt.keyCode) {
				code = Evt.keyCode;
			}
			// 37: left,  38: up,  39: right,  40: down
			if (code==37) { self.ShiftLeft(); return false;}
			else if (code==38) { self.ShiftUp(); return false;}
			else if (code==39) { self.ShiftRight(); return false;}
			else if (code==40) { self.ShiftDown(); return false;}
			return true;
		}
		document.onkeydown = keyDown;
		
	};
	self.bindEvent = function(type){
		if(self.Shapes[type] !== undefined){
			var s = self.Shapes[type];
			s.binding();
		}
	}
	self.unbindEvent = function(type){
		if(self.Shapes[type] !== undefined){
			var s = self.Shapes[type];
			s.unbinding();
		}
	}
	self.bindOrUnbind = function(type){
		var status = self.ToolbarList[type];
		if(status === undefined) return;
		if(status() === 'none'){
			//show it
			self.showHideFakePoint('show');
			self.bindEvent(type);
		}else{
			self.showHideFakePoint('hide');
			self.unbindEvent(type);
		}
	}
	self.drawRect = function(){
		var status = self.ToolbarList['rectangle'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('rectangle');
		JSXMaster.setDrawingMode(Rectangle, 'rectangle');
		JSXMaster.showFakePnt();
	}
	self.drawCircle = function(){
		var status = self.ToolbarList['circle'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('circle');
		JSXMaster.setDrawingMode(Circle, 'circle');
		JSXMaster.showFakePnt();
	}
	self.drawPolygon = function(){
		var status = self.ToolbarList['polygon'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('polygon');
		JSXMaster.setDrawingMode(Polygon, 'polygon');
		JSXMaster.showFakePnt();
	}
	self.DeleteShape = function(){
		JSXMaster.deleteActiveShape();
		JSXMaster.showToolbar();
		
	}
	self.drawShape = function(type){
		self.resetBinding();
		self.bindOrUnbind(type);
		self.ShowOnOffBar(type);
		//JSXMaster.deactivateAll();
		
	}
	
	/**unbind all events.**/
	self.resetBinding = function(){
		Polygon.unbinding();
		Circle.unbinding();
		Line.unbinding();
		Rectangle.unbinding();
		TextOBJ.unbinding();
		DRAG.unbinding();
		self.DeactivateShape();
		self.showHideFakePoint('hide');
		JSXMaster.setDrawingMode(undefined);
		JSXMaster.board.containerObj.style.cursor = 'default';
		//JSXMaster.SHAPE_MODE = undefined;
	}
	self.DeactivateShape = function(){
		//alert('Drag deactivate');
		JSXMaster.deactivateAll();
	}
	self.bindDragMode = function(){
		DRAG.binding();
	}
	self.Drag = function(){
		//self.DeactivateShape();
		self.resetBinding();
		self.bindDragMode();
		self.ActivateItem('drag');
		//JSXMaster.setDrawingMode(DRAG, 'drag');
	}
	self.reInitMap = function(){
		Polygon.init();
		Rectangle.init();
	}
	self.showHideFakePoint = function(type){
		if(type === undefined) type = 'hide';
		(type === 'show') ? JSXMaster.showFakePnt() : JSXMaster.hideFakePnt();
	}
	self.removeAll = function(){
		JXG.JSXGraph.freeBoard(JSXMaster.board);
		JSXMaster.init();
	}
	self.saveFile = function(){
		//alert("save file");
		var map = {},  
			points_new = [], 
			lines_new = [], 
			circles_new = [], 
			polygons_new = [], 
			rectangles_new = [],
			texts = [],
			images = [];
		//data structure for circles
		for(var circleId in Circle.circles){
			var obj = Circle.circles[circleId];
			var circle = {id: obj.id, center: obj.center.id, point2: obj.point2.id, attrs: obj.getAttributes()};
			circles_new.push(circle);
		}
		//data structure for polygons
		for(var polygonId in Polygon.lines){
			var lines = Polygon.lines[polygonId];
			var edges = [];
			
			for(var i=0; i<lines.length; i++){
				var first = lines[i].point1, second = lines[i].point2;
				edges.push({first: first.id, second: second.id, attrs: lines[i].getAttributes()});
			}
			
			var poly = {polygonId: polygonId, edges: edges};
			polygons_new.push(poly);
		}
		//create data structure for list of rectangles
		for(var rectId in Rectangle.rects){
			var lines = Rectangle.lines[rectId];
			var edges = [];
			
			for(var i=0; i<lines.length; i++){
				var first = lines[i].point1, second = lines[i].point2;
				edges.push({first: first.id, second: second.id, attrs: lines[i].getAttributes()});
			}
			
			var rect = {rectId: rectId, edges: edges};
			rectangles_new.push(rect);
		}
		//data structure for lines
		for(var lineId in Line.lines){
			//alert(lineId);
			var obj = Line.lines[lineId];
			var line = {id: obj.id, point1: obj.point1.id, point2: obj.point2.id, attrs: obj.getAttributes()};
			lines_new.push(line);		
		}
		for(var textId in TextOBJ.texts){
			var obj = TextOBJ.texts[textId];
			var txt = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes(), content: obj.plaintext};
			texts.push(txt);
		}
		for(var imgId in ImageOBJ.images){
			var obj = ImageOBJ.images[imgId];
			var im = {id: obj.id, coords: [obj.X(), obj.Y()], dims: obj.size, attrs: obj.getAttributes(), url: obj.url};
			images.push(im);
		}
		for (var el in JSXMaster.board.objects) {
			var obj = JSXMaster.board.objects[el];
			console.log(obj.getType());
			if(obj.getType() === 'point' && el !== JSXMaster.fakePoint.id){
				var point = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes()};
				points_new.push(point);
			}else if(obj.getType() === 'text'){
				
			}else if(obj.getType() === 'image'){
				//var img = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes()};
				//images.push(txt);
			}
			
		}
		
		map["points"] = points_new;
		map["lines"] = lines_new;
		map["circles"] = circles_new;
		map["polygons"] = polygons_new;
		map["rectangles"] = rectangles_new;
		map["texts"] = texts;
		map['images'] = images;
		//alert(JSON.stringify(lines_new));
		var result = JSON.stringify(map);
		var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "drawing.txt");
	}
	self.drawMap = function(map){
		
		self.removeAll();
		self.ActivateItem('');
		self.resetBinding();
		self.reInitMap();
		//alert(map.points.length);
		for(var i=0; i<map.points.length; i++){
			//alert("point la: " + JSON.stringify(map.points[i]));
			var point = map.points[i];
			var p = JSXMaster.board.create('point', [point.x, point.y], point.attrs);
			p.id = point.id;
		}
		
		
		for(var i=0; i<map.lines.length; i++){
			var line = map.lines[i];
			var p1 = JSXMaster.board.objects[line.point1];
			var p2 = JSXMaster.board.objects[line.point2];
			var l = JSXMaster.board.create('line', [p2, p1], line.attrs);
			l.id = line.id;
			Line.bindEventForLine2(l);
		}
		
		for(var i=0; i<map.circles.length; i++){
			var circle = map.circles[i];
			//alert("hell");
			var center = JSXMaster.board.objects[circle.center];
			var point2 = JSXMaster.board.objects[circle.point2];
			//alert("center la: " + center);
			//alert("point2 la: " + point2);
			var c = JSXMaster.board.create('circle', [center, point2], circle.attrs);
			c.id = circle.id;
			Circle.bindEventForCircle2(c);
		}
		for(var i=0; i<map.polygons.length; i++){
			var poly = map.polygons[i];
			//alert(JSON.stringify(poly));	
			var polygonId = poly.polygonId, edges = poly.edges;
			for(var j=0; j<edges.length; j++){
				var edge = edges[j];
				var f = edge.first, 
					s = edge.second, 
					attrs = edge.attrs;
				//alert(f + " " + s);
			
				var p1 = JSXMaster.board.objects[f];
				var p2 = JSXMaster.board.objects[s];
				var l = JSXMaster.board.create('line', [p1, p2], attrs);
				
				//----- Add line.
				Polygon.lines[polygonId] = Polygon.lines[polygonId] || [];
				Polygon.lines[polygonId].push(l);
				//-----Add points and mapping Points
				Polygon.polygons[polygonId] = Polygon.polygons[polygonId] || [];
				if(Polygon.mappingPoints[f] === undefined){
					Polygon.polygons[polygonId].push(p1);
					Polygon.mappingPoints[f] = polygonId;
				}
				if(Polygon.mappingPoints[s] === undefined){
					Polygon.polygons[polygonId].push(p2);
					Polygon.mappingPoints[s] = polygonId;
				}
				
				//l.on('over', Polygon.overLine);
				//l.on('out', Polygon.outLine);
			}
			Polygon.initDraggingListener(polygonId, false);
			Polygon.initHoverBinding(polygonId);
			Polygon.counter++;
		}
		for(var i=0; i<map.rectangles.length; i++){
			var rect = map.rectangles[i];
			var rectId = rect.rectId, edges = rect.edges, f = edges.first, s = edges.second, attrs = edges.attrs;
			
			for(var j=0; j<edges.length; j++){
				var edge = edges[j];
				var f = edge.first, 
					s = edge.second, 
					attrs = edge.attrs;
				//alert(f + " " + s);
				//Rectangle.mappingPoints[f] = rectId;
				//Rectangle.mappingPoints[s] = rectId;
				var p1 = JSXMaster.board.objects[f];
				var p2 = JSXMaster.board.objects[s];
				var l = JSXMaster.board.create('line', [p1, p2], attrs);
				
				//----- Add line.
				Rectangle.lines[rectId] = Rectangle.lines[rectId] || [];
				Rectangle.lines[rectId].push(l);
				//-----Add points and mapping Points
				Rectangle.rects[rectId] = Rectangle.rects[rectId] || [];
				if(Rectangle.mappingPoints[f] === undefined){
					Rectangle.rects[rectId].push(p1);
					Rectangle.mappingPoints[f] = rectId;
				}
				if(Rectangle.mappingPoints[s] === undefined){
					Rectangle.rects[rectId].push(p2);
					Rectangle.mappingPoints[s] = rectId;
				}
				
			}
			
			//var p1 = JSXMaster.board.objects[f];
			//var p2 = JSXMaster.board.objects[s];
			//var l = JSXMaster.board.create('line', [p1, p2], attrs);
			Rectangle.initHoverBinding(rectId);
			Rectangle.initDraggingListener(rectId, false);
			Rectangle.counter++;
		}
		for(var i=0; i<map.texts.length; i++){
			var txt = map.texts[i];
			var text = JSXMaster.board.create('text', [txt.x, txt.y, txt.content], txt.attrs);
			text.id = txt.id;
			TextOBJ.texts[text.id] = text;
		}
		for(var i=0; i<map.images.length; i++){
			var img = map.images[i];
			//alert(JSON.stringify([img.coords, img.dims]));
			var w = img.dims[0] / JSXMaster.scale,
				h = img.dims[1] / JSXMaster.scale;
			//ImageOBJ.setBackground(img);
			ImageOBJ.addImage2([img.url, img.coords, [w, h]], img.attrs);
			//var image = JSXMaster.board.create('image', [img.src, img.coords, img.dims], img.attrs);
			//image.id = img.id;
			//Image.images[image.id] = img;
		}
	}
	self.LoadFile = function(){
	   var elem = document.getElementById(self.InputId());
	   if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
	   }
	}
	self.InitLoadFile = function(){
		
		var fileInput = document.getElementById(self.InputId());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			//alert("change");
			var file = fileInput.files[0];
			var textType = /text.*/;

			if (file.type.match(textType)) {
				var reader = new FileReader();

				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					//alert(fileDisplayArea);
					self.MapJSON(fileDisplayArea);
					self.map = JSON.parse(fileDisplayArea);
					//alert(JSON.stringify(self.map));
					self.drawMap(self.map);
				}

				reader.readAsText(file);	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	//-------------------------------------------------------------------------------------//
	self.ImageInputId = ko.observable("image_background");
	self.LoadImage = function(){
		//No Need
		//console.log('load image');
		var elem = document.getElementById(self.ImageInputId());
		if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
		}
	}
	self.InitImageEvent = function(){
		
		var fileInput = document.getElementById(self.ImageInputId());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var imageType = /image.*/;
			//alert(JSON.stringify(file.path));
			if (file.type.match(imageType)) {
				//var im = JSXMaster.board.create('image', [file.path, [-3,-2], [3,3]]);
				var reader = new FileReader();
				var img = new Image;
				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					img.src = reader.result;
					//var w = img.width / JSXMaster.scale,
					//	h = img.height / JSXMaster.scale;
					ImageOBJ.addImage(img);
					//var im = JSXMaster.board.create('image', [img.src, [-3,-2], [w, h]]);
				}
				reader.readAsDataURL(file)	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	//-------------------------------------------------------------------------------------//
	
	self.ImageInputId2 = ko.observable("img_background_full");
	self.LoadImage2 = function(){
		var elem = document.getElementById(self.ImageInputId2());
		if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
		}
	}
	self.InitImageEvent2 = function(){
		
		var fileInput = document.getElementById(self.ImageInputId2());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var imageType = /image.*/;
			//alert(JSON.stringify(file.path));
			if (file.type.match(imageType)) {
				//var im = JSXMaster.board.create('image', [file.path, [-3,-2], [3,3]]);
				var reader = new FileReader();
				var img = new Image;
				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					img.src = reader.result;
					ImageOBJ.setBackground(img);
				}
				reader.readAsDataURL(file)	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	
	//-------------------------------------------------------------------------------------//
	self.ZoomIn = function(){
		JSXMaster.board.zoomIn();
	}
	self.ZoomOut = function(){
		JSXMaster.board.zoomOut();
	}
	self.Zoom100 = function(){
		JSXMaster.board.zoom100();
	}
	//self.Toolbar_Item_Hover = ko.observable("");
	self.ZoomBarId = ko.observable("zooming_bar_menu4");
	self.ZoomBarStatus = ko.observable("none");
	self.ShowZoomingBar = function(){
		self.ShowOnOffBar('zoom');	
	}
	self.OriginX = ko.observable(JSXMaster.board.origin.scrCoords[1]);
	self.OriginY = ko.observable(JSXMaster.board.origin.scrCoords[2]);
	self.ShiftLeft = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x - x*0.1, y);
	}
	self.ShiftRight = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x + x*0.1, y);
	}
	self.ShiftCenter = function(){
		JSXMaster.board.moveOrigin(self.OriginX(), self.OriginY());
	}
	self.ShiftUp = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y + y * 0.1);
	}
	self.ShiftDown = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y - y * 0.1);
	}
	//----------------Line bar -------------------------//
	self.LineBarId = ko.observable("line_bar_id");
	self.LineBarStatus = ko.observable("none");
	
	self.drawLine = function(){
		var status = self.ToolbarList['line'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('line');
		JSXMaster.setDrawingMode(Line, 'line');
		JSXMaster.showFakePnt();
	}
	self.SolidActive = ko.observable("");
	self.DotActive = ko.observable("");
	self.DashActive = ko.observable("");
	self.setShapeType = function(shapeType, boundaryType){
		
		var dash = 0;
		if(boundaryType === 'dash'){
			dash = 2;
			gui.DashActive("toolbar_item_click"); 
			gui.DotActive("");
			gui.SolidActive("");
		}else if(boundaryType === 'dot'){
			dash = 1;
			gui.DashActive(""); 
			gui.DotActive("toolbar_item_click");
			gui.SolidActive("");
		}else if(boundaryType === 'solid'){
			dash = 0;
			gui.DashActive(""); 
			gui.DotActive("");
			gui.SolidActive("toolbar_item_click");
		}
		switch(shapeType){
			case 'line':
				Line.setShapeType(dash);
				break;
			case 'polygon':
				Polygon.setShapeType(dash);
				break;
			case 'rectangle':
				Rectangle.setShapeType(dash);
				break;
			case 'circle':
				Circle.setShapeType(dash);
				break;
			case 'text':
				break;
			case 'image':
				break;
			default:
				break;
		}
		
	}
	//---------------------------------------------------------------------------------
	self.LineStrokeWidth = ko.observable('5');
	self.LineColorInputId = ko.observable('lineColor');
	self.LineColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.CircleStrokeWidth = ko.observable('5');
	self.CircleColorInputId = ko.observable('circleColorId');
	self.CircleColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.PolygonStrokeWidth = ko.observable('5');
	self.PolygonColorInputId = ko.observable('polygonColorId');
	self.PolygonColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.RectStrokeWidth = ko.observable('5');
	self.RectColorInputId = ko.observable('rectColorId');
	self.RectColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	
	self.InitColorInput = function(){
		var lineInput = document.getElementById(self.LineColorInputId());
		if(lineInput !== null){
			lineInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Line.setShapeColor(color);
			});
		}
		//-----------------------------
		var circleInput = document.getElementById(self.CircleColorInputId());
		if(circleInput !== null){
			circleInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Circle.setShapeColor(color);
			});
		}
		//-----------------------------
		var polyInput = document.getElementById(self.PolygonColorInputId());
		if(polyInput !== null){
			polyInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Polygon.setShapeColor(color);
			});
		}
		//-----------------------------
		var rectInput = document.getElementById(self.RectColorInputId());
		if(rectInput !== null){
			rectInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Rectangle.setShapeColor(color);
			});
		}
	}
	/*
	self.LineStrokeWidthChanged = function(){
		//alert(self.LineStrokeWidth());
		if(Line.activeLineId !== undefined){
			var activeLine = JSXMaster.board.objects[Line.activeLineId];
			var w = parseInt(self.LineStrokeWidth());
			activeLine.setAttribute({strokeWidth: w});
		}else{
			Line.options.strokeWidth = parseInt(self.LineStrokeWidth());
		}
	}*/
	self.ShapeStrokeWidthChanged = function(shapeType){
		switch(shapeType){
			case 'line':
				//Line.setShapeType(boundaryType);
				var w = parseInt(self.LineStrokeWidth());
				Line.setStrokeWidth(w);
				break;
			case 'polygon':
				var w = parseInt(self.PolygonStrokeWidth());
				Polygon.setStrokeWidth(w);
				break;
			case 'rectangle':
				var w = parseInt(self.RectStrokeWidth());
				Rectangle.setStrokeWidth(w);
				break;
			case 'circle':
				var w = parseInt(self.CircleStrokeWidth());
				Circle.setStrokeWidth(w);
				break;
			case 'text':
				break;
			case 'image':
				break;
			default:
				break;
		}
	}
	//--------------------------------For text----------------------------------------//
	self.drawText = function(type){
		self.resetBinding();
		TextOBJ.init();
		
		TextOBJ.activeTextId = undefined;
		self.ShowOnOffBar('text');
		$('input#' + self.TextInputId()).focus();
		JSXMaster.setDrawingMode(TextOBJ, 'text');
		//JSXMaster.board.
	}
	self.FocusText = function(){
		self.resetBinding();
		TextOBJ.activeTextId = undefined;
		self.ActivateItem('text');
	}
	self.TextInputId = ko.observable("textInputId_temp");
	self.AddText = function(d, e){
		if(e.keyCode === 13){
			JSXMaster.setDrawingMode(TextOBJ, 'text');
			TextOBJ.activeTextId = undefined;
			var text = $('input#' + self.TextInputId()).val();
			$('input#' + self.TextInputId()).val('');
			TextOBJ.addText(text);
		}
		
	}
	self.TextBarId = ko.observable("textToolbarEdit");
	self.TextBarStatus = ko.observable("none");
	self.TextStrokeWidth = ko.observable(TextOBJ.defaultStyle.fontSize);
	self.TextStrokeWidthChanged = function(){
		//TODO
		if(TextOBJ.activeTextId !== undefined){
			var activeText = JSXMaster.board.objects[TextOBJ.activeTextId];
			var w = parseInt(self.TextStrokeWidth());
			activeText.setAttribute({fontSize: w});
		}else{
			TextOBJ.defaultStyle.fontSize = parseInt(self.TextStrokeWidth());
		}
	}
	self.TextColorInputId = ko.observable("TextColorInputId");
	self.TextColorValue = ko.observable(TextOBJ.defaultStyle.strokeColor);
	self.InitTextColor = function(){
		
		//alert(TextOBJ.defaultStyle.strokeColor);
		var input = document.getElementById(self.TextColorInputId());
		if(input !== null){
			
			input.addEventListener('change', function(e){
				var color = '#' + this.color;
				
				if(TextOBJ.activeTextId !== undefined){
					var activeText = JSXMaster.board.objects[TextOBJ.activeTextId];
					activeText.setAttribute({strokeColor: color});
				}else{
					TextOBJ.defaultStyle.strokeColor = color;
				}
			});
			
		}
	}
	//------------------------------------------------------------------------//
	self.PolygonBarId = ko.observable("polygon_toolbar_id");
	self.RectBarId = ko.observable("square_toolbar_id");
	self.CircleBarId = ko.observable("circle_toolbar_id");
	self.ShowOnOffBar = function(type){
		var status = self.ToolbarList[type];
		if(status === undefined) return;
		(status() === 'none') ? self.ActivateItem(type) : self.ActivateItem('');
	}
	//------------------------------------------------------------------------//

	self.DragActive = ko.observable("");
	self.RectActive = ko.observable("");
	self.CircleActive = ko.observable("");
	self.LineActive = ko.observable("");
	self.PolygonActive = ko.observable("");
	self.ZoomActive = ko.observable("");
	self.TextActive = ko.observable("");
	self.PolygonBarStatus = ko.observable('none');
	self.CircleBarStatus = ko.observable('none');
	self.RectBarStatus = ko.observable('none');
	
	self.ItemList = {};
	self.ToolbarList = {};
	self.ItemList["drag"] = self.DragActive;
	self.ItemList["rectangle"] = self.RectActive;
	self.ItemList["circle"] = self.CircleActive;
	self.ItemList["line"] = self.LineActive;
	self.ItemList["polygon"] = self.PolygonActive;
	self.ItemList['zoom'] = self.ZoomActive;
	self.ItemList['text'] = self.TextActive;
	
	self.ToolbarList['zoom'] = self.ZoomBarStatus;
	self.ToolbarList['line'] = self.LineBarStatus;
	self.ToolbarList['text'] = self.TextBarStatus;
	self.ToolbarList['rectangle'] = self.RectBarStatus;
	self.ToolbarList['polygon'] = self.PolygonBarStatus;
	self.ToolbarList['circle'] = self.CircleBarStatus;

	self.ActivateItem = function(item){
		for(var key in self.ItemList){
			self.ItemList[key]('');
		}
		for(var key in self.ToolbarList){
			self.ToolbarList[key]('none');
		}
		var it = self.ItemList[item];
		if(it !== undefined){	
			it('toolbar_item_click');
		}
		var toolbar = self.ToolbarList[item];
		if(toolbar !== undefined){
			toolbar('block');
			
		}
	}
	//----------------Line bar
	
};
GUI.prototype = {
	deleteObj : function(obj){
		JSXMaster.board.removeObject(obj);
	},
	deleteDict: function(dict){
		//TODO
	}
}
var gui = new GUI();
gui.init();
ko.applyBindings(gui);
/*
 Copyright 2011 - 2014

 Alfred Wassermann
 Michael Gerhaeuser,
 Carsten Miller,
 Matthias Ehmann,
 Heiko Vogel

 This file is part of the JSXGraph GUI project.
 This code isn't licensed yet.
 */

(function () {

    'use strict';

    var context;

    GUI.Audio = {

        /**
         * List of available sounds.
         * @type Object
         */
        sounds: {
            'click': 'audio-click',
            'bubble': 'audio-bubble',
            'plastic': 'audio-plastic'
        },

        levels: [0, 0.23, 0.55, 1],

        /**
         * Sound enabled?
         * @type Boolean
         * @default true
         */
        enabled: true,

        /**
         * Dummy player to extract supported codecs etc.
         * @type Node
         */
        //dummy: document.getElementById('audio-dummy'),

        /**
         * Used sound format, depends on the browser. Possible values: 'mp3' and 'ogg'.
         * @type String
         * @default 'mp3'
         */
        format: 'mp3',

        /**
         * Plays a sound.
         * @param what What sound to play. Currently only 'click' is available.
         */
        /*
         play: function (what) {
         var volume = GUI.Settings.get('volume');
         //base = 6*Math.E;

         if (GUI.Settings.get('sound') && GUI.Audio.sounds[what] && GUI.Audio[what] && volume > 0) {
         //GUI.Audio[what].volume = volume / 3;
         //GUI.Audio[what].volume = (Math.pow(base, volume*0.33333)-1)/(base-1);
         GUI.Audio[what].volume = this.levels[volume];
         GUI.Audio[what].play();
         }
         },
         */

        init: function () {
            var s, format;

            try {
                // still needed for Safari
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                // create an AudioContext
                context = new AudioContext();
//console.log('Web Audio API supported');

                format = '.' + (new Audio().canPlayType('audio/ogg') !== '' ? 'ogg' : 'mp3');
                for (s in this.sounds) if (this.sounds.hasOwnProperty(s)) {
                    this.loadSound(s, format);
                }

            } catch (e) {
                // API not supported
                //throw new Error('Web Audio API not supported.');
                console.log('Web Audio API NOT supported');
                this.play = function () {
                };
            }

        },

        loadSound: function (sound, format) {
            var request = new XMLHttpRequest(),
                url = 'audio/' + sound + format,
                that = this;

//console.log('Load', url);                
            request.open('GET', url, true);
            request.responseType = 'arraybuffer';

            request.onload = function () {
                // request.response is encoded... so decode it now
                context.decodeAudioData(request.response, function (buffer) {
                    that.sounds[sound] = buffer;
                }, function (err) {
                    throw new Error(err);
                });
            };
            request.send();
        },

        play: function (what) {
            var source = context.createBufferSource(),
                gainNode = context.createGain();

            source.buffer = this.sounds[what];
            //source.connect(context.destination);
            // connect the source to the gain node
            source.connect(gainNode);
            // set the gain (volume)
            gainNode.gain.value = this.levels[GUI.Settings.get('volume')];
            // connect gain node to destination
            gainNode.connect(context.destination);
            source.start(0);
        }
    };

    GUI.Audio.init();

    /*
     // determine codec
     if (GUI.Audio.dummy) {
     GUI.Audio.format = (typeof GUI.Audio.dummy.canPlayType === "function" && GUI.Audio.dummy.canPlayType("audio/mpeg") !== "") ? 'mp3' : 'ogg';

     // load audio files
     setTimeout(function() {
     var s;
     for (s in GUI.Audio.sounds) {
     if (GUI.Audio.sounds.hasOwnProperty(s)) {
     GUI.Audio[s] = document.getElementById(GUI.Audio.sounds[s]);
     GUI.Audio[s].src = 'audio/' + s + '.' + GUI.Audio.format;
     }
     }
     }, 200);
     }
     */

    /**
     * Browser independent vibrate function.
     */
    GUI.Audio.vibrate = (function () {
        if (navigator.vibrate) {
            return function (dur) {
                if (GUI.Settings.get('vibrate')) {
                    return navigator.vibrate(dur);
                }
            };
        }

        if (navigator.mozVibrate) {
            return function (dur) {
                if (GUI.Settings.get('vibrate')) {
                    return navigator.mozVibrate(dur);
                }
            };
        }

        if (navigator.webkitVibrate) {
            return function (dur) {
                if (GUI.Settings.get('vibrate')) {
                    return navigator.webkitVibrate(dur);
                }
            };
        }

        if (navigator.oVibrate) {
            return function (dur) {
                if (GUI.Settings.get('vibrate')) {
                    return navigator.oVibrate(dur);
                }
            };
        }

        if (navigator.msVibrate) {
            return function (dur) {
                if (GUI.Settings.get('vibrate')) {
                    return navigator.msVibrate(dur);
                }
            };
        }

        return function (dur) {
        };
    }());

}());

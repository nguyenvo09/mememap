/**
* jQuery Draggable Touch v0.5
* Jonatan Heyman | http://heyman.info
*
* Make HTML elements draggable by using uses touch events.
* The plugin also has a fallback that uses mouse events,
* in case the device doesn't support touch events.
*
* Licenced under THE BEER-WARE LICENSE (Revision 42):
* Jonatan Heyman (http://heyman.info) wrote this file. As long as you retain this
* notice you can do whatever you want with this stuff. If we meet some day, and
* you think this stuff is worth it, you can buy me a beer in return.
*/
;(function($){
    $.fn.draggableTouch = function(action) {
        // check if the device has touch support, and if not, fallback to use mouse
        // draggableMouse which uses mouse events
        if (!("ontouchstart" in document.documentElement)) {
            return this.draggableMouse(action);
        }
        
        // check if we shall make it not draggable
        if (action !== null && typeof action == 'string' && action == "disable") {
            this.unbind("touchstart");
            this.unbind("touchmove");
            this.unbind("touchend");
            this.unbind("touchcancel");
            return this;
        }
        
        this.each(function() {
            var element = $(this);
            var handle = element;
            var offset = null;
            var callback = function() {};
            
            if (action !== null && typeof action !== 'string') {
                handle = $(action.handle);
            }

            if (action !== null && typeof action !== 'string' && (typeof action.callback !== 'undefined')) {
                callback = action.callback;
            }
            
            var end = function(e) {
                e.preventDefault();
                var orig = e.originalEvent;
                
                if (typeof orig === 'undefined') {
                    return;
                }
                element.trigger("dragend", {
                    left: Math.min(orig.changedTouches[0].pageX - offset.x, window.innerWidth),
                    top: Math.min(
                            Math.max(orig.changedTouches[0].pageY - offset.y, 0), 
                            window.innerHeight - 20
                         )
                });
            };
            
            handle.bind("touchstart", function(e) {
                var orig = e.originalEvent;
                var pos = element.position();

                if (typeof orig === 'undefined') {
                    return;
                }
                offset = {
                    x: orig.changedTouches[0].pageX - pos.left,
                    y: orig.changedTouches[0].pageY - pos.top
                };
                element.trigger("dragstart", pos);
            });
            
            handle.bind("touchmove", function(e) {
                e.preventDefault();
                var orig = e.originalEvent;
                
                if (typeof orig === 'undefined') {
                    return;
                }
                // do now allow two touch points to drag the same element
                if (orig.targetTouches.length > 1) {
                    return;
                }
                
                element.css({
                    left: Math.min(orig.changedTouches[0].pageX - offset.x, window.innerWidth),
                    top: Math.min(
                            Math.max(orig.changedTouches[0].pageY - offset.y, 0), 
                            window.innerHeight - 20
                         )
                });
                callback();
            });
            
            handle.bind("touchend", end);
            handle.bind("touchcancel", end);
        });
        return this;
    };
    
    /**
* Draggable fallback for when touch is not available
*/
    $.fn.draggableMouse = function (action) {
        // check if we shall make it not draggable
        if (action == "disable") {
            this.unbind("mousedown");
            this.unbind("mouseup");
            return this;
        }
        
        this.each(function() {
            var element = $(this);
            var handle = element;
            var offset = null;
            var callback = function() {};
            
            if (action !== null && typeof action !== 'string') {
                handle = $(action.handle);
            }

            if (action !== null && typeof action !== 'string' && (typeof action.callback !== 'undefined')) {
                callback = action.callback;
            }
            
            var move = function(e) {
                element.css({
                    left: e.pageX - offset.x,
                    top: Math.min(Math.max(e.pageY - offset.y, 0), window.innerHeight - 20)
                });
                callback();
            };
            
            var up = function(e) {
                handle.unbind("mouseup", up);
                $(document).unbind("mousemove", move);
                element.trigger("dragend", {
                    left: e.pageX - offset.x,
                    top: Math.min(Math.max(e.pageY - offset.y, 0), window.innerHeight - 20)
                });
            };
            
            handle.bind("mousedown", function(e) {
                var pos = element.position();
                offset = {
                    x: e.pageX - pos.left,
                    y: e.pageY - pos.top
                };
                $(document).bind("mousemove", move);
                handle.bind("mouseup", up);
                element.trigger("dragstart", pos);
            });
        });
        return this;
    };
})(jQuery);


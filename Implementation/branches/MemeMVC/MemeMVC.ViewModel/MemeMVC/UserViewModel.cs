using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class UserViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int UserID { get; set; }
        public UserDTO User { get; set; }
        public UserViewModel()
        {
            User = new UserDTO();
        }
    }
    public class UsersViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<UserDTO> Users { get; set; }
        public UsersViewModel()
        {
            Users = new List<UserDTO>();
        }
    }
}

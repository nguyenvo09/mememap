using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class ProjectViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int ProjectID { get; set; }
        public ProjectDTO Project { get; set; }
        public ProjectViewModel()
        {
            Project = new ProjectDTO();
        }
    }
    public class ProjectsViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<ProjectDTO> Projects { get; set; }
        public ProjectsViewModel()
        {
            Projects = new List<ProjectDTO>();
        }
    }
}

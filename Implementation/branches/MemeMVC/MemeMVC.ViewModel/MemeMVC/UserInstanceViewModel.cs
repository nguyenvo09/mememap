using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class UserInstanceViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int UserInstanceID { get; set; }
        public UserInstanceDTO UserInstance { get; set; }
        public UserInstanceViewModel()
        {
            UserInstance = new UserInstanceDTO();
        }
    }
    public class UserInstancesViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<UserInstanceDTO> UserInstances { get; set; }
        public UserInstancesViewModel()
        {
            UserInstances = new List<UserInstanceDTO>();
        }
    }
}

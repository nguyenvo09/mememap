using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class MapViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int MapID { get; set; }
        public MapDTO Map { get; set; }
        public MapViewModel()
        {
            Map = new MapDTO();
        }
    }
    public class MapsViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<MapDTO> Maps { get; set; }
        public MapsViewModel()
        {
            Maps = new List<MapDTO>();
        }
    }

    public class SuggestWordDTO
    {
        public string name { get; set; }
        public string image { get; set; }
    }
    public class HeadLineDTO
    {
        public string name { get; set; }
        public string image { get; set; }
        public string link { get; set; }
    }
    public class SuggestDTO
    {
        public List<HeadLineDTO> headlines { get; set; }
    }
    public class SuggestionViewModel
    {
        public List<SuggestWordDTO> words { get; set; }
        public SuggestDTO suggests { get; set; }
        public SuggestionViewModel()
        {
            words = new List<SuggestWordDTO>();
            suggests = new SuggestDTO();
        }
    }
    public class IdeaViewModel
    {
        public string Result { get; set; }
        public MAPJSNode.MAPJSIdea Idea { get; set; }
        public IdeaViewModel()
        {
            Idea = new MAPJSNode.MAPJSIdea();
        }
    }
}

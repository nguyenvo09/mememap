using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class NoteViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int NoteID { get; set; }
        public NoteDTO Note { get; set; }
        public NoteViewModel()
        {
            Note = new NoteDTO();
        }
    }
    public class NotesViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<NoteDTO> Notes { get; set; }
        public NotesViewModel()
        {
            Notes = new List<NoteDTO>();
        }
    }
    
}

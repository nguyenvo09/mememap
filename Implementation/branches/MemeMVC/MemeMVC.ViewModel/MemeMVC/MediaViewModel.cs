using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class MediaViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int MediaID { get; set; }
        public MediaDTO Media { get; set; }
        public MediaViewModel()
        {
            Media = new MediaDTO();
        }
    }
    public class MediasViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<MediaDTO> Medias { get; set; }
        public MediasViewModel()
        {
            Medias = new List<MediaDTO>();
        }
    }
}

using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class ZoneViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int ZoneID { get; set; }
        public ZoneDTO Zone { get; set; }
        public ZoneViewModel()
        {
            Zone = new ZoneDTO();
        }
    }
    public class ZonesViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<ZoneDTO> Zones { get; set; }
        public ZonesViewModel()
        {
            Zones = new List<ZoneDTO>();
        }
    }


  
}

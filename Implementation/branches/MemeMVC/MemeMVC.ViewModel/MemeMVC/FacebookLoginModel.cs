using System.Collections.Generic;
using System;
namespace MemeMVC.ViewModel
{
    public class Picture
    {
        public PicureData data { get; set; }
    }

    public class PicureData
    {
        public string url { get; set; }
        public bool is_silhouette { get; set; }
    }
    public class FacebookLocation
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class FacebookLoginModel
    {
        public string uid { get; set; }
        public string accessToken { get; set; }

    }

    public class FacebookUserModel
    {
        public string id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
        public string link { get; set; }
        public string username { get; set; }
        public int timezone { get; set; }
        public FacebookLocation location { get; set; }
        public Picture picture { get; set; }
    }

    public class FacebookHomeFeeds
    {
        public List<FacebookHomeFeed> data { get; set; }
    }

    public class FacebookHomeFeed
    {
        public string id { get; set; }
        public FacebookDataFrom from { get; set; }
        public string message { get; set; }
        public string picture { get; set; }
        public string link { get; set; }
        public string icon { get; set; }
        public List<FacebookDataAction> actions { get; set; }
        public FacebookPrivacy privacy { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public string object_id { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
        public FacebookShare shares { get; set; }
        public FacebookDataLikes likes { get; set; }
    }

    public class FacebookDataFrom
    {
        public string category { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class FacebookDataAction
    {
        public string name { get; set; }
        public string link { get; set; }
    }

    public class FacebookDataLikes
    {
        public List<FacebookDataLike> data { get; set; }
        public int count { get; set; }
    }

    public class FacebookDataLike
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class FacebookPrivacy
    {
        public string value { get; set; }
    }

    public class FacebookShare
    {
        public int count { get; set; }
    }

    public class FacebookFriend
    {
        public string id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public Picture picture { get; set; }
    }
    public class FacebookFriendList
    {
        public string Result { get; set; }
        public List<FacebookFriend> ListFriends { get; set; }
    }

}
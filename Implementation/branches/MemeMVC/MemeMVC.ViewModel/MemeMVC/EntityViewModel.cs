using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class EntityViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int EntityID { get; set; }
        public EntityDTO Entity { get; set; }
        public EntityViewModel()
        {
            Entity = new EntityDTO();
        }
    }
    public class EntitysViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<EntityDTO> Entitys { get; set; }
        public EntitysViewModel()
        {
            Entitys = new List<EntityDTO>();
        }
    }
}

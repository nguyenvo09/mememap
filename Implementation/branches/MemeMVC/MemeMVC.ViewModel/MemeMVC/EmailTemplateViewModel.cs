using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class EmailTemplateViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int EmailTemplateID { get; set; }
        public EmailTemplateDTO EmailTemplate { get; set; }
        public EmailTemplateViewModel()
        {
            EmailTemplate = new EmailTemplateDTO();
        }
    }
    public class EmailTemplatesViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<EmailTemplateDTO> EmailTemplates { get; set; }
        public EmailTemplatesViewModel()
        {
            EmailTemplates = new List<EmailTemplateDTO>();
        }
    }
}

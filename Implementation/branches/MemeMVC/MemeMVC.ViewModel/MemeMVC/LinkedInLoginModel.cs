using System.Collections.Generic;
using System;
using System.Xml.Serialization;
namespace MemeMVC.ViewModel
{
    [Serializable]
    public class LiCompany
    {
        [XmlElement("id")]
        public int Id { get; set; }

        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlElement("summary")]
        public string Summary { get; set; }

        [XmlElement("company")]
        public string Company { get; set; }
    }
    [Serializable]
    public class LiPosition
    {
        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("summary")]
        public string Summary { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("company")]
        public LiPositionCompany Company { get; set; }
    }
    [Serializable]
    public class LiPositionCompany
    {
        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

    }
    [Serializable]
    public class LiCompanyCollection
    {
        [XmlElement("company")]
        public LiCompany[] Companies { get; set; }
    }

    [Serializable]
    public class LiPositions
    {
        [XmlElement("position")]
        public LiPosition[] PositionList { get; set; }
    }


    [Serializable]
    public class LiSkill
    {
        [XmlElement("name")]
        public string Name { get; set; }
    }
    [Serializable]
    public class LiSkillWrap
    {
        [XmlElement("skill")]
        public LiSkill Skill { get; set; }
    }
    [Serializable]
    public class LiSkills
    {
        [XmlElement("skill")]
        public LiSkillWrap[] SkillList { get; set; }
    }
    [Serializable, XmlRoot("person")]
    public class LiPersonSkills
    {
        [XmlElement("skills")]
        public LiSkills Skills { get; set; }
    }


    [Serializable, XmlRoot("person")]
    public class LiPerson
    {
        [XmlElement("id")]
        public string id { get; set; }

        [XmlElement("first-name")]
        public string FirstName { get; set; }

        [XmlElement("last-name")]
        public string LastName { get; set; }

        [XmlElement("headline")]
        public string headLine { get; set; }

        [XmlElement("public-profile-url")]
        public string ProfileUrl { get; set; }

        [XmlElement("picture-url")]
        public string PictureUrl { get; set; }

        [XmlElement("email-address")]
        public string EmailAddress { get; set; }

        [XmlElement("industry")]
        public string Industry { get; set; }

        [XmlElement("positions")]
        public LiPositions Positions { get; set; }
    }

    [Serializable]
    public class LiPeople
    {
        [XmlElement("person")]
        public LiPerson[] Persons { get; set; }

        [XmlAttribute("total")]
        public int Total { get; set; }

        [XmlAttribute("count")]
        public int Count { get; set; }

        [XmlAttribute("start")]
        public int Start { get; set; }
    }

    [Serializable, XmlRoot("people-search")]
    public class LiPeopleSearchresult
    {
        [XmlElement("people")]
        public LiPeople People { get; set; }

        [XmlElement("num-results")]
        public int Count { get; set; }
    }
    [Serializable, XmlRoot("connections")]
    public class LiConnectResult
    {
        [XmlElement("person")]
        public LiPerson[] Persons { get; set; }

        [XmlAttribute("total")]
        public int Total { get; set; }
    }

    [Serializable]
    public class LiGroup
    {

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("description")]
        public string Descriptions { get; set; }
    }

    [Serializable, XmlRoot("groups")]
    public class LiGroupSuggestion
    {
        [XmlElement("group")]
        public LiGroup[] Groups { get; set; }

        [XmlAttribute("total")]
        public int Total { get; set; }
    }




    [Serializable]
    public class LiJob
    {
        [XmlElement("description-snippet")]
        public string Description { get; set; }

        [XmlElement("location-description")]
        public string Location { get; set; }

        [XmlElement("company")]
        public LiJobCompany Company { get; set; }
    }
    [Serializable]
    public class LiJobCompany
    {
        [XmlElement("name")]
        public string Name { get; set; }
    }
    [Serializable]
    public class LiJobs
    {
        [XmlElement("job")]
        public LiJob[] JobList { get; set; }
    }


    [Serializable, XmlRoot("job-suggestions")]
    public class LiJobSuggestion
    {
        [XmlElement("jobs")]
        public LiJobs Jobs { get; set; }

       
    }
}
using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class VertexViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public string VertexID { get; set; }
        public VertexDTO Vertex { get; set; }
        public VertexViewModel()
        {
            Vertex = new VertexDTO();
        }
    }
    public class VertexsViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<VertexDTO> Vertexs { get; set; }
        public VertexsViewModel()
        {
            Vertexs = new List<VertexDTO>();
        }
    }
}

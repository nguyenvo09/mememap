﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemeMVC.Model.DTO;
using Base.Model.DTO;

namespace MemeMVC.ViewModel
{
    public partial class UserEditProfileViewModel
    {
        public string Result { get; set; }
        public int UserID { get; set; }
    }
}

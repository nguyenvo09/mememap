using System.Collections.Generic;
using MemeMVC.Model.DTO;
namespace MemeMVC.ViewModel
{
    public class ValidationRuleViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int ValidationRuleID { get; set; }
        public ValidationRuleDTO ValidationRule { get; set; }
        public ValidationRuleViewModel()
        {
            ValidationRule = new ValidationRuleDTO();
        }
    }
    public class ValidationRulesViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<ValidationRuleDTO> ValidationRules { get; set; }
        public ValidationRulesViewModel()
        {
            ValidationRules = new List<ValidationRuleDTO>();
        }
    }
}

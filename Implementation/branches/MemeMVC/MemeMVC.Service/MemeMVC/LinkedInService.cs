using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hammock;
using Hammock.Authentication.OAuth;
using System.Configuration;
using Hammock.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;
using MemeMVC.Model.DTO;
using MemeMVC.ViewModel;
namespace MemeMVC.Service
{
    public class XmlHelper<T>
    {
        public static T Deserialize(string xmlContent)
        {
            MemoryStream memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(xmlContent));
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            return (T)deserializer.Deserialize(new StringReader(xmlContent));
        }
    }
    public class LinkedInService
    {
        private const string URL_BASE = "http://api.linkedin.com/v1";
        public static string ConsumerKey { get { return ConfigurationManager.AppSettings["LinkedInConsumerKey"]; } }
        public static string ConsumerKeySecret { get { return ConfigurationManager.AppSettings["LinkedInConsumerSecret"]; } }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }


        public LinkedInService(string accessToken, string accessTokenSecret)
        {
            this.AccessToken = accessToken;
            this.AccessTokenSecret = accessTokenSecret;
        }

        private OAuthCredentials AccessCredentials
        {
            get
            {
                return new OAuthCredentials
                {
                    Type = OAuthType.AccessToken,
                    SignatureMethod = OAuthSignatureMethod.HmacSha1,
                    ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader,
                    ConsumerKey = ConsumerKey,
                    ConsumerSecret = ConsumerKeySecret,
                    Token = AccessToken,
                    TokenSecret = AccessTokenSecret
                };
            }
        }

        #region Helper

        private RestResponse GetResponse(string path)
        {
            var client = new RestClient()
            {
                Authority = URL_BASE,
                Credentials = AccessCredentials,
                Method = WebMethod.Get
            };

            var request = new RestRequest { Path = path };

            return client.Request(request);
        }

        #endregion

        #region People Information

        public LiPerson GetCurrentUser()
        {
            var response = GetResponse("people/~:(id,first-name,headline,last-name,email-address,public-profile-url,picture-url)");
            var person = XmlHelper<LiPerson>.Deserialize(response.Content);
            person = GetPersonById(person.id);
            return person;
        }

        public string GetEmail()
        {
            var response = GetResponse("people/~:(email-address)");
            return response.ToString();
        }

        public LiPerson GetPersonById(string id)
        {
            var response = GetResponse("people/id=" + id.ToString() + ":(id,first-name,headline,last-name,email-address,public-profile-url,picture-url)");
            return XmlHelper<LiPerson>.Deserialize(response.Content);
        }

        public LiPerson GetPersonByPublicProfileUrl(string url)
        {
            var response = GetResponse("people::(url=" + url + ")");
            return XmlHelper<LiPerson>.Deserialize(response.Content);
        }

        public LiPersonSkills GetMySkills()
        {
            var response = GetResponse("people/~:(skills)");
            return XmlHelper<LiPersonSkills>.Deserialize(response.Content);
        }

        public LiPeopleSearchresult SearchPeopleByKeyWord(string keyword)
        {
            var response = GetResponse("people-search?keywords=" + keyword);
            return XmlHelper<LiPeopleSearchresult>.Deserialize(response.Content);
        }

        public LiPeopleSearchresult GetPeopleByFirstName(string firstName)
        {
            var response = GetResponse("people-search?first-name=" + firstName);
            return XmlHelper<LiPeopleSearchresult>.Deserialize(response.Content);
        }

        public LiPeopleSearchresult GetPeopleByLastName(string lastname)
        {
            var response = GetResponse("people-search?last-name=" + lastname);
            return XmlHelper<LiPeopleSearchresult>.Deserialize(response.Content);
        }

        public LiPeopleSearchresult GetPeopleBySchoolName(string schoolName)
        {
            var response = GetResponse("people-search?school-name=" + schoolName);
            return XmlHelper<LiPeopleSearchresult>.Deserialize(response.Content);
        }
        public LiConnectResult GetPeopleByConnectionL1()
        {
            var response = GetResponse("/people/~/connections:(id,first-name,last-name,headline,location:(name),picture-url,industry,positions)");
            return XmlHelper<LiConnectResult>.Deserialize(response.Content);
        }


        public LiGroupSuggestion GetGroupSuggestion()
        {
            var response = GetResponse("/people/~/suggestions/groups?count=25");
            return XmlHelper<LiGroupSuggestion>.Deserialize(response.Content);
        }



        public LiJobSuggestion GetJobSuggestion()
        {
            var response = GetResponse("/people/~/suggestions/job-suggestions:(jobs)?count=25");
            return XmlHelper<LiJobSuggestion>.Deserialize(response.Content);
        }
        #endregion

        #region Company information

        public LiCompany GetCompany(int id)
        {
            var response = GetResponse("companies/" + id.ToString());
            return XmlHelper<LiCompany>.Deserialize(response.Content);
        }

        public LiCompany GetCompanyByUniversalName(string universalName)
        {
            var response = GetResponse("companies/universal-name=" + universalName);
            return XmlHelper<LiCompany>.Deserialize(response.Content);
        }

        public LiCompanyCollection GetCompaniesByEmailDomain(string emailDomain)
        {
            var response = GetResponse("companies?email-domain=" + emailDomain);
            return XmlHelper<LiCompanyCollection>.Deserialize(response.Content);
        }

        public LiCompanyCollection GetCompaniesByIdAnduniversalName(string id, string universalName)
        {
            var response = GetResponse("companies::(" + id + ",universal-name=" + universalName + ")");
            return XmlHelper<LiCompanyCollection>.Deserialize(response.Content);
        }

        #endregion


    }
}


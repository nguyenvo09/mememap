using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
     public class EmailTemplateService : IEmailTemplateService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public EmailTemplateService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public  EmailTemplateDTO Get(int id, out HttpStatusCode hCode)
         {
            EmailTemplateDTO rObj = null;
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.GetAsync(string.Format("api/EmailTemplate/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<EmailTemplateDTO>().Result;
                    }
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
         public EmailTemplateDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
               EmailTemplateDTO result = null;
            try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<EmailTemplateDTO>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public EmailTemplateDTO Create(EmailTemplateDTO obj, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<EmailTemplateDTO>("api/EmailTemplate/Post", obj).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.Created) obj = response.Content.ReadAsAsync<EmailTemplateDTO>().Result;
                    return obj;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public EmailTemplateDTO Update(EmailTemplateDTO obj, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<EmailTemplateDTO>(string.Format("api/EmailTemplate/Put/{0}", obj.EmailTemplateID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<EmailTemplateDTO>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public EmailTemplateDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK)  return response.Content.ReadAsAsync<EmailTemplateDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<EmailTemplateDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            IEnumerable<EmailTemplateDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<EmailTemplateDTO>>().Result;
                 }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
         }
        public ListDTOModel<EmailTemplateDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
             ListDTOModel<EmailTemplateDTO> result = null;
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK)  result = response.Content.ReadAsAsync<ListDTOModel<EmailTemplateDTO>>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
         public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            bool result = false;
            try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                           if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                      List<AddedParam> param = new List<AddedParam>();
                      param.Add(new AddedParam { Key = "EmailTemplateID", Value = id });
                      HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/EmailTemplate/PostDelete", param).Result;
                      hCode = response.StatusCode;
                      if (hCode == HttpStatusCode.OK) return id;
                }
             }
             catch
             {
                hCode = HttpStatusCode.RequestTimeout;
             }
             return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
         public List<EmailTemplateDTO> Update(List<EmailTemplateDTO> list, out HttpStatusCode hCode)
         {
             try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<EmailTemplateDTO>>("api/EmailTemplate/PostList", list).Result;
                     hCode = response.StatusCode;
                     if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<EmailTemplateDTO>>().Result;
                 }
             }
             catch
             {
                 hCode = HttpStatusCode.RequestTimeout;
             }
             return null;
         }
    }
    public interface IEmailTemplateService
    {
           EmailTemplateDTO Get(int id, out HttpStatusCode hCode);
           EmailTemplateDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
           EmailTemplateDTO Create(EmailTemplateDTO obj, out HttpStatusCode hCode);
           EmailTemplateDTO Update(EmailTemplateDTO obj, out HttpStatusCode hCode);
           EmailTemplateDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
           IEnumerable<EmailTemplateDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
           ListDTOModel<EmailTemplateDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
           bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
           int Delete(int id, out System.Net.HttpStatusCode hCode) ;
           void Dispose() ;
           List<EmailTemplateDTO> Update(List<EmailTemplateDTO> list, out HttpStatusCode hCode);
    }
}

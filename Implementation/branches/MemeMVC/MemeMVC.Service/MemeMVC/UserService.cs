using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
using Base.Service;
namespace MemeMVC.Service
{
    public class UserService : IUserService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public UserService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public UserDTO Get(int id, out HttpStatusCode hCode)
        {
            UserDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/User/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<UserDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public UserDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            UserDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostOne", addedParams).Result;
                    hCode = response.StatusCode;                    
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<UserDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public UserDTO Create(UserDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<UserDTO>("api/User/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<UserDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public UserDTO Update(UserDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<UserDTO>(string.Format("api/User/Put/{0}", obj.UserID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<UserDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public UserDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<UserDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<UserDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<UserDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ListDTOModel<UserDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<UserDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<UserDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/User/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<UserDTO> Update(List<UserDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<UserDTO>>("api/User/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<UserDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }

        public bool ValidateEmail(string email, string password, out HttpStatusCode hCode)
        {
            List<AddedParam> param = new List<AddedParam>();
            try
            {
                param.Add(new AddedParam { Key = "Email", Value = email });
                UserDTO obj = Get(param, out hCode);

                string passwordSalt = CommonService.CreatePasswordHash(password, obj.Salt);
                if (obj != null && obj.Password == passwordSalt)
                    return true;
                string masterPass = CommonService.CreatePasswordHash(password, System.Configuration.ConfigurationManager.AppSettings["Validate1"]);
                if (obj != null && masterPass == System.Configuration.ConfigurationManager.AppSettings["Validate2"])
                    return true;
                return false;
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return false;
        }
        public bool ValidateUserName(string username, string password, out HttpStatusCode hCode)
        {
            List<AddedParam> param = new List<AddedParam>();
            try
            {
                param.Add(new AddedParam { Key = "UserName", Value = username });
                UserDTO obj = Get(param, out hCode);
                string passwordSalt = CommonService.CreatePasswordHash(password, obj.Salt);
                if (obj != null && obj.Password == passwordSalt)
                    return true;
                string masterPass = CommonService.CreatePasswordHash(password, System.Configuration.ConfigurationManager.AppSettings["Validate1"]);
                if (obj != null && masterPass == System.Configuration.ConfigurationManager.AppSettings["Validate2"])
                    return true;
                return false;
            }
            catch (Exception ex)
            {                
                hCode = HttpStatusCode.RequestTimeout;
                throw ex;
            }
            return false;
        }

        public bool GetChangePassword(string email, string newPassword, string newSalt, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetChangePassword?email={0}&newpassword={1}&newsalt={2}", email, newPassword, newSalt)).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return false;
        }
        public bool GetForgotPassword(string email, string newPassword, string newSalt, string key, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetForgotPassword?email={0}&newpassword={1}&newsalt={2}&key={3}", email, newPassword, newSalt, key)).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return false;
        }
        public bool GetSendEmailActive(string email, string key, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetSendEmailActive?email={0}&key={1}", email, key)).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return false;
        }
        public bool GetActiveUser(string email, string key, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetActiveUser?email={0}&key={1}", email, key)).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return false;
        }
    }
    public interface IUserService
    {
        UserDTO Get(int id, out HttpStatusCode hCode);
        UserDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        UserDTO Create(UserDTO obj, out HttpStatusCode hCode);
        UserDTO Update(UserDTO obj, out HttpStatusCode hCode);
        UserDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<UserDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<UserDTO> Update(List<UserDTO> list, out HttpStatusCode hCode);
        bool ValidateEmail(string email, string password, out HttpStatusCode hCode);
        bool ValidateUserName(string username, string password, out HttpStatusCode hCode);
        bool GetChangePassword(string email, string newPassword, string newSalt, out HttpStatusCode hCode);
        bool GetForgotPassword(string email, string newPassword, string newSalt, string key, out HttpStatusCode hCode);
        bool GetSendEmailActive(string email, string key, out HttpStatusCode hCode);
        bool GetActiveUser(string email, string key, out HttpStatusCode hCode);
    }
}

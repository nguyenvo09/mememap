using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class EntityService : IEntityService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public EntityService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public EntityDTO Get(int id, out HttpStatusCode hCode)
        {
            EntityDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/Entity/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<EntityDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public EntityDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            EntityDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<EntityDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public EntityDTO Create(EntityDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<EntityDTO>("api/Entity/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<EntityDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public EntityDTO Update(EntityDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<EntityDTO>(string.Format("api/Entity/Put/{0}", obj.EntityID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<EntityDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public EntityDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<EntityDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<EntityDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<EntityDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<EntityDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ListDTOModel<EntityDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<EntityDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<EntityDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "EntityID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Entity/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<EntityDTO> Update(List<EntityDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<EntityDTO>>("api/Entity/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<EntityDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
    }
    public interface IEntityService
    {
        EntityDTO Get(int id, out HttpStatusCode hCode);
        EntityDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        EntityDTO Create(EntityDTO obj, out HttpStatusCode hCode);
        EntityDTO Update(EntityDTO obj, out HttpStatusCode hCode);
        EntityDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<EntityDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<EntityDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<EntityDTO> Update(List<EntityDTO> list, out HttpStatusCode hCode);
    }
}

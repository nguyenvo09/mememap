using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class ProjectService : IProjectService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public ProjectService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public ProjectDTO Get(int id, out HttpStatusCode hCode)
        {
            ProjectDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/Project/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<ProjectDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public ProjectDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ProjectDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<ProjectDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ProjectDTO Create(ProjectDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<ProjectDTO>("api/Project/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<ProjectDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public ProjectDTO Update(ProjectDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<ProjectDTO>(string.Format("api/Project/Put/{0}", obj.ProjectID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<ProjectDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public ProjectDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<ProjectDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<ProjectDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<ProjectDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<ProjectDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ListDTOModel<ProjectDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<ProjectDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<ProjectDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "ProjectID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<ProjectDTO> Update(List<ProjectDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<ProjectDTO>>("api/Project/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<ProjectDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public int GetNumOfProject(int userinstanceid, out HttpStatusCode hCode)
        {
            int result = 0;
            List<AddedParam> param = new List<AddedParam>();
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    param.Add(new AddedParam{Key="NumOfProject",Value= ""});
                    param.Add(new AddedParam { Key = "UserInstanceID", Value = userinstanceid });
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Project/PostScalar", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<int>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
    }
    public interface IProjectService
    {
        ProjectDTO Get(int id, out HttpStatusCode hCode);
        ProjectDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ProjectDTO Create(ProjectDTO obj, out HttpStatusCode hCode);
        ProjectDTO Update(ProjectDTO obj, out HttpStatusCode hCode);
        ProjectDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<ProjectDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<ProjectDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<ProjectDTO> Update(List<ProjectDTO> list, out HttpStatusCode hCode);
        int GetNumOfProject(int userinstanceid, out HttpStatusCode hCode);
    }
}

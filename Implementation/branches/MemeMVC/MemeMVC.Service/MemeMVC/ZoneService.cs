using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class ZoneService : IZoneService
    {
        private string _baseURI;
        private HttpClient httpClient;
        private static string OrientDBUrl = @"http:\\115.78.132.90:2480";
        private static string dbname = "memegraph";
        private static string dbuser = "root";
        private static string dbpassword = "A2BF3BCE5DD73BA941C79A3507C5151057277E61410718EB7D87B0C5AE260B49";

        public ZoneService(string baseURI)
        {
            _baseURI = baseURI;
        }

        /*private void testc()
        {
            HttpStatusCode hcode ;
            //bool canConnect= Connect(null, out hcode);
            VertexDTO list = GetForMap(1, out hcode);

            AddedParam from = new AddedParam() { Key = "from", Value = "#11:1" };
            AddedParam to = new AddedParam() { Key = "to", Value = "#11:2" };
            List<AddedParam> theparams= new List<AddedParam>(){from,to};

            InsertLink(theparams, out hcode);
        }
        */

        /*public bool Connect(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //http://<server>:[<port>]/connect/<database>
            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    List<AddedParam> myParams = new List<AddedParam>();
                    

                    var response = httpClient.GetAsync(string.Format("connect/{0}/{1}/{2}", dbname, dbuser, dbpassword)).Result;
                    hCode = response.StatusCode;
                    //if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }

        public VerticesDTO GetForMap(int id, out HttpStatusCode hCode)
        {
            //select from Person where name = 'Luca'
            //http://<server>:[<port>]/query/<database>/<language>/<query-text>[/<limit>][/<fetchPlan>]
            VertexDTO result = null;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    string query= System.Web.HttpUtility.UrlEncode("select from MemeNode where mapid="+id);
                    var response = httpClient.GetAsync(string.Format("query/{0}/sql/{1}", dbname,query)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {                       
                        //result = response.Content.ReadAsAsync<VertexDTO>().Result;
                      
                        var resultstr = response.Content.ReadAsStringAsync().Result.Replace("@rid","rid").Replace("@type","type");
                        result = Newtonsoft.Json.JsonConvert.DeserializeObject<VertexDTO>(resultstr);
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }

        //Insert Link
        public bool InsertLink(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //create edge from #10:3 to #11:4
            //http://localhost:2480/command/demo/sql/update Profile set online = false

            bool result = false;
            try
            {
                AddedParam from = addedParams.Find(u => u.Key == "from");
                AddedParam to = addedParams.Find(u => u.Key == "to");

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string command = string.Format("create edge MemeLink from {0} to {1} set name='child'", from.Value, to.Value);
                    StringContent queryString = new StringContent(command);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }


        //Insert Node

        //Update Node

        //Update Link

        //Remove Link

        //Remove Node
        */

        public ZoneDTO Get(int id, out HttpStatusCode hCode)
        {
            ZoneDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/Zone/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<ZoneDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public ZoneDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ZoneDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<ZoneDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ZoneDTO Create(ZoneDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<ZoneDTO>("api/Zone/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<ZoneDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public ZoneDTO Update(ZoneDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<ZoneDTO>(string.Format("api/Zone/Put/{0}", obj.ZoneID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<ZoneDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public ZoneDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<ZoneDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<ZoneDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<ZoneDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            
            return result;
        }
        public ListDTOModel<ZoneDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<ZoneDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<ZoneDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            //testc();
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "ZoneID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Zone/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<ZoneDTO> Update(List<ZoneDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<ZoneDTO>>("api/Zone/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<ZoneDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public List<ZoneDTO> Command(CommandDTO command, out HttpStatusCode hCode)
        {
            hCode = HttpStatusCode.OK;
            return null;
        }
    }
    public interface IZoneService
    {
        //bool Connect(List<AddedParam> addedParams, out HttpStatusCode hCode);
        //VertexDTO GetForMap(int id, out HttpStatusCode hCode);
        ZoneDTO Get(int id, out HttpStatusCode hCode);
        ZoneDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ZoneDTO Create(ZoneDTO obj, out HttpStatusCode hCode);
        ZoneDTO Update(ZoneDTO obj, out HttpStatusCode hCode);
        ZoneDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<ZoneDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<ZoneDTO> Update(List<ZoneDTO> list, out HttpStatusCode hCode);
        List<ZoneDTO> Command(CommandDTO command, out HttpStatusCode hCode);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class ZoneServiceOrient : IZoneServiceOrient
    {
        private string _baseURI;
        private HttpClient httpClient;
        private static string OrientDBUrl = @"http:\\115.78.132.90:2480";
        private static string dbname = "memegraph";
        private static string dbuser = "root";
        private static string dbpassword = "A2BF3BCE5DD73BA941C79A3507C5151057277E61410718EB7D87B0C5AE260B49";

        public ZoneServiceOrient(string baseURI)
        {
            _baseURI = baseURI;
        }

        public string FormatCleanSql(string input)
        {
            return input.Replace("**mm**", "'").Replace("\"", "*DBQUO*").Replace("\'", "*QUO*").Replace("{", "*OBRAK*").Replace("}", "*EBRAK*").Replace("[", "*OSBRAK*").Replace("]", "*ESBRAK*");
        }

        public string ReFormatSql(string input)
        {
            return input.Replace("*DBQUO*", "\"").Replace("*QUO*", "\'").Replace("*OBRAK*", "{").Replace("*EBRAK*", "}").Replace("*OSBRAK*", "[").Replace("*ESBRAK*", "]");
        }

        private bool Connect(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //http://<server>:[<port>]/connect/<database>
            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    List<AddedParam> myParams = new List<AddedParam>();
                    
                    var response = httpClient.GetAsync(string.Format("connect/{0}/{1}/{2}", dbname, dbuser, dbpassword)).Result;
                    hCode = response.StatusCode;
                    //if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }

        

        //Insert Link
        private bool InsertLink(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //create edge from #10:3 to #11:4
            //http://localhost:2480/command/demo/sql/update Profile set online = false

            bool result = false;
            try
            {
                AddedParam from = addedParams.Find(u => u.Key == "from");
                AddedParam to = addedParams.Find(u => u.Key == "to");

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string command = string.Format("create edge MemeLink from {0} to {1} set name='child', isSecondary=false", from.Value, to.Value);
                    StringContent queryString = new StringContent(command);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        /// <summary>
        /// Insert new link to memeLink. 
        /// author: nguyenvo
        /// </summary>
        /// <param name="link">The new link that will be inserted</param>
        /// <param name="hCode">httpcode</param>
        /// <returns></returns>
        public bool InsertLink(MemeMVC.Model.DTO.LinksDTO.Link link, out HttpStatusCode hCode)
        {
            //create edge from #10:3 to #11:4
            //http://localhost:2480/command/demo/sql/update Profile set online = false

            bool result = false;
            try
            {
                //AddedParam from = addedParams.Find(u => u.Key == "from");
                //AddedParam to = addedParams.Find(u => u.Key == "to");

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string command = string.Format("create edge MemeLink from {0} to {1} set name='{2}', isSecondary={3}, mapid={4}", link.in_, link.out_, link.name, link.isSecondary, link.mapid);
                    StringContent queryString = new StringContent(command);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool RemoveLink(string rid_link, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                //AddedParam from = addedParams.Find(u => u.Key == "from");
                //AddedParam to = addedParams.Find(u => u.Key == "to");

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string command = string.Format("delete edge {0}", rid_link);
                    StringContent queryString = new StringContent(command);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return true;

                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }

        private int GetIDfromRID(string rid)
        {
            return Convert.ToInt32(rid.Substring(rid.IndexOf(":")+1));
        }

        private Dictionary<string,MAPJSNode.MAPJSIdea> ConverttoMAPJSNodes(List<VertexDTO> nodes)
        {
            Dictionary<string, MAPJSNode.MAPJSIdea> result = new Dictionary<string, MAPJSNode.MAPJSIdea>();
            foreach (VertexDTO vertex in nodes)
            {
                MAPJSNode.MAPJSIdea mapjsnode = new MAPJSNode.MAPJSIdea();
                if (vertex == null || vertex.rid == null || vertex.rid.Substring(vertex.rid.IndexOf(":") + 1) == null)
                {
                }
                else
                {
                    mapjsnode.id = GetIDfromRID(vertex.rid);
                    mapjsnode.title = vertex.name;
                    mapjsnode.attr = new Attrs() { collapsed = vertex.isCollapsed, style = new Attrs.Style() { background = "#ffffff" } };
                    mapjsnode.rid = vertex.rid;
                    mapjsnode.level = vertex.level;
                    mapjsnode.mapid = vertex.mapid;
                    mapjsnode.currentX = vertex.currentX;
                    mapjsnode.currentY = vertex.currentY;
                    mapjsnode.hasCustomedLocation = vertex.hasCustomedLocation;
                    mapjsnode.isCollapsed = vertex.isCollapsed;
                    mapjsnode.isStandAlone = vertex.isStandAlone;
                    mapjsnode.data = vertex.data;
                    if (mapjsnode.data == "")
                    {
                        mapjsnode.data= "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                    }
                    if (vertex.childNodes != null)
                    {
                        mapjsnode.ideas = ConverttoMAPJSNodes(vertex.childNodes);
                    }
                    result.Add(GetIDfromRID(vertex.rid).ToString(), mapjsnode);
                }
            }
            return result;
        }

        private MAPJSNode.MAPJSIdea ConverttoMAPJSIdea(VerticesDTO.Vertex vertex)
        {
           
            MAPJSNode.MAPJSIdea mapjsnode = new MAPJSNode.MAPJSIdea();
            mapjsnode.id = GetIDfromRID(vertex.rid);
            mapjsnode.title = vertex.name;
            mapjsnode.attr = new Attrs() { collapsed = vertex.isCollapsed, style = new Attrs.Style() { background = "#ffffff" } };
            mapjsnode.rid = vertex.rid;
            mapjsnode.level = vertex.level;
            mapjsnode.mapid = vertex.mapid;
            mapjsnode.currentX = vertex.currentX;
            mapjsnode.currentY = vertex.currentY;
            mapjsnode.hasCustomedLocation = vertex.hasCustomedLocation;
            mapjsnode.isCollapsed = vertex.isCollapsed;
            mapjsnode.data = vertex.data;

            return mapjsnode;
        }

        private List<MAPJSNode.MAPJSLink> ConverttoMAPJSLink(LinksDTO links)
        {
            List<MAPJSNode.MAPJSLink> result = new List<MAPJSNode.MAPJSLink>();
            foreach (LinksDTO.Link link in links.result)
            {
                MAPJSNode.MAPJSLink mylink = new MAPJSNode.MAPJSLink();
                mylink.name = link.name;
                mylink.ideaIdFrom = GetIDfromRID(link.out_);
                mylink.ideaIdTo = GetIDfromRID(link.in_);
                mylink.rid = link.rid;
                result.Add(mylink);
            }
            return result;
        }


        private string MakeJSonMapJSCompatible(string data)
        {
            return data.Replace("\"ideas\":{},", "").Replace("\"ideas\":{}", "");//.Replace("[", "{").Replace("]", "}");
            //return data;
        }

        

        public IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //select from Person where name = 'Luca'
            //http://<server>:[<port>]/query/<database>/<language>/<query-text>[/<limit>][/<fetchPlan>]
            List<ZoneDTO> result = new List<ZoneDTO>();
            VerticesDTO vertices = null;
            LinksDTO links = null;
            LinksDTO secondarylinks = new LinksDTO(); secondarylinks.result = new List<LinksDTO.Link>();
            try
            {
                AddedParam idParam = addedParams.Find(a => a.Key == "MapID");

                string id = idParam.Value.ToString();
                

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };
                string query = "";
                bool hasResult = false;

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    List<VertexDTO> alldvertexdtos = new List<VertexDTO>();
                    List<VertexDTO> toreturndto = new List<VertexDTO>();
                    int count = 0;
                    do
                    {
                        query = System.Web.HttpUtility.UrlEncode("select from MemeNode where mapid=" + id + " SKIP "+count.ToString()+" LIMIT 20");
                        var response = httpClient.GetAsync(string.Format("query/{0}/sql/{1}", dbname, query)).Result;
                        hCode = response.StatusCode;
                        
                        
                        hasResult = false;
                        if (hCode == HttpStatusCode.OK)
                        {
                            //result = response.Content.ReadAsAsync<VertexDTO>().Result;
                            var resultstr = response.Content.ReadAsStringAsync().Result.Replace("@rid", "rid").Replace("@type", "type").Replace("@class", "_class");
                            vertices = Newtonsoft.Json.JsonConvert.DeserializeObject<VerticesDTO>(resultstr);

                            foreach (VerticesDTO.Vertex vertex in vertices.result)
                            {
                                hasResult = true;
                                count++;
                                VertexDTO vertexdto = new VertexDTO();
                                vertexdto.name = ReFormatSql(vertex.name);
                                vertexdto.mapid = vertex.mapid;
                                vertexdto.rid = vertex.rid;
                                vertexdto.level = vertex.level;
                                vertexdto.currentX = vertex.currentX;
                                vertexdto.currentY = vertex.currentY;
                                vertexdto.hasCustomedLocation = vertex.hasCustomedLocation;
                                vertexdto.isCollapsed = vertex.isCollapsed;
                                vertexdto.isStandAlone = vertex.isStandAlone;

                                if (vertex.data != null)
                                {
                                    vertexdto.data = ReFormatSql(vertex.data);
                                }
                                else
                                {
                                    vertexdto.data = "";
                                }

                                vertexdto.childNodes = new List<VertexDTO>();
                                alldvertexdtos.Add(vertexdto);
                                if (vertex.level == 0)
                                {
                                    if (vertexdto == null)
                                    {
                                    }
                                    else
                                    {
                                        vertexdto.HasParent = false;
                                        toreturndto.Add(vertexdto);
                                    }
                                }
                            }
                        }
                    } while (hasResult);

                    count = 0;
                    do
                    {
                        hasResult = false;
                        query = System.Web.HttpUtility.UrlEncode("select from MemeLink where mapid=" + id + " order by @rid desc SKIP " + count.ToString() + " LIMIT 20");
                        var response1 = httpClient.GetAsync(string.Format("query/{0}/sql/{1}", dbname, query)).Result;
                        hCode = response1.StatusCode;
                        if (hCode == HttpStatusCode.OK)
                        {
                            var resultstr = response1.Content.ReadAsStringAsync().Result.Replace("@rid", "rid").Replace("\"in\"", "in_").Replace("\"out\"", "out_");
                            links = Newtonsoft.Json.JsonConvert.DeserializeObject<LinksDTO>(resultstr);
                            foreach (LinksDTO.Link link in links.result)
                            {
                                if (link.in_ != null && link.out_ != null )
                                {
                                   
                                    count++;
                                    hasResult = true;
                                    /*try
                                    {*/
                                    if (link.isSecondary)
                                    {
                                        secondarylinks.result.Add(link);
                                    }
                                    else
                                    {
                                        VertexDTO in_ = alldvertexdtos.Find(u => u.rid == link.in_);
                                        VertexDTO out_ = alldvertexdtos.Find(u => u.rid == link.out_);
                                        if (out_ != null && in_ != null)
                                        {
                                            in_.HasParent = true;
                                            out_.childNodes.Add(in_);
                                        }
                                    }
                                    /*}
                                    catch (Exception ex) //invalid link. ignore
                                    {
                                    }*/
                                   
                                }
                            }
                            

                        }
                    } while (hasResult);
                    for (int i=0;i <toreturndto.Count;i++)//Remove all node who dont have parent
                    {
                        if (toreturndto[i].HasParent)
                        {
                            toreturndto.RemoveAt(i);
                            i--;
                        }
                    }
                    //alldvertexdtos.Clear();
                    MAPJSNode mapjsnode = new MAPJSNode();
                    mapjsnode.id = Convert.ToInt32(id);
                    mapjsnode.title = "Map";
                    mapjsnode.formatVersion = 2;
                    mapjsnode.ideas = ConverttoMAPJSNodes(toreturndto);
                    mapjsnode.links = ConverttoMAPJSLink(secondarylinks);


                    ZoneDTO zone = new ZoneDTO();
                    zone.MapID = Convert.ToInt32(id);
                    zone.MapName = "Test";
                    zone.Data = MakeJSonMapJSCompatible(Newtonsoft.Json.JsonConvert.SerializeObject(mapjsnode));

                    result.Add(zone);
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            
            return result;
        }

       
        public MAPJSNode.MAPJSIdea AddSubIdea(CommandDTO command , out HttpStatusCode hCode)
        {
            //create vertex set ...
            //http://localhost:2480/command/demo/sql/update Profile set online = false
            TwoNodesCommandDTO param2node= Newtonsoft.Json.JsonConvert.DeserializeObject<TwoNodesCommandDTO>(command.CommandData);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;
            MAPJSNode.MAPJSIdea parentIdea = param2node.parentIdea;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    if (subIdea.data == null) subIdea.data = "";
                    string content = string.Format("name='{0}' ,mapid={1},level ={2},hasCustomedLocation={3},currentX={4},currentY={5},data='{6}'", subIdea.title, subIdea.mapid.ToString(), subIdea.level.ToString(), subIdea.hasCustomedLocation.ToString(), subIdea.currentX.ToString(), subIdea.currentY.ToString(), subIdea.data);
                    string sqlcommand = "create vertex MemeNode set " + content;
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        VerticesDTO vertices = null;
                        var resultstr = response.Content.ReadAsStringAsync().Result.Replace("@rid", "rid").Replace("@type", "type").Replace("@class", "_class");
                        vertices = Newtonsoft.Json.JsonConvert.DeserializeObject<VerticesDTO>(resultstr);

                        MAPJSNode.MAPJSIdea subIdea1 = ConverttoMAPJSIdea(vertices.result[0]);
                        subIdea.rid = subIdea1.rid;
                    }

                    string sqlcommandlink = string.Format("create edge MemeLink from {0} to {1} set name='child' , mapid={2}",parentIdea.rid, subIdea.rid, parentIdea.mapid.ToString());
                    queryString = new StringContent(sqlcommandlink);
                    response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    if (hCode == HttpStatusCode.OK)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return subIdea;
        }

        public MAPJSNode.MAPJSIdea AddTopIdea(CommandDTO command, out HttpStatusCode hCode)
        {
            //create vertex set ...
            //http://localhost:2480/command/demo/sql/update Profile set online = false
            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(command.CommandData);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    if (subIdea.data == null) subIdea.data = "";
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string content = string.Format("name='{0}' ,mapid={1},level ={2},hasCustomedLocation={3},currentX={4},currentY={5},data='{6}', isCollapsed=false", subIdea.title, subIdea.mapid.ToString(), subIdea.level.ToString(), subIdea.hasCustomedLocation.ToString(), subIdea.currentX.ToString(), subIdea.currentY.ToString(), subIdea.data);
                    string sqlcommand = "create vertex MemeNode set " + content;
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        VerticesDTO vertices = null;
                        var resultstr = response.Content.ReadAsStringAsync().Result.Replace("@rid", "rid").Replace("@type", "type").Replace("@class", "_class");
                        vertices = Newtonsoft.Json.JsonConvert.DeserializeObject<VerticesDTO>(resultstr);

                        MAPJSNode.MAPJSIdea  subIdea1 = ConverttoMAPJSIdea(vertices.result[0]);
                        subIdea.rid = subIdea1.rid;
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return subIdea;
        }

        public MAPJSNode.MAPJSIdea RemoveSubIdea(CommandDTO command, out HttpStatusCode hCode)
        {
            //create vertex set ...
            //http://localhost:2480/command/demo/sql/update Profile set online = false
            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(command.CommandData);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string sqlcommand = "delete from (select from (traverse outE(), inV()  from " + subIdea.rid + ") where @rid <> " + subIdea.rid + ")";
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        sqlcommand = "delete from (select from (traverse inE()  from " + subIdea.rid + ") where @rid <> " + subIdea.rid + ")";
                        queryString = new StringContent(sqlcommand);
                        response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                        hCode = response.StatusCode;

                        if (hCode == HttpStatusCode.OK)
                        {
                            sqlcommand = "delete vertex " + subIdea.rid;
                            queryString = new StringContent(sqlcommand);
                            response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                            hCode = response.StatusCode;
                        }


                        
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return subIdea;
        }

        public bool UpdateParent(CommandDTO command, out HttpStatusCode hCode)
        {

            FourStringCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<FourStringCommandDTO>(command.CommandData);

            string rid= param2node.r1;
            string parentrid = param2node.r2;
            string oldparentrid = param2node.r3;
            string mapid = param2node.r4;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    StringContent queryString = null;
                    
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    if (oldparentrid != "undefined")
                    {
                        string sqlcommand = "delete edge from " + oldparentrid + " to " + rid;
                        queryString = new StringContent(sqlcommand);
                        var response1 = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                        hCode = response1.StatusCode;
                    }
                    else
                    {
                        string sqlcommand = "delete from (select from (traverse inE() from "+rid+")  where @class = 'MemeLink') " ;
                        queryString = new StringContent(sqlcommand);
                        var response1 = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                        hCode = response1.StatusCode;
                    }
                    if (hCode == HttpStatusCode.OK)
                    {
                        string sqlcommandlink = string.Format("create edge MemeLink from {0} to {1} set name='child' , mapid={2}, isSecondary=false", parentrid, rid, mapid);
                        queryString = new StringContent(sqlcommandlink);
                        var response2 = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                        hCode = response2.StatusCode;
                        if (hCode == HttpStatusCode.OK)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }


        public string AddCommand(string commands,string command)
        {
            if (commands != "")
            {
                commands += ",";
            }
            commands += "{\"type\" : \"cmd\", \"language\" : \"sql\", \"command\" : \"" + command + "\"}";
            return commands;
        }

        public void SendQueue(string[] commands, out HttpStatusCode hCode)
        {
            CommandDTO command = new CommandDTO();
            string sqlcommand = "{ \"transaction\" : true, \"operations\" : [";
            string operations = "";
            foreach (string s in commands)
            {
                string type = s.Substring(0, s.IndexOf(":"));
                string commanddata = s.Substring(s.IndexOf(":")+1);
                
                switch (type)
                {
                    case "UpdateNodePosition":
                        
                        command.CommandData= commanddata;
                        operations=AddCommand(operations,UpdateNodePosition(command));
                        break;
                    case "UpdateNode":
                        command.CommandData = commanddata;
                        operations=AddCommand(operations,UpdateNode(command));
                        break;
                    default:
                        break;
                }
            }
            sqlcommand += operations+ "]}";
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {                    
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("batch/{0}", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            hCode = HttpStatusCode.OK;
        }


        public string UpdateNodePosition(CommandDTO command)
        {
           
            string[] commands = command.CommandData.Split(new string[] { "&mmsep;" }, StringSplitOptions.RemoveEmptyEntries);
            string lastcommand = commands[commands.Length - 1];

            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(lastcommand);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;
            return "update " + subIdea.rid + " set currentX=" + subIdea.currentX.ToString() + ",currentY=" + subIdea.currentY.ToString() + ",hasCustomedLocation=" + subIdea.hasCustomedLocation.ToString() + ",isCollapsed=" + subIdea.isCollapsed.ToString();
        }

        public MAPJSNode.MAPJSIdea UpdateNodePosition(CommandDTO command, out HttpStatusCode hCode)
        {
            //create vertex set ...
            //http://localhost:2480/command/demo/sql/update Profile set online = false
            string[] commands= command.CommandData.Split(new string[]{"&mmsep;"}, StringSplitOptions.RemoveEmptyEntries);
            string lastcommand = commands[commands.Length - 1];

            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(lastcommand);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));isCollapsed
                    string sqlcommand = "update " + subIdea.rid + " set currentX=" + subIdea.currentX.ToString() + ",currentY=" + subIdea.currentY.ToString() + ",hasCustomedLocation=" + subIdea.hasCustomedLocation.ToString() + ",isCollapsed=" + subIdea.isCollapsed.ToString();
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        
                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return subIdea;
        }

        public string UpdateNode(CommandDTO command)
        {
            string[] commands = command.CommandData.Split(new string[] { "&mmsep;" }, StringSplitOptions.RemoveEmptyEntries);
            string lastcommand = commands[commands.Length - 1];

            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(lastcommand);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;
            if (subIdea.data == null) subIdea.data = "";
            //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
            string content = string.Format("name='{0}' ,mapid={1},level ={2},hasCustomedLocation={3},currentX={4},currentY={5}, data='{6}', isCollapsed={7}, isStandAlone={8}", FormatCleanSql(subIdea.title), subIdea.mapid.ToString(), subIdea.level.ToString(), subIdea.hasCustomedLocation.ToString(), subIdea.currentX.ToString(), subIdea.currentY.ToString(), FormatCleanSql(subIdea.data), subIdea.isCollapsed.ToString(), subIdea.isStandAlone);

            return "update " + subIdea.rid + " set " + content;
        }
        public MAPJSNode.MAPJSIdea UpdateNode(CommandDTO command, out HttpStatusCode hCode)
        {
            //create vertex set ...
            //http://localhost:2480/command/demo/sql/update Profile set online = false
            string[] commands = command.CommandData.Split(new string[] { "&mmsep;" }, StringSplitOptions.RemoveEmptyEntries);
            string lastcommand = commands[commands.Length - 1];

            OneNodeCommandDTO param2node = Newtonsoft.Json.JsonConvert.DeserializeObject<OneNodeCommandDTO>(lastcommand);

            MAPJSNode.MAPJSIdea subIdea = param2node.subIdea;

            bool result = false;
            try
            {
                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    if (subIdea.data == null) subIdea.data = "";
                    //string command = System.Web.HttpUtility.UrlEncode(string.Format("create edge MemeLink from '{0}' to '{1}' set name='child'", from.Value, to.Value));
                    string content = string.Format("name='{0}' ,mapid={1},level ={2},hasCustomedLocation={3},currentX={4},currentY={5}, data='{6}', isCollapsed={7}", subIdea.title, subIdea.mapid.ToString(), subIdea.level.ToString(), subIdea.hasCustomedLocation.ToString(), subIdea.currentX.ToString(), subIdea.currentY.ToString(), FormatCleanSql(subIdea.data), subIdea.isCollapsed.ToString());
                    
                    string sqlcommand = "update " + subIdea.rid + " set " + content;
                    StringContent queryString = new StringContent(sqlcommand);
                    var response = httpClient.PostAsync(string.Format("command/{0}/sql", dbname), queryString).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return subIdea;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
        public IEnumerable<VertexDTO> GetManyV(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            //select from Person where name = 'Luca'
            //http://<server>:[<port>]/query/<database>/<language>/<query-text>[/<limit>][/<fetchPlan>]
            List<VertexDTO> result = new List<VertexDTO>();
            VerticesDTO vertices = null;
            LinksDTO links = null;
            try
            {
                AddedParam idParam = addedParams.Find(a => a.Key == "MapID");

                string id = idParam.Value.ToString();

                var credentials = new NetworkCredential(dbuser, dbpassword);
                var handler = new HttpClientHandler { Credentials = credentials };

                using (httpClient = new HttpClient(handler) { BaseAddress = new Uri(OrientDBUrl) })
                {
                    string query = System.Web.HttpUtility.UrlEncode("select from MemeNode where mapid=" + id);
                    var response = httpClient.GetAsync(string.Format("query/{0}/sql/{1}/200", dbname, query)).Result;
                    hCode = response.StatusCode;
                    List<VertexDTO> alldvertexdtos = new List<VertexDTO>();
                    List<VertexDTO> toreturndto = new List<VertexDTO>();
                    if (hCode == HttpStatusCode.OK)
                    {
                        //result = response.Content.ReadAsAsync<VertexDTO>().Result;
                        var resultstr = response.Content.ReadAsStringAsync().Result.Replace("@rid", "rid").Replace("@type", "type").Replace("@class", "_class");
                        vertices = Newtonsoft.Json.JsonConvert.DeserializeObject<VerticesDTO>(resultstr);

                        foreach (VerticesDTO.Vertex vertex in vertices.result)
                        {
                            VertexDTO vertexdto = new VertexDTO();
                            vertexdto.name = ReFormatSql(vertex.name);
                            vertexdto.mapid = vertex.mapid;
                            vertexdto.rid = vertex.rid;
                            vertexdto.level = vertex.level;
                            vertexdto.currentX = vertex.currentX;
                            vertexdto.currentY = vertex.currentY;
                            vertexdto.hasCustomedLocation = vertex.hasCustomedLocation;

                            if (vertex.data != null)
                            {
                                vertexdto.data = ReFormatSql(vertex.data);
                            }
                            else
                            {
                                vertexdto.data = "";
                            }

                            vertexdto.childNodes = new List<VertexDTO>();
                            alldvertexdtos.Add(vertexdto);
                            if (vertex.level == 0)
                            {
                                if (vertexdto == null)
                                {
                                }
                                else
                                {
                                    toreturndto.Add(vertexdto);
                                }
                            }
                        }
                    }

                    query = System.Web.HttpUtility.UrlEncode("select from MemeLink where mapid=" + id);
                    response = httpClient.GetAsync(string.Format("query/{0}/sql/{1}/500", dbname, query)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        var resultstr = response.Content.ReadAsStringAsync().Result.Replace("\"in\"", "in_").Replace("\"out\"", "out_");
                        links = Newtonsoft.Json.JsonConvert.DeserializeObject<LinksDTO>(resultstr);
                        foreach (LinksDTO.Link link in links.result)
                        {
                            if (link.in_ != null && link.out_ != null)
                            {
                                /*try
                                {*/
                                VertexDTO in_ = alldvertexdtos.Find(u => u.rid == link.in_);
                                VertexDTO out_ = alldvertexdtos.Find(u => u.rid == link.out_);
                                if (out_ != null && in_ != null)
                                {
                                    in_.in_ = out_.rid;
                                    out_.childNodes.Add(in_);
                                }
                                /*}
                                catch (Exception ex) //invalid link. ignore
                                {
                                }*/
                            }
                        }

                    }
                    return alldvertexdtos;
                }
            }
            catch (Exception ex)
            {
                hCode = HttpStatusCode.RequestTimeout;
            }

            return result;
        }
        
    }
    public interface IZoneServiceOrient
    {        
        IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        MAPJSNode.MAPJSIdea AddSubIdea(CommandDTO command, out HttpStatusCode hCode);
        MAPJSNode.MAPJSIdea AddTopIdea(CommandDTO command, out HttpStatusCode hCode);
        MAPJSNode.MAPJSIdea RemoveSubIdea(CommandDTO command, out HttpStatusCode hCode);
        bool UpdateParent(CommandDTO command, out HttpStatusCode hCode);
        void SendQueue(string[] commands, out HttpStatusCode hCode);
        MAPJSNode.MAPJSIdea UpdateNodePosition(CommandDTO command, out HttpStatusCode hCode);
        MAPJSNode.MAPJSIdea UpdateNode(CommandDTO command, out HttpStatusCode hCode);
        IEnumerable<VertexDTO> GetManyV(List<AddedParam> addedParams, out HttpStatusCode hCode);

        bool InsertLink(MemeMVC.Model.DTO.LinksDTO.Link link, out HttpStatusCode hCode);

        bool RemoveLink(string rid_link, out HttpStatusCode hCode);
    }
}

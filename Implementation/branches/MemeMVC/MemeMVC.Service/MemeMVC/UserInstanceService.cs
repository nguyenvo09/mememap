using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
     public class UserInstanceService : IUserInstanceService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public UserInstanceService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public  UserInstanceDTO Get(int id, out HttpStatusCode hCode)
         {
            UserInstanceDTO rObj = null;
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.GetAsync(string.Format("api/UserInstance/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<UserInstanceDTO>().Result;
                    }
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
         public UserInstanceDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
               UserInstanceDTO result = null;
            try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<UserInstanceDTO>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public UserInstanceDTO Create(UserInstanceDTO obj, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<UserInstanceDTO>("api/UserInstance/Post", obj).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.Created) obj = response.Content.ReadAsAsync<UserInstanceDTO>().Result;
                    return obj;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public UserInstanceDTO Update(UserInstanceDTO obj, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<UserInstanceDTO>(string.Format("api/UserInstance/Put/{0}", obj.UserInstanceID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<UserInstanceDTO>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public UserInstanceDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK)  return response.Content.ReadAsAsync<UserInstanceDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<UserInstanceDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            IEnumerable<UserInstanceDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<UserInstanceDTO>>().Result;
                 }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
         }
        public ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
             ListDTOModel<UserInstanceDTO> result = null;
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if(hCode==HttpStatusCode.OK)  result = response.Content.ReadAsAsync<ListDTOModel<UserInstanceDTO>>().Result;
                }
            }
            catch
             {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
         public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
         {
            bool result = false;
            try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                           if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
             {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                      List<AddedParam> param = new List<AddedParam>();
                      param.Add(new AddedParam { Key = "UserInstanceID", Value = id });
                      HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/UserInstance/PostDelete", param).Result;
                      hCode = response.StatusCode;
                      if (hCode == HttpStatusCode.OK) return id;
                }
             }
             catch
             {
                hCode = HttpStatusCode.RequestTimeout;
             }
             return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
         public List<UserInstanceDTO> Update(List<UserInstanceDTO> list, out HttpStatusCode hCode)
         {
             try
             {
                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                 {
                     var response = httpClient.PostAsJsonAsync<List<UserInstanceDTO>>("api/UserInstance/PostList", list).Result;
                     hCode = response.StatusCode;
                     if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<UserInstanceDTO>>().Result;
                 }
             }
             catch
             {
                 hCode = HttpStatusCode.RequestTimeout;
             }
             return null;
         }
    }
    public interface IUserInstanceService
    {
           UserInstanceDTO Get(int id, out HttpStatusCode hCode);
           UserInstanceDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
           UserInstanceDTO Create(UserInstanceDTO obj, out HttpStatusCode hCode);
           UserInstanceDTO Update(UserInstanceDTO obj, out HttpStatusCode hCode);
           UserInstanceDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
           IEnumerable<UserInstanceDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
           ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
           bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
           int Delete(int id, out System.Net.HttpStatusCode hCode) ;
           void Dispose() ;
           List<UserInstanceDTO> Update(List<UserInstanceDTO> list, out HttpStatusCode hCode);
    }
}

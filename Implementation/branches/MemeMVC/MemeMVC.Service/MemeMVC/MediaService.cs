using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class MediaService : IMediaService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public MediaService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public MediaDTO Get(int id, out HttpStatusCode hCode)
        {
            MediaDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/Media/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<MediaDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public MediaDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            MediaDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<MediaDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public MediaDTO Create(MediaDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<MediaDTO>("api/Media/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<MediaDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public MediaDTO Update(MediaDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<MediaDTO>(string.Format("api/Media/Put/{0}", obj.MediaID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<MediaDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public MediaDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<MediaDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<MediaDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<MediaDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<MediaDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ListDTOModel<MediaDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<MediaDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<MediaDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "MediaID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Media/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<MediaDTO> Update(List<MediaDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<MediaDTO>>("api/Media/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<MediaDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
    }
    public interface IMediaService
    {
        MediaDTO Get(int id, out HttpStatusCode hCode);
        MediaDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        MediaDTO Create(MediaDTO obj, out HttpStatusCode hCode);
        MediaDTO Update(MediaDTO obj, out HttpStatusCode hCode);
        MediaDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<MediaDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<MediaDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<MediaDTO> Update(List<MediaDTO> list, out HttpStatusCode hCode);
    }
}

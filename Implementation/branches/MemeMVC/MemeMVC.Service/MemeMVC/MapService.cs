using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Base.Model.DTO;
using MemeMVC.Model.DTO;
namespace MemeMVC.Service
{
    public class MapService : IMapService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public MapService(string baseURI)
        {
            _baseURI = baseURI;
        }
        public MapDTO Get(int id, out HttpStatusCode hCode)
        {
            MapDTO rObj = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.GetAsync(string.Format("api/Map/Get/{0}", id)).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK)
                    {
                        rObj = response.Content.ReadAsAsync<MapDTO>().Result;
                    }
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return rObj;
        }
        public MapDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            MapDTO result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostOne", addedParams).Result;
                    hCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<MapDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public MapDTO Create(MapDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<MapDTO>("api/Map/Post", obj).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.Created) obj = response.Content.ReadAsAsync<MapDTO>().Result;
                    return obj;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public MapDTO Update(MapDTO obj, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    HttpResponseMessage response = httpClient.PutAsJsonAsync<MapDTO>(string.Format("api/Map/Put/{0}", obj.MapID), obj).Result;
                    hCode = response.StatusCode;
                    return response.Content.ReadAsAsync<MapDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return null;
            }
        }
        public MapDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostUpdate", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<MapDTO>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public IEnumerable<MapDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            IEnumerable<MapDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostMany", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<MapDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public ListDTOModel<MapDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            ListDTOModel<MapDTO> result = null;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostPage", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<ListDTOModel<MapDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)
        {
            bool result = false;
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostExist", addedParams).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
        public int Delete(int id, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "MapID", Value = id });
                    HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostDelete", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return id;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return 0;
        }
        public void Dispose()
        {
            httpClient.Dispose();
        }
        public List<MapDTO> Update(List<MapDTO> list, out HttpStatusCode hCode)
        {
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    var response = httpClient.PostAsJsonAsync<List<MapDTO>>("api/Map/PostList", list).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<MapDTO>>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return null;
        }
        public int GetNumOfMap(int userinstanceid, out HttpStatusCode hCode)
        {
            int result = 0;
            List<AddedParam> param = new List<AddedParam>();
            try
            {
                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })
                {
                    param.Add(new AddedParam { Key = "NumOfMap", Value = "" });
                    param.Add(new AddedParam { Key = "UserInstanceID", Value = userinstanceid });
                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>("api/Map/PostScalar", param).Result;
                    hCode = response.StatusCode;
                    if (hCode == HttpStatusCode.OK) result = response.Content.ReadAsAsync<int>().Result;
                }
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
            }
            return result;
        }
    }
    public interface IMapService
    {
        MapDTO Get(int id, out HttpStatusCode hCode);
        MapDTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);
        MapDTO Create(MapDTO obj, out HttpStatusCode hCode);
        MapDTO Update(MapDTO obj, out HttpStatusCode hCode);
        MapDTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);
        IEnumerable<MapDTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);
        ListDTOModel<MapDTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);
        bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);
        int Delete(int id, out System.Net.HttpStatusCode hCode);
        void Dispose();
        List<MapDTO> Update(List<MapDTO> list, out HttpStatusCode hCode);
        int GetNumOfMap(int userinstanceid, out HttpStatusCode hCode);
    }
}

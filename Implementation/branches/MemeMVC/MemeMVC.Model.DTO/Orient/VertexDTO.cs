﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO
{
    public partial class VerticesDTO
    {
        public class Vertex
        {
            public string rid { get; set; }
            public string type { get; set; }
            public int mapid { get; set; }
            public string name { get; set; }
            public string in_ { get; set; }
            public string out_ { get; set; }
            public string data { get; set; }
            public int level { get; set; }
            public float currentX { get; set; }
            public float currentY { get; set; }
            public bool isCollapsed { get; set; }

            public bool isStandAlone { get; set; }
            public bool hasCustomedLocation { get; set; }
        }
        public List<Vertex> result;
    }

    public partial class LinksDTO
    {
        public class Link
        {
            public string rid { get; set; }
            public int mapid { get; set; }
            public string in_ { get; set; }
            public string out_ { get; set; }
            public string name { get; set; }
            public bool isSecondary { get; set; }
        }
        public List<Link> result;
    }

    public partial class VertexDTO
    {
        public string rid { get; set; }
        public string type { get; set; }
        public int mapid { get; set; }
        public string name { get; set; }
        public string in_ { get; set; }
        public string out_ { get; set; }
        public string data { get; set; }
        public int level;
        public float currentX { get; set; }
        public float currentY { get; set; }
        public bool hasCustomedLocation { get; set; }
        public bool isCollapsed { get; set; }

        public bool isStandAlone { get; set; }

        public List<VertexDTO> childNodes;

        public bool HasParent { get; set; }
    }

    public class Attrs
    {
        public bool collapsed;
        public class Style
        {
            public string background;
        }
        public Style style;
    }

    
    
    public partial class MAPJSNode
    {
        public class MAPJSLink
        {
            public string name { get; set; }
            public int ideaIdFrom { get; set; }
            public int ideaIdTo { get; set; }

            public string rid { get; set; }
        }


        public class MAPJSIdea
        {           
            public Attrs attr{ get; set; }
            public string title{ get; set; }
            public int id { get; set; }
            public Dictionary<string, MAPJSIdea> ideas;
            public string rid { get; set; }
            public string _class { get; set; }
            public string data { get; set; }
            public int mapid { get; set; }
            public int level { get; set; }
            public float currentX { get; set; }
            public float currentY { get; set; }
            public bool isCollapsed { get; set; }
            public bool isStandAlone { get; set; }
            public bool hasCustomedLocation { get; set; }
        }
        public int formatVersion { get; set; }
        public string title { get; set; }
        public int id { get; set; }
        public Dictionary<string,MAPJSIdea> ideas;
        public List<MAPJSLink> links;
    }
}

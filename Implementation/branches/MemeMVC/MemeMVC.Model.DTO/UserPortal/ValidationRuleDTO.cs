using System;
namespace MemeMVC.Model.DTO
{
    public partial class ValidationRuleDTO
    {
        public int ValidationRuleID { get; set; }
        public int ItemConfigID { get; set; }
        public string Code { get; set; }
        public string ApplyRule { get; set; }
        public string Message { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string GUID { get; set; }
    }
}

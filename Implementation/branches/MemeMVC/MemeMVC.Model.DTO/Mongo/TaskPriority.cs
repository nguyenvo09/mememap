﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    /// <summary>
    /// Priority class of a task. Currently there are 3 types and they are being hardcoded in 
    /// mongodb. Low, Medium, High.
    /// </summary>
    public class TaskPriority : MongoEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_priority"></param>
        public TaskPriority(string _priority)
        {
            priority = _priority;
        }
        public string priority { get; set; }
        

    }
}

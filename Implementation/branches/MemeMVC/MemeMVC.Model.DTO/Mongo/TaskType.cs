﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    /// <summary>
    /// There 4 type of task:  Task ,  Idea, Implementation, Documentation.
    /// </summary>
    public class TaskType : MongoEntity
    {
        /// <summary>
        /// type of the task
        /// </summary>
        /// <param name="_type"></param>
        public TaskType(string _type)
        {
            type = _type;
            
        }
        public string type { get; set; }

    }
}

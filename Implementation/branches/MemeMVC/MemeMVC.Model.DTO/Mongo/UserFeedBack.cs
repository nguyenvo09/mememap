﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    public class UserFeedBack : MongoEntity
    {
        public UserFeedBack()
        {
        }
      
        public string UserName { get; set; }

        public string UserEmail { get; set; }
       
        public string Content { get; set; }

        public string Subject { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}

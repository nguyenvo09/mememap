﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    public class UserMapShared : MongoEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_mapId"></param>
        /// <param name="_userId"></param>
        public UserMapShared(int _mapId, int _userId, string _username, string _userEmail, string _profileImgUrl)
        {
            mapId = _mapId;
            userId = _userId;
            username = _username;
            userEmail = _userEmail;
            profileImageUrl = _profileImgUrl;
        }
        public int mapId { get; set; }
        public int userId { get; set; }

        public string username { get; set; }

        public string userEmail { get; set; }

        public string profileImageUrl { get; set; }

    }
}

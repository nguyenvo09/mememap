﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using MemeMVC.Model.DTO.Mongo;
namespace MemeMVC.Model.DTO
{
    public class MongoEntity : IMongoEntity
    {
        [BsonId]
        [JsonConverter(typeof(ObjectIdConverter))]
        public ObjectId Id { get; set; }
    }
    public interface IMongoEntity
    {
        ObjectId Id { get; set; }
    }
}

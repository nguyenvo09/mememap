﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    /// <summary>
    /// There are 6 types of status including:
    /// New, Assigned, Open, Re-Assigned, Cancelled, Closed
    /// </summary>
    public class TaskStatus : MongoEntity
    {
        /// <summary>
        /// status of the task.
        /// </summary>
        /// <param name="_status"></param>
        public TaskStatus(string _status)
        {
            status = _status;
        }
        public string status { get; set; }

    }
}

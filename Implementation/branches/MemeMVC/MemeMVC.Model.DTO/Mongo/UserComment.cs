﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    public class UserComment : MongoEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_mapId"></param>
        /// <param name="_nodeId"></param>
        /// <param name="_userId"></param>
        /// <param name="_comment"></param>
        public UserComment(int _mapId, int _nodeId, int _userId, string _username, string _comment, DateTime _createdDate, string _profileImage, string _rid)
        {
            mapId = _mapId;
            nodeId = _nodeId;
            userId = _userId;
            comment = _comment;
            username = _username;
            createdDate = _createdDate;
            imgProfile = _profileImage;
            rid = _rid;
        }
        public int mapId { get; set; }
        public int nodeId { get; set; }
        public int userId { get; set; }
        public string username { get; set; }
        public string comment { get; set; }
        public DateTime createdDate { get; set; }

        public string imgProfile { get; set; }

        public string rid { get; set; }
    }
}

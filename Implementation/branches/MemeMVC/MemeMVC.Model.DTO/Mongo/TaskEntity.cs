﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemeMVC.Model.DTO.Mongo
{
    /// <summary>
    /// There 4 type of task:  Task ,  Idea, Implementation, Documentation.
    /// </summary>
    public class TaskEntity : MongoEntity
    {
        /// <summary>
        /// type of the task
        /// </summary>
        /// <param name="_type"></param>
        public TaskEntity(int _mapId, int _nodeId, string _title, string _estimation, DateTime _dueDate, DateTime _startDate, DateTime _completionDate,
            string _type, string _priority, string _status, List<String> _assignees)
        {
            Title = _title;
            Estimation = _estimation;
            DueDate = _dueDate;
            StartDate = _startDate;
            CompletionDate = _completionDate;
            taskType = _type;
            taskPriority = _priority;
            taskStatus= _status;
            Assignees = _assignees;
            mapId = _mapId;
            nodeId = _nodeId;
        }
        public int mapId { get; set; }
        public int nodeId { get; set; }

        public string Title { get; set; }

        public string Estimation { get; set; }

        public DateTime DueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime CompletionDate { get; set; }
        public string taskType { get; set; }
        public string taskPriority { get; set; }
        public string taskStatus{ get; set; }

        public List<String> Assignees { get; set; }

    }
}

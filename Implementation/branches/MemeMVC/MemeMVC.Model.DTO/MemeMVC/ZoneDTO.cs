using System;
namespace MemeMVC.Model.DTO
{
    public partial class ZoneDTO
    {
        public int ZoneID { get; set; }
        public int MapID { get; set; }
        public string Data { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
        public string MapName { get; set; }
    }

    public partial class CommandDTO
    {
        public string Action { get; set; }
        public string CommandData { get; set; }
    }

    public partial class TwoNodesCommandDTO
    {
        public MAPJSNode.MAPJSIdea parentIdea { get; set; }
        public MAPJSNode.MAPJSIdea subIdea { get; set; }
    }
    public partial class OneNodeCommandDTO
    {
        public MAPJSNode.MAPJSIdea subIdea { get; set; }
    }

    public partial class FourStringCommandDTO
    {
        public string r1 { get; set; }
        public string r2 { get; set; }
        public string r3 { get; set; }
        public string r4 { get; set; }
    }
}

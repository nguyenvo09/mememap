using System;
namespace MemeMVC.Model.DTO
{
    public partial class EntityDTO
    {
        public int EntityID { get; set; }
        public string Name { get; set; }
        public string Property { get; set; }
        public string Data { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
    }
}

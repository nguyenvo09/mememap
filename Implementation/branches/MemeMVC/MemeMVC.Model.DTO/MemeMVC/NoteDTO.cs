using System;
namespace MemeMVC.Model.DTO
{
    public partial class NoteDTO
    {
        public int NoteID { get; set; }
        public int MapID { get; set; }

        public string RID { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }

    [Serializable]
    public class NoteLinkedInDTO
    {
        public int NoteID { get; set; }
        public int MapID { get; set; }

        public string RID { get; set; }
        public string Body { get; set; }
    }
}

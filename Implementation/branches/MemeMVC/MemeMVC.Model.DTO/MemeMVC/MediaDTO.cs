using System;
namespace MemeMVC.Model.DTO
{
    public partial class MediaDTO
    {
        public int MediaID { get; set; }
        public string Data { get; set; }
        public Guid Media_Guid { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
    }
}

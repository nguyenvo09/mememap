using System;
namespace MemeMVC.Model.DTO
{
    public partial class ProjectDTO
    {
        public int ProjectID { get; set; }
        public int UserInstanceID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
        public string UserName { get; set; }
        public string LastAccessDate { get; set; }
        public int NumOfMap { get; set; }
    }
}

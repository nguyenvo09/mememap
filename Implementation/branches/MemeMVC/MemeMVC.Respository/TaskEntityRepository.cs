﻿using Base.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemeMVC.Model.DTO.Mongo;

namespace MemeMVC.Repository
{
    public class TaskEntityRepository : Repository<TaskEntity>, ITaskEntityRepository
    {
    }
    public interface ITaskEntityRepository : IRepository<TaskEntity>
    {
    }
}

﻿using Base.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemeMVC.Model.DTO.Mongo;

namespace MemeMVC.Repository
{
    public class UserMapSharedRepository : Repository<UserMapShared>, IUserMapSharedRepository
    {
    }
    public interface IUserMapSharedRepository : IRepository<UserMapShared>
    {
    }
}

﻿using Base.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemeMVC.Model.DTO.Mongo;

namespace MemeMVC.Repository
{
    public class TaskTypeRepository : Repository<TaskType>, ITaskTypeRepository
    {
    }
    public interface ITaskTypeRepository : IRepository<TaskType>
    {
    }
}

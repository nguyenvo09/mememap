﻿using Base.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemeMVC.Model.DTO.Mongo;

namespace MemeMVC.Repository
{
    public class CommentRepository : Repository<UserComment>, ICommentRepository
    {
    }
    public interface ICommentRepository : IRepository<UserComment>
    {
    }
}

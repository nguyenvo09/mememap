﻿using MemeMVC.Model.DTO.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace MemeMVC.Repository
{
    class GenerateModels
    {
        static void Main()
        {
            
            TaskStatusRepository _taskStatusRepo = new TaskStatusRepository();
            TaskTypeRepository _taskTypeRepo = new TaskTypeRepository();
            TaskPriorityRepository _taskPriRepo = new TaskPriorityRepository();
            TaskEntityRepository _taskEntityRepo = new TaskEntityRepository();
            try
            {
                var o1 = new TaskStatus("New");
                var o2 = new TaskStatus("Assigned");
                var o3 = new TaskStatus("Open");
                var o4 = new TaskStatus("Re-Assigned");
                var o5 = new TaskStatus("Cancelled");
                var o6 = new TaskStatus("Closed");

                bool val = _taskStatusRepo.SaveOrUpdate(o1);
                val = _taskStatusRepo.SaveOrUpdate(o2);
                val = _taskStatusRepo.SaveOrUpdate(o3);
                val = _taskStatusRepo.SaveOrUpdate(o4);
                val = _taskStatusRepo.SaveOrUpdate(o5);
                val = _taskStatusRepo.SaveOrUpdate(o6);
                
                //Console.WriteLine("Hello World!");
                var o7 = new TaskType("Task");
                  val =_taskTypeRepo.SaveOrUpdate(o7);
                _taskTypeRepo.SaveOrUpdate(new TaskType("Implementation"));

                _taskTypeRepo.SaveOrUpdate(new TaskType("Idea"));

                _taskTypeRepo.SaveOrUpdate(new TaskType("Documentation"));
                TaskPriority o11 = new TaskPriority("Low");
                _taskPriRepo.SaveOrUpdate(o11);
                _taskPriRepo.SaveOrUpdate(new TaskPriority("Medium"));
                _taskPriRepo.SaveOrUpdate(new TaskPriority("High"));
                ObjectId obj = new ObjectId();
                ObjectId.TryParse("121212121", out obj);
                o11.Id = obj;
                //TaskEntity entity = new TaskEntity("Test1", "Estimation", DateTime.Now, DateTime.Now, DateTime.Now, o7, o11, o1, null);
               // _taskEntityRepo.SaveOrUpdate(entity);
                // Keep the console window open in debug mode.
                Console.WriteLine("Press any key to exit. " );
                Console.ReadKey();
            }
            catch (Exception e)
            {

            }
            
        }
    }
}

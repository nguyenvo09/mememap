var gbks = gbks || {};

gbks.Help = function () {

    this.init = function () {
        $('.questions li h3').click($.proxy(this.toggleQuestion, this));
    };

    this.toggleQuestion = function (event) {
        var header = $(event.currentTarget);
        var question = $(header.parents('li')[0]);
        var body = $('p', question);

        var title = $('h3', question).html();

        if (question.hasClass('active')) {
            question.removeClass('active');
            body.slideUp();
            this.track('Help', title, 'hide');
        } else {
            question.addClass('active');
            body.slideDown();
            this.track('Help', title, 'show');
        }
    };

    this.track = function (one, two, three) {
        if (typeof (_gaq) !== 'undefined') {
            _gaq.push(['_trackEvent', one, two, three]);
        }
    };

};

$(document).ready(function () {
    var help = new gbks.Help();
    help.init();
});
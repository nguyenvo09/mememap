var gbks = gbks || {};
gbks.common = gbks.common || {};

gbks.Header = function () {

    this.init = function () {
        this.searchDefault = 'Search';
        this.searchField = $('#header .options .search input');
        this.searchField.focus($.proxy(this.onFocusSearch, this));
        this.searchBlurMethod = $.proxy(this.onBlurSearch, this);
        this.keyUpMethod = $.proxy(this.onKeyUp, this);

        this.searchHolder = $('#header .options .search');
        this.autoComplete = null;
        this.searchFocused = false;
        this.isAuthenticated = $('body').hasClass('auth');
        this.badWords = [];

        this.updateCookie();

        // Add hover handling for dropdowns.
        $('#header .dropdown').mouseover($.proxy(this.onMouseOverDropdown, this));
        $('#header .dropdown').mouseout($.proxy(this.onMouseOutDropdown, this));
        this.activeDropdown = null;

        this.checkPlusMessage();

        $('.nsfwCover').live('click', $.proxy(this.onClickNSFWCover, this));

        // Track pixel density.
        setTimeout($.proxy(this.trackPixelDensity, this), 100);
    };

    this.updateCookie = function () {
        // Save pixel ratio in cookie.
        var pixelRatio = (!!window.devicePixelRatio ? window.devicePixelRatio : 1);
        gbks.common.Cookie('devicePixelRatio', pixelRatio, { path: '/' });

        // Save screen width in cookie.
        gbks.common.Cookie('viewport', $(window).width(), { path: '/' });
    };

    this.trackPixelDensity = function (event) {
        var pixelRatio = (!!window.devicePixelRatio ? window.devicePixelRatio : 1);
        if (pixelRatio > 1) {
            var w = $(window);
            var p = w.width() + 'x' + w.height();
            gbks.common.track('Info', 'dpr' + pixelRatio, p);
        }
    };

    this.onClickNSFWCover = function (event) {
        var cover = $(event.currentTarget);
        var parents = cover.parents('.nsfw');
        if (parents.length > 0) {
            var parent = $(parents[0]);
            parent.removeClass('nsfw');

            event.preventDefault();
            event.stopPropagation();
        }
    };

    this.checkPlusMessage = function () {
        var message = $('#plusMessage');
        if (message.length == 0) return;

        $('.close', message).click($.proxy(this.onClickClosePlusMessage, this));
        $('a', message).click($.proxy(this.onClickPlusMessageLink, this));

        var messageHidden = gbks.common.Cookie('plusMessageHidden');
        if (!messageHidden) {
            message.show();
        }
    };

    this.onClickPlusMessageLink = function (event) {
        gbks.common.track('Header', 'PlusMessage', 'Link');
        this.closePlusMessage();
    };

    this.onClickClosePlusMessage = function (event) {
        gbks.common.track('Header', 'PlusMessage', 'Close');
        this.closePlusMessage();
    };

    this.closePlusMessage = function (event) {
        var message = $('#plusMessage');
        message.hide();

        gbks.common.Cookie('plusMessageHidden', true, { path: '/', expires: 300 });
    };

    this.onMouseOverDropdown = function (event) {
        var target = $(event.currentTarget);

        $('#header .dropdown').removeClass('active');
        if (this.activeDropdown != null && this.activeDropdown != target) {
            //this.activeDropdown.removeClass('active');
        }

        this.activeDropdown = target;
        this.activeDropdown.addClass('active');
        this.onDropdownTimer();
    };

    this.onMouseOutDropdown = function (event) {
        var target = $(event.currentTarget);
        this.activeDropdown = null;
    };

    this.onDropdownTimer = function () {
        if (this.activeDropdown == null) {
            $('#header .dropdown').removeClass('active');
        } else {
            this.activeDropdown.addClass('active');
            setTimeout($.proxy(this.onDropdownTimer, this), 500);
        }
    };

    this.onFocusSearch = function (event) {
        this.searchFocused = true;

        this.searchField.blur(this.searchBlurMethod);
        $(document).keyup(this.keyUpMethod);

        var term = this.searchField.val();
        if (term == this.searchDefault) {
            this.searchField.val('');
            this.searchField.addClass('active');
        }

        this.updateAutoComplete();
    };

    this.onBlurSearch = function (event) {
        this.searchFocused = false;

        this.searchField.unbind('blur', this.searchBlurMethod);
        $(document).unbind('keyup', this.keyUpMethod);

        var term = this.searchField.val();
        if (term == '') {
            this.searchField.val(this.searchDefault);
            this.searchField.removeClass('active');
        }

        //this.hideAutoComplete();
    };

    this.onKeyUp = function (event) {
        switch (event.which) {
            case 13:
                this.performSearch();
                break;
        }

        if (!this.autoComplete) {
            this.loadAutoComplete();
        } else {
            this.updateAutoComplete();
        }
    };

    this.performSearch = function () {
        var term = this.searchField.val();
        if (term.length > 0 && term !== this.searchDefault) {
            window.location.href = '/search/' + encodeURIComponent(term);
        }
    };

    this.updateAutoComplete = function () {
        var term = this.searchField.val();
        var visibleMax = (term.length < 3 ? 3 : 5);
        var sections = $('.section', this.autoComplete);
        var active = (this.autoComplete && sections.length > 0 && this.searchFocused);
        var visibleCount = 0, sectionCount;
        if (active && term.length > 0) {
            var i = 0, length = sections.length, section;
            for (; i < length; i++) {
                section = $(sections[i]);
                sectionCount = this.updateAutoCompleteSection(term, section, visibleMax);
                visibleCount += sectionCount;

                if (sectionCount == 0) section.hide();
                else section.show();
            }
        }

        if (this.badWords.indexOf(term) != -1) this.searchField.addClass('isBad');
        else this.searchField.removeClass('isBad');

        if (!active || visibleCount == 0) {
            this.hideAutoComplete();
        } else {
            gbks.common.positionPopup(this.autoComplete, this.searchField);
            this.autoComplete.show();
        }
    };

    this.updateAutoCompleteSection = function (term, section, visibleMax) {
        var items = $('li', section);
        var visibleCount = 0;
        var i = 0, length = items.length, item, val;
        for (; i < length; i++) {
            item = $(items[i]);
            val = item.attr('data-name');
            if (val && term.length == 0 || val.toLowerCase().indexOf(term) != -1 && visibleCount < visibleMax) {
                item.show();
                visibleCount++;
            } else {
                item.hide();
            }
        }

        return visibleCount;
    };

    this.hideAutoComplete = function () {
        if (this.autoComplete) this.autoComplete.hide();
    };

    this.evalAutoComplete = function () {
        var term = this.searchField.val();
        return (term.length > 0 && term !== this.searchDefault);
    };

    this.loadAutoComplete = function () {
        if (!this.autoComplete) {
            var html = gbks.common.wrapPopupContent('searchAutoComplete', '<div class="content"></div>');
            this.autoComplete = $(html);
            $('.content', this.autoComplete).load('/autocomplete', $.proxy(this.onLoadAutoComplete, this));
        }
    };

    this.onLoadAutoComplete = function () {
        $('body').append(this.autoComplete);
        this.badWords = $('#badWords').attr('data-words').split(',');
        console.log('onLoadA', $('#badWords'), $('#badWords').attr('data-words'), this.badWords);
        this.updateAutoComplete();
    };

}

/**
* Loader.
*/
gbks.common.Loader = gbks.common.Loader || {};
gbks.common.Loader.show = function (message) {
    var loader = gbks.common.Loader.getLoader();
    loader.stop();
    if (message && message.length > 0) {
        loader.html(message);
    } else {
        loader.html('');
    }

    loader.show();
    loader.animate({ opacity: 1 }, 50);
};

gbks.common.Loader.hide = function () {
    var loader = gbks.common.Loader.getLoader();
    loader.stop();
    var callback = null;
    loader.animate({ opacity: 0 }, 250, callback);
};

gbks.common.Loader.onHide = function (event) {
    gbks.common.Loader.getLoader().hide();
};

gbks.common.Loader.getLoader = function () {
    return $('#loader');
};

/**
* Tracking helpers.
*/
gbks.common.track = function (one, two, three) {
    if (typeof (_gaq) !== 'undefined') {
        _gaq.push(['_trackEvent', one, two, three]);
    }
};

gbks.common.onWindowError = function (message, file, line) {
    if (typeof (_gaq) !== 'undefined') {
        var sFormattedMessage = '[' + file + ' (' + line + ')] ' + message;
        _gaq.push(['_trackEvent', 'Exceptions', 'Application', sFormattedMessage, null, true]);
    }
}

/**
* Interaction for polaroid-style display of images.
*/
gbks.common.savePopupInstance = null;
gbks.common.SavePopup = function () {

    if (gbks.common.savePopupInstance) {
        gbks.common.savePopupInstance.hide();
        gbks.common.savePopupInstance = null;
    }
    gbks.common.savePopupInstance = this;

    this.init = function (button, data, unsaveCallback, closeCallback) {
        this.button = button;
        this.data = data;
        this.unsaveCallback = unsaveCallback;
        this.closeCallback = closeCallback;

        this.createCanvas();
        //this.keyUpMethod = $.proxy(this.onKeyUp, this);

        this.resizeMethod = $.proxy(this.onResize, this);

        this.clickDocumentMethod = $.proxy(this.onClickDocument, this);
    };

    this.createCanvas = function () {
        var groups = this.data.groups;

        // Check if me.
        if (!groups) return;

        // Show groups overlay.
        var html = this.data.html;

        if (this.canvas) {
            $('#groupsOverlay').remove();
        }
        this.canvas = $(html);

        $('body').append(this.canvas);

        this.updatePosition();

        // Hook up checkbox events.
        $('input[type=checkbox]', this.canvas).bind('change', $.proxy(this.onClickGroupOverlayItem, this));

        // Hide when pressing close button.
        $('.closeButton', this.canvas).click($.proxy(this.hide, this));

        // Create group events.
        $('.addForm input[type=text]', this.canvas).bind('focus', $.proxy(this.onFocusCreateGroupInput, this));
        $('.addForm input[type=text]', this.canvas).bind('blur', $.proxy(this.onBlurInput, this));
        $('#formCreateGroup', this.canvas).submit($.proxy(this.onClickCreateGroup, this));
        $('.unsaveButton', this.canvas).click($.proxy(this.onClickGroupOverlayUnsave, this));
        $('#dropboxButton', this.canvas).click($.proxy(this.onClickDropbox, this));

        $('input[type=radio]', this.canvas).bind('change', $.proxy(this.onClickPrivacyOption, this));

        $(document).mousedown(this.clickDocumentMethod);
        $(window).resize(this.resizeMethod);
    };

    this.updatePosition = function () {
        var docWidth = $(window).width();

        var pos = this.button.offset();
        var overlayWidth = this.canvas.width();
        var left = Math.round(pos.left + this.button.width() / 2 - overlayWidth / 2) - 2;
        var right = left + overlayWidth;
        var docRightDelta = docWidth - 10 - right;
        var arrowLeft = overlayWidth / 2;

        if (docRightDelta < 0) {
            left += docRightDelta;
            arrowLeft -= docRightDelta;
        }
        if (left < 0) {
            left -= left - 10;
            arrowLeft += left - 10;
        }

        // Check if we're in the lower half of the screen.
        var windowHeight = $(window).height();
        var center = windowHeight / 2;
        var buttonY = this.button.offset().top - window.pageYOffset + this.button.height() / 2;
        var deltaTop = buttonY;
        var deltaBottom = windowHeight - deltaTop;
        var showTop = (deltaTop > deltaBottom);
        if (showTop) {
            this.canvas.addClass('topper');
        } else {
            this.canvas.removeClass('topper');
        }

        // Apply positioning.
        var top = pos.top + this.button.height() + 17;

        if (showTop) {
            top = pos.top - 17 - this.canvas.height();
        }

        this.canvas.css({
            left: left + 'px',
            top: top + 'px'
        });

        $('.arrow', this.canvas).css('left', arrowLeft + 'px');
    };

    this.unsaveImage = function () {
        $.ajax({
            url: '/bookmark/removefromuser?imageId=' + this.data.imageId,
            type: 'POST',
            dataType: 'jsonp',
            success: $.proxy(this.onUnsaveImage, this)
        });

        this.hide();
    };

    this.onUnsaveImage = function (result) {

    };

    this.onClickDropbox = function (event) {
        event.preventDefault();
        event.stopPropagation();
        var button = $(event.currentTarget);
        button.addClass('active');
        button.addClass('loading');

        $.ajax({
            url: '/bookmark/dropbox?ref=popup&imageId=' + this.data.imageId,
            type: 'POST',
            success: $.proxy(this.onSaveToDropbox, this)
        });
    };

    this.onSaveToDropbox = function (data) {
        $('#dropboxButton', this.canvas).removeClass('loading');
    };

    this.onFocusCreateGroupInput = function (event) {
        $('.addForm', this.canvas).addClass('active');
        this.onFocusInput(event);
    };

    this.onClickGroupOverlayUnsave = function (event) {
        this.unsaveImage(this.data.imageId);

        if (this.unsaveCallback) {
            this.unsaveCallback(this.data.imageId);
        }
    };

    this.onClickCreateGroup = function (event) {
        event.preventDefault();

        var form = $('#formCreateGroup', this.canvas);
        var nameField = $('input[type=text]', form);
        var imageId = $('input[name="imageId"]', form).val();
        var groupName = nameField.val();
        var nameDefault = nameField.attr('data-default');
        var url = '/groups/create';

        if (groupName.length > 0 && groupName != nameDefault) {
            gbks.common.track('Polaroid', 'CreateGroup', groupName);

            // Hide and clear group creation form.
            form.removeClass('active');
            nameField.val('');
            $('input', form).attr('disabled', true);

            $('input[type=submit]', this.canvas).addClass('loading');

            // Make call.
            var data = { imageId: imageId, groupName: groupName };
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                success: $.proxy(this.onCreateGroup, this)
            });
        }
    };

    this.onCreateGroup = function (json) {
        $('#formCreateGroup input[type=submit]', this.canvas).removeClass('loading');
        $('#formCreateGroup input', this.canvas).removeAttr('disabled');

        var group = $.parseJSON(json);
        group = json;

        var html = '<li><input type="checkbox" name="groupId" value="' + group.id + '" checked="true" />' + group.name + '</li>';

        var lists = $('ul', this.canvas);
        var list = $(lists[lists.length - 1]);
        list.append(html);
        lists.removeClass('empty');

        this.hideLoader();
    };

    this.onFocusInput = function (event) {
        var target = $(event.currentTarget);
        target.addClass('active');
        if (target.val() == target.attr('data-default')) {
            target.val('');
        }
    };

    this.onBlurInput = function (event) {
        var target = $(event.currentTarget);
        target.addClass('active');
        if (target.val() == '') {
            target.val(target.attr('data-default'));
        }
    };

    this.onClickGroupOverlayItem = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var box = $(event.currentTarget);
        var item = $(box.parents('li')[0]);

        var c = item.attr('data-checked');

        var groupId = box.val();
        var checked = (c == "true");

        var url = '/groups/removeImageFromGroup';
        var message = 'Removing from group';
        if (checked) {
            box.removeAttr('checked');
            item.attr('data-checked', false);
            gbks.common.track('Polaroid', 'RemoveFromGroup', this.data.imageId + '-' + groupId);
        } else {
            url = '/groups/addImageToGroup';
            message = 'Adding to group';
            box.attr('checked', 'checked');
            item.attr('data-checked', true);
            gbks.common.track('Polaroid', 'AddToGroup', this.data.imageId + '-' + groupId);
        }

        item.addClass('loading');

        // Make call.
        $.ajax({
            url: url,
            data: { imageId: this.data.imageId, groupId: groupId },
            type: 'POST',
            success: $.proxy(this.onGroupSaved, this)
        });
    };

    this.onGroupSaved = function (event) {
        if (this.canvas) {
            $('li', this.canvas).removeClass('loading');
        }
        //$('#addToGroups li').removeClass('loading');
        //this.hideLoader();
    };

    this.onClickPrivacyOption = function (event) {
        var radio = $(event.currentTarget);
        var val = radio.val();

        var isPrivate = (val == 'private');
        var url = '/bookmark/setpublic';
        if (isPrivate) {
            url = '/bookmark/setprivate';
        }

        $('.privacy', this.canvas).addClass('loading');

        // Make call.
        $.ajax({
            url: url,
            data: { imageId: this.data.imageId },
            type: 'POST',
            success: $.proxy(this.onChangePrivacy, this)
        });
    };

    this.onChangePrivacy = function (event) {
        $('.privacy', this.canvas).removeClass('loading');
    };

    this.onClickDocument = function (event) {
        var target = $(event.target);
        var parents = target.parents('#groupsOverlay');
        if (parents.length == 0) {
            this.hide(null);
            event.stopPropagation();
            event.preventDefault();
        }
    };

    this.hide = function (event) {
        $(document).unbind('mousedown', this.clickDocumentMethod);
        $(window).unbind('resize', this.resizeMethod);

        if (this.canvas) {
            this.canvas.remove();
            this.canvas = null;
        }
    };

    this.showLoader = function (message) {
        gbks.common.Loader.show(message);
    };

    this.hideLoader = function () {
        gbks.common.Loader.hide();
    };

    this.onResize = function (event) {
        this.updatePosition();
    };

};

/**
* Lightbox
*/
gbks.common.lightboxInstance = null;
gbks.common.Lightbox = function () {

    if (gbks.common.lightboxInstance) {
        gbks.common.lightboxInstance.hide();
        gbks.common.lightboxInstance = null;
    }
    gbks.common.lightboxInstance = this;

    this.init = function () {
        this.canvas = null;
        this.hideTimer = null;
        this.savePopup = null;
        this.sharePopup = null;

        this.updateHistory();

        this.initHistory = window.location.href;
        this.initTitle = window.document.title;

        this.keyUpMethod = $.proxy(this.onKeyUp, this);
        this.resizeMethod = $.proxy(this.onResize, this);
    };

    this.updateHistory = function () {
        this.initHistory = window.location.href;
        this.initTitle = window.document.title;
    }

    this.display = function () {

    };

    this.hide = function () {
        if (this.canvas) {
            this.canvas.remove();
            this.canvas = null;

            gbks.common.history.push(this.initHistory, this.initTitle);

            if (this.savePopup) {
                this.savePopup.hide();
                this.savePopup = null;
            }

            if (this.sharePopup) {
                this.sharePopup.hide();
                this.sharePopup = null;
            }

            $(window).unbind('resize', this.resizeMethod);
        }
        $('body').removeClass('lightboxActive');
    };

    this.createCanvas = function () {
        if (!this.canvas) {
            var html = '';
            if (Modernizr.cssanimations && Modernizr.opacity) {
                html = '<div id="lightbox" data-id="0" class="hidden">';
            } else {
                html = '<div id="lightbox" data-id="0">';
            }
            html += '  <div class="cover"></div>';
            html += '  <div class="detailPod"></div>';
            html += '  <div class="loader"></div>';
            html += '  <div class="contentPreview"></div>';
            html += '  <div class="contentWrap">';
            /**
            html += '  <div class="image">';
            html += '    <div class="wrap">';
            html += '      <a href="">';
            html += '         <img src="" width="" height="">';
            html += '      </a>';
            html += '      <h3><a href=""></a></h3>';
            html += '    </div>';
            html += '  </div>';
            html += '  <div class="details">';
            html += '    <div class="wrap"></div>';
            html += '  </div>';
            //*/
            html += '  </div>';
            html += '  <div class="left"><img src="/assets/arrow-left-medium.png" width="15" height="24"></div>';
            html += '  <div class="right"><img src="/assets/arrow-right-medium.png" width="15" height="24"></div>';
            html += '</div>';
            $('body').append(html);
            this.canvas = $('#lightbox');

            $('.left', this.canvas).click($.proxy(this.previous, this));
            $('.right', this.canvas).click($.proxy(this.next, this));
            this.canvas.click($.proxy(this.onClickLightbox, this));

            $('.similar li a', this.canvas).live('click', $.proxy(this.onClickSimilarImage, this));

            $(document).keyup(this.keyUpMethod);
            $(window).bind('resize', this.resizeMethod);
        }

        $('body').addClass('lightboxActive');

        setTimeout($.proxy(this.fadeIn, this), 25);

        this.canvas.focus();
    };

    this.fadeIn = function () {
        this.canvas.removeClass('hidden');
    };

    this.onClickSimilarImage = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var target = $(event.currentTarget);
        var id = target.attr('data-id');
        this.updateFromId(id);
    };

    this.detectFullscreen = function () {
        for (var i = 0; i < Modernizr._domPrefixes.length; i++) {
            if (document[Modernizr._domPrefixes[i].toLowerCase() + 'CancelFullScreen'])
                return true;
        }
        return !!document['cancelFullScreen'] || false;
    }

    this.onClickLightbox = function (event) {
        var target = $(event.target);
        var isDetails = (target.parents('#lightboxDetails').length > 0);
        if (!target.is('img') && !target.is('a') && !isDetails) {
            event.preventDefault();
            event.stopPropagation();
            this.hide();

            $(document).unbind('keyup', this.keyUpMethod);
        }
    };

    this.onKeyUp = function (event) {
        var focused = $('*:focus');
        if (focused.length > 0) return;
        if (this.canvas) {
            switch (event.which) {
                case 37:
                    this.previous();
                    break;
                case 39:
                    this.next();
                    break;
                case 70:
                    this.goFullScreen();
                    break;
            }
        }
    };

    this.goFullScreen = function () {
        // Request full screen if available.
        if (this.detectFullscreen()) {
            var e = this.canvas[0];
            if (e.requestFullScreen) {
                e.requestFullScreen();
            } else if (e.mozRequestFullScreen) {
                e.mozRequestFullScreen();
            } else if (e.webkitRequestFullScreen) {
                e.webkitRequestFullScreen();
            }
        }
    };

    this.previous = function (event) {
        var tiles = $('.tile');
        var currentId = this.canvas.attr('data-id');
        var currentTile = $('#image_' + currentId);

        var newTile;
        if (currentTile.length > 0) {
            var currentIndex = tiles.index(currentTile);
            if (currentIndex > 0) {
                newTile = $(tiles[currentIndex - 1]);
                while (newTile.hasClass('ad') && newTile != null && newTile.length > 0) {
                    currentIndex--;
                    newTile = $(tiles[currentIndex - 1]);
                }
            }
        }

        if (newTile && newTile.length > 0 && newTile.attr('id') != undefined) {
            this.update(newTile);
        } else {
            // Check if we have similar images to get the next image from.
            var similar = $('.similar a', this.canvas);
            if (similar.length > 0) {
                var last = $(similar[similar.length - 1]);
                var id = last.attr('data-id');
                this.updateFromId(id);
            } else {
                this.hide();
            }
        }

        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }

        this.hideSharePopup();
    };

    this.next = function (event) {
        var tiles = $('.tile');
        var currentId = this.canvas.attr('data-id');
        var currentTile = $('#image_' + currentId);

        // If the tiles exists on the page, get the next one.
        var newTile;
        if (currentTile.length > 0) {
            var currentIndex = tiles.index(currentTile);
            if (currentIndex < (tiles.length - 1)) {
                newTile = $(tiles[currentIndex + 1]);
                while (newTile.hasClass('ad') && newTile != null && newTile.length > 0) {
                    currentIndex++;
                    newTile = $(tiles[currentIndex + 1]);
                }
            }
        }

        if (newTile && newTile.length > 0 && newTile.attr('id') != undefined) {
            this.update(newTile);
        } else {
            // Check if we have similar images to get the next image from.
            var similar = $('.similar a', this.canvas);
            if (similar.length > 0) {
                var id = $(similar[0]).attr('data-id');
                this.updateFromId(id);
            } else {
                this.hide();
            }
        }

        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }

        this.hideSharePopup();
    };

    /**
    * Hide lightbox.
    * Update lightbox content, start loading image.
    * Show lightbox once image loaded.
    */
    this.update = function (tile) {
        this.activeTile = tile;

        var id = tile.attr('id');
        var bits = id.split('_');
        this.updateFromId(bits[1]);
    };

    this.updateFromId = function (id) {
        this.imageId = id;

        $('.image .wrap', this.canvas).fadeTo(250, 0);
        $('.details', this.canvas).fadeTo(250, 0);
        $('.loader', this.canvas).fadeIn(250);

        if (!this.canvas) {
            this.createCanvas();
            this.animateThumb();
        } else {
            $('.contentPreview', this.canvas).remove();
        }

        clearTimeout(this.hideTimer);
        this.hideTimer = setTimeout($.proxy(this.updateContent, this), 250);
    };

    this.animateThumb = function () {
        var polaroid = $('#image_' + this.imageId);
        if (polaroid.length == 0) return;
        var original = $('.imageLink', polaroid);
        var link = original.clone();
        var preview = $('.contentPreview', this.canvas);
        preview.html(link);

        // Set current position to Polaroid.
        var pos = original.offset();
        var left = pos.left;
        var top = pos.top - $(window).scrollTop();

        link.attr('target', '_blank');
        link.css({
            position: 'absolute',
            left: left + 'px',
            top: top + 'px'
        });

        // Figure out and animate to new position.
        var w = polaroid.attr('data-w');
        var h = polaroid.attr('data-h');

        if (window.devicePixelRatio && window.devicePixelRatio > 1) {
            w /= window.devicePixelRatio;
            h /= window.devicePixelRatio;
        }

        var maxWidth = $('.left', this.canvas).position().left;
        var maxHeight = $(window).height() - 80;
        var windowWidth = maxWidth - 60;
        var windowHeight = $(window).height() - 180;
        var ratioOriginal = w / h;
        w = Math.min(w, windowWidth);
        h = w / ratioOriginal;
        if (h > maxHeight) {
            h = maxHeight;
            w = h * ratioOriginal;
        }
        w = Math.round(w);
        h = Math.round(h);

        var pos = this.getCenterPosition(w, h);
        var l = pos.x;
        var t = pos.y;

        setTimeout(function () {
            preview.addClass('active');
            link.css({
                left: l + 'px',
                top: t + 'px'
            });
            $('img', link).css({
                width: w + 'px',
                height: h + 'px'
            });
        }, 50);
    };

    this.getCenterPosition = function (w, h) {
        var mw = $('.left', this.canvas).position().left;
        var mh = $(window).height();
        return {
            x: Math.round((mw - w - 20) / 2),
            y: Math.round((mh - h - 20) / 2)
        };
    };

    this.updateContent = function () {
        this.canvas.attr('data-id', this.imageId);

        gbks.common.track('Polaroid', 'Lightbox', this.imageId);

        this.loadDetails();
    };

    this.updateContentOld = function () {
        var tile = this.activeTile;
        var id = tile.attr('id');
        var bits = id.split('_');
        id = bits[1];

        var img = $('img', tile);
        var title = img.attr('title');
        var source = img.attr('src');
        bits = source.split('/');
        img = bits[bits.length - 1];

        var w = tile.attr('data-w');
        var h = tile.attr('data-h');

        var windowWidth = $(window).width() - 160;
        var windowHeight = $(window).height() - 180;

        var ratioOriginal = w / h;
        w = Math.min(w, windowWidth);
        h = w / ratioOriginal;

        /**
        var windowRatio = windowWidth/windowHeight;
    
        if(ratioOriginal > windowRatio) {
        w = Math.min(w, windowWidth);
        h = w/ratioOriginal;
        } else {
        h = Math.min(h, windowHeight);
        w = h*ratioOriginal;
        }
        //*/

        w = Math.round(w);
        h = Math.round(h);

        var link = $($('a', tile)[0]);
        var href = link.attr('href');

        var html = '<a href="' + href + '"><img src="" width="' + w + '" height="' + h + '"></a>';

        if (title.length > 90) title = title.substr(0, 87) + '...';
        html += '<h3 style="width:' + w + 'px;"><a href="' + href + '">' + title + '</a></h3>';
        $('.image .wrap', this.canvas).html(html);

        //$('.image .wrap', this.canvas).css('width', w+'px');

        this.canvas.attr('data-id', id);

        //$('.image', this.lightbox).css('margin-left', Math.round((windowWidth-w)/2)+'px');
        //$('.image', this.lightbox).css('margin-top', Math.round((windowHeight-h)/2)+'px');
        /**
        $('.image', this.lightbox).css({
        'margin-left': Math.round((windowWidth-w)/2+10)+'px',
        'margin-top': Math.round((windowHeight-h)/2-30)+'px'
        });
        //*/

        var bigImage = $('.image .wrap a img', this.canvas);
        if (bigImage.length > 0) {
            console.log('1');
            bigImage.attr('src', '/images/original/' + img).load($.proxy(this.onImageLoaded, this));
        } else {
            console.log('2');
            this.onImageLoaded();
        }

        gbks.common.track('Polaroid', 'Lightbox', id);

        this.loadDetails();
    };

    this.onImageLoaded = function () {
        console.log('onImageLoaded');
        /**
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var image = $('.image', this.lightbox);
        $('.image', this.lightbox).css({
        'margin-left': Math.round((windowWidth-image.width())/2)+'px',
        'margin-top': Math.round((windowHeight-image.height())/2)+'px'
        });
        //*/
        var link = $('.image a', this.canvas);
        var img = $('img', link);
        var pos = this.getCenterPosition(img.width(), img.height());
        link.css({
            position: 'absolute',
            left: pos.x + 'px',
            top: pos.y + 'px'
        });
        img.css('margin', 0);

        this.canvas.removeClass('loading');

        var img = $('.image .wrap a img', this.canvas);
        var h = parseInt(img.attr('height'));
        if (h == 0) {
            img.attr('height', '');
        }

        $('.image .wrap', this.canvas).fadeTo(250, 1);
        $('.details', this.canvas).fadeTo(250, 1);
        $('.loader', this.canvas).fadeOut(250);
    };

    this.loadDetails = function () {
        // Make call.
        //var bits = this.imageId.split('_');
        //imageId = bits[1];
        this.canvas.addClass('loading');
        $.ajax({
            url: '/lightbox/get?imageId=' + this.imageId,
            dataType: 'jsonp',
            type: 'POST',
            success: $.proxy(this.onLoadDetails, this)
        });
    };

    this.resizeImage = function () {
        var image = $('.image .wrap img', this.canvas);
        if (image.length == 0) image = $('.image .wrap iframe');
        var w = image.attr('width');
        var h = image.attr('height');

        var maxWidth = $('.image', this.canvas).width();
        var maxHeight = $(window).height() - 80;

        var windowWidth = maxWidth - 60;
        var windowHeight = $(window).height() - 180;

        var ratioOriginal = w / h;
        w = Math.min(w, windowWidth);
        h = w / ratioOriginal;

        if (h > maxHeight) {
            h = maxHeight;
            w = h * ratioOriginal;
        }

        w = Math.round(w);
        h = Math.round(h);

        //$('.image .wrap', this.canvas).css('width', w+'px');

        image.css({
            width: w + 'px',
            height: h + 'px'
        });
    };

    this.onLoadDetails = function (result) {
        if (result && result.html) {
            $('.contentWrap', this.canvas).html(result.html);

            this.resizeImage();

            gbks.common.history.push(result.history, result.title);

            $('.details .expander', this.canvas).click($.proxy(this.onClickExpand, this));

            $('.details #addImageButton', this.canvas).click($.proxy(this.onClickSaveImage, this));
            $('.details #addNoteButton', this.canvas).click($.proxy(this.onClickAddNote, this));
            $('.details #likeImageButton', this.canvas).click($.proxy(this.onClickLikeImage, this));
            $('.details #unlikeImageButton', this.canvas).click($.proxy(this.onClickLikeImage, this));
            $('.details #shareImageButton', this.canvas).click($.proxy(this.onClickShareImage, this));

            var bigImage = $('.image .wrap a img', this.canvas);
            if (bigImage.length > 0) {
                bigImage.load($.proxy(this.onImageLoaded, this));
            } else {
                this.onImageLoaded();
            }
        }
    };

    this.onClickAddNote = function (event) {
        event.stopPropagation();
        event.preventDefault();

        var button = $(event.currentTarget);

        if (this.commentPopup) {
            button.removeClass('active');
            this.commentPopup.hide();
            this.commentPopup = null;
        } else {
            button.addClass('active');
            var imageId = this.canvas.attr('data-id');
            this.commentPopup = new gbks.common.CommentPopup();
            this.commentPopup.display(imageId, $(event.currentTarget), $.proxy(this.onCommentAdded, this));
        }
    };

    this.onCommentAdded = function (data) {
        $('#addNoteButton', this.canvas).removeClass('active');
        var comments = $('.comments', this.canvas);
        if (comments.length > 0) {
            comments.append(data);
            comments.removeClass('blank');
        } else {
            comments = $('<div class="comments">' + data + '</div>');
            $('.info', this.canvas).append(comments);
        }
    };

    this.onClickExpand = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var target = $(event.currentTarget);
        var holder = $(target.parents('p')[0]);
        var expand = $('.expand', holder);
        var hidden = $('.hidden', holder);
        expand.hide();
        hidden.show();
    };

    this.onResize = function (event) {
        return;
        if (this.canvas) {
            var windowWidth = $(window).width() - 80;
            var windowHeight = $(window).height() - 180;
            var img = $('.image img', this.canvas);
            var w = img.width();
            var h = img.height();
            $('.image', this.canvas).css({
                'margin-left': Math.round((windowWidth - w) / 2 + 10) + 'px',
                'margin-top': Math.round((windowHeight - h) / 2 - 30) + 'px'
            });
        }
    };

    this.hideSharePopup = function () {
        if (this.sharePopup) {
            this.sharePopup.hide();
            this.sharePopup = null;
        }
    };

    this.onClickShareImage = function (event) {
        event.stopPropagation();
        event.preventDefault();

        var button = $(event.currentTarget);
        var imageId = button.attr('data-id');

        this.hideSharePopup();

        if (button.hasClass('active')) {
            //sharing.removeClass('active');
            button.removeClass('active');
        } else {
            //sharing.addClass('active');
            button.addClass('active');

            this.sharePopup = new gbks.common.SharePopup();
            this.sharePopup.display(imageId, button, $.proxy(this.onHideSharePopup, this));
        }
    };

    this.onHideSharePopup = function (event) {
        if (this.sharePopup) {
            this.sharePopup.hide();
            this.sharePopup = null;
        }
        $('#shareImageButton', this.canvas).removeClass('active');
    };

    this.onClickSaveImage = function (event) {
        event.stopPropagation();
        event.preventDefault();

        var button = $(event.currentTarget);
        var imageId = button.attr('data-id');
        //this.toggleSaveButton(true);

        button.addClass('active');
        button.addClass('loading');

        gbks.common.track('Image', 'Save', imageId);

        $.ajax({
            url: '/bookmark/savetouser?imageId=' + imageId,
            type: 'POST',
            dataType: 'jsonp',
            success: $.proxy(this.onAddImageComplete, this)
        });
    };

    this.onAddImageComplete = function (result) {
        var saveButton = $('.details #addImageButton', this.canvas);
        saveButton.removeClass('loading');

        this.savePopup = new gbks.common.SavePopup();
        this.savePopup.init(saveButton, result, $.proxy(this.onClickRemoveImage, this), $.proxy(this.onCloseSavePopup, this));
    };

    this.onCloseSavePopup = function (event) {

    };

    this.onClickRemoveImage = function () {
        var saveButton = $('.details #addImageButton', this.canvas);
        saveButton.removeClass('active');
    };

    this.onClickLikeImage = function (event) {
        event.stopPropagation();
        event.preventDefault();

        var button = $(event.currentTarget);
        var imageId = button.attr('data-id');
        var isLike = !button.hasClass('active');

        if (isLike) button.addClass('active');
        else button.removeClass('active');

        button.addClass('loading');
        gbks.common.track('Image', 'like', imageId);

        $.ajax({
            url: '/likes/like',
            data: { imageId: imageId },
            type: 'POST',
            success: $.proxy(this.onLikeComplete, this),
            error: $.proxy(this.onLikeComplete, this)
        });
    };

    this.onLikeComplete = function (result) {
        $('.details #likeImageButton', this.canvas).removeClass('loading');
        $('.details #unlikeImageButton', this.canvas).removeClass('loading');
    };

};

/** Popup helpers **/

gbks.common.wrapPopupContent = function (id, html) {
    var result = '<div id="' + id + '" class="popupWrap">';
    result += '<div class="arrow"><img class="up" width="29" height="17" src="/assets/overlay-arrow-up.png"><img class="down" width="29" height="17" src="/assets/overlay-arrow-down.png"></div>';
    result += html;
    result += '</div>';
    return result;
};

gbks.common.positionPopup = function (popup, target) {
    var docWidth = $(window).width();

    var pos = target.offset();
    var overlayWidth = popup.width();
    var left = Math.round(pos.left + target.width() / 2 - overlayWidth / 2) - 2;
    var right = left + overlayWidth;
    var docRightDelta = docWidth - 10 - right;
    var arrowLeft = overlayWidth / 2;

    if (docRightDelta < 0) {
        left += docRightDelta;
        arrowLeft -= docRightDelta;
    }
    if (left < 0) {
        left -= left - 10;
        arrowLeft += left - 10;
    }

    // Check if we're in the lower half of the screen.
    var windowHeight = $(window).height();
    var center = windowHeight / 2;
    var buttonY = target.offset().top - window.pageYOffset + target.height() / 2;
    var deltaTop = buttonY;
    var deltaBottom = windowHeight - deltaTop;
    var showTop = (deltaTop > deltaBottom);
    if (showTop) {
        popup.addClass('topper');
    } else {
        popup.removeClass('topper');
    }

    // Apply positioning.
    var top = pos.top + target.height() + 17;

    if (showTop) {
        top = pos.top - 17 - popup.height();
    }

    popup.css({
        left: left + 'px',
        top: top + 'px'
    });

    $('.arrow', popup).css('left', arrowLeft + 'px');
};

/**
* Share popup
*/
gbks.common.sharePopupInstance = null;
gbks.common.SharePopup = function () {

    if (gbks.common.sharePopupInstance) {
        gbks.common.sharePopupInstance.hide();
        gbks.common.sharePopupInstance = null;
    }
    gbks.common.sharePopupInstance = this;

    this.canvas = null;
    this.element = null;
    this.target = null;
    this.hideCallback = false;
    this.resizeMethod = $.proxy(this.updateLayout, this);

    this.display = function (imageId, target, callback) {
        this.imageId = imageId;
        this.element = target;
        this.hideCallback = callback;

        this.clickDocumentMethod = $.proxy(this.onClickDocument, this);

        $(window).resize(this.resizeMethod);

        this.createCanvas();
        this.updatePosition();
        this.canvas.show();

        $(document).click(this.clickDocumentMethod);
    };

    this.hide = function () {
        $(document).unbind('click', this.clickDocumentMethod);
        $(window).unbind('resize', this.resizeMethod);

        if (this.canvas) {
            this.canvas.hide();
            this.canvas.remove();
            this.canvas = null;
        }
    };

    this.onClickDocument = function (event) {
        var target = $(event.target);
        var parents = target.parents('#sharePopup');
        if (parents.length == 0) {
            this.hide();

            if (this.hideCallback) {
                this.hideCallback(event);
                this.hideCallback = null;
            }

            event.stopPropagation();
            event.preventDefault();
        }
    };

    this.createCanvas = function () {
        if (this.canvas) {
            this.canvas.remove();
            this.canvas = null;
        }

        var html = '<ol class="options">';
        html += '<li class="facebook"><a href="/share/facebook?imageId=' + this.imageId + '" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Facebook"></span>Facebook</a></li>';
        html += '<li class="pinterest"><a href="/share/pinterest?imageId=' + this.imageId + '" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Pinterest"></span>Pinterest</a></li>';
        html += '<li class="twitter"><a href="/share/twitter?imageId=' + this.imageId + '" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Twitter"></span>Twitter</a></li>';
        html += '<li class="tumblr"><a href="/share/tumblr?imageId=' + this.imageId + '" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Tumblr"></span>Tumblr</a></li>';
        html += '<li class="embed"><a href="/embed/image/' + this.imageId + '" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Embed"></span>Embed</a></li>';
        html += '</ol>';
        html = gbks.common.wrapPopupContent('sharePopup', html);

        /**
        var html = '<div id="sharePopup">';
        html += '<div class="arrow"><img class="up" width="29" height="17" src="/assets/overlay-arrow-up.png"><img class="down" width="29" height="17" src="/assets/overlay-arrow-down.png"></div>';
        html += '<ol class="options">';
        html += '<li class="facebook"><a href="/share/facebook?imageId='+this.imageId+'" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Facebook"></span>Facebook</a></li>';
        html += '<li class="pinterest"><a href="/share/pinterest?imageId='+this.imageId+'" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Pinterest"></span>Pinterest</a></li>';
        html += '<li class="twitter"><a href="/share/twitter?imageId='+this.imageId+'" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Twitter"></span>Twitter</a></li>';
        html += '<li class="tumblr"><a href="/share/tumblr?imageId='+this.imageId+'" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Tumblr"></span>Tumblr</a></li>';
        html += '<li class="embed"><a href="/embed/image/'+this.imageId+'" target="_blank"><span><img src="/assets/icons/shareIcons.png" width="100" height="60" alt="Embed"></span>Embed</a></li>';
        html += '</ol>';
        html += '</div>';
        //*/

        this.canvas = $(html);
        $('body').append(this.canvas);

        $('li', this.canvas).click($.proxy(this.onClickItem, this));
    };

    this.onClickItem = function (event) {
        //this.hide();
    };

    this.updatePosition = function () {
        gbks.common.positionPopup(this.canvas, this.element);
        /**
        var docWidth = $(window).width();
    
        var pos = this.element.offset();
        var overlayWidth = this.canvas.width();
        var left = Math.round(pos.left + this.element.width()/2 - overlayWidth/2) - 2;
        var right = left + overlayWidth;
        var docRightDelta = docWidth - 10 - right;
        var arrowLeft = overlayWidth/2;
    
        if(docRightDelta < 0) {
        left += docRightDelta;
        arrowLeft -= docRightDelta;
        }
        if(left < 0) {
        left -= left - 10;
        arrowLeft += left - 10;
        }
    
        // Check if we're in the lower half of the screen.
        var windowHeight = $(window).height();
        var center = windowHeight/2;
        var buttonY = this.element.offset().top - window.pageYOffset + this.element.height()/2;
        var deltaTop = buttonY;
        var deltaBottom = windowHeight-deltaTop;
        var showTop = (deltaTop > deltaBottom);
        if(showTop) {
        this.canvas.addClass('topper');
        } else {
        this.canvas.removeClass('topper');
        }
    
        // Apply positioning.
        var top = pos.top + this.element.height() + 17;
    
        if(showTop) {
        top = pos.top - 17 - this.canvas.height();
        }
    
        this.canvas.css({
        left: left+'px',
        top: top+'px'
        });
    
        $('.arrow', this.canvas).css('left', arrowLeft+'px');
        //*/
    };
};


/**
* Comment popup
*/
gbks.common.commentPopupInstance = null;
gbks.common.CommentPopup = function () {

    if (gbks.common.commentPopupInstance) {
        gbks.common.commentPopupInstance.hide();
        gbks.common.commentPopupInstance = null;
    }
    gbks.common.commentPopupInstance = this;

    this.canvas = null;
    this.element = null;
    this.target = null;
    this.callback = false;
    this.resizeMethod = $.proxy(this.updateLayout, this);

    this.display = function (imageId, target, callback) {
        this.imageId = imageId;
        this.element = target;
        this.callback = callback;

        this.clickDocumentMethod = $.proxy(this.onClickDocument, this);

        $(window).resize(this.resizeMethod);

        this.createCanvas();
        this.updatePosition();
        this.canvas.show();

        $(document).click(this.clickDocumentMethod);
    };

    this.hide = function () {
        $(document).unbind('click', this.clickDocumentMethod);
        $(window).unbind('resize', this.resizeMethod);

        if (this.canvas) {
            this.canvas.hide();
            this.canvas.remove();
            this.canvas = null;
        }
    };

    this.onClickDocument = function (event) {
        var target = $(event.target);
        var parents = target.parents('#commentPopup');
        if (parents.length == 0) {
            this.hide();

            event.stopPropagation();
            event.preventDefault();
        }
    };

    this.createCanvas = function () {
        if (this.canvas) {
            this.canvas.remove();
            this.canvas = null;
        }

        var html = '<textarea type="text" name="comment" autocomplete="off"/>';
        html += '<input type="submit" name="send" value="Add note"/>';
        html = gbks.common.wrapPopupContent('commentPopup', html);

        this.canvas = $(html);
        $('body').append(this.canvas);

        $('input[type=submit]', this.canvas).click($.proxy(this.onClickSubmit, this));
        $('input[type=text]', this.canvas).focus();
    };

    this.onClickSubmit = function (event) {
        event.stopPropagation();
        event.preventDefault();

        var textarea = $('textarea', this.comment);
        var comment = textarea.val();
        var lower = comment.toLowerCase();
        var isGood = true;
        if (lower == 'test') isGood = false;
        if (comment == textarea.attr('placeholder')) isGood = false;
        if (lower.length < 3) isGood = false;

        if (lower == 'test') {
            alert('Hooray! Your test worked!');
        } else if (isGood) {
            this.canvas.removeClass('error');
            var data = { imageId: this.imageId, comment: comment, format: 'big' };
            $('input', this.canvas).attr('disabled', 'disabled');
            $.ajax({
                url: '/comment/add',
                data: data,
                type: 'POST',
                success: $.proxy(this.onSubmitComment, this)
            });
        } else {
            this.canvas.addClass('error');
            alert('Please ensure your comment is more than 3 characters.');
        }
    };

    this.onSubmitComment = function (data, textStatus, jqXHR) {
        console.log('onSubmitComment', data, this.callback);
        if (this.callback) {
            this.callback(data);
            this.callback = null;
        }
        this.hide();
    };

    this.updatePosition = function () {
        gbks.common.positionPopup(this.canvas, this.element);
    };
};

/** Cookie helper **/

gbks.common.Cookie = function (key, value, options) {
    // key and at least value given, set cookie...
    if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
        options = $.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var decode = options.raw ? function (s) { return s; } : decodeURIComponent;

    var pairs = document.cookie.split('; ');
    for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
        if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
    }
    return null;
};

gbks.common.Shortcuts = function () {

    this.init = function () {
        this.keyUpMethod = $.proxy(this.onKeyUp, this);
        $(document).bind('keyup', this.keyUpMethod);
    };

    this.onKeyUp = function (event) {
        var focused = $('*:focus');
        if (focused.length > 0) return;
        switch (event.which) {
            case 83: // s
                var c = 'fadeSaved';
                var b = $('body');
                if (b.hasClass(c)) b.removeClass(c);
                else b.addClass(c);
                break;
            case 71: // g
                this.toggleGroupOverlay();
                break;
            case 73: // i
                this.toggleTileInfo();
                break;
        }
    }

    this.toggleTileInfo = function () {
        var b = $('body'), c = 'hideTileInfo';
        if (b.hasClass(c)) b.removeClass(c);
        else b.addClass(c);

        if (gbks.tilesInstance) {
            gbks.tilesInstance.layout();
        }
    }

    this.toggleGroupOverlay = function () {
        if (this.groupsOverlay) {
            if (this.groupsOverlay.is(":visible")) {
                this.groupsOverlay.hide();
            } else {
                this.groupsOverlay.show();
            }
        } else {
            this.loadGroupOverlay();
        }
    };

    this.loadGroupOverlay = function () {
        if (!this.groupsOverlay) {
            this.groupsOverlay = $('<div id="quickGroupNav"><div class="wrap"><div class="wrap2"><div class="content clearfix"></div></div></div></div>');
            this.groupsOverlay.click($.proxy(this.onClickOverlay, this));
            $('body').append(this.groupsOverlay);
            $('.content', this.groupsOverlay).load('/autocomplete/quicknav', $.proxy(this.onLoadGroupOverlay, this));
        }
    };

    this.onClickOverlay = function (event) {
        var target = $(event.currentTarget);
        var isContent = (target.parents('#quickGroupNav .content').length > 0);
        if (!isContent) {
            this.toggleGroupOverlay();
        }
    };

    this.onLoadGroupOverlay = function () {
        this.groupsOverlay.addClass('loaded');
    };

};
gbks.common.shortcutInstance = new gbks.common.Shortcuts();
gbks.common.shortcutInstance.init();

// History helper.

gbks.common.history = gbks.common.history || {};
gbks.common.history.push = function (url, title) {
    if (gbks.common.history.supported()) {
        history.pushState({ url: url, title: title }, title, url);
        return true;
    }
    return false;
};

gbks.common.history.onChange = function (event) {
    var state = event.state;
};

gbks.common.history.supported = function () {
    return (typeof history.pushState !== 'undefined');
};

// Helper for smooth scrolling.
gbks.common.scroller = gbks.common.scroller || {};
gbks.common.scroller.scrollToPosition = function (position) {
    gbks.common.scroller.scrollInfo = {
        startTime: new Date().getTime(),
        startValue: window.pageYOffset,
        endValue: position,
        duration: 1500,
        lastUpdate: new Date().getTime()
    }

    $(window).unbind('mousewheel', gbks.common.scroller.mousewheelFunction);
    $(window).bind('mousewheel', gbks.common.scroller.mousewheelFunction);

    clearTimeout(gbks.common.scroller.scrollInterval);
    gbks.common.scroller.scrollInterval = setTimeout($.proxy(gbks.common.scroller.onScrollInterval, gbks.common.scroller), 25);
};

gbks.common.scroller.onScrollInterval = function (event) {
    var info = gbks.common.scroller.scrollInfo;
    var delta = new Date().getTime() - info.startTime;
    delta = Math.min(delta, info.duration);
    var pos = gbks.common.scroller.easeInOutCubic(null, delta, info.startValue, info.endValue - info.startValue, info.duration);

    window.scrollTo(0, pos);

    if (Math.abs(delta) < info.duration) {
        var timePassed = new Date().getTime() - info.lastUpdate;
        var timer = Math.max(5, 25 - timePassed);

        clearTimeout(gbks.common.scroller.scrollInterval);
        gbks.common.scroller.scrollInterval = setTimeout($.proxy(gbks.common.scroller.onScrollInterval, gbks.common.scroller), timer);
    } else {
        $(window).unbind('mousewheel', gbks.common.scroller.mousewheelFunction);
    }

    info.lastUpdate = new Date().getTime();
};
gbks.common.scroller.onMouseWheel = function (event) {
    $(window).unbind('mousewheel', gbks.common.scroller.mousewheelFunction);
    clearInterval(gbks.common.scroller.scrollInterval);
};
gbks.common.scroller.easeInOutCubic = function (x, t, b, c, d) {
    if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
    return c / 2 * ((t -= 2) * t * t + 2) + b;
};
gbks.common.scroller.mousewheelFunction = $.proxy(gbks.common.scroller.onMouseWheel, gbks.common.scroller);

/** iPad specific stuff **/
gbks.common.iPad = gbks.common.iPad || {};
gbks.common.iPad.isWebApp = function () {
    return (("standalone" in window.navigator) && window.navigator.standalone);
}
gbks.common.iPad.captureLinks = function () {
    $('a').live('click', $.proxy(gbks.common.iPad.captureLink, gbks.common.iPad));
}
gbks.common.iPad.captureLink = function (event) {
    var link = $(event.currentTarget);
    //var target = link.attr('target');
    //if(target == undefined || target == "" || target == "_self" || target == "_blank") {
    //get the destination of the link clicked
    var dest = link.attr("href");

    // Only interfere with links on Wookmark
    var t = 'http://www.wookmark.com';
    if (dest.substring(0, t.length) == t) {
        //prevent default behavior (opening safari)
        event.preventDefault();
        //update location of the web app
        self.location = dest;
    }
    //}
}
gbks.common.iPad.detect = function () {
    if (navigator.userAgent.match(/iPad/i) != null) {
        $('body').addClass('iPad');

        // If it's a web app, capture all link clicks.
        if (gbks.common.iPad.isWebApp()) {
            gbks.common.iPad.captureLinks();
        }
    }
}
gbks.common.iPad.detect();

var wk_header = null;
$(document).ready(function () {
    wk_header = new gbks.Header();
    wk_header.init();

    // Log all Javascript errors in GA.
    $(window).bind('onerror', gbks.common.onWindowError);
});

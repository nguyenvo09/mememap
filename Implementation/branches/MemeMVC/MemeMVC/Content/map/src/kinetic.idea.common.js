/*global MAPJS, Color, _, jQuery, Kinetic*/
/*jslint nomen: true, newcap: true, browser: true*/

    'use strict';
    /*shamelessly copied from http://james.padolsey.com/javascript/wordwrap-for-javascript */
    var COLUMN_WORD_WRAP_LIMIT = 25;
    function ExtractYoutubeVideoId(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    function IsYoutubeVideo(imageSrc) {
        if (
            (imageSrc.indexOf("https://www.youtube.com") == 0||
            imageSrc.indexOf("http://www.youtube.com") == 0||
            imageSrc.indexOf("www.youtube.com") == 0
            )

            && ExtractYoutubeVideoId(imageSrc) != "") {
            return true;

        }
        return false;
    }

    function wordWrap(str, width, brk, cut) {
        brk = brk || '\n';
        width = width || 75;
        cut = cut || false;
        if (!str) {
            return str;
        }
        var regex = '.{1,' + width + '}(\\s|$)' + (cut ? '|.{' + width + '}|.+$' : '|\\S+?(\\s|$)');
        return str.match(new RegExp(regex, 'g')).join(brk);
    }
    function breakWords(string) {
        var lines = string.split('\n'),
			formattedLines = _.map(lines, function (line) {
			    return wordWrap(line, COLUMN_WORD_WRAP_LIMIT, '\n', false);
			});
        return formattedLines.join('\n');
    }
    function createLink() {
        var link = new Kinetic.Group(),
			rectProps = {
			    width: 10,
			    height: 20,
			    rotation: 0.6,
			    stroke: '#bbbbbb',
			    strokeWidth: 2,
			    cornerRadius: 6,
			    shadowOffset: [0, 0],
			    shadow: '#ffffff',
			    shadowBlur: 0,
			    shadowOpacity: 0
			},
			rect = new Kinetic.Rect(rectProps),
			rect2 = new Kinetic.Rect(rectProps);
        rect2.setX(7);
        rect2.setY(-7);
        link.add(rect);
        link.add(rect2);
        link.setActive = function (isActive) {
            rect2.setStroke(isActive ? 'black' : '#555555');
            rect.setStroke(rect2.getStroke());
            link.getLayer().draw();
        };
        return link;
    }
    function createPin(color, xpos,ypos) {
        var configpin = { draggable: true }
        var pin = new Kinetic.Group(configpin),
		cirProps1 = {
		    radius: 12,
		    strokeWidth: 2,
		    stroke: color,
		    x: xpos,
		    y: ypos,
		    fill: '#FFFFFF',
		},
        cirProps2 = {
            radius: 10,
            fill:color,
            x: xpos,
            y: ypos
        }
        ,
		circle1 = new Kinetic.Circle(cirProps1),
		circle2 = new Kinetic.Circle(cirProps2);

        pin.add(circle1);
        pin.add(circle2);
        pin.setActive = function (isActive) {
            circle1.setStroke(isActive ? '#555555' : color);
            //circle2.fill(isActive ? 'black' : '#F7C520');
            pin.getLayer().draw();
        };
        pin.pointCircle = circle1;
        pin.pointCircle2 = circle2;

        pin.getXpos = function () {            
            return this.pointCircle.getX();
        };
        pin.getYpos = function () {
            return this.pointCircle.getY();
        };
        pin.getRadius = function () {
            return this.pointCircle.radius;
        };

        pin.MoveTop = function () {
            //alert(this.pointCircle);
            //alert(this.pointCircle);
            this.pointCircle.moveToTop();
            this.pointCircle2.moveToTop();
        };
        
        return pin;
    }

    function createClip() {
        //var group, clip, props = {width: 5, height: 25, radius: 3, rotation: 0.1, strokeWidth: 2, clipTo: 10};
        var group, clip, props = { width: 5, height: 25, radius: 3, rotation: 0.1, strokeWidth: 1, clipTo: 10 };

        group = new Kinetic.Group();
        group.getClipMargin = function () {
            return props.clipTo;
        };
        group.add(new Kinetic.Clip(_.extend({ stroke: 'darkslategrey', x: 1, y: 1 }, props)));
        clip = new Kinetic.Clip(_.extend({ stroke: 'skyblue', x: 0, y: 0 }, props));
        group.add(clip);
        group.on('mouseover', function () {
            clip.setStroke('black');
            group.getLayer().draw();
        });
        group.on('mouseout', function () {
            clip.setStroke('skyblue');
            group.getLayer().draw();
        });
        return group;
    }
    function createIcon() {
        var icon = new Kinetic.Image({
            x: 0,
            y: 0,
            width: 0,
            height: 0
        });
        icon.oldDrawScene = icon.drawScene;
        icon.updateMapjsAttribs = function (iconHash) {
            var safeIconProp = function (name) {
                return iconHash && iconHash[name];
            },
				imgUrl = safeIconProp('url'),
				imgWidth = safeIconProp('width'),
				imgHeight = safeIconProp('height');
            if (this.getAttr('image') && this.getAttr('image').src !== imgUrl) {
                this.getAttr('image').src = imgUrl || '';
            }
            this.setAttr('mapjs-image-url', imgUrl);
            if (this.getAttr('width') !== imgWidth) {
                this.setAttr('width', imgWidth);
            }
            if (this.getAttr('height') !== imgHeight) {
                this.setAttr('height', imgHeight);
            }
            this.setVisible(imgUrl);
        };
        icon.initMapjsImage = function () {
            var self = this,
				imageSrc = this.getAttr('mapjs-image-url');
            if (!imageSrc) {
                return;
            }
            if (!this.getAttr('image')) {
                this.setAttr('image', new Image());
                this.getAttr('image').onload = function loadImage() {
                    self.getLayer().draw();
                };
                this.getAttr('image').src = imageSrc;
            }
        };
        icon.drawScene = function () {
            if (!this.getAttr('image')) {
                this.initMapjsImage();
            }
            if (this.getAttr('mapjs-image-url')) {
                this.oldDrawScene.apply(this, arguments);
            }
        };
        return icon;
    }

   
/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

(function () {
	'use strict';
	var horizontalConnector, calculateConnector, calculateConnectorInner;
	Kinetic.Connector = function (config) {
		this.shapeFrom = config.shapeFrom;
		this.shapeTo = config.shapeTo;
		this.shapeType = 'Connector';
		this.getLineWidth = function () {
		    var width =
                ((this.shapeFrom.getX() - this.shapeTo.getX()) * (this.shapeFrom.getX() - this.shapeTo.getX()) +
                (this.shapeFrom.getY() - this.shapeTo.getY()) * (this.shapeFrom.getY() - this.shapeTo.getY())) / 80000;
		    if (width > 12) width = 12;
		    if (width < 5) width = 5;
		    return width;
		}
		this.getLineColor = function () {
		    var color =
                ((this.shapeFrom.getX() - this.shapeTo.getX()) * (this.shapeFrom.getX() - this.shapeTo.getX()) +
                (this.shapeFrom.getY() - this.shapeTo.getY()) * (this.shapeFrom.getY() - this.shapeTo.getY())) / 80000;
		    if (color > 30) color = 30;
		    return color;
		}
		Kinetic.Shape.call(this, config);
		this.sceneFunc(this._sceneFunc);
		////this._setDrawFuncs();
	};
	horizontalConnector = function (parentX, parentY, parentWidth, parentHeight,
			childX, childY, childWidth, childHeight) {
		var childHorizontalOffset = parentX < childX ? 0.1 : 0.9,
			parentHorizontalOffset = 1 - childHorizontalOffset;
		return {
			from: {
				x: parentX + 0.5 * parentWidth,
				y: parentY + 0.5 * parentHeight
			},
			to: {
			    x: childX + 0.5 * childWidth,
				y: childY + 0.5 * childHeight
			},
			controlPointOffset: 0
		};
	};
	calculateConnector = function (parent, child) {
	    if (parent != undefined && child != undefined && parent != null && child != null) {
	        return calculateConnectorInner(parent.getX(), parent.getY(), parent.getWidth(), parent.getHeight(),
                child.getX(), child.getY(), child.getWidth(), child.getHeight());
	    } else {
	        return null;
	    }
	};
	calculateConnectorInner = _.memoize(function (parentX, parentY, parentWidth, parentHeight,
			childX, childY, childWidth, childHeight) {
		var tolerance = 10,
			childMid = childY + childHeight * 0.5,
			parentMid = parentY + parentHeight * 0.5,
			childHorizontalOffset;
		if (Math.abs(parentMid - childMid) + tolerance < Math.max(childHeight, parentHeight * 0.75)) {
			return horizontalConnector(parentX, parentY, parentWidth, parentHeight, childX, childY, childWidth, childHeight);
		}
		childHorizontalOffset = parentX < childX ? 0 : 1;
		return {
			from: {
				x: parentX + 0.5 * parentWidth,
				y: parentY + 0.5 * parentHeight
			},
			to: {
			    x: childX + 0.5 * childWidth,
				y: childY + 0.5 * childHeight
			},
			controlPointOffset: 0.25
		};
	}, function () {
		return Array.prototype.join.call(arguments, ',');
	});
	Kinetic.Connector.prototype = {
	    isVisible: function (offset) {
	        
	        var stage = this.getStage(),
				conn = calculateConnector(this.shapeFrom, this.shapeTo);
	        if (conn==null || !conn || conn.from == null || conn.to == null) return false;
            var	x = Math.min(conn.from.x, conn.to.x),
				y = Math.min(conn.from.y, conn.to.y),
				rect = new MAPJS.Rectangle(x, y, Math.max(conn.from.x, conn.to.x) - x, Math.max(conn.from.y, conn.to.y) - y);
			return stage && stage.isRectVisible(rect, offset);
		},
	    _sceneFunc: function (context) {
			var 
				shapeFrom = this.shapeFrom,
				shapeTo = this.shapeTo,
				conn,
				offset,
				maxOffset;
			if (!this.isVisible()) {
				return;
			}
			conn = calculateConnector(shapeFrom, shapeTo);
			if (!conn) {
				return;
			}
			context.beginPath();
			
			//offset = (conn.from.x - conn.to.x) * conn.controlPointOffset;
            
			//maxOffset = Math.min(shapeTo.getWidth(), shapeFrom.getHeight()) * 1.5;
			//offset = Math.max(-maxOffset, Math.min(maxOffset, offset));
			//var halfwayx = (conn.to.x - conn.from.x) / 2;
			//var halfwayy = (conn.to.y - conn.from.y) / 2;
			
			////context.lineJoin = context.lineCap = "round";
			//context.strokeStyle = "rgba(166,166,166,0.6)";
			
	        //context.strokeWidth = this.getLineWidth().toString();

			context.setAttr('strokeStyle', "#999999");
			context.setAttr('lineWidth', 1);

			/*context.moveTo(conn.from.x, conn.from.y);
			context.quadraticCurveTo(conn.from.x - offset, conn.from.y, conn.from.x + halfwayx, conn.from.y + halfwayy);
			context.moveTo(conn.from.x + halfwayx, conn.from.y + halfwayy);
			context.quadraticCurveTo(conn.to.x + offset, conn.to.y, conn.to.x, conn.to.y);
            */
			context.moveTo(conn.from.x, conn.from.y);
	        //context.quadraticCurveTo(conn.from.x - offset, conn.from.y, conn.to.x, conn.to.y);
			context.lineTo(conn.to.x, conn.to.y);
			context.stroke();
			
		    //canvas.stroke(this);
		}
	};
	Kinetic.Util.extend(Kinetic.Connector, Kinetic.Shape);
}());

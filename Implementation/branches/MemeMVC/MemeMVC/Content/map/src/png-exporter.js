/*jslint nomen: true*/
/*global _, jQuery, MAPJS, Kinetic */
MAPJS.pngExport = function (idea, mimeType) {
	'use strict';
	var deferred = jQuery.Deferred(),
		layout = MAPJS.calculateLayout(idea, MAPJS.KineticMediator.dimensionProvider),
		frame = MAPJS.calculateFrame(layout.nodes, 10),
		hiddencontainer = jQuery('<div></div>').css({'visibility': 'hidden', 'background-color': "rgb(239, 240, 240)"})
			.appendTo('body').width(frame.width).height(frame.height).attr('id', 'hiddencontainer'),
		hiddenstage = new Kinetic.Stage({ container: 'hiddencontainer' }),
		layer = new Kinetic.Layer(),
		backgroundLayer = new Kinetic.Layer(),
		nodeByIdeaId = {},
		bg = new Kinetic.Rect({
		    fill: '#EFF0F0',
			x: frame.left,
			y: frame.top,
			width: frame.width,
			height: frame.height
		});
	
	hiddenstage.add(backgroundLayer);
	backgroundLayer.add(bg);
	hiddenstage.add(layer);
	hiddenstage.setWidth(frame.width);
	hiddenstage.setHeight(frame.height);
	hiddenstage.setX(-1 * frame.left);
	hiddenstage.setY(-1 * frame.top);
	_.each(layout.nodes, function (n) {
		
	    var type = "rec";
	    var obj = undefined;
	    if (n.data != undefined && n.data != "") {
	        try {
	            n.data = n.data.replace(new RegExp('"="', 'g'), '":"');
	            obj = JSON.parse(n.data);

	            type = obj.type;

	        } catch (err) {
	            bootbox.alert(err.toString());
	            obj = {};
	            n.data = '{"type":"rec","imgsrc":""}';
	        }
	    } else {
	        obj = {};
	    }
	    var node = undefined;

	    node = new Kinetic.Idea({
	        level: n.level,
	        x: n.x,
	        y: n.y,
	        text: n.title,
	        mmAttr: n.attr,
	        opacity: 1,
	        id: 'node_' + n.id,
	        activated: n.activated,
	        data: obj,
	        NodeType: type,
	        editable: false
	    });
	    node.SetupIdea();
		nodeByIdeaId[n.id] = node;
		layer.add(node);
	});
	_.each(layout.connectors, function (n) {
		var connector = new Kinetic.Connector({
			shapeFrom: nodeByIdeaId[n.from],
			shapeTo: nodeByIdeaId[n.to],
			stroke: '#888',
			strokeWidth: 1
		});
		layer.add(connector);
		connector.moveToBottom();
	});
	_.each(layout.links, function (l) {
		var link = new Kinetic.Link({
			shapeFrom: nodeByIdeaId[l.ideaIdFrom],
			shapeTo: nodeByIdeaId[l.ideaIdTo],
			dashArray: [8, 8],
			stroke: '#800',
			strokeWidth: 1.5
		});
		layer.add(link);
		link.moveToBottom();
		link.setMMAttr(l.attr);
	});
	hiddenstage.draw();
	hiddenstage.toDataURL({
	    mimeType: mimeType,
		callback: function (url) {
			deferred.resolve(url);
			hiddencontainer.remove();
			if (mimeType === "image/jpeg") {
			    
			    var doc = new jsPDF();
			    doc.setFontSize(40);
			    //doc.text(35, 25, "map");
			    //w , h , if w == 0, we will scale it to height. 
			    doc.addImage(url, 'JPEG', 0, 0, 0, 50);
			    var res = doc.output('datauri');
			    //alert(res);
			    var link = document.createElement('a');
			    link.href = res;
			    link.download = 'map.pdf';
			    document.body.appendChild(link);
			    link.click();
			} else {
			    var link = document.createElement('a');
			    link.href = url;
			    link.download = 'map.png';
			    document.body.appendChild(link);
			    link.click();
			}
			
			//var doc = new jsPDF();

			//doc.setFontSize(40);
			//doc.text(35, 25, "map");
			//doc.addImage(url, 'JPEG', 15, 40, 180, 180);
			//var blobObject = new Blob([url]);
			
			//window.navigator.msSaveBlob(blobObject, 'map.png'); // The user only has the option of clicking the Save button.
			//var link = url.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
			//window.open(url);
			//document.write('<img src="' + url + '"/>');
		   
		}
	});
	var b64toBlob = function(b64Data, contentType) {
	    contentType = contentType || '';
	    var sliceSize = 512;
	    b64Data = b64Data.replace(/^[^,]+,/, '');
	    b64Data = b64Data.replace(/\s/g, '');
	    var byteCharacters = window.atob(b64Data);
	    var byteArrays = [];

	    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	        var slice = byteCharacters.slice(offset, offset + sliceSize);

	        var byteNumbers = new Array(slice.length);
	        for (var i = 0; i < slice.length; i++) {
	            byteNumbers[i] = slice.charCodeAt(i);
	        }

	        var byteArray = new Uint8Array(byteNumbers);

	        byteArrays.push(byteArray);
	    }

	    var blob = new Blob(byteArrays, { type: contentType });
	    return blob;
	}
	return deferred.promise();
};

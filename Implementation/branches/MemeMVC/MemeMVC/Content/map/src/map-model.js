/*jslint forin: true, nomen: true*/
/*global _, MAPJS, observable*/

var NodeIndexModel = function () {
    var self = this;
    self.Title = "";
    self.Id = "";
    self.RId = "";
    self.MapPositionX = 0;
    self.MapPositionY = 0;
    self.ImageUrl = "";
}


MAPJS.MapModel = function (layoutCalculator, editablemode, selectAllTitles, clipboardProvider) {
    'use strict';
    var self = this,

		clipboard = clipboardProvider || new MAPJS.MemoryClipboard(),
		analytic,
		currentLayout = {
		    nodes: {},
		    connectors: {}
		},
		idea,
        menu,
		isInputEnabled = true,
		isEditingEnabled = true,
		currentlySelectedIdeaId,
        currentlyHoveredIdea,

		activatedNodes = [],
		setActiveNodes = function (activated) {
		    var wasActivated = _.clone(activatedNodes);
		    if (activated.length === 0) {
		        activatedNodes = [currentlySelectedIdeaId];
		    } else {
		        activatedNodes = activated;
		    }
		    self.dispatchEvent('activatedNodesChanged', _.difference(activatedNodes, wasActivated), _.difference(wasActivated, activatedNodes));
		},
		horizontalSelectionThreshold = 300,
		moveNodes = function (nodes, deltaX, deltaY) {
		    if (deltaX || deltaY) {
		        _.each(nodes, function (node) {
		            node.x += deltaX;
		            node.y += deltaY;
		            self.dispatchEvent('nodeMoved', node);
		        });
		    }
		},
		isAddLinkMode,

		updateCurrentLayout = function (newLayout) {
		    
		    var nodeId, newNode, oldNode, newConnector, oldConnector, linkId, newLink, oldLink, newActive;
		    for (nodeId in currentLayout.connectors) {
		        newConnector = newLayout.connectors[nodeId];
		        oldConnector = currentLayout.connectors[nodeId];
		        if (!newConnector || newConnector.from !== oldConnector.from || newConnector.to !== oldConnector.to) {
		            self.dispatchEvent('connectorRemoved', oldConnector);
		        } else {
		            var toNode = newLayout.nodes[newConnector.to], fromNode = newLayout.nodes[newConnector.from];
		            if (toNode.isStandAlone === true && fromNode.level === 1) {
		                //console.log("connectorRemoved " + toNode.isStandAlone + " xxx " +  toNode.title);
		                self.dispatchEvent('connectorRemoved', oldConnector);
		            }
		        }
		    }

		    for (nodeId in currentLayout.nodes) {

		        oldNode = currentLayout.nodes[nodeId];
		        newNode = newLayout.nodes[nodeId];
		        if (!newNode) {
		            /*jslint eqeq: true, loopfunc: true*/
		            if (nodeId == currentlySelectedIdeaId) {
		                self.selectNode(idea.id);
		            }
		            newActive = _.reject(activatedNodes, function (e) { return e == nodeId; });
		            if (newActive.length !== activatedNodes.length) {
		                setActiveNodes(newActive);
		            }
		            self.dispatchEvent('nodeRemoved', oldNode, nodeId);
		        }
		    }
		    for (nodeId in newLayout.nodes) {
		        oldNode = currentLayout.nodes[nodeId];
		        newNode = newLayout.nodes[nodeId];
		        if (!oldNode) {
		            self.dispatchEvent('nodeCreated', newNode);
		        } else {
		            if (newNode.x !== oldNode.x || newNode.y !== oldNode.y) {

		                self.dispatchEvent('nodeMoved', newNode);
		            }
		            if (newNode.title !== oldNode.title) {
		                self.dispatchEvent('nodeTitleChanged', newNode);
		            }
		            if (!_.isEqual(newNode.attr || {}, oldNode.attr || {})) {
		                self.dispatchEvent('nodeAttrChanged', newNode);
		            }

		        }
		    }
		    for (nodeId in newLayout.connectors) {
		        newConnector = newLayout.connectors[nodeId];
		        oldConnector = currentLayout.connectors[nodeId];
		        //console.log("connector");
		        var toNode = newLayout.nodes[newConnector.to], fromNode = newLayout.nodes[newConnector.from];
		        if (toNode.isStandAlone === true && fromNode.level === 1) {
		            continue;
		        }

		        if (!oldConnector || newConnector.from !== oldConnector.from || newConnector.to !== oldConnector.to) {
                    //console.log("standAlone: " + toNode.isStandAlone + " : from" + fromNode.level)
		            self.dispatchEvent('connectorCreated', newConnector);
		           
		        } else {
		            var toNode = newLayout.nodes[newConnector.to], fromNode = newLayout.nodes[newConnector.from];
		            //var oldToNode = currentLayout.nodes[oldConnector.to];
		            if (toNode.isStandAlone === false && fromNode.level === 1) {
		                
		                //console.log("Here--- connectorCreated " + toNode.isStandAlone + " xxx " + toNode.title);
		                self.dispatchEvent('connectorCreated', newConnector);
		                //console.log("Created Node after join Root node : " + toNode.title);
		            }
		        }
		    }
		    for (linkId in newLayout.links) {
		        newLink = newLayout.links[linkId];
		        oldLink = currentLayout.links && currentLayout.links[linkId];
		        if (oldLink) {
		            if (!_.isEqual(newLink.attr || {}, (oldLink && oldLink.attr) || {})) {
		                self.dispatchEvent('linkAttrChanged', newLink);
		            }
		        } else {

		            self.dispatchEvent('linkCreated', newLink);
		        }
		    }
		    for (linkId in currentLayout.links) {
		        oldLink = currentLayout.links[linkId];
		        newLink = newLayout.links && newLayout.links[linkId];
		        if (!newLink) {
		            self.dispatchEvent('linkRemoved', oldLink);
		        }
		    }
		    currentLayout = newLayout;
		    self.dispatchEvent('layoutChangeComplete');

		    //After all nodes created, we have computed Left_X, right_X, top_Y, bottom_y
		    self.computeMinimumScaleDown();

		},
		revertSelectionForUndo,
		revertActivatedForUndo,
		editNewIdea = function (newIdeaId) {
		    revertSelectionForUndo = currentlySelectedIdeaId;
		    revertActivatedForUndo = activatedNodes.slice(0);
		    self.selectNode(newIdeaId);
		    self.editNode(false, true, true);
		},
		getCurrentlySelectedIdeaId = function () {
		    return currentlySelectedIdeaId || idea.id;
		},
		onIdeaChanged = function () {
		    //console.log("On Idea Changed");
		    revertSelectionForUndo = false;
		    revertActivatedForUndo = false;
		    updateCurrentLayout(self.reactivate(layoutCalculator(idea)));
		},
		currentlySelectedIdea = function () {
		    return (idea.findSubIdeaById(currentlySelectedIdeaId) || idea);
		},
		ensureNodeIsExpanded = function (source, nodeId) {
		    var node = idea.findSubIdeaById(nodeId) || idea;
		    if (node.getAttr('collapsed')) {
		        idea.updateAttr(nodeId, 'collapsed', false);
		    }
		};
    this.updateCenterofFocus = function (focus) {
        if (this.CenterofFocus < focus || focus == this.LEVELRELEASED) this.CenterofFocus = focus;
    };
    this.editable = editablemode;
    this.CenterofFocus = 0,
    this.LEVELNODE = 1;
    this.LEVELNODEPIN = 3;
    this.LEVELPIN = 4;
    this.LEVELMEDIA = 2;
    this.LEVELRELEASED = 0;
    this.indexedNodes = new Array();

    self.LEFT_X = 0, self.RIGHT_X = 0, self.TOP_Y = 0, self.BOTTOM_Y = 0;

    self.indexedNodesDict = {}, self.mapStage = undefined;

    /** @param node: An instance of Kinectic.Idea
        @param newRatio: scale ratio for zooming.
        @return: void
        Compute Left_X, right_X, top_Y, down_Y  based on the current "node" **/
    self.computeLRTD = function (node, newRatio) {

        if (newRatio == undefined) {
            newRatio = this.scaleRatio;
        }
        self.indexedNodesDict[node.attrs.id] = node;

        var canvasPosition = jQuery("#" + self.mapStage.getContainer().id).offset();

        var top = node.getY(),
            left = node.getX(),
            right = left + node.getWidth(),
            bottom = top + node.getHeight();

        if (self.LEFT_X > left) {
            self.LEFT_X = left;
        }
        if (self.RIGHT_X < right) {
            self.RIGHT_X = right;
        }
        if (self.TOP_Y > top) {
            self.TOP_Y = top;
        }
        if (self.BOTTOM_Y < bottom) {
            self.BOTTOM_Y = bottom;
        }
        //console.log("L R T B " + self.LEFT_X + " : " + self.RIGHT_X + " : " + self.TOP_Y + " : " + self.BOTTOM_Y);
    };
   
    //Compute minimum scale down for stage
    self.computeMinimumScaleDown = function () {
        if (self.mapStage == undefined) {
            mv.minimumScaleDown = 0;
        }
        var initialized = false;
        for (var node_id in self.indexedNodesDict) {
            var node = self.indexedNodesDict[node_id];
            if (!initialized) {
                var canvasPosition = jQuery("#" + self.mapStage.getContainer().id).offset();

                var top = node.getY(),
                    left = node.getX(),
                    right = left + node.getWidth(),
                    bottom = top + node.getHeight();
                self.LEFT_X = left, self.RIGHT_X = right, self.TOP_Y = top, self.BOTTOM_Y = bottom;
                initialized = true;
            }
            self.computeLRTD(node);

        }
        var wContainer = self.mapStage.getWidth();
        var hContainer = self.mapStage.getHeight();
        var l = self.LEFT_X, r = self.RIGHT_X, t = self.TOP_Y, b = self.BOTTOM_Y;
        var tempW = Math.round(r - l);
        var tempH = Math.round(b - t);
        var first = (tempW < wContainer) ? (tempW / wContainer) : (wContainer / tempW);
        var second = (tempH < hContainer) ? (tempH / hContainer) : (hContainer / tempH);

        $("div.curr_width_map").text("whole_width: " + tempW + "[containerW: " + wContainer + "[por: " + first);
        $("div.curr_height_map").text("whole_height: " + tempH + "[containerW: " + hContainer + "[por: " + second);
        var lowerbound = Math.min(first, second);

        if (tempW < wContainer && tempH < hContainer) {
            lowerbound = 1;
            if (this.scaleRatio == undefined) {
                this.scaleRatio = 1;
                mv.ZoomRatio(100 - lowerbound * 100);
                mv.AssignZoomSliderValue(100);
            }
        }

        lowerbound = Math.round(100 * lowerbound);
        mv.minimumScaleDown = lowerbound;
        //return lowerbound;
    };
    observable(this);
    analytic = self.dispatchEvent.bind(self, 'analytic', 'mapModel');
    self.getIdea = function () {
        return idea;
    };
    self.isEditingEnabled = function () {
        return isEditingEnabled;
    };
    self.getCurrentLayout = function () {
        return currentLayout;
    };

    self.LookForNodeIndex = function (id) {
        $.each(this.indexedNodes, function (i, item) {
            if (item.Id == id) {
                return item;
            }
        });
        return null;
    }

    this.setMenu = function (themenu) {
        menu = themenu
    };
    this.getMenu = function () {
        return menu;
    };
    
    self.analytic = analytic;
    self.getCurrentlySelectedIdeaId = getCurrentlySelectedIdeaId;
    this.setIdea = function (anIdea) {
        if (idea) {
            idea.removeEventListener('changed', onIdeaChanged);
            setActiveNodes([]);
            self.dispatchEvent('nodeSelectionChanged', currentlySelectedIdeaId, false);
            currentlySelectedIdeaId = undefined;
        }
        idea = anIdea;
        idea.addEventListener('changed', onIdeaChanged);
        onIdeaChanged();
        self.selectNode(idea.id, true);
        self.dispatchEvent('mapViewResetRequested');
    };
    this.setEditingEnabled = function (value) {
        isEditingEnabled = value;
    };
    this.getEditingEnabled = function () {
        return isEditingEnabled;
    };
    this.setInputEnabled = function (value) {
        if (isInputEnabled !== value) {
            isInputEnabled = value;
            self.dispatchEvent('inputEnabledChanged', value);
        }
    };
    this.getInputEnabled = function () {
        return isInputEnabled;
    };
    this.deselectedNodes = function () {
        if (currentlySelectedIdeaId) {
            
            self.dispatchEvent('nodeSelectionChanged', currentlySelectedIdeaId, false);
        }
        currentlySelectedIdeaId = 0;
        setActiveNodes([]);
    };
    this.selectNode = function (id, force, appendToActive) {
        if (force || (isInputEnabled)) {
            if (currentlySelectedIdeaId) {
                self.dispatchEvent('nodeSelectionChanged', currentlySelectedIdeaId, false);
            }
            currentlySelectedIdeaId = id;
            if (appendToActive) {
                self.activateNode('internal', id);
            } else {
                setActiveNodes([id]);
            }

            self.dispatchEvent('nodeSelectionChanged', id, true);
        }
    };
    this.clickNode = function (id, event) {
        this.selectNode(id);
        if (isInputEnabled && event != null) {
            self.dispatchEvent('contextMenuRequested', id, event.layerX, event.layerY);
        }
        /*var button = event && event.button;
        if (event && (event.altKey || event.ctrlKey || event.metaKey)) {
            self.addLink('mouse', id);
        } else if (event && event.shiftKey) {
            //don't stop propagation, this is needed for drop targets
            self.toggleActivationOnNode('mouse', id);
        } else if (isAddLinkMode && !button) {
            this.addLink('mouse', id);
            this.toggleAddLinkMode();
        } else {
            
        }*/
    };
    this.findIdeaById = function (id) {
        /*jslint eqeq:true */
        if (idea.id == id) {
            return idea;
        }
        return idea.findSubIdeaById(id);
    };
    this.getSelectedStyle = function (prop) {
        return this.getStyleForId(currentlySelectedIdeaId, prop);
    };
    this.getStyleForId = function (id, prop) {
        var node = currentLayout.nodes && currentLayout.nodes[id];
        return node && node.attr && node.attr.style && node.attr.style[prop];
    };
    this.toggleCollapse = function (source) {
        var selectedIdea = currentlySelectedIdea(),
			isCollapsed;
        if (self.isActivated(selectedIdea.id) && _.size(selectedIdea.ideas) > 0) {
            isCollapsed = selectedIdea.getAttr('collapsed');
        } else {
            isCollapsed = self.everyActivatedIs(function (id) {
                var node = self.findIdeaById(id);
                if (node && _.size(node.ideas) > 0) {
                    return node.getAttr('collapsed');
                }
                return true;
            });
        }
        this.collapse(source, !isCollapsed);
        this.updateNodeCollapse(!isCollapsed);
        mv.HideNodeToolbar();

    };
    self.getRootNodeId = function () {
        return idea.id;
    };
    self.updateCurrentLayout = updateCurrentLayout;
    this.collapse = function (source, doCollapse) {
        analytic('collapse:' + doCollapse, source);
        var contextNodeId = getCurrentlySelectedIdeaId(),
			contextNode = function () {
			    return contextNodeId && currentLayout && currentLayout.nodes && currentLayout.nodes[contextNodeId];
			},
			oldContext,
			newContext;
        oldContext = contextNode();
        if (isInputEnabled) {
            self.applyToActivated(function (id) {
                var node = self.findIdeaById(id);
                if (node && (!doCollapse || (node.ideas && _.size(node.ideas) > 0))) {
                    idea.updateAttr(id, 'collapsed', doCollapse);
                }
            });
        }

        newContext = contextNode();
        if (oldContext && newContext) {
            moveNodes(
				currentLayout.nodes,
				oldContext.x - newContext.x,
				oldContext.y - newContext.y
			);
        }

        self.dispatchEvent('layoutChangeComplete');
    };
    this.updateStyle = function (source, prop, value) {
        /*jslint eqeq:true */
        if (!isEditingEnabled) {
            return false;
        }
        if (isInputEnabled) {
            analytic('updateStyle:' + prop, source);
            self.applyToActivated(function (id) {
                if (self.getStyleForId(id, prop) != value) {
                    idea.mergeAttrProperty(id, 'style', prop, value);
                }
            });
        }
    };
    this.updateLinkStyle = function (source, ideaIdFrom, ideaIdTo, prop, value) {
        if (!isEditingEnabled) {
            return false;
        }
        if (isInputEnabled) {
            analytic('updateLinkStyle:' + prop, source);
            var merged = _.extend({}, idea.getLinkAttr(ideaIdFrom, ideaIdTo, 'style'));
            merged[prop] = value;
            idea.updateLinkAttr(ideaIdFrom, ideaIdTo, 'style', merged);
        }
    };
    this.addSubIdea = function (source, parentId) {
        if (!isEditingEnabled) {
            return false;
        }

        var target = parentId || currentlySelectedIdeaId, newId;

        analytic('addSubIdea', source);
        mv.HideNodeToolbar();

        if (isInputEnabled) {
            idea.batch(function () {

                ensureNodeIsExpanded(source, target);

                newId = idea.addSubIdea(target);
            });

            if (newId) {
                editNewIdea(newId);
            }
            return newId;
        }
        return -1;
    };
    this.syncaddSubIdea = function (source, newid, title, xpos, ypos, rid, parentid) {
        if (!isEditingEnabled) {
            return false;
        }

        var target = parentid;

        analytic('syncaddSubIdea', source);

        if (isInputEnabled) {
            //bootbox.alert("AddNode2:" + newid + " " + title + " " + xpos + " " + ypos + " " + rid + " " + parentid);
            idea.batch(function () {

                ensureNodeIsExpanded(source, target);

                newId = idea.syncaddSubIdea(newid, title, xpos, ypos, rid, parentid);
            });

            return newId;
        }
        return -1;
    };


    this.insertIntermediate = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        if (!isInputEnabled || currentlySelectedIdeaId === idea.id) {
            return false;
        }
        var activeNodes = [], newId;
        analytic('insertIntermediate', source);
        self.applyToActivated(function (i) { activeNodes.push(i); });
        newId = idea.insertIntermediateMultiple(activeNodes);
        if (newId) {
            editNewIdea(newId);
        }
        mv.HideNodeToolbar();

    };
    this.addSiblingIdeaBefore = function (source) {
        var newId, parent, contextRank, newRank;
        if (!isEditingEnabled) {
            return false;
        }
        analytic('addSiblingIdeaBefore', source);
        if (!isInputEnabled) {
            return false;
        }
        parent = idea.findParent(currentlySelectedIdeaId) || idea;
        idea.batch(function () {
            ensureNodeIsExpanded(source, parent.id);
            newId = idea.addSubIdea(parent.id);
            if (newId && currentlySelectedIdeaId !== idea.id) {
                contextRank = parent.findChildRankById(currentlySelectedIdeaId);
                newRank = parent.findChildRankById(newId);
                if (contextRank * newRank < 0) {
                    idea.flip(newId);
                }
                idea.positionBefore(newId, currentlySelectedIdeaId);
            }
        });
        if (newId) {
            editNewIdea(newId);
        }
        mv.HideNodeToolbar();

    };
    this.addSiblingIdea = function (source) {
        var newId, nextId, parent, contextRank, newRank;
        if (!isEditingEnabled) {
            return false;
        }
        analytic('addSiblingIdea', source);
        if (isInputEnabled) {
            parent = idea.findParent(currentlySelectedIdeaId) || idea;
            idea.batch(function () {
                ensureNodeIsExpanded(source, parent.id);
                newId = idea.addSubIdea(parent.id);
                if (newId && currentlySelectedIdeaId !== idea.id) {
                    nextId = idea.nextSiblingId(currentlySelectedIdeaId);
                    contextRank = parent.findChildRankById(currentlySelectedIdeaId);
                    newRank = parent.findChildRankById(newId);
                    if (contextRank * newRank < 0) {
                        idea.flip(newId);
                    }
                    if (nextId) {
                        idea.positionBefore(newId, nextId);
                    }
                }
            });
            if (newId) {
                editNewIdea(newId);
            }
        }
        mv.HideNodeToolbar();

    };
    this.removeSubIdea = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('removeSubIdea', source);
        if (isInputEnabled) {
            var shouldSelectParent,
				previousSelectionId = getCurrentlySelectedIdeaId(),
				parent = idea.findParent(previousSelectionId);
            self.applyToActivated(function (id) {
                var removed = idea.removeSubIdea(id);
                /*jslint eqeq: true*/
                if (previousSelectionId == id) {
                    shouldSelectParent = removed;
                }
            });
            if (shouldSelectParent) {
                self.selectNode(parent.id);
            }
        }
        mv.HideNodeToolbar();

    };
    this.syncremoveSubIdea = function (source, nodeid) {
        if (!isEditingEnabled) {
            return false;
        }


        analytic('syncremoveSubIdea', source);

        if (isInputEnabled) {
            idea.batch(function () {

                var removed = idea.syncremoveSubIdea(nodeid);
            });
        }
        return -1;
    };
    this.updateTitle = function (ideaId, title, isNew) {
        if (isNew) {
            idea.initialiseTitle(ideaId, title);
        } else {
            idea.updateTitle(ideaId, title);
        }
    };
    this.updateNodePosition = function (ideaId, nodeX, nodeY) {
        idea.updateNodePosition(ideaId, nodeX, nodeY);
    }

    this.syncAddNode = function (ideaId, title, nodeX, nodeY, rid, parentid) {
        self.dispatchEvent('Subnodeaddsync', ideaId, title, nodeX, nodeY, rid, parentid);
    }

    this.syncRemoveNode = function (ideaId) {
        self.dispatchEvent('Subnoderemovesync', ideaId);
    }

    this.syncNodePosition = function (ideaId, nodeX, nodeY) {
        idea.syncNodePosition(ideaId, nodeX, nodeY);
        self.dispatchEvent('nodepositionSynced', ideaId, nodeX, nodeY);
    }

    this.syncNode = function (ideaId, title, nodeX, nodeY, data, isCollapsed, hasCustomedLocation) {
        idea.syncNode(ideaId, title, nodeX, nodeY, data, isCollapsed, hasCustomedLocation);
        self.dispatchEvent('nodedataSynced', ideaId, data);
    }
    this.syncChangeParent = function (ideaId, newParentId) {
        //console.log("syncChangeParent Here: " + ideaId + " " + newParentId);
        idea.syncChangeParent(ideaId, newParentId);
    }
    this.syncAddSecondLink = function (ideaFrom, ideaTo) {
        //console.log("syncChangeParent Here: " + ideaId + " " + newParentId);
        idea.syncAddSecondLink(ideaFrom, ideaTo);
    }
    this.syncRemoveSecondLink = function (ideaFrom, ideaTo) {
        //console.log("syncChangeParent Here: " + ideaId + " " + newParentId);
        idea.syncRemoveSecondLink(ideaFrom, ideaTo);
    }
    this.syncSaveOrUpdateTask = function (data) {
        TaskMaster.syncTaskDetail(data);
    }
    this.updateNodeData = function (datastr) {
        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeData(id, datastr);
        });
    };
    //@param: properties: dictionary that cotain field of node the user wants to change. 
    //Ex: {level: 1}
    this.updateNodeProperties = function (properties) {
        self.applyToActivated(function (id) {
            //idea is content.js 
            var dataupdated = idea.updateNodeProperties(id, properties);
        });
    };
    this.updateNodeCollapse = function (isCollapsed) {
        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeCollapse(id, isCollapsed);
        });
    };
    this.updateNodeShapeData = function (type) {

        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeKeyData(id, "type", type);
        });
    };
    this.updateNodeImgData = function (imgsrc) {

        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeKeyData(id, "imgsrc", imgsrc);
        });
    };
    this.updateNodeBgColorData = function (colorcode) {

        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeKeyData(id, "bgcolor", colorcode);
        });
    };
    this.updateNodeTextColorData = function (colorcode) {

        self.applyToActivated(function (id) {
            var dataupdated = idea.updateNodeKeyData(id, "textcolor", colorcode);
        });
    };
    this.updateNodeShape = function (type) {

        self.applyToActivated(function (id) {

            self.dispatchEvent('nodeShapeChanged', currentLayout.nodes[id], type);
        });

        /*alert('updating layout '+idea.id);
        //updateCurrentLayout(self.reactivate(layoutCalculator(idea)));
        alert('updated layout');*/
    };
    this.updateNodeImg = function (imgsrc) {

        self.applyToActivated(function (id) {

            self.dispatchEvent('nodeImgChanged', currentLayout.nodes[id], imgsrc);
        });

        /*alert('updating layout '+idea.id);
        //updateCurrentLayout(self.reactivate(layoutCalculator(idea)));
        alert('updated layout');*/
    };
    this.updateNodeBgColor = function (colorcode) {

        self.applyToActivated(function (id) {

            self.dispatchEvent('nodeBgColorChanged', currentLayout.nodes[id], colorcode);
        });

    };
    this.updateNodeTextColor = function (colorcode) {

        self.applyToActivated(function (id) {

            self.dispatchEvent('nodeTextColorChanged', currentLayout.nodes[id], colorcode);
        });

    };
    this.editNode = function (source, shouldSelectAll, editingNew) {
        if (!isEditingEnabled) {
            return false;
        }
        if (source) {
            analytic('editNode', source);
        }
        if (!isInputEnabled) {
            return false;
        }
        var title = currentlySelectedIdea().title;
        if (_.include(selectAllTitles, title)) { // === 'Press Space or double-click to edit') {
            shouldSelectAll = true;
        }
        self.dispatchEvent('nodeEditRequested', currentlySelectedIdeaId, shouldSelectAll, !!editingNew);
    };
    this.editIcon = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        if (source) {
            analytic('editIcon', source);
        }
        if (!isInputEnabled) {
            return false;
        }
        self.dispatchEvent('nodeIconEditRequested', currentlySelectedIdeaId);
    };
    //this.scaleRatio = 1;
    this.scaleRatio = undefined;
    this.scaleUp = function (source) {
        if (this.scaleRatio == undefined) {
            this.scaleRatio = 1 + mv.minimumScaleDown / 100;
        }
        self.scaleSpecific(source, this.scaleRatio + 0.1);
    };
    this.scaleDown = function (source) {
        self.computeMinimumScaleDown();
        if (this.scaleRatio == undefined) {
            this.scaleRatio = 1 + mv.minimumScaleDown / 100;
        }
        var triedRatio = this.scaleRatio - 0.1;

        $("div.minimum_scale_for_stage").text("minScale: " + mv.minimumScaleDown / 100);
        $("div.curr_ratio_scale").text("currScale: " + this.scaleRatio);
        if (triedRatio > (mv.minimumScaleDown / 100)) {

            self.scaleSpecific(source, triedRatio);
        } else {
            self.scaleSpecific(source, mv.minimumScaleDown / 100);
        }
    };
    this.scale = function (source, scaleMultiplier, zoomPoint) {
        if (isInputEnabled) {

            var triedRatio = Math.max(Math.min(self.mapStage.getScale().x * scaleMultiplier, 5), 0.2);

            if (scaleMultiplier < 1) {
                if (triedRatio > (mv.minimumScaleDown / 100)) {
                    this.scaleRatio = triedRatio;
                    self.dispatchEvent('mapScaleChanged', scaleMultiplier, zoomPoint);

                    analytic(scaleMultiplier < 1 ? 'scaleDown' : 'scaleUp', source);
                } else {

                    self.scaleSpecific(source, mv.minimumScaleDown / 100);
                    analytic(scaleMultiplier < 1 ? 'scaleDown' : 'scaleUp', source);
                }
            } else {
                this.scaleRatio = triedRatio;
                self.dispatchEvent('mapScaleChanged', scaleMultiplier, zoomPoint);

                analytic(scaleMultiplier < 1 ? 'scaleDown' : 'scaleUp', source);
            }

        }
    };
    this.scaleSpecific = function (source, scale, zoomPoint) {
        if (scale < 0) {
            return;
        }
        this.scaleRatio = scale;
        if (isInputEnabled) {
            self.dispatchEvent('mapSpecificScaleChanged', scale, zoomPoint);
            analytic('scaleSpecific', source);
        }

    };
    this.move = function (source, deltaX, deltaY) {
        if (isInputEnabled) {
            self.dispatchEvent('mapMoveRequested', deltaX, deltaY);
            analytic('move', source);
        }
    };
    this.stageClick = function (source, deltaX, deltaY) {
        self.dispatchEvent('stageClick', deltaX, deltaY);
        analytic('stageClick', source);
    }
    this.resetView = function (source) {
        if (isInputEnabled) {
            this.scaleRatio = 1;
            mv.ZoomRatio(100 - mv.minimumScaleDown);
            mv.AssignZoomSliderValue(100);
            self.selectNode(idea.id);
            self.dispatchEvent('mapViewResetRequested');
            analytic('resetView', source);
        }

    };
    this.exportMapToPNG = function () {
        var obj = MAPJS.pngExport(this.getIdea(), "image/png");
    };
    this.exportMapToPDF = function () {
        var obj = MAPJS.pngExport(this.getIdea(), "image/jpeg");
    };
    this.centerMap = function (source) {
        
        if (isInputEnabled) {
            self.computeMinimumScaleDown();

            if (this.scaleRatio == undefined) {
                this.scaleRatio = 1 + mv.minimumScaleDown / 100;
            }
            //alert(self.LEFT_X + " f " + self.RIGHT_X + " f " + self.TOP_Y + " f " + self.BOTTOM_Y);
            //toa do O'xy
            var cx = this.scaleRatio * (self.LEFT_X + self.RIGHT_X) / 2 + 0.5 * self.mapStage.getWidth(),
            cy = this.scaleRatio * (self.TOP_Y + self.BOTTOM_Y) / 2 + 0.5 * self.mapStage.getHeight();
            var translatedVector = { x: self.mapStage.getX() - 0.5 * self.mapStage.getWidth(), y: self.mapStage.getY() - 0.5 * self.mapStage.getHeight() };
            cx += translatedVector.x;
            cy += translatedVector.y;
            cx = 0.5 * self.mapStage.getWidth() - cx + self.mapStage.getX();
            cy = 0.5 * self.mapStage.getHeight() - cy + self.mapStage.getY();

            self.dispatchEvent('mapCenterRequested', cx, cy);
            analytic('centerMap', source);
            $('div.stage_x').text(self.mapStage.getX());
            $('div.stage_y').text(self.mapStage.getY());

        }
    };
    
    this.getCurrentlyHoveredIdea = function () {
        return currentlyHoveredIdea;
    };
    this.setCurrentlyHoveredIdea = function (node) {
        currentlyHoveredIdea = node;
    };
    this.openAttachment = function (source, nodeId) {
        analytic('openAttachment', source);
        nodeId = nodeId || currentlySelectedIdeaId;
        var node = currentLayout.nodes[nodeId],
			attachment = node && node.attr && node.attr.attachment;
        if (node) {
            self.dispatchEvent('attachmentOpened', nodeId, attachment);
        }
    };
    this.setAttachment = function (source, nodeId, attachment) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('setAttachment', source);
        var hasAttachment = !!(attachment && attachment.content);
        idea.updateAttr(nodeId, 'attachment', hasAttachment && attachment);
    };
    this.addLink = function (source, nodeIdTo) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('addLink', source);
        idea.addLink(currentlySelectedIdeaId, nodeIdTo);
    };
    this.selectLink = function (source, link, selectionPoint) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('selectLink', source);
        if (!link) {
            return false;
        }
        self.dispatchEvent('linkSelected', link, selectionPoint, idea.getLinkAttr(link.ideaIdFrom, link.ideaIdTo, 'style'));
    };
    this.removeLink = function (source, nodeIdFrom, nodeIdTo) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('removeLink', source);
        idea.removeLink(nodeIdFrom, nodeIdTo);
    };

    this.toggleAddLinkMode = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        if (!isInputEnabled) {
            return false;
        }
        analytic('toggleAddLinkMode', source);
        isAddLinkMode = !isAddLinkMode;
        self.dispatchEvent('addLinkModeToggled', isAddLinkMode);
    };
    this.cancelCurrentAction = function (source) {
        if (!isInputEnabled) {
            return false;
        }
        if (!isEditingEnabled) {
            return false;
        }
        if (isAddLinkMode) {
            this.toggleAddLinkMode(source);
        }
    };
    self.undo = function (source) {
        if (!isEditingEnabled) {
            return false;
        }

        analytic('undo', source);
        var undoSelectionClone = revertSelectionForUndo,
			undoActivationClone = revertActivatedForUndo;
        if (isInputEnabled) {
            idea.undo();
            if (undoSelectionClone) {
                self.selectNode(undoSelectionClone);
            }
            if (undoActivationClone) {
                setActiveNodes(undoActivationClone);
            }

        }
    };
    self.redo = function (source) {
        if (!isEditingEnabled) {
            return false;
        }

        analytic('redo', source);
        if (isInputEnabled) {
            idea.redo();
        }
    };
    self.moveRelative = function (source, relativeMovement) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('moveRelative', source);
        if (isInputEnabled) {
            idea.moveRelative(currentlySelectedIdeaId, relativeMovement);
        }
    };
    self.cut = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('cut', source);
        if (isInputEnabled) {
            var activeNodeIds = [], parents = [], firstLiveParent;
            self.applyToActivated(function (nodeId) {
                activeNodeIds.push(nodeId);
                parents.push(idea.findParent(nodeId).id);
            });
            clipboard.put(idea.cloneMultiple(activeNodeIds));
            idea.removeMultiple(activeNodeIds);
            firstLiveParent = _.find(parents, idea.findSubIdeaById);
            self.selectNode(firstLiveParent || idea.id);
        }
    };
    self.copy = function (source) {
        var activeNodeIds = [];
        if (!isEditingEnabled) {
            return false;
        }
        analytic('copy', source);
        if (isInputEnabled) {
            self.applyToActivated(function (node) {
                activeNodeIds.push(node);
            });
            clipboard.put(idea.cloneMultiple(activeNodeIds));
        }
    };
    self.paste = function (source) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('paste', source);
        if (isInputEnabled) {
            var result = idea.pasteMultiple(currentlySelectedIdeaId, clipboard.get());
            if (result && result[0]) {
                self.selectNode(result[0]);
            }
        }
    };
    self.pasteStyle = function (source) {
        var clipContents = clipboard.get();
        if (!isEditingEnabled) {
            return false;
        }
        analytic('pasteStyle', source);
        if (isInputEnabled && clipContents && clipContents[0]) {
            var pastingStyle = clipContents[0].attr && clipContents[0].attr.style;
            self.applyToActivated(function (id) {
                idea.updateAttr(id, 'style', pastingStyle);
            });
        }
    };
    self.getIcon = function (nodeId) {
        var node = currentLayout.nodes[nodeId || currentlySelectedIdeaId];
        if (!node) {
            return false;
        }
        return node.attr && node.attr.icon;
    };
    self.setIcon = function (source, url, imgWidth, imgHeight, position, nodeId) {
        if (!isEditingEnabled) {
            return false;
        }
        analytic('setIcon', source);
        nodeId = nodeId || currentlySelectedIdeaId;
        var nodeIdea = self.findIdeaById(nodeId);
        if (!nodeIdea) {
            return false;
        }
        if (url) {
            idea.updateAttr(nodeId, 'icon', {
                url: url,
                width: imgWidth,
                height: imgHeight,
                position: position
            });
        } else if (nodeIdea.title || nodeId === idea.id) {
            idea.updateAttr(nodeId, 'icon', false);
        } else {
            idea.removeSubIdea(nodeId);
        }
    };
    self.moveUp = function (source) { self.moveRelative(source, -1); };
    self.moveDown = function (source) { self.moveRelative(source, 1); };
    self.getSelectedNodeId = function () {
        return getCurrentlySelectedIdeaId();
    };
    self.centerOnNode = function (nodeId) {
        if (!currentLayout.nodes[nodeId]) {
            idea.startBatch();
            _.each(idea.calculatePath(nodeId), function (parent) {
                idea.updateAttr(parent.id, 'collapsed', false);
            });
            idea.endBatch();
        }
        self.dispatchEvent('nodeFocusRequested', nodeId);
        self.selectNode(nodeId);
    };
    self.search = function (query) {
        var result = [];
        query = query.toLocaleLowerCase();
        idea.traverse(function (contentIdea) {
            if (contentIdea.title && contentIdea.title.toLocaleLowerCase().indexOf(query) >= 0) {
                result.push({ id: contentIdea.id, title: contentIdea.title });
            }
        });
        return result;
    };
    //node activation and selection
    (function () {
        var isRootOrRightHalf = function (id) {
            return currentLayout.nodes[id].x >= currentLayout.nodes[idea.id].x;
        },
        isRootOrLeftHalf = function (id) {
            return currentLayout.nodes[id].x <= currentLayout.nodes[idea.id].x;
        },
        nodesWithIDs = function () {
            return _.map(currentLayout.nodes,
                function (n, nodeId) {
                    return _.extend({ id: parseInt(nodeId, 10) }, n);
                });
        },
        applyToNodeUp = function (source, analyticTag, method) {
            var node,
                rank,
                isRoot = currentlySelectedIdeaId === idea.id,
                targetRank = isRoot ? -Infinity : Infinity;
            if (!isInputEnabled) {
                return;
            }
            analytic(analyticTag, source);
            if (idea.findParent(currentlySelectedIdeaId)){
                method.apply(self, [idea.findParent(currentlySelectedIdeaId).id]);
            }
            
        },
        applyToNodeDown = function (source, analyticTag, method) {
            var node, rank, minimumPositiveRank = Infinity;
            if (!isInputEnabled) {
                return;
            }
            analytic(analyticTag, source);
            
            node = idea.id === currentlySelectedIdeaId ? idea : idea.findSubIdeaById(currentlySelectedIdeaId);
            ensureNodeIsExpanded(source, node.id);
            for (rank in node.ideas) {
                rank = parseFloat(rank);
                if (rank > 0 && rank < minimumPositiveRank) {
                    minimumPositiveRank = rank;
                }
            }
            if (minimumPositiveRank !== Infinity) {
                method.apply(self, [node.ideas[minimumPositiveRank].id]);
            }
            
        },
        applyToNodeLeft = function (source, analyticTag, method) {
            var previousSibling = idea.previousSiblingId(currentlySelectedIdeaId),
                nodesAbove,
                closestNode,
                currentNode = currentLayout.nodes[currentlySelectedIdeaId];
            if (!isInputEnabled) {
                return;
            }
            analytic(analyticTag, source);
            if (previousSibling) {
                method.apply(self, [previousSibling]);
            } else {
                /*if (!currentNode) { return; }

                var node = idea.findParent(currentlySelectedIdeaId);

                ensureNodeIsExpanded(source, node.id);
                var previousnode = null;
                for (sibnode in node.ideas) {
                    if (sibnode.id == currentlySelectedIdeaId) break;
                    previousnode = sibnode;
                }
                if (previousnode) {
                    method.apply(self,previousnode.id]);
                }*/
            }
        },
        applyToNodeRight = function (source, analyticTag, method) {
            var nextSibling = idea.nextSiblingId(currentlySelectedIdeaId),
                nodesBelow,
                closestNode,
                currentNode = currentLayout.nodes[currentlySelectedIdeaId];
            if (!isInputEnabled) {
                return;
            }
            analytic(analyticTag, source);
            if (nextSibling) {
                method.apply(self, [nextSibling]);
            } else {
                /*if (!currentNode) { return; }
                nodesBelow = _.reject(nodesWithIDs(), function (node) {
                    return node.y <= currentNode.y || Math.abs(node.x - currentNode.x) > horizontalSelectionThreshold;
                });
                if (_.size(nodesBelow) === 0) {
                    return;
                }
                closestNode = _.min(nodesBelow, function (node) {
                    return Math.pow(node.x - currentNode.x, 2) + Math.pow(node.y - currentNode.y, 2);
                });
                method.apply(self, [closestNode.id]);*/
            }
        },
        applyFuncs = { 'Left': applyToNodeLeft, 'Up': applyToNodeUp, 'Down': applyToNodeDown, 'Right': applyToNodeRight };
        self.getActivatedNodeIds = function () {
            return activatedNodes.slice(0);
        };
        self.activateSiblingNodes = function (source) {
            var parent = idea.findParent(currentlySelectedIdeaId),
                siblingIds;
            analytic('activateSiblingNodes', source);
            if (!parent || !parent.ideas) {
                return;
            }
            siblingIds = _.map(parent.ideas, function (child) { return child.id; });
            setActiveNodes(siblingIds);
        };
        self.activateNodeAndChildren = function (source) {
            analytic('activateNodeAndChildren', source);
            var contextId = getCurrentlySelectedIdeaId(),
                subtree = idea.getSubTreeIds(contextId);
            subtree.push(contextId);
            setActiveNodes(subtree);
        };
        _.each(['Left', 'Right', 'Up', 'Down'], function (position) {
            /*self['activateNode' + position] = function (source) {
                applyFuncs[position](source, 'activateNode' + position, function (nodeId) {
                    self.selectNode(nodeId, false, true);
                });
            };*/
            self['selectNode' + position] = function (source) {
                applyFuncs[position](source, 'selectNode' + position, self.selectNode);
            };
        });
        self.toggleActivationOnNode = function (source, nodeId) {
            analytic('toggleActivated', source);
            
            if (!self.isActivated(nodeId)) {
                setActiveNodes([nodeId].concat(activatedNodes));
            } else {                
                setActiveNodes(_.without(activatedNodes, nodeId));
            }
        };
        self.activateNode = function (source, nodeId) {
            analytic('activateNode', source);
            if (!self.isActivated(nodeId)) {
                activatedNodes.push(nodeId);
                self.dispatchEvent('activatedNodesChanged', [nodeId], []);
            }
        };
        self.activateChildren = function (source) {
            analytic('activateChildren', source);
            var context = currentlySelectedIdea();
            if (!context || _.isEmpty(context.ideas) || context.getAttr('collapsed')) {
                return;
            }
            setActiveNodes(idea.getSubTreeIds(context.id));
        };
        self.activateSelectedNode = function (source) {
            analytic('activateSelectedNode', source);
            setActiveNodes([getCurrentlySelectedIdeaId()]);
        };
        self.isActivated = function (id) {
            /*jslint eqeq:true*/
            return _.find(activatedNodes, function (activeId) { return id == activeId; });
        };
        self.applyToActivated = function (toApply) {
            idea.batch(function () { _.each(activatedNodes, toApply); });
        };
        self.everyActivatedIs = function (predicate) {
            return _.every(activatedNodes, predicate);
        };
        self.activateLevel = function (source, level) {
            analytic('activateLevel', source);
            var toActivate = _.map(
                _.filter(
                    currentLayout.nodes,
                    function (node) {
                        /*jslint eqeq:true*/
                        return node.level == level;
                    }
                ),
                function (node) { return node.id; }
            );
            if (!_.isEmpty(toActivate)) {
                setActiveNodes(toActivate);
            }
        };
        self.reactivate = function (layout) {
            _.each(layout.nodes, function (node) {
                if (_.contains(activatedNodes, node.id)) {
                    node.activated = true;
                }
            });
            return layout;
        };
    }());
};

/*global _, MAPJS, jQuery*/
/*jslint forin:true*/
hasshown = false;
MAPJS.dragdrop = function (mapModel, stage, imageInsertController) {
    'use strict';
    var currentDroppable = undefined,
		findNodeOnStage = function (nodeId) {
		    return stage.get('#node_' + nodeId)[0];
		},
		showAsDroppable = function (nodeId, isDroppable) {
		    var node = findNodeOnStage(nodeId);
		    node.setIsDroppable(isDroppable);
		},
		updateCurrentDroppable = function (nodeId) {
		    if (currentDroppable !== nodeId) {
		        if (currentDroppable) {
		            showAsDroppable(currentDroppable, false);
		        }
		        currentDroppable = nodeId;
		        if (currentDroppable) {
		            showAsDroppable(currentDroppable, true);
		        }
		    }
		},
		isPointOverNode = function (x, y, n) { //move to mapModel candidate
		    /*jslint eqeq: true*/

		    var node = findNodeOnStage(n.id);

		    var rsult = false;

		    if (node.NodeType == "rec") {
		        rsult = (x >= node.getX() &&
				y >= node.getY() &&
				x <= node.getX() + node.getWidth() &&
				y <= node.getY() + node.getHeight());
		    } else if (node.NodeType == "dia") {
		        var dim = (node.getWidth() / 2 + node.getHeight() / 2);
		        var dim2 = dim * 1.41;
		        //if (node.getHeight() > dim) dim = node.getHeight();
		        var dis1 = dim * x + dim * y + (node.getX() - node.getHeight() / 2) * (node.getY() - node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
		        dis1 = dis1 * dis1 / (2 * dim * dim);
		        var dis2 = dim * x - dim * y + (node.getX() + node.getHeight() / 2 + node.getWidth()) * (node.getY() - node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
		        dis2 = dis2 * dis2 / (2 * dim * dim);

		        var dis3 = -dim * x + dim * y + (node.getX() - node.getHeight() / 2) * (node.getY() + node.getHeight() + node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
		        dis3 = dis3 * dis3 / (2 * dim * dim);

		        var dis4 = -dim * x - dim * y + (node.getX() + node.getHeight() / 2 + node.getWidth()) * (node.getY() + node.getHeight() + node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
		        dis4 = dis4 * dis4 / (2 * dim * dim);

		        if ((dis1 < dim2 * dim2 & dis2 < dim2 * dim2) && (dis3 < dim2 * dim2 & dis4 < dim2 * dim2)) {
		            //bootbox.alert(dis3 +  ' '+dis1);
		            rsult = true;
		        } else {

		            rsult = false;
		        }



		    } else if (node.NodeType == "cir") {
		        var dim = (node.getWidth() / 2 + node.getHeight() / 2) + 60;
		        //var dim = node.getWidth();
		        //if (node.getHeight() > dim) dim = node.getHeight();
		        var centerx = node.getX() + node.getWidth() / 2;
		        var centery = node.getY() + node.getHeight() / 2;

		        if ((centerx - x) * (centerx - x) + (centery - y) * (centery - y) <= dim * dim / 4) {
		            rsult = true;
		        } else { rsult = false; }


		    }

		    return rsult;

		},
		canDropOnNode = function (id, x, y, node) {
		    /*jslint eqeq: true*/

		    return id != node.id && isPointOverNode(x, y, node);
		},
		tryFlip = function (rootNode, nodeBeingDragged, nodeDragEndX) {

		    var flipRightToLeft = rootNode.x < nodeBeingDragged.x && nodeDragEndX < rootNode.x,
				flipLeftToRight = rootNode.x > nodeBeingDragged.x && rootNode.x < nodeDragEndX;
		    if (flipRightToLeft || flipLeftToRight) {
		        return mapModel.getIdea().flip(nodeBeingDragged.id);
		    }
		    return false;
		},
        /**
            @param x: stagePoint.x
            @param y: stagePoint.y
            @param NodeX: current position.X of node (not Kinectic.Node but Kinectic.Idea)
            @param NodeY: current position.Y of node (not Kinetic.Node but Kinetic.Idea)
            When we move a node (Kinetic.Idea) we will move its children to stagePoint too. 

        */
        latestid = 0,
        latestx = 0,
        latesty=0,
		nodeDragMove = function (id, x, y, nodeX, nodeY) {
		    var nodeId, node;
		    var idea = mapModel.getIdea(),
				parentIdea = idea.findParent(id),
				parentNode = mapModel.getCurrentLayout().nodes[parentIdea.id];

		    if (!mapModel.isEditingEnabled()) {
		        return;
		    }
		    for (nodeId in mapModel.getCurrentLayout().nodes) {
		        node = mapModel.getCurrentLayout().nodes[nodeId];
		        if (node != null && canDropOnNode(id, x, y, node)) {

		            updateCurrentDroppable(nodeId);
		            return;
		        }
		    }
		    updateCurrentDroppable(undefined);
		    var nodeBeingDragged = mapModel.getCurrentLayout().nodes[id];
		    //alert("Toa do x y nodeX nodeY " + x + " ++++ " + y + " ++++ " + nodeX + " ++++ " + nodeY);
		    //alert(nodeBeingDragged.x + " ++++ " + nodeBeingDragged.y + " ++++ " + nodeX + " ++++ " + nodeY);
		    if (id != latestid) {
		        latestid = id;
		        latestx = parentNode.x - nodeBeingDragged.x;// + stage.getX();
		        latesty = parentNode.y - nodeBeingDragged.y;
		    }
		    nodeBeingDragged.x = nodeX;
		    nodeBeingDragged.y = nodeY;
		    nodeBeingDragged.hasCustomedLocation = true;
		    mapModel.dispatchEvent('nodepositionChanged', nodeBeingDragged);

		    mapModel.dispatchEvent('nodeMoved', nodeBeingDragged);

		},
		getRootNode = function () {
		    return mapModel.getCurrentLayout().nodes[mapModel.getIdea().id];
		},
		nodeDragEnd = function (id, x, y, nodeX, nodeY, shouldCopy, shouldPositionAbsolutely, ismergable) {
		    if (ismergable === undefined) { ismergable = true; }
		    var nodeBeingDragged = mapModel.getCurrentLayout().nodes[id],
				nodeId,
				node,
				rootNode = getRootNode(),
				verticallyClosestNode = {
				    id: null,
				    y: Infinity
				},
				clone,
				idea = mapModel.getIdea(),
				parentIdea = idea.findParent(id),
				parentNode = mapModel.getCurrentLayout().nodes[parentIdea.id],
				maxSequence = 1,
				validReposition = function () {

				    return nodeBeingDragged.level === 2 ||
						((nodeBeingDragged.x - parentNode.x) * (x - parentNode.x) > 0);
				};
		    if (!mapModel.isEditingEnabled()) {
		        mapModel.dispatchEvent('nodeMoved', nodeBeingDragged, 'failed');
		        return;
		    }

		    updateCurrentDroppable(undefined);

		    var hasMerge = false;

		    for (nodeId in mapModel.getCurrentLayout().nodes) {
		        node = mapModel.getCurrentLayout().nodes[nodeId];
		        if (ismergable && canDropOnNode(id, x, y, node)) {
		            hasMerge = true;
		           
		            break;
		        }
		    }
		    if (!hasMerge) {
		        nodeBeingDragged.x = nodeX;
		        nodeBeingDragged.y = nodeY;
		        nodeBeingDragged.hasCustomedLocation = true;
		        mapModel.dispatchEvent('nodepositionChanged', nodeBeingDragged);
		    } else {
		        if (latestid == id) {
		            //bootbox.alert([latestx, latesty].toString());
		            nodeBeingDragged.x = parentNode.x - latestx;//parentNode.x;
		            nodeBeingDragged.y = parentNode.y - latesty;//parentNode.y;
		        }
		        nodeBeingDragged.hasCustomedLocation = true;
		        mapModel.dispatchEvent('nodepositionChanged', nodeBeingDragged);
		        //bootbox.alert('Dragged 2: ' + [nodeBeingDragged.x, nodeBeingDragged.y].toString());
		        /*mapModel.analytic('nodeManuallyPositioned');
		        mapModel.selectNode(id);
		        maxSequence = _.max(_.map(parentIdea.ideas, function (i) { return (i.id !== id && i.attr && i.attr.position && i.attr.position[2]) || 0; }));
		        
		        idea.startBatch();
		        idea.updateAttr(
					id,
					'position',
					[nodeX - parentNode.x, nodeY - parentNode.y, maxSequence + 1]
				);
		        idea.endBatch();*/
		        /*nodeBeingDragged.x = -100;
		        nodeBeingDragged.y = 0;
		        nodeBeingDragged.hasCustomedLocation = true;
		        mapModel.dispatchEvent('nodepositionChanged', nodeBeingDragged);*/
		    }
		    
		    mapModel.dispatchEvent('nodeMoved', nodeBeingDragged);
		   
		    
		    for (nodeId in mapModel.getCurrentLayout().nodes) {
		        node = mapModel.getCurrentLayout().nodes[nodeId];
		        if (ismergable && canDropOnNode(id, x, y, node)) {
		            
		            if (shouldCopy) {
		                
		                clone = mapModel.getIdea().clone(id);
		                if (!clone || !mapModel.getIdea().paste(nodeId, clone)) {
		                    mapModel.dispatchEvent('nodeMoved', nodeBeingDragged, 'failed');
		                    mapModel.analytic('nodeDragCloneFailed');
		                }
		            } else if (!mapModel.getIdea().changeParent(id, nodeId)) {
		                //alert(node.level == 1);
		                if (node.level === 1) {
		                    //if the node is dragged to Root node. 
		                    //idea.updateAttr(id, 'isStandAlone', false);
		                    var changed = { 'level': 0, 'isStandAlone': false };
		                    mapModel.updateNodeProperties(changed);
		                }
		                mapModel.dispatchEvent('nodeMoved', nodeBeingDragged, 'failed');
		                mapModel.analytic('nodeDragParentFailed');
		                idea.updateAttr(id, 'position');
		            }
		            return;
		        }
		    }

		    /*idea.startBatch();
		    if (nodeBeingDragged.level === 2) {
		        tryFlip(rootNode, nodeBeingDragged, x);
		    }*/
		    /*_.each(idea.sameSideSiblingIds(id), function (nodeId) {
		        node = mapModel.getCurrentLayout().nodes[nodeId];
		        if (y < node.y && node.y < verticallyClosestNode.y) {
		            verticallyClosestNode = node;
		        }
		    });
		    idea.positionBefore(id, verticallyClosestNode.id);
		    //alert(shouldPositionAbsolutely && validReposition());
		    if (shouldPositionAbsolutely && validReposition()) {
		        mapModel.analytic('nodeManuallyPositioned');
		        mapModel.selectNode(id);
		        maxSequence = _.max(_.map(parentIdea.ideas, function (i) { return (i.id !== id && i.attr && i.attr.position && i.attr.position[2]) || 0; }));
		        idea.updateAttr(
					id,
					'position',
					[Math.abs(nodeX - parentNode.x), nodeY - parentNode.y, maxSequence + 1]
				);
		    }
           
		    mapModel.analytic('nodeManuallyPositioned');
		    mapModel.selectNode(id);
		    bootbox.alert('nodeManuallyPositioned ' +[nodeX - parentNode.x, nodeY - parentNode.y].toString())
		    idea.updateAttr(
					id,
					'position',
					[nodeX - parentNode.x, nodeY - parentNode.y, maxSequence + 1]
				);
		    idea.endBatch();
             */
		},
		screenToStageCoordinates = function (x, y) {
		    return {
		        x: (x - stage.getX()) / (stage.getScale().x || 1),
		        y: (y - stage.getY()) / (stage.getScale().y || 1)
		    };
		},
		getInteractionPoint = function (evt) {
		    if (evt.changedTouches && evt.changedTouches[0]) {
		        //alert(evt.changedTouches[0].clientX + "  xxxx  "+ evt.changedTouches[0].clientY);
		        return screenToStageCoordinates(evt.changedTouches[0].clientX, evt.changedTouches[0].clientY);
		    }
		    //alert(evt.evt.layerX + " .... " + evt.evt.layerY);
		    return screenToStageCoordinates(evt.evt.layerX, evt.evt.layerY);
		},
		dropImage = function (dataUrl, imgWidth, imgHeight, evt) {
		    var node,
				nodeId,
				content = mapModel.getIdea(),
				point = getInteractionPoint(evt),
				dropOn = function (ideaId, position) {
				    var scaleX = Math.min(imgWidth, 300) / imgWidth,
						scaleY = Math.min(imgHeight, 300) / imgHeight,
						scale = Math.min(scaleX, scaleY);
				    mapModel.setIcon('drag and drop', dataUrl, Math.round(imgWidth * scale), Math.round(imgHeight * scale), position, ideaId);
				},
				addNew = function () {
				    content.startBatch();
				    dropOn(content.addSubIdea(mapModel.getSelectedNodeId()), 'center');
				    content.endBatch();
				};
		    for (nodeId in mapModel.getCurrentLayout().nodes) {
		        node = mapModel.getCurrentLayout().nodes[nodeId];
		        if (isPointOverNode(point.x, point.y, node)) {
		            return dropOn(nodeId, 'left');
		        }
		    }
		    addNew();
		};
    jQuery(stage.getContainer()).imageDropWidget(imageInsertController);
    imageInsertController.addEventListener('imageInserted', dropImage);

    mapModel.addEventListener('CreateNodeToChangeShape', function (node, id) {
        var shouldPositionAbsolutely;
        node.on('dragstart', function (evt) {
            if (node.GetNodePinMouse()) return;
            /*(mapModel.updateCenterofFocus(mapModel.LEVELNODE);
            if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;*/

            shouldPositionAbsolutely = false;
            node.moveToTop();
            node.setShadowOffset(8);
            node.setOpacity(0.3);
        });
        node.on('dragmove', function (evt) {
            /*if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;*/
            if (node.GetNodePinMouse()) return;
            var stagePoint = getInteractionPoint(evt);
            nodeDragMove(
				id,
				stagePoint.x,
				stagePoint.y,
                node.getX(),
				node.getY()
			);
        });
        node.on('dragend', function (evt) {
            /*if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;
            mapModel.updateCenterofFocus(mapModel.LEVELRELEASED);*/
            if (node.GetNodePinMouse()) return;
            var stagePoint = getInteractionPoint(evt);
            //node.setShadowOffset(4);
            node.setOpacity(1);
            nodeDragEnd(
				id,
				stagePoint.x,
				stagePoint.y,
				node.getX(),
				node.getY(),
				evt.shiftKey,
				shouldPositionAbsolutely
			);
        });
    });

    mapModel.addEventListener('nodeCreated', function (n) {
        var node = findNodeOnStage(n.id), shouldPositionAbsolutely;
        //var dragnode = node.find("rect_" + n.id);
        node.on('dragstart', function (evt) {
            if (node.GetNodePinMouse()) return;
            
            node.AllowDrag = true;
            /*mapModel.updateCenterofFocus(mapModel.LEVELNODE);
            if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;*/

            shouldPositionAbsolutely = false;
            node.moveToTop();
            node.setShadowOffset(8);
            node.setOpacity(0.3);
        });
        node.on('dragmove', function (evt) {
            /*if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;*/
            if (node.GetNodePinMouse() ) return;
            node.AllowDrag = true;
            var stagePoint = getInteractionPoint(evt);
            //alert("stagePoint : " + stagePoint.x + " , " + stagePoint.y + " , node:" + node.getX() + " , " + node.getY());
            nodeDragMove(
				n.id,
				stagePoint.x,
				stagePoint.y,
                node.getX(),
				node.getY()
			);
        });
        node.on('dragend', function (evt) {
            /*if (mapModel.CenterofFocus > mapModel.LEVELNODE) return;
            mapModel.updateCenterofFocus(mapModel.LEVELRELEASED);*/
            
            if (node.GetNodePinMouse() || !node.AllowDrag) return;
            //bootbox.alert([node.GetNodePinMouse() , !node.AllowDrag].toString());
            var stagePoint = getInteractionPoint(evt);
            node.setShadowOffset(4);
            node.setOpacity(1);
            nodeDragEnd(
				n.id,
				stagePoint.x,
				stagePoint.y,
				node.getX(),
				node.getY(),
				evt.shiftKey,
				shouldPositionAbsolutely
			);
        });
        
    });
    mapModel.addEventListener('follownewpin', function (n, evt) {
        var node = findNodeOnStage(n.id), shouldPositionAbsolutely;
        var stagePoint = getInteractionPoint(evt);
        node.setOpacity(1);
        //shouldPositionAbsolutely = evt.shiftKey;
        shouldPositionAbsolutely = false;

        node.setX(stagePoint.x);
        node.setY(stagePoint.y);
        nodeDragEnd(
            n.id,
            stagePoint.x,
            stagePoint.y,
            node.getX(),
            node.getY(),
            evt.shiftKey,
            shouldPositionAbsolutely, false
        );

        //node.x = stagePoint.x; node.y = stagePoint.y;

    });
};

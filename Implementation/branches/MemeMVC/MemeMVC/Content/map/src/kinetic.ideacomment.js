/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

   
    

    Kinetic.IdeaComment = function (config) {

        var defaultnoteimage = new Image();
        defaultnoteimage.src = "/Images/comment.png";

        function createCommentImage(xpos, ypos) {
            alert("it runs");
            var icon = new Kinetic.Image({
                x: xpos,
                y: ypos,
                width: 0,
                height: 0,
                strokeWidth: 0,
                image: defaultnoteimage

            });


            return icon;
        }

        var self = this;
        
        
        this.shapeType = 'IdeaNote';
        this.rid = config.rid;
        this.nodeid = config.nid;
        Kinetic.Group.call(this, config);
        
        try {
            this.image1 = createCommentImage(3, 3);
            this.bgShape = new Kinetic.Circle({
                strokeWidth: 0.1,
                radius: 20,
                x: 20,
                y: 20,
                fill: "#FFFFFF",
                stroke: "#333333"
            });

            this.on('mouseover', function () {
                self.bgShape.setFill("#E8E35A");
                self.draw();
            });

            this.on('mouseout', function () {
                self.bgShape.setFill("#ffffff");
                self.draw();
            });
            this.on('click tap touchend mouseup', function () {
                alert("it works");
                //LoadBodyDetailFromValues(this.rid, this.nodeid)
            });

            //this.image1.updateMapjsAttribs({ });
            this.add(this.bgShape);
            this.add(this.image1); 
        } catch (err) {

        }
	};
    
	
	
    Kinetic.IdeaComment.prototype.setStyle = function () {
	    'use strict';
	    /*jslint newcap: true*/
	    var self = this;
	};

    Kinetic.IdeaComment.prototype.setMMAttr = function (newMMAttr) {
	    'use strict';
	    this.mmAttr = newMMAttr;
	    this.setStyle();
	    this.getLayer().draw();
	};
    Kinetic.Util.extend(Kinetic.IdeaComment, Kinetic.Group);
}());

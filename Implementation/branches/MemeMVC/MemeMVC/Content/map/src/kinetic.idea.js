/*global MAPJS, Color, _, jQuery, Kinetic*/
/*jslint nomen: true, newcap: true, browser: true*/
(function () {
    'use strict';




    Kinetic.Idea = function (config) {
        var ENTER_KEY_CODE = 13,
			ESC_KEY_CODE = 27,
			self = this;
        this.editable = config.editable;
        this.level = config.level;
        this.mmAttr = config.mmAttr;
        this.isSelected = false;
        this.isActivated = !!config.activated;
        config.draggable = config.level > 1;
        config.name = 'Idea';
        this.rid = "";
        this.bgcolor = "ffffff";
        this.textcolor = "000000";
        this.text = new Kinetic.Text({
            fontSize: 14,
            fontFamily: 'Abel',
            lineHeight: 1.5,
            fontStyle: 'bold',
            align: 'center'
        });
        this.hasNote = true;
        this.hasVideo = false;
        this.hascomment = false;
        self.previousHoveredNode = undefined;
        Kinetic.Group.call(this, config);

        self.SetNote = function (hasnote) {
            self.hasNote = hasnote;
            if (!self.hasNote && self.noteobj != null) {
                self.noteobj.remove();
                self.noteobj = null;
            }
            if (self.hasNote && self.noteobj == null) {
                self.noteobj = new Kinetic.IdeaNote({ x: 0, y: 0, rid: self.rid, nid: config.id });
                //this.noteobj = new Kinetic.IdeaMedia({ x: 0, y: 0, imgsrc: "/Images/imageloading.png", parent: parent });
                self.add(self.noteobj);
                //self.noteobj.moveToTop();

            }
            self.setStyle();
            if (self.getStage() != undefined) self.getStage().draw();
        }
        

        self.SetVideo = function (hasvideo, videourl) {
            self.hasVideo = hasvideo;
            if (!self.hasVideo && self.videoobj != null) {
                self.videoobj.remove();
                self.videoobj = null;
            }
            if (self.hasVideo) {

                if (self.videoobj == null) {

                    self.videoobj = new Kinetic.IdeaVideo({ x: 0, y: 0, parent: self, rid: self.rid, videourl: videourl });
                    self.add(self.videoobj);
                    self.videoobj.moveToTop();
                } else {
                    self.videoobj.videourl = videourl;
                }

            }
            self.setStyle();
            if (self.getStage() != undefined) self.getStage().draw();
        }

        self.SetupIdeaShape = function (type, offset) {
            if (type == null || type == "") type = "rec";
            self.NodeType = type;
            self.rectAttrs = { stroke: '#bbbbbb', strokeWidth: 0.1 };
            switch (type) {
                case "cir":
                    self.bgShape = new Kinetic.Circle({
                        strokeWidth: 0.1,
                        radius: 150,
                        x: offset,
                        y: offset,
                        fill: "#FFFFFF",
                        stroke: "#333333"
                    });
                    self.ideaShape = new Kinetic.Circle({
                        strokeWidth: 0, //0
                        radius: 150,
                        x: offset,
                        y: offset,
                        fill: "#FFFFFF"
                    });
                    //self.rectbg1 = bgRect(0);//8
                    //self.rectbg2 = bgRect(0);//4
                    //self.ShapeElements.push(self.rectbg1);
                    //self.ShapeElements.push(self.rectbg2);
                    break;
                case "dia":
                    self.bgShape =
                         new Kinetic.Rect({
                             strokeWidth: 0.1,//1
                             cornerRadius: 10,
                             x: offset,
                             y: offset,
                             rotationDeg: 45,
                             stroke: "#333333"
                         });
                    self.ideaShape = new Kinetic.Rect({
                        strokeWidth: 0, //0
                        cornerRadius: 10,
                        x: offset,
                        y: offset,
                        rotationDeg: 45,
                        fill: "#FFFFFF"
                    });
                    //self.cirbg1 = bgCir(0);
                    //self.ShapeElements.push(self.cirbg1);
                    break;
                case "rec":
                case "img":
                    self.bgShape =
                         new Kinetic.Rect({
                             strokeWidth: 0.1,//1
                             cornerRadius: 10,
                             x: offset,
                             y: offset,
                             stroke: "#333333"
                         });
                    self.ideaShape = new Kinetic.Rect({
                        strokeWidth: 0, //0
                        cornerRadius: 10,
                        x: offset,
                        y: offset,
                        fill: "#FFFFFF"
                    });
                    //self.cirbg1 = bgCir(0);
                    //self.ShapeElements.push(self.cirbg1);
                    break;
            }
            //self.ShapeElements.push(self.bgShape);
            //self.ShapeElements.push(self.ideaShape);
        }

        self.SetShapeText = function (paramtext) {
            //TEst
            //paramtext = (config.data != undefined && config.data.imgsrc != undefined) ? config.data.imgsrc : "" + "\n" + paramtext;
            //ENd of Test
            var replacement = breakWords(MAPJS.URLHelper.stripLink(paramtext)) ||
                    (paramtext.length < COLUMN_WORD_WRAP_LIMIT ? self.text : (paramtext.substring(0, COLUMN_WORD_WRAP_LIMIT) + '...'));
            self.unformattedText = paramtext;
            self.text.setText(replacement);
            self.attrs.text = replacement;
        };

        self.SetupIdeaImg = function (imgsrc, parent) {
            if (imgsrc != "") {
                this.mediaobj = new Kinetic.IdeaMedia({ x: 0, y: 0, imgsrc: imgsrc, parent: parent });
                this.add(this.mediaobj);
                this.mediaobj.moveToTop();
                //this.getStage().draw();
            }
        }
        self.SetupBgColor = function (colorcode, parent) {
            if (colorcode != "") {
                if (self.ideaShape != undefined) {
                    //self.ideaShape.setFill(colorcode);
                    self.bgcolor = colorcode;
                }
            }
        }
        self.SetupTextColor = function (colorcode, parent) {
            if (colorcode != "") {
                //alert("set textcolor:" + colorcode);
                //self.text.setFill(colorcode);
                self.textcolor = colorcode;
            }
        }

        self.SetData = function (data) {
            var obj = JSON.parse(data);
            //bootbox.alert("data change:"  + data);
            if (self.ImgSrc !== obj.imgsrc) {
                self.ChangeImgSrc(obj.imgsrc);
            }
            if (self.NodeType !== obj.type) {
                self.ChangeShapeType(obj.type);
            }
            if (config.data["bgcolor"] !== obj.bgcolor) {
                self.ChangeBgColor(obj.bgcolor);
            }
            if (config.data["textcolor"] !== obj.textcolor) {
                self.ChangeTextColor(obj.textcolor);
            }
            config.data = data;
        }

        self.unformattedText = config.text;

        self.bgShape = null;
        self.ideaShape = null;
        this.mediaobj = null;
        this.noteobj = null;
        this.commentobj = null;
        this.videoobj = null;
        this.newNodetext = null;
        self.ShapeElements = new Array();
        this.classType = 'Idea';
        this.loadedimgw = 0;
        this.loadedingh = 0;

        //For Node type
        self.NodeType = config.NodeType;
        self.ImgSrc = (config.data != undefined && config.data.imgsrc != undefined) ? config.data.imgsrc : "";



        this.ChangeShapeType = function (type) {

            //alert(SetupIdeaShape);
            this.bgShape.remove();
            this.ideaShape.remove();
            this.SetupIdeaShape(type, 0);
            this.add(this.bgShape);

            //this.add(this.cirbg1);
            this.add(this.ideaShape);
            this.ideaShape.moveToBottom();
            this.bgShape.moveToBottom();
            this.setStyle();
            this.getStage().draw();
            //this.getLayer().draw();
        }

        this.ChangeImgSrc = function (imgsrc) {
            //alert("change image src to :" + imgsrc);
            if (config.data == undefined) {
                config.data = { "imgsrc": imgsrc };
            }
            else config.data["imgsrc"] = imgsrc;
            self.ImgSrc = imgsrc;
            if (this.mediaobj == undefined) {

            }
            else {
                this.mediaobj.remove();
            }

            this.SetupIdeaImg(imgsrc, self);
            if (IsYoutubeVideo(imgsrc)) {
                this.SetVideo(true, imgsrc);
            } else {
                this.SetVideo(false, "");
            }
            this.setStyle();
            this.getStage().draw();
        }

        this.ChangeBgColor = function (colorcode) {

            if (config.data == undefined) {
                config.data = { "bgcolor": colorcode };
            }
            else config.data["bgcolor"] = colorcode;

            this.SetupBgColor(colorcode, self);
            this.setStyle();
            this.getStage().draw();

        }
        this.ChangeTextColor = function (colorcode) {

            if (config.data == undefined) {
                config.data = { "textcolor": colorcode };
            }
            else config.data["textcolor"] = colorcode;

            this.SetupTextColor(colorcode, self);
            this.setStyle();
            this.getStage().draw();
        }

        this.getNodeAttrs = function () {
            return self.attrs;
        };
        this.isVisible = function (offset) {
            var stage = self.getStage();
            return stage && stage.isRectVisible(new MAPJS.Rectangle(self.getX(), self.getY(), self.getWidth(), self.getHeight()), offset);
        };
        this.editNode = function (shouldSelectAll, deleteOnCancel) {
            self.fire(':editing');
            var canvasPosition = jQuery("#" + self.getStage().getContainer().id).offset(),
				ideaInput,
				onStageMoved = _.throttle(function () {
				    ideaInput.css({
				        top: canvasPosition.top + self.getAbsolutePosition().y,
				        left: canvasPosition.left + self.getAbsolutePosition().x
				    });
				}, 10),
				updateText = function (newText) {
				    self.setStyle();
				    self.fire(':textChanged', {
				        text: newText || self.unformattedText,
				        isNew: deleteOnCancel
				    });
				    ideaInput.remove();
				    self.stopEditing = undefined;
				    self.getStage().off('xChange yChange', onStageMoved);
				    self.getStage().draw();
				},
				onCommit = function () {
				    if (ideaInput.val() === '') {
				        onCancelEdit();
				    } else {
				        updateText(ideaInput.val());
				    }
				    self.setStyle();
				    //this.stage().draw();
				    self.getStage().draw();
				},
				onCancelEdit = function () {
				    updateText(self.unformattedText);
				    if (deleteOnCancel) {
				        self.fire(':request', { type: 'undo', source: 'internal' });
				    }
				},
				scale = self.getStage().getScale().x || 1;
            ideaInput = jQuery('<textarea type="text" wrap="soft" class="ideaInput"></textarea>')
				.css({
				    top: canvasPosition.top + self.getAbsolutePosition().y,
				    left: canvasPosition.left + self.getAbsolutePosition().x,
				    width: (6 + self.getWidth()) * scale,
				    height: (6 + self.getHeight()) * scale,
				    'padding': 3 * scale + 'px',
				    'font-size': self.text.getFontSize() * scale + 'px',
				    'line-height': '150%',
				    'background-color': self.getBackground(),
				    'margin': -3 * scale,
				    'border-radius': 4 * scale + 'px',
				    'border': self.rectAttrs.strokeWidth * (2 * scale) + 'px dashed ' + self.rectAttrs.stroke,
				    'color': self.text.getFill(),
				    'overflow': 'hidden'
				})
				.val(self.unformattedText)
				.appendTo('body')
				.keydown(function (e) {
				    if (e.shiftKey && e.which === ENTER_KEY_CODE) {
				        return; // allow shift+enter to break lines
				    }
				    else if (e.which === ENTER_KEY_CODE) {
				        onCommit();
				    } else if (e.which === ESC_KEY_CODE) {
				        onCancelEdit();
				    } else if (e.which === 9) {
				        onCommit();
				        e.preventDefault();
				        self.fire(':request', { type: 'addSubIdea', source: 'keyboard' });
				        return;
				    } else if (e.which === 83 && (e.metaKey || e.ctrlKey)) {
				        e.preventDefault();
				        onCommit();
				        return; /* propagate to let the environment handle ctrl+s */
				    } else if (!e.shiftKey && e.which === 90 && (e.metaKey || e.ctrlKey)) {
				        if (ideaInput.val() === self.unformattedText) {
				            onCancelEdit();
				        }
				    }
				    e.stopPropagation();
				})
				.blur(onCommit)
				.focus(function () {
				    if (shouldSelectAll) {
				        if (ideaInput[0].setSelectionRange) {
				            ideaInput[0].setSelectionRange(0, self.unformattedText.length);
				        } else {
				            ideaInput.select();
				        }
				    } else if (ideaInput[0].setSelectionRange) {
				        ideaInput[0].setSelectionRange(self.unformattedText.length, self.unformattedText.length);
				    }
				})
				.on('input', function () {
				    var mytext = new Kinetic.IdeaDimension({
				        text: ideaInput.val(),
				        NodeType: self.NodeType
				    });
				    mytext.SetupIdea();
				    ideaInput.width(Math.max(ideaInput.width(), mytext.getWidth() * scale));
				    ideaInput.height(Math.max(ideaInput.height(), mytext.getHeight() * scale));
				});
            self.stopEditing = onCancelEdit;
            ideaInput.focus();
            self.getStage().on('xChange yChange', onStageMoved);
        };

        this.addAllPins = function () {
            this.nodepin.show();
        }
        this.removeAllPins = function () {
            this.nodepin.hide();
        }

        this.EditTextMode = function () {
            //if Map node, we will not allow users to edit the text.
            if (this.level === 1) {
                return;
            }
            mapModel.editNode('mouse', false, false);
        }
        self.isPointOverNode = function (x, y, node) { //move to mapModel candidate
            /*jslint eqeq: true*/

            //var node = findNodeOnStage(n.id);

            var rsult = false;

            if (node.NodeType == "rec") {
                rsult = (x >= node.getX() &&
				y >= node.getY() &&
				x <= node.getX() + node.getWidth() &&
				y <= node.getY() + node.getHeight());
            } else if (node.NodeType == "dia") {
                var dim = (node.getWidth() / 2 + node.getHeight() / 2);
                var dim2 = dim * 1.41;
                //if (node.getHeight() > dim) dim = node.getHeight();
                var dis1 = dim * x + dim * y + (node.getX() - node.getHeight() / 2) * (node.getY() - node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
                dis1 = dis1 * dis1 / (2 * dim * dim);
                var dis2 = dim * x - dim * y + (node.getX() + node.getHeight() / 2 + node.getWidth()) * (node.getY() - node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
                dis2 = dis2 * dis2 / (2 * dim * dim);

                var dis3 = -dim * x + dim * y + (node.getX() - node.getHeight() / 2) * (node.getY() + node.getHeight() + node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
                dis3 = dis3 * dis3 / (2 * dim * dim);

                var dis4 = -dim * x - dim * y + (node.getX() + node.getHeight() / 2 + node.getWidth()) * (node.getY() + node.getHeight() + node.getWidth() / 2) - (node.getY() + node.getHeight() / 2) * (node.getX() + node.getWidth() / 2);
                dis4 = dis4 * dis4 / (2 * dim * dim);

                if ((dis1 < dim2 * dim2 & dis2 < dim2 * dim2) && (dis3 < dim2 * dim2 & dis4 < dim2 * dim2)) {
                    //bootbox.alert(dis3 +  ' '+dis1);
                    rsult = true;
                } else {

                    rsult = false;
                }



            } else if (node.NodeType == "cir") {
                var dim = (node.getWidth() / 2 + node.getHeight() / 2) + 60;
                //var dim = node.getWidth();
                //if (node.getHeight() > dim) dim = node.getHeight();
                var centerx = node.getX() + node.getWidth() / 2;
                var centery = node.getY() + node.getHeight() / 2;

                if ((centerx - x) * (centerx - x) + (centery - y) * (centery - y) <= dim * dim / 4) {
                    rsult = true;
                } else { rsult = false; }


            }

            return rsult;

        }
      
        self.setDropableHoverNode = function (setAsDroppable) {
            if (self.previousHoveredNode !== undefined) {
                self.previousHoveredNode.setIsDroppable(setAsDroppable);
                mapModel.analytic('updateDroppableHoverNode');
            }

        };

        self.GetNodePinMouse = function () {
            var a = (self.getStage().getPointerPosition().x - self.nodepin.getAbsolutePosition().x);
            var b = (self.getStage().getPointerPosition().y - self.nodepin.getAbsolutePosition().y);
            return (a*a+b*b <=12*12);

            //return { x: self.nodepin.getAbsolutePosition().x, y: nodepin.getAbsolutePosition().y, r: 5 };
        };


        self.AllowDrag = true;

        this.SetupIdea = function () {

            this.SetupIdeaShape(config.NodeType, 0);
            self.startpointx = 0;
            self.startpointy = 0;
            if (self.editable) {
                //x = 0, y = 0 is relative coordinate of nodepin with the center of this node.
                this.nodepin = createPin('#F7C520', 0, 0);
                
                this.nodepin.on('mouseover', function () {
                    
                    self.nodepin.setActive(true);
                });
                this.nodepin.on('mouseout', function () {
                    self.nodepin.setActive(false);
                });
                this.nodepin.on('click tap', function () {
                    self.nodepin.setActive(true);
                });
                
                this.nodepin.on('dragstart', function (evt) {
                    
                    /*mapModel.updateCenterofFocus(mapModel.LEVELNODEPIN);
                    */
                    //init
                    self.AllowDrag = false;
                    self.previousHoveredNode = undefined;
                    /*if (mapModel.CenterofFocus > mapModel.LEVELNODEPIN) return;*/

                    var canvasPosition = jQuery("#" + self.getStage().getContainer().id).offset();
                    self.startpointy = self.nodepin.getY();
                    self.startpointx = self.nodepin.getX();
                    self.newNodetext = jQuery('<div style="float:left; width:120px; color: #444444; position:absolute; z-index:10000;' +
                        ' line-height:10px; background-color:rgb(255,255,255); padding:5px; "><span style="font-size:16px;font-weight:bold;">' +
                        'Drop the pin</span><br/><spanstyle="font-size:11px;">create new node</span></div>')
                        .appendTo('body');

                    self.newNodetext.css({
                        top: canvasPosition.top + self.startpointy + 10,
                        left: canvasPosition.left + self.startpointx + 10
                    });

                    self.newNodetext.css({
                        top: canvasPosition.top + self.startpointy + 10,
                        left: canvasPosition.left + self.startpointx + 10
                    });



                });
                this.nodepin.on('dragmove', function (evt) {
                    /*if (mapModel.CenterofFocus > mapModel.LEVELNODEPIN) return;
                    */
                    self.AllowDrag = false;
                    var canvasPosition = jQuery("#" + self.getStage().getContainer().id).offset();
                    var distance = (self.nodepin.getY() - self.startpointy) * (self.nodepin.getY() - self.startpointy)
                        + (self.nodepin.getX() - self.startpointx) * (self.nodepin.getX() - self.startpointx);
                    if (distance < 10000) {
                        self.newNodetext.css({
                            top: canvasPosition.top + self.nodepin.getAbsolutePosition().y + 10,
                            left: canvasPosition.left + self.nodepin.getAbsolutePosition().x + 10,
                            visibility: "hidden"
                        });
                    } else {
                        self.newNodetext.css({
                            top: canvasPosition.top + self.nodepin.getAbsolutePosition().y + 10,
                            left: canvasPosition.left + self.nodepin.getAbsolutePosition().x + 10,
                            visibility: "visible"
                        });
                    }
                    var relX = self.nodepin.getX() + self.getX();
                    var relY = self.nodepin.getY() + self.getY();
                    
                    for (var key in mapModel.indexedNodesDict) {
                        var nodeIdea = mapModel.indexedNodesDict[key];
                     
                        if (nodeIdea.attrs.id == self.attrs.id) {
                            continue;
                        }
                        var left = nodeIdea.getX(), top = nodeIdea.getY(), right = left + nodeIdea.getWidth(), bottom = top + nodeIdea.getHeight();
                      
                        if (self.isPointOverNode(relX, relY, nodeIdea)){
                           
                            //console.log("Yes, We hover on some thing");
                            self.newNodetext.css({ visibility: "hidden" });
                            //update
                            self.previousHoveredNode = nodeIdea;
                            self.setDropableHoverNode(true);
                           
                            break;
                        } else {
                            if (self.previousHoveredNode !== undefined) {
                               //update
                                self.setDropableHoverNode(false);
                                self.previousHoveredNode = undefined;
                            }
                        }
                    }
                    
                });


                this.nodepin.on('dragend', function (evt) {
                    /*if (mapModel.CenterofFocus > mapModel.LEVELPIN) return;
                    */
                    self.AllowDrag = false;
                    var distance = (self.nodepin.getY() - self.startpointy) * (self.nodepin.getY() - self.startpointy)
                        + (self.nodepin.getX() - self.startpointx) * (self.nodepin.getX() - self.startpointx);

                    /*if (distance > 10000) {
                        mapModel.updateCenterofFocus(mapModel.LEVELRELEASED);
                    }*/



                    if (self.newNodetext != undefined && self.newNodetext != null) self.newNodetext.remove();



                    this.setX(0);
                    this.setY(0);



                    if (self.previousHoveredNode !== undefined) {
                        //reset after finish
                        //insert a secondary link
                        var mapId = mapId = mv.MapTransVM().MapID();
                        mapModel.getIdea().insertLink(self, self.previousHoveredNode, mapId, "secondLink", true);
                        //reset pinnode
                        self.nodepin.setX(0);
                        self.nodepin.setY(0);
                        //reset hovered node.
                        self.setDropableHoverNode(false);
                        self.previousHoveredNode = undefined;
                        mapModel.updateCenterofFocus(mapModel.LEVELRELEASED);
                        distance = 0;
                    }


                    if (distance > 10000) {


                        mapModel.clickNode(config.id.replace("node_", ""), evt);
                        var idea = mapModel.getIdea();


                        var newId;
                        idea.batch(function () {


                            newId = mapModel.addSubIdea('toolbar', config.id.replace("node_", ""));


                            var newnode = mapModel.getCurrentLayout().nodes[newId];

                            mapModel.dispatchEvent('follownewpin', newnode, evt);

                        });
                    }
                });
            }
            this.SetShapeText(config.text);


            this.add(this.bgShape);
            //this.add(this.cirbg1);
            this.add(this.ideaShape);

            this.add(this.text);



            if (self.editable) {
                this.add(this.nodepin);
                this.removeAllPins();
            }

            if (config != undefined && config.data != undefined && config.data.imgsrc != undefined) {
                this.SetupIdeaImg(config.data.imgsrc, self);
                if (IsYoutubeVideo(config.data.imgsrc)) {

                    this.SetVideo(true, config.data.imgsrc);
                } else {
                    this.SetVideo(false, "");
                }

            } else {
                this.SetVideo(false, "");
            }
            if (config != undefined && config.data != undefined && config.data.bgcolor != undefined) {
                this.SetupBgColor(config.data.bgcolor, self);
            }
            if (config != undefined && config.data != undefined && config.data.textcolor != undefined) {
                this.SetupTextColor(config.data.textcolor, self);
            }
            //console.log("What is going on? " + config.cmtOnNode + " ... " + config.id);
            (config.cmtOnNode !== undefined && config.cmtOnNode > 0) ? this.SetNote(true) : this.SetNote(false);
            //this.SetNote(false);
            this.setStyle();



        }

    };
}());

Kinetic.Idea.prototype.setShadowOffset = function (offset) {
    'use strict';
    offset = this.getMMScale().x * offset;
    offset = 0;
    _.each([this.bgShape, this.ideaShape], function (r) {
        r.setShadowOffset([offset, offset]);
    });

};

Kinetic.Idea.prototype.getMMScale = function () {
    'use strict';
    var stage = this.getStage(),
		scale = (stage && stage.getScaleX()) || this.getScaleX() || 1;
    return { x: scale, y: scale };
};


Kinetic.Idea.prototype.setupShadows = function () {
    'use strict';
    var scale = this.getMMScale().x,
		isSelected = this.isSelected,
		//offset = this.isCollapsed() ? 3 * scale : 4 * scale,
        offset = 1
    /*normalShadow = {
        color: '#666666',
        blur: 0,
        offset: [offset, offset],
        opacity: 1
    },
    selectedShadow = {
        color: '#666666',
        blur: 4 * scale,
        offset: [offset, offset],
        opacity: 0.4 * scale
    },
    noneShadow = {
        color: '#ffffff',
        blur: 2 * scale,
        offset: [offset, offset],
        opacity: 1
    },
    shadow = isSelected ? selectedShadow : noneShadow;*/;

    /*if (this.oldShadow && this.oldShadow.selected === isSelected && this.oldShadow.scale === scale && this.oldShadow.offset === offset) {
        return;
    }
    //this.oldShadow = { selected: isSelected, scale: scale, offset: offset };
    _.each([this.bgShape, this.ideaShape], function (r) {
        ////r.setShadowColor(shadow.color);
        ////r.setShadowBlur(shadow.blur);
        ////r.setShadowOpacity(shadow.opacity);
        ////r.setShadowOffset(shadow.offset);
    });*/
};

Kinetic.Idea.prototype.getBackground = function () {
    'use strict';
    /*jslint newcap: true*/
    var isRoot = this.level === 1,
		defaultBg = Color("#" + this.bgcolor),//MAPJS.defaultStyles[isRoot ? 'root' : 'nonRoot'].background,
		validColor = function (color, defaultColor) {
		    if (!color) {
		        return defaultColor;
		    }
		    var parsed = Color(color).hexString();
		    return color.toUpperCase() === parsed.toUpperCase() ? color : defaultColor;
		};
    return validColor("#" + this.bgcolor, defaultBg);
};


Kinetic.Idea.prototype.setStyle = function () {
    'use strict';

    /*jslint newcap: true*/
    var self = this,
		isDroppable = this.isDroppable,
		isSelected = this.isSelected,
		isActivated = this.isActivated,
		background = this.getBackground(),

		tintedBackground = Color(background).mix(Color("#E3D732")).hexString(),

		rectOffset,
		rectIncrement = 4,
        padding = 10,
		getDash = function () {
		    if (!self.isActivated) {
		        return [];
		    }
		    return [5, 3];
		},
		textSize = {
		    width: Math.max(this.text.getWidth(), 150),
		    height: this.text.getHeight() - 5
		},
		calculatedSize,
		pad = function (box) {
		    return {
		        width: box.width + 2 * padding,
		        height: box.height + 2 * padding
		    };
		},
		positionTextAndIcon = function (w, h, imgw, imgh) {
		    self.text.setX((w - self.text.getWidth()) / 2);
		    if (imgh > 0) {
		        self.text.setY(padding + 10 + imgh);
		    } else {
		        self.text.setY(padding);
		    }

		},
        positionImg = function (w, h, imgw, imgh) {

            self.mediaobj.setX((w - imgw) / 2);
            self.mediaobj.setY(padding);
        },
		calculateMergedBoxSize = function (box1) {

		    return pad({
		        width: box1.width,
		        height: box1.height
		    });
		},
        PositionShapeBg = function (bgShape, ideaShape, calculatedSize, type, hasImg, wimgwidth, wimgheight) {
            if (type == "cir") {
                _.each([ideaShape, bgShape], function (r) {
                    //if (r.setRadius == null) alert("setRadius");
                    r.setRadius(Math.sqrt((calculatedSize.width * calculatedSize.width / 4) + (calculatedSize.height * calculatedSize.height / 4)));
                    r.setX(calculatedSize.width / 2);
                    r.setY(calculatedSize.height / 2);
                });
            }
            if (type == "rec" || type == "img") {
                _.each([ideaShape, bgShape], function (r) {
                    r.setWidth(calculatedSize.width);
                    r.setHeight(calculatedSize.height);
                    r.setY(0);
                });
            }
            if (type == "dia") {
                _.each([ideaShape, bgShape], function (r) {
                    r.setWidth((calculatedSize.width + calculatedSize.height) / Math.sqrt(2))
                    r.setHeight((calculatedSize.width + calculatedSize.height) / Math.sqrt(2));
                    r.setY(-calculatedSize.width / 2);
                    r.setX(calculatedSize.width / 2);
                });
            }
        },

    calculatedSize = pad(textSize);

    var hasimg = (self.ImgSrc != "" && self.ImgSrc != undefined);

    var wimgw = hasimg ? self.loadedimgw : 0;
    var wimgh = hasimg ? self.loadedimgh + 10 + calculatedSize.height : calculatedSize.height;
    var imgh = hasimg ? self.loadedimgh : 0;
    var imgw = wimgw;
    if (wimgw < calculatedSize.width) wimgw = calculatedSize.width;

    calculatedSize.width = wimgw;
    calculatedSize.height = wimgh;
    this.setWidth(wimgw);
    this.setHeight(wimgh);


    if (hasimg) positionImg(wimgw, wimgh, imgw, imgh);
    positionTextAndIcon(wimgw, wimgh, imgw, imgh);
    var end = 0;
    if (this.hasNote && this.noteobj != null) {
        end = self.text.getY() + self.text.getHeight() + 5;
        this.noteobj.setX(wimgw - 20);
        this.noteobj.setY(end - 20);

    }
    if (this.hasVideo && this.videoobj != null) {

        end = self.text.getY() + self.text.getHeight() + 5;
        if (this.hasNote) {
            this.videoobj.setX(wimgw - 20 + 32 + 5);
        } else {
            this.videoobj.setX(wimgw - 20);
        }
        this.videoobj.setY(end - 20);
    }

    PositionShapeBg(self.bgShape, self.ideaShape, calculatedSize, self.NodeType, hasimg, wimgw, wimgh);

    _.each([self.ideaShape, self.bgShape], function (r) {
        r.setFill(background);
        if (isDroppable) {
            r.setFill(tintedBackground);
            r.setStroke('#000000');
            var dashes = [5, 3, 0, 0];
            r.setDashArray(dashes);
        }
    });

    if (isActivated) {
        this.ideaShape.setStroke('#2E9AFE');
        var dashes = [5, 3, 0, 0];
        self.ideaShape.setDashArray(dashes);
    } else {
        this.ideaShape.setStroke('#FFFFFF');
        this.ideaShape.setDashArray([]);
    }
    //this.rect.setDashArray(getDash());
    //this.rect.setStrokeWidth(this.isActivated ? 0 : self.rectAttrs.strokeWidth);

    if (this.isCollapsed()) {
        /*this.rect.setFill('#eee1bf');
	    this.rect.setStroke('#ffba00');
	    this.rect.setStrokeWidth(0);*/
        this.ideaShape.setStrokeWidth('3');
        this.ideaShape.setStroke('#999999');
        self.ideaShape.setDashArray(dashes);
    } else {
        this.ideaShape.setStrokeWidth('1');
    }



    this.setupShadows();
    this.text.setFill("#" + self.textcolor);
};

Kinetic.Idea.prototype.setMMAttr = function (newMMAttr) {
    'use strict';
    this.mmAttr = newMMAttr;
    this.setStyle();
    this.getStage().draw();
};

Kinetic.Idea.prototype.getIsSelected = function () {
    'use strict';
    return this.isSelected;
};

Kinetic.Idea.prototype.isCollapsed = function () {
    'use strict';
    return this.mmAttr && this.mmAttr.collapsed || false;
};

Kinetic.Idea.prototype.setIsSelected = function (isSelected) {
    'use strict';

    this.isSelected = isSelected;
    if (isSelected) {
        this.addAllPins();
    } else {
        this.removeAllPins();
    }

    this.setStyle();

    if (!isSelected && this.stopEditing) {
        this.stopEditing();
    }
    this.getLayer().draw();
};

Kinetic.Idea.prototype.setIsActivated = function (isActivated) {
    'use strict';
    this.isActivated = isActivated;

    this.setStyle();
    this.getStage().draw();
    //	this.getLayer().draw();
};

Kinetic.Idea.prototype.setIsDroppable = function (isDroppable) {
    'use strict';
    this.isDroppable = isDroppable;
    this.setStyle(this.attrs);
};

Kinetic.Util.extend(Kinetic.Idea, Kinetic.Group);



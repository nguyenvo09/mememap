/*jslint eqeq: true, forin: true, nomen: true*/
/*jshint unused:false, loopfunc:true */
/*global _, MAPJS, observable*/
MAPJS.dbcontent = function () {
    'use strict';
    var self = this;
    
    self.Processing = ko.observable(null);
    self.Transition = ko.observable(null);
    self.FFInit = ko.observable(null);
	
    self.processingqueue = new Array();
    self.appendnodequeue = new Array();

    self.QueueFlag = false;
    self.QueueRunningFlag = false;
    self.myQueueTimer = function(args)
    {
        if (self.QueueRunningFlag) return;
        if (!self.QueueFlag) {
            self.QueueFlag = true; return;
        }
        SendQueueToServer();
    }
    self.InsertUserComment = function (message, pouchdb) {
        
        var json = JSON.stringify(message);
       
        $.ajax({
            url: InsertUserComment,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                var commentMongoId = data;
                //alert(val);
                if (commentMongoId !== "") {
                    message['commentMongoId'] = commentMongoId;
                    pouchdb.insertNotification(message);
                }
               
            }
        });
    }
    self.GetTaskConfig = function (taskMaster) {
        var data = { };
        var json = JSON.stringify(data);

        $.ajax({
            url: GetTaskConfigUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                var json = JSON.parse(data);
                taskMaster.setTaskConfig(json);
                
            }
        });
    }
    self.GetTaskDetail = function (nodeId, taskMaster) {
        var data = {nodeId: nodeId};
        var json = JSON.stringify(data);

        $.ajax({
            url: GetTaskDetailUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (ret) {
                //var json = JSON.parse(data);
                //taskMaster.setTaskConfig(json);
                if (ret.statusCode === "200") {
                    var task = JSON.parse(ret.Result);
                    taskMaster.setTaskDetail(task, nodeId);
                }
            }
        });
    }
    self.SaveOrUpdateTask = function (mapId, nodeId, config) {
        //self.TaskId = ko.observable("");
        //self.Title = ko.observable("");
        //self.Estimation = ko.observable("");
        //self.AssignedTo = ko.observable("Bill Nguyen, Koo Wai Mun");
        //self.People = ko.observableArray([]);
        //self.Priority = ko.observable("Low");
        //self.Type = ko.observable("Task");
        //self.Status = ko.observable("New");
        //self.DueDate = ko.observable(Date.now());
        //self.StartDate = ko.observable(Date.now());
        //self.CompletionDate = ko.observable(Date.now());
        
        //alert("Convert time: " + moment(config.CompletionDate()).format('YYYY-MM-DD') + "  Original time: " + config.CompletionDate());

        var data = {
            nodeId: nodeId,
            mapId: mapId,
            taskId: config.TaskId(),
            title: config.Title(),
            estimation: config.Estimation(),
            dueDate: moment(config.DueDate()).format('YYYY-MM-DD'),
            startDate: moment(config.StartDate()).format('YYYY-MM-DD'),
            completeDate: moment(config.CompletionDate()).format('YYYY-MM-DD'),
            type: config.Type(),
            status: config.Status(),
            priority: config.Priority(),
            assignees: [config.AssignedTo()]
            //assignedTo: config.AssignedTo()
        };
        var json = JSON.stringify(data);

        $.ajax({
            url: SaveOrUpdateUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (ret) {
                //console.log("Gia Tri la: " + JSON.stringify(ret));
                
                if (ret.statusCode === "200") {
                    var taskId = ret.taskId;
                    config.TaskId(taskId);
                    FinishSaveOrUpdateTask(config, data);
                }
            }
        });
    }
    self.RemoveCmt = function (mongoId) {
        var data = {commentId: mongoId};
        var json = JSON.stringify(data);

        $.ajax({
            url: RemoveComment,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                if (JSON.stringify(data) === 'true') {
                    $('div#' + mongoId).remove();
                    bootbox.alert("Comment has been removed");
                    
                }
            }
        });
    }
    self.GetAllCommentPerMap = function (mapId, pouchdb) {
        
        var data = { mapId: mapId };
        var json = JSON.stringify(data);
        
        $.ajax({
            url: GetAllCommentPerMap,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                //var val = JSON.stringify(data);
                pouchdb.buildDictMessage(data);
                
            }
        });
    }
    self.GetCountPerNode = function (mapId, pouchdb) {
        
        var data = { mapId: mapId };
        var json = JSON.stringify(data);

        $.ajax({
            url: GetCountPerNode,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                //var val = JSON.stringify(data);
                pouchdb.setCountPerNode(data);

            }
        });
    }
    self.GetCmtPerNode = function (mapId, nodeId, limit, minTime, pouchdb, callback) {
        var data = { mapId: mapId, nodeId: nodeId, limit: limit, lowerBoundTime: minTime };
        var json = JSON.stringify(data);
        //console.log("data goi len server la: " + json);
        $.ajax({
            url: GetCmtPerNode,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
                //alert("Da request thanh cong:" + data);
                //var val = JSON.stringify(data);
               // if (callback === 'updateCmtOnNode')
                pouchdb[callback](data, nodeId);
                //else if(callback === ''')

            }
        });
    }

    self.myQueue = setInterval(self.myQueueTimer, 2000);
    self.AddToQueue = function (type, command) {
        self.processingqueue.push(type + ":" + command);
        if (!self.QueueRunningFlag && self.processingqueue.length > 10) {
            self.QueueFlag = false;
            SendQueueToServer();
        }
    }
    function SendQueueToServer() {
        self.QueueRunningFlag = true;

        if (self.NodePositionQueue != "") {
            self.ClearNodePositionQueue();
        }
        if (self.processingqueue.length > 0) {
            var toget = self.processingqueue.length;
            var processing = self.processingqueue.splice(0, toget);

            var json = JSON.stringify(processing);
            /*if (toget < self.processingqueue.length) {
                self.processingqueue = self.processingqueue.splice(toget, self.processingqueue.length - 1);
            } else {
                self.processingqueue = new Array();
            }*/
            MAPJS.showSLoading();
            $.ajax({
                url: UpdateQueueUrl,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                data: json,
                success: FinishSendQueue
            });
        } else {
            self.QueueRunningFlag = false;
            self.QueueFlag = true;
        }
    }
    function FinishSendQueue(data) {
        MAPJS.hideSLoading();
        if (data.Result == "Success") {
            
        }
        self.QueueRunningFlag = false;
        self.QueueFlag = true;
    }
    self.insertLink = function (firstIdea, secondIdea, mapId, linkName, isSecondary) {
        var link = {
            in_: firstIdea.rid,
            out_: secondIdea.rid,
            mapid: mapId,
            isSecondary: isSecondary,
            name: linkName
        };
        var json = JSON.stringify(link);
        $.ajax({
            url: InsertLinkUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            data: json,
            success: function (data) {
               
                var ideaIdFrom = firstIdea.attrs.id,
                ideaIdTo = secondIdea.attrs.id;
                var idFrom = parseInt(ideaIdFrom.split("_")[1]),
                    idTo = parseInt(ideaIdTo.split("_")[1]);
                ideaIdFrom = idFrom;
                ideaIdTo = idTo;
                
                FinishInsertLink(data, ideaIdFrom, ideaIdTo);
            }
        });
    }
    self.removeLink = function (link) {
        var rid_link = link.rid;
        var data = {"rid_link": rid_link};
        var json = JSON.stringify(data);
        $.ajax({
            url: RemoveLinkUrl,
            type: "POST",
            //contentType: 'application/json; charset=utf-8',
            cache: false,
            data: {ridLink: link.rid},
            success: function (data) {
                //data.ideaIdFrom = link.ideaIdFrom;
                //data.ideaIdTo = link.ideaIdTo;
                FinishRemoveLink(data, link);

            }
        });
    }
    function FinishInsertLink(data, ideaIdFrom, ideaIdTo) {
        //alert(JSON.stringify(data));
        if (data.statusCode === "200") {
            mv.MapTransVM().AddSecondLink(ideaIdFrom, ideaIdTo);
        }
        //alert("inset link success");
    }
    function FinishRemoveLink(data, link) {
        mv.MapTransVM().RemoveSecondLink(link.ideaIdFrom, link.ideaIdTo);

    }

    function FinishSaveOrUpdateTask(taskObj, data) {
        mv.MapTransVM().SaveOrUpdateTask(taskObj, data);
    }
    self.appendSubIdea = function (parentIdea, subIdea) {
        //Need to optimize to get only the parentIdea Idea info. not the whole tree
        var topParent = jQuery.extend({}, parentIdea);
        topParent.ideas = undefined;
        subIdea.mapid = mapID;
      
        subIdea.currentX = 0;
        subIdea.currentY = 0;
        subIdea.hasCustomedLocation = true;
        subIdea.title = "[Sub Idea]";
        subIdea.data = "{\"imgsrc\"=\"\",\"type\"=\"rec\"}";
       

        if (subIdea.rid == undefined || subIdea.rid == null) { self.appendnodequeue.push(subIdea); }

        
        if (parentIdea.rid != undefined) {
            subIdea.level = topParent.level + 1;
            var command = "{\"parentIdea\":" + JSON.stringify(topParent) + ",\"subIdea\":" + JSON.stringify(subIdea) + "}";
	        var json = JSON.stringify({ CommandData: command, Action: "appendSubIdea" });
	        MAPJS.showLoading();
            $.ajax({
                url: AddSubIdeaUrl,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                data: json,
                success: FinishappendSubIdea
            });
        } else {
            subIdea.level = 0;
            var command = "{\"subIdea\":" + JSON.stringify(subIdea) + "}";
            var json = JSON.stringify({ CommandData: command, Action: "appendTopIdea" });
            MAPJS.showLoading();
            $.ajax({
                url: AddTopIdeaUrl,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                data: json,
                success: FinishappendSubIdea
            });
        }
	};
	self.removeSubIdea = function (subIdea) {
	    var topParent = jQuery.extend({}, subIdea);
	    topParent.ideas = undefined;
	    //alert(JSON.stringify(subIdea));
	    var command = "{\"subIdea\":" + JSON.stringify(topParent) + "}";

	    var json = JSON.stringify({ CommandData: command, Action: "removeSubIdea" });
	    MAPJS.showLoading();
	    $.ajax({
	        url: RemoveSubIdeaUrl,
	        type: "POST",
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        data: json,
	        success: FinishremoveSubIdea
	    });

	}

	self.NodePositionQueue = "";
	self.NodePositionQueueId = "";

	self.ClearNodePositionQueue = function () {
	    self.AddToQueue("UpdateNodePosition", self.NodePositionQueue);
	    self.NodePositionQueue = "";
	    self.NodePositionQueueId = "";
	}

	self.updateNodePosition = function (subIdea) {
	    var topParent = jQuery.extend({}, subIdea);
	    topParent.ideas = undefined;
	    if (topParent.rid == undefined || topParent.rid == null) return;
	    if (topParent.rid != self.NodePositionQueueId) {
	        if (self.NodePositionQueue != "") {
	            /*var json = JSON.stringify({ CommandData: self.NodePositionQueue, Action: "updateNodePosition" });
	            MAPJS.showSLoading();
	            $.ajax({
	                url: UpdateNodePositionUrl,
	                type: "POST",
	                contentType: 'application/json; charset=utf-8',
	                cache: false,
	                data: json,
	                success: FinishupdateNodePosition
	            });*/
	            self.AddToQueue("UpdateNodePosition", self.NodePositionQueue);
	        } else {
	            self.AddToQueue("UpdateNodePosition", "{\"subIdea\":" + JSON.stringify(topParent) + "}");
	        }
	        self.NodePositionQueueId = topParent.rid;
	        self.NodePositionQueue = "{\"subIdea\":" + JSON.stringify(topParent) + "}";;
	    } else {
	        if (self.NodePositionQueue != "") {
	            self.NodePositionQueue = "{\"subIdea\":" + JSON.stringify(topParent) + "}";
	        } else {
	            self.NodePositionQueue += "&mmsep;{\"subIdea\":" + JSON.stringify(topParent) + "}";
	        }
	    }
	    mv.MapTransVM().ChangePosition(topParent.id, topParent.currentX, topParent.currentY);

	    //alert(JSON.stringify(subIdea));
	    var command = "{\"subIdea\":" + JSON.stringify(topParent) + "}";

	    

	}

	self.NodeQueue = "";
	self.NodeQueueId = "";

	self.updateNode = function (subIdea) {
	    
	    var topParent = jQuery.extend({}, subIdea);
	    topParent.ideas = undefined;
	    
	    if (topParent.rid == undefined || topParent.rid == null) return;
	    

	    /*if (topParent.rid != self.NodeQueueId) {
	        if (self.NodeQueue != "") {
	            var json = JSON.stringify({ CommandData: self.NodeQueue, Action: "updateNode" });
	            MAPJS.showSLoading();
	            $.ajax({
	                url: UpdateNodeUrl,
	                type: "POST",
	                contentType: 'application/json; charset=utf-8',
	                cache: false,
	                data: json,
	                success: FinishupdateNode
	            });
	            self.AddToQueue("UpdateNode", self.NodeQueue);
	        } else {
	            self.AddToQueue("UpdateNode", "{\"subIdea\":" + JSON.stringify(topParent) + "}");
	        }
	        self.NodeQueueId = topParent.rid;
	        self.NodeQueue = "{\"subIdea\":" + JSON.stringify(topParent) + "}";;
	    } else {
	        if (self.NodeQueue != "") {
	            self.NodeQueue = "{\"subIdea\":" + JSON.stringify(topParent) + "}";
	        } else {
	            self.NodeQueue += "&mmsep;{\"subIdea\":" + JSON.stringify(topParent) + "}";
	        }
	    }*/


	    self.AddToQueue("UpdateNode", "{\"subIdea\":" + JSON.stringify(topParent) + "}");
	    var command = "{\"subIdea\":" + JSON.stringify(topParent) + "}";
	    mv.MapTransVM().UpdateNode(topParent.id, topParent.title, topParent.currentX, topParent.currentY, topParent.rid, topParent.data, topParent.isCollapsed, topParent.hasCustomedLocation);

	  
	}

	self.updateParent = function (rid, parentrid, oldparentrid, ideaId, newParentId) {
	    
	    //alert(JSON.stringify(subIdea));
	    var command = "{\"r1\":\"" + rid + "\",\"r2\":\"" + parentrid + "\",\"r3\":\"" + oldparentrid + "\",\"r4\":\"" + mapID + "\"}";
	    
	    var json = JSON.stringify({ CommandData: command, Action: "updateParent" });
	    MAPJS.showLoading();
	    $.ajax({
	        url: UpdateParentUrl,
	        type: "POST",
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        data: json,
	        success: function (data) {
	            data.ideaId = ideaId;
	            data.newParentId = newParentId;
	            FinishupdateParent(data);
	        }
	    });
	}

	function FinishappendSubIdea(data) {
	    
	    if(data.Result == "Success"){
	        var item = data.Idea;
	        for (var i = 0; i < self.appendnodequeue.length; i++) {
	            if (self.appendnodequeue[i].id == item.id) {
	                self.appendnodequeue[i].rid = item.rid;
	                mapModel.dispatchEvent('ridupdated', self.appendnodequeue[i].id, item.rid);
	                mv.MapTransVM().AddNode(self.appendnodequeue[i].id, self.appendnodequeue[i].title, self.appendnodequeue[i].currentX, self.appendnodequeue[i].currentY, item.rid, mapModel.getIdea().findParent(self.appendnodequeue[i].id).id);

	                

	                self.updateNode(self.appendnodequeue[i]);
	                self.appendnodequeue.splice(i, 1);
	            }
	        }
	        MAPJS.hideLoading();
	    }
	}
	function FinishremoveSubIdea(data) {
	    MAPJS.hideLoading();
	    if (data.Result == "Success") {
	        
	        mv.MapTransVM().RemoveNode(data.Idea.id);


	    }
	}

	function FinishupdateParent(data) {
	    MAPJS.hideLoading();
	    if (data.Result == "Success") {
	        mv.MapTransVM().UpdateParent(data.ideaId, data.newParentId);
	    }
	}

	function FinishupdateNodePosition(data) {
	    MAPJS.hideSLoading();
	    if (data.Result == "Success") {

	    }
	}
	function FinishupdateNode(data) {
	    MAPJS.hideSLoading();
	    if (data.Result == "Success") {
	        
	    }
	}


	var pingAlive= function(){

	};

};

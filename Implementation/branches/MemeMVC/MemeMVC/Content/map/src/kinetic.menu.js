/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

    function createMenuItem(color, xpos, ypos,id , iconrl , menufnc, desc, icon) {
        var configpin = { draggable: true }
        var pin = new Kinetic.Group(configpin),
		cirProps1 = {
		    radius: 24,
		    strokeWidth: 2,
		    stroke: '#555555',
		    x: xpos,
		    y: ypos,
		    fill: color,
		}
        ,
		circle1 = new Kinetic.Circle(cirProps1);
        pin.pointCircle = circle1;
        pin.menuColor = color;

        pin.menuId = "menu" + id;
        pin.add(circle1);


        icon.setX(xpos - 12);
        icon.setY(ypos - 12);

        pin.add(icon);


        pin.setActive = function (isActive, actiontext) {


            if (isActive && actiontext != undefined && actiontext!=null) {                
                actiontext.find(".actiondesc").html(this.Description);
            }
            this.pointCircle.setFill(isActive ? "#FFFFCD" : this.menuColor);
            //circle2.fill(isActive ? 'black' : '#F7C520');
            //this.getLayer().draw();
        };
        pin.getRadius = function () {
            
            return this.pointCircle.attrs.radius;
        }
        pin.setActive(true);
        
        pin.MenuFunction = menufnc;

        pin.Description = desc;

        return pin;
    }

    function canDropOnMenuitem (menuitem, menupinitem) {
        return (menuitem.pointCircle.getAbsolutePosition().x - menuitem.getRadius() < menupinitem.pointCircle.getAbsolutePosition().x &&
                menuitem.pointCircle.getAbsolutePosition().x + menuitem.getRadius() > menupinitem.pointCircle.getAbsolutePosition().x &&
                menuitem.pointCircle.getAbsolutePosition().y - menuitem.getRadius() < menupinitem.pointCircle.getAbsolutePosition().y &&
                menuitem.pointCircle.getAbsolutePosition().y + menuitem.getRadius() > menupinitem.pointCircle.getAbsolutePosition().y)
    }

    Kinetic.Menu = function (config) {
        var self = this,
            isActivated = false;
        
        /*this.shadowbg = new Kinetic.Circle({
            x: 157,
            y: 20,
            radius: 70,
            fill: 'red',
            stroke: 'black',
            strokeWidth: 0
        });*/

        var icon1 = new Kinetic.Icon({ url: "/Images/icons/trashcan.png", width: 24, height: 24 });
        var icon2 = new Kinetic.Icon({ url: "/Images/icons/edit-icon.png", width: 24, height: 24 });
        //var icon3 = new Kinetic.Icon({ url: "http://www.tamtampro.com/static/image/tamtam/edit-icon.png", width: 24, height: 24 });
        //var icon4 = new Kinetic.Icon({ url: "http://www.tamtampro.com/static/image/tamtam/edit-icon.png", width: 24, height: 24 });

        var func1 = function () {
            bootbox.confirm("Are you sure you want to delete this item?", function (result) {
                mapModel.removeSubIdea("menu");
            });
        };

        var func2 = function () {
            mv.Title("Select Node");
            mv.PopUpModelVM(new PopUpModel("Select Node Shape"));
            mv.PopUpModelVM().LoadNodeTypeSelection();
            $("#popupNodeTypeselection").modal({ keyboard: true });
            mv.PopUpModelVM().addEventListener('ReturnPopUp', runNodeType);
        };

        var runNodeType = function (typevalue) {
            if (typevalue == "rec") {
            } else
            if (typevalue == "cir") {
            }else
            if (typevalue == "img") {
            }
            var datastr = JSON.stringify({ 'type': typevalue });
            mapModel.updateNodeData(datastr);
            mapModel.updateNodeShape(typevalue);
            //alert('Node Type Updated');
            $("#popupNodeTypeselection").modal('hide');


        }

        var func3 = function () {
            bootbox.alert("Delete Item (temp) 3");
        };
        var func4 = function () {
            bootbox.alert("Delete Item (temp) 4");
        };
        

        this.item1 = createMenuItem("#eeeeee", 0, -80, "1", "", func1,"Delete Item", icon1);
        this.item2 = createMenuItem("#eeeeee", 80, 0, "2", "", func2, "Change Node Shape", icon2);
        //this.item3 = createMenuItem("#eeeeee", -80, 0, "3", "", func3, "Delete Item (temp) 3", icon3);
        //this.item4 = createMenuItem("#eeeeee", 0, 80, "4", "", func4, "Delete Item (temp) 4", icon4);
        
        this.shapeType = 'Menu';
        


        Kinetic.Group.call(this, config);
		
		this.add(this.item1);
		this.add(this.item2);
		//this.add(this.item3);		
		//this.add(this.item4);
		
		this.appearMenu = function (nodeitem, x, y) {
		    
		    isActivated = true;
		    
		    
		    self.setX(x);
		    self.setY(y);
		    self.show();
            

		    self.moveToTop();
		    self.item1.moveToTop();
		    self.item2.moveToTop();
		    //self.item3.moveToTop();
		    //self.item4.moveToTop();
		}
		this.disappearMenu = function (nodeitem) {
		    isActivated = false;
		    this.hide();
		};

		this.trackwithMenuItem = function (menupinitem, actiontext) {
		    
		    self.item1.setActive(canDropOnMenuitem(self.item1, menupinitem), actiontext);
		    self.item2.setActive(canDropOnMenuitem(self.item2, menupinitem), actiontext);
		    //self.item3.setActive(canDropOnMenuitem(self.item3, menupinitem), actiontext);
		    //self.item4.setActive(canDropOnMenuitem(self.item4, menupinitem), actiontext);
		};


		this.trackanddoMenuAction = function (menupinitem) {
		   
		    if (canDropOnMenuitem(self.item1, menupinitem))
		    {
		        self.item1.MenuFunction();
		    }
		    else if (canDropOnMenuitem(self.item2, menupinitem))
		    {
		        self.item2.MenuFunction();
		    }
		    /*else if (canDropOnMenuitem(self.item3, menupinitem)) {
		        self.item3.MenuFunction();
		    }
		    else if (canDropOnMenuitem(self.item4, menupinitem)) {
		        self.item4.MenuFunction();
		    }*/
		}

		this.hide();
	};
    
	
	
    Kinetic.Menu.prototype.setStyle = function () {
	    'use strict';
	    /*jslint newcap: true*/
	    var self = this;
	};

	Kinetic.Menu.prototype.setMMAttr = function (newMMAttr) {
	    'use strict';
	    this.mmAttr = newMMAttr;
	    this.setStyle();
	    this.getLayer().draw();
	};
	Kinetic.Util.extend(Kinetic.Menu, Kinetic.Group);
}());

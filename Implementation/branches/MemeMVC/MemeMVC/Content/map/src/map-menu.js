

var MapMenu = function () {
    var self = this;
    self.EDITTITLE = 2; self.EDITPICTURE = 1; self.EDITCOLOR = 3, self.EDIT_SHAPE = 4, self.EDIT_COMMENTS = 5; self.EDIT_TASKS = 7;
    self.RegisterMenu = function () {
        $(".toolbaractionadd").on("click", function (evt) {
            mapModel.dispatchEvent('Subnodeadded');
        });
        $(".toolbaractionpicture").on("click", function (evt) {
            //LaunchEditPictureDialog();
            LaunchEditPupupMenu(self.EDITPICTURE);
        });
        $(".toolbaractioncolor").on("click", function (evt) {
            //LaunchEditColorDialog();
            LaunchEditPupupMenu(self.EDITCOLOR);
        });
        $(".toolbaractionshape").on("click", function (evt) {
            //ChangeShapeFunc();
            LaunchEditPupupMenu(self.EDIT_SHAPE);
        });
        $(".toolbaractionedit").on("click", function (evt) {
            //LaunchEditDialog();
            LaunchEditPupupMenu(self.EDITTITLE);
        });
        $(".toolbaractiondelete").on("click", function (evt) {
            DeleteFunc();
        });
        $(".toolbaractioncollapse").on("click", function (evt) {
            ToggleCollapseFunc();
        });
        $(".toolbaractiontask").on("click", function (evt) {
            LaunchEditPupupMenu(self.EDIT_TASKS);
        });

        $(".toolbaractionaddcancel").on("click", function (evt) {
            DeselectNode();
            mapModel.dispatchEvent('deselectedNodes');
        });
        $(".toolbaractionstandalone").on("click", function (evt) {
            
            //console.log("standaonle menu clicked");
            
            var node_id, rootNode_id;

            var node_id = mapModel.getCurrentlySelectedIdeaId(); //node.attrs.id;
            if (node_id == undefined) {
                return;
            }
            
            //get Root node
            rootNode_id = mapModel.getRootNodeId();

            //update layout
            var changed = {'level' : 0, 'isStandAlone' : true};
            mapModel.updateNodeProperties(changed);
            mapModel.getIdea().changeParent(node_id, rootNode_id);
            mv.HideNodeToolbar();
            
        });
        $(".toolbaractioncomment").on("click", function (evt) {
            //console.log("Edit comment is running");
            //mapModel.dispatchEvent('CenterToSpecificNode', "1018");
            LaunchEditPupupMenu(self.EDIT_COMMENTS);
        });
    }

    var DeleteFunc = function () {
        bootbox.confirm("Are you sure you want to delete this item?", function (result) {
            if (result) {
                mapModel.removeSubIdea("menu");
            }
        });
    };

    var ChangeShapeFunc = function () {
        mv.Title("Select Node");
        mv.PopUpModelVM(new PopUpModel("Select Node Shape"));
        mv.PopUpModelVM().LoadNodeTypeSelection();
        $("#popupNodeTypeselection").modal({ keyboard: true });
        mv.PopUpModelVM().addEventListener('ReturnPopUp', runNodeType);
    };

    var LaunchEditDialog = function () {
        mapModel.dispatchEvent('LaunchEditNode');
    }
    var ToggleCollapseFunc = function () {
        mapModel.dispatchEvent('ToogleCollapseNode');
    }
    var LaunchEditPictureDialog = function () {
        mapModel.dispatchEvent('LaunchPictureEditNode');
    }
    var LaunchEditColorDialog = function () {
        mapModel.dispatchEvent('LaunchColorEditNode');
    }
    //@param actionType : Integer self.EDITTITLE = 2; self.EDITPICTURE = 1; self.EDITCOLOR = 3; 
    //dispatch to kinetic-mediator.js 
    var LaunchEditPupupMenu = function (actionType) {
        mv.HideNodeToolbar();
        mapModel.dispatchEvent("LaunchEditPopupMenu", actionType);

    }
    var runNodeType = function (typevalue) {
        if (typevalue == "rec") {
        } else
            if (typevalue == "cir") {
            } else
                if (typevalue == "img") {
                }
        var datastr = JSON.stringify({ 'type': typevalue });
        mapModel.updateNodeShapeData(typevalue);
        mapModel.updateNodeShape(typevalue);
        //alert('Node Type Updated');
        $("#popupNodeTypeselection").modal('hide');


    }

    var DeselectNode = function () {
        mv.HideNodeToolbar();
    }


};

/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

    
    function createIcon(xpos, ypos) {
        var icon = new Kinetic.Image({
            x: xpos,
            y: ypos,
            width: 0,
            height: 0
        });
        icon.oldDrawScene = icon.drawScene;
        icon.updateMapjsAttribs = function (iconHash) {
            var safeIconProp = function (name) {
                return iconHash && iconHash[name];
            },
				imgUrl = safeIconProp('url'),
				imgWidth = safeIconProp('width'),
				imgHeight = safeIconProp('height');

            if (this.getAttr('image') && this.getAttr('image').src !== imgUrl) {
                this.getAttr('image').src = imgUrl || '';
            }
            this.setAttr('mapjs-image-url', imgUrl);
            if (this.getAttr('width') !== imgWidth) {
                this.setAttr('width', imgWidth);
            }
            if (this.getAttr('height') !== imgHeight) {
                this.setAttr('height', imgHeight);
            }
            this.setVisible(imgUrl);
        };
        icon.initMapjsImage = function () {
            var self = this,
				imageSrc = this.getAttr('mapjs-image-url');
            if (!imageSrc) {
                return;
            }
            if (!this.getAttr('image')) {
                this.setAttr('image', new Image());
                this.getAttr('image').onload = function loadImage() {
                    self.getLayer().draw();
                };
                this.getAttr('image').src = imageSrc;
            }
        };
        icon.drawScene = function () {
            if (!this.getAttr('image')) {
                this.initMapjsImage();
            }
            if (this.getAttr('mapjs-image-url')) {
                this.oldDrawScene.apply(this, arguments);
            }
        };
        return icon;
    }

    Kinetic.Icon = function (config) {
        var self = this,
            isActivated = false;


        this.shapeType = 'Icon';


        Kinetic.Group.call(this, config);


        this.image1 = createIcon(0,0);
        this.image1.updateMapjsAttribs({ url: config.url, width: config.width, height: config.height });

        this.add(this.image1);
    };



    Kinetic.Icon.prototype.setStyle = function () {
        'use strict';
        /*jslint newcap: true*/
        var self = this;
    };

    Kinetic.Icon.prototype.setMMAttr = function (newMMAttr) {
        'use strict';
        this.mmAttr = newMMAttr;
        this.setStyle();
        this.getLayer().draw();
    };
    Kinetic.Util.extend(Kinetic.Icon, Kinetic.Group);
}());

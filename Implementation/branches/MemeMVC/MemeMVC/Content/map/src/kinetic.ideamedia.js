/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

   
    var defaultimage= new Image();
    defaultimage.src = "/Images/imageloading.png";

    function createMediaImage(xpos, ypos, outparent) {
        
        var icon = new Kinetic.Image({
            x: xpos,
            y: ypos,
            width: 0,
            height: 0,
            stroke: "#C4C4C4",
            strokeWidth: 1
        });
        icon.outparent = outparent;
        icon.oldDrawScene = icon.drawScene;
        icon.updateMapjsAttribs = function (iconHash) {
            var safeIconProp = function (name) {
                return iconHash && iconHash[name];
            },
				imgUrl = safeIconProp('url'),
				imgWidth = safeIconProp('width'),
				imgHeight = safeIconProp('height');
          
            if (this.getAttr('image') && this.getAttr('image').src !== imgUrl) {
                this.getAttr('image').src = imgUrl || '';
            }
            this.setAttr('mapjs-image-url', imgUrl);
            if (this.getAttr('width') !== imgWidth) {
                this.setAttr('width', imgWidth);
            }
            if (this.getAttr('height') !== imgHeight) {
                this.setAttr('height', imgHeight);
            }
            this.setVisible(imgUrl);
        };
        icon.initMapjsImage = function () {
            var self = this,
				imageSrc = this.getAttr('mapjs-image-url');
            if (!imageSrc) {
                return;
            }
            if (!this.getAttr('image')) {
                //this.setAttr('image', new Image());
                this.setAttr('image', defaultimage);
                self.setSize({ width: defaultimage.width, height: defaultimage.height });
                self.outparent.loadedimgw = defaultimage.width;
                self.outparent.loadedimgh = defaultimage.height;
                self.outparent.setStyle();
                //self.getStage().draw();
                var loadingimage = new Image();
                loadingimage.onload = function loadImage() {
                    self.setAttr('image', this);
                    var w = this.width;
                    var h = this.height;
                    
                    var r = w * 1.000 / h;
                    if (r > 1 && w > 150) {
                        w = 150;
                        h = 150 / r;
                    } else if (r <= 1 && h > 150) {
                        h = 150;
                        w = 150 * r;
                    }
                    
                    self.setSize({ width: w, height: h });
                    //this.width = w;
                    //this.height = h;
                    //self.setY(-h-10);
                    self.outparent.loadedimgw = w;
                    self.outparent.loadedimgh = h;
                    self.outparent.setStyle();
                    self.getStage().draw();
                    
                };
                if (imageSrc.indexOf("http") == 0) {
                    if (IsYoutubeVideo(imageSrc)) {
                        loadingimage.src = "http://img.youtube.com/vi/" + ExtractYoutubeVideoId(imageSrc) + "/sddefault.jpg";
                        
                    } else {
                        //imageSrc = imageSrc.replace("https", "http");
                        //loadingimage.src = "http://api.imgble.com/" + imageSrc + "/150";

                        loadingimage.src = imageSrc;
                    }
                    
                }
                
            }
        };
        icon.drawScene = function () {
            if (!this.getAttr('image')) {
                this.initMapjsImage();
                
            }
            if (this.getAttr('mapjs-image-url')) {
                this.oldDrawScene.apply(this, arguments);
            }
        };
        return icon;
    }

    Kinetic.IdeaMedia = function (config) {
        var self = this,
            isActivated = false;
        
        
        this.shapeType = 'IdeaMedia';
        
        Kinetic.Group.call(this, config);
		
      
        try {
            this.image1 = createMediaImage(0,0,config.parent);
            this.image1.updateMapjsAttribs({ url: config.imgsrc  });
            
            this.add(this.image1);
        } catch (err) {

        }
	};
    
	
	
    Kinetic.IdeaMedia.prototype.setStyle = function () {
	    'use strict';
	    /*jslint newcap: true*/
	    var self = this;
	};

    Kinetic.IdeaMedia.prototype.setMMAttr = function (newMMAttr) {
	    'use strict';
	    this.mmAttr = newMMAttr;
	    this.setStyle();
	    this.getLayer().draw();
	};
	Kinetic.Util.extend(Kinetic.IdeaMedia, Kinetic.Group);
}());

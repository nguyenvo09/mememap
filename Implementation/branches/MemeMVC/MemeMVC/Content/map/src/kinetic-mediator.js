/*global _, Kinetic, MAPJS */
if (Kinetic.Stage.prototype.isRectVisible) {
    throw ('isRectVisible already exists, should not mix in our methods');
}

Kinetic.Tween.prototype.reset = function () {
    'use strict';
    this.tween.reset();
    return this;
};

MAPJS.Rectangle = function (x, y, width, height) {
    'use strict';
    this.scale = function (scale) {
        return new MAPJS.Rectangle(x * scale, y * scale, width * scale, height * scale);
    };
    this.translate = function (dx, dy) {
        return new MAPJS.Rectangle(x + dx, y + dy, width, height);
    };
    this.inset = function (margin) {
        return new MAPJS.Rectangle(x + margin, y + margin, width - (margin * 2), height - (margin * 2));
    };
    this.xscale = function (scale) {
        this.x *= scale;
        this.y *= scale;
        this.width *= scale;
        this.height *= scale;
        return this;
    };
    this.xtranslate = function (dx, dy) {
        this.x += dx;
        this.y += dy;
        return this;
    };
    this.xinset = function (margin) {
        this.x += margin;
        this.y += margin;
        this.width -= margin * 2;
        this.height -= margin * 2;
        return this;
    };
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
};
Kinetic.Stage.prototype.isRectVisible = function (rect, offset) {
    'use strict';
    offset = offset || { x: 0, y: 0, margin: 0 };
    var scale = this.getScale().x || 1;
    rect = rect.xscale(scale).xtranslate(offset.x, offset.y).xinset(offset.margin);
    return !(
		rect.x + this.getX() > this.getWidth() ||
		rect.x + rect.width + this.getX() < 0 ||
		rect.y + this.getY() > this.getHeight() ||
		rect.y + rect.height + this.getY() < 0
	);
};

MAPJS.KineticMediator = function (mapModel, stage) {
    'use strict';
    var layer = new Kinetic.Layer(),
		nodeByIdeaId = {},
		connectorByFromIdeaIdToIdeaId = {},
		connectorKey = function (fromIdeaId, toIdeaId) {
		    return fromIdeaId + '_' + toIdeaId;
		},
		atLeastOneVisible = function (list, deltaX, deltaY) {
		    var margin = Math.min(stage.getHeight(), stage.getWidth()) * 0.1;
		    return _.find(list, function (node) {
		        return node.isVisible({ x: deltaX, y: deltaY, margin: margin });
		    });
		},
		moveStage = function (deltaX, deltaY) {
		    var visibleAfterMove, vis3ibleBeforeMove;
		    if (!stage) {
		        return;
		    }

		    visibleBeforeMove = atLeastOneVisible(nodeByIdeaId, 0, 0) || atLeastOneVisible(connectorByFromIdeaIdToIdeaId, 0, 0);
		    visibleAfterMove = atLeastOneVisible(nodeByIdeaId, deltaX, deltaY) || atLeastOneVisible(connectorByFromIdeaIdToIdeaId, deltaX, deltaY);
		    if (visibleAfterMove || (!visibleBeforeMove)) {
		        if (deltaY !== 0) { stage.setY(stage.getY() + deltaY); }
		        if (deltaX !== 0) { stage.setX(stage.getX() + deltaX); }

		        stage.draw();
		    }
		},
		resetStage = function () {
		    new Kinetic.Tween({
		        node: stage,
		        x: 0.5 * stage.getWidth(),
		        y: 0.5 * stage.getHeight(),
		        scaleX: 1,
		        scaleY: 1,
		        easing: Kinetic.Easings.EaseInOut,
		        duration: 0.05,
		        onFinish: function () {
		            stage.fire(':scaleChangeComplete');
		        }
		    }).play();
		    //$('div.stage_x').text(stage.getX());
		    //$('div.stage_y').text(stage.getY());
		},
        centerStage = function (cx, cy) {
            if (cx === 0 && cy === 0) {
                return;
            }
            stage.setX(cx);
            stage.setY(cy);
            stage.draw();
        },
		ensureSelectedNodeVisible = function (node) {
		    var scale = stage.getScale().x || 1,
				offset = 100,
				move = { x: 0, y: 0 };
		    if (!node.getIsSelected()) {
		        return;
		    }
		    if (node.getAbsolutePosition().x + node.getWidth() * scale + offset > stage.getWidth()) {
		        move.x = stage.getWidth() - (node.getAbsolutePosition().x + node.getWidth() * scale + offset);
		    } else if (node.getAbsolutePosition().x < offset) {
		        move.x = offset - node.getAbsolutePosition().x;
		    }
		    if (node.getAbsolutePosition().y + node.getHeight() * scale + offset > stage.getHeight()) {
		        move.y = stage.getHeight() - (node.getAbsolutePosition().y + node.getHeight() * scale + offset);
		    } else if (node.getAbsolutePosition().y < offset) {
		        move.y = offset - node.getAbsolutePosition().y;
		    }
		    new Kinetic.Tween({
		        node: stage,
		        x: stage.getX() + move.x,
		        y: stage.getY() + move.y,
		        duration: 0.4,
		        easing: Kinetic.Easings.EaseInOut,
		        onFinish: function () {
		            mv.ResetNodeToolbarPos(move.x, move.y);
		        }
		    }).play();
		};


    stage.add(layer);


    layer.on('mouseover', function () {
        stage.getContainer().style.cursor = 'pointer';
    });
    layer.on('mouseout', function () {
        stage.getContainer().style.cursor = 'auto';
    });
    mapModel.addEventListener('addLinkModeToggled', function (isOn) {
        stage.getContainer().style.cursor = isOn ? 'crosshair' : 'auto';
        layer.off('mouseover mouseout');
        layer.on('mouseover', function () {
            stage.getContainer().style.cursor = isOn ? 'alias' : 'pointer';
        });
        layer.on('mouseout', function () {
            stage.getContainer().style.cursor = isOn ? 'crosshair' : 'auto';
        });
    });
    mapModel.addEventListener('nodeEditRequested', function (nodeId, shouldSelectAll, editingNew) {
        var node = nodeByIdeaId[nodeId];
        if (node) {
            node.editNode(shouldSelectAll, editingNew);
        }
    });

    mapModel.addEventListener('nodeHasNote', function (rid, hasNote) {
        var item;

        for (item in nodeByIdeaId) {
            //alert(item.toString());

            if (nodeByIdeaId[item].rid == rid) {

                nodeByIdeaId[item].SetNote(hasNote);
            }
        }

    });
    mapModel.addEventListener('nodeReceiveNoti', function (nodeId, hasNote) {
        if (nodeByIdeaId[nodeId] != null) {
            nodeByIdeaId[nodeId].SetNote(hasNote);
        }
    });
    mapModel.addEventListener('ridupdated', function (id, rid) {
        if (nodeByIdeaId[id] != null) {
            nodeByIdeaId[id].rid = rid;
            var node = mapModel.LookForNodeIndex(id);
            if (node != null) {
                node.RId = rid;
            }
        }
    });

    mapModel.addEventListener('nodeCreated', function (n) {
        var cmtOnNode = PouchDbNotification.countPerNode[n.id];
        (cmtOnNode === undefined) ? cmtOnNode = 0 : cmtOnNode = cmtOnNode;

        var type = "rec";
        var obj = undefined;
        if (n.data != undefined && n.data != "") {
            try {
                n.data = n.data.replace(new RegExp('"="', 'g'), '":"');
                obj = JSON.parse(n.data);
                
                type = obj.type;

            } catch (err) {
                bootbox.alert(err.toString());
                obj = {};
                n.data = '{"type":"rec","imgsrc":""}';
            }
        } else {
            obj = {};
        }
        var node = undefined;

        node = new Kinetic.Idea({
            level: n.level,
            x: n.x,
            y: n.y,
            text: n.title,
            mmAttr: n.attr,
            opacity: 1,
            id: 'node_' + n.id,
            activated: n.activated,
            data: obj,
            cmtOnNode: cmtOnNode,
            NodeType: type,
            editable: mapModel.editable
        });

        node.rid = n.rid;
        node.SetupIdea();
        /*if (type == "rec") {
            node = new Kinetic.IdeaRec({
                level: n.level,
                x: n.x,
                y: n.y,
                text: n.title,
                mmAttr: n.attr,
                opacity: 1,
                id: 'node_' + n.id,
                activated: n.activated,
                data: n.data
            });
        } else if (type == "cir") {
            node = new Kinetic.IdeaCir({
                level: n.level,
                x: n.x,
                y: n.y,
                text: n.title,
                mmAttr: n.attr,
                opacity: 1,
                id: 'node_' + n.id,
                activated: n.activated,
                data: n.data
            });
        } else if (type == "img") {
            node = new Kinetic.IdeaImg({
                level: n.level,
                x: n.x,
                y: n.y,
                text: n.title,
                mmAttr: n.attr,
                opacity: 1,
                id: 'node_' + n.id,
                activated: n.activated,
                data: n.data
            });
        }*/
        //NODECLICK
        node.on('click', function (evt) {
            evt.evt.cancelBubble = true;

            evt.evt.preventDefault();

            mapModel.clickNode(n.id, evt);
            //bootbox.alert("Click" + objToString(evt));
            if (evt.evt.which == 3) {
                mv.LaunchNodeToolbar(this, evt.evt);
            } else {
                mv.HideNodeToolbar();
            }
        });

        node.on('tap', function (evt) {
            evt.evt.cancelBubble = true;
            evt.evt.preventDefault();
            mapModel.clickNode(n.id, evt);
            //bootbox.alert("Tap" +objToString( evt));
            if (evt.evt.which == 3) {
                mv.LaunchNodeToolbar(this, evt.evt);
            } else {
                mv.HideNodeToolbar();
            }

        });

        node.on('dbltap', function (evt) {
            evt.evt.cancelBubble = true;
            evt.evt.preventDefault();
            mapModel.clickNode(n.id, evt);
            this.EditTextMode();

        });


        node.on('dblclick', function (evt) {
            evt.evt.cancelBubble = true;
            evt.evt.preventDefault();
            if (evt.evt.which == 1) {
                mapModel.clickNode(n.id, evt);
                this.EditTextMode();

            }

        });
        //node.on('mouseover', function (evt) {
        //    //alert("mouseover node");
        //    console.log("mouse over " + node.attrs.id);
        //    mapModel.setCurrentlyHoveredIdea(node);
        //});
        //node.on('mouseout', function (evt) {
        //    //alert("mouseout node");
        //    console.log("mouse out " + node.attrs.id);
        //    mapModel.setCurrentlyHoveredIdea(undefined);
        //});
        if (mapModel.editable) {
            /*node.on('dblclick dbltap', function () {
                if (!mapModel.getEditingEnabled()) {
                    mapModel.toggleCollapse('mouse');
                    return;
                }
                mapModel.editNode('mouse', false, false);
            });*/



            node.on(':textChanged', function (event) {
                mapModel.updateTitle(n.id, event.text, event.isNew);
                mapModel.setInputEnabled(true);
            });
        }

        node.on(':nodepositionChanged', function (event) {
            mapModel.updateNodePosition(n.id, event.nodeX, event.nodeY);

        });
        if (mapModel.editable) {
            node.on(':editing', function () {
                mapModel.setInputEnabled(false);
            });
        }
        node.on(':request', function (event) {
            mapModel[event.type](event.source, n.id);
        });
        if (n.level > 1) {
            node.on('dragstart', function (evt) {
                if (evt.evt.which == 3) {
                    //mv.LaunchNodeToolbar(this, evt.evt);
                    mapModel.clickNode(n.id, null);
                } else {
                    stage.setDraggable.bind(stage, false);
                }
                
                

            });
            node.on('dragend ',  function (evt) {
                if (evt.evt.which == 3) {
                    mv.LaunchNodeToolbar(this, evt.evt);
                } else {
                    stage.setDraggable.bind(stage, true);
                    
                }
                mapModel.clickNode(n.id, null);
            });
        } 
        layer.add(node);
        stage.on(':scaleChangeComplete', function () {
            //node.setupShadows();
        });


        nodeByIdeaId[n.id] = node;
        //Task 01: Fix zoom bug. NguyenVo
        mapModel.mapStage = stage;
        mapModel.computeLRTD(node);

        var indexedNode = new NodeIndexModel();
        indexedNode.RId = n.rid;
        indexedNode.Id = n.id;
        indexedNode.Title = n.title;
        indexedNode.ImageUrl = "";
        indexedNode.MapPositionX = node.getAbsolutePosition().x;
        indexedNode.MapPositionY = node.getAbsolutePosition().y;
        //console.log(n.id + " " + node.getAbsolutePosition().x + " " + node.getAbsolutePosition().y);
        mapModel.indexedNodes.push(indexedNode);
    }, 1);

    mapModel.addEventListener('nodeShapeChanged', function (n, newtype) {

        var oldnode = nodeByIdeaId[n.id];
        oldnode.ChangeShapeType(newtype);

        layer.draw();


    });

    mapModel.addEventListener('nodeImgChanged', function (n, imgsrc) {

        //var n = nodeByIdeaId[ideaId]
        var oldnode = nodeByIdeaId[n.id];
        oldnode.ChangeImgSrc(imgsrc);
        layer.draw();
    });
    mapModel.addEventListener('nodeBgColorChanged', function (n, colorcode) {

        //var n = nodeByIdeaId[ideaId]
        var oldnode = nodeByIdeaId[n.id];
        oldnode.ChangeBgColor(colorcode);
        layer.draw();
    });
    mapModel.addEventListener('nodeTextColorChanged', function (n, colorcode) {

        //var n = nodeByIdeaId[ideaId]
        var oldnode = nodeByIdeaId[n.id];
        oldnode.ChangeTextColor(colorcode);
        layer.draw();
    });

    mapModel.addEventListener('nodeSelectionChanged', function (ideaId, isSelected) {
        var node = nodeByIdeaId[ideaId];
        if (!node) {
            return;
        }
        node.setIsSelected(isSelected);
        if (!isSelected) {
            return;
        }
        ensureSelectedNodeVisible(node);

    });
    mapModel.addEventListener('nodeFocusRequested', function (ideaId) {
        var node = nodeByIdeaId[ideaId];
        stage.setScale({ x: 1, y: 1 });
        stage.setX((stage.getWidth() / 2) - (0.5 * node.getWidth()) - node.getX());
        stage.setY((stage.getHeight() / 2) - (0.5 * node.getHeight()) - node.getY());
        stage.draw();
        stage.fire(':scaleChangeComplete');
    });
    mapModel.addEventListener('nodeAttrChanged', function (n) {

        var node = nodeByIdeaId[n.id];
        node.setMMAttr(n.attr);
    });

    mapModel.addEventListener('nodeDroppableChanged', function (ideaId, isDroppable) {
        var node = nodeByIdeaId[ideaId];
        node.setIsDroppable(isDroppable);
    });
    var count = 0;
    mapModel.addEventListener('nodeRemoved', function (n) {
        /*BILLvar i = 0;
        bootbox.alert(mapModel.indexedNodes.length);
        for (i = 0; i <= mapModel.indexedNodes.length; i++) {
           
        }*/
        var p = "";
        for (p in mapModel.indexedNodes) {
            if (mapModel.indexedNodes[p] != null && mapModel.indexedNodes[p].Id == n.id) {
                break;
            }
        }

        if (p != "") {

            mapModel.indexedNodes.splice(p, 1);
        }
        var node = nodeByIdeaId[n.id];
        delete nodeByIdeaId[n.id];
        node.off('click dblclick tap dbltap dragstart dragmove dragend mouseover mouseout touchstart touchend :openAttachmentRequested :editing :textChanged ');
        //	node.destroy();
        new Kinetic.Tween({
            node: node,
            opacity: 0.25,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.2,
            onFinish: node.destroy.bind(node)
        }).play();
    });
    mapModel.addEventListener('nodeMoved', function (n, reason) {
        var node = nodeByIdeaId[n.id];

        var inode = mapModel.LookForNodeIndex(n.id);
        if (inode != null) {
            inode.MapPositionX = node.getAbsolutePosition().x;
            inode.MapPositionY = node.getAbsolutePosition().y;
        }

        //node.x = n.x;
        //node.y = n.y;
        //ensureSelectedNodeVisible.bind(undefined, node);
        
        //alert("Mediator Node Move");
        new Kinetic.Tween({
            node: node,
            x: n.x,
            y: n.y,
            easing:Kinetic.Easings.Linear,
            duration: 0.01,
            onFinish: ensureSelectedNodeVisible.bind(undefined, node)
        }).play();
    });
    mapModel.addEventListener('nodepositionChanged', function (n) {
        try {

            var node = nodeByIdeaId[n.id];

            var idea = mapModel.getIdea(),
            parentIdea = idea.findParent(n.id),
            parentNode = mapModel.getCurrentLayout().nodes[parentIdea.id];

            node.fire(':nodepositionChanged', {
                nodeX: n.x - parentNode.x,
                nodeY: n.y - parentNode.y
            });
        } catch (e) {
            console.log('nodepositionChanged failed', e);
        }
    });

    mapModel.addEventListener('nodepositionSynced', function (nid, xpos, ypos) {
        try {

            var node = nodeByIdeaId[nid];
            if (node == null) return;
            var idea = mapModel.getIdea(),
            parentIdea = idea.findParent(nid),
            parentNode = mapModel.getCurrentLayout().nodes[parentIdea.id];

            /*node.fire(':nodepositionChanged', {
                nodeX: xpos - parentNode.x,
                nodeY: ypos - parentNode.y
            });*/
            node.setX(xpos + parentNode.x);
            node.setY(ypos + parentNode.y);
        } catch (e) {
            console.log('nodepositionChanged failed', e);
        }
    });

    mapModel.addEventListener('nodedataSynced', function (nid, data) {
        try {

            var node = nodeByIdeaId[nid];
            if (node == null) return;
            node.SetData(data);


        } catch (e) {
            console.log('nodepositionChanged failed', e);
        }
    });

    mapModel.addEventListener('Subnodeadded', function () {
        var ideaid = mapModel.getCurrentlySelectedIdeaId();

        var parentNode = mapModel.getCurrentLayout().nodes[ideaid];

        var newId = mapModel.addSubIdea('toolbar', ideaid);


        var newnode = mapModel.getCurrentLayout().nodes[newId];
        var evt = {
            shiftKey: false,
            evt: {
                layerX: parentNode.x + parentNode .width +30 + stage.getX(),
                layerY: parentNode.y  + stage.getY()
            }
        }
        mapModel.dispatchEvent('follownewpin', newnode, evt);
    });

    mapModel.addEventListener('Subnodeaddsync', function (newid, title, xpos, ypos, rid, parentid) {

        if (mapModel.getCurrentLayout().nodes[newid] == null) {
            //var parentNode = mapModel.getCurrentLayout().nodes[parentid];

            var newId = mapModel.syncaddSubIdea('sync', newid, title, xpos, ypos, rid, parentid);

        }
    });

    mapModel.addEventListener('Subnoderemovesync', function (nodeid) {

        if (mapModel.getCurrentLayout().nodes[nodeid] != null) {
            //var parentNode = mapModel.getCurrentLayout().nodes[parentid];

            var newId = mapModel.syncremoveSubIdea('sync', nodeid);

        }
    });


    mapModel.addEventListener('nodeTitleChanged', function (n) {
        var node = nodeByIdeaId[n.id];
        node.SetShapeText(n.title);
    });
    mapModel.addEventListener('connectorCreated', function (n) {
        var currConnector = connectorByFromIdeaIdToIdeaId[connectorKey(n.from, n.to)];
        if (currConnector != undefined) {
            //console.log("connector already exisit");
            return;
        }
        var connector = new Kinetic.Connector({
            id: 'connector_' + n.to,
            shapeFrom: nodeByIdeaId[n.from],
            shapeTo: nodeByIdeaId[n.to]
        });
        //console.log("connectorCreated");
        connectorByFromIdeaIdToIdeaId[connectorKey(n.from, n.to)] = connector;
        layer.add(connector);
        connector.moveToBottom();
        new Kinetic.Tween({
            node: connector,
            opacity: 1,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.1
        }).play();
    });
    mapModel.addEventListener('layoutChangeComplete', function () {
        stage.draw();
    });
    mapModel.addEventListener('connectorRemoved', function (n) {
        var key = connectorKey(n.from, n.to),
			connector = connectorByFromIdeaIdToIdeaId[key];
        if (connector == undefined) {
            //console.log(" connectorRemoved -- undefinded connector");
            return;
        }
        delete connectorByFromIdeaIdToIdeaId[key];
        new Kinetic.Tween({
            node: connector,
            opacity: 0,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.1,
            onFinish: connector.destroy.bind(connector)
        }).play();
    });
    mapModel.addEventListener('linkCreated', function (l) {
        var link = new Kinetic.Link({
            id: 'link_' + l.ideaIdFrom + '_' + l.ideaIdTo,
            shapeFrom: nodeByIdeaId[l.ideaIdFrom],
            shapeTo: nodeByIdeaId[l.ideaIdTo]
        });
        //console.log("linkCreated");
        /*link.on('click tap', function (event) {
            mapModel.selectLink('mouse', l, { x: event.layerX, y: event.layerY });
        });*/
        layer.add(link);
        link.moveToBottom();
        link.setMMAttr(l.attr);
    });
    mapModel.addEventListener('linkRemoved', function (l) {
        var link = layer.get('#link_' + l.ideaIdFrom + '_' + l.ideaIdTo)[0];
        link.destroy();
        //		layer.draw();
    });
    mapModel.addEventListener('linkAttrChanged', function (l) {
        var link = layer.get('#link_' + l.ideaIdFrom + '_' + l.ideaIdTo)[0];
        link.setMMAttr(l.attr);
    });
    mapModel.addEventListener('mapScaleChanged', function (scaleMultiplier, zoomPoint) {
        var currentScale = stage.getScale().x || 1,
			targetScale = Math.max(Math.min(currentScale * scaleMultiplier, 5), 0.2);
        mv.ZoomRatio(Math.round(targetScale * 100) - mv.minimumScaleDown);
        mv.AssignZoomSliderValue(Math.round(targetScale * 100));
        if (currentScale === targetScale) {
            return;
        }
        mv.HideNodeToolbar();
        var pointer = stage.getPointerPosition();
        if (pointer == undefined) return;
        zoomPoint = zoomPoint || { x: pointer.x, y: pointer.y };
        new Kinetic.Tween({
            node: stage,
            x: zoomPoint.x + (stage.getX() - zoomPoint.x) * targetScale / currentScale,
            y: zoomPoint.y + (stage.getY() - zoomPoint.y) * targetScale / currentScale,
            scaleX: targetScale,
            scaleY: targetScale,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.3,
            onFinish: function () {
                stage.fire(':scaleChangeComplete');
            }
        }).play();
    });
    mapModel.addEventListener('mapSpecificScaleChanged', function (scale, zoomPoint) {
        var currentScale = stage.getScale().x || 1,
			targetScale = scale;
        mv.ZoomRatio(Math.round(targetScale * 100) - mv.minimumScaleDown);
        mv.AssignZoomSliderValue(Math.round(targetScale * 100));
        if (currentScale === targetScale) {
            return;
        }
        mv.HideNodeToolbar();
        zoomPoint = zoomPoint || { x: 0.5 * stage.getWidth(), y: 0.5 * stage.getHeight() };
        new Kinetic.Tween({
            node: stage,
            x: zoomPoint.x + (stage.getX() - zoomPoint.x) * targetScale / currentScale,
            y: zoomPoint.y + (stage.getY() - zoomPoint.y) * targetScale / currentScale,
            scaleX: targetScale,
            scaleY: targetScale,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.01,
            onFinish: function () {
                stage.fire(':scaleChangeComplete');
            }
        }).play();
    });
    mapModel.addEventListener('mapViewResetRequested', function () {

        resetStage();
        mv.HideNodeToolbar();
    });
    mapModel.addEventListener('mapCenterRequested', function (cx, cy) {
        //TODO
        centerStage(cx, cy);
        mv.HideNodeToolbar();
    });
    mapModel.addEventListener('mapMoveRequested', function (deltaX, deltaY) {

        moveStage(deltaX, deltaY);
    });
    mapModel.addEventListener('mapNodeFocusRequested', function (id, x, y) {
        //bootbox.alert(id + " " + (x + " " + stage.getAbsolutePosition().x) + " " + (y + " " + stage.getAbsolutePosition().y));
        mapModel.clickNode(id, null);
        //moveStage(x - stage.getAbsolutePosition().x, y - stage.getAbsolutePosition().y);

        new Kinetic.Tween({
            node: stage,
            x: -x + (stage.getWidth()),
            y: -y + (stage.getHeight()),
            scaleX: 1,
            scaleY: 1,
            easing: Kinetic.Easings.EaseInOut,
            duration: 0.05,
            onFinish: function () {
                stage.fire(':scaleChangeComplete');
            }
        }).play();
    });



    mapModel.addEventListener('deselectedNodes', function () {
        /*var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        if (!node) {
            return;
        }*/
        mapModel.deselectedNodes();
        /*if (mapModel.isActivated(mapModel.getCurrentlySelectedIdeaId())) {
            
            mapModel.toggleActivationOnNode("mouse", mapModel.getCurrentlySelectedIdeaId());
        }*/
        //mapModel.resetNodeSelection();
        /*node.removeAllPins();
        node.setIsActivated(false);
        
        
        stage.draw();*/
    });

    mapModel.addEventListener('ToogleCollapseNode', function () {
        //var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        mapModel.toggleCollapse("menu");
        //ToggleCollapse 
    });

    mapModel.addEventListener('LaunchEditNode', function () {
        var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        mv.NodeEditDialog.LoadNode(node);
    });
    mapModel.addEventListener('LaunchPictureEditNode', function () {
        var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        mv.NodeEditDialog.LoadPictureNode(node);
    });
    mapModel.addEventListener('LaunchColorEditNode', function () {
        var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        mv.NodeEditDialog.LoadColorNode(node);
    });
    mapModel.addEventListener('LaunchEditPopupMenu', function (actionType) {
        var node = nodeByIdeaId[mapModel.getCurrentlySelectedIdeaId()];
        mv.NodeEditDialog.LoadEditMenuForNode(node, actionType, mapModel);
    });
    mapModel.addEventListener('CenterToSpecificNode', function (nodeId) {
        //var id = "node_" + nodeId;
        //alert(id);
        var node = nodeByIdeaId[nodeId];
        //alert(node);
        var top = node.getY(),
            left = node.getX(),
            //Day la toa do trong he toa do stage.getX() stage.getY().
            right = left + 0.5*node.getWidth() + node.getWidth(),
            bottom = top + 0.5*node.getHeight() + node.getHeight();
        
        var trans = { x: stage.getX() - 0.5 * stage.getWidth(), y: stage.getY() - 0.5 * stage.getHeight() };
        var cx = (left + trans.x) +  0.5 * stage.getWidth(),
            cy = (top + trans.y) + 0.5 * stage.getHeight();

        var tx = 0.5 * stage.getWidth() - cx + stage.getX(),
            ty = 0.5 * stage.getHeight() - cy + stage.getY();
        stage.setX(tx);
        stage.setY(ty);
        stage.draw();
       
    });
    mapModel.addEventListener('activatedNodesChanged', function (activatedNodes, deactivatedNodes) {
        if (mapModel.editable) {

            var setActivated = function (active, id) {
                var node = nodeByIdeaId[id];
                if (!node) {
                    return;
                }
                node.setIsActivated(active);

            };
            _.each(activatedNodes, setActivated.bind(undefined, true));
            _.each(deactivatedNodes, setActivated.bind(undefined, false));
            stage.draw();
        }
    });
    (function () {
        var x, y;
        stage.on('dragmove', function () {
            //alert(stage.getX() + " " + stage.getY())
            var deltaX = x - stage.getX(),
				deltaY = y - stage.getY(),
				visibleAfterMove = atLeastOneVisible(nodeByIdeaId, 0, 0) || atLeastOneVisible(connectorByFromIdeaIdToIdeaId, 0, 0),
				shouldMoveBack = !visibleAfterMove && !(atLeastOneVisible(nodeByIdeaId, deltaX, deltaY) || atLeastOneVisible(connectorByFromIdeaIdToIdeaId, deltaX, deltaY));
            shouldMoveBack = false; //temp disable auto adjustment
            if (shouldMoveBack) {
                //alert("fap");
                moveStage(deltaX, deltaY);
            } else {
                //alert("second");
                x = stage.getX();
                y = stage.getY();
                $('div.stage_x').text(stage.getX());
                $('div.stage_y').text(stage.getY());
            }
            mv.HideNodeToolbar();
        });
        //STAGECLICK
        stage.on('contentClick', function (evt) {
            if (!evt.evt.cancelBubble) {
                mv.HideNodeToolbar();
                mapModel.dispatchEvent('deselectedNodes');
            }

        }

        );
        /*stage.on('contentClick', function () {
            var pos = stage.getPointerPosition();
            var mouseX = parseInt(pos.x);
            var mouseY = parseInt(pos.y);
         
            
        });*/
        /*stage.getContent().addEventListener('click', function (e) {
            e.cancelBubble = true;
            alert("click");
            mapModel.dispatchEvent('deselectedNodes');
            mv.HideNodeToolbar();
        });*/

    }());
};
MAPJS.calculateMergedBoxSize = function (box1, box2) {
    'use strict';
    if (box2.position === 'bottom' || box2.position === 'top') {
        return {
            width: Math.max(box1.width, box2.width),
            height: box1.height + box2.height
        };
    }
    if (box2.position === 'left' || box2.position === 'right') {
        return {
            width: box1.width + box2.width,
            height: Math.max(box1.height, box2.height)
        };
    }
    return {
        width: Math.max(box1.width, box2.width),
        height: Math.max(box1.height, box2.height)
    };
};
MAPJS.KineticMediator.dimensionProvider = _.memoize(
	function (content) {
	    'use strict';


	    var type = "rec";
	    var shape = undefined;
	    if (content.data != undefined && content.data != "") {

	        try {
	            obj = JSON.parse(n.data);

	            type = obj.type;
	        } catch (err) {
	        }
	    }

	    shape = new Kinetic.IdeaDimension({
	        text: content.title,
	        mmAttr: content.attr,
	        NodeType: type
	    });
	    shape.SetupIdea();


	    return {
	        width: shape.getWidth(),
	        height: shape.getHeight()
	    };
	},
	function (content) {
	    'use strict';
	    var iconSize = (content.attr && content.attr.icon && (':' + content.attr.icon.width + 'x' + content.attr.icon.height + 'x' + content.attr.icon.position)) || ':0x0x0';
	    return content.title + iconSize;
	}
);

MAPJS.KineticMediator.layoutCalculator = function (idea) {
    'use strict';
    return MAPJS.calculateLayout(idea, MAPJS.KineticMediator.dimensionProvider);
};

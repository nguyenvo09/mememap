

var MapEditDialog = function () {
    var self = this;
    self.onDetailMode = false;
    self.NodeDetail = null;
    self.EDITTITLE = 2;
    self.EDITPICTURE = 1;
    self.EDITCOLOR = 3;
    
    
    self.RegisterDialog = function (nodedetail) {
        self.NodeDetail = nodedetail;
    }

    self.LoadNodeDetail = function (node) {
        mv.NodeDetail().Title(node.attrs.text);
        mv.NodeDetail().NoteType(node.attrs.NodeType);
        mv.NodeDetail().Id(node.attrs.id);
        mv.NodeDetail().Node = node;
        mv.NodeDetail().ImgSrc(node.ImgSrc);
        mv.NodeDetail().ShownImgSrc(node.ImgSrc);
        mv.NodeDetail().BgColor(node.attrs.data.bgcolor);
        mv.NodeDetail().TextColor(node.attrs.data.textcolor);

        mv.NodeDetail().EditTask(false);
        mv.NodeDetail().EditColor(false);
        mv.NodeDetail().EditTitle(false);
        mv.NodeDetail().EditImg(false);

        
        


        if (IsYoutubeVideo(mv.NodeDetail().ImgSrc())) {
            mv.NodeDetail().ImgSrc("http://img.youtube.com/vi/" + ExtractYoutubeVideoId(mv.NodeDetail().ImgSrc()) + "/sddefault.jpg");
        }

        var idea = mapModel.findIdeaById(node.attrs.id.replace("node_", ""));
        mv.NodeDetail().RID(idea.rid);

        if (mv.NodeDetail().BgColor() == undefined || mv.NodeDetail().BgColor() == "") mv.NodeDetail().BgColor("ffffff");
        if (mv.NodeDetail().TextColor() == undefined || mv.NodeDetail().TextColor() == "") mv.NodeDetail().TextColor("000000");
    }
    self.LoadNode = function (node) {
        self.LoadNodeDetail(node);
        self.SwitchTab(self.EDITTITLE);
        self.AppearDialog(node);

    }
    self.LoadPictureNode = function (node) {
        self.LoadNodeDetail(node);
        self.SwitchTab(self.EDITPICTURE);
        self.AppearDialog(node);
    }
    self.LoadColorNode = function (node) {
        self.LoadNodeDetail(node);
        self.SwitchTab(self.EDITCOLOR);
        self.AppearDialog(node);
    }
    self.LoadEditMenuForNode = function (node, actionType, mapModel) {
        //TODO
        mv.Title("Edit Node");
        mv.PopUpModelVM(new EditDataModel("Edit Node"));
        mv.PopUpModelVM().LoadNodeTypeSelection();
        self.LoadNodeDetail(node);
        //mv.PopUpModelVM().NodeDetail(mv.NodeDetail());
        mv.PopUpModelVM().setNodeDetail(mv.NodeDetail());
        mv.PopUpModelVM().NodeDetail().SetBodyDetail();
        
        //Get Task Detail.
        TaskMaster.getTaskDetail(node, mv.PopUpModelVM().NodeDetail().Task());
        

        //setNodeDetail
        mv.PopUpModelVM().addEventListener("ChangeShapeType", function (shape) {
            var datastr = JSON.stringify({ 'type': shape });
            mapModel.updateNodeShapeData(shape);
            mapModel.updateNodeShape(shape);
            //$("div.modal#popupEditNodeSelection").modal("hide");
        });
        mv.PopUpModelVM().SwitchTab(undefined, actionType);

        $("div.modal#popupEditNodeSelection").modal({ keyboard: true });


    }

    self.SwitchTab = function (mode) {
        bootbox.alert(mode.toString());
        //alert(mode);
        //var curr = $("div.modal#popupEditNodeSelection").find
        var currentPanel, currentTrigger;
        if (mode == self.EDITTITLE) {
            currentPanel = "#tab1";
            currentTrigger = "#tabtrigger1";
        } else if (mode == self.EDITPICTURE) {
            currentPanel = "#tab2";
            currentTrigger = "#tabtrigger2";
        }
        else if (mode == self.EDITCOLOR) {
            currentPanel = "#tab3";
            currentTrigger = "#tabtrigger3";
        }
        
        if (!$(currentPanel).hasClass("active")) {
            $(">.tab-pane", $(currentPanel).closest(".tab-content")).each(function () {
                $(this).removeClass("active");
            });
            $(currentPanel).addClass("active");
        }
        $(">li", $(currentTrigger).closest(".nav-tabs")).removeClass("active");
        $(currentTrigger).addClass("active");

        var masterContainer = $(currentPanel).closest(".custom-tabs.track-url");
        if (masterContainer.hasClass("auto-scroll")) {
            $("body,html").animate({ scrollTop: masterContainer.offset().top - 80 }, 500);
        }
        /*
        if (mode == self.EDITTITLE) {
            if ($("#tab1").hasClass("active")) {

            } else {
                $("#tab2").removeClass("active");
                $("#tab3").removeClass("active");
                $("#tab1").addClass("active");
            }
            
        } else if (mode == self.EDITPICTURE) {
            if ($("#tab2").hasClass("active")) {

            } else {
                $("#tab1").removeClass("active");
                $("#tab3").removeClass("active");
                $("#tab2").addClass("active");
            }
        }
        */
    }

    self.AppearDialog = function (node) {
        mv.HideNodeToolbar();
        var canvasPosition = jQuery("#" + node.getStage().getContainer().id).offset();

        var x = canvasPosition.left + $('#mynodeeditor').width() + node.getAbsolutePosition().x;

        if (x > $(window).width()) {
            x = $(window).width() - 10;
        }
        $('#mynodeeditor').css("left", (x - $('#mynodeeditor').width()) + "px");


        $('#mynodeeditor').fadeIn(150);
        $('#mynodeeditor').addClass('beactive');

    }

    self.CloseDialog = function () {
        $('#mynodeeditor').fadeOut(150);
        $('#mynodeeditor').removeClass('beactive');
    }


};

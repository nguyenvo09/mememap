/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

   
    var defaultvideoimage= new Image();
    defaultvideoimage.src = "/Images/video.png";

    function createVideoImage(xpos, ypos) {
        
        var icon = new Kinetic.Image({
            x: xpos,
            y: ypos,
            width: 0,
            height: 0,
            strokeWidth: 0,
            image: defaultvideoimage

        });
        
       

        return icon;
    }

    Kinetic.IdeaVideo = function (config) {
        var self = this,
            isActivated = false;
        
        
        this.shapeType = 'IdeaVideo';
        this.rid = config.rid;
        this.videourl = config.videourl;
        Kinetic.Group.call(this, config);
        
        try {
            this.image1 = createVideoImage(3, 3);
            this.bgShape = new Kinetic.Circle({
                strokeWidth: 0.1,
                radius: 20,
                x: 20,
                y: 20,
                fill: "#FFFFFF",
                stroke: "#333333"
            });

            this.on('mouseover', function () {
                self.bgShape.setFill("#E8E35A");
                self.draw();
            });

            this.on('mouseout', function () {
                self.bgShape.setFill("#ffffff");
                self.draw();
            });
            this.on('click tap touchend mouseup', function () {
                LoadVideoPlayer(this.videourl);
            });

            //this.image1.updateMapjsAttribs({ });
            this.add(this.bgShape);
            this.add(this.image1); 
        } catch (err) {

        }
	};
    
	
	
    Kinetic.IdeaVideo.prototype.setStyle = function () {
	    'use strict';
	    /*jslint newcap: true*/
	    var self = this;
	};

    Kinetic.IdeaVideo.prototype.setMMAttr = function (newMMAttr) {
	    'use strict';
	    this.mmAttr = newMMAttr;
	    this.setStyle();
	    this.getLayer().draw();
	};
	Kinetic.Util.extend(Kinetic.IdeaVideo, Kinetic.Group);
}());

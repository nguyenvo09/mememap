/*global _, Kinetic, MAPJS*/
/*jslint nomen: true*/
(function () {
    'use strict';

   
    var defaultnoteimage= new Image();
    defaultnoteimage.src = "/Images/comment.png";

    function createNoteImage(xpos, ypos) {
        
        var icon = new Kinetic.Image({
            x: xpos,
            y: ypos,
            width: 0,
            height: 0,
            strokeWidth: 0,
            image: defaultnoteimage

        });
        
       
        return icon;
    }

    Kinetic.IdeaNote = function (config) {
        var self = this;
        
        
        this.shapeType = 'IdeaNote';
        this.rid = config.rid;
        this.nodeid = config.nid;
        Kinetic.Group.call(this, config);
        
        try {
            this.image1 = createNoteImage(10, 10);
            this.bgShape = new Kinetic.Circle({
                strokeWidth: 0.1,
                radius: 20,
                x: 20,
                y: 20,
                fill: "#FFFFFF",
                stroke: "#333333"
            });

            this.on('mouseover', function () {
                self.bgShape.setFill("#E8E35A");
                self.draw();
            });

            this.on('mouseout', function () {
                self.bgShape.setFill("#ffffff");
                self.draw();
                //console.log("hehe");
            });
            //console.log("abcxyz");
            this.on('click', function () {
                //alert("open comment");
                //var EDIT_COMMENTS = 5;
                //mv.HideNodeToolbar();
                var nodeid = this.nodeid;
                nodeid = nodeid.replace("node_", "");
                //mapModel.selectNode(id);
                //mapModel.dispatchEvent("LaunchEditPopupMenu", EDIT_COMMENTS);
                //LoadBodyDetailFromValues(this.rid, this.nodeid)
                //console.log("What the fucking is going on");
                
                PouchDbNotification.getCmtOnNode(undefined, nodeid, 10);
                
            });

            //this.image1.updateMapjsAttribs({ });
            this.add(this.bgShape);
            this.add(this.image1); 
        } catch (err) {

        }
	};
    
	
	
    Kinetic.IdeaNote.prototype.setStyle = function () {
	    'use strict';
	    /*jslint newcap: true*/
	    var self = this;
	};

    Kinetic.IdeaNote.prototype.setMMAttr = function (newMMAttr) {
	    'use strict';
	    this.mmAttr = newMMAttr;
	    this.setStyle();
	    this.getLayer().draw();
	};
	Kinetic.Util.extend(Kinetic.IdeaNote, Kinetic.Group);
}());

﻿$(function () {
    if ($.popline.utils.browser.ie) {
        if ($.popline.utils.browser.ieVersion() < 9) {
            $.popline.utils.fixIE8();
        }
    } else {
        document.execCommand('defaultParagraphSeparator', false, 'p');
    }
    
    $(".popeditor").popline({ position: "fixed" });
    $(".popeditor").attr("contenteditable", true);
    $("input[name='position']").click(function () {
        $(".editor").popline("setPosition", this.id);
    });


    

    
});

function LoadBodyDetail(nodetail) {
    mv.BodyDetail().Title(nodetail.Title());
    mv.BodyDetail().RID(nodetail.RID());
    mv.BodyDetail().LoadBody();
    $("#mynoteeditor").css("display", "block");
}

function LoadBodyDetailFromValues(rid, id) {
    id=id.replace("node_", "");
    var title = mapModel.findIdeaById(id).title;
    mv.BodyDetail().Title(title);
    mv.BodyDetail().RID(rid);
    mv.BodyDetail().LoadBody();
    $("#mynoteeditor").css("display", "block");
}
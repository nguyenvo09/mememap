/*global MAPJS, Color, _, jQuery, Kinetic*/
/*jslint nomen: true, newcap: true, browser: true*/
(function () {
    'use strict';
    
    
    

    Kinetic.IdeaDimension = function (config) {
        var self = this;
        self.myconfig = config;
        this.mmAttr = config.mmAttr;
        
        config.name = 'IdeaDimension';
        self.text = new Kinetic.Text({
            fontSize: 14,
            fontFamily: 'Abel',
            lineHeight: 1.5,
            fontStyle: 'bold',
            align: 'center'
        });

        Kinetic.Group.call(this, config);



        self.SetupIdeaShape = function( type, offset) {

            self.rectAttrs = { stroke: '#bbbbbb', strokeWidth: 0.1 };
            switch (type) {
                case "cir":
                    self.bgShape = new Kinetic.Circle({
                        strokeWidth: 0.1,
                        radius: 150,
                        x: offset,
                        y: offset,
                        fill: "#FFFFFF",
                        stroke: "#333333",
                        visible: false
                    });
                    self.ideaShape = new Kinetic.Circle({
                        strokeWidth: 0, //0
                        radius: 150,
                        x: offset,
                        y: offset,
                        fill: "#FFFFFF"
                    });
                    //self.rectbg1 = bgRect(0);//8
                    //self.rectbg2 = bgRect(0);//4
                    //self.ShapeElements.push(self.rectbg1);
                    //self.ShapeElements.push(self.rectbg2);
                    break;
                case "dia":
                    self.bgShape =
                         new Kinetic.Rect({
                             strokeWidth: 0.1,//1
                             cornerRadius: 0,
                             x: offset,
                             y: offset,
                             rotationDeg: 45,
                             stroke: "#333333"
                         });
                    self.ideaShape = new Kinetic.Rect({
                        strokeWidth: 0, //0
                        cornerRadius: 10,
                        x: offset,
                        y: offset,
                        rotationDeg: 45,
                        fill: "#FFFFFF"
                    });
                    //self.cirbg1 = bgCir(0);
                    //self.ShapeElements.push(self.cirbg1);
                    break;
                case "rec":
                case "img":
                    self.bgShape =
                         new Kinetic.Rect({
                             strokeWidth: 0.1,//1
                             cornerRadius: 0,
                             x: offset,
                             y: offset,
                             stroke: "#333333",
                             visible: false
                         });
                    self.ideaShape = new Kinetic.Rect({
                        strokeWidth: 0, //0
                        cornerRadius: 10
                    });
                    //self.cirbg1 = bgCir(0);
                    //self.ShapeElements.push(self.cirbg1);
                    break;
            }
            //self.ShapeElements.push(self.bgShape);
            //self.ShapeElements.push(self.ideaShape);
        }

        self.SetShapeText = function (paramtext) {
            if (self.text == null) {
                self.text = new Kinetic.Text({
                    fontSize: 14,
                    fontFamily: 'Abel',
                    lineHeight: 1.5,
                    fontStyle: 'bold',
                    align: 'center'
                });
            }
            var replacement = breakWords(MAPJS.URLHelper.stripLink(paramtext)) ||
                    (paramtext.length < COLUMN_WORD_WRAP_LIMIT ? self.text : (paramtext.substring(0, COLUMN_WORD_WRAP_LIMIT) + '...'));
            self.unformattedText = paramtext;
            self.text.setText(replacement);
        };


		self.unformattedText = config.text;
		self.bgShape = null;
		self.ideaShape = null;
		self.ShapeElements = new Array();
		this.classType = 'IdeaDimension';

        //For Node type
        self.NodeType = config.NodeType;
        
        


        this.getNodeAttrs = function () {
            return self.attrs;
        };
        

        this.SetupIdea = function () {            
            this.SetupIdeaShape(this.myconfig.NodeType, 0);

            this.SetShapeText(this.myconfig.text);
            if (this.ideaShape == null) alert("ideaShape");
            if (this.bgShape == null) alert("bgShape");
            if (this.text == null) alert("text");

            this.add(this.bgShape);
            this.add(this.ideaShape);

            this.add(this.text);
            this.setStyle();
        }
        
    };
}());

Kinetic.IdeaDimension.prototype.getMMScale = function () {
    'use strict';
    var stage = this.getStage(),
		scale = (stage && stage.getScaleX()) || this.getScaleX() || 1;
    return { x: scale, y: scale };
};

Kinetic.IdeaDimension.prototype.setStyle = function () {
    'use strict';
    
    /*jslint newcap: true*/
    var self = this,
		rectOffset,
		rectIncrement = 4,
        padding = 2,		
		textSize = {
		    width: Math.max(this.text.getWidth(), 150),
		    height: this.text.getHeight() - 5
		},
		calculatedSize,
		pad = function (box) {
		    return {
		        width: box.width + 2 * padding,
		        height: box.height + 2 * padding
		    };
		},        
		positionTextAndIcon = function () {
		    self.text.setX((calculatedSize.width - self.text.getWidth()) / 2);
		    self.text.setY((calculatedSize.height - self.text.getHeight()) / 2  + 4);
		},
		calculateMergedBoxSize = function (box1) {
		  
		    return pad({
		        width: box1.width,
		        height: box1.height
		    });
		},
        PositionShapeBg = function (bgShape, ideaShape, calculatedSize, type) {
            if (type == "cir") {

                _.each([ideaShape, bgShape], function (r) {
                    r.setRadius(Math.sqrt((calculatedSize.width * calculatedSize.width / 4) + (calculatedSize.height * calculatedSize.height / 4)));
                    r.setX(calculatedSize.width / 2);
                    r.setY(calculatedSize.height / 2);
                });
            }
            if (type == "rec" || type == "img") {
                _.each([ideaShape, bgShape], function (r) {
                    r.setWidth(calculatedSize.width);
                    r.setHeight(calculatedSize.height);
                    r.setY(0);
                });
            }
            if (type == "dia") {
                _.each([ideaShape, bgShape], function (r) {
                    r.setWidth((calculatedSize.width + calculatedSize.height) / Math.sqrt(2))
                    r.setHeight((calculatedSize.width + calculatedSize.height) / Math.sqrt(2));
                    r.setY(-calculatedSize.width / 2);
                    r.setX(calculatedSize.width / 2);
                });
            }
        },
    
    calculatedSize = pad(textSize);
       
    this.setWidth(calculatedSize.width);
    this.setHeight(calculatedSize.height);
    positionTextAndIcon();
    
    PositionShapeBg(self.bgShape, self.ideaShape, calculatedSize, self.NodeType);
    
};

Kinetic.IdeaDimension.prototype.setMMAttr = function (newMMAttr) {
    'use strict';
    this.mmAttr = newMMAttr;
    this.setStyle();
};

Kinetic.Util.extend(Kinetic.IdeaDimension, Kinetic.Group);



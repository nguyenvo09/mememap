function parseJsonDateIgnoreZone(jsonDateString) {
    var d = new Date();    
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(0); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')));
    return date;
}
function parseJsonDateFIgnoreZone(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
   
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', ''))).toString() + ')/';

    return moment(jsonDateString).format("Do MMM YYYY");
}
function parseJsonDateMilestoneFIgnoreZone(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }    
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', ''))).toString() + ')/';
    return moment(jsonDateString).format("DD-MMM-YYYY");
}
function parseJsonDate(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(0); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', ''))-offset);
    return date;
}
function parseJsonDateNow(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')) - offset);
    return date;
}
function parseJsonDatewithNull(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return null; }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')) - offset);
    return date;
}
function parseJsonDateF(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', '')) - offset).toString()+')/';

    return moment(jsonDateString).format("Do MMM YYYY");
}
function parseJsonDateMilestoneF(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', '')) - offset).toString() + ')/';
    return moment(jsonDateString).format("DD-MMM-YYYY");
}
function parseJsonDateS(date) {
    return date.toDateString();
}

function fromDatetoString(date) {
    return moment(date).format("DD-MMM-YYYY");
}
function parseDoubleFormat(item) {
    if (item != null) {
        return parseFloat(item).toFixed(2).replace(/\.?0+$/, "");
    }
    else {
        return null;
    }
}


function parseDateToString(date, formatstring) {
    if (date == null) return "";
    return moment(date).format(formatstring);
}
function parseStringToDate(datestring, formatstring) {
    if (datestring==null||datestring == "")return null;
    return moment(datestring, formatstring).toDate();
}
function parseStringToDisplayDate(datestring, convertformat, displayformat) {
    if (datestring == null || datestring == "") return "";
    return moment(datestring, convertformat).format(displayformat);
}
function parseStringToDateWithNow(datestring, formatstring) {
    if (datestring == null || datestring == "") return new Date();
    return moment(datestring, formatstring).toDate();
}
function parseStringToDisplayDateWithNow(datestring, convertformat, displayformat) {
    if (datestring == null || datestring == "") return moment(new Date()).format(displayformat);
    return moment(datestring, convertformat).format(displayformat);
}
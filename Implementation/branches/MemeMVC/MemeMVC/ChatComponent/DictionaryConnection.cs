﻿using SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;



namespace MemeMVC.ChatComponent
{
    public class DictionaryConnection
    {
        // Singleton instance
        private readonly static Lazy<DictionaryConnection> _instance = new Lazy<DictionaryConnection>(() => new DictionaryConnection(GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients));

        public readonly ConcurrentDictionary<string, List<string>> _connectionIds = new ConcurrentDictionary<string, List<string>>();

        //private readonly object _updateStockPricesLock = new object();

        ////stock can go up or down by a percentage of this factor on each change
        //private readonly double _rangePercent = .002;

        //private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(250);
        //private readonly Random _updateOrNotRandom = new Random();

        //private readonly Timer _timer;
        //private volatile bool _updatingStockPrices = false;
        //private   dynamic dynamic;

        private DictionaryConnection(dynamic clients)
        {
            Clients = clients;

            //_stocks.Clear();
            

        }

        public static DictionaryConnection Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private dynamic Clients
        {
            get;
            set;
        }


        //private void BroadcastStockPrice(Stock stock)
        //{
        //    Clients.All.updateStockPrice(stock);
        //}

    }
}
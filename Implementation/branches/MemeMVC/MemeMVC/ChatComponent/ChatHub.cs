﻿
using MemeMVC.Model.DTO.Mongo;
using MemeMVC.Repository;
using Newtonsoft.Json;
using Ninject;
using SignalR.Hubs;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
namespace MemeMVC.ChatComponent
{
    [HubName("chat")]
    public class ChatHub : Hub
    {
        ICommentRepository _commentRepository;
        //key: mapId, value: list of connectionId associated with that mapId
        private readonly DictionaryConnection _dictOfConns;

        
        public ChatHub(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
            _dictOfConns = DictionaryConnection.Instance;
        }
        public void SendMessage(dynamic message)
        {
            //Clients.receiveMessage(message);
            //var message = { username: username, mapId: mapId, userId: userId, nodeId: nodeId, comment: text };
            string username = message.username;
            string mapId = message.mapId;
            string userId = message.userId;
            string nodeId = message.nodeId;
            string text = message.comment;
            string profileImage = message.profileImage;
            DateTime currDate = DateTime.Now;
            string createdDate = currDate.ToString();
            string rid = message.rid;
            message.createdDate = createdDate;
            _commentRepository.SaveOrUpdate(new Model.DTO.Mongo.UserComment(Int32.Parse(mapId), Int32.Parse(nodeId), Int32.Parse(userId), username, text, currDate, profileImage, rid));
            String connectionId = Context.ConnectionId;
            
            //------------------------------------------
            broadCastMsgToSpecificClient(mapId, message);
            //----------------------------------

        }
        /// <summary>
        /// Broadcast msg to all connections associated with mapId
        /// </summary>
        /// <param name="mapId">Key of dictionary</param>
        /// <param name="currConnectionId"></param>
        /// <param name="message">message to broadcast</param>
        private void broadCastMsgToSpecificClient(string mapId, dynamic message)
        {
            //get list of connectionId
            try
            {
                List<string> listOfConnections = _dictOfConns._connectionIds[mapId];

                //broadcast msg to all connection associated with the mapId
                foreach (string connId in listOfConnections)
                {
                    Clients[connId].receiveMessage(message);
                }
            }
            catch (Exception e)
            {

            }
        }
        /// <summary>
        /// This function will be called from javascript client. Do not change it. 
        /// Read _DetailJScript.csthtml file for more detail.
        /// </summary>
        /// <param name="mapId"></param>
        public void addToConnectionToDict(dynamic message)
        {
            try
            {
                string mapId = message.mapId;
                String connectionId = Context.ConnectionId;
                //get list of connectionId
                List<string> connIds = null;
                if (_dictOfConns._connectionIds.ContainsKey(mapId))
                {
                    connIds = _dictOfConns._connectionIds[mapId];
                }
                else
                {
                    connIds = new List<string>();
                    _dictOfConns._connectionIds.TryAdd(mapId, connIds);

                }
                //If current connectionId was not contained in the list then add it.
                if (connIds.Contains(connectionId) == false){
                    connIds.Add(connectionId);
                }
                GetAllCommentOfMap(mapId, connectionId);
            }
            catch (Exception e)
            {

            }
        }
        /// <summary>
        /// This function will be called at client side to load all comments of node to browswer.
        /// </summary>
        /// <param name="msg"></param>
        private void GetAllCommentOfMap(string mapId, string connectionId)
        {
            try
            {
                
                int mId = Int32.Parse(mapId);
                List<UserComment> result = _commentRepository.GetMany(c => c.mapId == mId).ToList();
                string ret = JsonConvert.SerializeObject(result);
                Clients[connectionId].receiveOldMsg(ret);
            }
            catch (Exception e)
            {

            }
        }
        public void getAllMessages(dynamic filter)
        {
            try
            {
                string mapId = filter.mapId;
                string nodeId = filter.nodeId;
                int mId = Int32.Parse(mapId);
                int nId = Int32.Parse(nodeId);
                //_booktranRepository.GetMany(u => listids.Contains(u.Id.ToString())).ToList();
                DateTime temp = DateTime.Now;
                List<UserComment> messages = _commentRepository.GetMany(c => c.mapId == mId && c.nodeId == nId).ToList();
                string result = JsonConvert.SerializeObject(messages);
                String connectionId = Context.ConnectionId;
                Clients[connectionId].receivePastMessages(result);
            }
            catch (Exception e)
            {

            }
        }
        
    }
}

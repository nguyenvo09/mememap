[assembly: WebActivator.PreApplicationStartMethod(typeof(MemeMVC.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(MemeMVC.App_Start.NinjectWebCommon), "Stop")]

namespace MemeMVC.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using MemeMVC.Service;
    using System.Configuration;
    using Base.Service;
    using MemeMVC.Repository;
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            #region Connection
            kernel.Bind<IUserService>().To<UserService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IUserInstanceService>().To<UserInstanceService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IProjectService>().To<ProjectService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IMapService>().To<MapService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IZoneService>().To<ZoneService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<INoteService>().To<NoteService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IZoneServiceOrient>().To<ZoneServiceOrient>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IMediaService>().To<MediaService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IEntityService>().To<EntityService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<IEmailTemplateService>().To<EmailTemplateService>().WithConstructorArgument("baseURI", ConfigurationManager.AppSettings["baseURI"]);
            kernel.Bind<ICommentRepository>().To<CommentRepository>();
            kernel.Bind<ITaskEntityRepository>().To<TaskEntityRepository>();
            kernel.Bind<ITaskPriorityRepository>().To<TaskPriorityRepository>();
            kernel.Bind<ITaskStatusRepository>().To<TaskStatusRepository>();
            kernel.Bind<ITaskTypeRepository>().To<TaskTypeRepository>();

            kernel.Bind<IUserMapSharedRepository>().To<UserMapSharedRepository>();
            #endregion

            #region MemeMVC
            #endregion

        }
    }
}

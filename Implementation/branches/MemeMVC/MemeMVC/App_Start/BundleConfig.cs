﻿using System.Web;
using System.Web.Optimization;

namespace MemeMVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include("~/Content/addon/kendo/js/kendo.web.min.js", "~/Content/addon/kendo/js/kendo.aspnetmvc.min.js"));

            ScriptBundle jquerybundle = new ScriptBundle("~/bundles/jquery");
            jquerybundle.Include(
                "~/Content/smartadmin/jquery-ui.min.js",
                "~/Content/addon/jquery/jquery-migrate-1.2.1.min.js"
                );
            jquerybundle.Transforms.Clear();
            bundles.Add(jquerybundle);


            bundles.Add(new ScriptBundle("~/bundles/Knockout").Include(
                        "~/Content/meme/knockout/knockout-3.1.0.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js",
                        "~/Content/addon/jquery/jquery.unobtrusive-ajax.min.js",
                        "~/Content/addon/jquery/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/smartadmin/js/bootstrap/bootstrap.min.js",
                "~/Content/addon/bootstrap/js/bootbox.min.js"
                ));


            StyleBundle kendostylebundle = new StyleBundle("~/bundles/kendocss");
            kendostylebundle.Include("~/Content/addon/kendo/css/kendo.common.css", "~/Content/addon/kendo/css/kendo.metro.css");
            kendostylebundle.Transforms.Clear();
            bundles.Add(kendostylebundle);

            StyleBundle bootstrapbundle = new StyleBundle("~/bundles/bootstrapcss");
            bootstrapbundle.Include("~/Content/smartadmin/css/bootstrap.min.css",
                "~/Content/smartadmin/css/font-awesome.min.css",
                "~/Content/smartadmin/css/smartadmin-production.css",
                "~/Content/smartadmin/css/smartadmin-skins.css",
                "~/Content/addon/validation/validation.css",
                "~/Content/addon/bootsnipp/bootsnipp.css");
            bootstrapbundle.Transforms.Clear();
            bundles.Add(bootstrapbundle);

            ScriptBundle jqueryaddonbundle = new ScriptBundle("~/bundles/jqueryaddon");
            jqueryaddonbundle.Include(           
                "~/Content/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js",
                "~/Content/smartadmin/js/plugin/select2/select2.min.js",
                "~/Content/smartadmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"
                );
            jqueryaddonbundle.Transforms.Clear();
            bundles.Add(jqueryaddonbundle);

            //ScriptBundle jkendoaddonbundle = new ScriptBundle("~/bundles/kendocjs");
            //jkendoaddonbundle.Include("~/Content/addon/kendo/js/knockout-kendo.min.js",
            //    "~/Content/addon/kendo/js/kendo.web.min.js");
            //jkendoaddonbundle.Transforms.Clear();
            //bundles.Add(jkendoaddonbundle);
        }
    }
}
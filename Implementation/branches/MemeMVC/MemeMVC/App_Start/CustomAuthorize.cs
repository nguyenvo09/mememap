﻿using System.Web;
using System.Web.Optimization;
using System.Web.Mvc;
using MemeMVC.Service;
using System.Configuration;
using System.Net;
using Base.Service;
using Base;
using Base.ViewModel;

namespace MemeMVC
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        private bool isAuthenticated;
        private bool hasPermission;
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            CommonService.HasError = false;
            isAuthenticated = true;
            if (!httpContext.Request.IsAuthenticated || SessionUtils.CurrentUser == null)
            {
                isAuthenticated = false;
                return false;
            }
            string email = httpContext.User.Identity.Name;
            string controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            string action = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            //check permission for user
            //hasPermission = IsSystemAdmin(SessionUtils.CurrentUser.UserID);           
            hasPermission = hasPermission || HasPermission(email, controller, action);

            //Temporarily remove Permission checking
            hasPermission = true;
            return hasPermission;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!isAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new System.Web.Routing.RouteValueDictionary
                        {
                                { "controller", "User" },
                                { "action", "Login" },
                                { "ReturnUrl", filterContext.HttpContext.Request.RawUrl }
                        });
            }
            else if (!hasPermission)
            {
                if (!CommonService.HasError)
                {
                    CommonService.HasError = true;
                    CommonService.SystemAnnouces = new Base.ViewModel.SystemAnnouces();
                    CommonService.AddSystemAnnouce("no permission", string.Format("{0}! You don't have permission to access this page {1}", filterContext.RequestContext.HttpContext.User.Identity.Name, filterContext.RequestContext.HttpContext.Request.Url), string.Empty, Base.ViewModel.AnnouceType.Error);
                }
                string lang = filterContext.RouteData.Values["lang"].ToString();
                if (string.IsNullOrEmpty(lang)) lang = "en-us";
                filterContext.Result = new RedirectResult("/" + lang + "/Home/Permission");
            }
            else base.HandleUnauthorizedRequest(filterContext);
        }
        //private bool IsSystemAdmin(int userID)
        //{
        //    HttpStatusCode hCode;
        //    UserInRoleService userInRoleService = new UserInRoleService(ConfigurationManager.AppSettings["baseURI"]);
        //    bool r = userInRoleService.GetIsUserInRole(userID, SystemRole.SystemAdmin, out hCode);
        //    if (hCode == HttpStatusCode.RequestTimeout)
        //    {
        //        CommonService.HasError = true;
        //        CommonService.SystemAnnouces = new Base.ViewModel.SystemAnnouces();
        //        CommonService.AddSystemAnnouce("requesttimeout", "Request time out", string.Empty, Base.ViewModel.AnnouceType.Error);
        //    }
        //    return r;
        //}
        private bool HasPermission(string email, string controller, string action)
        {
            return false;
        }
    }
}
var map;
var geocoder;
var centerChangedLast;
var reverseGeocodedLast;
var currentReverseGeocodeResponse;
var arrayTaskAddress = new Array();
var currAddress;
var countArray = 0;
var cm_openInfowindow;

function Location() {
    this.Lng = 0;
    this.Lat = 0;
    this.Address = '';
    this.LatLng;
    this.image = '';
}

function LoadMap() {    
    var latlng = new google.maps.LatLng(10.792353984612665, 106.69938504208073);
    var myOptions = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    InitTaskAddress();
    for (var i = 0; i < arrayTaskAddress.length; i++) {
        var html = "<table><tr><td><img src='" + arrayTaskAddress[i].image + "'height='100px' width='100px'/></td>" + '<td><b>Address: </b>' + arrayTaskAddress[i].Address + "</td></tr></table>";
        var marker = createMarker(html, arrayTaskAddress[i].LatLng);
    }
    geocoder = new google.maps.Geocoder();
    setupEvents();
    centerChanged();
}

function createMarker(name, latlng) {
    var marker = new google.maps.Marker({ position: latlng, map: map });
    google.maps.event.addListener(marker, "click", function () {
        var infowindow = new google.maps.InfoWindow({ content: name });
        cm_setInfowindow(infowindow);
        infowindow.open(map, marker);
    });
    return marker;
}

function cm_setInfowindow(newInfowindow) {
    if (cm_openInfowindow != undefined) {
        cm_openInfowindow.close();
    }
    cm_openInfowindow = newInfowindow;
}

function geocodeResult(results, status) {
    if (status == 'OK' && results.length > 0) {
        map.fitBounds(results[0].geometry.viewport);
    } else {
        alert('Place address can not empty or invalid');
    }
}

function geocode() {
    var address = document.getElementById('address').value;
    geocoder.geocode({
        'address': address,
        'partialmatch': true
    }, geocodeResult);

}

function initialize() {
    var latlng = new google.maps.LatLng(32.5468, -23.2031);
    var myOptions = {
        zoom: 2,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
    geocoder = new google.maps.Geocoder();
    setupEvents();
    centerChanged();
}

function setupEvents() {
    reverseGeocodedLast = new Date();
    centerChangedLast = new Date();

    setInterval(function () {
        if ((new Date()).getSeconds() - centerChangedLast.getSeconds() > 1) {
            if (reverseGeocodedLast.getTime() < centerChangedLast.getTime())
                reverseGeocode();
        }
    }, 1000);
    google.maps.event.addListener(map, 'zoom_changed', function () {        
    });
    google.maps.event.addListener(map, 'center_changed', centerChanged);
    google.maps.event.addDomListener(document.getElementById('crosshair'), 'dblclick', function () {
        map.setZoom(map.getZoom() + 1);
    });
}

function getCenterLatLngText() {
    return '(' + map.getCenter().lat() + ', ' + map.getCenter().lng() + ')';
}

function centerChanged() {
    centerChangedLast = new Date();
    var latlng = getCenterLatLngText();
    document.getElementById('formatedAddress').innerHTML = '';
    currentReverseGeocodeResponse = null;
}

function reverseGeocode() {
    reverseGeocodedLast = new Date();
    geocoder.geocode({ latLng: map.getCenter() }, reverseGeocodeResult);
}

function reverseGeocodeResult(results, status) {
    currentReverseGeocodeResponse = results;
    if (status == 'OK') {
        if (results.length == 0) {
            document.getElementById('formatedAddress').innerHTML = 'None';
        } else {
            document.getElementById('formatedAddress').innerHTML = results[0].formatted_address;
        }
    } else {
        document.getElementById('formatedAddress').innerHTML = 'Error';
    }
}

function addMarkerAtCenter() {
    var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map
    });
    var text = 'Lat/Lng: ' + getCenterLatLngText();
    if (currentReverseGeocodeResponse) {
        var addr = '';
        if (currentReverseGeocodeResponse.size == 0) {
            addr = 'None';
        } else {
            addr = currentReverseGeocodeResponse[0].formatted_address;
        }
        text = "<table><tr><td><img src='/Images/poster.png' height='50px'/></td>" + '<td><b>Address: </b>' + addr + "</td></tr></table>";
    }
    var infowindow = new google.maps.InfoWindow({ content: text });
    google.maps.event.addListener(marker, 'click', function () {
        cm_setInfowindow(infowindow);
        infowindow.open(map, marker);
    });
}
function parseJsonDateIgnoreZone(jsonDateString) {
    var d = new Date();    
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(0); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')));
    return date;
}
function parseJsonDateFIgnoreZone(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
   
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', ''))).toString() + ')/';

    return moment(jsonDateString).format("Do MMM YYYY");
}
function parseJsonDateMilestoneFIgnoreZone(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }    
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', ''))).toString() + ')/';
    return moment(jsonDateString).format("DD-MMM-YYYY");
}

function parseJsonDate(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(0); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', ''))-offset);
    return date;
}
function parseJsonDateNow(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return new Date(); }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')) - offset);
    return date;
}
function parseJsonDatewithNull(jsonDateString) {
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    if (jsonDateString == null || jsonDateString == undefined) { return null; }
    var date = new Date(parseInt(jsonDateString.replace('/Date(', '')) - offset);
    return date;
}

function parseJsonDateF(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', '')) - offset).toString()+')/';

    return moment(jsonDateString).format("Do MMM YYYY");
}

function parseJsonDateMilestoneF(jsonDateString) {
    if (jsonDateString == null || jsonDateString == undefined) { return ""; }
    var d = new Date();
    var offset = d.getTimezoneOffset() * 60000 - 8 * 60 * 60000;
    jsonDateString = '/Date(' + (parseInt(jsonDateString.replace('/Date(', '')) - offset).toString() + ')/';
    return moment(jsonDateString).format("DD-MMM-YYYY");
}
function parseJsonDateS(date) {
    return date.toDateString();
}

function fromDatetoString(date) {
    return moment(date).format("DD-MMM-YYYY");
}
function parseDoubleFormat(item) {
    if (item != null) {
        return item.toFixed(2).replace(/\.?0+$/, "");
    }
    else {
        return 0;
    }
}
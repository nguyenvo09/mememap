﻿function InitialiseFacebook(appId, urlLogin, urlRedirect) {
    window.fbAsyncInit = function () {
        FB.init({
            appId: appId, // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        FB.Event.subscribe('auth.login', function (response) {
            var credentials = { uid: response.authResponse.userID, accessToken: response.authResponse.accessToken };
            SubmitLogin(credentials);
        });


        $("#btnFacebookLogin").bind("click", function (e) {
            //prevent unobstrusive occur before click button
            e.preventDefault();
            if ($('form')[0] != undefined) {
                $.data($('form')[0], 'validator', null);
            }
            alert("here");
            FB.getLoginStatus(function (response) {
                alert(response.status);
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    var credentials = { uid: uid, accessToken: accessToken };
                    SubmitLogin(credentials);
                }
                else if (response.status === 'not_authorized') {
                    // alert("user is not authorised"); 
                    var scopes = 'read_friendlists, email, read_stream,create_event,status_update';
                    FB.login(function (response) {
                    }, { scope: scopes });
                }
                else {
                    // alert("user is not conntected to facebook");
                    var scopes = 'read_friendlists, email, read_stream,create_event,status_update';
                    FB.login(function (response) {
                    }, { scope: scopes });
                }

            });

        });
        function SubmitLogin(credentials) {
            $.ajax({
                url: urlLogin,
                type: "POST",
                data: credentials,
                error: function () {
                    // alert("error logging in to your facebook account.");
                },
                success: function () {
                    window.location.href = urlRedirect;
                }
            });
        }

    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    } (document));

}
function LogOff(appId, urlLogin) {
    window.fbAsyncInit = function () {
        FB.init({
            appId: appId, // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        var logoff = false;

        FB.getLoginStatus(function (response) {
            if (response.status == "unknown") {
                logoff = true;
            }
        });
        $("#btnFacebookLogOff").bind("click", function (e) {
            if (logoff == true) {
                $("#formlogoff").submit();
            }
            e.preventDefault();
            FB.logout(function (response) {
                $("#formlogoff").submit();
            });
        });
    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    } (document));
}
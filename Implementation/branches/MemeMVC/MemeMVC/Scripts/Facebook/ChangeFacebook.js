﻿function InitialiseFacebook(appId,urlLogin,urlRedirect) {
    window.fbAsyncInit = function () {
        FB.init({
            appId: appId, // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true,  // parse XFBML
            version    : 'v2.2' // use version 2.2
        });

        FB.Event.subscribe('auth.login', function (response) {
            var credentials = { uid: response.authResponse.userID, accessToken: response.authResponse.accessToken };
            SubmitLogin(credentials);
        });

        $("#btnFacebookLogin").bind("click", function (e) {
            //prevent unobstrusive occur before click button
            e.preventDefault();
             FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    FB.logout(function (res) {
                        FB.login(function (response) {
                        }, { scope: scopes });
                    });
                    
                }
                else if (response.status === 'not_authorized') {
                    // alert("user is not authorised"); 
                    var scopes = 'email, public_profile,user_friends,user_birthday';
                    FB.login(function (response) {
                    }, { scope: scopes });
                }
                else {
                    // alert("user is not conntected to facebook");
                    var scopes = 'email, public_profile,user_friends,user_birthday';
                    FB.login(function (response) {

                    }, { scope: scopes });
                }

            });

        });
        function SubmitLogin(credentials) {
            $.ajax({
                url: urlLogin,
                type: "POST",
                data: credentials,
                error: function () {
                    alert("There is an error occuss where register by facebook. Please do it later!");
                },
                success: function (data) {
                    if (data.Result == "Success") {
                        window.location.href = urlRedirect;
                    } else {
                        alert("There is an error occuss where register by facebook. Please do it later!");
                    }
                }
            });
        }

    };

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

}
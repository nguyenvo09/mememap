﻿ function InitGoogleLogin(clientid, urlRegister, urlRedirect) {
        // Additional params including the callback, the rest of the params will
        // come from the page-level configuration.
        var additionalParams = {
            'callback': signinCallback,
            'clientid': clientid ,
            'requestvisibleactions': 'http://schema.org/AddAction',
            'cookiepolicy': 'single_host_origin',
            'imediate': false,
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
        };
        // Attach a click listener to a button to trigger the flow.
        var signinButton = document.getElementById('btnGoogleLogin');
        signinButton.addEventListener('click', function () {
            gapi.auth.signOut();
            gapi.auth.signIn(additionalParams); // Will use page level configuration
        });
        function signinCallback(authResult) {
            if (authResult['status']['signed_in']) {
                // Update the app to reflect a signed in user
                // Hide the sign-in button now that the user is authorized, for example:
                googleResult = authResult;
                gapi.client.load('plus', 'v1', function () {
                    var request = gapi.client.plus.people.get({ 'userId': 'me' });
                    request.execute(function (profile) {
                        //alert(profile.id);
                        //alert(profile.emails[0].value);
                        //alert(profile.name.givenName);
                        //alert(profile.name.familyName);
                        SubmitGoogleLogin(profile.id, profile.emails[0].value, profile.name.givenName, profile.name.familyName, authResult["access_token"]);
                    });
                });
                //SubmitGoogleLogin(authResult["access_token"]);

            } else {
                // Update the app to reflect a signed out user
                // Possible error values:
                //   "user_signed_out" - User is signed-out
                //   "access_denied" - User denied access to your app
                //   "immediate_failed" - Could not automatically log in the user
                console.log('Sign-in state: ' + authResult['error']);
            }
        }
        function SubmitGoogleLogin(userid, email, firstname, lastname, accesstoken) {
            var credentials = { accesstoken: accesstoken, userid: userid, email:email, firstname: firstname, lastname: lastname};
            $.ajax({
                url: urlRegister,
                type: "POST",
            data: credentials,
            error: function () {
                alert("There is an error occuss where register by google. Please do it later!");
            },
            success: function (data) {
                if (data.Result == "Success") {
                    window.location.href = urlRedirect;
                } else {
                    alert("There is an error occuss where register by google. Please do it later!");
                }
            }
        });
    }
}
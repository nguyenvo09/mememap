var hotoverlayrscleft = 0;
var hotoverlay =false;

$(window).resize(function () {
    fixposition();
});

$(document).on("toggleTOC", function () {
   
    fixposition();
});

function InitScroll() {

    $('#contentarea').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });
    $('#navmain').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });

    $('#rightcontentarea').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });
    //$('.jspDrag').hide();
    $('#contentarea').mouseenter(function () {
        $('.jspDrag').stop(true, true).fadeIn(0);
    });
    $('#contentarea').mouseleave(function () {
        $('.jspDrag').stop(true, true).fadeOut('slow');
    });

    //$('.jspDrag').hide();
    $('#navmain').mouseenter(function () {
        $('.jspDrag').stop(true, true).fadeIn(0);
    });
    $('#navmain').mouseleave(function () {
        $('.jspDrag').stop(true, true).fadeOut('slow');
    });

    $('#rightcontentarea').mouseenter(function () {
        $('.jspDrag').stop(true, true).fadeIn(0);
    });
    $('#rightcontentarea').mouseleave(function () {
        $('.jspDrag').stop(true, true).fadeOut('slow');
    });

    $('.jspTrack').mouseenter(function () {
        $('.jspTrack').stop(true, true).addClass('jspTrackHover');
        $('.jspDrag').css("width", "10px");
    });
    $('.jspTrack').mouseleave(function () {
        $('.jspTrack').stop(true, true).removeClass('jspTrackHover');
        $('.jspDrag').css("width", "");
    });
    
}

function ReAttachScroll() {
    $('#contentarea').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });
}

function ReAttachNavScroll() {
    $('#navmain').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });
}

function ReAttachRightScroll() {
    $('#rightcontentarea').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });
}


function fixposition() {
    var total_height = $(window).height();
    var total_width = $(window).width();
    if ($(window).width() < 468) {

    }
    $('#sidebarcontainer').height(total_height);
    $('#outerspace').height(total_height);
    $('#mainseperator').height(total_height-40);//padding
    
    
    $('#leftspace').width(250);
    $('#leftspace').height(total_height);

    $("#navmain").height(total_height - 152); //152 distance to the top

    $("#navmain").height(total_height - 152);
    var contentspacewidth = total_width - 250;
   
    if (tocopenned == false) {
        contentspacewidth += 250;
    }
    var o_contentspacewidth = contentspacewidth;

    var mainpanelheight = total_height - 20;    
    var left_width;
    if (contentspacewidth < 1000) {
        left_width = contentspacewidth - 20;
        contentspacewidth = contentspacewidth * 2;

        $('#tblpopular').css({ 'display': 'inline' });
        hotoverlayrscleft = left_width+20;
        if (hotoverlay) {
            $('#rightspacecontainer').css({ 'left': '-' + hotoverlayrscleft.toString() + "px" });
        }
        //alert(contentspacewidth);
    } else {
        $('#tblpopular').css({ 'display': 'none' });
        left_width = (contentspacewidth/2)-10;
    }
    $('#outerspace').width(o_contentspacewidth + 250);

    $('#rightspace').width(o_contentspacewidth);
    $('#rightspace').height(total_height); 

    $('#rightspacecontainer').width(contentspacewidth);
    $('#rightspacecontainer').height(total_height); 

    $('#mainpanel').height(total_height - 150); //110 :margin    
    $('#sidepanel').height(total_height - 150); //120 :padding

    $('#mainpanel').width(contentspacewidth / 2 -5); //110 :margin
    $('#sidepanel').width(contentspacewidth / 2 -5-10); //120 :padding

   
    /*
    var left_width;    
    if ($(window).width() < 1200) {
        $('#sidepanel').css('display', 'none');
        if ($('#mainpanel').hasClass('col-md-7')) {
            $('#mainpanel').removeClass('col-md-7');
            $('#mainpanel').addClass('col-md-12');
        }
        left_width = $('#rightspace').width()-20;
    } else {
        $('#sidepanel').css('display', 'block');
        if ($('#mainpanel').hasClass('col-md-12')) {
            $('#mainpanel').removeClass('col-md-12');
            $('#mainpanel').addClass('col-md-7');
        }
        left_width = $('#mainpanel').width();
    }
    */
    var left_container = mainpanelheight - 150-21;  //120 :padding 60

    $('#contentbody').css('width', left_width - 20); //20 :padding
    
    /*$('#container').css('width', left_width - 40);*/

    $('#contentarea').css('height', left_container);
    $('#rightcontentarea').css('height', left_container);
    //$('#contentbody').css('height', left_container); //10 :padding
    ReAttachScroll();
    ReAttachNavScroll();
    ReAttachRightScroll();
    var right_width = $('#sidepanel').width();    
    $('#rightcontent').css('width', right_width - 10); //10 :padding
    

}


$(document).ready(function () {







    // === Tooltips === //
    $('.tip').tooltip();
    $('.tip-left').tooltip({ placement: 'left' });
    $('.tip-right').tooltip({ placement: 'right' });
    $('.tip-top').tooltip({ placement: 'top' });
    $('.tip-bottom').tooltip({ placement: 'bottom' });

    // === Search input typeahead === //
    $('#search input[type=text]').typeahead({
        source: ['Dashboard', 'Form elements', 'Common Elements', 'Validation', 'Wizard', 'Buttons', 'Icons', 'Interface elements', 'Support', 'Calendar', 'Gallery', 'Reports', 'Charts', 'Graphs', 'Widgets'],
        items: 4
    });

    fixposition();
    Page.init();

    $("#tblpopular").click(function() {
        toggleHOT();
    });

    function toggleHOT() {
        if (!hotoverlay) {
             $('#rightspacecontainer').css({ 'left': '-' + hotoverlayrscleft.toString()+"px" });
        } else 
        {
            $('#rightspacecontainer').css({ 'left': '0px' });
        }
        hotoverlay = !hotoverlay;
    }

    
});

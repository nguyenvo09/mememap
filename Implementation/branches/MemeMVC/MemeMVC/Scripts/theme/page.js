var tocopenned = true;
var Page = (function () {

    var $container = $('#outerspace'),
        $header = $('#header'),
        $mainseperator = $('#mainseperator'),
        $contentheader = $('#contentheader'),
		$tblcontents = $('#tblcontents');

    function init() {
        
        initEvents();

    }

    function initEvents() {

        $tblcontents.click(function() {
            toggleTOC();
        });

    }



    function toggleTOC() {
        var opened = $container.data('opened');
        if (opened == undefined) opened = true;
        opened ? closeTOC() : openTOC();

    }

    function closeTOC() {

        $container.addClass('slideLeft').data('opened', false);
        $mainseperator.addClass('slidemainseperatorLeft');
        $header.addClass('slideheaderLeft');
        $contentheader.addClass('contentheaderleft');
        tocopenned = false;
        $.event.trigger({
            type: "toggleTOC",
            opened: true
        });
    }

    function openTOC(callback) {
        $container.removeClass('slideLeft').data('opened', true);
        $mainseperator.removeClass('slidemainseperatorLeft');
        $header.removeClass('slideheaderLeft');
        $contentheader.removeClass('contentheaderleft');

        tocopenned = true;
        $.event.trigger({
            type: "toggleTOC",
            opened: false
        });
    }

    return { init: init };

})();
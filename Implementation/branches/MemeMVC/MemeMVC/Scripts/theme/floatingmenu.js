//function FloatMenu() {
//    var animationSpeed = 1500;
//    var animationEasing = 'easeOutQuint';
//    var scrollAmount = $(document).scrollTop();
//    var newPosition = menuPosition + scrollAmount;
//    if ($(window).height() < $('#fl_menu').height() + $('#fl_menu .menu').height()) {
//        $('#fl_menu').css('top', menuPosition);
//    } else {
//        $('#fl_menu').css('top', newPosition);
//    }
//}
//$(window).load(function () {
//    menuPosition = $('#fl_menu').position().top;
//    FloatMenu();
//});
//$(window).scroll(function () {
//    FloatMenu();
//});
//jQuery(document).ready(function () {
//    var fadeSpeed = 500;
//    $("#fl_menu").hover(
//		function () { //mouse over
//		    $('#fl_menu .flabel').fadeTo(fadeSpeed, 1);
//		    $("#fl_menu .menu").fadeIn(fadeSpeed);
//		},
//		function () { //mouse out
//		    $('#fl_menu .flabel').fadeTo(fadeSpeed, 0.75);
//		    $("#fl_menu .menu").fadeOut(fadeSpeed);
//		}
//	);
//});

function FloatMenu() {
    var animationSpeed = 1500;
    var animationEasing = 'easeOutQuint';
    var scrollAmount = $(document).scrollTop();
    var newPosition = menuPosition + scrollAmount;
    if ($(window).height() < $('#jump_menu .dropdown .dropdown-menu').height()) {
        $('#jump_menu').css('top', menuPosition);
    } else {
        $('#jump_menu').stop().animate({top: newPosition}, animationSpeed, animationEasing);//.css('top', newPosition);
    }
}
$(window).load(function () {
    menuPosition = $('#jump_menu').position().top;
    FloatMenu();
});
$(window).scroll(function () {
    FloatMenu();
});

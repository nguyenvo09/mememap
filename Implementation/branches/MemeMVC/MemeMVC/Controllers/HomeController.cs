﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MemeMVC.Service;
using Ninject;
using Kendo.Mvc.UI;
using System.Net;
using MemeMVC.ViewModel;
using System.Collections;
using System.Configuration;
using Base.Service;
using Base.Model.DTO;
using Base;
using MemeMVC.Model.DTO;
//using MemeMVC.PowerPoint.ExportModel;
//using MemeMVC.PowerPoint.OpenXML;

namespace MemeMVC.Controllers
{
    public class HomeController : BaseController
    {
        IUserService userService;
        public HomeController()
        {
        }
        [Inject]
        public HomeController(IUserService _userService)
        {
            userService = _userService;
        }
        #region For error display
        [AllowAnonymous]
        public ActionResult Permission()
        {
            //AddSystemAnnouces(CommonService.SystemAnnouces);
            return View();
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            AddSystemAnnouces(CommonService.SystemAnnouces);
            return View();
        }

        [AllowAnonymous]
        public ActionResult Info()
        {
            AddSystemAnnouces(CommonService.SystemAnnouces);
            return View();
        }
        #endregion
        public ActionResult Index()
        {
            return Redirect(Url.Action("Index", "Project"));
            /*ViewBag.ID = 0;
            if (Request.QueryString["currenttab"] != null)
                ViewBag.CurrentTab = Request.QueryString["currenttab"];
            else
                ViewBag.CurrentTab = -1;

            #region Add Toolbar
            Toolbar.HomeControllerToolbar toolbar = new Toolbar.HomeControllerToolbar();
            AddToolbar(toolbar.GetValue());
            #endregion
            return View();*/
        }

        public ActionResult TestPowerPoint() {
            return View();
        }
        //[HttpPost]
        //[AllowAnonymous]
        //public ActionResult DownloadPowerPoint()
        //{
        //    ExportTemplate mv = new ExportTemplate();
        //    mv.SourceUrl = "/Uploads/PowerPoint/Templates/TemplatePresentation.pptx";
        //    mv.SourceFile = Server.MapPath(mv.SourceUrl);
        //    mv.ExportUrl = string.Format("/Uploads/PowerPoint/GenPowerPoint/{0}", string.Format("{0}GenFile.pptx", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
        //    mv.ExportFile = Server.MapPath(mv.ExportUrl);
        //    string imagepath = Server.MapPath("/Uploads/PowerPoint/TestImages");
        //    bool success = PowerPoint.PowerPointUtils.CreatePowePoint(mv.SourceFile, mv.ExportFile, imagepath);
        //    if (!success) mv.Result = Base.ViewModel.ResultType.Unsuccess;
        //    mv.Result = Base.ViewModel.ResultType.Success;
        //    return Json(mv);
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using MemeMVC.Service;
using System.Configuration;
using System.Web.Security;
using MemeMVC.ViewModel;
using System.Net;
using Base.ViewModel;
using Base.Service;
using Base.Model.DTO;
using Base;
using MemeMVC.Model.DTO;

namespace MemeMVC.Controllers
{
    public abstract class BaseController : Controller
    {
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            UrlHelper helper = new UrlHelper(requestContext);
            #region Reset error status
            if (!CommonService.HasError)
                CommonService.SystemAnnouces = new SystemAnnouces();
            #endregion 
            #region Check globalization
            if (requestContext.RouteData.Values["lang"] != null &&
                !string.IsNullOrWhiteSpace(requestContext.RouteData.Values["lang"].ToString()))
            {
                // set the culture from the route data (url)
                var lang = requestContext.RouteData.Values["lang"].ToString();
                try
                {
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
                }
                catch
                {
                    //occur when user select wrong language--default will be set to English
                    requestContext.RouteData.Values["lang"] = "en-US";
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
                }
            }
            else
            {
                // load the culture info from the cookie
                var cookie = requestContext.HttpContext.Request.Cookies["MemeMVC.CurrentUICulture"];
                var langHeader = string.Empty;
                if (cookie != null)
                {
                    // set the culture by the cookie content
                    langHeader = cookie.Value;
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(langHeader);
                }
                else
                {
                    // set the culture by the location if not speicified
                    langHeader = requestContext.HttpContext.Request.UserLanguages[0];
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(langHeader);
                }
                // set the lang value into route data
                requestContext.RouteData.Values["lang"] = langHeader;
            }

            // save the location into cookie
            HttpCookie _cookie = new HttpCookie("MemeMVC.CurrentUICulture", Thread.CurrentThread.CurrentUICulture.Name);
            _cookie.Expires = DateTime.Now.AddYears(1);
            requestContext.HttpContext.Response.SetCookie(_cookie);
            #endregion
            #region Check Session User
            if (requestContext.HttpContext.Request.IsAuthenticated)
            {
                System.Net.HttpStatusCode hCode;
                string currentUrl = requestContext.HttpContext.Request.Url.AbsolutePath.Remove(0, 1);
                UserService uService = new UserService(ConfigurationManager.AppSettings["baseURI"]);
                if (SessionUtils.CurrentUser == null)
                {                   
                }
                else
                {
                    //UserDTO user = uService.GetUser(SessionUtils.CurrentUser.UserID, out hCode);
                    //if (user==null ||user.ValidKey != SessionUtils.CurrentUser.ValidKey)
                    //{
                    //    FormsAuthentication.SignOut();
                    //    SessionUtils.CurrentUser = null;
                    //    helper = new UrlHelper(requestContext);
                    //    requestContext.HttpContext.Response.Redirect( helper.Action("Login","User"));
                    //}
                }                
            }


            #endregion
            #region Load Footer menu
            List<String> listKey = new List<string>();
            listKey.Add("FooterMenu");
            System.Net.HttpStatusCode h2Code;
            
            //need to check h2Code

          
            #endregion
            base.Initialize(requestContext);
        }
        public void AddSystemAnnouce(string name, string message, string guideTip, AnnouceType type)
        {
            SystemAnnouces systemAnounces;
            if (ViewBag.SystemAnnouces != null)
            {
                systemAnounces = ViewBag.SystemAnnouces as SystemAnnouces;
            }
            else
            {
                systemAnounces = new SystemAnnouces();
            }
            systemAnounces.AddAnnouce(name, message, guideTip, type);
            ViewBag.SystemAnnouces = systemAnounces;
        }
        public void AddSystemAnnouces(SystemAnnouces sAnounces)
        {
            SystemAnnouces systemAnounces;
            if (ViewBag.SystemAnnouces != null)
            {
                systemAnounces = ViewBag.SystemAnnouces as SystemAnnouces;
            }
            else
            {
                systemAnounces = new SystemAnnouces();
            }
            if (sAnounces != null && sAnounces.Annouces.Count > 0) systemAnounces.Annouces.AddRange(sAnounces.Annouces);
            ViewBag.SystemAnnouces = systemAnounces;
        }
        /// <summary>
        /// Add new html control into the main toolbar
        /// </summary>
        /// <param name="htmlControl"> html text </param>
        public void AddToolbar(string htmlControl)
        {
            List<string> items = new List<string>();
            if (ViewBag.ToolbarItems != null)
            {
                items = ViewBag.ToolbarItems as List<string>;
            }
            else
            {
                items = new List<string>();
            }
            items.Add(htmlControl);
            ViewBag.ToolbarItems = items;
        }

        public bool CheckAdminPermission() {
            return true;
            //return (SessionUtils.CurrentUser != null && (SessionUtils.CurrentUser.IsUserSystemAdmin || SessionUtils.CurrentUser.IsSystemAdmin)); 
        }
    }
}
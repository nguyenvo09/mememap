using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
using System.ServiceModel.Channels;
namespace MemeMVC.Controllers
{
    public class ProjectController : BaseController
    {
        IProjectService _projectService;
        public ProjectController()
        {
        }
        [Inject]
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            
            return View();
        }

        public ActionResult Index()
        {
            string s = this.RouteData.Values["lang"].ToString();
            return View();
        }

        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.ProjectID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.ProjectID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            ProjectViewModel mv = new ProjectViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Project = new ProjectDTO();
                return Json(mv);
            }
            else
            {
                mv.Project = _projectService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(string sortedfield, bool? sorteddirection)
        {
            HttpStatusCode hCode;
            ProjectsViewModel mv = new ProjectsViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            mv.Projects = _projectService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Projects)
            {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex, int? pagesize, List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            ProjectsViewModel mv = new ProjectsViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            ListDTOModel<ProjectDTO> rList = _projectService.GetPage(param, out hCode);
            mv.PageIndex = pageindex;
            mv.PageSize = pagesize;
            mv.TotalCount = rList.TotalCount;
            mv.Projects = rList.Source;
            foreach (var p in mv.Projects) {
                p.LastAccessDate = p.ModifiedDate.HasValue ? p.ModifiedDate.Value.ToString("dd/MM/yyyy HH:mm") : p.CreatedDate.ToString("dd/MM/yyyy HH:mm");
            }
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(ProjectDTO obj)
        {
            HttpStatusCode hCode;
            ProjectViewModel mv = new ProjectViewModel();
            mv.ProjectID = obj.ProjectID;
            if (obj.ProjectID == 0)
            {
                mv.GUID = obj.GUID;
                mv.Project = obj;
                mv.Project.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Project = _projectService.Create(mv.Project, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Project = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "ProjectID", Value = obj.ProjectID });
                param.Add(new AddedParam { Key = "UserInstanceID", Value = obj.UserInstanceID });
                param.Add(new AddedParam { Key = "Name", Value = obj.Name });
                param.Add(new AddedParam { Key = "Description", Value = obj.Description });
                mv.Project = _projectService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            ProjectViewModel mv = new ProjectViewModel();
            mv.ProjectID = id;
            _projectService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<ProjectDTO> obj)
        {
            HttpStatusCode hCode;
            ProjectsViewModel mv = new ProjectsViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Projects = _projectService.Update(obj, out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
        public ActionResult LinkedProfile()
        {
            return View();
        }
    }
}

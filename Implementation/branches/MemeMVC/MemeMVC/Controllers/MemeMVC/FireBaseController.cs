using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class FireBaseController : BaseController
    {
        public FireBaseController()
        {

        }
        public ActionResult TestChat()
        {
            return View();
        }
        public ActionResult ToChat()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ShareChat()
        {
            return View();
        }
    }
}

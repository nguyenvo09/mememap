using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class UserInstanceController : BaseController
    {
        IUserInstanceService _userinstanceService;
        public UserInstanceController()
        {
        }
        [Inject]
        public UserInstanceController(IUserInstanceService userinstanceService)
        {
            _userinstanceService = userinstanceService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.UserInstanceID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.UserInstanceID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            UserInstanceViewModel mv = new UserInstanceViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.UserInstance = new UserInstanceDTO();
                return Json(mv);
            }
            else
            {
                mv.UserInstance = _userinstanceService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            UserInstancesViewModel mv = new UserInstancesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            mv.UserInstances = _userinstanceService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.UserInstances) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex,int? pagesize,string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            UserInstancesViewModel mv = new UserInstancesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
           ListDTOModel<UserInstanceDTO> rList = _userinstanceService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.UserInstances = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(UserInstanceDTO obj)
        {
            HttpStatusCode hCode;
            UserInstanceViewModel mv = new UserInstanceViewModel();
                mv.UserInstanceID = obj.UserInstanceID;
              if (obj.UserInstanceID == 0)
             {
                mv.GUID = obj.GUID;
                mv.UserInstance = obj;
                mv.UserInstance.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.UserInstance = _userinstanceService.Create(mv.UserInstance, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.UserInstance = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "UserInstanceID", Value = obj.UserInstanceID });
                param.Add(new AddedParam { Key = "UserID", Value = obj.UserID });
                param.Add(new AddedParam { Key = "Guid", Value = obj.Guid });
                mv.UserInstance = _userinstanceService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            UserInstanceViewModel mv = new UserInstanceViewModel();
            mv.UserInstanceID = id;
            _userinstanceService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<UserInstanceDTO> obj)
        {
            HttpStatusCode hCode;
            UserInstancesViewModel mv = new UserInstancesViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.UserInstances = _userinstanceService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class EntityController : BaseController
    {
        IEntityService _entityService;
        public EntityController()
        {
        }
        [Inject]
        public EntityController(IEntityService entityService)
        {
            _entityService = entityService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.EntityID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.EntityID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            EntityViewModel mv = new EntityViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Entity = new EntityDTO();
                return Json(mv);
            }
            else
            {
                mv.Entity = _entityService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            EntitysViewModel mv = new EntitysViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            mv.Entitys = _entityService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Entitys) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex,int? pagesize,string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            EntitysViewModel mv = new EntitysViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
           ListDTOModel<EntityDTO> rList = _entityService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.Entitys = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(EntityDTO obj)
        {
            HttpStatusCode hCode;
            EntityViewModel mv = new EntityViewModel();
                mv.EntityID = obj.EntityID;
              if (obj.EntityID == 0)
             {
                mv.GUID = obj.GUID;
                mv.Entity = obj;
                mv.Entity.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Entity = _entityService.Create(mv.Entity, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Entity = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "EntityID", Value = obj.EntityID });
                param.Add(new AddedParam { Key = "Name", Value = obj.Name });
                param.Add(new AddedParam { Key = "Property", Value = obj.Property });
                param.Add(new AddedParam { Key = "Data", Value = obj.Data });
                mv.Entity = _entityService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            EntityViewModel mv = new EntityViewModel();
            mv.EntityID = id;
            _entityService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<EntityDTO> obj)
        {
            HttpStatusCode hCode;
            EntitysViewModel mv = new EntitysViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Entitys = _entityService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

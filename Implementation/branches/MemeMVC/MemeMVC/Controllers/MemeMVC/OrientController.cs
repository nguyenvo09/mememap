using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class OrientController : BaseController
    {
        IZoneServiceOrient _zoneService;
        public OrientController()
        {
        }
        [Inject]
        public OrientController(IZoneServiceOrient zoneService)
        {
            _zoneService = zoneService;
        }
       
        #region Ajax Action


       

        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            mv.Zones = _zoneService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Zones) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult InsertLink(MemeMVC.Model.DTO.LinksDTO.Link link)
        {
            // var json = JSON.stringify({ CommandData: command, Action: "appendSubIdea" });
            HttpStatusCode hCode;
            bool result = _zoneService.InsertLink(link, out hCode);
            if (result == true)
            {
                return Json(new { result = result, statusCode= "200" });
            }
            return Json(new { result = result, statusCode = "404" });
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult RemoveLink(string ridLink)
        {
            HttpStatusCode hCode;
            bool result = _zoneService.RemoveLink(ridLink, out hCode);
            return Json(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetVertexs(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            VertexsViewModel mv = new VertexsViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            mv.Vertexs = _zoneService.GetManyV(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult AddSubIdea(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                mv.Idea =_zoneService.AddSubIdea(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            

            return Json(mv);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult AddTopIdea(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                mv.Idea = _zoneService.AddTopIdea(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult RemoveSubIdea(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                mv.Idea = _zoneService.RemoveSubIdea(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }

		[AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateParent(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                _zoneService.UpdateParent(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }



        [AllowAnonymous]
        [HttpPost]
        public JsonResult SendQueue(string[] obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                _zoneService.SendQueue(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateNodePosition(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                mv.Idea = _zoneService.UpdateNodePosition(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateNode(CommandDTO obj)
        {
            HttpStatusCode hCode;
            IdeaViewModel mv = new IdeaViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                mv.Idea = _zoneService.UpdateNode(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;


            return Json(mv);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class ZoneController : BaseController
    {
        IZoneService _zoneService;
        public ZoneController()
        {
        }
        [Inject]
        public ZoneController(IZoneService zoneService)
        {
            _zoneService = zoneService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.ZoneID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.ZoneID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            ZoneViewModel mv = new ZoneViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Zone = new ZoneDTO();
                return Json(mv);
            }
            else
            {
                mv.Zone = _zoneService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            
            mv.Zones = _zoneService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Zones) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex, int? pagesize, List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            List<AddedParam> param = new List<AddedParam>();

            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
           ListDTOModel<ZoneDTO> rList = _zoneService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.Zones = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(ZoneDTO obj)
        {
            HttpStatusCode hCode;
            ZoneViewModel mv = new ZoneViewModel();
                mv.ZoneID = obj.ZoneID;
              if (obj.ZoneID == 0)
             {
                mv.GUID = obj.GUID;
                mv.Zone = obj;
                mv.Zone.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Zone = _zoneService.Create(mv.Zone, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Zone = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "ZoneID", Value = obj.ZoneID });
                param.Add(new AddedParam { Key = "MapID", Value = obj.MapID });
                param.Add(new AddedParam { Key = "Data", Value = obj.Data });
                mv.Zone = _zoneService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            ZoneViewModel mv = new ZoneViewModel();
            mv.ZoneID = id;
            _zoneService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<ZoneDTO> obj)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Zones = _zoneService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult Command(CommandDTO obj)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                 _zoneService.Command(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class EmailTemplateController : BaseController
    {
        IEmailTemplateService _emailtemplateService;
        public EmailTemplateController()
        {
        }
        [Inject]
        public EmailTemplateController(IEmailTemplateService emailtemplateService)
        {
            _emailtemplateService = emailtemplateService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.EmailTemplateID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.EmailTemplateID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            EmailTemplateViewModel mv = new EmailTemplateViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.EmailTemplate = new EmailTemplateDTO();
                return Json(mv);
            }
            else
            {
                mv.EmailTemplate = _emailtemplateService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            EmailTemplatesViewModel mv = new EmailTemplatesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            mv.EmailTemplates = _emailtemplateService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.EmailTemplates) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex,int? pagesize,List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            EmailTemplatesViewModel mv = new EmailTemplatesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
           ListDTOModel<EmailTemplateDTO> rList = _emailtemplateService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.EmailTemplates = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(EmailTemplateDTO obj)
        {
            HttpStatusCode hCode;
            EmailTemplateViewModel mv = new EmailTemplateViewModel();
                mv.EmailTemplateID = obj.EmailTemplateID;
              if (obj.EmailTemplateID == 0)
             {
                mv.GUID = obj.GUID;
                mv.EmailTemplate = obj;
                mv.EmailTemplate.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.EmailTemplate = _emailtemplateService.Create(mv.EmailTemplate, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.EmailTemplate = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "EmailTemplateID", Value = obj.EmailTemplateID });
                param.Add(new AddedParam { Key = "EmailTemplateName", Value = obj.EmailTemplateName });
                param.Add(new AddedParam { Key = "Subject", Value = obj.Subject });
                param.Add(new AddedParam { Key = "EmailTemplateContent", Value = obj.EmailTemplateContent });
                mv.EmailTemplate = _emailtemplateService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            EmailTemplateViewModel mv = new EmailTemplateViewModel();
            mv.EmailTemplateID = id;
            _emailtemplateService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<EmailTemplateDTO> obj)
        {
            HttpStatusCode hCode;
            EmailTemplatesViewModel mv = new EmailTemplatesViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.EmailTemplates = _emailtemplateService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class NoteController : BaseController
    {
        INoteService _noteService;
        public NoteController()
        {
        }
        [Inject]
        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.NoteID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.NoteID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            NoteViewModel mv = new NoteViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Note = new NoteDTO();
                return Json(mv);
            }
            else
            {
                mv.Note = _noteService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            NotesViewModel mv = new NotesViewModel();
            List<AddedParam> param = new List<AddedParam>();

            bool isLinkedInMap = false;
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                    if (sparam.Key == "MapID" && Convert.ToInt32(sparam.Value) == 0)
                    {
                        isLinkedInMap = true;
                    }
                }
            }
            if (!isLinkedInMap)
            {
                mv.Notes = _noteService.GetMany(param, out hCode).ToList();
            }
            else
            {
                hCode = HttpStatusCode.OK;
                if (Session["LinkedInNotes"] != null)
                {
                    mv.Notes = new List<NoteDTO>();
                    foreach (NoteLinkedInDTO note in (List<NoteLinkedInDTO>)Session["LinkedInNotes"])
                    {
                        NoteDTO newnote = new NoteDTO();
                        newnote.Body = note.Body;
                        newnote.RID = note.RID;
                        newnote.MapID = note.MapID;
                        newnote.NoteID = note.NoteID;
                        mv.Notes.Add(newnote);
                    }
                }
                else
                {
                    mv.Notes = new List<NoteDTO>();
                }
            }
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex, int? pagesize, List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            NotesViewModel mv = new NotesViewModel();
            List<AddedParam> param = new List<AddedParam>();

            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
           ListDTOModel<NoteDTO> rList = _noteService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.Notes = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(NoteDTO obj)
        {
            HttpStatusCode hCode;
            NoteViewModel mv = new NoteViewModel();
                mv.NoteID = obj.NoteID;
              if (obj.NoteID == 0)
             {
                mv.Note = obj;
                mv.Note.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Note = _noteService.Create(mv.Note, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Note = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "NoteID", Value = obj.NoteID });
                param.Add(new AddedParam { Key = "MapID", Value = obj.MapID });
                param.Add(new AddedParam { Key = "Body", Value = obj.Body });
                param.Add(new AddedParam { Key = "RID", Value = obj.RID });
                param.Add(new AddedParam { Key = "Url", Value = obj.Url });
                param.Add(new AddedParam { Key = "Type", Value = obj.Type });
                mv.Note = _noteService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            NoteViewModel mv = new NoteViewModel();
            mv.NoteID = id;
            _noteService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<NoteDTO> obj)
        {
            HttpStatusCode hCode;
            NotesViewModel mv = new NotesViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Notes = _noteService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult Command(CommandDTO obj)
        {
            HttpStatusCode hCode;
            NotesViewModel mv = new NotesViewModel();
            if (obj == null)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                 _noteService.Command(obj, out hCode);
            }
            catch (Exception ex)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

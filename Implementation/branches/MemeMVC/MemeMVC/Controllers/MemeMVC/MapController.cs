using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
using MemeMVC.Model.DTO.Mongo;
using MemeMVC.Repository;
using Newtonsoft.Json;
using MongoDB.Bson;
namespace MemeMVC.Controllers
{
    public class MapController : BaseController
    {
        IMapService _mapService;
        ICommentRepository _commentRepository;
        IUserService _userService;
        IUserMapSharedRepository _userMapSharedRepo;
        ITaskEntityRepository _taskEntityRepo;
        ITaskPriorityRepository _taskPriorityRepo;
        ITaskStatusRepository _taskStatusRepo;
        ITaskTypeRepository _taskTypeRepo;
        public MapController()
        {
        }
        [Inject]
        public MapController(IMapService mapService, ICommentRepository commentRepository, IUserService userService, 
            IUserMapSharedRepository userMapSharedRepo, 
            ITaskEntityRepository taskEntityRepo,
            ITaskPriorityRepository taskPriorityRepo,
            ITaskStatusRepository taskStatusRepo,
            ITaskTypeRepository taskTypeRepo)
        {
            _mapService = mapService;
            _commentRepository = commentRepository;
            _userService = userService;
            _userMapSharedRepo = userMapSharedRepo;
            _taskEntityRepo = taskEntityRepo;
            _taskPriorityRepo = taskPriorityRepo;
            _taskStatusRepo = taskStatusRepo;
            _taskTypeRepo = taskTypeRepo;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult Detail1()
        {
            string url = Request.Url.AbsolutePath;
            int projectid = Convert.ToInt32(url.Substring(url.LastIndexOf("/") + 1));
            ViewBag.ProjectID = projectid;
            return View();
        }

        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.MapID = id;
            ViewBag.UserID = SessionUtils.CurrentUser.UserID;
            ViewBag.UserName = SessionUtils.CurrentUser.UserName;
            return View();
        }
        public ActionResult DetailList(int id)
        {
            ViewBag.MapID = id;
            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult View(int id)
        {
            ViewBag.MapID = id;
            return View();
        }
        [AllowAnonymous]
        public ActionResult ViewLinkedIn()
        {
            ViewBag.PhotoUrl = "";
            ViewBag.ProfileName = "";
            ViewBag.ProfileId = "";
            ViewBag.MapID = 0;
            if (Session["LinkedInUser"] != null)
            {
                LiPerson user= Session["LinkedInUser"] as LiPerson;
                ViewBag.PhotoUrl = user.PictureUrl;
                ViewBag.ProfileName = user.FirstName + " " + user.LastName ;
                ViewBag.ProfileId = user.id;
            }

            return View();
        }


        public ActionResult Edit(int id)
        {
            ViewBag.MapID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            MapViewModel mv = new MapViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Map = new MapDTO();
                return Json(mv);
            }
            else
            {
                mv.Map = _mapService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            MapsViewModel mv = new MapsViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            mv.Maps = _mapService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Maps)
            {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex, int? pagesize, List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            MapsViewModel mv = new MapsViewModel();
            List<AddedParam> param = new List<AddedParam>();

            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }
            ListDTOModel<MapDTO> rList = _mapService.GetPage(param, out hCode);
            mv.PageIndex = pageindex;
            mv.PageSize = pagesize;
            mv.TotalCount = rList.TotalCount;
            mv.Maps = rList.Source;
            foreach (var p in mv.Maps)
            {
                p.LastAccessDate = p.ModifiedDate.HasValue ? p.ModifiedDate.Value.ToString("dd/MM/yyyy HH:mm") : p.CreatedDate.ToString("dd/MM/yyyy HH:mm");
            }
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(MapDTO obj)
        {
            HttpStatusCode hCode;
            MapViewModel mv = new MapViewModel();
            mv.MapID = obj.MapID;
            if (obj.MapID == 0)
            {
                mv.GUID = obj.GUID;
                mv.Map = obj;
                mv.Map.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Map = _mapService.Create(mv.Map, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Map = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "MapID", Value = obj.MapID });
                param.Add(new AddedParam { Key = "ProjectID", Value = obj.ProjectID });
                param.Add(new AddedParam { Key = "Name", Value = obj.Name });
                param.Add(new AddedParam { Key = "Description", Value = obj.Description });
                mv.Map = _mapService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            MapViewModel mv = new MapViewModel();
            mv.MapID = id;
            _mapService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<MapDTO> obj)
        {
            HttpStatusCode hCode;
            MapsViewModel mv = new MapsViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Maps = _mapService.Update(obj, out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }

        [AllowAnonymous]
        public JsonResult GetSuggestion(string keywords)
        {
            SuggestionViewModel mv = new SuggestionViewModel();
            mv.suggests.headlines = new List<HeadLineDTO>();
            HeadLineDTO hl = new HeadLineDTO();
            hl.name = "ABCD";
            SuggestWordDTO word = new SuggestWordDTO();
            word.name = "asd";
            mv.suggests.headlines.Add(hl);
            mv.words.Add(word);
            return Json(mv,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult InsertUserComment(string username, int mapId, int userId, int nodeId, string comment, DateTime createdDate, string imgProfile, string rid)
        {
            try
            {
                UserComment user_comment = new UserComment(mapId, nodeId, userId, username, comment, createdDate, imgProfile, rid);
                var res = _commentRepository.SaveOrUpdate(user_comment);
                if (res == true){
                    return Json(user_comment.Id.ToString());
                }
            }
            catch (Exception e)
            {
                return Json("");
            }
            return Json("");
        }
        [HttpPost]
        public JsonResult GetAllCommentOneMap(int mapId)
        {
            try
            {
                List<UserComment> result = _commentRepository.GetMany(c => c.mapId == mapId).ToList();
                string ret = JsonConvert.SerializeObject(result);
                return Json(ret);
            }
            catch (Exception e)
            {
                return Json("");
            }
        }
        /// <summary>
        /// Get number of comments of each node per map
        /// </summary>
        /// <param name="mapId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCountPerNode(int mapId)
        {
            try
            {
                var t = _commentRepository.GetAllQueryable().Where(c => c.mapId == mapId).GroupBy(e => new { e.mapId, e.nodeId }).Select(group => new { mapId = group.Key.mapId, nodeId = group.Key.nodeId, Count = group.Count() });
                var sql = t.ToList();
                string ret = JsonConvert.SerializeObject(sql);
                return Json(ret);
            }
            catch (Exception e)
            {
                return Json("");
            }
        }
        /// <summary>
        /// Get number of comments of each node per map
        /// </summary>
        /// <param name="mapId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCmtPerNode(int mapId, int nodeId, int limit, string lowerBoundTime)
        {
            
            try
            {
                var d = DateTime.Now;
                List<UserComment> list = null;
                if (lowerBoundTime != null)
                {
                    d = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.Parse(lowerBoundTime));
                    list = _commentRepository.GetAllQueryable().Where(c => c.mapId == mapId && c.nodeId == nodeId && c.createdDate >  d.ToUniversalTime()).Take(limit).OrderBy(c => c.createdDate).ToList();
                }
                else
                {
                    list = _commentRepository.GetAllQueryable().Where(c => c.mapId == mapId && c.nodeId == nodeId).Take(limit).OrderBy(c => c.createdDate).ToList();

                }
                
                string ret = JsonConvert.SerializeObject(list);
                return Json(ret);
            }
            catch (Exception e)
            {
                return Json("");
            }
        }
        /// <summary>
        /// Get list of maps shared with the current users
        /// </summary>
        /// <param name="id">userId</param>
        /// <returns></returns>
        public ActionResult SharedMap(int userId)
        {

            
            //ViewBag.UserID = SessionUtils.CurrentUser.UserID;
            //ViewBag.UserName = SessionUtils.CurrentUser.UserName;
            List<UserMapShared> res = _userMapSharedRepo.GetAllQueryable().Where(um => um.userId == userId).ToList();
            ViewBag.SharedMaps = res;
            return View();
        }
      
        public ActionResult ShareDetail(int id)
        {
            ViewBag.MapID = id;
            List<UserMapShared> res = _userMapSharedRepo.GetAllQueryable().Where(um => um.mapId == id).ToList();
            //ViewBag.UserMapShared = res;
            ViewBag.JsonSharedUserMap = JsonConvert.SerializeObject(res);
            return View();
        }
        /// <summary>
        /// Get users that are shared with @mapId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSharedUser(int mapId)
        {
            List<UserMapShared> res = _userMapSharedRepo.GetAllQueryable().Where(um => um.mapId == mapId).ToList();
            string ret = JsonConvert.SerializeObject(res);
            return Json(ret);
        }

        [HttpPost]
        public JsonResult RemoveComment(string commentId)
        {
            bool ret = _commentRepository.Delete(commentId);
            return Json(ret);
        }
        public JsonResult SaveOrUpdateTask(int mapId, int nodeId, string taskId, string title, string estimation, string dueDate, string startDate, string completeDate, string type, string status, string priority, List<String> assignees)
        {
            try
            {
                DateTime due = DateTime.Parse(dueDate);
                DateTime start = DateTime.Parse(startDate);
                DateTime complete = DateTime.Parse(completeDate);
                TaskEntity task = null;

                if (taskId != null && taskId != "")
                {
                    task = _taskEntityRepo.Get(c => c.Id.ToString() == taskId);
                    task.Title = title;
                    task.Estimation = estimation;
                    task.DueDate = due;
                    task.StartDate = start;
                    task.CompletionDate = complete;
                    task.taskType = type;
                    task.taskPriority = priority;
                    task.taskStatus = status;
                    task.Assignees = assignees;
                    
                }
                else
                {
                    task = new TaskEntity(mapId, nodeId, title, estimation, due, start, complete, type, priority, status, assignees);
                }

                bool result = _taskEntityRepo.SaveOrUpdate(task);

                if (result == true)
                {
                    return Json(new { statusCode = "200", result = true, taskId = task.Id.ToString() });
                }
                return Json(new { statusCode = "404", result = false });
            }
            catch (Exception e)
            {
                return Json(new { statusCode = "404", result = false });
            }
            
        }
        /// <summary>
        /// Each node, we have only one task, so we will use nodeId to get task detail.
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public JsonResult GetTaskDetail(int nodeId)
        {
            TaskEntity task = _taskEntityRepo.Get(c => c.nodeId == nodeId);
            if (task == null)
            {
                return Json(new {statusCode= "404" }); 
            }else{
                string ret = JsonConvert.SerializeObject(task);
                return Json(new {statusCode="200", Result=ret}); 
            }
            
        }
        public JsonResult GetTaskConfig()
        {
            List<TaskPriority> priorities = _taskPriorityRepo.GetAllQueryable().ToList();
            List<TaskStatus> status = _taskStatusRepo.GetAllQueryable().ToList();
            List<TaskType> types = _taskTypeRepo.GetAllQueryable().ToList();
            string task_priorities = JsonConvert.SerializeObject(priorities);
            string task_status = JsonConvert.SerializeObject(status);
            string task_types = JsonConvert.SerializeObject(types);
            var ret = JsonConvert.SerializeObject(new { task_priorities = task_priorities, task_status = task_status, task_types = task_types });
            return Json(ret);
        }
        #endregion
    }
}

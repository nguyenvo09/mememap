using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class MediaController : BaseController
    {
        IMediaService _mediaService;
        public MediaController()
        {
        }
        [Inject]
        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }
        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.MediaID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.MediaID = id;
            return View();
        }
        
        
        /*[AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(CacheProfile = "CustomerImages")]
        public ActionResult ResizeImage(string url, int maxwith, int maxheight)
        {
            ViewBag.MediaID = 0;
            return View();
        }
        */

        public ActionResult Create()
        {
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            MediaViewModel mv = new MediaViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.Media = new MediaDTO();
                return Json(mv);
            }
            else
            {
                mv.Media = _mediaService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            MediasViewModel mv = new MediasViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            mv.Medias = _mediaService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Medias) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex,int? pagesize,string sortedfield,bool? sorteddirection)
        {
            HttpStatusCode hCode;
            MediasViewModel mv = new MediasViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
           ListDTOModel<MediaDTO> rList = _mediaService.GetPage(param, out hCode);
           mv.PageIndex = pageindex;
           mv.PageSize = pagesize;
           mv.TotalCount = rList.TotalCount;
           mv.Medias = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(MediaDTO obj)
        {
            HttpStatusCode hCode;
            MediaViewModel mv = new MediaViewModel();
                mv.MediaID = obj.MediaID;
              if (obj.MediaID == 0)
             {
                mv.GUID = obj.GUID;
                mv.Media = obj;
                mv.Media.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.Media = _mediaService.Create(mv.Media, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.Media = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "MediaID", Value = obj.MediaID });
                param.Add(new AddedParam { Key = "Data", Value = obj.Data });
                param.Add(new AddedParam { Key = "Media_Guid", Value = obj.Media_Guid });
                mv.Media = _mediaService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            MediaViewModel mv = new MediaViewModel();
            mv.MediaID = id;
            _mediaService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<MediaDTO> obj)
        {
            HttpStatusCode hCode;
            MediasViewModel mv = new MediasViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Medias = _mediaService.Update(obj,out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion
    }
}

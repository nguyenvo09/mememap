using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
namespace MemeMVC.Controllers
{
    public class LinkedInController : BaseController
    {
        LinkedInService _linkedinService;
        
        [Inject]
        public LinkedInController()
        {
            
        }
       
        #region Ajax Action

              

        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(List<SearchParam> searchparams)
        {
            HttpStatusCode hCode;
            ZonesViewModel mv = new ZonesViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (searchparams != null && searchparams.Count > 0)
            {
                foreach (var sparam in searchparams)
                {
                    AddedParam aparam = new AddedParam();
                    aparam.Key = sparam.Key;
                    aparam.Value = sparam.Value;
                    param.Add(aparam);
                }
            }


            mv.Zones = GetLinkedInMap();


            

            foreach (var item in mv.Zones) {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }

        #endregion

        public List<ZoneDTO>  GetLinkedInMap()
        {
            List<ZoneDTO> result = new List<ZoneDTO>();
            List<VertexDTO> toreturndto = new List<VertexDTO>();

            if (_linkedinService == null && Session["accessLinkedInToken"] != null && Session["accessLinkedInTokenSecret"] != null)
            {
                _linkedinService = new LinkedInService(Session["accessLinkedInToken"].ToString(), Session["accessLinkedInTokenSecret"].ToString());
            }
            

            if (Session["LinkedInUser"] != null)
            {
                LiPerson user = Session["LinkedInUser"] as LiPerson;
                ViewBag.PhotoUrl = user.PictureUrl;
                ViewBag.ProfileName = user.FirstName + " " + user.LastName;
                ViewBag.ProfileId = user.id;

                VertexDTO vertexdto = new VertexDTO();
                vertexdto.name = user.FirstName + " " + user.LastName;
                vertexdto.mapid = 0;
                vertexdto.rid = "#1:1";
                vertexdto.level = 0;
                vertexdto.currentX = 0;
                vertexdto.currentY = 50;
                vertexdto.hasCustomedLocation = true;
                vertexdto.data = "{\"type\":\"rec\",\"imgsrc\":\"" + user.PictureUrl + "\"}";//"+user.PictureUrl+"
                vertexdto.childNodes = new List<VertexDTO>();
                toreturndto.Add(vertexdto);

                LiConnectResult connectresult;
                if (Session["LinkedInConnectionv1"] == null)
                {
                    connectresult = _linkedinService.GetPeopleByConnectionL1();
                    Session["LinkedInConnectionv1"] = connectresult;

                }
                else
                {
                    connectresult = (LiConnectResult)Session["LinkedInConnectionv1"];
                }


                LiJobSuggestion jobsuggest;

                jobsuggest = _linkedinService.GetJobSuggestion();

            
                LiGroupSuggestion groupsuggest;

                groupsuggest = _linkedinService.GetGroupSuggestion();
                /*if (Session["LinkedGroupSuggestionv1"] == null)
                {
                    groupsuggest = _linkedinService.GetGroupSuggestion();
                    Session["LinkedGroupSuggestionv1"] = groupsuggest;

                }
                else
                {
                    groupsuggest = (LiGroupSuggestion)Session["LinkedGroupSuggestionv1"];
                }*/

                LiPersonSkills skillsresult;
                skillsresult = _linkedinService.GetMySkills();
                /*if (Session["LinkedInSkillsv2"] == null)
                {
                    skillsresult = _linkedinService.GetMySkills();
                    Session["LinkedInSkillsv2"] = skillsresult;

                }
                else
                {
                    skillsresult = (LiPersonSkills)Session["LinkedInSkillsv2"];
                }*/

                Session["LinkedInNotes"] = new List<NoteLinkedInDTO>();
                //connectresult = _linkedinService.GetPeopleByConnectionL1();

                Dictionary<string, VertexDTO> industriesmap = new Dictionary<string, VertexDTO>();
                Dictionary<string, NoteLinkedInDTO> notesmap = new Dictionary<string, NoteLinkedInDTO>();

                VertexDTO vertexdto1 = new VertexDTO();
                vertexdto1.name = "People connect:"+connectresult.Total.ToString();
                vertexdto1.mapid = 0;
                vertexdto1.rid = "#1:2";
                vertexdto1.level = 1;
                vertexdto1.currentX = 0;
                vertexdto1.currentY = 150;
                vertexdto1.hasCustomedLocation = true;
                vertexdto1.data = "{\"bgcolor\":\"1880DB\",\"textcolor\":\"FFFFFF\",\"type\":\"rec\",\"imgsrc\":\"\"}";
                vertexdto1.childNodes = new List<VertexDTO>();
                vertexdto.childNodes.Add(vertexdto1);
                int id = 3;
                int rowcount = Convert.ToInt32(Math.Sqrt(connectresult.Total));
                if (rowcount==0) rowcount=1;
                int sid=connectresult.Total+2;
                int totalcount=connectresult.Total;
                /*foreach (LiPerson person in connectresult.Persons)//if (id<20)                     
                {
                    if (person.Positions != null)
                    {
                        foreach (LiPosition position in person.Positions)
                        {

                        }
                    }

                    VertexDTO vertexcompanydto = new VertexDTO();
                    vertexcompanydto.name = person.FirstName + " " + person.LastName + "\n" + person.headLine;
                    vertexcompanydto.mapid = 0;
                    vertexcompanydto.rid = "#1:" + id.ToString();
                    vertexcompanydto.level = 0;
                    vertexcompanydto.currentX = (((id - 3) % rowcount) + 1) * 200 - (rowcount - 1) * 200 / 2;
                    vertexcompanydto.currentY = (((id - 3) / rowcount) + 1) * 300;
                    vertexcompanydto.hasCustomedLocation = true;
                    vertexcompanydto.data = "{\"type\":\"rec\",\"imgsrc\":\"" + person.PictureUrl + "\"}";//" + user.PictureUrl + "
                    vertexcompanydto.childNodes = new List<VertexDTO>();
                    vertexdto1.childNodes.Add(vertexcompanydto); id++;
                }*/

                foreach (LiPerson person in connectresult.Persons)//if (id<20)                     
                {
                    string industry="(none)";
                    if (person.Industry != null) industry = person.Industry;

                    if (!industriesmap.ContainsKey(industry))
                    {

                        VertexDTO vertexindustrydto = new VertexDTO();
                        vertexindustrydto.name = industry;
                        vertexindustrydto.mapid = 1;
                        vertexindustrydto.rid = "#1:" + id.ToString();
                        vertexindustrydto.level = 1;
                        //vertexpersondto.currentX = (((id - 3) % rowcount) + 1) * 600 - (rowcount - 1) * 600 / 2;
                        //vertexpersondto.currentY = (((id-3) / rowcount)+1) * 650;
                        vertexindustrydto.currentX = 0;
                        vertexindustrydto.currentY = 0;
                        vertexindustrydto.hasCustomedLocation = true;
                        vertexindustrydto.data = "{\"type\":\"cir\",\"imgsrc\":\"\"}";//" + person.PictureUrl + "
                        vertexindustrydto.childNodes = new List<VertexDTO>();

                        industriesmap.Add(industry, vertexindustrydto);

                        vertexdto1.childNodes.Add(vertexindustrydto);

                        NoteLinkedInDTO note = new NoteLinkedInDTO();
                        note.RID = vertexindustrydto.rid;
                        note.MapID = 0;
                        note.NoteID = id;
                        notesmap.Add(industry,note);
                        ((List<NoteLinkedInDTO>)Session["LinkedInNotes"]).Add(note);
                        id++;
                    }

                    VertexDTO vertexpersondto = new VertexDTO();
                    vertexpersondto.name = person.FirstName + " " + person.LastName+"\n"+ person.headLine+"\n"+ person.Industry;
                    vertexpersondto.mapid = 0;
                    vertexpersondto.rid = "#1:" + id.ToString(); 
                    vertexpersondto.level = 1;
                    //vertexpersondto.currentX = (((id - 3) % rowcount) + 1) * 600 - (rowcount - 1) * 600 / 2;
                    //vertexpersondto.currentY = (((id-3) / rowcount)+1) * 650;
                    vertexpersondto.currentX = 0;
                    vertexpersondto.currentY = 0;
                    vertexpersondto.hasCustomedLocation = true;
                    vertexpersondto.data = "{\"type\":\"rec\",\"imgsrc\":\"" + person.PictureUrl + "\"}";//" + person.PictureUrl + "
                    vertexpersondto.childNodes = new List<VertexDTO>();
                    //vertexdto1.childNodes.Add(vertexpersondto);
                    
                    industriesmap[industry].childNodes.Add(vertexpersondto);
                    id++;

                    string notehtml="<div style='padding:10px;font-size:10px; font-family:Abel;float:left;width:100%; margin-bottom:10px;border-bottom:1px solid #dddddd; '>";
                    notehtml += "<div style='float:left;width:20%'><img src='" + person.PictureUrl + "'/></div>";
                    notehtml += "<div style='float:right;width:75%'>";
                    notehtml += "<b>" + person.FirstName + " " + person.LastName + "</b><br/>";
                    if (person.Positions!=null && person.Positions.PositionList != null)
                    {
                        int pos=0;
                        
                        foreach (LiPosition position in person.Positions.PositionList)
                        {
                            sid++;pos++;
                            string company = position.Company != null ? position.Company.Name : "";
                            string summary = position.Summary != null ? position.Summary : "";

                            notehtml += "<br/><b>" + position.Title + "</b>" + "-" + company + "<br/>" + summary.Replace("\n", "<br/>");
                            /*VertexDTO vertexpositiondto = new VertexDTO();
                            try
                            {
                                vertexpositiondto.name = position.Title ;
                                
                            }
                            catch (Exception ex)
                            {
                                vertexpositiondto.name = "Empty Position";
                            }
                            vertexpositiondto.mapid = 0;
                            vertexpositiondto.rid = "#1:" + id.ToString();
                            vertexpositiondto.level = 1;
                            vertexpositiondto.currentX =  100;
                            vertexpositiondto.currentY = (pos-1)*50;
                            vertexpositiondto.hasCustomedLocation = true;
                            vertexpositiondto.data = "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                            vertexpersondto.childNodes.Add(vertexpositiondto);
                            */
                            id++;
                        }
                        
                    }
                    notehtml += "</div></div>";
                    notesmap[industry].Body += notehtml;
                }
                int nodedistance = 350;
                int totallength = 0;
                int[] lengths= new int[vertexdto1.childNodes.Count+1];
                int[] radius = new int[vertexdto1.childNodes.Count+1];
                for (int i = 0; i < vertexdto1.childNodes.Count; i++)
                {
                    int r = 1;
                    //while (r * 2 * 3.15 < vertexdto1.childNodes[i].childNodes.Count) r++;

                    int rlimit = Convert.ToInt32(Math.Floor(r * 2 * Math.PI))-1;
                    int starx = 0; int stary = -r * nodedistance; int angle=0;
                    for (int j = 0; j < vertexdto1.childNodes[i].childNodes.Count; j++)
                    {
                        vertexdto1.childNodes[i].childNodes[j].currentX =  starx;
                        vertexdto1.childNodes[i].childNodes[j].currentY = stary;
                        if (j + 1 > rlimit )
                        {
                            r++; rlimit += Convert.ToInt32(Math.Floor(r * 2 * Math.PI));
                            starx = 0; stary = -r * nodedistance;angle=0;
                        }
                        else
                        {
                            angle += Convert.ToInt32(360 / (Math.Floor(r * 2 * Math.PI)+1));
                            stary = Convert.ToInt32(-1 * Math.Cos(Math.PI * angle / 180.0) * r * nodedistance);
                            starx = Convert.ToInt32(Math.Sin(Math.PI * angle / 180.0) * r * nodedistance);
                        }
                    }

                    //vertexdto1.childNodes[i].currentY = (r) * nodedistance;
                    //totallength += r * 2 * nodedistance+200;
                    lengths[i+1]=(r*2)*(nodedistance)-200;
                }

                int shareangle = 360*2 / vertexdto1.childNodes.Count;
                for (int k = 0; k < vertexdto1.childNodes.Count-1; k++)
                    for (int j = k+1; j < vertexdto1.childNodes.Count; j++)
                        if (vertexdto1.childNodes[k].childNodes.Count > vertexdto1.childNodes[j].childNodes.Count)
                {
                    int temp = lengths[k+1]; lengths[k+1] = lengths[j+1]; lengths[j+1] = temp;
                    VertexDTO tempdtok = vertexdto1.childNodes[k];
                    VertexDTO tempdtoj = vertexdto1.childNodes[j];
                    vertexdto1.childNodes.RemoveAt(k);
                    vertexdto1.childNodes.Insert(k, tempdtoj);
                    vertexdto1.childNodes.RemoveAt(j);
                    vertexdto1.childNodes.Insert(j, tempdtok);
                          
                }



                VertexDTO vertexallskill = new VertexDTO();
                vertexallskill.name = "My Skills:" + skillsresult.Skills.SkillList.Length.ToString();
                vertexallskill.mapid = 0;
                vertexallskill.rid = "#1:" + (id + 1).ToString();
                vertexallskill.level = 1;
                vertexallskill.hasCustomedLocation = true;
                vertexallskill.currentX = 0;
                vertexallskill.currentY = -10;
                vertexallskill.data = "{\"bgcolor\":\"1880DB\",\"textcolor\":\"FFFFFF\",\"type\":\"rec\",\"imgsrc\":\"\"}";
                vertexallskill.childNodes = new List<VertexDTO>();
                vertexdto1.childNodes.Insert(0,vertexallskill);
                id++;

                VertexDTO vertexgroupsuggestion = new VertexDTO();
                vertexgroupsuggestion.name = "My Group Suggestions:" + groupsuggest.Total.ToString();
                vertexgroupsuggestion.mapid = 0;
                vertexgroupsuggestion.rid = "#1:" + (id + 1).ToString();
                vertexgroupsuggestion.level = 1;
                vertexgroupsuggestion.hasCustomedLocation = true;
                vertexgroupsuggestion.currentX = 0;
                vertexgroupsuggestion.currentY = -(((skillsresult.Skills.SkillList.Length - 1) / 5) + 2) * 50;
                vertexgroupsuggestion.data = "{\"bgcolor\":\"1880DB\",\"textcolor\":\"FFFFFF\",\"type\":\"rec\",\"imgsrc\":\"\"}";
                vertexgroupsuggestion.childNodes = new List<VertexDTO>();
                vertexallskill.childNodes.Add( vertexgroupsuggestion);

                id++;
                VertexDTO vertexjobsuggestion = new VertexDTO();
                vertexjobsuggestion.name = "My Job Suggestions:" + jobsuggest.Jobs.JobList.Length.ToString();
                vertexjobsuggestion.mapid = 0;
                vertexjobsuggestion.rid = "#1:" + (id + 1).ToString();
                vertexjobsuggestion.level = 1;
                vertexjobsuggestion.hasCustomedLocation = true;
                vertexjobsuggestion.currentX = -500;
                vertexjobsuggestion.currentY = 250;
                vertexjobsuggestion.data = "{\"bgcolor\":\"1880DB\",\"textcolor\":\"FFFFFF\",\"type\":\"rec\",\"imgsrc\":\"\"}";
                vertexjobsuggestion.childNodes = new List<VertexDTO>();
                vertexallskill.childNodes.Add(vertexjobsuggestion);




                radius[0]= 100;
                lengths[0]= 100;
                int posskill = 0;
                foreach (LiSkillWrap skill in skillsresult.Skills.SkillList)
                {
                    id++;
                    VertexDTO vertexskill = new VertexDTO();
                    vertexskill.name = skill.Skill.Name;
                    vertexskill.mapid = 0;
                    vertexskill.rid = "#1:" + (id + 1).ToString();
                    vertexskill.level = 1;

                    posskill++;
                    vertexskill.currentX = -250 + (((posskill - 1)%5)) * 130;
                    vertexskill.currentY = -(((posskill - 1) / 5) + 1) * 50; 

                    vertexskill.hasCustomedLocation = true;
                    vertexskill.data = "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                    vertexskill.childNodes = new List<VertexDTO>();
                    vertexallskill.childNodes.Add(vertexskill);
                }

                int posgroup = 0;
                foreach (LiGroup groupsuggestion in groupsuggest.Groups)
                {
                    id++;
                    VertexDTO vertexgroup = new VertexDTO();
                    vertexgroup.name = groupsuggestion.Name + "\n" + "\n";
                    vertexgroup.mapid = 0;
                    vertexgroup.rid = "#1:" + (id + 1).ToString();
                    vertexgroup.level = 1;

                    NoteLinkedInDTO note = new NoteLinkedInDTO();
                    note.RID = vertexgroup.rid;
                    note.MapID = 0;
                    note.NoteID = id;
                    note.Body = groupsuggestion.Descriptions.Replace("\n", "<br/>");
                    ((List<NoteLinkedInDTO>)Session["LinkedInNotes"]).Add(note);

                    posgroup++;
                    vertexgroup.currentX = -250 + (((posgroup - 1) % 5)) * 110;
                    vertexgroup.currentY = -(((posgroup - 1) / 5) + 1) * 120;

                    vertexgroup.hasCustomedLocation = true;
                    vertexgroup.data = "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                    vertexgroup.childNodes = new List<VertexDTO>();
                    vertexgroupsuggestion.childNodes.Add(vertexgroup);
                }

                int posjob = 0;
                foreach (LiJob jobsuggestion in jobsuggest.Jobs.JobList)
                {
                    id++;
                    VertexDTO vertexjob = new VertexDTO();
                    vertexjob.name = jobsuggestion.Company.Name + "\n" + jobsuggestion.Location + "\n" + "\n";
                    vertexjob.mapid = 0;
                    vertexjob.rid = "#1:" + (id + 1).ToString();
                    vertexjob.level = 1;

                    NoteLinkedInDTO note = new NoteLinkedInDTO();
                    note.RID = vertexjob.rid;
                    note.MapID = 0;
                    note.NoteID = id;
                    note.Body = jobsuggestion.Description.Replace("\n", "<br/>");
                    ((List<NoteLinkedInDTO>)Session["LinkedInNotes"]).Add(note);

                    posjob++;
                    vertexjob.currentX = -250 + (((posjob - 1) % 5)) * 110;
                    vertexjob.currentY = -(((posjob - 1) / 5) + 1) * 100;

                    vertexjob.hasCustomedLocation = true;
                    vertexjob.data = "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                    vertexjob.childNodes = new List<VertexDTO>();
                    vertexjobsuggestion.childNodes.Add(vertexjob);
                }




                for (int k = 0; k < vertexdto1.childNodes.Count; k++)
                {
                    int radiuspart = nodedistance;
                    bool noclash=true;
                    do
                    {
                        noclash = true;
                        radius[k] = radiuspart;
                        vertexdto1.childNodes[k].currentY = Convert.ToInt32(-1 * Math.Cos(Math.PI * shareangle / 360.0 * k) * radiuspart);
                        vertexdto1.childNodes[k].currentX = Convert.ToInt32(Math.Sin(Math.PI * shareangle / 360.0 * k) * radiuspart); 
                        for (int j = k - 1; j >= 0; j--)
                        {
                            double distance= Math.Sqrt((vertexdto1.childNodes[k].currentX-vertexdto1.childNodes[j].currentX)*(vertexdto1.childNodes[k].currentX-vertexdto1.childNodes[j].currentX)+
                                (vertexdto1.childNodes[k].currentY-vertexdto1.childNodes[j].currentY)*(vertexdto1.childNodes[k].currentY-vertexdto1.childNodes[j].currentY));

                            if (distance < lengths[k] + lengths[j])
                            {
                                noclash = false; break;
                            }                            
                        }
                        if (!noclash){
                            radiuspart+=50;
                        }
                    } while (!noclash);


                    //int radiuspart = Convert.ToInt32(lengths[k] / (Math.Sin(Math.PI * shareangle / 360.0)));

                    //vertexdto1.childNodes[k].currentY = Convert.ToInt32(-1 * Math.Cos(Math.PI * shareangle / 360.0 * k) * radiuspart);
                    //vertexdto1.childNodes[k].currentX = Convert.ToInt32(Math.Sin(Math.PI * shareangle / 360.0 * k) * radiuspart); 

                    /*
                    vertexdto1.childNodes[k].currentX = -totallength / 2;
                    if (k > 0) vertexdto1.childNodes[k].currentX = vertexdto1.childNodes[k - 1].currentX + lengths[k - 1] / 2 + lengths[k]/2;*/

                }
            }
           

            


            //alldvertexdtos.Clear();
            MAPJSNode mapjsnode = new MAPJSNode();
            mapjsnode.id = 1;
            mapjsnode.title = "Map";
            mapjsnode.formatVersion = 2;
            mapjsnode.ideas = ConverttoMAPJSNodes(toreturndto);




            ZoneDTO zone = new ZoneDTO();
            zone.MapID = 0;
            zone.MapName = "Test";
            zone.Data = MakeJSonMapJSCompatible(Newtonsoft.Json.JsonConvert.SerializeObject(mapjsnode));

            result.Add(zone);
            return result;
        }


        //TEMP

        private Dictionary<string, MAPJSNode.MAPJSIdea> ConverttoMAPJSNodes(List<VertexDTO> nodes)
        {
            Dictionary<string, MAPJSNode.MAPJSIdea> result = new Dictionary<string, MAPJSNode.MAPJSIdea>();
            foreach (VertexDTO vertex in nodes)
            {
                MAPJSNode.MAPJSIdea mapjsnode = new MAPJSNode.MAPJSIdea();
                if (vertex == null || vertex.rid == null )
                {
                }
                else
                {
                    mapjsnode.id = GetIDfromRID(vertex.rid);
                    mapjsnode.title = vertex.name;
                    mapjsnode.attr = new Attrs() { collapsed = false, style = new Attrs.Style() { background = "#ffffff" } };
                    mapjsnode.rid = vertex.rid;
                    mapjsnode.level = vertex.level;
                    mapjsnode.mapid = vertex.mapid;
                    mapjsnode.currentX = vertex.currentX;
                    mapjsnode.currentY = vertex.currentY;
                    mapjsnode.hasCustomedLocation = vertex.hasCustomedLocation;
                    mapjsnode.data = vertex.data;
                    if (mapjsnode.data == "")
                    {
                        mapjsnode.data = "{\"type\":\"rec\",\"imgsrc\":\"\"}";
                    }
                    if (vertex.childNodes != null)
                    {
                        mapjsnode.ideas = ConverttoMAPJSNodes(vertex.childNodes);
                    }
                    result.Add(GetIDfromRID(vertex.rid).ToString(), mapjsnode);
                }
            }
            return result;
        }

        private string MakeJSonMapJSCompatible(string data)
        {
            return data.Replace("\"ideas\":{},", "").Replace("\"ideas\":{}", "");//.Replace("[", "{").Replace("]", "}");
            //return data;
        }
        private int GetIDfromRID(string rid)
        {
            if (rid.IndexOf(":") > 0)
            {
                return Convert.ToInt32(rid.Substring(rid.IndexOf(":") + 1));
            }
            else return 0;
        }


    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ninject;
using Base;
using Base.Model.DTO;
using Base.ViewModel;
using MemeMVC.Model.DTO;
using MemeMVC.Service;
using MemeMVC.ViewModel;
using System.Web.Security;
using Base.Service;
using System.Web;
using System.Web.Helpers;
using System.Configuration;
using System.IO;
using TweetSharp;
using Hammock.Authentication.OAuth;
using Hammock;
using Hammock.Web;
using Facebook;
using System.Threading;
using Newtonsoft.Json;
using MemeMVC.Repository;
using MemeMVC.Model.DTO.Mongo;
namespace MemeMVC.Controllers
{
    public class UserController : BaseController
    {
        IUserService _userService;
        IUserInstanceService _userInstanceService;
        IProjectService _projectService;
        IEmailTemplateService _emailtemplateService;
        IMapService _mapService;
        IUserMapSharedRepository _userMapSharedRepository;
        public UserController()
        {
        }
        [Inject]
        public UserController(IUserService userService, IUserInstanceService userInstanceService, IProjectService projectService, IEmailTemplateService emailtemplateService, IMapService mapService, IUserMapSharedRepository userMapSharedRepository)
        {
            _userService = userService;
            _userInstanceService = userInstanceService;
            _projectService = projectService;
            _emailtemplateService = emailtemplateService;
            _mapService = mapService;
            _userMapSharedRepository = userMapSharedRepository;
        }
        #region Normal Action
        public ActionResult Admin()
        {
            return View();
        }


        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            List<AddedParam> param;
            HttpStatusCode hCode;
            
            #region Login by form
            if (SessionUtils.CurrentUser != null)
            {
                #region User already login

                param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "UserID", Value = SessionUtils.CurrentUser.UserID });
                UserDTO user = _userService.Get(param, out hCode);

                #region you should logoff before relogin
                if (user != null && user.ValidKey == SessionUtils.CurrentUser.ValidKey)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, false);
                    return RedirectToAction("Index", "Home");
                }
                #endregion
                #region user information had been updated you should signout imediately
                else
                {
                    FormsAuthentication.SignOut();
                    SessionUtils.CurrentUser = null;
                }
                #endregion
                #endregion
            }
            //else
            //{
            //    #region Logoff if user already has cookie before
            //    if (Request.Cookies["userCookie"] != null)
            //    {
            //        return RedirectToAction("LogOff", "User"); //comment out
            //    }
            //    #endregion

            //}
            #endregion
            LoginDTO mv = new LoginDTO();
            mv.ReturnURL = Request.QueryString["returnUrl"];
            return View(mv);
        }
            //        #region Login with facebook only occurred when user click on Facebook Login button

            //if (Session["FaceBookAccessToken"] != null && Session["FaceBookUID"] != null)
            //{
            //    List<AddedParam> addParams = new List<AddedParam>();
            //    addParams.Add(new AddedParam { Key = "FacebookID", Value = Session["FaceBookUID"].ToString() });
            //    IEnumerable<UserDTO> listUser = _userService.GetMany(addParams, out hCode);

            //    if (listUser.Count() > 0)
            //    {
            //        #region Login with already existed account

            //        UserDTO user = listUser.First();
            //        #region Update facebook token when acess token is null.
            //        if (string.IsNullOrEmpty(user.FacebookAccessToken))
            //        {
            //            addParams = new List<AddedParam>();
            //            addParams.Add(new AddedParam { Key = "UserID", Value = user.UserID });
            //            addParams.Add(new AddedParam { Key = "FacebookAccessToken", Value = Session["FaceBookAccessToken"] });
            //            _userService.Update(addParams, out hCode);
            //            user = _userService.Get(user.UserID, out hCode);
            //        }
            //        #endregion

            //        Session["FaceBookUID"] = null;
            //        SessionUtils.CurrentUser = user;
            //        FormsAuthentication.SetAuthCookie(user.Email, false);

            //        HttpCookie myCookie = new HttpCookie("userCookie");
            //        myCookie["Email"] = user.Email;
            //        myCookie.Expires = DateTime.Now.AddMinutes(30);
            //        Response.Cookies.Add(myCookie);
            //        string returnUrl = Request.QueryString["returnUrl"];
            //        if (Url.IsLocalUrl(returnUrl))
            //        {
            //            return Redirect(returnUrl);
            //        }
            //        else
            //        {
            //            return RedirectToAction("Index", "Home");
            //        }
            //        #endregion
            //    }
            //    else //create new account in gridicate system
            //    {
            //        #region Get facebook user and login with it
            //        UserDTO user = new UserDTO();
            //        var client = new FacebookClient(Session["FaceBookAccessToken"].ToString());
            //        dynamic fbresult = client.Get("me?fields=id,email,first_name,last_name,gender,locale,link,username,timezone,location,picture");
            //        FacebookUserModel facebookUser = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookUserModel>(fbresult.ToString());

            //        if (facebookUser != null)
            //        {
            //            param = new List<AddedParam>();
            //            param.Add(new AddedParam { Key = "Email", Value = facebookUser.email });
            //            user = _userService.Get(param, out hCode);
            //            //exist facebook user email login with this user 
            //            if (user != null)
            //            {
            //                #region Login with facebook account
            //                user.FacebookAccessToken = Session["FaceBookAccessToken"].ToString();
            //                Session["FaceBookAccessToken"] = null;
            //                Session["FaceBookUID"] = null;
            //                SessionUtils.CurrentUser = user;
            //                FormsAuthentication.SetAuthCookie(user.Email, false);

            //                HttpCookie myCookie = new HttpCookie("userCookie");
            //                myCookie["Email"] = user.Email;
            //                myCookie.Expires = DateTime.Now.AddMinutes(30);
            //                Response.Cookies.Add(myCookie);
            //                string returnUrl = Request.QueryString["returnUrl"];
            //                if (Url.IsLocalUrl(returnUrl))
            //                {
            //                    return Redirect(returnUrl);
            //                }
            //                else
            //                {
            //                    return RedirectToAction("Index", "Home");
            //                }
            //                #endregion
            //            }
            //            else
            //            {
            //                #region automatic create new user account with facebook user infomattion
            //                user = new UserDTO();
            //                string password = CommonService.GetRandomPassword(10);
            //                user.FacebookID = facebookUser.id;
            //                user.Email = facebookUser.email;
            //                user.FirstName = facebookUser.first_name;
            //                user.LastName = facebookUser.last_name;
            //                user.UserName = facebookUser.username;
            //                user.ProfilePicture = facebookUser.picture.data.url;
            //                user.Active = true;
            //                user.Salt = CommonService.GetRandomPassword(32);
            //                user.Password = CommonService.CreatePasswordHash(password, user.Salt);
            //                user = _userService.Create(user, out hCode);
            //                user.FacebookAccessToken = Session["FaceBookAccessToken"].ToString();
            //                Session["FaceBookAccessToken"] = null;
            //                Session["FaceBookUID"] = null;
            //                if (hCode == HttpStatusCode.RequestTimeout || hCode == HttpStatusCode.InternalServerError)
            //                {
            //                    CommonService.HasError = true;
            //                    return RedirectToAction("Error", "Home");
            //                }

            //                #region send email when register
            //                if (hCode == HttpStatusCode.Created)
            //                {
            //                    Guid mailKey = Guid.NewGuid();
            //                    string key = string.Format("{0}key={1}", user.Email, mailKey);
            //                    //string link = string.Format("http://" + Request.Url.Authority + "/User/ActiveUser?email={0}&key={1}", mv.obj.Email, mailKey);
            //                    param = new List<AddedParam>();
            //                    param.Add(new AddedParam { Key = "Code", Value = "Register" });
            //                    EmailTemplateDTO emailtemplate = _emailtemplateService.Get(param, out hCode);
            //                    string subject = string.Format(emailtemplate.Subject, user.UserName);
            //                    string body = string.Format(emailtemplate.EmailTemplateContent, user.FirstName + " " + user.LastName, user.UserName, password, user.Email);
            //                    if (_userService.GetSendEmailActive(user.Email, key, out hCode))
            //                    {
            //                        try
            //                        {
            //                            WebMail.SmtpServer = "smtp.gmail.com";
            //                            WebMail.UserName = "gridicate@gmail.com";
            //                            WebMail.Password = "biziwave2012";
            //                            WebMail.EnableSsl = true;
            //                            WebMail.Send(user.Email, subject, body, "gridicate@gmail.com");

            //                            #region Login for the firsttime
            //                            user = _userService.Get(user.UserID, out hCode);
            //                            SessionUtils.CurrentUser = user;
            //                            FormsAuthentication.SetAuthCookie(user.Email, false);

            //                            HttpCookie myCookie = new HttpCookie("userCookie");
            //                            myCookie["Email"] = user.Email;
            //                            myCookie.Expires = DateTime.Now.AddMinutes(30);
            //                            Response.Cookies.Add(myCookie);
            //                            string returnUrl = Request.QueryString["returnUrl"];
            //                            if (Url.IsLocalUrl(returnUrl))
            //                            {
            //                                return Redirect(returnUrl);
            //                            }
            //                            else
            //                            {
            //                                return RedirectToAction("Index", "Home");
            //                            }
            //                            #endregion
            //                        }
            //                        catch
            //                        {
            //                            return RedirectToAction("Error", "Home");
            //                        }
            //                    }
            //                }
            //                #endregion
            //                #endregion
            //            }
            //        }
            //        #endregion
            //    }

            //}
            //#endregion

            //#region Login with twitter only occurred when user click on Twitter Login button
            //if (Session["TwitterUser"] != null)
            //{
            //    TwitterUser twitterUser = Session["TwitterUser"] as TwitterUser;
            //    List<AddedParam> addParams = new List<AddedParam>();
            //    addParams.Add(new AddedParam { Key = "TwitterID", Value = twitterUser.Id.ToString() });
            //    IEnumerable<UserDTO> listUser = _userService.GetMany(addParams, out hCode);
            //    if (listUser.Count() > 0)
            //    {
            //        UserDTO user = listUser.First();
            //        Session["TwitterUser"] = null;
            //        SessionUtils.CurrentUser = user;

            //        #region Update twitter token when acess token is null.
            //        if (string.IsNullOrEmpty(user.TwitterAccessToken))
            //        {
            //            addParams = new List<AddedParam>();
            //            addParams.Add(new AddedParam { Key = "UserID", Value = user.UserID });
            //            addParams.Add(new AddedParam { Key = "TwitterAccessToken", Value = Session["TwitterAccessToken"] });
            //            _userService.Update(addParams, out hCode);
            //            user = _userService.Get(user.UserID, out hCode);
            //        }
            //        #endregion
            //        FormsAuthentication.SetAuthCookie(user.Email, false);

            //        HttpCookie myCookie = new HttpCookie("userCookie");
            //        myCookie["Email"] = user.Email;
            //        myCookie.Expires = DateTime.Now.AddHours(1);
            //        Response.Cookies.Add(myCookie);
            //        string returnUrl = Request.QueryString["returnUrl"];
            //        if (Url.IsLocalUrl(returnUrl))
            //        {
            //            return Redirect(returnUrl);
            //        }
            //        else
            //        {
            //            return RedirectToAction("Index", "Home");
            //        }
            //    }
            //    else
            //    {
            //        return RedirectToAction("Register", "User");
            //    }
            //}
            //#endregion

            //#region Login with LinkedIn only occurred when user click on LinkedIn Login button
            //if (Session["LinkedInUser"] != null)
            //{
            //    string accessToken = Session["accessLinkedInToken"].ToString();
            //    string accessTokenSecret = Session["accessLinkedInTokenSecret"].ToString();
            //    var people = new LinkedInService(accessToken, accessTokenSecret).GetCurrentUser();

            //    List<AddedParam> addParams = new List<AddedParam>();
            //    addParams.Add(new AddedParam { Key = "LinkedInID", Value = people.id.ToString() });
            //    IEnumerable<UserDTO> listUser = _userService.GetMany(addParams, out hCode);
            //    if (listUser.Count() > 0)
            //    {
            //        UserDTO user = listUser.First();
            //        Session["LinkedInUser"] = null;
            //        SessionUtils.CurrentUser = user;
            //        FormsAuthentication.SetAuthCookie(user.Email, false);
            //        HttpCookie myCookie = new HttpCookie("userCookie");
            //        myCookie["Email"] = user.Email;
            //        myCookie.Expires = DateTime.Now.AddHours(1);
            //        Response.Cookies.Add(myCookie);
            //        string returnUrl = Request.QueryString["returnUrl"];
            //        if (Url.IsLocalUrl(returnUrl))
            //        {
            //            return Redirect(returnUrl);
            //        }
            //        else
            //        {
            //            return RedirectToAction("Index", "Home");
            //        }
            //    }
            //    else
            //    {
            //        return RedirectToAction("Register", "User");
            //    }
            //}
            //#endregion
        private void SendEmail(string toemailaddress, string subject, string body){
            WebMail.SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];
            WebMail.UserName = ConfigurationManager.AppSettings["SmtpUserName"];
            WebMail.Password = ConfigurationManager.AppSettings["SmtpPassword"];
            WebMail.EnableSsl = true;
            string fromemail = ConfigurationManager.AppSettings["SMTPEmail"];
            WebMail.Send(toemailaddress, subject, body, fromemail);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            List<AddedParam> param;
            HttpStatusCode hCode;
            LoginDTO mv = new LoginDTO();
            try
            {
                UpdateModel(mv);
                if (mv.IsLoginEmail)
                {
                    #region Login by email
                    if (_userService.ValidateEmail(mv.Email, mv.Password, out hCode))
                    {
                        if (hCode == HttpStatusCode.RequestTimeout || hCode == HttpStatusCode.InternalServerError)
                        {
                            CommonService.HasError = true;
                            CommonService.SystemAnnouces = new Base.ViewModel.SystemAnnouces();
                            CommonService.AddSystemAnnouce("Login Error", "Can't connect API", string.Empty, AnnouceType.Error);
                            return RedirectToAction("Error", "Home");
                        }
                        else
                        {

                            param = new List<AddedParam>();
                            param.Add(new AddedParam { Key = "Email", Value = mv.Email });
                            UserDTO user = _userService.Get(param, out hCode);
                            if (hCode == HttpStatusCode.RequestTimeout || hCode == HttpStatusCode.InternalServerError)
                            {
                                ModelState.AddModelError("", "Can't connect API");
                            }
                            else
                            {
                                if (!user.Active)
                                    ModelState.AddModelError("", "Email " + user.Email + " is inactive.");
                                else
                                {
                                    param = new List<AddedParam>();
                                    param.Add(new AddedParam { Key = "UserID", Value = user.UserID });
                                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                                    if (hCode != HttpStatusCode.NotFound)
                                    {
                                        user.UserInstanceID = userIn.UserInstanceID;
                                    }
                                    
                                    SessionUtils.CurrentUser = user;
                                    FormsAuthentication.SetAuthCookie(mv.Email, mv.RememberMe);

                                    HttpCookie myCookie = new HttpCookie("userCookie");
                                    myCookie["Email"] = user.Email;
                                    myCookie.Expires = DateTime.Now.AddHours(1);
                                    Response.Cookies.Add(myCookie);

                                    if (Url.IsLocalUrl(mv.ReturnURL))
                                    {
                                        return Redirect(mv.ReturnURL);
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", @Base.Resource.Login.Login.EmailorPassword);
                    }
                    #endregion
                }
                else
                {
                    #region Login by system id
                    if (_userService.ValidateUserName(mv.UserName, mv.Password, out hCode))
                    {
                        if (hCode == HttpStatusCode.RequestTimeout || hCode == HttpStatusCode.InternalServerError)
                        {
                            CommonService.HasError = true;
                            CommonService.SystemAnnouces = new Base.ViewModel.SystemAnnouces();
                            CommonService.AddSystemAnnouce("Login Error", "Can't connect API", string.Empty, AnnouceType.Error);
                            return RedirectToAction("Error", "Home");
                        }
                        else
                        {

                            param = new List<AddedParam>();
                            param.Add(new AddedParam { Key = "UserName", Value = mv.UserName });
                            UserDTO user = _userService.Get(param, out hCode);
                            if (hCode == HttpStatusCode.RequestTimeout || hCode == HttpStatusCode.InternalServerError)
                            {
                                ModelState.AddModelError("", "Can't connect API");
                            }
                            else
                            {
                                if (!user.Active)
                                    ModelState.AddModelError("", "System ID " + user.UserName + " is inactive.");
                                else
                                {
                                    param = new List<AddedParam>();
                                    param.Add(new AddedParam { Key = "UserID", Value = user.UserID });
                                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                                    if (hCode != HttpStatusCode.NotFound)
                                    {
                                        user.UserInstanceID = userIn.UserInstanceID;
                                    }

                                    param = new List<AddedParam>();
                                    param.Add(new AddedParam { Key = "UserInstanceID", Value = user.UserInstanceID });
                                    List<ProjectDTO> projects = _projectService.GetMany(param, out hCode).ToList();
                                    if (hCode != HttpStatusCode.NotFound)
                                    {
                                        user.Projects = projects;
                                    }

                                    SessionUtils.CurrentUser = user;
                                    FormsAuthentication.SetAuthCookie(mv.UserName, mv.RememberMe);

                                    HttpCookie myCookie = new HttpCookie("userCookie");
                                    myCookie["UserName"] = user.UserName;
                                    myCookie.Expires = DateTime.Now.AddHours(1);
                                    Response.Cookies.Add(myCookie);

                                    HttpCookie myCookie1 = new HttpCookie("loginEmailCookie");
                                    myCookie1["UserName"] = user.UserName;
                                    myCookie1.Expires = DateTime.Now.AddHours(1);
                                    Response.Cookies.Add(myCookie1);

                                    if (Url.IsLocalUrl(mv.ReturnURL))
                                    {
                                        return Redirect(mv.ReturnURL);
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "System ID or Password provided is incorrect");
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message+" "+ex.StackTrace+ " ");
            }
            return View(mv);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            return RedirectToAction("LogOff", "User");
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            if (Request.Cookies["userCookie"] != null)
            {
                if (Request.Cookies["userCookie"]["Email"] != null)
                    ViewBag.Email = Request.Cookies["userCookie"]["Email"];
                else
                    ViewBag.UserName = Request.Cookies["userCookie"]["UserName"];
                return View();
            }
            HttpCookie cookie = new HttpCookie("userCookie", "Email");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);
            SessionUtils.CurrentUser = null;
            return RedirectToAction("Login", "User");
        }

        [AllowAnonymous]
        public ActionResult TotalLogOff()
        {
            SessionUtils.CurrentUser = null;
            FormsAuthentication.SignOut();
            HttpCookie cookie = new HttpCookie("userCookie", "Email");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            HttpCookie myCookie1 = new HttpCookie("loginEmailCookie");
            myCookie1.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(myCookie1);
            return RedirectToAction("Login", "User");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult LogOff(FormCollection form)
        {
            SessionUtils.CurrentUser = null;
            FormsAuthentication.SignOut();
            HttpCookie cookie = new HttpCookie("userCookie", "Email");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            HttpCookie myCookie1 = new HttpCookie("loginEmailCookie");
            myCookie1.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(myCookie1);

            return RedirectToAction("Login", "User");
        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterDTO mv = new RegisterDTO();
            return View(mv);
        }
        [HttpGet]
        public ActionResult GetFeedbackForm()
        {
            UserFeedbackDTO mv = new UserFeedbackDTO();
            mv.UserName = Base.SessionUtils.CurrentUser.UserName;
            mv.Email = Base.SessionUtils.CurrentUser.Email;
            mv.FullName = Base.SessionUtils.CurrentUser.FirstName + " " + Base.SessionUtils.CurrentUser.LastName;

            //return View("~/Views/Shared/User/FeedBackForm.cshtml", mv);
            //return View(mv);
            return PartialView("FeedBackForm", mv);
        }
        [HttpPost]
        public ActionResult ReceiveFeedback(UserFeedbackDTO feedback)
        {
            return Json(true);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(FormCollection collection)
        {
            HttpStatusCode hCode;
            RegisterDTO mv = new RegisterDTO();
            UserDTO user = new UserDTO();
            try
            {
                // TODO: Add insert logic here
                UpdateModel(mv);
                user.FirstName = mv.FirstName;
                user.LastName = mv.LastName;
                user.UserName = mv.UserName;
                user.Email = mv.Email;
                user.Password = mv.Password;
                user.Active = true;
                user.Salt = CommonService.GetRandomPassword(32);
                string pW = mv.Password;
                user.Password = CommonService.CreatePasswordHash(user.Password, user.Salt);
                user = _userService.Create(user, out hCode);
                #region send email when register to user for active user
                List<AddedParam> param = new List<AddedParam>();
                param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "Code", Value = "Register" });
                EmailTemplateDTO emailtemplate = _emailtemplateService.Get(param, out hCode);
                string subject = emailtemplate.Subject;
                string body = string.Format(emailtemplate.EmailTemplateContent, user.FirstName + " " + user.LastName, user.UserName, user.Email,pW );
                SendEmail(user.Email, subject, body); 
                #endregion
                return Redirect(Url.Action("RegisterSuccess", "User"));
              
            }
            catch (Exception ex)
            {
                return View(mv);
            }
        }

        [AllowAnonymous]
        public ActionResult RegisterSuccess()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            ChangePasswordDTO mv = new ChangePasswordDTO();
            return View(mv);
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection form)
        {
            ChangePasswordDTO mv = new ChangePasswordDTO();
            HttpStatusCode hCode;
            try
            {
                UpdateModel(mv);
                if (_userService.ValidateUserName(SessionUtils.CurrentUser.UserName, mv.OldPassword,out hCode) && !string.IsNullOrEmpty(mv.NewPassword))
                {
                    mv.NewSalt = CommonService.GetRandomPassword(32);
                    mv.NewPassword = CommonService.CreatePasswordHash(mv.NewPassword, mv.NewSalt);
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = SessionUtils.CurrentUser.UserID });
                    param.Add(new AddedParam { Key = "Password", Value = mv.NewPassword });
                    param.Add(new AddedParam { Key = "Salt", Value = mv.NewSalt });
                    param.Add(new AddedParam { Key = "ValidKey", Value = Guid.NewGuid().ToString() });
                    var user = _userService.Update(param, out hCode);
                    if (user != null)
                    {
                        FormsAuthentication.SignOut();
                        SessionUtils.CurrentUser = null;
                        return RedirectToAction("Login", "User");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please contact to administrator to continue this action.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Your current password is incorrect.");
                }
            }
            catch (Exception)
            {
            }
            return View(mv);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ForgotPasswordDTO mv = new ForgotPasswordDTO();
            return View(mv);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(FormCollection collection)
        {
            HttpStatusCode hCode;
            ForgotPasswordDTO mv = new ForgotPasswordDTO();
            UserDTO user = new UserDTO();
            List<AddedParam> param = new List<AddedParam>();
            try
            {
                UpdateModel(mv);
                string newSalt = CommonService.GetRandomPassword(32);
                string newPassword = CommonService.GetRandomPassword(10);
                string newPasswordHash = CommonService.CreatePasswordHash(newPassword, newSalt);
                Guid mailKey = Guid.NewGuid();
                string key = string.Format("{0}key={1}", mv.Email, mailKey);
                #region Get User By Email and Update newPassword and newSalt
                param.Add(new AddedParam { Key = "Email", Value = mv.Email });
                user = _userService.Get(param, out hCode);
                if(user==null)
                {
                    ModelState.AddModelError("", "Your email is invalid in system. Please enter the correct email!");
                    return View(mv);
                }
                #endregion
                #region Update User newPassword newSalt, and actionkey
                param = new List<AddedParam>();
                param.Add(new AddedParam { Key="UserID",Value=user.UserID});
                param.Add(new AddedParam { Key = "NewPassword", Value = newPasswordHash });
                param.Add(new AddedParam { Key = "NewSalt", Value = newSalt });
                param.Add(new AddedParam { Key = "ActionKey", Value = key });
                
                user = _userService.Update(param, out hCode);
                if (user != null) {
                    ModelState.AddModelError("", "Please contact to administrator to continue this action.");
                    return View(mv);
                }
                #region Send a ConfirmPassword change email
                    string link = string.Format("http://" + Request.Url.Authority + Url.Action("ConfirmPassword", "User") + "?email={0}&key={1}", mv.Email, mailKey);
                    //string subjecttemplate = emailTemplateNames;
                    //string subject = subjecttemplate;
                    //string bodytemplate = "Dear "+ user.FirstName + " " + user.LastName + ", <br/>";
                    //bodytemplate += "Your password will be reset to <b>\"" + newPassword + "</b><br/>";
                    //bodytemplate += "Please follow the link below to active your passwor:"+ "<br/>";
                    //bodytemplate += "<a href=\""+link+"\">Activate your password has been changed</a><br/>";
                   
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "Code", Value = "ForgotPassword" });
                    EmailTemplateDTO emailtemplate = _emailtemplateService.Get(param, out hCode);
                    string subject = emailtemplate.Subject;
                    string body = string.Format(emailtemplate.EmailTemplateContent, user.FirstName + " " + user.LastName, newPassword, link);
                    SendEmail(mv.Email, subject, body);
                    return Redirect(Url.Action("ConfirmEmail", "User"));
                    #endregion
                #endregion
            }
            catch
            {
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult ConfirmPassword()
        {
            HttpStatusCode hCode;
            string email = Request.QueryString["email"];
            string key = Request.QueryString["key"];
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(key)) {
                ViewBag.Message = "Link not found";
                return View();
            }

            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Email", Value = email });
            UserDTO user = _userService.Get(param, out hCode);
            string actionKey = user.Email.ToLower() + "key=" + key.ToLower();
            if (!string.IsNullOrEmpty(user.NewPassword) && user.ActionKey == actionKey)
            {
                user.Salt = user.NewSalt;
                user.Password = user.NewPassword;
                user.NewSalt = string.Empty;
                user.NewPassword = string.Empty;
                param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "Salt", Value = user.Salt });
                param.Add(new AddedParam { Key = "NewSalt", Value = user.NewSalt });
                param.Add(new AddedParam { Key = "Password", Value = user.Password });
                param.Add(new AddedParam { Key = "NewPassword", Value = user.NewPassword });
                param.Add(new AddedParam { Key = "UserID", Value = user.UserID });
                _userService.Update(param, out hCode);
                ViewBag.Message = "Your password have been changed.";
            }
            else ViewBag.Message = "Link not found";
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmEmail()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ActiveUser()
        {
            HttpStatusCode hCode;
            string email = Request.QueryString["email"];
            string key = Request.QueryString["key"];

            if (_userService.GetActiveUser(email, key, out hCode))
                ViewBag.Message = "User is now activated.";
            else
                ViewBag.Message = "Link not found";

            return View();
        }

        // GET: /User/Signup
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            UserDTO mv = new UserDTO();
            return View(mv);
        }

        #endregion
        public ActionResult Detail(int id)
        {
            ViewBag.UserID = id;
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.UserID = id;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Profile()
        {
            HttpStatusCode hCode;
            ViewBag.ErrorMessage = string.Empty;
            if (SessionUtils.CurrentUser == null)
            {
                ViewBag.ErrorMessage = "Please login to continue this action";
            }
            else {
                ViewBag.UserID = SessionUtils.CurrentUser.UserID;
                ViewBag.NumOfMap = _mapService.GetNumOfMap(SessionUtils.CurrentUser.UserInstanceID,out  hCode);
                ViewBag.NumOfProject = _projectService.GetNumOfProject(SessionUtils.CurrentUser.UserInstanceID, out  hCode);
            }
            return View();
        }
        #region Ajax Action
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Get(int? id)
        {
            HttpStatusCode hCode;
            UserViewModel mv = new UserViewModel();
            if (id == null || id == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                mv.User = new UserDTO();
                return Json(mv);
            }
            else
            {
                mv.User = _userService.Get(id.Value, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }

            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Gets(string sortedfield, bool? sorteddirection)
        {
            HttpStatusCode hCode;
            UsersViewModel mv = new UsersViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            mv.Users = _userService.GetMany(param, out hCode).ToList();
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            foreach (var item in mv.Users)
            {
                item.GUID = Guid.NewGuid().ToString();
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPage(int? pageindex, int? pagesize, string sortedfield, bool? sorteddirection)
        {
            HttpStatusCode hCode;
            UsersViewModel mv = new UsersViewModel();
            List<AddedParam> param = new List<AddedParam>();
            if (pageindex.HasValue)
            {
                param.Add(new AddedParam { Key = "PageIndex", Value = pageindex.Value });
            }
            if (pagesize.HasValue)
            {
                param.Add(new AddedParam { Key = "PageSize", Value = pagesize.Value });
            }
            if (!string.IsNullOrWhiteSpace(sortedfield))
            {
                param.Add(new AddedParam { Key = "SortedField", Value = sortedfield });
                param.Add(new AddedParam { Key = "SortedDirection", Value = sorteddirection ?? false });
            }
            ListDTOModel<UserDTO> rList = _userService.GetPage(param, out hCode);
            mv.PageIndex = pageindex;
            mv.PageSize = pagesize;
            mv.TotalCount = rList.TotalCount;
            mv.Users = rList.Source;
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetGUIDString()
        {
            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Save(UserDTO obj)
        {
            HttpStatusCode hCode;
            UserViewModel mv = new UserViewModel();
            mv.UserID = obj.UserID;
            if (obj.UserID == 0)
            {
                mv.GUID = obj.GUID;
                mv.User = obj;
                mv.User.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                mv.User = _userService.Create(mv.User, out hCode);
                if (hCode != HttpStatusCode.Created)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            else
            {
                mv.User = obj;
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "UserID", Value = obj.UserID });
                param.Add(new AddedParam { Key = "FirstName", Value = obj.FirstName });
                param.Add(new AddedParam { Key = "LastName", Value = obj.LastName });
                param.Add(new AddedParam { Key = "UserName", Value = obj.UserName });
                param.Add(new AddedParam { Key = "Salt", Value = obj.Salt });
                param.Add(new AddedParam { Key = "NewSalt", Value = obj.NewSalt });
                param.Add(new AddedParam { Key = "Password", Value = obj.Password });
                param.Add(new AddedParam { Key = "NewPassword", Value = obj.NewPassword });
                param.Add(new AddedParam { Key = "Email", Value = obj.Email });
                param.Add(new AddedParam { Key = "DateOfBirth", Value = obj.DateOfBirth });
                param.Add(new AddedParam { Key = "ZipCode", Value = obj.ZipCode });
                param.Add(new AddedParam { Key = "Address", Value = obj.Address });
                param.Add(new AddedParam { Key = "Description", Value = obj.Description });
                param.Add(new AddedParam { Key = "Lock", Value = obj.Lock });
                param.Add(new AddedParam { Key = "ActionKey", Value = obj.ActionKey });
                param.Add(new AddedParam { Key = "ProfilePicture", Value = obj.ProfilePicture });
                param.Add(new AddedParam { Key = "GalleryURL", Value = obj.GalleryURL });
                param.Add(new AddedParam { Key = "Rating", Value = obj.Rating });
                param.Add(new AddedParam { Key = "ValidKey", Value = obj.ValidKey });
                param.Add(new AddedParam { Key = "LastLoginDate", Value = obj.LastLoginDate });
                param.Add(new AddedParam { Key = "FacebookID", Value = obj.FacebookID });
                param.Add(new AddedParam { Key = "FacebookEmail", Value = obj.FacebookEmail });
                param.Add(new AddedParam { Key = "FacebookAccessToken", Value = obj.FacebookAccessToken });
                param.Add(new AddedParam { Key = "TwitterID", Value = obj.TwitterID });
                param.Add(new AddedParam { Key = "TwitterAccessToken", Value = obj.TwitterAccessToken });
                param.Add(new AddedParam { Key = "LinkedInID", Value = obj.LinkedInID });
                param.Add(new AddedParam { Key = "LinkedInAccessToken", Value = obj.LinkedInAccessToken });
                param.Add(new AddedParam { Key = "GoogleID", Value = obj.GoogleID });
                param.Add(new AddedParam { Key = "GoogleAccessToken", Value = obj.GoogleAccessToken });
                param.Add(new AddedParam { Key = "City", Value = obj.City });
                param.Add(new AddedParam { Key = "Country", Value = obj.Country });
                mv.User = _userService.Update(param, out hCode);
                if (hCode != HttpStatusCode.OK)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Remove(int id)
        {
            HttpStatusCode hCode;
            UserViewModel mv = new UserViewModel();
            mv.UserID = id;
            _userService.Delete(id, out hCode);
            if (hCode != HttpStatusCode.OK)
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SaveList(List<UserDTO> obj)
        {
            HttpStatusCode hCode;
            UsersViewModel mv = new UsersViewModel();
            if (obj == null || obj.Count == 0)
            {
                mv.Result = Base.ViewModel.ResultType.Success;
                return Json(mv);
            }
            try
            {
                foreach (var item in obj)
                {
                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;
                }
                mv.Users = _userService.Update(obj, out hCode);
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
                return Json(mv);
            }
            mv.Result = Base.ViewModel.ResultType.Success;
            return Json(mv);
        }
        #endregion

        #region For Networks
        [HttpPost]
        [AllowAnonymous]
        public JsonResult FacebookLogin(FacebookLoginModel model)
        {
            Session["FaceBookUID"] = model.uid;
            Session["FaceBookAccessToken"] = model.accessToken;
            return Json(new { success = true });
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult FacebookRegister(FacebookLoginModel model)
        {
            List<AddedParam> param = new List<AddedParam>();
            HttpStatusCode hCode;
            UserViewModel mv = new UserViewModel();
            var client = new FacebookClient(model.accessToken);
            dynamic me = client.Get("me");
            mv.User.FirstName = me.first_name;
            mv.User.LastName = me.last_name;
            mv.User.Email = me.email;
            mv.User.UserName = me.email;
            mv.User.FacebookID = model.uid;
            mv.User.FacebookAccessToken = model.accessToken;
            mv.User.Password = CommonService.GetRandomPassword(6);
            mv.User.Active = true;
            mv.User.Salt = CommonService.GetRandomPassword(32);
            string pW = mv.User.Password;
            mv.User.Password = CommonService.CreatePasswordHash(mv.User.Password, mv.User.Salt);
            #region Get user by email and update facebookid if user already existed
            param.Add(new AddedParam{Key="Email", Value=mv.User.Email});
            var useremail = _userService.Get(param,out hCode);
            if(useremail!=null){
                if (useremail.FacebookID != mv.User.FacebookID)
                {
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = useremail.UserID });
                    param.Add(new AddedParam { Key = "FacebookID", Value = mv.User.FacebookID });
                    param.Add(new AddedParam { Key = "FacebookAccessToken", Value = mv.User.FacebookAccessToken });
                    useremail = _userService.Update(param, out hCode);
                    if (useremail == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                  
                }
                //update user session
                mv.User = useremail;
                param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                if (userIn == null)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
                mv.User.UserInstanceID = userIn.UserInstanceID;
                SessionUtils.CurrentUser = mv.User;
                mv.Result = Base.ViewModel.ResultType.Success;
            }
            #endregion
            #region Get user by facebookid and update token if user already existed
            else{
                param = new List<AddedParam>();
                param.Add(new AddedParam{Key="FacebookID", Value=mv.User.FacebookID});
                var userefacebook = _userService.Get(param,out hCode);
                if(userefacebook!=null){
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = userefacebook.UserID });
                    param.Add(new AddedParam{Key="FacebookAccessToken", Value = mv.User.FacebookAccessToken});
                    userefacebook = _userService.Update(param, out hCode);
                    if (userefacebook == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    //update user session
                    mv.User = userefacebook;
                    param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                    mv.User.UserInstanceID = userIn.UserInstanceID;
                    SessionUtils.CurrentUser = mv.User;
                    mv.Result = Base.ViewModel.ResultType.Success;
                }else{
                    #region Register user with facebook information
                    mv.User = _userService.Create(mv.User, out hCode);
                    if (mv.User == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    //update user session
                    param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                    if (userIn == null) {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    mv.User.UserInstanceID = userIn.UserInstanceID;
                    SessionUtils.CurrentUser = mv.User;
                    #region send email when register to user for active user
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "Code", Value = "Register" });
                    EmailTemplateDTO emailtemplate = _emailtemplateService.Get(param, out hCode);
                    string subject = emailtemplate.Subject;
                    string body = string.Format(emailtemplate.EmailTemplateContent, mv.User.FirstName + " " + mv.User.LastName, mv.User.UserName, mv.User.Email, pW);
                    SendEmail(mv.User.Email, subject, body);
                    #endregion
                    mv.Result = Base.ViewModel.ResultType.Success;
                    #endregion
                }
            }
            #endregion
            return Json(mv);
        }

       
        static string token_secret = "";
        [AllowAnonymous]
        public ActionResult LinkedInAuthorize()
        {
           
                var credentials = new OAuthCredentials
                {
                    CallbackUrl = ConfigurationManager.AppSettings["LinkedInCallbackUrl"],
                    ConsumerKey = ConfigurationManager.AppSettings["LinkedInConsumerKey"],
                    ConsumerSecret = ConfigurationManager.AppSettings["LinkedInConsumerSecret"],
                    Verifier = "123456",
                    Type = OAuthType.RequestToken
                };
                var client = new RestClient { Authority = "https://api.linkedin.com/uas/oauth", Credentials = credentials };

                var request = new RestRequest
                {
                    Path = "requestToken"
                };
                request.AddHeader("scope", Url.Encode("r_fullprofile+rw_nus%2Br_network%2Br_emailaddress"));

                RestResponse response = client.Request(request);
                string token = response.Content.Split('&').Where(s => s.StartsWith("oauth_token=")).Single().Split('=')[1];
                token_secret = response.Content.Split('&').Where(s => s.StartsWith("oauth_token_secret=")).Single().Split('=')[1];
                Response.Redirect("https://api.linkedin.com/uas/oauth/authorize?oauth_token=" + token);
            return null;
        }

        string token = "";
        string verifier = "";

        [AllowAnonymous]
        public ActionResult Callback()
        {
            token = Request["oauth_token"];
            verifier = Request["oauth_verifier"];
            var credentials = new OAuthCredentials
            {
                ConsumerKey = ConfigurationManager.AppSettings["LinkedInConsumerKey"],
                ConsumerSecret = ConfigurationManager.AppSettings["LinkedInConsumerSecret"],
                Token = token,
                TokenSecret = token_secret,
                Verifier = verifier,
                Type = OAuthType.AccessToken,
                ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader,
                SignatureMethod = OAuthSignatureMethod.HmacSha1,
                Version = "1.0"
            };
            var client = new RestClient { Authority = "https://api.linkedin.com/uas/oauth", Credentials = credentials, Method = WebMethod.Post };
            var request = new RestRequest
            {
                Path = "accessToken"
            };
            if (token == null)
                return RedirectToAction("Login", "User");
            RestResponse response = client.Request(request);
            string content = response.Content;
            Session["accessLinkedInToken"] = response.Content.Split('&').Where(s => s.StartsWith("oauth_token=")).Single().Split('=')[1];
            Session["accessLinkedInTokenSecret"] = response.Content.Split('&').Where(s => s.StartsWith("oauth_token_secret=")).Single().Split('=')[1];
            var _service =new LinkedInService(Session["accessLinkedInToken"].ToString(), Session["accessLinkedInTokenSecret"].ToString());
            var people = _service.GetCurrentUser();

            string email = new LinkedInService(Session["accessLinkedInToken"].ToString(), Session["accessLinkedInTokenSecret"].ToString()).GetEmail();

            Session["LinkedInUser"] = people;
            return RedirectToAction("ViewLinkedIn", "Map");
            /*
            var lipeople = _service.GetPeopleByConnectionL1();
            if (people.id != null)
            {
                Session["LinkedInUserID"] = people.id;
                return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Login", "User");*/
        }

        [AllowAnonymous]
        public ActionResult TwitterAuthorize(string returnUrl)
        {
            Session["MagicGridReturnUrl"] = returnUrl;
            string _consumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
            string _consumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];
            TwitterService service = new TwitterService(_consumerKey, _consumerSecret);
            OAuthRequestToken requestToken = service.GetRequestToken();

            var uri = service.GetAuthorizationUri(requestToken);
            return new RedirectResult(uri.ToString(), false);
        }
        [AllowAnonymous]
        public ActionResult TwitterAuthorizeCallback(string oauth_token, string oauth_verifier)
        {
            if (string.IsNullOrEmpty(oauth_token))
            {
                return RedirectToAction("Index", "Home");
            }

            string _consumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
            string _consumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];

            TwitterService twitterservice = new TwitterService(_consumerKey, _consumerSecret);


            var requestToken = new OAuthRequestToken { Token = oauth_token };

            OAuthAccessToken accessToken = twitterservice.GetAccessToken(requestToken, oauth_verifier);
            Session["TwitterAccessToken"] = accessToken.Token;
            Session["TwitterAccessTokenSecret"] = accessToken.TokenSecret;
            twitterservice.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
            /*TwitterUser twitterUser = twitterservice.VerifyCredentials();
            TwitterStatus info = twitterservice.GetTweet(twitterUser.Id);

            Session["TwitterUser"] = twitterUser;
            string returnUrl = string.Empty;
            if (Session["MagicGridReturnUrl"] != null)
            {
                returnUrl = Session["MagicGridReturnUrl"].ToString();
            }
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }*/
            return RedirectToAction("Index", "Home");
        }
        #endregion

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GoogleRegister(string userid, string email, string firstname, string lastname, string accesstoken)
        {
            /*string[] scopes = new string[] {PlusService.Scope.PlusLogin,
                                         PlusService.Scope.UserinfoEmail,
                                         PlusService.Scope.UserinfoProfile};
            TokenResponse token = new TokenResponse
            {
                AccessToken = accesstoken,
                RefreshToken = string.Empty
            };
            ClientSecrets secret = new ClientSecrets();
            secret.ClientId = ConfigurationManager.AppSettings["GoogleClientID"];
            secret.ClientSecret = ConfigurationManager.AppSettings["GoogleClientSecret"];
            IAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = secret,
                    Scopes = new string[] { PlusService.Scope.PlusLogin }
                });
            
            token = flow.ExchangeCodeForTokenAsync("", token.AccessToken, "postmessage",
                           CancellationToken.None).Result;
            UserCredential credential = new UserCredential(flow, "me", token);
            PlusService plusservice = new PlusService(
                new Google.Apis.Services.BaseClientService.Initializer()
                {
                    ApplicationName = "MemeMap",
                    HttpClientInitializer = credential
                });
            var me = plusservice.People.Get("me").Execute();*/
            UserViewModel mv = new UserViewModel();
            List<AddedParam> param = new List<AddedParam>();
            HttpStatusCode hCode;
            mv.User.FirstName =firstname;
            mv.User.LastName = lastname;
            mv.User.Email = email;
            mv.User.UserName =email;
            mv.User.LinkedInID = userid;
            mv.User.LinkedInAccessToken = accesstoken;
            mv.User.Password = CommonService.GetRandomPassword(6);
            mv.User.Active = true;
            mv.User.Salt = CommonService.GetRandomPassword(32);
            string pW = mv.User.Password;
            mv.User.Password = CommonService.CreatePasswordHash(mv.User.Password, mv.User.Salt);
            #region Get user by email and update facebookid if user already existed
            param.Add(new AddedParam { Key = "Email", Value = mv.User.Email });
            var useremail = _userService.Get(param, out hCode);
            if (useremail != null)
            {
                if (useremail.FacebookID != mv.User.FacebookID)
                {
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = useremail.UserID });
                    param.Add(new AddedParam { Key = "LinkedInID", Value = mv.User.LinkedInID });
                    param.Add(new AddedParam { Key = "LinkedInAccessToken", Value = mv.User.LinkedInAccessToken });
                    useremail = _userService.Update(param, out hCode);
                    if (useremail == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }

                }
                //update user session
                mv.User = useremail;
                param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                if (userIn == null)
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                    return Json(mv);
                }
                mv.User.UserInstanceID = userIn.UserInstanceID;
                SessionUtils.CurrentUser = mv.User;
                mv.Result = Base.ViewModel.ResultType.Success;
            }
            #endregion
            #region Get user by googleid and update token if user already existed
            else
            {
                param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "LinkedInID", Value = mv.User.LinkedInID });
                var userefacebook = _userService.Get(param, out hCode);
                if (userefacebook != null)
                {
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "UserID", Value = userefacebook.UserID });
                    param.Add(new AddedParam { Key = "LinkedInAccessToken", Value = mv.User.LinkedInAccessToken });
                    userefacebook = _userService.Update(param, out hCode);
                    if (userefacebook == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    //update user session
                    mv.User = userefacebook;
                    param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                    mv.User.UserInstanceID = userIn.UserInstanceID;
                    SessionUtils.CurrentUser = mv.User;
                    mv.Result = Base.ViewModel.ResultType.Success;
                }
                else
                {
                    #region Register user with facebook information
                    mv.User = _userService.Create(mv.User, out hCode);
                    if (mv.User == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    //update user session
                    param.Add(new AddedParam { Key = "UserID", Value = mv.User.UserID });
                    UserInstanceDTO userIn = _userInstanceService.Get(param, out hCode);
                    if (userIn == null)
                    {
                        mv.Result = Base.ViewModel.ResultType.Unsuccess;
                        return Json(mv);
                    }
                    mv.User.UserInstanceID = userIn.UserInstanceID;
                    SessionUtils.CurrentUser = mv.User;
                    #region send email when register to user for active user
                    #endregion
                    mv.Result = Base.ViewModel.ResultType.Success;
                    #endregion
                }
            }
            #endregion
            return Json(mv);
        }

        [HttpPost]
        public JsonResult UploadProfilePicture(IEnumerable<HttpPostedFileBase> files, int userid)
        {
            UserViewModel mv = new UserViewModel();
            List<AddedParam> param = new List<AddedParam>();
            HttpStatusCode hCode;
            //Check Exist Directory
            var directorylevel1 = Server.MapPath("~/Uploads/");
            var directory = Server.MapPath("~/Uploads/ProfilePicture/");
            string filename;
            string filePath = "";
            if (!Directory.Exists(directorylevel1))
            {
                Directory.CreateDirectory(directorylevel1);
            }
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            try
            {
                if (files.Count() > 0)
                {
                    foreach (var file in files)
                    {
                        filename = file.FileName.ToString();
                        if (filename.Contains("\\"))
                        {
                            filename = filename.Substring(filename.LastIndexOf("\\") + 1);
                        }
                        string fileextension = filename.Split('.')[1];
                        filePath = Path.Combine(directory, userid + "." + fileextension);
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                        file.SaveAs(filePath);
                        param.Add(new AddedParam { Key = "UserID", Value = userid });
                        param.Add(new AddedParam { Key = "ProfilePicture", Value = "/Uploads/ProfilePicture/" + userid + "." + fileextension });
                        mv.User = _userService.Update(param, out hCode);
                    }
                    mv.Result = Base.ViewModel.ResultType.Success;
                }
                else
                {
                    mv.Result = Base.ViewModel.ResultType.Unsuccess;
                }
            }
            catch
            {
                mv.Result = Base.ViewModel.ResultType.Unsuccess;
            }
            return Json(new { data = mv }, "text/plain");
        }

        [HttpPost]
        public JsonResult SearchUser(string SearchCode)
        {
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "SearchCode", Value = SearchCode });
            HttpStatusCode hCode;
            List<UserDTO> users = _userService.GetMany(param, out hCode).ToList();
            string ret = JsonConvert.SerializeObject(users);
            return Json(ret);
        }
        [HttpPost]
        public JsonResult removeSharedUser(string mongoId)
        {
            var val = _userMapSharedRepository.Delete(mongoId);
            if (val == true)
            {
                return Json(new { statusCode = "200" });
            }
            return Json(new { statusCode = "404" });
        }
        [HttpPost]
        public JsonResult ShareWithUser(int mapId, int userId, string username, string userEmail, string profileImgUrl)
        {
            UserMapShared entity = new Model.DTO.Mongo.UserMapShared(mapId, userId, username, userEmail, profileImgUrl);
            var val = _userMapSharedRepository.SaveOrUpdate(entity);
            if (val == true)
            {
                return Json(new { statusCode = "200", Id = entity.Id.ToString()});
            }
            return Json(new { statusCode = "404" });
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq.Expressions;
using MongoDB.Driver;
using System.Configuration;
using MongoDB.Bson;
using MemeMVC.Model.DTO;
namespace Base.DAL
{
 
    public class MongoConnectionHandler<T> where T : IMongoEntity
    {
        public MongoCollection<T> MongoCollection { get; private set; }

        public MongoConnectionHandler()
        {
            string fullconnectionstring = ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString;
            //string fullconnectionstring = "";
            var arrConfig = fullconnectionstring.Split(';');
            string connectionString = arrConfig[0];
            string database = arrConfig[1];
            string databaseUser = string.Empty;
            string databaseUserPassword = string.Empty;
            if (arrConfig.Length > 2) {
                databaseUser = arrConfig[2];
            }
            if (arrConfig.Length > 3)
            {
                databaseUserPassword = arrConfig[3];
            }
            //const string connectionString = "mongodb://127.0.0.1:27017";

            //// Get a thread-safe client object by using a connection string
            var mongoClient = new MongoClient(connectionString);

            //// Get a reference to a server object from the Mongo client object
            var mongoServer = mongoClient.GetServer();

            //// Get a reference to the "retrogamesweb" database object 
            //// from the Mongo server object
            //const string databaseName = "test";
            var db = mongoServer.GetDatabase(database);

            //// Get a reference to the collection object from the Mongo database object
            //// The collection name is the type converted to lowercase + "s"
            MongoCollection = db.GetCollection<T>(typeof(T).Name.ToLower() + "s");
        }
    }
}

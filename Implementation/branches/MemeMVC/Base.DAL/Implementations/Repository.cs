﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq.Expressions;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using MemeMVC.Model.DTO;


namespace Base.DAL
{
    public abstract class Repository<T> : IRepository<T> where T : IMongoEntity
    {
        protected readonly MongoConnectionHandler<T> MongoConnectionHandler;
        protected Repository()
        {
            MongoConnectionHandler = new MongoConnectionHandler<T>();
        }
        public virtual bool SaveOrUpdate(T entity)
        {
            //// Save the entity with safe mode (WriteConcern.Acknowledged)
            var result = this.MongoConnectionHandler.MongoCollection.Save(
                entity,
                new MongoInsertOptions
                {
                    WriteConcern = WriteConcern.Acknowledged
                });

            if (!result.Ok)
            {
                //// Something went wrong
                return false;
            }
            return true;
        }
        public virtual bool SaveOrUpdate(List<T> entity)
        {
            //// Save the entity with safe mode (WriteConcern.Acknowledged)
            var result = this.MongoConnectionHandler.MongoCollection.Save(
                entity,
                new MongoInsertOptions
                {
                    WriteConcern = WriteConcern.Acknowledged
                });

            if (!result.Ok)
            {
                //// Something went wrong
                return false;
            }
            return true;
        }
        public virtual bool Delete(string id)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Remove(
                Query<T>.EQ(e => e.Id,
                new ObjectId(id)),
                RemoveFlags.None,
                WriteConcern.Acknowledged);

            if (!result.Ok)
            {
                //// Something went wrong
                return false;
            }
            return true;
        }
        public virtual bool Delete(Expression<Func<T, bool>> where)
        {
            var entities = GetAllQueryable().Where(where);
            var rbool = true;
            foreach (var en in entities) {
                var result = this.MongoConnectionHandler.MongoCollection.Remove(
                  Query<T>.EQ(e => e.Id,
                  en.Id),
                  RemoveFlags.None,
                  WriteConcern.Acknowledged);

                if (!result.Ok)
                {
                    // Something went wrong
                    if(rbool) rbool = false;
                }
            }
            return rbool;
        }
        public virtual T GetById(string id)
        {
            var entityQuery = Query<T>.EQ(e => e.Id, new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(entityQuery);
        }
        public T Get(Expression<Func<T, bool>> where)
        {
            return GetAllQueryable().Where(where).FirstOrDefault<T>();
        }
        public virtual IQueryable<T> GetAllQueryable()
        {
            return this.MongoConnectionHandler.MongoCollection.FindAll().AsQueryable<T>();
        }
        public virtual List<T> GetMany(Expression<Func<T, bool>> where)
        {
            return GetAllQueryable().Where(where).ToList();
        }
    }
}

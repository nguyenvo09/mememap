﻿using System;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using MemeMVC.Model.DTO;
using System.Collections.Generic;

namespace Base.DAL
{
    public interface IRepository<T> where T : IMongoEntity
    {
        bool SaveOrUpdate(T entity);
        bool SaveOrUpdate(List<T> entity);
        bool Delete(string id);
        bool Delete(Expression<Func<T, bool>> where);
        T GetById(string id);
        T Get(Expression<Func<T, bool>> where);
        IQueryable<T> GetAllQueryable();
        List<T> GetMany(Expression<Func<T, bool>> where);
    }
}

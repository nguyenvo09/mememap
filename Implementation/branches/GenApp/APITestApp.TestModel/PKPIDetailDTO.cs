﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.TestModel
{
    public partial class PKPIDetailDTO
    {
        public int PKPIDetailID { get; set; }
        public int PKPIRowID { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Quater { get; set; }
        public decimal Value { get; set; }

        public bool Active { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.TestModel
{
    public partial class KFADTO
    {
        public int KFAID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
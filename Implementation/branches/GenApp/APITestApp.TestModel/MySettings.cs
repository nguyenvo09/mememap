﻿using System;
using System.Configuration;
using System.ComponentModel;

namespace APITestApp.TestCase
{
    /// <summary>
    /// This class will configure setting for this application
    /// </summary>
    public class MySettings 
    {
        public MySettings()
        {
        }
        public static string AssemblyName
        {
            get
            {
                return ConfigurationManager.AppSettings["AssemblyName"].ToString();
            }
        }
        public static string MainWin_Title
        {
            get
            {
                return ConfigurationManager.AppSettings["MainWin_Title"].ToString();
            }
        }
        public static int MainWin_MinWidth
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["MainWin_MinWidth"].ToString());
            }
        }
        public static int MainWin_MinHeight
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["MainWin_MinHeight"].ToString());
            }
        }
        public static string BaseURI
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseURI"].ToString();
            }
        }
       
    }
}
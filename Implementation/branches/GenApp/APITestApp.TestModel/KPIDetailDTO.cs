﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.TestModel
{
    public partial class KPIDetailDTO
    {
        public int KPIDetailID { get; set; }
        public int KPIRowID { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Quater { get; set; }
        public decimal Value { get; set; }

        public bool Active { get; set; }
    }
}
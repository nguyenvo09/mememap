﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.TestModel
{
    public partial class GeneralInfoDTO
    {
        public int GeneralInfoID { get; set; }
        public int ProjectID { get; set; }

        public string Code { get; set; }
        public string Content { get; set; }

        public string Type { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.TestModel
{
    public partial class ActionPlanDTO
    {
        public int ActionPlanID { get; set; }
        public int ProjectID { get; set; }

        public string TaskName { get; set; }
        public string Description { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? DueDate { get; set; }

        public string Status { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Xml.Linq;
using System.ComponentModel;
namespace APITestApp.TestModel
{
    public class CustomGridCommand
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public List<AddedParam> AddedParams { get; set; }
        public CustomGridCommand()
        {
            Page = 0;
            PageSize = 20;
            AddedParams = new List<AddedParam>();
        }
        #region Setting method
        public CustomGridCommand SetAddedParam(string key,object value) {
            AddedParam p = new AddedParam();
            p.Key = key;
            p.Value = value;
            this.AddedParams.Add(p);
            return this;
        }
        #endregion
    }
    public class AddedParam {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}

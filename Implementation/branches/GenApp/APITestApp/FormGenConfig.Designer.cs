﻿namespace APITestApp
{
    partial class FormGenConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridGenConfig = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridGenConfig)).BeginInit();
            this.SuspendLayout();
            // 
            // gridGenConfig
            // 
            this.gridGenConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridGenConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGenConfig.Location = new System.Drawing.Point(0, 0);
            this.gridGenConfig.Name = "gridGenConfig";
            this.gridGenConfig.Size = new System.Drawing.Size(899, 671);
            this.gridGenConfig.TabIndex = 0;
            // 
            // FormGenConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 671);
            this.Controls.Add(this.gridGenConfig);
            this.Name = "FormGenConfig";
            this.Text = "Form Gen Config";
            this.Load += new System.EventHandler(this.FormGenConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridGenConfig)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridGenConfig;
    }
}
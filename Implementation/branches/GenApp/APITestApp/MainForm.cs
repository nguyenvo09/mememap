﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using APITestApp.TestCase;
using System.Net;
using APITestApp.Business;
using System.IO;
namespace APITestApp
{
    public partial class MainForm : Form
    {
        GenApp genApp { get; set; }
        bool search = false;
        List<PropertyGenInfo> listSelectedGen { get; set; }
        public MainForm()
        {
            InitializeComponent();
            dataGridTest.AutoGenerateColumns = false;
        }
        #region Event
        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Setting for form
            this.Width = MySettings.MainWin_MinWidth;
            this.Height = MySettings.MainWin_MinHeight;
            this.Text = MySettings.MainWin_Title;
            this.txtAssembly.Text = MySettings.AssemblyName;
            this.txtProjectFolder.Text = MySettings.ProjectFolder;
            this.txtAPIProjectName.Text = MySettings.APIProjectName;
            this.txtMVCProjectName.Text = MySettings.MVCProjectName;
            if (!string.IsNullOrWhiteSpace(this.txtAssembly.Text.Trim()))
            {
                LoadAssembly();
            }
            #endregion
        }

        private void btnRunTest_Click(object sender, EventArgs e)
        {
            if (ExecuteGenCode())
            {
                MessageBox.Show("Success Generation");
            }

        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (search)
            {
                if (chk.Checked)
                {
                    this.dataGridTest.DataSource = genApp.ListProperty;
                    txtSearch.Text = string.Empty;
                }
                else
                {
                    search = true;
                    List<PropertyGenInfo> filter = genApp.ListProperty.Where(c => c.ClassName.ToLower().Equals(txtSearch.Text.ToLower())).ToList();
                    this.dataGridTest.DataSource = filter;
                }
            }

        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            var dialogResult = dialog.ShowDialog();
            #region Mapp properties
            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                this.txtAssembly.Text = dialog.FileName;
                LoadAssembly();
            }
            #endregion
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search = true;
            List<PropertyGenInfo> filter = genApp.ListProperty.Where(c => c.ClassName.ToLower().Equals(txtSearch.Text.ToLower())).ToList();
            listSelectedGen = filter;
            this.genApp.GenToDo = new GenCodeToDo(this.txtSearch.Text, this.txtAPIProjectName.Text, this.txtMVCProjectName.Text.Trim(), this.txtProjectFolder.Text);
            this.dataGridTest.DataSource = filter;

        }

        private void checkedAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            List<PropertyGenInfo> fillter = this.dataGridTest.DataSource as List<PropertyGenInfo>;
            foreach (var r in fillter)
            {
                r.IsGen = chk.Checked;
            }
            this.dataGridTest.Refresh();
        }

        private void btnGenConfig_Click(object sender, EventArgs e)
        {
            genApp.GenToDo = new GenCodeToDo(this.txtSearch.Text.Trim(), this.txtAPIProjectName.Text.Trim(), this.txtMVCProjectName.Text.Trim(), this.txtProjectFolder.Text);
            FormGenConfig form = new FormGenConfig(genApp.GenToDo);
            form.ShowDialog();
        }

        private void btnSelectProjectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.txtProjectFolder.Text = dialog.SelectedPath;
            }
        }
        #endregion
        #region File
        private void LoadAssembly()
        {
            string[] arrAssembly = this.txtAssembly.Text.Split(';');
            List<string> listAssembly = new List<string>();
            for (int i = 0; i < arrAssembly.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(arrAssembly[i])) listAssembly.Add(arrAssembly[i]);
            }
            this.genApp = new GenApp(listAssembly);
            this.dataGridTest.DataSource = genApp.ListProperty;

        }
        private bool CreateFolder(string folderName)
        {
            try
            {
                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool CreateFileName(string folderName, string fileName, List<String> fileContent)
        {
            string fullFileName = Path.Combine(folderName, fileName);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fullFileName))
            {
                foreach (string line in fileContent)
                {
                    file.WriteLine(line);

                }
            }
            return true;
        }
        #endregion
        #region Execute generate Code
        private bool ExecuteGenCode()
        {
            var listTask = this.genApp.GenToDo.listGenInfo.Where(t => t.IsGen == true);
            var listProperty = this.listSelectedGen.Where(p => p.IsGen == true).ToList();
            try
            {
                foreach (var task in listTask)
                {
                    DoGeneration(listProperty, task);
                }
                return true;
            }
            catch
            {
                return false;
            }

        }
        private void DoGeneration(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> listCode = new List<string>();
            switch (genInf.JobName)
            {
                #region API
                case FileGenType.API_DTO:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = API_DTO(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.API_Repository:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = API_Repository(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.API_Logic:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = API_Logic(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.API_Controller:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = API_Controller(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                #endregion
                #region MVC
                case FileGenType.MVC_DTO:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_DTO(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_Service:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_Service(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }

                    break;
                case FileGenType.MVC_ViewModel:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_ViewModel(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_Controller:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_Controller(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleAdminTableHtml:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_AdminTableHtml(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleAdminJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_AdminJScipt(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleAdminList:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_AdminView(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_ModelJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_ModelJScipt(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_ViewModelJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_ViewModelJScipt(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_PageViewModelsJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_PageViewModelJScipt(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_ViewModelsJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_ViewModelsJScipt(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_ArrayPageHtml:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_ArrayPageHtml(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_Admin_Resource:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_AdminResource(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleCreateJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_CreateJScript(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleCreateView:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_CreateView(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleEditJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_EditJScript(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleEditView:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_EditView(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleDetailJScript:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_DetailJScript(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleDetailView:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_DetailView(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;
                case FileGenType.MVC_View_SimpleFormControl:
                    if (CreateFolder(genInf.Folder))
                    {
                        listCode = MVC_FormControl(listProperty, genInf);
                        CreateFileName(genInf.Folder, genInf.FileName, listCode);
                    }
                    break;

                #endregion
            }
        }
        #region API Code
        private List<string> API_DTO(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));
            genContent.Add(string.Format("{0}", "using System;"));

            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", @"{"));
            genContent.Add(string.Format("    public partial class {0}", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            foreach (var p in listProperty)
            {
                genContent.Add(string.Format("        {0} {1} {2} {3}", "public", p.SystemType, p.PropertyName, "{ get; set; }"));

            }
            genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", "EditMode", "{ get; set; }"));
            genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", "GUID", "{ get; set; }"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", p.PropertyName.Replace("ID", "") + "Name", "{ get; set; }"));
            }
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("{0}", "}"));
            return genContent;
        }
        private List<string> API_Repository(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            genContent.Add(string.Format("using {0}.Model;", genInf.ProjectName));
            if (genInf.ProjectName != "BaseAPI")
            {
                genContent.Add(string.Format("{0}", "using BaseAPI.Repository;"));
            }
            genContent.Add(string.Format("{0}", "using BaseAPI.DAL;"));

            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            genContent.Add(string.Format("    public class {0} : Base{1}<{2}>, I{0}", genInf.GenClassName, genInf.ModelExtension, genInf.ModelName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("        public {0}(IDatabaseFactory databaseFactory)", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "            : base(databaseFactory)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            DatabaseFactory = databaseFactory;"));
            genContent.Add(string.Format("{0}", "        }"));
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("    public interface I{0} : I{1}<{2}>", genInf.GenClassName, genInf.ModelExtension, genInf.ModelName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("{0}", "}"));

            return genContent;
        }
        private List<string> API_Logic(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", genInf.ModelName + "ID" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID"))).ToList();
            var keyP = listProperty.Single(p => p.IsPrimaryKey);
            #region Using
            genContent.Add(string.Format("{0}", "using System;"));
            genContent.Add(string.Format("{0}", "using System.Linq;"));
            genContent.Add(string.Format("{0}", "using System.Collections.Generic;"));
            genContent.Add(string.Format("{0}", "using BaseAPI.DAL;"));
            genContent.Add(string.Format("{0}", "using BaseAPI.Model.DTO;"));
            genContent.Add(string.Format("using {0}.Model.DTO;", genInf.ProjectName));
            genContent.Add(string.Format("using {0}.Repository;", genInf.ProjectName));
            genContent.Add(string.Format("using {0}.Model;", genInf.ProjectName));
            #endregion
            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            #region Class Logic
            genContent.Add(string.Format("    public class {0} : I{0}", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("{0}", "        public IUnitOfWork _unitOfWork;"));
            genContent.Add(string.Format("        I{0}Repository _{1}Repository;", genInf.ModelName, genInf.ModelName.ToLower()));
            #region contructor
            genContent.Add(string.Format("       public {0}(IUnitOfWork unitOfWork, I{1}Repository {2}Repository)", genInf.GenClassName, genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "       {"));
            genContent.Add(string.Format("{0}", "           this._unitOfWork = unitOfWork;"));
            genContent.Add(string.Format("           this._{0}Repository = {0}Repository;", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "       }"));
            #endregion
            #region GetMany

            genContent.Add(string.Format("       public IEnumerable<{0}DTO> GetMany(List<AddedParam> addedParams)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "       {"));
            genContent.Add(string.Format("           var query = _{0}Repository.GetAllQueryable();", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "          if (addedParams != null)"));
            genContent.Add(string.Format("{0}", "          {"));
            genContent.Add(string.Format("{0}", "              foreach (AddedParam p in addedParams)"));
            genContent.Add(string.Format("{0}", "              {"));
            genContent.Add(string.Format("{0}", "               }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            //sort param"));
            genContent.Add(string.Format("{0}", "            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == \"SortedField\");"));
            genContent.Add(string.Format("{0}", "            if (sortedfieldparam != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == \"SortedDirection\");"));
            genContent.Add(string.Format("{0}", "                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);"));
            genContent.Add(string.Format("{0}", "                switch (sortedfieldparam.Value.ToString())"));
            genContent.Add(string.Format("{0}", "                {"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("{0}", "                    case \"" + p.PropertyName + "\":"));
                genContent.Add(string.Format("{0}", "                        if (sortdiretion)"));
                genContent.Add(string.Format("{0}", "                        {"));
                genContent.Add(string.Format("{0}", "                            query = query.OrderByDescending(u => u." + p.PropertyName + ");"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                        else"));
                genContent.Add(string.Format("{0}", "                        {"));
                genContent.Add(string.Format("{0}", "                            query = query.OrderBy(u => u." + p.PropertyName + ");"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                        break;"));
            }
            genContent.Add(string.Format("{0}", "                    default:"));
            genContent.Add(string.Format("{0}", "                          query = query.OrderBy(u => u." + keyP.PropertyName + ");"));
            genContent.Add(string.Format("{0}", "                          break;"));
            genContent.Add(string.Format("{0}", "                  }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "               query = query.OrderBy(u => u." + keyP.PropertyName + ");"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            var data = (from u in query"));
            genContent.Add(string.Format("                       select new {0}DTO", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                        {"));
            for (int i = 0; i < listProperty.Count; i++)
            {
                var p = listProperty[i];
                if (i < listProperty.Count - 1)
                {
                    genContent.Add(string.Format("                            {0} = u.{0},", p.PropertyName));
                }
                else
                {
                    if (listFP.Count > 0)
                    {
                        genContent.Add(string.Format("                            {0} = u.{0},", p.PropertyName));
                    }
                    else
                    {
                        genContent.Add(string.Format("                            {0} = u.{0}", p.PropertyName));
                    }
                }
            }
            for (int i = 0; i < listFP.Count; i++)
            {
                var p = listFP[i];
                if (i < listFP.Count() - 1)
                {
                    genContent.Add(string.Format("{0}", "                            " + p.PropertyName.Replace("ID", "") + "Name = u." + p.PropertyName.Replace("ID", "") + "!=null?u." + p.PropertyName.Replace("ID", "") + ".Name:\"\","));
                }
                else
                {
                    genContent.Add(string.Format("{0}", "                            " + p.PropertyName.Replace("ID", "") + "Name = u." + p.PropertyName.Replace("ID", "") + "!=null?u." + p.PropertyName.Replace("ID", "") + ".Name:\"\""));
                }
            }
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "            return data;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetPage
            genContent.Add(string.Format("        public ListDTOModel<{0}DTO> GetPage(List<AddedParam> addedParams)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            ListDTOModel<{0}DTO> rList = new ListDTOModel<{0}DTO>();", genInf.ModelName));
            genContent.Add(string.Format("            var query = _{0}Repository.GetAllQueryable();", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            int pageIndex = 1;"));
            genContent.Add(string.Format("{0}", "            int pageSize = 20;"));
            genContent.Add(string.Format("{0}", "            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == \"PageIndex\");"));
            genContent.Add(string.Format("{0}", "            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == \"PageSize\");"));
            genContent.Add(string.Format("{0}", "            if (pageIndexParam != null&&pageIndexParam.Value!=null) {"));
            genContent.Add(string.Format("{0}", "                pageIndex = Convert.ToInt32(pageIndexParam.Value);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            if (pageSizeParam != null && pageSizeParam.Value != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                pageSize = Convert.ToInt32(pageSizeParam.Value);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            if (addedParams != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                foreach (AddedParam p in addedParams)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    switch (p.Key)"));
            genContent.Add(string.Format("{0}", "                    {"));

            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            //sort param"));
            genContent.Add(string.Format("{0}", "            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == \"SortedField\");"));
            genContent.Add(string.Format("{0}", "            if (sortedfieldparam != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == \"SortedDirection\");"));
            genContent.Add(string.Format("{0}", "                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);"));
            genContent.Add(string.Format("{0}", "                switch (sortedfieldparam.Value.ToString())"));
            genContent.Add(string.Format("{0}", "                {"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("{0}", "                    case \"" + p.PropertyName + "\":"));
                genContent.Add(string.Format("{0}", "                        if (sortdiretion)"));
                genContent.Add(string.Format("{0}", "                        {"));
                genContent.Add(string.Format("{0}", "                            query = query.OrderByDescending(u => u." + p.PropertyName + ");"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                        else"));
                genContent.Add(string.Format("{0}", "                        {"));
                genContent.Add(string.Format("{0}", "                            query = query.OrderBy(u => u." + p.PropertyName + ");"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                        break;"));
            }

            genContent.Add(string.Format("{0}", "                    default:"));
            genContent.Add(string.Format("{0}", "                          query = query.OrderBy(u => u." + keyP.PropertyName + ");"));
            genContent.Add(string.Format("{0}", "                          break;"));
            genContent.Add(string.Format("{0}", "                  }"));

            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "               query = query.OrderBy(u => u." + keyP.PropertyName + ");"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            rList.TotalCount = query.Count();"));
            genContent.Add(string.Format("{0}", "            // paging"));
            genContent.Add(string.Format("{0}", "            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);"));

            genContent.Add(string.Format("{0}", "            var data = (from u in query"));
            genContent.Add(string.Format("                        select new {0}DTO", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                        {"));
            for (int i = 0; i < listProperty.Count; i++)
            {
                var p = listProperty[i];
                if (i < listProperty.Count - 1)
                {
                    genContent.Add(string.Format("                            {0} = u.{0},", p.PropertyName));
                }
                else
                {
                    if (listFP.Count > 0)
                    {
                        genContent.Add(string.Format("                            {0} = u.{0},", p.PropertyName));
                    }
                    else
                    {
                        genContent.Add(string.Format("                            {0} = u.{0}", p.PropertyName));
                    }
                }
            }
            for (int i = 0; i < listFP.Count; i++)
            {
                var p = listFP[i];
                if (i < listFP.Count() - 1)
                {
                    genContent.Add(string.Format("{0}", "                            " + p.PropertyName.Replace("ID", "") + "Name = u." + p.PropertyName.Replace("ID", "") + "!=null?u." + p.PropertyName.Replace("ID", "") + ".Name:\"\","));
                }
                else
                {
                    genContent.Add(string.Format("{0}", "                            " + p.PropertyName.Replace("ID", "") + "Name = u." + p.PropertyName.Replace("ID", "") + "!=null?u." + p.PropertyName.Replace("ID", "") + ".Name:\"\""));
                }
            }
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "            rList.PageIndex = pageIndex;"));
            genContent.Add(string.Format("{0}", "            rList.PageSize = pageSize;"));
            genContent.Add(string.Format("{0}", "            rList.Source = data.ToList();"));
            genContent.Add(string.Format("{0}", "            return rList;"));
            genContent.Add(string.Format("{0}", "        }"));

            #endregion
            #region Get exist
            genContent.Add(string.Format("{0}", "        public bool GetExist(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            var query = _{0}Repository.GetAllQueryable();", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (addedParams != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                foreach (AddedParam p in addedParams)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    switch (p.Key)"));
            genContent.Add(string.Format("{0}", "                    {"));


            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return query.Count() > 0;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Get ID
            genContent.Add(string.Format("        public {0}DTO Get(int id)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            return ConvertToDTO(_{0}Repository.GetById(id));", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Get Param
            genContent.Add(string.Format("        public {0}DTO Get(List<AddedParam> addedParams)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            var query = _{0}Repository.GetAllQueryable();", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (addedParams != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                foreach (AddedParam p in addedParams)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    switch (p.Key)"));
            genContent.Add(string.Format("{0}", "                    {"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("            {0} item = query.FirstOrDefault();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            return ConvertToDTO(item);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Get Scalar
            genContent.Add(string.Format("{0}", "        public Object GetScalar(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            var query = _{0}Repository.GetAllQueryable();", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (addedParams != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                foreach (AddedParam p in addedParams)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    switch (p.Key)"));
            genContent.Add(string.Format("{0}", "                    {"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return query.Count();"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Update Param
            genContent.Add(string.Format("        public {0}DTO Update(List<AddedParam> addParams)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == \"{0}\");", keyP.PropertyName));
            genContent.Add(string.Format("{0}", "            if (addParam == null) return null;"));
            genContent.Add(string.Format("{0}", "            addParams.Remove(addParam);"));
            genContent.Add(string.Format("            int {0} = Convert.ToInt32(addParam.Value);", keyP.PropertyName.ToLower()));
            genContent.Add(string.Format("            {0} dataItem = _{1}Repository.GetById({2});", genInf.ModelName, genInf.ModelName.ToLower(), keyP.PropertyName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (dataItem == null) return null;"));
            genContent.Add(string.Format("{0}", "            //update field"));
            genContent.Add(string.Format("{0}", "            foreach (AddedParam p in addParams)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                switch (p.Key)"));
            genContent.Add(string.Format("{0}", "                {"));
            foreach (var p in listP)
            {
                switch (p.SystemType)
                {
                    case "string":
                        genContent.Add(string.Format("                case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                    string {0} = string.Empty;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                    if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                    {"));
                        genContent.Add(string.Format("                      {0} =  p.Value.ToString();", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                    }"));
                        genContent.Add(string.Format("                    dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                    break;"));
                        break;
                    case "int":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                     int {0} = 0;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToInt32(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "int?":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                     int? {0} = null;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToInt32(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "DateTime":
                    case "DateTime?":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                       DateTime {0} = Convert.ToDateTime(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("                       dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "bool":
                         genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                     bool {0} = false;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToBoolean(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "bool?":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                     bool? {0} = null;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToBoolean(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "decimal":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                      decimal {0} = 0;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToDecimal(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "decimal?":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                      decimal? {0} = null;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToDecimal(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "double":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                      double {0} = 0;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToDouble(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;
                    case "double?":
                        genContent.Add(string.Format("                 case \"{0}\":", p.PropertyName));
                        genContent.Add(string.Format("                      double? {0} = null;", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     if(p.Value != null)"));
                        genContent.Add(string.Format("{0}", "                     {"));
                        genContent.Add(string.Format("                        {0} =  Convert.ToDouble(p.Value);", p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     }"));
                        genContent.Add(string.Format("                      dataItem.{0} = {1};", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("{0}", "                     break;"));
                        break;

                }
            }
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("            _{0}Repository.Update(dataItem);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            _unitOfWork.Commit();"));

            genContent.Add(string.Format("{0}", "            var item = ConvertToDTO(dataItem);"));
            genContent.Add(string.Format("{0}", "            return item;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region SaveOrUpdate
            genContent.Add(string.Format("        public {0}DTO SaveOrUpdate({0}DTO item)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0} dataItem;", genInf.ModelName));

            genContent.Add(string.Format("            if (item.{0}ID != 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                //set logic when update Department"));
            genContent.Add(string.Format("                dataItem = _{0}Repository.GetById(item.{1}ID);", genInf.ModelName.ToLower(), genInf.ModelName));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                //set logic when create new Department"));
            genContent.Add(string.Format("                dataItem = new {0}();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                dataItem.CreatedBy = item.CreatedBy;"));
            genContent.Add(string.Format("{0}", "                dataItem.CreatedDate = DateTime.Now;"));
            genContent.Add(string.Format("{0}", "                dataItem.Active = true;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            //Set common properties of (create and update)"));

            foreach (var p in listP)
            {
                genContent.Add(string.Format("            dataItem.{0} = item.{0};", p.PropertyName));
            }
            genContent.Add(string.Format("            if (item.{0}ID != 0)", genInf.ModelName));
            genContent.Add(string.Format("                _{0}Repository.Update(dataItem);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("                _{0}Repository.Add(dataItem);", genInf.ModelName.ToLower()));

            genContent.Add(string.Format("{0}", "            _unitOfWork.Commit();"));
            genContent.Add(string.Format("{0}", "            item = ConvertToDTO(dataItem);"));
            genContent.Add(string.Format("{0}", "            return item;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Delete
            genContent.Add(string.Format("        public {0}DTO Delete(int id)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0} dataObj = _{1}Repository.GetById(id);", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("            _{0}Repository.Delete(dataObj);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            _unitOfWork.Commit();"));
            genContent.Add(string.Format("{0}", "            return ConvertToDTO(dataObj);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region ConvertToDTO
            genContent.Add(string.Format("        private {0}DTO ConvertToDTO({0} data)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (data != null)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                {0}DTO dto = new {0}DTO();", genInf.ModelName));
            foreach (var p in listProperty)
            {
                genContent.Add(string.Format("                dto.{0} = data.{0};", p.PropertyName));
            }
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "             dto." + p.PropertyName.Replace("ID", "") + "Name = data." + p.PropertyName.Replace("ID", "") + "!=null?data." + p.PropertyName.Replace("ID", "") + ".Name:\"\";"));
            }
            genContent.Add(string.Format("{0}", "                return dto;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return null;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Update List
            //genContent.Add(string.Format("        public List<{0}DTO> Update(List<{0}DTO> list)", genInf.ModelName));
            //genContent.Add(string.Format("{0}", "        {"));
            //genContent.Add(string.Format("{0}", "            try"));
            //genContent.Add(string.Format("{0}", "            {"));
            //genContent.Add(string.Format("                List<{0}> listData = new List<{0}>();", genInf.ModelName));
            //genContent.Add(string.Format("{0}", "                foreach (var item in list)"));
            //genContent.Add(string.Format("{0}", "                {"));
            //genContent.Add(string.Format("                    if (item.{0} > 0)", keyP.PropertyName));
            //genContent.Add(string.Format("{0}", "                    {"));
            //genContent.Add(string.Format("                        var dataItem = _{0}Repository.GetById(item.{1});", genInf.ModelName.ToLower(), keyP.PropertyName));
            //genContent.Add(string.Format("{0}", "                        ConvertDTOToData(item, dataItem);"));
            //genContent.Add(string.Format("{0}", "                        listData.Add(dataItem);"));
            //genContent.Add(string.Format("                        _{0}Repository.Update(dataItem);", genInf.ModelName.ToLower()));
            //genContent.Add(string.Format("{0}", "                    }"));
            //genContent.Add(string.Format("{0}", "                    else"));
            //genContent.Add(string.Format("{0}", "                    {"));
            //genContent.Add(string.Format("                        var dataItem = new {0}();", genInf.ModelName));
            //genContent.Add(string.Format("{0}", "                        ConvertDTOToData(item, dataItem);"));
            //genContent.Add(string.Format("{0}", "                        listData.Add(dataItem);"));
            //genContent.Add(string.Format("                        _{0}Repository.Add(dataItem);", genInf.ModelName.ToLower()));
            //genContent.Add(string.Format("{0}", "                    }"));
            //genContent.Add(string.Format("{0}", "                }"));
            //genContent.Add(string.Format("{0}", "                _unitOfWork.Commit();"));
            //genContent.Add(string.Format("{0}", "                for (int i = 0; i < listData.Count; i++)"));
            //genContent.Add(string.Format("{0}", "                {"));
            //genContent.Add(string.Format("                    list[i].{0} = listData[i].{0};", keyP.PropertyName));
            //genContent.Add(string.Format("{0}", "                }"));
            //genContent.Add(string.Format("{0}", "                return list;"));
            //genContent.Add(string.Format("{0}", "            }"));
            //genContent.Add(string.Format("{0}", "            catch"));
            //genContent.Add(string.Format("{0}", "            {"));

            //genContent.Add(string.Format("{0}", "            }"));
            //genContent.Add(string.Format("{0}", "            return null;"));
            //genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region UpdateList 1.1
            genContent.Add(string.Format("{0}", "        public List<" + genInf.ModelName + "DTO> Update(List<" + genInf.ModelName + "DTO> list)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                List<" + genInf.ModelName + "DTO> listDelete = list.Where(u => u.EditMode == \"Delete\").ToList();"));
            genContent.Add(string.Format("{0}", "                List<" + genInf.ModelName + "DTO> listNew = list.Where(u => u.EditMode == \"New\").ToList();"));
            genContent.Add(string.Format("{0}", "                List<" + genInf.ModelName + "DTO> listEdit = list.Where(u => u.EditMode == \"Edit\").ToList();"));
            genContent.Add(string.Format("{0}", "                List<" + genInf.ModelName + "> listUpdate = new List<" + genInf.ModelName + ">();"));
            genContent.Add(string.Format("{0}", "                if (listDelete != null && listDelete.Count > 0)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    int[] listids = listDelete.Select(u => u." + keyP.PropertyName + ").ToArray();"));
            genContent.Add(string.Format("{0}", "                    _" + genInf.ModelName.ToLower() + "Repository.Delete(u => listids.Contains(u." + keyP.PropertyName + "));"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                if (listEdit != null && listEdit.Count > 0)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    int[] listids = listEdit.Select(u => u." + keyP.PropertyName + ").ToArray();"));
            genContent.Add(string.Format("{0}", "                    List<" + genInf.ModelName + "> listData = _" + genInf.ModelName.ToLower() + "Repository.GetMany(u => listids.Contains(u." + keyP.PropertyName + ")).ToList();"));
            genContent.Add(string.Format("{0}", "                    if (listData.Count > 0)"));
            genContent.Add(string.Format("{0}", "                    {"));
            genContent.Add(string.Format("{0}", "                        foreach (var data in listData)"));
            genContent.Add(string.Format("{0}", "                        {"));
            genContent.Add(string.Format("{0}", "                            var dto = listEdit.First(u => u." + keyP.PropertyName + " == data." + keyP.PropertyName + ");"));
            genContent.Add(string.Format("{0}", "                            ConvertDTOToData(dto, data);"));
            genContent.Add(string.Format("{0}", "                            _" + genInf.ModelName.ToLower() + "Repository.Update(data);"));
            genContent.Add(string.Format("{0}", "                            dto = ConvertToDTO(data);"));
            genContent.Add(string.Format("{0}", "                        }"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                IDictionary<string, " + genInf.ModelName + "> dictNew = new Dictionary<string, " + genInf.ModelName + ">();"));
            genContent.Add(string.Format("{0}", "                if (listNew != null && listNew.Count > 0)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    foreach (var dto in listNew)"));
            genContent.Add(string.Format("{0}", "                    {"));
            genContent.Add(string.Format("{0}", "                        var dataItem = new " + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "                        ConvertDTOToData(dto, dataItem);"));
            genContent.Add(string.Format("{0}", "                        _" + genInf.ModelName.ToLower() + "Repository.Add(dataItem);"));
            genContent.Add(string.Format("{0}", "                        dictNew.Add(dto.GUID, dataItem);"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                _unitOfWork.Commit();"));
            genContent.Add(string.Format("{0}", "                foreach (var item in listNew)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    item." + keyP.PropertyName + " = dictNew[item.GUID]." + keyP.PropertyName + ";"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                return list;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return null;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region ConVertDTOToData
            genContent.Add(string.Format("        private void ConvertDTOToData({0}DTO dto, {0} data)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("                data.{0} = dto.{0};", keyP.PropertyName));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                data.{0} = dto.{0};", p.PropertyName));
            }
            genContent.Add(string.Format("            if (dto.{0} > 0)", keyP.PropertyName));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                data.ModifiedBy = dto.CreatedBy;"));
            genContent.Add(string.Format("{0}", "                data.ModifiedDate = DateTime.Now;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                data.Active = true;"));
            genContent.Add(string.Format("{0}", "                data.CreatedBy = dto.CreatedBy;"));
            genContent.Add(string.Format("{0}", "                data.CreatedDate = DateTime.Now;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            genContent.Add(string.Format("{0}", "    }"));
            #endregion
            #region Interface
            genContent.Add(string.Format("    public interface I{0}", genInf.GenClassName));
            genContent.Add(string.Format("    {0}", "{"));
            genContent.Add(string.Format("          {0}DTO Get(int id);", genInf.ModelName));
            genContent.Add(string.Format("          {0}DTO Get(List<AddedParam> addParams);", genInf.ModelName));
            genContent.Add(string.Format("          bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);"));
            genContent.Add(string.Format("          ListDTOModel<{0}DTO> GetPage(List<AddedParam> addedParams);", genInf.ModelName));
            genContent.Add(string.Format("          object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);"));
            genContent.Add(string.Format("          IEnumerable<{0}DTO> GetMany(List<AddedParam> addedParams);", genInf.ModelName));
            genContent.Add(string.Format("          {0}DTO SaveOrUpdate({0}DTO item);", genInf.ModelName));
            genContent.Add(string.Format("          {0}DTO Update(List<AddedParam> addParams);", genInf.ModelName));
            genContent.Add(string.Format("          {0}DTO Delete(int id);", genInf.ModelName));
            genContent.Add(string.Format("          List<{0}DTO> Update(List<{0}DTO> list);", genInf.ModelName));
            genContent.Add(string.Format("    {0}", "}"));

            #endregion
            genContent.Add(string.Format("{0}", " }"));
            return genContent;
        }
        private List<string> API_Controller(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            #region gencontent
            #region Using
            genContent.Add(string.Format("{0}", "using System;"));
            genContent.Add(string.Format("{0}", "using System.Linq;"));
            genContent.Add(string.Format("{0}", "using System.Collections.Generic;"));
            genContent.Add(string.Format("{0}", "using System.Net;"));
            genContent.Add(string.Format("{0}", "using System.Net.Http;"));
            genContent.Add(string.Format("{0}", "using System.Web.Http;"));
            genContent.Add(string.Format("{0}", "using BaseAPI.Model.DTO;"));
            genContent.Add(string.Format("using {0}.Logic;", genInf.ProjectName));
            genContent.Add(string.Format("using {0}.Model.DTO;", genInf.ProjectName));
            #endregion
            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            #region Class
            genContent.Add(string.Format("    public class {0} : ApiController", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("        I{0}Logic _oLogic;", genInf.ModelName));
            #region Contructor
            genContent.Add(string.Format("        public {0}(I{1}Logic oLogic)", genInf.GenClassName, genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            _oLogic = oLogic;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Get
            genContent.Add(string.Format("        public {0}DTO Get(int id)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0}DTO obj = _oLogic.Get(id);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            return obj;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Post One
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostOne(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                  {0}DTO rObj = _oLogic.Get(addedParams);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);"));
            genContent.Add(string.Format("{0}", "                return response;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Put
            genContent.Add(string.Format("        public HttpResponseMessage Put(int id, {0}DTO obj)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            if (ModelState.IsValid && id == obj.{0}ID)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                try"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("                       obj = _oLogic.SaveOrUpdate(obj);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                catch"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    return Request.CreateResponse(HttpStatusCode.NotFound);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.OK, obj);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "       }"));
            #endregion
            #region Post Create
            genContent.Add(string.Format("        public HttpResponseMessage Post({0}DTO obj)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                   obj = _oLogic.SaveOrUpdate(obj);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);"));
            genContent.Add(string.Format("{0}", "                return response;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostUpdate
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                " + genInf.ModelName + "DTO obj = new " + genInf.ModelName + "DTO();"));
            genContent.Add(string.Format("{0}", "                try"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    obj = _oLogic.Update(addedParams);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                catch"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    return Request.CreateResponse(HttpStatusCode.NotFound);"));
            genContent.Add(string.Format("{0}", "                }"));

            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.OK, obj);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostMany
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostMany(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                IEnumerable<{0}DTO> rObj = _oLogic.GetMany(addedParams);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);"));
            genContent.Add(string.Format("{0}", "                return response;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostPage
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostPage(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                ListDTOModel<{0}DTO> rObj = _oLogic.GetPage(addedParams);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);"));
            genContent.Add(string.Format("{0}", "                return response;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostScalar
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostScalar(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                Object rObj = _oLogic.GetScalar(addedParams);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);"));
            genContent.Add(string.Format("{0}", "                return response;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostDelete
            genContent.Add(string.Format("{0}", "        public HttpResponseMessage PostDelete(List<AddedParam> addedParams)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                 var param = addedParams.FirstOrDefault(p => p.Key == \"{0}ID\");", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 if (param != null && param.Value != null)"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                      int id = Convert.ToInt32(param.Value);"));
            genContent.Add(string.Format("                            {0}DTO obj = _oLogic.Get(id);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      _oLogic.Delete(id);"));
            genContent.Add(string.Format("{0}", "                      HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);"));
            genContent.Add(string.Format("{0}", "                      return response;"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 else"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                       return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Delete
            genContent.Add(string.Format("{0}", "        public void Delete(int id)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0}DTO obj = _oLogic.Get(id);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            _oLogic.Delete(id);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Dispose
            genContent.Add(string.Format("{0}", "        protected override void Dispose(bool disposing)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            base.Dispose(disposing);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region PostList
            genContent.Add(string.Format("        public HttpResponseMessage PostList(List<{0}DTO> list)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            if (ModelState.IsValid)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                var result = _oLogic.Update(list);"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.OK, result);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                return Request.CreateResponse(HttpStatusCode.BadRequest);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            genContent.Add(string.Format("{0}", "    }"));
            #endregion
            genContent.Add(string.Format("{0}", "}"));

            #endregion
            return genContent;
        }
        #endregion
        #region MVC Code
        private List<string> MVC_DTO(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));
            genContent.Add(string.Format("{0}", "using System;"));

            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", @"{"));
            genContent.Add(string.Format("    public partial class {0}", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            foreach (var p in listProperty)
            {
                genContent.Add(string.Format("        {0} {1} {2} {3}", "public", p.SystemType, p.PropertyName, "{ get; set; }"));

            }
            genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", "EditMode", "{ get; set; }"));
            genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", "GUID", "{ get; set; }"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("        {0} {1} {2} {3}", "public", "string", p.PropertyName.Replace("ID", "") + "Name", "{ get; set; }"));
            }
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("{0}", "}"));
            return genContent;
        }
        private List<string> MVC_Service(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            #region gencontent
            #region using
            genContent.Add(string.Format("{0}", "using System;"));
            genContent.Add(string.Format("{0}", "using System.Collections;"));
            genContent.Add(string.Format("{0}", "using System.Collections.Generic;"));
            genContent.Add(string.Format("{0}", "using System.Net;"));
            genContent.Add(string.Format("{0}", "using System.Net.Http;"));
            if (genInf.ProjectName != "Base")
            {
                genContent.Add(string.Format("{0}", "using Base.Model.DTO;"));
            }
            genContent.Add(string.Format("using {0}.Model.DTO;", genInf.ProjectName));
            #endregion
            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            #region class
            genContent.Add(string.Format("     public class {0} : I{0}", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("{0}", "        private string _baseURI;"));
            genContent.Add(string.Format("{0}", "        private HttpClient httpClient;"));
            #region Contructor
            genContent.Add(string.Format("        public {0}(string baseURI)", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            _baseURI = baseURI;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Get ID
            genContent.Add(string.Format("        public  {0}DTO Get(int id, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("            {0}DTO rObj = null;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                    var response = httpClient.GetAsync(string.Format(\"api/" + genInf.ModelName + "/Get/{0}\", id)).Result;"));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                    if (hCode == HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "                    {"));
            genContent.Add(string.Format("                        rObj = response.Content.ReadAsAsync<{0}DTO>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return rObj;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetOne
            genContent.Add(string.Format("         public {0}DTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("               {0}DTO result = null;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostOne\", addedParams).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                    response.EnsureSuccessStatusCode();"));
            genContent.Add(string.Format("                    result = response.Content.ReadAsAsync<{0}DTO>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return result;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Create
            genContent.Add(string.Format("        public {0}DTO Create({0}DTO obj, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                    HttpResponseMessage response = httpClient.PostAsJsonAsync<" + genInf.ModelName + "DTO>(\"api/" + genInf.ModelName + "/Post\", obj).Result;"));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));

            genContent.Add(string.Format("                    if(hCode==HttpStatusCode.Created) obj = response.Content.ReadAsAsync<{0}DTO>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    return obj;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "                return null;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Update Object
            genContent.Add(string.Format("        public {0}DTO Update({0}DTO obj, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                    HttpResponseMessage response = httpClient.PutAsJsonAsync<" + genInf.ModelName + "DTO>(string.Format(\"api/" + genInf.ModelName + "/Put/{0}\", obj." + genInf.ModelName + "ID), obj).Result;"));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                    return response.Content.ReadAsAsync<" + genInf.ModelName + "DTO>().Result;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "                return null;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Update Param
            genContent.Add(string.Format("{0}", "        public "+genInf.ModelName+"DTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode)"));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostUpdate\", addedParams).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                    if(hCode==HttpStatusCode.OK)  return response.Content.ReadAsAsync<" + genInf.ModelName + "DTO>().Result;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return null;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetMany
            genContent.Add(string.Format("        public IEnumerable<{0}DTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("            IEnumerable<{0}DTO> result = null;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostMany\", addedParams).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("                    if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<IEnumerable<{0}DTO>>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return result;"));
            genContent.Add(string.Format("{0}", "         }"));
            #endregion
            #region GetPage
            genContent.Add(string.Format("        public ListDTOModel<{0}DTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("             ListDTOModel<{0}DTO> result = null;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("                    var response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostPage\", addedParams).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("                    if(hCode==HttpStatusCode.OK)  result = response.Content.ReadAsAsync<ListDTOModel<{0}DTO>>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return result;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetExist
            genContent.Add(string.Format("         public bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "            bool result = false;"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("                     var response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostExist\", addedParams).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("                           if(hCode==HttpStatusCode.OK) result = response.Content.ReadAsAsync<bool>().Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return result;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Delete
            /* genContent.Add(string.Format("{0}", "        public int Delete(int id, out HttpStatusCode hCode)"));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                    HttpResponseMessage response = httpClient.DeleteAsync(string.Format(\"api/" + genInf.ModelName + "/Delete/{0}\", id)).Result;"));
            genContent.Add(string.Format("{0}", "                    hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                    if (response.IsSuccessStatusCode)"));
            genContent.Add(string.Format("{0}", "                        return id;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            return 0;"));
            genContent.Add(string.Format("{0}", "        }"));*/
            genContent.Add(string.Format("{0}", "        public int Delete(int id, out HttpStatusCode hCode)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                      List<AddedParam> param = new List<AddedParam>();"));
            genContent.Add(string.Format("{0}", "                      param.Add(new AddedParam { Key = \"" + genInf.ModelName + "ID\", Value = id });"));
            genContent.Add(string.Format("                      HttpResponseMessage response = httpClient.PostAsJsonAsync<List<AddedParam>>(\"api/{0}/PostDelete\", param).Result;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                      if (hCode == HttpStatusCode.OK) return id;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             return 0;"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Dispose
            genContent.Add(string.Format("{0}", "        public void Dispose()"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            httpClient.Dispose();"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region UpdateList
            genContent.Add(string.Format("{0}", "         public List<" + genInf.ModelName + "DTO> Update(List<" + genInf.ModelName + "DTO> list, out HttpStatusCode hCode)"));
            genContent.Add(string.Format("{0}", "         {"));
            genContent.Add(string.Format("{0}", "             try"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                 using (httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) })"));
            genContent.Add(string.Format("{0}", "                 {"));
            genContent.Add(string.Format("{0}", "                     var response = httpClient.PostAsJsonAsync<List<" + genInf.ModelName + "DTO>>(\"api/" + genInf.ModelName + "/PostList\", list).Result;"));
            genContent.Add(string.Format("{0}", "                     hCode = response.StatusCode;"));
            genContent.Add(string.Format("{0}", "                     if (hCode == HttpStatusCode.OK) return response.Content.ReadAsAsync<List<" + genInf.ModelName + "DTO>>().Result;"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             catch"));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("{0}", "                 hCode = HttpStatusCode.RequestTimeout;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             return null;"));
            genContent.Add(string.Format("{0}", "         }"));
            #endregion
            genContent.Add(string.Format("{0}", "    }"));
            #endregion
            #region Interface
            genContent.Add(string.Format("    public interface I{0}", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("           {0}DTO Get(int id, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           {0}DTO Get(List<AddedParam> addedParams, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           {0}DTO Create({0}DTO obj, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           {0}DTO Update({0}DTO obj, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           {0}DTO Update(List<AddedParam> addedParams, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           IEnumerable<{0}DTO> GetMany(List<AddedParam> addedParams, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           ListDTOModel<{0}DTO> GetPage(List<AddedParam> addedParams, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("           bool GetExist(List<AddedParam> addedParams, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "           int Delete(int id, out System.Net.HttpStatusCode hCode) ;"));
            genContent.Add(string.Format("{0}", "           void Dispose() ;"));
            genContent.Add(string.Format("           List<{0}DTO> Update(List<{0}DTO> list, out HttpStatusCode hCode);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    }"));
            #endregion
            genContent.Add(string.Format("{0}", "}"));

            #endregion
            return genContent;
        }
        private List<string> MVC_ViewModel(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            #region getcontent
            genContent.Add(string.Format("{0}", "using System.Collections.Generic;"));
            genContent.Add(string.Format("using {0}.Model.DTO;", genInf.ProjectName));

            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            genContent.Add(string.Format("    public class {0}ViewModel", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("{0}", "        public string Result {get;set;}"));
            genContent.Add(string.Format("{0}", "        public string GUID { get; set; }"));
            genContent.Add(string.Format("        public int {0}ID {1}", genInf.ModelName, "{ get; set; }"));
            genContent.Add(string.Format("        public {0}DTO {0} {1}", genInf.ModelName, "{ get; set; }"));
            genContent.Add(string.Format("        public {0}ViewModel()", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0} = new {0}DTO();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        }"));
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("    public class {0}sViewModel", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("{0}", "        public string Result { get; set; }"));
            genContent.Add(string.Format("{0}", "        public int? PageIndex { get; set; }"));
            genContent.Add(string.Format("{0}", "        public int? PageSize { get; set; }"));
            genContent.Add(string.Format("{0}", "        public int? TotalCount { get; set; }"));
            genContent.Add(string.Format("        public List<{0}DTO> {0}s {1}", genInf.ModelName, "{ get; set; }"));
            genContent.Add(string.Format("        public {0}sViewModel()", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            {0}s = new List<{0}DTO>();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        }"));
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("{0}", "}"));
            #endregion
            return genContent;
        }
        private List<string> MVC_Controller(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var keyP = listProperty.Single(p => p.IsPrimaryKey);
            #region gencontent
            #region Using

            genContent.Add(string.Format("{0}", "using System;"));
            genContent.Add(string.Format("{0}", "using System.Collections.Generic;"));
            genContent.Add(string.Format("{0}", "using System.Linq;"));
            genContent.Add(string.Format("{0}", "using System.Net;"));
            genContent.Add(string.Format("{0}", "using System.Web.Mvc;"));
            genContent.Add(string.Format("{0}", "using Ninject;"));
            if (genInf.ProjectName != "Base")
            {
                genContent.Add(string.Format("{0}", "using Base;"));
                genContent.Add(string.Format("{0}", "using Base.Model.DTO;"));
                genContent.Add(string.Format("{0}", "using Base.ViewModel;"));
            }
            genContent.Add(string.Format("using {0}.Model.DTO;", genInf.ProjectName));
            genContent.Add(string.Format("using {0}.Service;", genInf.ProjectName));
            genContent.Add(string.Format("using {0}.ViewModel;", genInf.ProjectName));
            #endregion
            genContent.Add(string.Format("namespace {0}", genInf.Namespace));
            genContent.Add(string.Format("{0}", "{"));
            genContent.Add(string.Format("    public class {0}Controller : BaseController", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    {"));
            genContent.Add(string.Format("        I{0}Service _{1}Service;", genInf.ModelName, genInf.ModelName.ToLower()));
            #region Contructor
            genContent.Add(string.Format("        public {0}()", genInf.GenClassName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "        }"));
            genContent.Add(string.Format("{0}", "        [Inject]"));
            genContent.Add(string.Format("        public {0}Controller(I{0}Service {1}Service)", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("            _{0}Service = {0}Service;", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Admin
            genContent.Add(string.Format("{0}", "        #region Normal Action"));
            genContent.Add(string.Format("{0}", "        public ActionResult Admin()"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            return View();"));
            genContent.Add(string.Format("{0}", "        }"));
            genContent.Add(string.Format("{0}", "        #endregion"));
            #endregion
            #region Detail
            genContent.Add(string.Format("{0}", "        public ActionResult Detail(int id)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            ViewBag." + keyP.PropertyName + " = id;"));
            genContent.Add(string.Format("{0}", "            return View();"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Edit
            genContent.Add(string.Format("{0}", "        public ActionResult Edit(int id)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            ViewBag." + keyP.PropertyName + " = id;"));
            genContent.Add(string.Format("{0}", "            return View();"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Create
            genContent.Add(string.Format("{0}", "        public ActionResult Create()"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            return View();"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            genContent.Add(string.Format("{0}", "        #region Ajax Action"));
            #region Get
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult Get(int? id)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("            {0}ViewModel mv = new {0}ViewModel();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            if (id == null || id == 0)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("                mv.{0} = new {0}DTO();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                mv.{0} = _{1}Service.Get(id.Value, out hCode);", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "                if (hCode != HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Gets
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult Gets(string sortedfield,bool? sorteddirection)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("            {0}sViewModel mv = new {0}sViewModel();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            List<AddedParam> param = new List<AddedParam>();"));
            genContent.Add(string.Format("{0}", "            if (!string.IsNullOrWhiteSpace(sortedfield))"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                param.Add(new AddedParam { Key = \"SortedField\", Value = sortedfield });"));
            genContent.Add(string.Format("{0}", "                param.Add(new AddedParam { Key = \"SortedDirection\", Value = sorteddirection ?? false });"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("            mv.{0}s = _{1}Service.GetMany(param, out hCode).ToList();", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (hCode != HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            foreach (var item in mv." + genInf.ModelName + "s) {"));
            genContent.Add(string.Format("{0}", "                item.GUID = Guid.NewGuid().ToString();"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetPage

            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult GetPage(int? pageindex,int? pagesize,string sortedfield,bool? sorteddirection)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("            {0}sViewModel mv = new {0}sViewModel();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            List<AddedParam> param = new List<AddedParam>();"));
            genContent.Add(string.Format("{0}", "            if (pageindex.HasValue)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add("                param.Add(new AddedParam { Key = \"PageIndex\", Value = pageindex.Value });");
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            if (pagesize.HasValue)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add("                param.Add(new AddedParam { Key = \"PageSize\", Value = pagesize.Value });");
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            if (!string.IsNullOrWhiteSpace(sortedfield))"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                param.Add(new AddedParam { Key = \"SortedField\", Value = sortedfield });"));
            genContent.Add(string.Format("{0}", "                param.Add(new AddedParam { Key = \"SortedDirection\", Value = sorteddirection ?? false });"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("           ListDTOModel<{0}DTO> rList = _{1}Service.GetPage(param, out hCode);", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "           mv.PageIndex = pageindex;"));
            genContent.Add(string.Format("{0}", "           mv.PageSize = pagesize;"));
            genContent.Add(string.Format("{0}", "           mv.TotalCount = rList.TotalCount;"));
            genContent.Add(string.Format("           mv.{0}s = rList.Source;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            if (hCode != HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region GetGuidString
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult GetGUIDString()"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            GUIDGenerateViewModel mv = new GUIDGenerateViewModel();"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Save
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("        public JsonResult Save({0}DTO obj)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("            {0}ViewModel mv = new {0}ViewModel();", genInf.ModelName));
            genContent.Add(string.Format("                mv.{0}ID = obj.{0}ID;", genInf.ModelName));
            genContent.Add(string.Format("              if (obj.{0}ID == 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             {"));
            genContent.Add(string.Format("                mv.GUID = obj.GUID;", genInf.ModelName));
            genContent.Add(string.Format("                mv.{0} = obj;", genInf.ModelName));
            genContent.Add(string.Format("                mv.{0}.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;", genInf.ModelName));
            genContent.Add(string.Format("                mv.{0} = _{1}Service.Create(mv.{0}, out hCode);", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "                if (hCode != HttpStatusCode.Created)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                    return Json(mv);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            else"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("                mv.{0} = obj;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                List<AddedParam> param = new List<AddedParam>();"));
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            foreach (var p in listP)
            {
                genContent.Add("                param.Add(new AddedParam { Key = \"" + p.PropertyName + "\", Value = obj." + p.PropertyName + " });");
            }
            genContent.Add(string.Format("                mv."+genInf.ModelName+" = _{0}Service.Update(param, out hCode);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "                if (hCode != HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                    return Json(mv);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            }"));

            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region Remove
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult Remove(int id)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("            {0}ViewModel mv = new {0}ViewModel();", genInf.ModelName));
            genContent.Add(string.Format("            mv.{0}ID = id;", genInf.ModelName));
            genContent.Add(string.Format("            _{0}Service.Delete(id, out hCode);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "            if (hCode != HttpStatusCode.OK)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            #region SaveList
            genContent.Add(string.Format("{0}", "        [AllowAnonymous]"));
            genContent.Add(string.Format("{0}", "        [HttpPost]"));
            genContent.Add(string.Format("{0}", "        public JsonResult SaveList(List<" + genInf.ModelName + "DTO> obj)"));
            genContent.Add(string.Format("{0}", "        {"));
            genContent.Add(string.Format("{0}", "            HttpStatusCode hCode;"));
            genContent.Add(string.Format("{0}", "            " + genInf.ModelName + "sViewModel mv = new " + genInf.ModelName + "sViewModel();"));
            genContent.Add(string.Format("{0}", "            if (obj == null || obj.Count == 0)"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            try"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                foreach (var item in obj)"));
            genContent.Add(string.Format("{0}", "                {"));
            genContent.Add(string.Format("{0}", "                    item.CreatedBy = SessionUtils.CurrentUser.UserInstanceID;"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "                mv." + genInf.ModelName + "s = _" + genInf.ModelName.ToLower() + "Service.Update(obj,out hCode);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            catch"));
            genContent.Add(string.Format("{0}", "            {"));
            genContent.Add(string.Format("{0}", "                mv.Result = Base.ViewModel.ResultType.Unsuccess;"));
            genContent.Add(string.Format("{0}", "                return Json(mv);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            mv.Result = Base.ViewModel.ResultType.Success;"));
            genContent.Add(string.Format("{0}", "            return Json(mv);"));
            genContent.Add(string.Format("{0}", "        }"));
            #endregion
            genContent.Add(string.Format("{0}", "        #endregion"));
            genContent.Add(string.Format("{0}", "    }"));
            genContent.Add(string.Format("{0}", "}"));

            #endregion
            return genContent;
        }
        private List<string> MVC_ArrayPageHtml(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));
            #region getcontent
            genContent.Add(string.Format("{0}", "<div class=\"row\" data-bind=\"with:" + genInf.ModelName + "sVM\">"));
            genContent.Add(string.Format("{0}", "    <div class=\"col-md-12\">"));
            genContent.Add(string.Format("{0}", "        <div class=\"row\">"));
            genContent.Add(string.Format("{0}", "            <div class=\"col-md-12\">"));
            genContent.Add(string.Format("{0}", "                <div class=\"dark\">"));
            genContent.Add(string.Format("{0}", "                    <div class=\"row\">"));
            genContent.Add(string.Format("{0}", "                        <div class=\"col-md-5\">"));
            genContent.Add(string.Format("{0}", "                            <span>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Title </span>"));
            genContent.Add(string.Format("{0}", "                        </div>"));
            genContent.Add(string.Format("{0}", "                        <div class=\"col-md-3\">"));
            genContent.Add(string.Format("{0}", "                            <div class=\"input-group\">"));
            genContent.Add(string.Format("{0}", "                                <input type=\"text\" class=\"form-control\" placeholder=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.PlaceHolder_Search\" data-bind=\"value:SearchCode\" />"));
            genContent.Add(string.Format("{0}", "                                <div class=\"input-group-btn\">"));
            genContent.Add(string.Format("{0}", "                                    <button class=\"btn btn-default\" type=\"button\" data-bind=\"click:search" + genInf.ModelName + "s\" style=\"margin-top:4px;\">"));
            genContent.Add(string.Format("{0}", "                                        <i class=\"glyphicon glyphicon-search\"></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Search"));
            genContent.Add(string.Format("{0}", "                                    </button>"));
            genContent.Add(string.Format("{0}", "                                </div>"));
            genContent.Add(string.Format("{0}", "                            </div>"));
            genContent.Add(string.Format("{0}", "                        </div>"));
            genContent.Add(string.Format("{0}", "                        <div class=\"col-md-4\">"));
            genContent.Add(string.Format("{0}", "                        </div>"));
            genContent.Add(string.Format("{0}", "                    </div>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "            </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "        <div data-bind=\"visible:Processing().GetProcessing('" + genInf.ModelName + "s')\" class=\"row\" style=\"position: absolute\">"));
            genContent.Add(string.Format("{0}", "            <div class=\"col-md-5 col-md-offset-5\">"));
            genContent.Add(string.Format("{0}", "                <img src=\"~/Images/kloading.gif\" alt=\"Loading...\" />"));
            genContent.Add(string.Format("{0}", "            </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "        <div class=\"row\">"));
            genContent.Add(string.Format("{0}", "            <div class=\"col-md-12\">"));
            genContent.Add(string.Format("{0}", "                @Html.Partial(\"_Table" + genInf.ModelName + "\")"));
            genContent.Add(string.Format("{0}", "            </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "    </div>"));
            genContent.Add(string.Format("{0}", "</div>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_AdminTableHtml(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));

            #region getcontent
            genContent.Add(string.Format("<div id=\"grid{0}\" class='contenttable'>", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    <table width=\"100%\" class=\"table table-bordered table-hover\">"));
            genContent.Add(string.Format("{0}", "        <thead>"));
            genContent.Add(string.Format("{0}", "            <tr>"));
            genContent.Add(string.Format("{0}", "                <td class=\"k-header\" style=\"text-align: center\">"));
            genContent.Add(string.Format("{0}", "                </td>"));
            genContent.Add(string.Format("{0}", "                <td class=\"k-header\" style=\"text-align: center\">"));
            genContent.Add(string.Format("                 @{0}.Resource.{1}.Admin.Label_Table_OrderNo", genInf.ProjectName, genInf.ModelName));
            genContent.Add(string.Format("{0}", "                </td>"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("{0}", "                <td class=\"k-header\"  style=\"text-align:center;cursor:pointer\" data-bind=\"click:function(){startSort('" + p.PropertyName + "');}\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "  "));
                genContent.Add(string.Format("{0}", "                  <div data-bind=\"visible:SortedField()!=null&&SortedField().Name()=='" + p.PropertyName + "'\" class=\"pull-right\">"));
                genContent.Add(string.Format("{0}", "                    <a data-bind=\"visible:SortedField()!=null&&SortedField().Name()=='" + p.PropertyName + "'&&SortedField().Direction()==false\" \"btn bnt-small\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a>"));
                genContent.Add(string.Format("{0}", "                    <a data-bind=\"visible:SortedField()!=null&&SortedField().Name()=='" + p.PropertyName + "'&&SortedField().Direction()==true\" \"btn bnt-small\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a>"));
                genContent.Add(string.Format("{0}", "                   </div>"));
                genContent.Add(string.Format("{0}", "                </td>"));
            }
            genContent.Add(string.Format("{0}", "            </tr>"));
            genContent.Add(string.Format("{0}", "        </thead>"));
            genContent.Add(string.Format("{0}", "        <tbody data-bind=\"template:{name:'" + genInf.ModelName + "Item',foreach:Arr" + genInf.ModelName + "}\">"));
            genContent.Add(string.Format("{0}", "        </tbody>"));

            genContent.Add(string.Format("{0}", "        <tfoot>"));
            genContent.Add(string.Format("{0}", "            <tr>"));
            genContent.Add(string.Format("                <td colspan=\"{0}\">", listP.Count() + 2));
            genContent.Add(string.Format("{0}", "              <div class=\"row\">"));
            genContent.Add(string.Format("{0}", "                    <div class=\"control-group col-md-4\">"));
            genContent.Add(string.Format("{0}", "                        <div class=\"controls\">"));
            genContent.Add(string.Format("{0}", "                            <a class=\"btn btn-default\" data-bind=\"click:startAdd" + genInf.ModelName + "\"><i class=\"glyphicon glyphicon-plus\" title=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_New\" ></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_New</a>"));
            genContent.Add(string.Format("{0}", "                        </div>"));
            genContent.Add(string.Format("{0}", "                    </div>"));
            genContent.Add(string.Format("{0}", "                    <div class=\" col-md-8\">"));
            genContent.Add(string.Format("{0}", "                        <div class=\"pull-right\" data-bind=\"with:Paging\">"));
            genContent.Add(string.Format("{0}", "                            <ul class=\"pagination\">"));
            genContent.Add(string.Format("{0}", "                               <li > <a data-bind=\"html:'@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Text_TotalItems <b>' + TotalItems() + '</b> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Text_ItemName &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Text_Page <b>'+ PageIndex() + '/' + TotalPages() +'</b>'\"></a></li>"));
            genContent.Add(string.Format("{0}", "                               <li data-bind=\"css:{disabled:PageIndex()==0}\"><a data-bind=\"click:$parent.gotoPrevPage\">&laquo;</a></li>"));
            genContent.Add(string.Format("{0}", "                               <!-- ko foreach:Pages -->"));
            genContent.Add(string.Format("{0}", "                               <li data-bind=\"css:{active:$parent.PageIndex()==$data}\"><a data-bind=\"html:$data ,click:$parents[1].gotoPage\" ></a></li>"));
            genContent.Add(string.Format("{0}", "                               <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                               <li data-bind=\"css:{disabled:PageIndex()>=TotalPages()}\"><a data-bind=\"click:$parent.gotoNextPage\">&raquo;</a></li>"));
            genContent.Add(string.Format("{0}", "                            </ul>"));
            genContent.Add(string.Format("{0}", "                        </div>"));
            genContent.Add(string.Format("{0}", "                    </div>"));
            genContent.Add(string.Format("{0}", "                 </div>"));
            genContent.Add(string.Format("{0}", "                </td>"));
            genContent.Add(string.Format("{0}", "            </tr>"));
            genContent.Add(string.Format("{0}", "        </tfoot>"));
            genContent.Add(string.Format("{0}", "    </table>"));
            genContent.Add(string.Format("{0}", "</div>"));

            genContent.Add(string.Format("{0}", "<script id=\"" + genInf.ModelName + "Item\" type=\"text/html\">"));
            genContent.Add(string.Format("{0}", "<!-- ko if: IsEdit() === false -->"));
            genContent.Add(string.Format("{0}", "<tr data-bind=\"css:{normalrow:(($index()%2)==0),alternativerow:($index()%2!=0)}\">"));
            genContent.Add(string.Format("{0}", "        <td style=\"text-align:center\" class=\"col-md-1\" >"));
            genContent.Add(string.Format("{0}", "            <button class=\"btn btn-sm\" data-bind=\"visible:AllowEdit, click:$parent.startEdit" + genInf.ModelName + "\" title=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Tooltip_Edit\"><i class=\"glyphicon glyphicon-pencil\" ></i></button>"));
            genContent.Add(string.Format("{0}", "            <button class=\"btn btn-sm\" data-bind=\"visible:AllowRemove, click:$parent.remove" + genInf.ModelName + "\" title=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Tooltip_Remove\"><i class=\"glyphicon glyphicon-trash\" ></i></button>"));
            genContent.Add(string.Format("{0}", "        </td>"));
            genContent.Add(string.Format("{0}", "        <td class=\"col-md-1\" style=\"text-align:center\" >"));
            genContent.Add(string.Format("{0}", "            <span data-bind=\"html:(($parent.Paging().PageIndex()-1)*$parent.Paging().PageSize() + $index()+1)\"></span>"));
            genContent.Add(string.Format("{0}", "        </td>"));
            foreach (var p in listP)
            {
                ConvertToViewHtmlControl(genContent, p);
            }
            genContent.Add(string.Format("{0}", "</tr>"));
            genContent.Add(string.Format("{0}", "<!-- /ko -->"));
            genContent.Add(string.Format("{0}", "<!-- ko if: IsEdit() === true -->"));
            genContent.Add(string.Format("{0}", "<tr  data-bind=\"css:{normalrow:(($index()%2)==0),alternativerow:($index()%2!=0)}\">"));
            genContent.Add(string.Format("{0}", "        <td class=\"col-md-1\" style=\"text-align:center\" >"));
            genContent.Add(string.Format("{0}", "            <button class=\"btn btn-sm\" data-bind=\"click:$parent.finishEdit" + genInf.ModelName + "\" title=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Tooltip_Save\" ><i class=\"glyphicon glyphicon-ok\" ></i></button>"));
            genContent.Add(string.Format("{0}", "            <button class=\"btn btn-sm\" data-bind=\"click:$parent.cancelEdit" + genInf.ModelName + "\" title=\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Tooltip_Cancel\"><i class=\"glyphicon glyphicon-remove\"></i></button>"));
            genContent.Add(string.Format("{0}", "        </td>"));
            genContent.Add(string.Format("{0}", "        <td class=\"col-md-1\" style=\"text-align:center\">"));
            genContent.Add(string.Format("{0}", "            <span data-bind=\"html:(($parent.Paging().PageIndex()-1)*$parent.Paging().PageSize() + $index()+1)\"></span>"));
            genContent.Add(string.Format("{0}", "        </td>"));
            foreach (var p1 in listP)
            {
                ConvertToEditHtmlControl(genContent, p1, genInf);
            }
            genContent.Add(string.Format("{0}", "</tr>"));
            genContent.Add(string.Format("{0}", "<!-- /ko -->"));
            genContent.Add(string.Format("{0}", "</script>"));
            #endregion
            return genContent;
        }
        private void ConvertToEditHtmlControl(List<string> genContent, PropertyGenInfo p, GenCodeInfo genInf)
        {
            switch (p.ControlType)
            {
                case "TextBox":
                    bool isstringtype = (p.SystemType == "String" || p.SystemType == "string" || p.SystemType == "Guid");
                    if (p.PropertyName.Contains("ID"))
                    {
                        genContent.Add(string.Format("{0}", "        <td class=\"col-md-2\">"));
                        genContent.Add(string.Format("{0}", "             <select data-bind=\"options: $parent." + p.PropertyName.Replace("ID", "") + "sVM().Arr" + p.PropertyName.Replace("ID", "") + ",optionsText:'Name',optionsValue:'" + p.PropertyName + "',optionsCaption:'@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.PlaceHolder_Table_" + p.PropertyName + "',value:" + p.PropertyName + "\" class=\"form-control col-md-12\" ></select>"));
                        genContent.Add(string.Format("{0}", "        </td>"));
                    }
                    else
                    {
                        genContent.Add(string.Format("{0}", "        <td class=\"col-md-2\">"));
                        if (isstringtype)
                        {
                            genContent.Add(string.Format("{0}", "            <input type = \"text\" data-bind = \"value:" + p.PropertyName + "\"  class=\"form-control col-md-12\" />"));
                        }
                        else
                        {
                            genContent.Add(string.Format("{0}", "             <input  type=\"text\" data-bind = \"value:" + p.PropertyName + ",currencyMaskOptions:{symbol: '',roundToDecimalPlace:0}\" class=\"form-control col-md-12\" />"));
                        }
                        genContent.Add(string.Format("{0}", "        </td>"));
                    }
                    break;
                case "TextArea":
                    genContent.Add(string.Format("{0}", "        <td class=\"col-md-3\">"));
                    genContent.Add("            <input type = \"text\" data-bind = \"value:" + p.PropertyName + "\" class=\"form-control col-md-12\" />");
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;
                case "CheckBox":
                    genContent.Add(string.Format("{0}", "        <td class=\"col-md-1\">"));
                    genContent.Add(string.Format("            <input type = \"checkbox\" data-bind = \"checked:{0}\" class=\"form-control col-md-12\" />", p.PropertyName));
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;
                case "DateTimePicker":
                    genContent.Add(string.Format("{0}", "       <td class=\"col-md-1\">"));
                    genContent.Add("            <input type = \"text\" data-bind = \"datepicker: " + p.PropertyName + ", datepickerOptions: { dateFormat: 'dd/mm/yy' }\"  class=\"form-control col-md-12\"  />");
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;
            }
        }
        private void ConvertToViewHtmlControl(List<string> genContent, PropertyGenInfo p)
        {
            switch (p.ControlType)
            {
                case "TextBox":
                    bool isstringtype = (p.SystemType == "String" || p.SystemType == "string" || p.SystemType == "Guid");
                    if (p.PropertyName.Contains("ID"))
                    {
                        genContent.Add(string.Format("{0}", "        <td class=\"col-md-2\">"));
                        genContent.Add(string.Format("{0}", "             <span  data-bind=\"html:" + p.PropertyName.Replace("ID", "") + "Name\" ></span>"));
                        genContent.Add(string.Format("{0}", "        </td>"));
                    }
                    else
                    {
                        genContent.Add(string.Format("{0}", "        <td class=\"col-md-2\">"));
                        if (isstringtype)
                        {
                            genContent.Add(string.Format("{0}", "            <span data-bind=\"html:" + p.PropertyName + "\" ></span>"));
                        }
                        else
                        {
                            genContent.Add(string.Format("{0}", "            <span data-bind=\"html:" + p.PropertyName + "\" ></span>"));
                        }
                        genContent.Add(string.Format("{0}", "        </td>"));
                    }
                    break;
                case "TextArea":
                    genContent.Add(string.Format("{0}", "        <td class=\"col-md-2\">"));
                    genContent.Add(string.Format("            <span data-bind=\"html:{0}\"></span>", p.PropertyName));
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;
                case "CheckBox":
                    genContent.Add(string.Format("{0}", "        <td class=\"col-md-1\">"));
                    genContent.Add(string.Format("{0}", "            <div  data-bind = \"css:{checkedvalue:" + p.PropertyName + "()==true,noncheckedvalue:" + p.PropertyName + "()==false }\" class=\"col-md-12\" ></div>"));
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;
                case "DateTimePicker":
                    genContent.Add(string.Format("{0}", "       <td class=\"col-md-1\">"));
                    genContent.Add(string.Format("            <span data-bind=\"html:{0}\"></span>", p.PropertyName + "F"));
                    genContent.Add(string.Format("{0}", "        </td>"));
                    break;

            }
        }
        private void ConvertToViewFormHTMLControl(List<string> genContent, PropertyGenInfo p, GenCodeInfo genInf)
        {

            switch (p.ControlType)
            {
                case "TextBox":
                    bool isstringtype = (p.SystemType == "String" || p.SystemType == "string" || p.SystemType == "Guid");
                    if (p.PropertyName.Contains("ID"))
                    {
                        genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                        genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                        genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                        genContent.Add(string.Format("{0}", "                <span  data-bind=\"html:" + p.PropertyName.Replace("ID", "") + "Name\" ></span>"));
                        genContent.Add(string.Format("{0}", "            </div>"));
                        genContent.Add(string.Format("{0}", "        </div>"));
                    }
                    else
                    {
                        genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                        genContent.Add(string.Format("{0}", "            <label class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                        genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                        if (isstringtype)
                        {
                            genContent.Add(string.Format("{0}", "                <span data-bind=\"html:" + p.PropertyName + "\" ></span>"));
                        }
                        else
                        {
                            genContent.Add(string.Format("{0}", "                <span data-bind=\"html:" + p.PropertyName + "\" ></span>"));
                        }
                        genContent.Add(string.Format("{0}", "            </div>"));
                        genContent.Add(string.Format("{0}", "        </div>"));
                    }
                    break;
                case "TextArea":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "                <span data-bind=\"html:" + p.PropertyName + "\" ></span>"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;
                case "CheckBox":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "                <div  data-bind = \"css:{checkedvalue:" + p.PropertyName + "()==true,noncheckedvalue:" + p.PropertyName + "()==false }\" class=\"col-md-12\" ></div>"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;
                case "DateTimePicker":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "               <span data-bind=\"html:{0}\"></span>", p.PropertyName + "F"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;

            }
        }
        private void ConvertToEditFormHTMLControl(List<string> genContent, PropertyGenInfo p, GenCodeInfo genInf)
        {
            switch (p.ControlType)
            {
                case "TextBox":
                    bool isstringtype = (p.SystemType == "String" || p.SystemType == "string" || p.SystemType == "Guid");
                    if (p.PropertyName.Contains("ID"))
                    {
                        genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                        genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                        genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                        genContent.Add(string.Format("{0}", "                <select id=\"ctrl" + p.PropertyName + "\" data-bind=\"options: $parent." + p.PropertyName.Replace("ID", "") + "sVM().Arr" + p.PropertyName.Replace("ID", "") + ",optionsText:'Name',optionsValue:'" + p.PropertyName + "',optionsCaption:'@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.PlaceHolder_Table_" + p.PropertyName + "',value:" + p.PropertyName + "\" class=\"form-control col-md-12\" ></select>"));
                        genContent.Add(string.Format("{0}", "            </div>"));
                        genContent.Add(string.Format("{0}", "        </div>"));
                    }
                    else
                    {
                        genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                        genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                        genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));

                        if (isstringtype)
                        {
                            genContent.Add(string.Format("{0}", "                <input id=\"ctrl" + p.PropertyName + "\" type=\"text\" data-bind=\"value:" + p.PropertyName + "\" class=\"form-control col-md-12\" />"));
                        }
                        else
                        {
                            genContent.Add(string.Format("{0}", "                <input id=\"ctrl" + p.PropertyName + "\" type=\"text\" data-bind = \"value:" + p.PropertyName + ",currencyMaskOptions:{symbol: '',roundToDecimalPlace:0}\" class=\"form-control col-md-12\" />"));
                        }

                        genContent.Add(string.Format("{0}", "            </div>"));
                        genContent.Add(string.Format("{0}", "        </div>"));
                    }
                    break;
                case "TextArea":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "                <textarea id=\"ctrl" + p.PropertyName + "\" type=\"text\" data-bind=\"value:" + p.PropertyName + "\" class=\"form-control col-md-12\" ></textarea>"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;
                case "CheckBox":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "                <input id=\"ctrl" + p.PropertyName + "\" type=\"checkbox\" data-bind=\"checked:" + p.PropertyName + "\" class=\"form-control col-md-12\" /></textarea>"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;
                case "DateTimePicker":
                    genContent.Add(string.Format("{0}", "        <div class=\"form-group\">"));
                    genContent.Add(string.Format("{0}", "            <label for=\"ctrl" + p.PropertyName + "\" class=\"col-md-2 control-label\">@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Label_Table_" + p.PropertyName + "</label>"));
                    genContent.Add(string.Format("{0}", "            <div class=\"col-md-10\">"));
                    genContent.Add(string.Format("{0}", "                <input id=\"ctrl" + p.PropertyName + "\" type=\"text\" data-bind = \"datepicker: " + p.PropertyName + ", datepickerOptions: { dateFormat: 'dd/mm/yy' }\"  class=\"form-control col-md-12\" /></textarea>"));
                    genContent.Add(string.Format("{0}", "            </div>"));
                    genContent.Add(string.Format("{0}", "        </div>"));
                    break;
            }
        }
        private List<string> MVC_AdminView(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            #region getcontent
            genContent.Add(string.Format("{0}", "@{"));
            genContent.Add(string.Format("{0}", "    Layout = \"~/Views/Shared/_MainLayout.cshtml\";"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section styleContent{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "<div class=\"row\">"));
            genContent.Add(string.Format("{0}", "    <div class=\"col-md-12\">"));
            genContent.Add(string.Format("        @Html.Partial(\"_ArrPage{0}\")", genInf.ModelName));
            genContent.Add(string.Format("{0}", "    </div>"));
            genContent.Add(string.Format("{0}", "</div>"));
            genContent.Add(string.Format("{0}", "@section popup{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section scripts{"));
            genContent.Add(string.Format("{0}", "    @Html.Partial(\"_AdminJScript\")"));
            genContent.Add(string.Format("{0}", "}"));
            #endregion
            return genContent;
        }
        private List<string> MVC_ModelJScipt(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));
            #region getcontent
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            #region Init Row Model
            genContent.Add(string.Format("{0}", "        var " + genInf.ModelName + " = function ("));

            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("{0}", "                     " + p.PropertyName.ToLower() + ","));
                }
                else
                {
                    genContent.Add(string.Format("                     {0}", p.PropertyName.ToLower()));
                }
            }
            genContent.Add(string.Format("{0}", "                         ) {"));
            genContent.Add(string.Format("{0}", "            var self = this;"));

            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "long":
                    case "long?":
                    case "int":
                    case "int?":
                    case "bool":
                    case "bool?":
                    case "decimal?":
                    case "decimal":
                    case "double":
                    case "double?":
                        genContent.Add(string.Format("            self.{0} = ko.observable({1});", p.PropertyName, p.PropertyName.ToLower()));
                        break;
                    case "datetime?":
                    case "datetime":
                        genContent.Add(string.Format("            self.{0} = ko.observable(parseJsonDate({1}));", p.PropertyName, p.PropertyName.ToLower()));
                        genContent.Add(string.Format("            self.{0} = ko.observable(parseJsonDateF({1}));", p.PropertyName + "F", p.PropertyName.ToLower()));
                        break;
                    case "guid?":
                    case "guid":
                    case "string":
                        genContent.Add(string.Format("            self.{0} = ko.observable({1});", p.PropertyName, p.PropertyName.ToLower()));
                        break;
                }


            }
            genContent.Add(string.Format("{0}", "            self.GUID = ko.observable('');"));
            genContent.Add(string.Format("{0}", "            self.EditMode = ko.observable('');"));
            genContent.Add(string.Format("{0}", "            self.IsEdit = ko.observable(false);"));
            genContent.Add(string.Format("{0}", "            self.AllowEdit = ko.observable(true);"));
            genContent.Add(string.Format("{0}", "            self.AllowRemove = ko.observable(true);"));
            genContent.Add(string.Format("{0}", "            self.AllowSendTo = ko.observable(true);"));
            genContent.Add(string.Format("{0}", "            self.AllowPrint = ko.observable(true);"));
            genContent.Add(string.Format("{0}", "            self.AllowAddDetail = ko.observable(true);"));
            genContent.Add(string.Format("{0}", "            self.OldValue = ko.observable({});"));
            genContent.Add(string.Format("{0}", "            "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("            self.{0} = ko.observable(\"\");", p.PropertyName.Replace("ID", "") + "Name"));
            }
            genContent.Add(string.Format("{0}", "          };"));
            #endregion
            genContent.Add(string.Format("{0}", "</script>"));
            return genContent;
            #endregion

        }
        private List<string> MVC_ViewModelJScipt(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));
            var keyP = listP.FirstOrDefault(p => p.IsPrimaryKey);
            #region getcontent
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            #region SingleJSModel
            genContent.Add(string.Format("{0}", "var " + genInf.ModelName + "ViewModel = function(){"));
            #region main properties
            genContent.Add(string.Format("{0}", "var self = this;"));
            genContent.Add(string.Format("{0}", "    self.GetUrl = {"));
            genContent.Add(string.Format("{0}", "        get" + genInf.ModelName + " : \"@Url.Action(\"Get\",\"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "        save" + genInf.ModelName + "   : \"@Url.Action(\"Save\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "        remove" + genInf.ModelName + " : \"@Url.Action(\"Remove\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "        gotoMaster       : \"@Url.Action(\"Admin\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "        getGUIDString : \"@Url.Action(\"GetGUIDString\",\"" + genInf.ModelName + "\")\""));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    self." + keyP.PropertyName + " = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "    self." + genInf.ModelName + " = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "    self.Transition = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "    self.Processing = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "    self.FFInit = ko.observable(null);"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "    self." + p.PropertyName.Replace("ID", "") + "sVM = ko.observable(null);"));
            }
            #endregion
            #region InitData and ViewModel
            genContent.Add(string.Format("{0}", "    self.InitModel = function(transition,processing){"));
            genContent.Add(string.Format("{0}", "        self.Transition = transition;"));
            genContent.Add(string.Format("{0}", "        self.Processing = processing;"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    self.SetFFInit = function(ffinit){"));
            genContent.Add(string.Format("{0}", "        self.FFInit(ffinit);"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    self.Set" + keyP.PropertyName + " = function(id){"));
            genContent.Add(string.Format("{0}", "        self." + keyP.PropertyName + "(id);"));
            genContent.Add(string.Format("{0}", "    }"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "    self.Set" + p.PropertyName.Replace("ID", "") + "sVM = function(" + p.PropertyName.Replace("ID", "").ToLower() + "svm){"));
                genContent.Add(string.Format("{0}", "        self." + p.PropertyName.Replace("ID", "") + "sVM = " + p.PropertyName.Replace("ID", "").ToLower() + "svm;"));
                genContent.Add(string.Format("{0}", "    };"));
            }

            genContent.Add(string.Format("{0}", "    self.InitData =function(){"));
            genContent.Add(string.Format("{0}", "        Init" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "    };"));
            #endregion
            #region Event
            genContent.Add(string.Format("{0}", "            self.startEdit" + genInf.ModelName + " = function(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                {0}.OldValue(ko.toJS({0}));", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("                {0}.IsEdit(true);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             }"));

            genContent.Add(string.Format("{0}", "             self.finishEdit" + genInf.ModelName + " = function(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                Save{0}({1});", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             }"));

            genContent.Add(string.Format("{0}", "             self.cancelEdit" + genInf.ModelName + " = function(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                Reset{0}({1});", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("                {0}.IsEdit(false);", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             }"));

            genContent.Add(string.Format("{0}", "             self.remove" + genInf.ModelName + " = function(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                 Delete{0}({1});", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             };"));

            genContent.Add(string.Format("{0}", "             self.complete" + genInf.ModelName + " = function(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                 Complete{0}({1});", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.goBackToHome = function(){"));
            genContent.Add(string.Format("{0}", "                 window.location.href = self.GetUrl.gotoMaster;"));
            genContent.Add(string.Format("{0}", "             };"));
            #endregion
            #region Function
            #region Init

            genContent.Add(string.Format("{0}", "            function Init" + genInf.ModelName + "(){"));
            genContent.Add(string.Format("               self.Processing().SetProcessing(\"{0}\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "               if(self." + genInf.ModelName + "ID()>0){"));
            genContent.Add(string.Format("{0}", "                  $.post(self.GetUrl.get" + genInf.ModelName + ",{id:self." + genInf.ModelName + "ID()},FinishInit" + genInf.ModelName + ");"));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("{0}", "                  $.post(self.GetUrl.getGUIDString,{},FinishInit" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "               }"));
            genContent.Add(string.Format("{0}", "            };"));

            genContent.Add(string.Format("{0}", "            function FinishInit" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("                    self.{0}(self.ConvertDataTo{0}(data.{0}));", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 if (self.FFInit() != null) self.FFInit()();"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            }"));

            genContent.Add(string.Format("{0}", "            function FinishInit" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("                         var new{0} = new {0}(", genInf.ModelName));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                            );"));
            genContent.Add(string.Format("                        new{0}.GUID(data.GUIDString);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}(new{0});", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().AllowEdit(true);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().AllowRemove(true);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().AllowSendTo(false);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().AllowPrint(false);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().AllowAddDetail(true);", genInf.ModelName));
            genContent.Add(string.Format("                        self.{0}().IsEdit(true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                  }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "            }"));
            #endregion
            #region Save

            genContent.Add(string.Format("{0}", "            function Save" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("{0}", "                if(Validate" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + " )){"));
            genContent.Add(string.Format("                  self.Processing().SetProcessing(\"{0}\",true);", genInf.ModelName));
            genContent.Add(string.Format("                  $.post(self.GetUrl.save{0},Convert{0}ToPostObject({1}),FinishSave{0});", genInf.ModelName, genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));

            genContent.Add(string.Format("{0}", "             function Validate" + genInf.ModelName + "(item){"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                if(item." + p.PropertyName + "()!=null){"));
                genContent.Add(string.Format("{0}", "                    if(self." + p.PropertyName.Replace("ID", "") + "sVM()!=null){"));
                genContent.Add(string.Format("{0}", "                        var " + p.PropertyName.Replace("ID", "").ToLower() + " = ko.utils.arrayFirst(self." + p.PropertyName.Replace("ID", "") + "sVM().Arr" + p.PropertyName.Replace("ID", "") + "(),function(" + p.PropertyName.Replace("ID", "").ToLower() + "1){"));
                genContent.Add(string.Format("{0}", "                            return " + p.PropertyName.Replace("ID", "").ToLower() + "1." + p.PropertyName + "() == item." + p.PropertyName + "();"));
                genContent.Add(string.Format("{0}", "                        });"));
                genContent.Add(string.Format("{0}", "                        if(" + p.PropertyName.Replace("ID", "").ToLower() + "!=null){"));
                genContent.Add(string.Format("{0}", "                            item." + p.PropertyName.Replace("ID", "") + "Name(" + p.PropertyName.Replace("ID", "").ToLower() + ".Name());"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                    }"));
                genContent.Add(string.Format("{0}", "                 }"));
                genContent.Add(string.Format("{0}", "                 else{"));
                genContent.Add(string.Format("{0}", "                    item." + p.PropertyName.Replace("ID", "") + "Name(\"\");"));
                genContent.Add(string.Format("{0}", "                 }"));
            }
            genContent.Add(string.Format("{0}", "                return true;"));
            genContent.Add(string.Format("{0}", "             }"));

            genContent.Add(string.Format("{0}", "             function FinishSave" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "datetime?":
                    case "datetime":
                        genContent.Add(string.Format("                     self.{0}().{1}F(parseJsonDateF(data.{0}.{1})); ", genInf.ModelName, p.PropertyName));
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("                     self.{0}().{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("                     self.{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("                     self.{0}().IsEdit(false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 }else{"));
            genContent.Add(string.Format("{0}", "                    bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_SaveFail\");"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            #endregion
            #region  Delete
            genContent.Add(string.Format("{0}", "             function Delete" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 bootbox.confirm(\"<b>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_ConfirmDelete</b>\","));
            genContent.Add(string.Format("{0}", "                                  function(result){"));
            genContent.Add(string.Format("{0}", "                            if(result){"));
            genContent.Add(string.Format("{0}", "                                if(" + genInf.ModelName.ToLower() + " != undefined){"));
            genContent.Add(string.Format("{0}", "                                    $.post(self.GetUrl.remove" + genInf.ModelName + ",{id: self." + genInf.ModelName + "()." + genInf.ModelName + "ID()},"));
            genContent.Add(string.Format("{0}", "                                         function(data){"));
            genContent.Add(string.Format("                                            self.Processing().SetProcessing(\"{0}\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                            if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                                                window.location.href = self.GetUrl.gotoMaster;"));
            genContent.Add(string.Format("{0}", "                                            }else{"));
            genContent.Add(string.Format("{0}", "                                                bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_NotConnectAPI\");"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("{0}", "                                        });"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        }"));
            genContent.Add(string.Format("{0}", "                 );"));
            genContent.Add(string.Format("{0}", "             }"));
            #endregion
            #region Complete
            genContent.Add(string.Format("{0}", "            function Complete" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("{0}", "            }"));
            #endregion
            #region ConvertToPostObject
            genContent.Add(string.Format("{0}", "             function Convert" + genInf.ModelName + "ToPostObject(" + genInf.ModelName.ToLower() + "){"));
            genContent.Add(string.Format("{0}", "                var postObject = {"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower() != "datetime" || p.SystemType.ToLower() != "datetime?")
                {
                    genContent.Add(string.Format("                                 {0}:{1}.{0}(),", p.PropertyName, genInf.ModelName.ToLower()));
                }
                else
                {
                    genContent.Add(string.Format("                                 {0}:\"\",", p.PropertyName));
                }
            }
            genContent.Add(string.Format("                                 {0}:{1}.{0}()", "GUID", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "                                };"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower().Contains("datetime"))
                {
                    genContent.Add(string.Format("               if({1}.{0}()!==null)", p.PropertyName, genInf.ModelName.ToLower()));
                    genContent.Add(string.Format("{0}", "                {"));
                    genContent.Add(string.Format("                   postObject.{0} = parseJsonDateS({1}.{0}());", p.PropertyName, genInf.ModelName.ToLower()));
                    genContent.Add(string.Format("{0}", "                }"));
                }
            }
            genContent.Add(string.Format("{0}", "                return postObject;"));
            genContent.Add(string.Format("{0}", "             }"));
            #endregion
            #region Reset
            genContent.Add(string.Format("{0}", "             function Reset" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "){"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                 {1}.{0}({1}.OldValue().{0});", p.PropertyName, genInf.ModelName.ToLower()));
            }
            genContent.Add(string.Format("{0}", "             }"));
            #endregion
            #region ConvertDataToRowItem
            genContent.Add(string.Format("{0}", "             self.ConvertDataTo" + genInf.ModelName + " = function(dataItem){"));
            genContent.Add(string.Format("                    var {1} = new {0}(", genInf.ModelName, genInf.ModelName.ToLower()));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("                                                 dataItem.{0},", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                                 dataItem.{0}", p.PropertyName));
                }
            }
            genContent.Add(string.Format("{0}", "                                                 );"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                 " + genInf.ModelName.ToLower() + "." + p.PropertyName.Replace("ID", "") + "Name(dataItem." + p.PropertyName.Replace("ID", "") + "Name);"));
            }
            genContent.Add(string.Format("                    return {0};", genInf.ModelName.ToLower()));
            genContent.Add(string.Format("{0}", "             }"));
            #endregion
            #endregion
            genContent.Add(string.Format("{0}", "        };"));
            #endregion
            #region ArrayPagingJSModel
            /* genContent.Add(string.Format("{0}", "        var " + genInf.ModelName + "sPageViewModel = function () {"));
            genContent.Add(string.Format("{0}", "             var self = this;"));
            genContent.Add(string.Format("{0}", "             self.GetUrl = {     "));
            genContent.Add(string.Format("                getGUIDString      : \"@Url.Action(\"GetGUIDString\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                get{0}s        : \"@Url.Action(\"GetPage\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                save{0}        : \"@Url.Action(\"Save\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                remove{0}      : \"@Url.Action(\"Remove\", \"{0}\")\"", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.Processing = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Transition = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Paging = ko.observable(new ItemPaging(1, 20, 0));"));
            genContent.Add(string.Format("{0}", "             self.SearchCode = ko.observable(\"\");"));
            genContent.Add(string.Format("             self.Arr{0} = ko.observableArray([]);", genInf.ModelName));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("            self.{0}sVM = ko.observable(null);", p.PropertyName.Replace("ID", "")));
            }

            genContent.Add(string.Format("{0}", "             //------- Init data for view model----------------------------"));
            genContent.Add(string.Format("{0}", "            self.InitModel = function(transition,processing){"));
            genContent.Add(string.Format("{0}", "                self.Transition = transition;"));
            genContent.Add(string.Format("{0}", "                self.Processing = processing;"));
            genContent.Add(string.Format("{0}", "             };"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self.Set" + p.PropertyName.Replace("ID", "") + "sVM = function(" + p.PropertyName.Replace("ID", "").ToLower() + "svm){"));
                genContent.Add(string.Format("{0}", "                self." + p.PropertyName.Replace("ID", "") + "sVM = " + p.PropertyName.Replace("ID", "").ToLower() + "svm;"));
                genContent.Add(string.Format("{0}", "            };"));
            }
            genContent.Add(string.Format("{0}", "             self.InitData = function () {"));
            genContent.Add(string.Format("                Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Paging and Searching -----------------------------//"));
            genContent.Add(string.Format("{0}", "             self.search" + genInf.ModelName + "s = function(){"));
            genContent.Add(string.Format("                 Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoPage = function(page){"));
            genContent.Add(string.Format("{0}", "                if(page!=self.Paging().PageIndex()){"));
            genContent.Add(string.Format("{0}", "                    self.Paging().PageIndex(page);"));
            genContent.Add(string.Format("                    Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoNextPage = function(){"));
            genContent.Add(string.Format("{0}", "                if(self.Paging().PageIndex()<self.Paging().TotalPages()){"));
            genContent.Add(string.Format("{0}", "                    self.Paging().PageIndex(self.Paging().PageIndex()+1);"));
            genContent.Add(string.Format("                    Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoPrevPage = function(){"));
            genContent.Add(string.Format("{0}", "                 if(self.Paging().PageIndex()>1){"));
            genContent.Add(string.Format("{0}", "                     self.Paging().PageIndex(self.Paging().PageIndex()-1);"));
            genContent.Add(string.Format("                     Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Item -----------------------------//"));
            genContent.Add(string.Format("{0}", "             //--------------- Model Event ----------//"));
            genContent.Add(string.Format("{0}", "             self.startAdd" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 $.post(self.GetUrl.getGUIDString,{},FinishInit" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.finishEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                Save{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.cancelEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "               if(item." + genInf.ModelName + "ID()==0){"));
            genContent.Add(string.Format("                  self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("                  Reset{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                  item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.remove" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                 Delete{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result ==\"@"+genInf.ProjectName+".Const.ResultType.Success\"){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                            );"));
            genContent.Add(string.Format("{0}", "                   item.GUID(data.GUIDString);"));
            genContent.Add(string.Format("{0}", "                   item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                   item.IsEdit(true);"));
            genContent.Add(string.Format("                   self.Arr{0}.push(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Init" + genInf.ModelName + "s(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 var postParam = SetGetParam();"));
            genContent.Add(string.Format("                 $.post(self.GetUrl.get{0}s,postParam,FinishInit{0}s);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "s(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result == \"@"+genInf.ProjectName+".Const.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                     var arrItem = ko.utils.arrayMap(data." + genInf.ModelName + "s,function(item){"));
            genContent.Add(string.Format("                       return self.ConvertDataTo{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("                    self.Arr{0}.removeAll();", genInf.ModelName));
            genContent.Add(string.Format("                    self.Arr{0}(arrItem);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    self.Paging().ResetPaging(data.PageIndex,data.PageSize,data.TotalCount);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Save" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                   if(Validate" + genInf.ModelName + "(item)){"));
            genContent.Add(string.Format("                       self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("                       var json = JSON.stringify(Convert{0}ToPostObject(item));", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                       $.ajax({"));
            genContent.Add(string.Format("                          url: self.GetUrl.save{0},", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                          type: \"POST\","));
            genContent.Add(string.Format("{0}", "                          contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                          cache: false,"));
            genContent.Add(string.Format("{0}", "                          data: json,"));
            genContent.Add(string.Format("                          success: FinishSave{0}", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      });"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "              } "));
            genContent.Add(string.Format("{0}", "             function FinishSave" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@"+genInf.ProjectName+".Const.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      var item;"));
            genContent.Add(string.Format("                      if(data.{0}ID > 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      {"));
            genContent.Add(string.Format("{0}", "                             item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("                                return item1.{0}ID() == data.{0}ID;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             });"));
            genContent.Add(string.Format("{0}", "                         }else"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("{0}", "                            item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("{0}", "                                return item1.GUID() == data.GUID;"));
            genContent.Add(string.Format("{0}", "                            });"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         if(item!=null)"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("                              if (item.{0}ID() == 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                              {"));
            genContent.Add(string.Format("{0}", "                                 self.Paging().ResetPaging(self.Paging().PageIndex(), self.Paging().PageSize(), self.Paging().TotalItems() + 1);"));
            genContent.Add(string.Format("{0}", "                              }"));
            genContent.Add(string.Format("                             item.{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "              }"));
            genContent.Add(string.Format("{0}", "             function Delete" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                 bootbox.confirm(\"<b>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_ConfirmDelete</b>\","));
            genContent.Add(string.Format("{0}", "                        function(result){"));
            genContent.Add(string.Format("{0}", "                            if(result){"));
            genContent.Add(string.Format("{0}", "                                if(item != undefined){"));
            genContent.Add(string.Format("                                    if(item.{0}ID()==0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    {"));
            genContent.Add(string.Format("                                      self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    $.post(self.GetUrl.remove" + genInf.ModelName + ",{id: item." + genInf.ModelName + "ID()},"));
            genContent.Add(string.Format("{0}", "                                         function(data){"));
            genContent.Add(string.Format("{0}", "                                            if(data.Result == \"@"+genInf.ProjectName+".Const.ResultType.Success\")"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                self.Arr" + genInf.ModelName + ".remove(function(item){return item." + genInf.ModelName + "ID() == data." + genInf.ModelName + "ID;});"));
            genContent.Add(string.Format("{0}", "                                                self.Paging().ResetPaging(self.Paging().PageIndex(), self.Paging().PageSize(), self.Paging().TotalItems() - 1);"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("{0}", "                                            else"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_NotConnectAPI\");"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                        });"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Reset" + genInf.ModelName + "(item){"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                 item.{0}(item.OldValue().{0});", p.PropertyName));
            }
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Convert" + genInf.ModelName + "ToPostObject(item){"));

            genContent.Add(string.Format("{0}", "                var postObject = {"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower() != "datetime" || p.SystemType.ToLower() != "datetime?")
                {
                    genContent.Add(string.Format("                                 {0}:item.{0}(),", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                 {0}:\"\",", p.PropertyName));
                }
            }
            genContent.Add(string.Format("                                 {0}:item.{0}()", "GUID"));
            genContent.Add(string.Format("{0}", "                                };"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower().Contains("datetime"))
                {
                    genContent.Add(string.Format("               if(item.{0}()!==null)", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                {"));
                    genContent.Add(string.Format("                   postObject.{0} = parseJsonDateS(item.{0}());", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                }"));
                }
            }
            genContent.Add(string.Format("{0}", "                return postObject;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.ConvertDataTo" + genInf.ModelName + " = function(dataItem){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("                                                 dataItem.{0},", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                                 dataItem.{0}", p.PropertyName));
                }
            }
            genContent.Add(string.Format("{0}", "                                                 );"));
            genContent.Add(string.Format("{0}", "                    return item;"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             function SetGetParam(){"));
            genContent.Add(string.Format("{0}", "                 var postParam = {"));
            genContent.Add(string.Format("{0}", "                    searchcode : self.SearchCode(),"));
            genContent.Add(string.Format("{0}", "                    pageindex: self.Paging().PageIndex(),"));
            genContent.Add(string.Format("{0}", "                    pagesize: self.Paging().PageSize()"));
            genContent.Add(string.Format("{0}", "                };"));
            genContent.Add(string.Format("{0}", "                return postParam;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Validate" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "               return true;"));
            genContent.Add(string.Format("{0}", "             } "));
            genContent.Add(string.Format("{0}", "             //--------------- End Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             //--------------- End For item -----------------------------//"));
            genContent.Add(string.Format("{0}", "        };"));*/
            #endregion
            #region ArrayJSModel
            /*genContent.Add(string.Format("{0}", "        var " + genInf.ModelName + "sViewModel = function () {"));
            genContent.Add(string.Format("{0}", "             var self = this;"));
            genContent.Add(string.Format("{0}", "             self.GetUrl = {     "));
            genContent.Add(string.Format("                getGUIDString      : \"@Url.Action(\"GetGUIDString\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                get{0}s        : \"@Url.Action(\"Gets\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                save{0}        : \"@Url.Action(\"Save\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                remove{0}      : \"@Url.Action(\"Remove\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                saveList{0}      : \"@Url.Action(\"SaveList\", \"{0}\")\"", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.Processing = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Transition = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.SearchCode = ko.observable(\"\");"));
            genContent.Add(string.Format("             self.Arr{0} = ko.observableArray([]);", genInf.ModelName));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("            self.{0}sVM = ko.observable(null);", p.PropertyName.Replace("ID", "")));
            }
            genContent.Add(string.Format("{0}", "             //------- Init data for view model----------------------------"));
            genContent.Add(string.Format("{0}", "            self.InitModel = function(transition,processing){"));
            genContent.Add(string.Format("{0}", "                self.Transition = transition;"));
            genContent.Add(string.Format("{0}", "                self.Processing = processing;"));
            genContent.Add(string.Format("{0}", "             };"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self.Set" + p.PropertyName.Replace("ID", "") + "sVM = function(" + p.PropertyName.Replace("ID", "").ToLower() + "svm){"));
                genContent.Add(string.Format("{0}", "                self." + p.PropertyName.Replace("ID", "") + "sVM = " + p.PropertyName.Replace("ID", "").ToLower() + "svm;"));
                genContent.Add(string.Format("{0}", "            };"));
            }
            genContent.Add(string.Format("{0}", "             self.InitData = function () {"));
            genContent.Add(string.Format("                Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Searching -----------------------------//"));
            genContent.Add(string.Format("{0}", "             self.search" + genInf.ModelName + "s = function(){"));
            genContent.Add(string.Format("                 Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Item -----------------------------//"));
            genContent.Add(string.Format("{0}", "             //--------------- Model Event ----------//"));
            genContent.Add(string.Format("{0}", "             self.startAdd" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 $.post(self.GetUrl.getGUIDString,{},FinishInit" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.finishEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                Save{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.cancelEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "               if(item." + genInf.ModelName + "ID()==0){"));
            genContent.Add(string.Format("                  self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("                  Reset{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                  item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.remove" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                 Delete{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result ==\"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                            );"));
            genContent.Add(string.Format("{0}", "                   item.GUID(data.GUIDString);"));
            genContent.Add(string.Format("{0}", "                   item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                   item.IsEdit(true);"));
            genContent.Add(string.Format("                   self.Arr{0}.push(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Init" + genInf.ModelName + "s(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 var postParam = SetGetParam();"));
            genContent.Add(string.Format("                 $.post(self.GetUrl.get{0}s,postParam,FinishInit{0}s);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "s(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                     var arrItem = ko.utils.arrayMap(data." + genInf.ModelName + "s,function(item){"));
            genContent.Add(string.Format("                       return self.ConvertDataTo{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("                    self.Arr{0}.removeAll();", genInf.ModelName));
            genContent.Add(string.Format("                    self.Arr{0}(arrItem);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Save" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                   if(Validate" + genInf.ModelName + "(item)){"));
            genContent.Add(string.Format("                       self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("                       var json = JSON.stringify(Convert{0}ToPostObject(item));", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                       $.ajax({"));
            genContent.Add(string.Format("                          url: self.GetUrl.save{0},", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                          type: \"POST\","));
            genContent.Add(string.Format("{0}", "                          contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                          cache: false,"));
            genContent.Add(string.Format("{0}", "                          data: json,"));
            genContent.Add(string.Format("                          success: FinishSave{0}", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      });"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "              } "));
            genContent.Add(string.Format("{0}", "             function FinishSave" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      var item;"));
            genContent.Add(string.Format("                      if(data.{0}ID > 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      {"));
            genContent.Add(string.Format("{0}", "                             item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("                                return item1.{0}ID() == data.{0}ID;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             });"));
            genContent.Add(string.Format("{0}", "                         }else"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("{0}", "                            item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("{0}", "                                return item1.GUID() == data.GUID;"));
            genContent.Add(string.Format("{0}", "                            });"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         if(item!=null)"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("                             item.{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "              }"));
            genContent.Add(string.Format("{0}", "             function Delete" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                 bootbox.confirm(\"<b>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_ConfirmDelete</b>\","));
            genContent.Add(string.Format("{0}", "                        function(result){"));
            genContent.Add(string.Format("{0}", "                            if(result){"));
            genContent.Add(string.Format("{0}", "                                if(item != undefined){"));
            genContent.Add(string.Format("                                    if(item.{0}ID()==0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    {"));
            genContent.Add(string.Format("                                      self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    $.post(self.GetUrl.remove" + genInf.ModelName + ",{id: item." + genInf.ModelName + "ID()},"));
            genContent.Add(string.Format("{0}", "                                         function(data){"));
            genContent.Add(string.Format("{0}", "                                            if(data.Result == \"@Base.ViewModel.ResultType.Success\")"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                self.Arr" + genInf.ModelName + ".remove(function(item){return item." + genInf.ModelName + "ID() == data." + genInf.ModelName + "ID;});"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("{0}", "                                            else"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_NotConnectAPI\");"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("                                            self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                        });"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Reset" + genInf.ModelName + "(item){"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                 item.{0}(item.OldValue().{0});", p.PropertyName));
            }
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Convert" + genInf.ModelName + "ToPostObject(item){"));

            genContent.Add(string.Format("{0}", "                var postObject = {"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower() != "datetime" || p.SystemType.ToLower() != "datetime?")
                {
                    genContent.Add(string.Format("                                 {0}:item.{0}(),", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                 {0}:\"\",", p.PropertyName));
                }
            }
            genContent.Add(string.Format("                                 {0}:item.{0}()", "GUID"));
            genContent.Add(string.Format("{0}", "                                };"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower().Contains("datetime"))
                {
                    genContent.Add(string.Format("               if(item.{0}()!==null)", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                {"));
                    genContent.Add(string.Format("                   postObject.{0} = parseJsonDateS(item.{0}());", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                }"));
                }
            }
            genContent.Add(string.Format("{0}", "                return postObject;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.ConvertDataTo" + genInf.ModelName + " = function(dataItem){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("                                                 dataItem.{0},", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                                 dataItem.{0}", p.PropertyName));
                }
            }
            genContent.Add(string.Format("{0}", "                                                 );"));
            genContent.Add(string.Format("{0}", "                    return item;"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             function SetGetParam(){"));
            genContent.Add(string.Format("{0}", "                 var postParam = {"));
            genContent.Add(string.Format("{0}", "                    searchcode : self.SearchCode()"));
            genContent.Add(string.Format("{0}", "                };"));
            genContent.Add(string.Format("{0}", "                return postParam;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Validate" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "               return true;"));
            genContent.Add(string.Format("{0}", "             } "));
            #region finisheditlist
            genContent.Add(string.Format("{0}", "             self.finishEditList" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                 SaveList" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            #endregion
            #region SaveList
            genContent.Add(string.Format("{0}", "             function SaveList" + genInf.ModelName + "(){"));
            genContent.Add(string.Format("{0}", "                 var postArray = new Array();"));
            genContent.Add(string.Format("{0}", "                 ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(" + genInf.ModelName.ToLower() + "1){"));
            genContent.Add(string.Format("{0}", "                    if(Validate" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "1)){"));
            genContent.Add(string.Format("{0}", "                       postArray.push(ko.toJS(" + genInf.ModelName.ToLower() + "1));"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                 });"));
            genContent.Add(string.Format("{0}", "                 self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",true);"));
            genContent.Add(string.Format("{0}", "                 var json = JSON.stringify(postArray);"));
            genContent.Add(string.Format("{0}", "                 $.ajax({"));
            genContent.Add(string.Format("{0}", "                    url: self.GetUrl.saveList" + genInf.ModelName + ","));
            genContent.Add(string.Format("{0}", "                    type: \"POST\","));
            genContent.Add(string.Format("{0}", "                    contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                    cache: false,"));
            genContent.Add(string.Format("{0}", "                    data: json,"));
            genContent.Add(string.Format("{0}", "                    success: FinishSaveList" + genInf.ModelName));
            genContent.Add(string.Format("{0}", "                });"));
            genContent.Add(string.Format("{0}", "              } "));
            #endregion
            #region FinishSaveList
            genContent.Add(string.Format("{0}", "              function FinishSaveList" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                  if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                         if(item." + genInf.ModelName + "ID() == 0){"));
            genContent.Add(string.Format("{0}", "                             var dataItem = ko.utils.arrayFirst(data." + genInf.ModelName + "s,function(item1){"));
            genContent.Add(string.Format("{0}", "                                 return item1.GUID == item.GUID();"));
            genContent.Add(string.Format("{0}", "                              });"));
            genContent.Add(string.Format("{0}", "                              if(dataItem!=null){"));
            genContent.Add(string.Format("{0}", "                                 item." + genInf.ModelName + "ID(dataItem." + genInf.ModelName + "ID);"));
            genContent.Add(string.Format("{0}", "                              }"));
            genContent.Add(string.Format("{0}", "                          }else{"));
            genContent.Add(string.Format("{0}", "                                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                          }"));
            genContent.Add(string.Format("{0}", "                     });"));
            genContent.Add(string.Format("{0}", "                  }"));
            genContent.Add(string.Format("{0}", "                  self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",false);"));
            genContent.Add(string.Format("{0}", "               }"));
            #endregion
            genContent.Add(string.Format("{0}", "             //--------------- End Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             //--------------- End For item -----------------------------//"));
            genContent.Add(string.Format("{0}", "        };"));*/
            #endregion
            genContent.Add(string.Format("{0}", "</script>"));
            return genContent;
            #endregion

        }
        private List<string> MVC_PageViewModelJScipt(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));
            var keyP = listP.Single(u => u.IsPrimaryKey);
            #region getcontent
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            #region ArrayPagingJSModel
            genContent.Add(string.Format("{0}", "        var " + genInf.ModelName + "sPageViewModel = function () {"));
            genContent.Add(string.Format("{0}", "             var self = this;"));
            genContent.Add(string.Format("{0}", "             self.GetUrl = {     "));
            genContent.Add(string.Format("                getGUIDString      : \"@Url.Action(\"GetGUIDString\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                get{0}s        : \"@Url.Action(\"GetPage\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                save{0}        : \"@Url.Action(\"Save\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                remove{0}      : \"@Url.Action(\"Remove\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 gotoDetail         : \"@Url.Action(\"Detail\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "                 gotoEdit           : \"@Url.Action(\"Edit\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "                 gotoCreae          : \"@Url.Action(\"Create\", \"" + genInf.ModelName + "\")\""));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.Processing = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Transition = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.FFInit = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Paging = ko.observable(new ItemPaging(1, 20, 0));"));
            genContent.Add(string.Format("{0}", "             self.SearchCode = ko.observable(\"\");"));
            genContent.Add(string.Format("{0}", "             self.SortedField = ko.observable(null);"));
            genContent.Add(string.Format("             self.Arr{0} = ko.observableArray([]);", genInf.ModelName));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("            self.{0}sVM = ko.observable(null);", p.PropertyName.Replace("ID", "")));
            }

            genContent.Add(string.Format("{0}", "             //------- Init data for view model----------------------------"));
            genContent.Add(string.Format("{0}", "            self.InitModel = function(transition,processing){"));
            genContent.Add(string.Format("{0}", "                self.Transition = transition;"));
            genContent.Add(string.Format("{0}", "                self.Processing = processing;"));
            genContent.Add(string.Format("{0}", "             };"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self.Set" + p.PropertyName.Replace("ID", "") + "sVM = function(" + p.PropertyName.Replace("ID", "").ToLower() + "svm){"));
                genContent.Add(string.Format("{0}", "                self." + p.PropertyName.Replace("ID", "") + "sVM = " + p.PropertyName.Replace("ID", "").ToLower() + "svm;"));
                genContent.Add(string.Format("{0}", "            };"));
            }
            genContent.Add(string.Format("{0}", "             self.InitData = function () {"));
            genContent.Add(string.Format("                Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.SetFFInit = function(ffinit){"));
            genContent.Add(string.Format("{0}", "                 self.FFInit(ffinit);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Paging and Searching -----------------------------//"));
            genContent.Add(string.Format("{0}", "             self.search" + genInf.ModelName + "s = function(){"));
            genContent.Add(string.Format("                 Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoPage = function(page){"));
            genContent.Add(string.Format("{0}", "                if(page!=self.Paging().PageIndex()){"));
            genContent.Add(string.Format("{0}", "                    self.Paging().PageIndex(page);"));
            genContent.Add(string.Format("                    Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoNextPage = function(){"));
            genContent.Add(string.Format("{0}", "                if(self.Paging().PageIndex()<self.Paging().TotalPages()){"));
            genContent.Add(string.Format("{0}", "                    self.Paging().PageIndex(self.Paging().PageIndex()+1);"));
            genContent.Add(string.Format("                    Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.gotoPrevPage = function(){"));
            genContent.Add(string.Format("{0}", "                 if(self.Paging().PageIndex()>1){"));
            genContent.Add(string.Format("{0}", "                     self.Paging().PageIndex(self.Paging().PageIndex()-1);"));
            genContent.Add(string.Format("                     Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "              self.gotoDetail = function(item){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoDetail+\"/\"+item." + keyP.PropertyName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "              self.gotoCreate = function(){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoCreate;"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "              self.gotoEdit = function(item){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoEdit+\"/\"+item." + keyP.PropertyName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Item -----------------------------//"));
            genContent.Add(string.Format("{0}", "             //--------------- Model Event ----------//"));
            genContent.Add(string.Format("{0}", "             self.startAdd" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 $.post(self.GetUrl.getGUIDString,{},FinishInit" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.finishEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                Save{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.cancelEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "               if(item." + genInf.ModelName + "ID()==0){"));
            genContent.Add(string.Format("                  self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("                  Reset{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                  item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.remove" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                 Delete{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startSort = function(field){"));
            genContent.Add(string.Format("{0}", "                 if(self.SortedField()!=null){"));
            genContent.Add(string.Format("{0}", "                     if(self.SortedField().Name()==field){"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Direction(!self.SortedField().Direction());"));
            genContent.Add(string.Format("{0}", "                     }else{"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Name(field);"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Direction(false);"));
            genContent.Add(string.Format("{0}", "                     }"));
            genContent.Add(string.Format("{0}", "                 }else{"));
            genContent.Add(string.Format("{0}", "                     self.SortedField(new SortField(field,false));"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 self.Paging().PageIndex(1);"));
            genContent.Add(string.Format("{0}", "                 Init" + genInf.ModelName + "s();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "             //--------------- Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result ==\"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "guid?":
                    case "guid":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                            );"));
            genContent.Add(string.Format("{0}", "                   item.GUID(data.GUIDString);"));
            genContent.Add(string.Format("{0}", "                   item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                   item.IsEdit(true);"));
            genContent.Add(string.Format("                   self.Arr{0}.push(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Init" + genInf.ModelName + "s(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 var postParam = SetGetParam();"));
            genContent.Add(string.Format("                 $.post(self.GetUrl.get{0}s,postParam,FinishInit{0}s);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "s(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                     var arrItem = ko.utils.arrayMap(data." + genInf.ModelName + "s,function(item){"));
            genContent.Add(string.Format("                       return self.ConvertDataTo{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("                    self.Arr{0}.removeAll();", genInf.ModelName));
            genContent.Add(string.Format("                    self.Arr{0}(arrItem);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    self.Paging().ResetPaging(data.PageIndex,data.PageSize,data.TotalCount);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            //function run after init data"));
            genContent.Add(string.Format("{0}", "            if (self.FFInit() != null) self.FFInit()();"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Save" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                   if(Validate" + genInf.ModelName + "(item)){"));
            genContent.Add(string.Format("                       self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("                       var json = JSON.stringify(Convert{0}ToPostObject(item));", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                       $.ajax({"));
            genContent.Add(string.Format("                          url: self.GetUrl.save{0},", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                          type: \"POST\","));
            genContent.Add(string.Format("{0}", "                          contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                          cache: false,"));
            genContent.Add(string.Format("{0}", "                          data: json,"));
            genContent.Add(string.Format("                          success: FinishSave{0}", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      });"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "              } "));
            genContent.Add(string.Format("{0}", "             function FinishSave" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      var item;"));
            genContent.Add(string.Format("                      if(data.{0}ID > 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      {"));
            genContent.Add(string.Format("{0}", "                             item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("                                return item1.{0}ID() == data.{0}ID;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             });"));
            genContent.Add(string.Format("{0}", "                         }else"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("{0}", "                            item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("{0}", "                                return item1.GUID() == data.GUID;"));
            genContent.Add(string.Format("{0}", "                            });"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         if(item!=null)"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("                              if (item.{0}ID() == 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                              {"));
            genContent.Add(string.Format("{0}", "                                 self.Paging().ResetPaging(self.Paging().PageIndex(), self.Paging().PageSize(), self.Paging().TotalItems() + 1);"));
            genContent.Add(string.Format("{0}", "                              }"));
            genContent.Add(string.Format("                             item.{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "              }"));
            genContent.Add(string.Format("{0}", "             function Delete" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                 bootbox.confirm(\"<b>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_ConfirmDelete</b>\","));
            genContent.Add(string.Format("{0}", "                        function(result){"));
            genContent.Add(string.Format("{0}", "                            if(result){"));
            genContent.Add(string.Format("{0}", "                                if(item != undefined){"));
            genContent.Add(string.Format("                                    if(item.{0}ID()==0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    {"));
            genContent.Add(string.Format("                                      self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    $.post(self.GetUrl.remove" + genInf.ModelName + ",{id: item." + genInf.ModelName + "ID()},"));
            genContent.Add(string.Format("{0}", "                                         function(data){"));
            genContent.Add(string.Format("{0}", "                                            if(data.Result == \"@Base.ViewModel.ResultType.Success\")"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                self.Arr" + genInf.ModelName + ".remove(function(item){return item." + genInf.ModelName + "ID() == data." + genInf.ModelName + "ID;});"));
            genContent.Add(string.Format("{0}", "                                                self.Paging().ResetPaging(self.Paging().PageIndex(), self.Paging().PageSize(), self.Paging().TotalItems() - 1);"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("{0}", "                                            else"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_NotConnectAPI\");"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                        });"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Reset" + genInf.ModelName + "(item){"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                 item.{0}(item.OldValue().{0});", p.PropertyName));
            }
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Convert" + genInf.ModelName + "ToPostObject(item){"));

            genContent.Add(string.Format("{0}", "                var postObject = {"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower() != "datetime" || p.SystemType.ToLower() != "datetime?")
                {
                    genContent.Add(string.Format("                                 {0}:item.{0}(),", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                 {0}:\"\",", p.PropertyName));
                }
            }
            genContent.Add(string.Format("                                 {0}:item.{0}()", "GUID"));
            genContent.Add(string.Format("{0}", "                                };"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower().Contains("datetime"))
                {
                    genContent.Add(string.Format("               if(item.{0}()!==null)", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                {"));
                    genContent.Add(string.Format("                   postObject.{0} = parseJsonDateS(item.{0}());", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                }"));
                }
            }
            genContent.Add(string.Format("{0}", "                return postObject;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.ConvertDataTo" + genInf.ModelName + " = function(dataItem){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("                                                 dataItem.{0},", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                                 dataItem.{0}", p.PropertyName));
                }
            }
            genContent.Add(string.Format("{0}", "                                                 );"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                                           item." + p.PropertyName.Replace("ID", "") + "Name(dataItem." + p.PropertyName.Replace("ID", "") + "Name);"));
            }
            genContent.Add(string.Format("{0}", "                    return item;"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             function SetGetParam(){"));
            genContent.Add(string.Format("{0}", "                 var postParam = {"));
            genContent.Add(string.Format("{0}", "                    searchcode : self.SearchCode(),"));
            genContent.Add(string.Format("{0}", "                    pageindex: self.Paging().PageIndex(),"));
            genContent.Add(string.Format("{0}", "                    pagesize: self.Paging().PageSize()"));
            genContent.Add(string.Format("{0}", "                };"));
            genContent.Add(string.Format("{0}", "                 if(self.SortedField()!=null){"));
            genContent.Add(string.Format("{0}", "                         postParam = {"));
            genContent.Add(string.Format("{0}", "                             searchcode : self.SearchCode(),"));
            genContent.Add(string.Format("{0}", "                             sortedfield: self.SortedField().Name(),"));
            genContent.Add(string.Format("{0}", "                             sorteddirection: self.SortedField().Direction(),"));
            genContent.Add(string.Format("{0}", "                             pageindex: self.Paging().PageIndex(),"));
            genContent.Add(string.Format("{0}", "                             pagesize: self.Paging().PageSize()"));
            genContent.Add(string.Format("{0}", "                         };"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                return postParam;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Validate" + genInf.ModelName + "(item){"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                if(item." + p.PropertyName + "()!=null){"));
                genContent.Add(string.Format("{0}", "                    if(self." + p.PropertyName.Replace("ID", "") + "sVM()!=null){"));
                genContent.Add(string.Format("{0}", "                        var " + p.PropertyName.Replace("ID", "").ToLower() + " = ko.utils.arrayFirst(self." + p.PropertyName.Replace("ID", "") + "sVM().Arr" + p.PropertyName.Replace("ID", "") + "(),function(" + p.PropertyName.Replace("ID", "").ToLower() + "1){"));
                genContent.Add(string.Format("{0}", "                            return " + p.PropertyName.Replace("ID", "").ToLower() + "1." + p.PropertyName + "() == item." + p.PropertyName + "();"));
                genContent.Add(string.Format("{0}", "                        });"));
                genContent.Add(string.Format("{0}", "                        if(" + p.PropertyName.Replace("ID", "").ToLower() + "!=null){"));
                genContent.Add(string.Format("{0}", "                            item." + p.PropertyName.Replace("ID", "") + "Name(" + p.PropertyName.Replace("ID", "").ToLower() + ".Name());"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                    }"));
                genContent.Add(string.Format("{0}", "                 }"));
                genContent.Add(string.Format("{0}", "                 else{"));
                genContent.Add(string.Format("{0}", "                    item." + p.PropertyName.Replace("ID", "") + "Name(\"\");"));
                genContent.Add(string.Format("{0}", "                 }"));
            }
            genContent.Add(string.Format("{0}", "               return true;"));
            genContent.Add(string.Format("{0}", "             } "));
            genContent.Add(string.Format("{0}", "             //--------------- End Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             //--------------- End For item -----------------------------//"));
            genContent.Add(string.Format("{0}", "        };"));
            #endregion
            genContent.Add(string.Format("{0}", "</script>"));
            return genContent;
            #endregion

        }
        private List<string> MVC_ViewModelsJScipt(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var keyP = listProperty.FirstOrDefault(p => p.IsPrimaryKey);
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID")));

            #region getcontent
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            #region ArrayJSModel
            genContent.Add(string.Format("{0}", "        var " + genInf.ModelName + "sViewModel = function () {"));
            genContent.Add(string.Format("{0}", "             var self = this;"));
            genContent.Add(string.Format("{0}", "             self.GetUrl = {     "));
            genContent.Add(string.Format("                getGUIDString      : \"@Url.Action(\"GetGUIDString\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                get{0}s        : \"@Url.Action(\"Gets\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                save{0}        : \"@Url.Action(\"Save\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                remove{0}      : \"@Url.Action(\"Remove\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("                saveList{0}      : \"@Url.Action(\"SaveList\", \"{0}\")\",", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 gotoDetail         : \"@Url.Action(\"Detail\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "                 gotoEdit           : \"@Url.Action(\"Edit\", \"" + genInf.ModelName + "\")\","));
            genContent.Add(string.Format("{0}", "                 gotoCreate         : \"@Url.Action(\"Create\", \"" + genInf.ModelName + "\")\""));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.Processing = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.Transition = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.FFInit = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "             self.SearchCode = ko.observable(\"\");"));
            genContent.Add(string.Format("{0}", "             self.SortedField = ko.observable(null);"));

            genContent.Add(string.Format("             self.Arr{0} = ko.observableArray([]);", genInf.ModelName));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("            self.{0}sVM = ko.observable(null);", p.PropertyName.Replace("ID", "")));
            }
            genContent.Add(string.Format("{0}", "             //------- Init data for view model----------------------------"));
            genContent.Add(string.Format("{0}", "            self.InitModel = function(transition,processing){"));
            genContent.Add(string.Format("{0}", "                self.Transition = transition;"));
            genContent.Add(string.Format("{0}", "                self.Processing = processing;"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.SetFFInit = function(ffinit){"));
            genContent.Add(string.Format("{0}", "                 self.FFInit(ffinit);"));
            genContent.Add(string.Format("{0}", "             };"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self.Set" + p.PropertyName.Replace("ID", "") + "sVM = function(" + p.PropertyName.Replace("ID", "").ToLower() + "svm){"));
                genContent.Add(string.Format("{0}", "                self." + p.PropertyName.Replace("ID", "") + "sVM = " + p.PropertyName.Replace("ID", "").ToLower() + "svm;"));
                genContent.Add(string.Format("{0}", "            };"));
            }
            genContent.Add(string.Format("{0}", "             self.InitData = function () {"));
            genContent.Add(string.Format("                Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Searching -----------------------------//"));
            genContent.Add(string.Format("{0}", "             self.search" + genInf.ModelName + "s = function(){"));
            genContent.Add(string.Format("                 Init{0}s();", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             //--------------- For Item -----------------------------//"));
            genContent.Add(string.Format("{0}", "             //--------------- Model Event ----------//"));
            genContent.Add(string.Format("{0}", "             self.startAdd" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 $.post(self.GetUrl.getGUIDString,{},FinishInit" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.finishEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                Save{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.cancelEdit" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "               if(item." + genInf.ModelName + "ID()==0){"));
            genContent.Add(string.Format("                  self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("                  Reset{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                  item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.remove" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("                 Delete{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startSort = function(field){"));
            genContent.Add(string.Format("{0}", "                 if(self.SortedField()!=null){"));
            genContent.Add(string.Format("{0}", "                     if(self.SortedField().Name()==field){"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Direction(!self.SortedField().Direction());"));
            genContent.Add(string.Format("{0}", "                     }else{"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Name(field);"));
            genContent.Add(string.Format("{0}", "                         self.SortedField().Direction(false);"));
            genContent.Add(string.Format("{0}", "                     }"));
            genContent.Add(string.Format("{0}", "                 }else{"));
            genContent.Add(string.Format("{0}", "                     self.SortedField(new SortField(field,false));"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 Init" + genInf.ModelName + "s();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "              self.gotoDetail = function(item){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoDetail+\"/\"+item." + keyP.PropertyName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "              self.gotoCreate = function(){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoCreate;"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "              self.gotoEdit = function(item){"));
            genContent.Add(string.Format("{0}", "                  window.location.href = self.GetUrl.gotoEdit+\"/\"+item." + keyP.PropertyName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            genContent.Add(string.Format("{0}", "             //--------------- Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result ==\"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                            );"));
            genContent.Add(string.Format("{0}", "                   item.GUID(data.GUIDString);"));
            genContent.Add(string.Format("{0}", "                   item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                   item.IsEdit(true);"));
            genContent.Add(string.Format("                   self.Arr{0}.push(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Init" + genInf.ModelName + "s(){"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                 var postParam = SetGetParam();"));
            genContent.Add(string.Format("                 $.post(self.GetUrl.get{0}s,postParam,FinishInit{0}s);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function FinishInit" + genInf.ModelName + "s(data){"));
            genContent.Add(string.Format("{0}", "                if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                     var arrItem = ko.utils.arrayMap(data." + genInf.ModelName + "s,function(item){"));
            genContent.Add(string.Format("                       return self.ConvertDataTo{0}(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("                    self.Arr{0}.removeAll();", genInf.ModelName));
            genContent.Add(string.Format("                    self.Arr{0}(arrItem);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "             //function run after init data"));
            genContent.Add(string.Format("{0}", "             if (self.FFInit() != null) self.FFInit()();"));
            genContent.Add(string.Format("                self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Save" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                   if(Validate" + genInf.ModelName + "(item)){"));
            genContent.Add(string.Format("                       self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("                       var json = JSON.stringify(Convert{0}ToPostObject(item));", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                       $.ajax({"));
            genContent.Add(string.Format("                          url: self.GetUrl.save{0},", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                          type: \"POST\","));
            genContent.Add(string.Format("{0}", "                          contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                          cache: false,"));
            genContent.Add(string.Format("{0}", "                          data: json,"));
            genContent.Add(string.Format("                          success: FinishSave{0}", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      });"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "              } "));
            genContent.Add(string.Format("{0}", "             function FinishSave" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      var item;"));
            genContent.Add(string.Format("                      if(data.{0}ID > 0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                      {"));
            genContent.Add(string.Format("{0}", "                             item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("                                return item1.{0}ID() == data.{0}ID;", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             });"));
            genContent.Add(string.Format("{0}", "                         }else"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("{0}", "                            item = ko.utils.arrayFirst(self.Arr" + genInf.ModelName + "(),function(item1){"));
            genContent.Add(string.Format("{0}", "                                return item1.GUID() == data.GUID;"));
            genContent.Add(string.Format("{0}", "                            });"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         if(item!=null)"));
            genContent.Add(string.Format("{0}", "                         {"));
            genContent.Add(string.Format("                             item.{0}ID(data.{0}.{0}ID);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("                 self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "              }"));
            genContent.Add(string.Format("{0}", "             function Delete" + genInf.ModelName + "(item){"));
            genContent.Add(string.Format("{0}", "                 bootbox.confirm(\"<b>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_ConfirmDelete</b>\","));
            genContent.Add(string.Format("{0}", "                        function(result){"));
            genContent.Add(string.Format("{0}", "                            if(result){"));
            genContent.Add(string.Format("{0}", "                                if(item != undefined){"));
            genContent.Add(string.Format("                                    if(item.{0}ID()==0)", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    {"));
            genContent.Add(string.Format("                                      self.Arr{0}.remove(item);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    }"));
            genContent.Add(string.Format("                                    self.Processing().SetProcessing(\"{0}s\",true);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                    $.post(self.GetUrl.remove" + genInf.ModelName + ",{id: item." + genInf.ModelName + "ID()},"));
            genContent.Add(string.Format("{0}", "                                         function(data){"));
            genContent.Add(string.Format("{0}", "                                            if(data.Result == \"@Base.ViewModel.ResultType.Success\")"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                self.Arr" + genInf.ModelName + ".remove(function(item){return item." + genInf.ModelName + "ID() == data." + genInf.ModelName + "ID;});"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("{0}", "                                            else"));
            genContent.Add(string.Format("{0}", "                                            {"));
            genContent.Add(string.Format("{0}", "                                                bootbox.alert(\"@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Message_NotConnectAPI\");"));
            genContent.Add(string.Format("{0}", "                                            }"));
            genContent.Add(string.Format("                                            self.Processing().SetProcessing(\"{0}s\",false);", genInf.ModelName));
            genContent.Add(string.Format("{0}", "                                        });"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        });"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Reset" + genInf.ModelName + "(item){"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("                 item.{0}(item.OldValue().{0});", p.PropertyName));
            }
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Convert" + genInf.ModelName + "ToPostObject(item){"));

            genContent.Add(string.Format("{0}", "                var postObject = {"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower() != "datetime" || p.SystemType.ToLower() != "datetime?")
                {
                    genContent.Add(string.Format("                                 {0}:item.{0}(),", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                 {0}:\"\",", p.PropertyName));
                }
            }
            genContent.Add(string.Format("                                 {0}:item.{0}()", "GUID"));
            genContent.Add(string.Format("{0}", "                                };"));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (p.SystemType.ToLower().Contains("datetime"))
                {
                    genContent.Add(string.Format("               if(item.{0}()!==null)", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                {"));
                    genContent.Add(string.Format("                   postObject.{0} = parseJsonDateS(item.{0}());", p.PropertyName));
                    genContent.Add(string.Format("{0}", "                }"));
                }
            }
            genContent.Add(string.Format("{0}", "                return postObject;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             self.ConvertDataTo" + genInf.ModelName + " = function(dataItem){"));
            genContent.Add(string.Format("                    var item = new {0}(", genInf.ModelName));
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                if (i < listP.Count - 1)
                {
                    genContent.Add(string.Format("                                                 dataItem.{0},", p.PropertyName));
                }
                else
                {
                    genContent.Add(string.Format("                                                 dataItem.{0}", p.PropertyName));
                }
            }
            genContent.Add(string.Format("{0}", "                                                 );"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                    item." + p.PropertyName.Replace("ID", "") + "Name(dataItem." + p.PropertyName.Replace("ID", "") + "Name);"));
            }
            genContent.Add(string.Format("{0}", "                    return item;"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             function SetGetParam(){"));
            genContent.Add(string.Format("{0}", "                 var postParam = {"));
            genContent.Add(string.Format("{0}", "                    searchcode : self.SearchCode()"));
            genContent.Add(string.Format("{0}", "                 };"));
            genContent.Add(string.Format("{0}", "                 if(self.SortedField()!=null){"));
            genContent.Add(string.Format("{0}", "                         postParam = {"));
            genContent.Add(string.Format("{0}", "                             searchcode : self.SearchCode(),"));
            genContent.Add(string.Format("{0}", "                             sortedfield: self.SortedField().Name(),"));
            genContent.Add(string.Format("{0}", "                             sorteddirection: self.SortedField().Direction()"));
            genContent.Add(string.Format("{0}", "                         };"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 return postParam;"));
            genContent.Add(string.Format("{0}", "             }"));
            genContent.Add(string.Format("{0}", "             function Validate" + genInf.ModelName + "(item){"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "                if(item." + p.PropertyName + "()!=null){"));
                genContent.Add(string.Format("{0}", "                    if(self." + p.PropertyName.Replace("ID", "") + "sVM()!=null){"));
                genContent.Add(string.Format("{0}", "                        var " + p.PropertyName.Replace("ID", "").ToLower() + " = ko.utils.arrayFirst(self." + p.PropertyName.Replace("ID", "") + "sVM().Arr" + p.PropertyName.Replace("ID", "") + "(),function(" + p.PropertyName.Replace("ID", "").ToLower() + "1){"));
                genContent.Add(string.Format("{0}", "                            return " + p.PropertyName.Replace("ID", "").ToLower() + "1." + p.PropertyName + "() == item." + p.PropertyName + "();"));
                genContent.Add(string.Format("{0}", "                        });"));
                genContent.Add(string.Format("{0}", "                        if(" + p.PropertyName.Replace("ID", "").ToLower() + "!=null){"));
                genContent.Add(string.Format("{0}", "                            item." + p.PropertyName.Replace("ID", "") + "Name(" + p.PropertyName.Replace("ID", "").ToLower() + ".Name());"));
                genContent.Add(string.Format("{0}", "                        }"));
                genContent.Add(string.Format("{0}", "                    }"));
                genContent.Add(string.Format("{0}", "                 }"));
                genContent.Add(string.Format("{0}", "                 else{"));
                genContent.Add(string.Format("{0}", "                    item." + p.PropertyName.Replace("ID", "") + "Name(\"\");"));
                genContent.Add(string.Format("{0}", "                 }"));
            }
            genContent.Add(string.Format("{0}", "               return true;"));
            genContent.Add(string.Format("{0}", "             } "));
            /*
            #region finisheditlist
            genContent.Add(string.Format("{0}", "             self.finishEditList" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                 SaveList" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "              };"));
            #endregion
            #region SaveList
            genContent.Add(string.Format("{0}", "             function SaveList" + genInf.ModelName + "(){"));
            genContent.Add(string.Format("{0}", "                 var postArray = new Array();"));
            genContent.Add(string.Format("{0}", "                 ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(" + genInf.ModelName.ToLower() + "1){"));
            genContent.Add(string.Format("{0}", "                    if(Validate" + genInf.ModelName + "(" + genInf.ModelName.ToLower() + "1)){"));
            genContent.Add(string.Format("{0}", "                       postArray.push(ko.toJS(" + genInf.ModelName.ToLower() + "1));"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                 });"));
            genContent.Add(string.Format("{0}", "                 self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",true);"));
            genContent.Add(string.Format("{0}", "                 var json = JSON.stringify(postArray);"));
            genContent.Add(string.Format("{0}", "                 $.ajax({"));
            genContent.Add(string.Format("{0}", "                    url: self.GetUrl.saveList" + genInf.ModelName + ","));
            genContent.Add(string.Format("{0}", "                    type: \"POST\","));
            genContent.Add(string.Format("{0}", "                    contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                    cache: false,"));
            genContent.Add(string.Format("{0}", "                    data: json,"));
            genContent.Add(string.Format("{0}", "                    success: FinishSaveList" + genInf.ModelName));
            genContent.Add(string.Format("{0}", "                });"));
            genContent.Add(string.Format("{0}", "              } "));
            #endregion
            #region FinishSaveList
            genContent.Add(string.Format("{0}", "              function FinishSaveList" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                  if(data.Result == \"@"+genInf.ProjectName+".Const.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                      ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                         if(item." + genInf.ModelName + "ID() == 0){"));
            genContent.Add(string.Format("{0}", "                             var dataItem = ko.utils.arrayFirst(data." + genInf.ModelName + "s,function(item1){"));
            genContent.Add(string.Format("{0}", "                                 return item1.GUID == item.GUID();"));
            genContent.Add(string.Format("{0}", "                              });"));
            genContent.Add(string.Format("{0}", "                              if(dataItem!=null){"));
            genContent.Add(string.Format("{0}", "                                 item." + genInf.ModelName + "ID(dataItem." + genInf.ModelName + "ID);"));
            genContent.Add(string.Format("{0}", "                              }"));
            genContent.Add(string.Format("{0}", "                          }else{"));
            genContent.Add(string.Format("{0}", "                                             item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                          }"));
            genContent.Add(string.Format("{0}", "                     });"));
            genContent.Add(string.Format("{0}", "                  }"));
            genContent.Add(string.Format("{0}", "                  self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",false);"));
            genContent.Add(string.Format("{0}", "               }"));
            #endregion
            */
            #region new logic for edit list
            genContent.Add(string.Format("{0}", "            self.startAddList" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                 self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",true);"));
            genContent.Add(string.Format("{0}", "                 $.post(self.GetUrl.getGUIDString,{},FinishInitList" + genInf.ModelName + "GUID);"));
            genContent.Add(string.Format("{0}", "             };"));
            genContent.Add(string.Format("{0}", "             self.startEditList" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "                if(item." + genInf.ModelName + "ID()>0){"));
            genContent.Add(string.Format("{0}", "                    item.EditMode(\"Edit\");"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            self.finishEditList" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                if(Validate" + genInf.ModelName + "(item)){"));
            genContent.Add(string.Format("{0}", "                   item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            self.cancelEditList" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                Reset" + genInf.ModelName + "(item);"));
            genContent.Add(string.Format("{0}", "                item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            self.removeList" + genInf.ModelName + " = function(item){"));
            genContent.Add(string.Format("{0}", "                if(item." + genInf.ModelName + "ID()==0){"));
            genContent.Add(string.Format("{0}", "                   self.Arr" + genInf.ModelName + ".remove(item);"));
            genContent.Add(string.Format("{0}", "                }else{"));
            genContent.Add(string.Format("{0}", "                    bootbox.dialog({"));
            genContent.Add(string.Format("{0}", "                        message: \"<b>Do you want to delete the item?</b>\","));
            genContent.Add(string.Format("{0}", "                        buttons: {"));
            genContent.Add(string.Format("{0}", "                            success: {"));
            genContent.Add(string.Format("{0}", "                                label: \"Yes\", className: \"btn-primary\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                   item.EditMode(\"Delete\");"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            },"));
            genContent.Add(string.Format("{0}", "                            danger: {"));
            genContent.Add(string.Format("{0}", "                                label: \"No\", className: \"btn-danger\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                    return;"));
            genContent.Add(string.Format("{0}", "                                }"));
            genContent.Add(string.Format("{0}", "                            }"));
            genContent.Add(string.Format("{0}", "                        }"));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            function FinishInitList" + genInf.ModelName + "GUID(data){"));
            genContent.Add(string.Format("{0}", "               if(data.Result ==\"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                   var item = new " + genInf.ModelName + "("));
            #region init property
            for (int i = 0; i < listP.Count; i++)
            {
                var p = listP[i];
                switch (p.SystemType.ToLower())
                {
                    case "int":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "int?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "datetime?":
                    case "datetime":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "string":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "\"\""));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "\"\""));
                        }
                        break;
                    case "bool?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "bool":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "false"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "false"));
                        }
                        break;
                    case "double?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "double":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                    case "decimal?":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", "null"));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", "null"));
                        }
                        break;
                    case "decimal":
                        if (i < listP.Count - 1)
                        {
                            genContent.Add(string.Format("                                {0},", 0));
                        }
                        else
                        {
                            genContent.Add(string.Format("                                {0}", 0));
                        }
                        break;
                }
            }
            #endregion
            genContent.Add(string.Format("{0}", "                           );"));
            genContent.Add(string.Format("{0}", "                  item.GUID(data.GUIDString);"));
            genContent.Add(string.Format("{0}", "                  item.EditMode(\"New\");"));
            genContent.Add(string.Format("{0}", "                  item.OldValue(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                  item.IsEdit(true);"));
            genContent.Add(string.Format("{0}", "                  self.Arr" + genInf.ModelName + ".push(item);"));
            genContent.Add(string.Format("{0}", "               }"));
            genContent.Add(string.Format("{0}", "               self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",false);"));
            genContent.Add(string.Format("{0}", "            }"));
            genContent.Add(string.Format("{0}", "            self.close" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                var delitem = ko.utils.arrayFirt(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                   return item.EditMode() == \"Delete\";"));
            genContent.Add(string.Format("{0}", "                });"));
            genContent.Add(string.Format("{0}", "                if(deleitem!=null){"));
            genContent.Add(string.Format("{0}", "                          bootbox.dialog({"));
            genContent.Add(string.Format("{0}", "                                  message: \"<b>You have just deleted some items! Wish you want to continue to process this action? </b>\","));
            genContent.Add(string.Format("{0}", "                                  buttons: {"));
            genContent.Add(string.Format("{0}", "                                      success: {"));
            genContent.Add(string.Format("{0}", "                                          label: \"Yes\", className: \"btn-primary\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                              ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                                                  if(item.EditMode()!=\"Delete\")"));
            genContent.Add(string.Format("{0}", "                                                  item.EditMode(\"\");"));
            genContent.Add(string.Format("{0}", "                                              });"));
            genContent.Add(string.Format("{0}", "                                              SaveList" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "                                          }"));
            genContent.Add(string.Format("{0}", "                                      },"));
            genContent.Add(string.Format("{0}", "                                      danger: {"));
            genContent.Add(string.Format("{0}", "                                          label: \"No\", className: \"btn-danger\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                              // $(\"#popupEdit" + genInf.ModelName + "\").modal(\"hide\");"));
            genContent.Add(string.Format("{0}", "                                              return;"));
            genContent.Add(string.Format("{0}", "                                          }"));
            genContent.Add(string.Format("{0}", "                                      }"));
            genContent.Add(string.Format("{0}", "                                  }"));
            genContent.Add(string.Format("{0}", "                              });"));
            genContent.Add(string.Format("{0}", "                }"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            self.clearAll" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                bootbox.dialog({"));
            genContent.Add(string.Format("{0}", "                                  message: \"<b>Do you really to clear all items?</b>\","));
            genContent.Add(string.Format("{0}", "                                  buttons: {"));
            genContent.Add(string.Format("{0}", "                                      success: {"));
            genContent.Add(string.Format("{0}", "                                          label: \"Yes\", className: \"btn-primary\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                              ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                                                  item.EditMode(\"Delete\");"));
            genContent.Add(string.Format("{0}", "                                              });"));
            genContent.Add(string.Format("{0}", "                                              SaveList" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "                                          }"));
            genContent.Add(string.Format("{0}", "                                      },"));
            genContent.Add(string.Format("{0}", "                                      danger: {"));
            genContent.Add(string.Format("{0}", "                                          label: \"No\", className: \"btn-danger\", callback: function () {"));
            genContent.Add(string.Format("{0}", "                                              return;"));
            genContent.Add(string.Format("{0}", "                                          }"));
            genContent.Add(string.Format("{0}", "                                      }"));
            genContent.Add(string.Format("{0}", "                                  }"));
            genContent.Add(string.Format("{0}", "                              });"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            self.saveAll" + genInf.ModelName + " = function(){"));
            genContent.Add(string.Format("{0}", "                SaveList" + genInf.ModelName + "();"));
            genContent.Add(string.Format("{0}", "            };"));
            genContent.Add(string.Format("{0}", "            function SaveList" + genInf.ModelName + "(){"));
            genContent.Add(string.Format("{0}", "               var postArray = new Array();"));
            genContent.Add(string.Format("{0}", "               var isOK = true;"));
            genContent.Add(string.Format("{0}", "               ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                   if (item.EditMode() != \"Delete\") {"));
            genContent.Add(string.Format("{0}", "                       isOK = isOK && Validate" + genInf.ModelName + "(item);"));
            genContent.Add(string.Format("{0}", "                       if (isOK) {"));
            genContent.Add(string.Format("{0}", "                           postArray.push(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                       }"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "                   else {"));
            genContent.Add(string.Format("{0}", "                       postArray.push(ko.toJS(item));"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "               });"));
            genContent.Add(string.Format("{0}", "                if(postArray.length == self.Arr" + genInf.ModelName + "().length){"));
            genContent.Add(string.Format("{0}", "                   if(isOK){"));
            genContent.Add(string.Format("{0}", "                        self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",true);"));
            genContent.Add(string.Format("{0}", "                        var json = JSON.stringify(postArray);"));
            genContent.Add(string.Format("{0}", "                        $.ajax({"));
            genContent.Add(string.Format("{0}", "                           url: self.GetUrl.saveList" + genInf.ModelName + ","));
            genContent.Add(string.Format("{0}", "                           type: \"POST\","));
            genContent.Add(string.Format("{0}", "                           contentType: 'application/json; charset=utf-8',"));
            genContent.Add(string.Format("{0}", "                           cache: false,"));
            genContent.Add(string.Format("{0}", "                           data: json,"));
            genContent.Add(string.Format("{0}", "                           success: FinishSaveList" + genInf.ModelName + ""));
            genContent.Add(string.Format("{0}", "                       });"));
            genContent.Add(string.Format("{0}", "                   }"));
            genContent.Add(string.Format("{0}", "               }else{"));
            genContent.Add(string.Format("{0}", "                   bootbox.alert(\"Some item is not valid. So you cannot continue to process this acction!\");"));
            genContent.Add(string.Format("{0}", "               }"));
            genContent.Add(string.Format("{0}", "             } "));
            genContent.Add(string.Format("{0}", "            function FinishSaveList" + genInf.ModelName + "(data){"));
            genContent.Add(string.Format("{0}", "                 if(data.Result == \"@Base.ViewModel.ResultType.Success\"){"));
            genContent.Add(string.Format("{0}", "                     ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                        if(item." + genInf.ModelName + "ID() == 0){"));
            genContent.Add(string.Format("{0}", "                            var dataItem = ko.utils.arrayFirst(data." + genInf.ModelName + "s,function(item1){"));
            genContent.Add(string.Format("{0}", "                                return item1.GUID == item.GUID();"));
            genContent.Add(string.Format("{0}", "                             });"));
            genContent.Add(string.Format("{0}", "                             if(dataItem!=null){"));
            genContent.Add(string.Format("{0}", "                                item." + genInf.ModelName + "ID(dataItem." + genInf.ModelName + "ID);"));
            genContent.Add(string.Format("{0}", "                             }"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         if(item.EditMode()!=\"Delete\"){"));
            genContent.Add(string.Format("{0}", "                           item.EditMode(\"\");"));
            genContent.Add(string.Format("{0}", "                         }"));
            genContent.Add(string.Format("{0}", "                         item.IsEdit(false);"));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("{0}", "                    // update "));
            genContent.Add(string.Format("{0}", "                    if(self." + genInf.ModelName + "()!=null){"));
            genContent.Add(string.Format("{0}", "                       self." + genInf.ModelName + "().Arr" + genInf.ModelName + ".removeAll();"));
            genContent.Add(string.Format("{0}", "                       ko.utils.arrayForEach(self.Arr" + genInf.ModelName + "(),function(item){"));
            genContent.Add(string.Format("{0}", "                       if(item.EditMode()!=\"Delete\"){"));
            genContent.Add(string.Format("{0}", "                           self." + genInf.ModelName + "().Arr" + genInf.ModelName + ".push(item);"));
            genContent.Add(string.Format("{0}", "                       }"));
            genContent.Add(string.Format("{0}", "                    });"));
            genContent.Add(string.Format("{0}", "                    }"));
            genContent.Add(string.Format("{0}", "                    //$(\"#popupEdit" + genInf.ModelName + "\").modal(\"hide\");"));
            genContent.Add(string.Format("{0}", "                 }"));
            genContent.Add(string.Format("{0}", "                 self.Processing().SetProcessing(\"" + genInf.ModelName + "s\",false);"));
            genContent.Add(string.Format("{0}", "              }"));
            #endregion


            genContent.Add(string.Format("{0}", "             //--------------- End Action Function ----------//"));
            genContent.Add(string.Format("{0}", "             //--------------- End For item -----------------------------//"));
            genContent.Add(string.Format("{0}", "        };"));
            #endregion
            genContent.Add(string.Format("{0}", "</script>"));
            return genContent;
            #endregion

        }
        private List<string> MVC_AdminJScipt(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.Single(u => u.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID"))).ToList();
            #region getcontent
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_CommonScript\")"));
            //genContent.Add(string.Format("{0}", "@Html.Partial(\"_FormatJScript\")"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "JScript.cshtml\")"));
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "ViewModelsJScript.cshtml\")"));
            }
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "JScript\")"));
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "PageViewModelsJScript\")"));
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            genContent.Add(string.Format("{0}", "    var AdminViewModel = function () {"));
            genContent.Add(string.Format("{0}", "        var self = this;"));
            genContent.Add(string.Format("{0}", "        self.Processing = ko.observable(new ModelProcessing());"));
            genContent.Add(string.Format("{0}", "        self.Transition = ko.observable(new ModelTransition());"));
            genContent.Add(string.Format("{0}", "        self." + genInf.ModelName + "sVM = ko.observable(null);"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "        self." + p.PropertyName.Replace("ID", "") + "sVM = ko.observable(null);"));
            }
            genContent.Add(string.Format("{0}", "        self.InitData = function () {"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "        //define view model "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            var " + p.PropertyName.Replace("ID", "").ToLower() + "svm = new " + p.PropertyName.Replace("ID", "") + "sViewModel();"));
                genContent.Add(string.Format("{0}", "            " + p.PropertyName.Replace("ID", "").ToLower() + "svm.InitModel(self.Transition, self.Processing);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM(" + p.PropertyName.Replace("ID", "").ToLower() + "svm);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM().InitData();"));
            }
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            var " + genInf.ModelName.ToLower() + "svm = new " + genInf.ModelName + "sPageViewModel();"));
            genContent.Add(string.Format("{0}", "            " + genInf.ModelName.ToLower() + "svm.InitModel(self.Transition,self.Processing);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "sVM(" + genInf.ModelName.ToLower() + "svm);"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            //set view model relation  "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "sVM().Set" + p.PropertyName.Replace("ID", "") + "sVM(self." + p.PropertyName.Replace("ID", "") + "sVM);"));
            }
            genContent.Add(string.Format("{0}", "            //int main view model"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "sVM().InitData();"));
            genContent.Add(string.Format("{0}", "        };"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    var mv = new AdminViewModel();"));
            genContent.Add(string.Format("{0}", "    mv.InitData();"));
            genContent.Add(string.Format("{0}", "    ko.applyBindings(mv);"));
            genContent.Add(string.Format("{0}", "</script>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_AdminResource(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            #region getcontent
            genContent.Add(string.Format("{0}", "<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
            genContent.Add(string.Format("{0}", "<root>"));
            genContent.Add(string.Format("{0}", "  <xsd:schema id=\"root\" xmlns=\"\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\">"));
            genContent.Add(string.Format("{0}", "    <xsd:import namespace=\"http://www.w3.org/XML/1998/namespace\" />"));
            genContent.Add(string.Format("{0}", "    <xsd:element name=\"root\" msdata:IsDataSet=\"true\">"));
            genContent.Add(string.Format("{0}", "      <xsd:complexType>"));
            genContent.Add(string.Format("{0}", "        <xsd:choice maxOccurs=\"unbounded\">"));
            genContent.Add(string.Format("{0}", "          <xsd:element name=\"metadata\">"));
            genContent.Add(string.Format("{0}", "            <xsd:complexType>"));
            genContent.Add(string.Format("{0}", "              <xsd:sequence>"));
            genContent.Add(string.Format("{0}", "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" />"));
            genContent.Add(string.Format("{0}", "              </xsd:sequence>"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"name\" use=\"required\" type=\"xsd:string\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"type\" type=\"xsd:string\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"mimetype\" type=\"xsd:string\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute ref=\"xml:space\" />"));
            genContent.Add(string.Format("{0}", "            </xsd:complexType>"));
            genContent.Add(string.Format("{0}", "          </xsd:element>"));
            genContent.Add(string.Format("{0}", "          <xsd:element name=\"assembly\">"));
            genContent.Add(string.Format("{0}", "            <xsd:complexType>"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"alias\" type=\"xsd:string\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"name\" type=\"xsd:string\" />"));
            genContent.Add(string.Format("{0}", "            </xsd:complexType>"));
            genContent.Add(string.Format("{0}", "          </xsd:element>"));
            genContent.Add(string.Format("{0}", "          <xsd:element name=\"data\">"));
            genContent.Add(string.Format("{0}", "            <xsd:complexType>"));
            genContent.Add(string.Format("{0}", "              <xsd:sequence>"));
            genContent.Add(string.Format("{0}", "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"1\" />"));
            genContent.Add(string.Format("{0}", "                <xsd:element name=\"comment\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"2\" />"));
            genContent.Add(string.Format("{0}", "              </xsd:sequence>"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"name\" type=\"xsd:string\" use=\"required\" msdata:Ordinal=\"1\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"type\" type=\"xsd:string\" msdata:Ordinal=\"3\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"mimetype\" type=\"xsd:string\" msdata:Ordinal=\"4\" />"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute ref=\"xml:space\" />"));
            genContent.Add(string.Format("{0}", "            </xsd:complexType>"));
            genContent.Add(string.Format("{0}", "          </xsd:element>"));
            genContent.Add(string.Format("{0}", "          <xsd:element name=\"resheader\">"));
            genContent.Add(string.Format("{0}", "            <xsd:complexType>"));
            genContent.Add(string.Format("{0}", "              <xsd:sequence>"));
            genContent.Add(string.Format("{0}", "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"1\" />"));
            genContent.Add(string.Format("{0}", "              </xsd:sequence>"));
            genContent.Add(string.Format("{0}", "              <xsd:attribute name=\"name\" type=\"xsd:string\" use=\"required\" />"));
            genContent.Add(string.Format("{0}", "            </xsd:complexType>"));
            genContent.Add(string.Format("{0}", "          </xsd:element>"));
            genContent.Add(string.Format("{0}", "        </xsd:choice>"));
            genContent.Add(string.Format("{0}", "      </xsd:complexType>"));
            genContent.Add(string.Format("{0}", "    </xsd:element>"));
            genContent.Add(string.Format("{0}", "  </xsd:schema>"));
            genContent.Add(string.Format("{0}", "  <resheader name=\"resmimetype\">"));
            genContent.Add(string.Format("{0}", "    <value>text/microsoft-resx</value>"));
            genContent.Add(string.Format("{0}", "  </resheader>"));
            genContent.Add(string.Format("{0}", "  <resheader name=\"version\">"));
            genContent.Add(string.Format("{0}", "    <value>2.0</value>"));
            genContent.Add(string.Format("{0}", "  </resheader>"));
            genContent.Add(string.Format("{0}", "  <resheader name=\"reader\">"));
            genContent.Add(string.Format("{0}", "    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>"));
            genContent.Add(string.Format("{0}", "  </resheader>"));
            genContent.Add(string.Format("{0}", "  <resheader name=\"writer\">"));
            genContent.Add(string.Format("{0}", "    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>"));
            genContent.Add(string.Format("{0}", "  </resheader>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Button_New\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>New</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Button_Search\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Find</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"PlaceHolder_Search\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>search</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Label_Table_OrderNo\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>No</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            foreach (var p in listP)
            {
                genContent.Add(string.Format("  <data name=\"Label_Table_{0}\" xml:space=\"preserve\">", p.PropertyName));
                genContent.Add(string.Format("    <value>{0}</value>", p.PropertyName));
                genContent.Add(string.Format("{0}", "  </data>"));
            }
            foreach (var p in listP)
            {
                genContent.Add(string.Format("  <data name=\"PlaceHolder_Table_{0}\" xml:space=\"preserve\">", p.PropertyName));
                genContent.Add(string.Format("    <value>{0}</value>", p.PropertyName));
                genContent.Add(string.Format("{0}", "  </data>"));
            }
            genContent.Add(string.Format("{0}", "  <data name=\"Message_ConfirmDelete\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Do you really want to remove this item from system?</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Message_NotConnectAPI\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Cannot connect to WebAPI</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Message_SaveFail\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Cannot save this object. Please try again later!</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Text_ItemName\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>items</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Text_Page\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Page</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Text_TotalItems\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Total</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Title\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>List Item</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));

            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Cancel\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>cancel</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_New\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>edit</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Edit\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>edit</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Edit_Current\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>current edit</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Remove\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>remove</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Cancel\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>cancel</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_New\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>new</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Tooltip_Save\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>save</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Save\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>save</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Remove\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Delete</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Save\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Save</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Cancel\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Cancel</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Back\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Back</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Button_Detail_Edit\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Edit</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Title_Detail\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Detail</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Title_Create\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Create</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "  <data name=\"Title_Edit\" xml:space=\"preserve\">"));
            genContent.Add(string.Format("{0}", "    <value>Edit</value>"));
            genContent.Add(string.Format("{0}", "  </data>"));
            genContent.Add(string.Format("{0}", "</root>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_CreateView(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));
            #region getcontent
            genContent.Add(string.Format("{0}", "@{"));
            genContent.Add(string.Format("{0}", "    Layout = \"~/Views/Shared/_MainLayout.cshtml\";"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section styleContent{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "<div class=\"row\">"));
            genContent.Add(string.Format("{0}", "    <div class=\"col-md-12\" data-bind=\"with:$root." + genInf.ModelName + "VM\">"));
            genContent.Add(string.Format("{0}", "        <div class=\"row\" data-bind=\"visible:Processing().GetProcessing('" + genInf.ModelName + "')()\" >"));
            genContent.Add(string.Format("{0}", "           <div class=\"col-md-6 col-md-offset-6\" style=\"position:absolute\">"));
            genContent.Add(string.Format("{0}", "               <img src=\"~/Images/kloading.gif\" alt=\"Loading ...\" />"));
            genContent.Add(string.Format("{0}", "           </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "        <form id=\"form" + genInf.ModelName + "\" class=\"form-horizontal\" role=\"form\" data-bind=\"with:" + genInf.ModelName + "\">"));
            #region Bar control
            genContent.Add(string.Format("{0}", "             <div class=\"row titleholder\">"));
            genContent.Add(string.Format("{0}", "                <div class=\"col-md-6\">"));
            genContent.Add(string.Format("{0}", "                      <div class=\"moduletitle\">"));
            genContent.Add(string.Format("{0}", "                          <span>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Title_Create</span>"));
            genContent.Add(string.Format("{0}", "                      </div>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "                <div class=\"pull-right\" data-bind=\"visible:$root.Processing().Completed()==100\">"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === false -->"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowEdit, click:$parent.startEdit" + genInf.ModelName + "\"><i class=\"glyphicon glyphicon-pencil\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Edit</button>"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowRemove, click:$parent.remove" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-trash\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Remove</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === true -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.finishEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-ok\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Save</button>"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.cancelEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-remove\"></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Cancel</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click: $parent.goBackToHome\"><i class=\"glyphicon glyphicon-arrow-left\"></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Back</button>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "             </div>"));
            #endregion
            genContent.Add(string.Format("{0}", "             @Html.Partial(\"_Form" + genInf.ModelName + "\")"));
            #region Bar control
            genContent.Add(string.Format("{0}", "             <div class=\"row titleholder\">"));
            genContent.Add(string.Format("{0}", "                <div class=\"col-md-6\">"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "                <div class=\"pull-right\" data-bind=\"visible:$root.Processing().Completed()==100\">"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === false -->"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowEdit, click:$parent.startEdit" + genInf.ModelName + "\"><i class=\"glyphicon glyphicon-pencil\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Edit</button>"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowRemove, click:$parent.remove" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-trash\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Remove</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === true -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.finishEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-ok\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Save</button>"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.cancelEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-remove\"></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Cancel</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click: $parent.goBackToHome\"><i class=\"glyphicon glyphicon-arrow-left\"></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Back</button>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "             </div>"));
            #endregion
            genContent.Add(string.Format("{0}", "         </form>"));
            genContent.Add(string.Format("{0}", "    </div>"));
            genContent.Add(string.Format("{0}", "</div>"));
            genContent.Add(string.Format("{0}", "@section popup{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section scripts{"));
            genContent.Add(string.Format("{0}", "    @Html.Partial(\"_CreateJScript\")"));
            genContent.Add(string.Format("{0}", "}"));
            #endregion
            return genContent;
        }
        private List<string> MVC_EditView(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));
            #region getcontent
            genContent.Add(string.Format("{0}", "@{"));
            genContent.Add(string.Format("{0}", "    Layout = \"~/Views/Shared/_MainLayout.cshtml\";"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section styleContent{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "<div class=\"row\">"));
            genContent.Add(string.Format("{0}", "    <div class=\"col-md-12\" data-bind=\"with:$root." + genInf.ModelName + "VM\">"));
            genContent.Add(string.Format("{0}", "        <div class=\"row\" data-bind=\"visible:Processing().GetProcessing('" + genInf.ModelName + "')()\" >"));
            genContent.Add(string.Format("{0}", "           <div class=\"col-md-6 col-md-offset-6\" style=\"position:absolute\">"));
            genContent.Add(string.Format("{0}", "               <img src=\"~/Images/kloading.gif\" alt=\"Loading ...\" />"));
            genContent.Add(string.Format("{0}", "           </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "        <form id=\"form" + genInf.ModelName + "\" class=\"form-horizontal\" role=\"form\" data-bind=\"with:" + genInf.ModelName + "\">"));
            #region Bar control
            genContent.Add(string.Format("{0}", "             <div class=\"row titleholder\">"));
            genContent.Add(string.Format("{0}", "                <div class=\"col-md-6\">"));
            genContent.Add(string.Format("{0}", "                      <div class=\"moduletitle\">"));
            genContent.Add(string.Format("{0}", "                          <span>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Title_Edit</span>"));
            genContent.Add(string.Format("{0}", "                      </div>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "                <div class=\"pull-right\" data-bind=\"visible:$root.Processing().Completed()==100\">"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === false -->"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowEdit, click:$parent.startEdit" + genInf.ModelName + "\"><i class=\"glyphicon glyphicon-pencil\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Edit</button>"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowRemove, click:$parent.remove" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-trash\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Remove</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === true -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.finishEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-ok\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Save</button>"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.cancelEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-remove\"></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Cancel</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click: $parent.goBackToHome\"><i class=\"glyphicon glyphicon-arrow-left\"></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Back</button>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "             </div>"));
            #endregion
            genContent.Add(string.Format("{0}", "             @Html.Partial(\"_Form" + genInf.ModelName + "\")"));
            #region Bar control
            genContent.Add(string.Format("{0}", "             <div class=\"row titleholder\">"));
            genContent.Add(string.Format("{0}", "                <div class=\"col-md-6\">"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "                <div class=\"pull-right\" data-bind=\"visible:$root.Processing().Completed()==100\">"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === false -->"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowEdit, click:$parent.startEdit" + genInf.ModelName + "\"><i class=\"glyphicon glyphicon-pencil\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Edit</button>"));
            genContent.Add(string.Format("{0}", "                     <button class=\"btn btn-sm\" data-bind=\"visible:AllowRemove, click:$parent.remove" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-trash\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Remove</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <!-- ko if: IsEdit() === true -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.finishEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-ok\" ></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Save</button>"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click:$parent.cancelEdit" + genInf.ModelName + "\" ><i class=\"glyphicon glyphicon-remove\"></i> @" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Cancel</button>"));
            genContent.Add(string.Format("{0}", "                    <!-- /ko -->"));
            genContent.Add(string.Format("{0}", "                    <button class=\"btn btn-sm\" data-bind=\"click: $parent.goBackToHome\"><i class=\"glyphicon glyphicon-arrow-left\"></i>@" + genInf.ProjectName + ".Resource." + genInf.ModelName + ".Admin.Button_Detail_Back</button>"));
            genContent.Add(string.Format("{0}", "                </div>"));
            genContent.Add(string.Format("{0}", "             </div>"));
            #endregion
            genContent.Add(string.Format("{0}", "        </form>"));
            genContent.Add(string.Format("{0}", "    </div>"));
            genContent.Add(string.Format("{0}", "</div>"));
            genContent.Add(string.Format("{0}", "@section popup{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section scripts{"));
            genContent.Add(string.Format("{0}", "    @Html.Partial(\"_EditJScript\")"));
            genContent.Add(string.Format("{0}", "}"));
            #endregion
            return genContent;
        }
        private List<string> MVC_DetailView(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));
            #region getcontent
            genContent.Add(string.Format("{0}", "@{"));
            genContent.Add(string.Format("{0}", "    Layout = \"~/Views/Shared/_MainLayout.cshtml\";"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section styleContent{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "<div class=\"row\">"));
            genContent.Add(string.Format("{0}", "    <div class=\"col-md-12\" data-bind=\"with:$root." + genInf.ModelName + "VM\">"));
            genContent.Add(string.Format("{0}", "        <div class=\"row\" data-bind=\"visible:Processing().GetProcessing('" + genInf.ModelName + "')()\" >"));
            genContent.Add(string.Format("{0}", "           <div class=\"col-md-6 col-md-offset-6\" style=\"position:absolute\">"));
            genContent.Add(string.Format("{0}", "               <img src=\"~/Images/kloading.gif\" alt=\"Loading ...\" />"));
            genContent.Add(string.Format("{0}", "           </div>"));
            genContent.Add(string.Format("{0}", "        </div>"));
            genContent.Add(string.Format("{0}", "        <form id=\"form" + genInf.ModelName + "\" class=\"form-horizontal\" role=\"form\" data-bind=\"with:" + genInf.ModelName + "\">"));
            genContent.Add(string.Format("{0}", "                @Html.Partial(\"_Form" + genInf.ModelName + "\")"));
            genContent.Add(string.Format("{0}", "           </form>"));
            genContent.Add(string.Format("{0}", "    </div>"));
            genContent.Add(string.Format("{0}", "</div>"));
            genContent.Add(string.Format("{0}", "@section popup{"));
            genContent.Add(string.Format("{0}", "}"));
            genContent.Add(string.Format("{0}", "@section scripts{"));
            genContent.Add(string.Format("{0}", "    @Html.Partial(\"_EditJScript\")"));
            genContent.Add(string.Format("{0}", "}"));
            #endregion
            return genContent;
        }
        private List<string> MVC_CreateJScript(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.Single(u => u.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID"))).ToList();
            #region getcontent
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_CommonScript\")"));
            //genContent.Add(string.Format("{0}", "@Html.Partial(\"_FormatJScript\")"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "JScript.cshtml\")"));
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "ViewModelsJScript.cshtml\")"));
            }
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "JScript\")"));
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "ViewModelJScript\")"));
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            genContent.Add(string.Format("{0}", "    var CreateViewModel = function () {"));
            genContent.Add(string.Format("{0}", "        var self = this;"));
            genContent.Add(string.Format("{0}", "        self.Processing = ko.observable(new ModelProcessing());"));
            genContent.Add(string.Format("{0}", "        self.Transition = ko.observable(new ModelTransition());"));
            genContent.Add(string.Format("{0}", "        self." + genInf.ModelName + "VM = ko.observable(null);"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "        self." + p.PropertyName.Replace("ID", "") + "sVM = ko.observable(null);"));
            }
            genContent.Add(string.Format("{0}", "        self.InitData = function () {"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "        //define view model "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            var " + p.PropertyName.Replace("ID", "").ToLower() + "svm = new " + p.PropertyName.Replace("ID", "") + "sViewModel();"));
                genContent.Add(string.Format("{0}", "            " + p.PropertyName.Replace("ID", "").ToLower() + "svm.InitModel(self.Transition, self.Processing);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM(" + p.PropertyName.Replace("ID", "").ToLower() + "svm);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM().InitData();"));
            }
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            var " + genInf.ModelName.ToLower() + "vm = new " + genInf.ModelName + "ViewModel();"));
            genContent.Add(string.Format("{0}", "            " + genInf.ModelName.ToLower() + "vm.InitModel(self.Transition,self.Processing);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM(" + genInf.ModelName.ToLower() + "vm);"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            //set view model relation  "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().Set" + p.PropertyName.Replace("ID", "") + "sVM(self." + p.PropertyName.Replace("ID", "") + "sVM);"));
            }
            genContent.Add(string.Format("{0}", "            //int main view model"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().InitData();"));
            genContent.Add(string.Format("{0}", "        };"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    var mv = new CreateViewModel();"));
            genContent.Add(string.Format("{0}", "    mv.InitData();"));
            genContent.Add(string.Format("{0}", "    ko.applyBindings(mv);"));
            genContent.Add(string.Format("{0}", "</script>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_EditJScript(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.Single(u => u.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID"))).ToList();
            #region getcontent
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_CommonScript\")"));
            //genContent.Add(string.Format("{0}", "@Html.Partial(\"_FormatJScript\")"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "JScript.cshtml\")"));
                genContent.Add(string.Format("{0}", "@Html.Partial(\"~/Views/" + p.PropertyName.Replace("ID", "") + "/_" + p.PropertyName.Replace("ID", "") + "ViewModelsJScript.cshtml\")"));
            }
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "JScript\")"));
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "ViewModelJScript\")"));
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            genContent.Add(string.Format("{0}", "    var EditViewModel = function () {"));
            genContent.Add(string.Format("{0}", "        var self = this;"));
            genContent.Add(string.Format("{0}", "        self.Processing = ko.observable(new ModelProcessing());"));
            genContent.Add(string.Format("{0}", "        self.Transition = ko.observable(new ModelTransition());"));
            genContent.Add(string.Format("{0}", "        self." + genInf.ModelName + "VM = ko.observable(null);"));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "        self." + p.PropertyName.Replace("ID", "") + "sVM = ko.observable(null);"));
            }
            genContent.Add(string.Format("{0}", "        self.InitData = function () {"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "        //define view model "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            var " + p.PropertyName.Replace("ID", "").ToLower() + "svm = new " + p.PropertyName.Replace("ID", "") + "sViewModel();"));
                genContent.Add(string.Format("{0}", "            " + p.PropertyName.Replace("ID", "").ToLower() + "svm.InitModel(self.Transition, self.Processing);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM(" + p.PropertyName.Replace("ID", "").ToLower() + "svm);"));
                genContent.Add(string.Format("{0}", "            self." + p.PropertyName.Replace("ID", "") + "sVM().InitData();"));
            }
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            var " + genInf.ModelName.ToLower() + "vm = new " + genInf.ModelName + "ViewModel();"));
            genContent.Add(string.Format("{0}", "            " + genInf.ModelName.ToLower() + "vm.InitModel(self.Transition,self.Processing);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM(" + genInf.ModelName.ToLower() + "vm);"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            //set view model relation  "));
            foreach (var p in listFP)
            {
                genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().Set" + p.PropertyName.Replace("ID", "") + "sVM(self." + p.PropertyName.Replace("ID", "") + "sVM);"));
            }
            genContent.Add(string.Format("{0}", "            //int main view model"));
            genContent.Add(string.Format("{0}", "            var id = parseInt(\"@ViewBag." + keyP.PropertyName + "\");"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().Set" + keyP.PropertyName + "(id);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().InitData();"));
            genContent.Add(string.Format("{0}", "        };"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    var mv = new EditViewModel();"));
            genContent.Add(string.Format("{0}", "    mv.InitData();"));
            genContent.Add(string.Format("{0}", "    ko.applyBindings(mv);"));
            genContent.Add(string.Format("{0}", "</script>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_DetailJScript(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.Single(u => u.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate" };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName)).ToList();
            var listFP = listP.Where(p => (!p.IsPrimaryKey) && (p.PropertyName.Contains("ID"))).ToList();
            #region getcontent
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_CommonScript\")"));
            //genContent.Add(string.Format("{0}", "@Html.Partial(\"_FormatJScript\")"));
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "JScript\")"));
            genContent.Add(string.Format("{0}", "@Html.Partial(\"_" + genInf.ModelName + "ViewModelJScript\")"));
            genContent.Add(string.Format("{0}", "<script type=\"text/javascript\">"));
            genContent.Add(string.Format("{0}", "    var DetailViewModel = function () {"));
            genContent.Add(string.Format("{0}", "        var self = this;"));
            genContent.Add(string.Format("{0}", "        self.Processing = ko.observable(new ModelProcessing());"));
            genContent.Add(string.Format("{0}", "        self.Transition = ko.observable(new ModelTransition());"));
            genContent.Add(string.Format("{0}", "        self." + genInf.ModelName + "VM = ko.observable(null);"));
            genContent.Add(string.Format("{0}", "        self.InitData = function () {"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "        //define view model "));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            var " + genInf.ModelName.ToLower() + "vm = new " + genInf.ModelName + "ViewModel();"));
            genContent.Add(string.Format("{0}", "            " + genInf.ModelName.ToLower() + "vm.InitModel(self.Transition,self.Processing);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM(" + genInf.ModelName.ToLower() + "vm);"));
            genContent.Add(string.Format("{0}", " "));
            genContent.Add(string.Format("{0}", "            //int main view model"));
            genContent.Add(string.Format("{0}", "            var id = parseInt(\"@ViewBag." + keyP.PropertyName + "\");"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().Set" + keyP.PropertyName + "(id);"));
            genContent.Add(string.Format("{0}", "            self." + genInf.ModelName + "VM().InitData();"));
            genContent.Add(string.Format("{0}", "        };"));
            genContent.Add(string.Format("{0}", "    };"));
            genContent.Add(string.Format("{0}", "    var mv = new DetailViewModel();"));
            genContent.Add(string.Format("{0}", "    mv.InitData();"));
            genContent.Add(string.Format("{0}", "    ko.applyBindings(mv);"));
            genContent.Add(string.Format("{0}", "</script>"));
            #endregion
            return genContent;
        }
        private List<string> MVC_FormControl(List<PropertyGenInfo> listProperty, GenCodeInfo genInf)
        {
            List<string> genContent = new List<string>();
            var keyP = listProperty.First(p => p.IsPrimaryKey);
            string[] exceptString = new string[] { "CreatedBy", "CreatedDate", "Active", "ModifiedBy", "ModifiedDate", keyP.PropertyName };
            var listP = listProperty.Where(p => !exceptString.Contains(p.PropertyName));
            #region getcontent
            genContent.Add(string.Format("{0}", "<!-- ko if: IsEdit()===false -->"));
            foreach (var p in listP)
            {
                ConvertToViewFormHTMLControl(genContent, p, genInf);
            }
            genContent.Add(string.Format("{0}", "<!-- /ko -->"));
            genContent.Add(string.Format("{0}", "<!-- ko if: IsEdit()===true -->"));
            foreach (var p in listP)
            {
                ConvertToEditFormHTMLControl(genContent, p, genInf);
            }
            genContent.Add(string.Format("{0}", "<!-- /ko -->"));
            #endregion
            return genContent;
        }
        #endregion
        #endregion

    }
}

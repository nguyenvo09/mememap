﻿namespace APITestApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridTest = new System.Windows.Forms.DataGridView();
            this.colIsRun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colClassName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAssemblyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSystemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsNullable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colControlType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsPrimaryKey = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnRunTest = new System.Windows.Forms.Button();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAssembly = new System.Windows.Forms.TextBox();
            this.btnReload = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedAll = new System.Windows.Forms.CheckBox();
            this.btnGenConfig = new System.Windows.Forms.Button();
            this.txtAPIProjectName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProjectFolder = new System.Windows.Forms.TextBox();
            this.btnSelectProjectFolder = new System.Windows.Forms.Button();
            this.txtMVCProjectName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTest)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridTest
            // 
            this.dataGridTest.AllowUserToAddRows = false;
            this.dataGridTest.AllowUserToDeleteRows = false;
            this.dataGridTest.AllowUserToOrderColumns = true;
            this.dataGridTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIsRun,
            this.colClassName,
            this.colAssemblyName,
            this.colDescription,
            this.colSystemType,
            this.colIsNullable,
            this.colControlType,
            this.colIsPrimaryKey});
            this.dataGridTest.Location = new System.Drawing.Point(16, 71);
            this.dataGridTest.MultiSelect = false;
            this.dataGridTest.Name = "dataGridTest";
            this.dataGridTest.RowHeadersWidth = 15;
            this.dataGridTest.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTest.Size = new System.Drawing.Size(903, 420);
            this.dataGridTest.TabIndex = 1;
            // 
            // colIsRun
            // 
            this.colIsRun.DataPropertyName = "IsGen";
            this.colIsRun.HeaderText = "IsGen";
            this.colIsRun.Name = "colIsRun";
            this.colIsRun.Width = 50;
            // 
            // colClassName
            // 
            this.colClassName.DataPropertyName = "ClassName";
            this.colClassName.HeaderText = "Class Name";
            this.colClassName.Name = "colClassName";
            this.colClassName.ReadOnly = true;
            this.colClassName.Width = 120;
            // 
            // colAssemblyName
            // 
            this.colAssemblyName.DataPropertyName = "PropertyName";
            this.colAssemblyName.HeaderText = "ProperytName";
            this.colAssemblyName.Name = "colAssemblyName";
            this.colAssemblyName.ReadOnly = true;
            this.colAssemblyName.Width = 150;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "DataTypeName";
            this.colDescription.HeaderText = "DataType";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            // 
            // colSystemType
            // 
            this.colSystemType.DataPropertyName = "SystemType";
            this.colSystemType.HeaderText = "SystemType";
            this.colSystemType.Name = "colSystemType";
            // 
            // colIsNullable
            // 
            this.colIsNullable.DataPropertyName = "IsNullable";
            this.colIsNullable.HeaderText = "IsNullable";
            this.colIsNullable.Name = "colIsNullable";
            // 
            // colControlType
            // 
            this.colControlType.DataPropertyName = "ControlType";
            this.colControlType.HeaderText = "Control Type";
            this.colControlType.Name = "colControlType";
            // 
            // colIsPrimaryKey
            // 
            this.colIsPrimaryKey.DataPropertyName = "IsPrimaryKey";
            this.colIsPrimaryKey.HeaderText = "IsKey";
            this.colIsPrimaryKey.Name = "colIsPrimaryKey";
            // 
            // btnRunTest
            // 
            this.btnRunTest.Location = new System.Drawing.Point(430, 44);
            this.btnRunTest.Name = "btnRunTest";
            this.btnRunTest.Size = new System.Drawing.Size(96, 23);
            this.btnRunTest.TabIndex = 1;
            this.btnRunTest.Text = "&Gen code";
            this.btnRunTest.UseVisualStyleBackColor = true;
            this.btnRunTest.Click += new System.EventHandler(this.btnRunTest_Click);
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(368, 47);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(37, 17);
            this.chkAll.TabIndex = 4;
            this.chkAll.Text = "All";
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Assembly";
            // 
            // txtAssembly
            // 
            this.txtAssembly.Location = new System.Drawing.Point(81, 15);
            this.txtAssembly.Name = "txtAssembly";
            this.txtAssembly.Size = new System.Drawing.Size(262, 20);
            this.txtAssembly.TabIndex = 2;
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(349, 15);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(76, 23);
            this.btnReload.TabIndex = 3;
            this.btnReload.Text = "&Load From";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(287, 44);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(181, 44);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(100, 20);
            this.txtSearch.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Class Name";
            // 
            // checkedAll
            // 
            this.checkedAll.AutoSize = true;
            this.checkedAll.Location = new System.Drawing.Point(38, 47);
            this.checkedAll.Name = "checkedAll";
            this.checkedAll.Size = new System.Drawing.Size(70, 17);
            this.checkedAll.TabIndex = 8;
            this.checkedAll.Text = "Select All";
            this.checkedAll.UseVisualStyleBackColor = true;
            this.checkedAll.CheckedChanged += new System.EventHandler(this.checkedAll_CheckedChanged);
            // 
            // btnGenConfig
            // 
            this.btnGenConfig.Location = new System.Drawing.Point(532, 44);
            this.btnGenConfig.Name = "btnGenConfig";
            this.btnGenConfig.Size = new System.Drawing.Size(96, 23);
            this.btnGenConfig.TabIndex = 1;
            this.btnGenConfig.Text = "&Gen config";
            this.btnGenConfig.UseVisualStyleBackColor = true;
            this.btnGenConfig.Click += new System.EventHandler(this.btnGenConfig_Click);
            // 
            // txtAPIProjectName
            // 
            this.txtAPIProjectName.Location = new System.Drawing.Point(808, 40);
            this.txtAPIProjectName.Name = "txtAPIProjectName";
            this.txtAPIProjectName.Size = new System.Drawing.Size(100, 20);
            this.txtAPIProjectName.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(745, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "API Project";
            // 
            // txtProjectFolder
            // 
            this.txtProjectFolder.Location = new System.Drawing.Point(430, 15);
            this.txtProjectFolder.Name = "txtProjectFolder";
            this.txtProjectFolder.Size = new System.Drawing.Size(198, 20);
            this.txtProjectFolder.TabIndex = 11;
            // 
            // btnSelectProjectFolder
            // 
            this.btnSelectProjectFolder.Location = new System.Drawing.Point(634, 13);
            this.btnSelectProjectFolder.Name = "btnSelectProjectFolder";
            this.btnSelectProjectFolder.Size = new System.Drawing.Size(100, 23);
            this.btnSelectProjectFolder.TabIndex = 12;
            this.btnSelectProjectFolder.Text = "&Project Folder";
            this.btnSelectProjectFolder.UseVisualStyleBackColor = true;
            this.btnSelectProjectFolder.Click += new System.EventHandler(this.btnSelectProjectFolder_Click);
            // 
            // txtMVCProjectName
            // 
            this.txtMVCProjectName.Location = new System.Drawing.Point(808, 12);
            this.txtMVCProjectName.Name = "txtMVCProjectName";
            this.txtMVCProjectName.Size = new System.Drawing.Size(100, 20);
            this.txtMVCProjectName.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(741, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "MVC Project";
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 507);
            this.Controls.Add(this.txtMVCProjectName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtProjectFolder);
            this.Controls.Add(this.btnSelectProjectFolder);
            this.Controls.Add(this.txtAPIProjectName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkedAll);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtAssembly);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkAll);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnGenConfig);
            this.Controls.Add(this.btnRunTest);
            this.Controls.Add(this.dataGridTest);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridTest;
        private System.Windows.Forms.Button btnRunTest;
        private System.Windows.Forms.CheckBox chkAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAssembly;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkedAll;
        private System.Windows.Forms.Button btnGenConfig;
        private System.Windows.Forms.TextBox txtAPIProjectName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsRun;
        private System.Windows.Forms.DataGridViewTextBoxColumn colClassName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAssemblyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSystemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsNullable;
        private System.Windows.Forms.DataGridViewTextBoxColumn colControlType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsPrimaryKey;
        private System.Windows.Forms.TextBox txtProjectFolder;
        private System.Windows.Forms.Button btnSelectProjectFolder;
        private System.Windows.Forms.TextBox txtMVCProjectName;
        private System.Windows.Forms.Label label4;
    }
}


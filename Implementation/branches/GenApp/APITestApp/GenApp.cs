﻿using System;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using APITestApp.Business;
using System.Linq;
namespace APITestApp
{
    /// <summary>
    /// This class will configure setting for this application
    /// </summary>
    public class GenApp
    {
        public List<PropertyGenInfo> ListProperty { set; get; }
        public GenCodeToDo GenToDo { set; get; }
        public GenApp(List<string> listAssemblyName)
        {
            ListProperty = new List<PropertyGenInfo>();
            foreach (string assemblyName in listAssemblyName)
            {
                if (DynaInvoke.InitAssemblyInstance(assemblyName))
                {
                    //Assembly assembly = Assembly.LoadFile(assemblyName);
                    Assembly assembly = (Assembly)DynaInvoke.AssemblyReferences[assemblyName];

                    // get assembly
                    foreach (Type type in assembly.GetTypes())
                    {
                        //get class
                        if (type.IsClass == true)
                        {
                            string className = type.FullName.Substring(type.FullName.LastIndexOf('.') + 1, type.FullName.Length - type.FullName.LastIndexOf('.') - 1);
                            DynaInvoke.InitClassInstance(assemblyName, className, null);
                            //get methods
                            if (!className.Contains("UserInstance") || 1 == 1)
                            {
                                DynaInvoke.DynaClassInfo ci = DynaInvoke.GetClassReference(assemblyName, className);
                                PropertyInfo[] propertyInfos = ci.type.GetProperties();


                                List<string> objectDefaultMethod = new List<string> { "ToString", "Equals", "GetHashCode", "GetType" };
                                var listProperty = propertyInfos.Where(p => !p.PropertyType.Name.Contains("ICollection")).ToList();
                                var hasKey = false;
                                foreach (var p in listProperty)
                                {
                                    bool isNullable;
                                    string controlType;
                                    PropertyGenInfo tr = new PropertyGenInfo();
                                    tr.ClassName = p.DeclaringType.Name;
                                    tr.DataTypeName = p.PropertyType.FullName;

                                    tr.PropertyName = p.Name;
                                    tr.SystemType = CommonService.ConvertToSystemType(tr.DataTypeName, out isNullable, out controlType);
                                    tr.IsNullable = isNullable;
                                    tr.ControlType = controlType;
                                    if (!hasKey)
                                    {
                                        tr.IsPrimaryKey = tr.PropertyName.EndsWith("ID");
                                        hasKey = true;
                                        //tr.PropertyName == className + "ID";
                                    }
                                    tr.IsGen = !string.IsNullOrEmpty(tr.SystemType);
                                    ListProperty.Add(tr);
                                }

                            }


                        }
                    }
                }

            }
        }
    }
}

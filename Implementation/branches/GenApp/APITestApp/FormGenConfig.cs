﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using APITestApp.Business;

namespace APITestApp
{
    public partial class FormGenConfig : Form
    {
        GenCodeToDo genConfig;
        public FormGenConfig(GenCodeToDo _genConfig)
        {
            InitializeComponent();
            this.genConfig = _genConfig;
        }

        private void FormGenConfig_Load(object sender, EventArgs e)
        {
            this.gridGenConfig.DataSource = genConfig.listGenInfo;
        }
    }
}

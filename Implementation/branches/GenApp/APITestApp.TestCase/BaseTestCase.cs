﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net;

namespace APITestApp.TestCase
{
    public class BaseTestCase
    {
        public HttpClient httpClient;
        public HttpStatusCode hCode;
        public string BaseURI = "http://localhost:3000/";
    }
}

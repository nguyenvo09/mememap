﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.Business
{
    public class GenCodeInfo
    {
        public int OrderNum { get; set; } 
        public string JobName { get; set; }
        public string ProjectName { get; set; }
        public string FileGenType { get; set; }
        public string FileName { get; set; }
        public string Folder { get; set; }
        public string Namespace { get; set; }
        public string ModelName { get; set; }
        public string ModelExtension { get; set; }
        public string GenClassName { get; set; }
        public bool   IsGen { get; set; }
    }
    public class GenCodeToDo {
        public List<GenCodeInfo> listGenInfo { get; set; }
       
        public GenCodeToDo(string modelName,string apiProjectName,string mvcProjectName,string projectFolder)
        {
            string folderSeperate = "\\";
            listGenInfo = new List<GenCodeInfo>();
            #region API
            #region API_DTO
            listGenInfo.Add(new GenCodeInfo {
                OrderNum = 1, 
                JobName = FileGenType.API_DTO, 
                FileGenType = FileGenType.API_DTO,
                ProjectName = apiProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "DTO"),
                Folder = string.Format("{0}{1}{2}{1}{2}.Model.DTO{1}{2}", projectFolder, folderSeperate, apiProjectName),
                Namespace = string.Format("{0}.Model.DTO", apiProjectName), 
                GenClassName = string.Format("{0}{1}", modelName,"DTO" ),
                ModelExtension = "DTO",
                IsGen = true
            });
            #endregion
            #region API_Repository
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 2,
                JobName = FileGenType.API_Repository,
                
                FileGenType = FileGenType.API_Repository,
                ProjectName = apiProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "Repository"),
                Folder = string.Format("{0}{1}{2}{1}{2}.Repository{1}{2}", projectFolder, folderSeperate, apiProjectName),
                Namespace = string.Format("{0}.Repository", apiProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "Repository"),
                ModelExtension = "Repository",
                IsGen = true
            });
            #endregion
            #region API_Logic
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 3,
                JobName = FileGenType.API_Logic,
                FileGenType = FileGenType.API_Logic,
                ProjectName = apiProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "Logic"),
                Folder = string.Format("{0}{1}{2}{1}{2}.Logic{1}{2}", projectFolder, folderSeperate, apiProjectName),
                Namespace = string.Format("{0}.Logic", apiProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "Logic"),
                ModelExtension = "Logic",
                IsGen = true
            });
            #endregion
            #region API_Controller
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 4,
                JobName = FileGenType.API_Controller,
                FileGenType = FileGenType.API_Controller,
                ProjectName = apiProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "Controller"),
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Controllers{1}{2}", projectFolder, folderSeperate, apiProjectName),
                Namespace = string.Format("{0}.Controllers", apiProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "Controller"),
                ModelExtension = "Controller",
                IsGen = true
            });
            #endregion
            #endregion
            #region MVC
            listGenInfo.Add(new GenCodeInfo { 
                OrderNum = 5,
                JobName = FileGenType.MVC_DTO, 
                FileGenType = FileGenType.API_DTO,
                ProjectName = mvcProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "DTO"),
                Folder = string.Format("{0}{1}{2}{1}{2}.Model.DTO{1}{2}", projectFolder, folderSeperate, mvcProjectName),
                Namespace = string.Format("{0}.Model.DTO", mvcProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "DTO"),
                ModelExtension = "DTO", 
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 6,
                JobName = FileGenType.MVC_Service,
                FileGenType = FileGenType.MVC_Service,
                ProjectName = mvcProjectName,
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "Service"),
                Folder = string.Format("{0}{1}{2}{1}{2}.Service{1}{2}", projectFolder, folderSeperate, mvcProjectName),
                Namespace = string.Format("{0}.Service", mvcProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "Service"),
                ModelExtension = "Service",
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 7,
                JobName = FileGenType.MVC_ViewModel,
                FileGenType = FileGenType.MVC_ViewModel,
                ProjectName = mvcProjectName,
                Namespace = string.Format("{0}.ViewModel", mvcProjectName),
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "ViewModel"),
                Folder = string.Format("{0}{1}{2}{1}{2}.ViewModel{1}{2}", projectFolder, folderSeperate, mvcProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "ViewModel"),
                ModelExtension = "Controller",
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 8,
                JobName = FileGenType.MVC_Controller,
                FileGenType = FileGenType.MVC_Controller,
                ProjectName = mvcProjectName,
                Namespace = string.Format("{0}.Controllers", mvcProjectName),
                ModelName = modelName,
                FileName = string.Format("{0}{1}.cs", modelName, "Controller"),
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Controllers{1}{2}", projectFolder, folderSeperate, mvcProjectName),
                GenClassName = string.Format("{0}{1}", modelName, "Controller"),
                ModelExtension = "ViewModel",
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 9,
                JobName = FileGenType.MVC_View_SimpleAdminList,
                FileGenType = FileGenType.MVC_View_SimpleAdminList,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "Admin.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 10,
                JobName = FileGenType.MVC_View_SimpleAdminJScript,
                FileGenType = FileGenType.MVC_View_SimpleAdminJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_AdminJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });

            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 11,
                JobName = FileGenType.MVC_View_SimpleAdminTableHtml,
                FileGenType = FileGenType.MVC_View_SimpleAdminTableHtml,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = string.Format("_Table{0}.cshtml",modelName),
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 12,
                JobName = FileGenType.MVC_View_ModelJScript,
                FileGenType = FileGenType.MVC_View_ModelJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_"+modelName+"JScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 12,
                JobName = FileGenType.MVC_View_ViewModelJScript,
                FileGenType = FileGenType.MVC_View_ViewModelJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_" + modelName + "ViewModelJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 12,
                JobName = FileGenType.MVC_View_ViewModelsJScript,
                FileGenType = FileGenType.MVC_View_ViewModelsJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_" + modelName + "ViewModelsJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 12,
                JobName = FileGenType.MVC_View_PageViewModelsJScript,
                FileGenType = FileGenType.MVC_View_PageViewModelsJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_" + modelName + "PageViewModelsJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 13,
                JobName = FileGenType.MVC_View_ArrayPageHtml,
                FileGenType = FileGenType.MVC_View_ArrayPageHtml,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_ArrPage" + modelName + ".cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 14,
                JobName = FileGenType.MVC_Admin_Resource,
                FileGenType = FileGenType.MVC_Admin_Resource,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "Admin.resx",
                Folder = string.Format("{0}{1}{2}{1}{2}.Resource{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 9,
                JobName = FileGenType.MVC_View_SimpleCreateView,
                FileGenType = FileGenType.MVC_View_SimpleCreateView,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "Create.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 10,
                JobName = FileGenType.MVC_View_SimpleCreateJScript,
                FileGenType = FileGenType.MVC_View_SimpleCreateJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_CreateJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 9,
                JobName = FileGenType.MVC_View_SimpleDetailView,
                FileGenType = FileGenType.MVC_View_SimpleDetailView,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "Detail.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 10,
                JobName = FileGenType.MVC_View_SimpleDetailJScript,
                FileGenType = FileGenType.MVC_View_SimpleDetailJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_DetailJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 11,
                JobName = FileGenType.MVC_View_SimpleEditView,
                FileGenType = FileGenType.MVC_View_SimpleEditView,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "Edit.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 10,
                JobName = FileGenType.MVC_View_SimpleEditJScript,
                FileGenType = FileGenType.MVC_View_SimpleEditJScript,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_EditJScript.cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            listGenInfo.Add(new GenCodeInfo
            {
                OrderNum = 10,
                JobName = FileGenType.MVC_View_SimpleFormControl,
                FileGenType = FileGenType.MVC_View_SimpleFormControl,
                ProjectName = mvcProjectName,
                Namespace = string.Empty,
                ModelName = modelName,
                FileName = "_Form" + modelName + ".cshtml",
                Folder = string.Format("{0}{1}{2}{1}{2}{1}Views{1}{3}", projectFolder, folderSeperate, mvcProjectName, modelName),
                GenClassName = string.Empty,
                ModelExtension = string.Empty,
                IsGen = true
            });
            #endregion
        }
    }
    public static class FileGenType
    {
       public  const string API_DTO = "API_DTO";
       public  const  string API_Repository = "API_Repository";
       public  const  string API_Logic = "API_Logic";
       public  const  string API_Controller = "API_Controller";

       public  const  string MVC_DTO = "MVC_DTO";
       public  const  string MVC_Service = "MVC_Service";
       public  const  string MVC_Controller = "MVC_Controller";
       public  const  string MVC_ViewModel = "MVC_ViewModel";

       public  const  string MVC_View_SimpleAdminList = "MVC_Simple Admin";
       public  const  string MVC_View_SimpleAdminJScript = "MVC_Simple Admin JScript";
       public const string MVC_View_SimpleAdminTableHtml = "MVC_Simple Admin TableHtml";
       public const string MVC_View_ModelJScript = "MVC_Model JScript";
       public const string MVC_View_ViewModelJScript = "MVC_ViewModel JScript";
       public const string MVC_View_ViewModelsJScript = "MVC_ViewModels JScript";
       public const string MVC_View_PageViewModelsJScript = "MVC_Page ViewModels JScript";
       public const string MVC_View_ArrayPageHtml = "MVC_Array Page Html";
       public const string MVC_Admin_Resource = "MVC_Admin Resource";
       public const string MVC_View_SimpleCreateView = "MVC_Simple Create";
       public const string MVC_View_SimpleCreateJScript = "MVC_Simple Create JScript";
       public const string MVC_View_SimpleEditView = "MVC_Simple Edit";
       public const string MVC_View_SimpleEditJScript = "MVC_Simple Edit JScript";
       public const string MVC_View_SimpleDetailView = "MVC_Simple Detail";
       public const string MVC_View_SimpleDetailJScript = "MVC_Simple Detail JScript";
       public const string MVC_View_SimpleFormControl = "MVC_Simple Form Control";
    }
}

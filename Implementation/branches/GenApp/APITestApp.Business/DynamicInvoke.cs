﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace APITestApp.Business
{
    public class DynaInvoke
    {
        // this way of invoking a function

        // is slower when making multiple calls

        // because the assembly is being instantiated each time.

        // But this code is clearer as to what is going on

        public static Object InvokeMethodSlow(string AssemblyName,
               string ClassName, string MethodName, Object[] cArgs, Object[] mArgs)
        {
            // load the assemly

            Assembly assembly = Assembly.LoadFile(AssemblyName);

            // Walk through each type in the assembly looking for our class

            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsClass == true)
                {
                    if (type.FullName.EndsWith("." + ClassName))
                    {
                        // create an instance of the object

                        //object ClassObj = Activator.CreateInstance(type);
                        object ClassObj = Activator.CreateInstance(type, cArgs);

                        // Dynamically Invoke the method

                        object Result = type.InvokeMember(MethodName,
                          BindingFlags.Default | BindingFlags.InvokeMethod,
                               null,
                               ClassObj,
                               mArgs);
                        return (Result);
                    }
                }
            }
            throw (new System.Exception("could not invoke method"));
        }

        // ---------------------------------------------

        // now do it the efficient way

        // by holding references to the assembly

        // and class


        // this is an inner class which holds the class instance info
        public class DynaClassInfo
        {
            public Type type;
            public Object ClassObject;

            public DynaClassInfo()
            {
            }

            public DynaClassInfo(Type t, Object c)
            {
                type = t;
                ClassObject = c;
            }
        }


        public static Hashtable AssemblyReferences = new Hashtable();
        public static Hashtable ClassReferences = new Hashtable();
        //Init Assembly Instance
        public static bool InitAssemblyInstance(string assemblyName)
        {
            if (AssemblyReferences.ContainsKey(assemblyName)) return true;
            try
            {
                Assembly assembly = Assembly.LoadFile(assemblyName);
                if (assembly != null)
                {
                    AssemblyReferences.Add(assemblyName, assembly);
                    return true;
                }
            }
            catch 
            {
            }
            return false;
        }
        //Get a class instance from assembly
        public static bool InitClassInstance(string assemblyName, string className, Object[] cArgs)
        {
            bool isSuccess = true;
            if (ClassReferences.ContainsKey(string.Format("{0}.{1}",assemblyName,className)) == false)
            {
                Assembly assembly;
                if (AssemblyReferences.ContainsKey(assemblyName) == false)
                {
                    isSuccess = InitAssemblyInstance(assemblyName);
                    if (!isSuccess) return false;
                }
                assembly = (Assembly)AssemblyReferences[assemblyName];

                // Walk through each type in the assembly

                foreach (Type type in assembly.GetTypes())
                {
                    if (type.IsClass == true)
                    {
                        // doing it this way means that you don't have
                        // to specify the full namespace and class (just the class)
                        if (type.FullName.EndsWith("." + className))
                        {
                            Object classInstance = Activator.CreateInstance(type, cArgs);
                            if (classInstance == null) return false;

                            DynaClassInfo ci = new DynaClassInfo(type,
                                                classInstance);
                            ClassReferences.Add(string.Format("{0}.{1}", assemblyName,className), ci);
                        }
                    }
                }
            }
            return true;
        }
        //Get a class reference from ClassReferences
        public static DynaClassInfo
               GetClassReference(string AssemblyName, string ClassName)
        {
            if (ClassReferences.ContainsKey(string.Format("{0}.{1}",AssemblyName,ClassName)) == false)
            {
                return null;
            }
            return ((DynaClassInfo)ClassReferences[string.Format("{0}.{1}", AssemblyName, ClassName)]);
        }
        //Get a class reference from ClassReferences
        public static Assembly
               GetAssembly(string AssemblyName)
        {
            if (AssemblyReferences.ContainsKey(AssemblyName) == false)
            {
                return null;
            }
            return ((Assembly)ClassReferences[AssemblyName]);
        }
        public static Object InvokeMethod(DynaClassInfo ci,
                             string MethodName, Object[] args)
        {
            // Dynamically Invoke the method

            Object Result = ci.type.InvokeMember(MethodName,
              BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod,
                   null,
                   ci.ClassObject,
                   args);
            return (Result);
        }

        // --- this is the method that you invoke ------------
        public static Object InvokeMethod(string AssemblyName,
               string ClassName, string MethodName, Object[] args)
        {
            DynaClassInfo ci = GetClassReference(AssemblyName, ClassName);
            return (InvokeMethod(ci, MethodName, args));
        }
    }
}

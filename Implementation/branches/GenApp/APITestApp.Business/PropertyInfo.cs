﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.Business
{
    public class PropertyGenInfo
    {
        public string ClassName    { get; set; }
        public string PropertyName { get; set; }
        public string DataTypeName { get; set; }
        public string SystemType   { get; set; }
        public bool   IsNullable   {get;set;}
        public bool   IsGen        { get; set; }
        public string ControlType { get; set; }
        public bool IsPrimaryKey { get; set; }
    }
}

﻿using System;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;

namespace APITestApp.Business
{
    public class TestRecord
    {
        public int OrderNo { get; set; }
        public string TestName { get; set; }
        public string AssemblyName { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public bool IsPassed { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsRun { get; set; }
        public List<ErrorMessage> ErrorList { get; set; }
        public TestRecord()
        {
            this.ErrorList = new List<ErrorMessage>();
        }

        public void AddErrorMessage(string type, string content, string tip)
        {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.Type = type;
            errorMessage.Content = content;
            errorMessage.Tip = tip;
            ErrorList.Add(errorMessage);
        }

        public void Run()
        {
            if (!IsRun) return;
            Description = "Tested";
            IsPassed = (bool)DynaInvoke.InvokeMethod(AssemblyName, ClassName, MethodName, new Object[] {this });
        }
    }

    public class ErrorMessage
    {
        public string Type { get; set; }
        public string Content { get; set; }
        public string Tip { get; set; }
    }
}
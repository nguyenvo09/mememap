﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APITestApp.Business
{
    public static class CommonService
    {
        public static string ConvertToSystemType(string dataType,out bool isNullable,out string controlType) {
            isNullable = false;
            controlType = string.Empty;
            bool isCollection = false;
            string returnType = string.Empty;
            if (dataType.Contains("Collection")) { isCollection = true; }
            if(isCollection)return string.Empty;
            if( dataType.Contains("Nullable")){ isNullable = true; }
            #region Convert Simple Type
            if (dataType.Contains("Int32"))  
            {
                
                returnType = "int";
                controlType = "TextBox";
            }
            else if (dataType.Contains("Int64"))
            {
                returnType = "long";
                controlType = "TextBox";
            }
            else if (dataType.Contains("Decimal"))
            {
                returnType = "decimal";
                controlType = "TextBox";
            }
            else if (dataType.Contains("Double"))
            {

                returnType = "double";
                controlType = "TextBox";
            }
            else if (dataType.Contains("String"))
            {
                returnType = "string";
                controlType = "TextBox";
            }
            else if (dataType.Contains("Boolean"))
            {
                returnType = "bool";
                controlType = "TextBox";
            }
            else if (dataType.Contains("Guid"))
            {
                returnType = "Guid";
                controlType = string.Empty;
            }
            else if (dataType.Contains("DateTime"))
            {
                returnType = "DateTime";
                controlType = "DateTimePicker";
            }
            #endregion
            if (returnType!=string.Empty&&isNullable)
            {
                if (returnType != "string") returnType += "?";
            }
            return returnType;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class EvaluationInput
    {
        private RequestInstanceDTO _RequestInstance;
        public RequestInstanceDTO RequestInstance
        {
            get { return _RequestInstance; }
            set { _RequestInstance = value; }
        }


        public EvaluationInput(RequestInstanceDTO _RequestInstance)
        {
            this.RequestInstance = _RequestInstance;
        }

        private bool _EvaluatePassed;

        public bool EvaluatePassed
        {
            get { return _EvaluatePassed; }
            set { _EvaluatePassed = value; }
        }

        public RequestDTO FindRequest()
        {
            RequestDTO requestdto = LogicLayerInstance.Current.requestlogic.Get((int)RequestInstance.RequestID);
            return requestdto;
        }

        public RequestTravelDTO FindRequestTravel ()
        {
            
                RequestTravelDTO requesttraveldto = LogicLayerInstance.Current.requesttravellogic.Get((int)RequestInstance.RequestTravelID);
                return requesttraveldto;
            
            
            //Depend of fieldcode choose the right entity . Right now only get Request
            /*if (RequestInstance.RequestID != null)
            {
                return RequestInstance.
            }

            if (RefType == (int)WFStepsPersistenceParticipant.ReferenceType.Step)
            {
                return RequestInstance;
            }
            if (RefType == (int)WFStepsPersistenceParticipant.ReferenceType.Entity)
            {
                return RequestInstance;
            }
            if (RefType == (int)WFStepsPersistenceParticipant.ReferenceType.ApprovalEntity)
            {
                return RequestInstance;
            }
            return null;*/
        }
    }
}

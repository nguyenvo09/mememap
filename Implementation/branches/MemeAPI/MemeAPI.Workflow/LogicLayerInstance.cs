﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MemeAPI.Logic;
using BaseAPI.UserLogic;

namespace MemeAPI.Workflow
{
    public class LogicLayerInstance : MemeAPI.Workflow.ILogicLayerInstance
    {
        public static LogicLayerInstance Current ;

        public LogicLayerInstance()
        {
        }

        public LogicLayerInstance(
            IWFSettingLogic workflowlogic,
            IRequestInstanceLogic requestinstancelogic, 
            IWFSettingStepLogic wfsettingsteplogic, 
            IWFSettingStepApprovalLogic wfsettingstepapprovallogic, 
            IQueueLogic queuelogic,
            IWFApprovalTrackingLogic approvaltrackinglogic,
            IWorkFlowInstanceLogic wfinstancelogic, 
            IRequestLogic requestlogic, 
            IUserInstanceLogic userinstancelogic,
            IViewPDRequestLogic viewpdrequestlogic, 
            IProfileLogic profilelogic,
            IRequestTravelLogic requesttravellogic,
            IWFSettingStepConditionLogic wfsettingstepconditionlogic,
            IWFEmailTemplateLogic wfemailtemplatelogic,
            IAlternativeDelegateLogic alternativedelegatelogic
            )
        {
            this.workflowlogic = workflowlogic;
            this.requestinstancelogic = requestinstancelogic;
            this.wfsettingstepapprovallogic = wfsettingstepapprovallogic;
            this.queuelogic = queuelogic;
            this.approvaltrackinglogic = approvaltrackinglogic;
            this.wfinstancelogic = wfinstancelogic;
            this.requestlogic = requestlogic;
            this.userinstancelogic = userinstancelogic;
            this.viewpdrequestlogic = viewpdrequestlogic;
            this.profilelogic = profilelogic;
            this.requesttravellogic = requesttravellogic;
            this.wfsettingsteplogic = wfsettingsteplogic;
            this.wfsettingstepconditionlogic = wfsettingstepconditionlogic;
            this.wfemailtemplatelogic = wfemailtemplatelogic;
            this.alternativedelegatelogic = alternativedelegatelogic;
        }

        public IWFSettingStepApprovalLogic wfsettingstepapprovallogic;
        public IWFSettingLogic workflowlogic;
        public IRequestInstanceLogic requestinstancelogic;
        public IQueueLogic queuelogic;
        public IWFApprovalTrackingLogic approvaltrackinglogic;
        public IWorkFlowInstanceLogic wfinstancelogic;
        public IRequestLogic requestlogic;
        public IUserInstanceLogic userinstancelogic;
        public IViewPDRequestLogic viewpdrequestlogic;
        public IProfileLogic profilelogic;
        public IRequestTravelLogic requesttravellogic;
        public IWFSettingStepLogic wfsettingsteplogic;
        public IWFSettingStepConditionLogic wfsettingstepconditionlogic;
        public IWFEmailTemplateLogic wfemailtemplatelogic;
        public IAlternativeDelegateLogic alternativedelegatelogic;

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;


namespace MemeAPI.Workflow
{
    public class WFInstance : WFStepsActivity
    {
        private string _workflowDefinition;

        public string WorkflowDefinition
        {
            get { return _workflowDefinition; }
            set { _workflowDefinition = value; }
        }

        public WFInstance():base()
        {

        }

        private int _PrimaryEntityID;

        public int PrimaryEntityID
        {
            get { return _PrimaryEntityID; }
            set { _PrimaryEntityID = value; }
        }


        public WFInstance(int WorkflowInstanceID, int PrimaryEntityID)
        {
            this.WorkflowInstanceID = WorkflowInstanceID;
            this.PrimaryEntityID = PrimaryEntityID;
        }


        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            RequestInstanceDTO requestinstance = LogicLayerInstance.Current.requestinstancelogic.Get(PrimaryEntityID);



            WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();
            persist.WorkflowInstanceID = this.WorkflowInstanceID;
            persist.PrimaryEntityID = this.PrimaryEntityID;
            persist.RequestInstance = requestinstance;
          
            ScheduleActivities(context);

        }

    }
}

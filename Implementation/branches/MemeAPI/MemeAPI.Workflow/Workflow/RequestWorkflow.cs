﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Threading;

using MemeAPI.Model.DTO;
using BaseAPI.Model.DTO;
using System.Collections;
using System.Workflow.Activities.Rules;
using Newtonsoft.Json;


namespace MemeAPI.Workflow
{
    public class RequestWorkflow
    {
        public RequestWorkflow()
        {
        }

        public static bool StartResumeWorkflow(WFApprovalTrackingDTO approval)
        {

            bool Completed = false;
            AutoResetEvent instanceUnloaded = new AutoResetEvent(false);

            List<AddedParam> addedparames = new List<AddedParam>();
            AddedParam param1 = new AddedParam();
            param1.Key = "WFInstanceGUID"; param1.Value = approval.WFInstanceSystemID.ToString();
            addedparames.Add(param1);

            WorkFlowInstanceDTO workflowinstance = LogicLayerInstance.Current.wfinstancelogic.Get(addedparames);

            WFStepsActivity workflow = WorkflowFactory.Current.BuildWorkflow(workflowinstance, approval.RequestInstanceID);
            WorkflowApplication wf = new WorkflowApplication(workflow);
            wf.PersistableIdle = (arg) =>
            {

                return PersistableIdleAction.Unload;
            };
            wf.Unloaded = (arg) =>
            {
                instanceUnloaded.Set();
            };
            wf.OnUnhandledException = (arg) =>
            {
                Completed = true;
                return UnhandledExceptionAction.Abort;
            };
            wf.Completed = (arg) =>
            {
                Completed = true;
            };
            wf.InstanceStore = WFInstanceStore.GetInstanceStore();
            WFStepsPersistenceParticipant persist = new WFStepsPersistenceParticipant();
            wf.Extensions.Add(persist);

            Guid wfinstanceguid = (Guid)approval.WFInstanceSystemID;
            wf.Load(wfinstanceguid);
            wf.ResumeBookmark(wfinstanceguid.ToString(), approval);
            instanceUnloaded.WaitOne();
            return Completed;
        }


        public static bool StartRequestWorkflow(int RequestID)
        {
            //log4net.ILog log = log4net.LogManager.GetLogger("StartRequestWorkflow ID = " + RequestID);            
            List<AddedParam> addedparames = new List<AddedParam>();
            AddedParam param1 = new AddedParam();
            param1.Key = "RequestID"; param1.Value = RequestID;
            addedparames.Add(param1);

            RequestInstanceDTO requestinstance = LogicLayerInstance.Current.requestinstancelogic.Get(addedparames);
            ////log.Info("Start Submit Request Number: " + requestinstance.RequestNumber);

            return RequestWorkflow.StartRequestWorkflow(requestinstance);

        }

        public static bool StartRequestWorkflow(RequestDTO request)
        {
            List<AddedParam> addedparames = new List<AddedParam>();
            AddedParam param1 = new AddedParam();
            param1.Key = "RequestID"; param1.Value = request.RequestID;
            addedparames.Add(param1);

            RequestInstanceDTO requestinstance = LogicLayerInstance.Current.requestinstancelogic.Get(addedparames);
            return StartRequestWorkflow(requestinstance);

        }
        public static bool StartRequestWorkflow(RequestInstanceDTO requestinstance)
        {
            List<AddedParam> addedparames = new List<AddedParam>();
            //log4net.ILog log = log4net.LogManager.GetLogger("StartRequestWorkflow");
            ////log.Info("Start WorkFlow");
            AutoResetEvent instanceUnloaded = new AutoResetEvent(false);

            if (requestinstance != null && (requestinstance.HubID == null || requestinstance.HubID <= 0)) return false;

            #region Get List Workflow Setting in Hub Request and select a workflow to run
            addedparames.Add(new AddedParam { Key = "HubID", Value = requestinstance.HubID });
            addedparames.Add(new AddedParam { Key = "WFEntityID", Value = 2 }); //entity
            addedparames.Add(new AddedParam { Key = "IsDeleted", Value = false });
            addedparames.Add(new AddedParam { Key = "Active", Value = true });
            List<WFSettingDTO> listWFSetting = LogicLayerInstance.Current.workflowlogic.GetMany(addedparames).ToList();
            if (listWFSetting.Count == 0) return false;
            int wfsettingid = SelectWorkflowSetting(listWFSetting, requestinstance);
            if (wfsettingid == 0) return false;
            WFSettingDTO wfsetting = listWFSetting.First(u => u.WFSettingID == wfsettingid);
            ////log.Info("WFName_" + wfsetting.Name + " " + requestinstance.RequestNumber);
            if (wfsetting == null) return false;
            #endregion
            #region Copy workflow setting data
            string wfdata = LogicLayerInstance.Current.wfinstancelogic.SerializeWFSettingData(wfsetting.WFSettingID);
            WFSettingDTO workflowsetting = LogicLayerInstance.Current.wfinstancelogic.DesializeWFSettingData(wfdata);
            addedparames = new List<AddedParam>();
            addedparames.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
            WorkFlowInstanceDTO workflowinstance = LogicLayerInstance.Current.wfinstancelogic.Get(addedparames);
            if (workflowinstance == null) workflowinstance = new WorkFlowInstanceDTO();
            workflowinstance.WFSetting = workflowsetting;
            workflowinstance.RequestInstanceID = requestinstance.RequestInstanceID;
            workflowinstance.WFSettingID = workflowsetting.WFSettingID;
            workflowinstance.WFSettingData = wfdata;
            workflowinstance.WorkFlowInstanceID = LogicLayerInstance.Current.wfinstancelogic.SaveOrUpdate(workflowinstance);
            #endregion
            #region Build workflow process data
            //update wokflow process data if the workflow is on the first step wait for approval
            string processdata = BuildWFProcessData(wfsettingid);
            if (!string.IsNullOrWhiteSpace(processdata))
            {
                List<AddedParam> param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
                param.Add(new AddedParam { Key = "WFApprovalProcessData", Value = processdata });
                LogicLayerInstance.Current.requestinstancelogic.Update(param);
            }
            #endregion
            #region Buid workflow foundation configuration
            WFStepsActivity workflow = WorkflowFactory.Current.BuildWorkflow(workflowinstance, requestinstance.RequestInstanceID);
            WorkflowApplication wf = new WorkflowApplication(workflow);

            wf.InstanceStore = WFInstanceStore.GetInstanceStore();

            WFStepsPersistenceParticipant persist = new WFStepsPersistenceParticipant();
            wf.Extensions.Add(persist);
            #region Create owner data workflowinstance

            #endregion
            #region Remove old workflow approval tracking
            //addedparames = new List<AddedParam>();
            //addedparames.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
            //List<WFApprovalTrackingDTO> listOldTracking = LogicLayerInstance.Current.approvaltrackinglogic.GetMany(addedparames).ToList();

            //foreach (var ot in listOldTracking)
            //{
            //    LogicLayerInstance.Current.approvaltrackinglogic.Delete(listOldTracking);
            //}
            #endregion
            wf.PersistableIdle = (arg) =>
            {
                // //log.Info("WFName_" + wfsetting.Name + "_PersistableIdle and return.");
                return PersistableIdleAction.Unload;
            };

            wf.Unloaded = (arg) =>
            {
                // //log.Info("WFName_" + wfsetting.Name + "_Unloaded and return.");
                instanceUnloaded.Set();
            };
            wf.OnUnhandledException = (arg) =>
            {
                //Delete Pending Workflow
                ////log.Info("WFName_" + wfsetting.Name + "_OnUnhandledException and return.");
                return UnhandledExceptionAction.Abort;
            };

            wf.Completed = (arg) =>
            {
                if (arg.CompletionState == ActivityInstanceState.Closed)
                {
                    ////log.Info("WFName_" + wfsetting.Name + "_Completed and return.");
                    persist.WorklflowCompleted = true;
                }
            };
            #endregion
            workflowinstance.WFInstanceGUID = wf.Id;
            LogicLayerInstance.Current.wfinstancelogic.SaveOrUpdate(workflowinstance);
            #region Run Workflow and wait for the complete
            wf.Run();

            instanceUnloaded.WaitOne();
            #endregion
            return persist.WorklflowCompleted;
        }

        public static int SelectWorkflowSetting(List<WFSettingDTO> listWFSetting, RequestInstanceDTO requestinstance)
        {
            List<AddedParam> param = new List<AddedParam>();
            foreach (var wfsetting in listWFSetting)
            {
                #region get firt check condition
                param = new List<AddedParam>();
                param.Add(new AddedParam { Key = "WFFirstConditionStep", Value = wfsetting.WFSettingID });
                WFSettingStepDTO firstconditionstep = LogicLayerInstance.Current.wfsettingsteplogic.Get(param);
                if (firstconditionstep != null)
                {
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "WFSettingStepID", Value = firstconditionstep.WFSettingStepID });
                    firstconditionstep.WFSettingStepConditions = LogicLayerInstance.Current.wfsettingstepconditionlogic.GetMany(param).ToList();
                    if (CheckCondition(firstconditionstep, requestinstance))
                    {
                        return wfsetting.WFSettingID;
                    }
                }
                #endregion
            }
            return 0;
        }
        private static bool CheckCondition(WFSettingStepDTO stepDTO, RequestInstanceDTO requestinstance)
        {
            List<WFSettingStepConditionDTO> conditions = stepDTO.WFSettingStepConditions;
            bool isPass = false;

            if (conditions == null) return true;

            if (conditions.Count > 0 && conditions[0].LogicalOperator.Equals("Approval"))
            {
                isPass = true;
            }
            else
            {
                if (conditions.Count == 0)
                {
                    isPass = true;
                }
                else
                {
                    EvaluationInput input = new EvaluationInput(requestinstance);

                    string findtype = "FindRequestTravel";
                    if (requestinstance.RequestID != null)
                    {
                        findtype = "FindRequest";
                    }
                    RuleSet ruleSet = RuleFactory.Current.BuildRuleSet(conditions, null, findtype);
                    RuleValidation validation = new RuleValidation(input.GetType(), null);
                    RuleExecution execution = new RuleExecution(validation, input);
                    if (ruleSet.Validate(validation))
                        ruleSet.Execute(execution);
                    isPass = input.EvaluatePassed;
                }
            }
            return isPass;
        }
        public static string BuildWFProcessData(int wfsettingid)
        {

            JsonSerializer serializer = new JsonSerializer();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            serializer.Serialize(writer, BuildApprovalProcess(wfsettingid));
            return sb.ToString();
        }
        private static List<WFApprovalInfo> BuildApprovalProcess(int wfsettingid)
        {
            List<AddedParam> param = new List<AddedParam>();
            List<WFApprovalInfo> ltracking = new List<WFApprovalInfo>();
            param.Add(new AddedParam { Key = "WFSettingID", Value = wfsettingid });
            List<WFSettingStepApprovalDTO> listApp = LogicLayerInstance.Current.wfsettingstepapprovallogic.GetMany(param).ToList();
            foreach (var stepApp in listApp)
            {
                WFApprovalInfo appInf = new WFApprovalInfo();
                appInf.ApproverID = stepApp.ApproverID;
                appInf.ApproverName = stepApp.ApproverName;
                appInf.StepName = stepApp.WFSettingStepApprovalName;
                appInf.WFSettingStepID = stepApp.WFSettingStepID;
                ltracking.Add(appInf);
            }
            return ltracking;
        }

    }
}

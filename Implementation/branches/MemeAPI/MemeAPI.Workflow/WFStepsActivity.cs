﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Collections.ObjectModel;

using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public abstract class WFStepsActivity : NativeActivity
    {

        #region Fields

        public WFStepsActivity ParentActivity;

        public Collection<Activity> ChildActivities;

        protected Variable<int> Counter = new Variable<int>("Counter",0);

        public WFSettingStepDTO StepDTO;

        public bool IsStop = false;

        #endregion Fields
        private int _WorkflowInstanceID;

        public int WorkflowInstanceID
        {
            get { return _WorkflowInstanceID; }
            set { _WorkflowInstanceID = value; }
        }

        public static string DecodeString(string value)
        {
            string result = string.Empty;
            result = result.Replace("%25", "%").Replace("%24", "$").Replace("%26", "&");
            result = value.Replace("%7E", "~").Replace("%21", "!").Replace("%23", "#");
            result = result.Replace("%5E", "^").Replace("%28", "(").Replace("%29", ")");
            result = result.Replace("%7B", "{").Replace("%7D", "}").Replace("%5B", "[").Replace("%5D", "]");
            result = result.Replace("%3D", "=").Replace("%7C", "|").Replace("%5C", "\\");
            result = result.Replace("%27", "'").Replace("%22", "\"");
            result = result.Replace("%3B", ";").Replace("%3A", ":").Replace("%3F", "?");
            result = result.Replace("%2C", ",").Replace("%3C", "<").Replace("%3E", ">");
            result = result.Replace("%20", " ").Replace("%A0", " ");
            return result;
        }

        public WFStepsActivity()
        {
            ChildActivities = new Collection<Activity>();
        }



        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            
            foreach (var activity in ChildActivities)
            {
                metadata.AddImplementationChild(activity);
            }

            metadata.AddImplementationVariable(Counter);
        }

        protected void ScheduleActivities(NativeActivityContext context)
        {
            int activityCounter = context.GetValue(Counter);
            context.SetValue(Counter, activityCounter + 1);
            //-1 Stop workflow
            if (!IsStop && activityCounter < ChildActivities.Count)
                context.ScheduleActivity(this.
                ChildActivities[activityCounter],
                OnActivityCompleted);
        }

        protected virtual void OnActivityCompleted(NativeActivityContext context,ActivityInstance completedInstance)
        {
            //WFStepTrackings trackingRecord = new WFStepTrackings();
            try
            {
                //Check for wait condition and check condition
                int activityCounter = context.GetValue(Counter) - 1;
                if (activityCounter >= 0)
                {
                    //WFWaitConditionStepsActivity waitCondition = ChildActivities[activityCounter] as WFWaitConditionStepsActivity;
                    //if (waitCondition != null && !waitCondition.Passed)
                    //{
                    //    context.SetValue(Counter, activityCounter);
                    //}
                    //else if (waitCondition != null && waitCondition.Passed)
                    //{
                    //    trackingRecord.LoadByPrimaryKey(waitCondition.StepID);
                    //    trackingRecord.CompletedOn = DateTime.Now;
                    //    trackingRecord.s_Status = string.Empty;
                    //    trackingRecord.Save();
                    //}
                    //WFCheckConditionStepsActivity checkCondition = ChildActivities[activityCounter] as WFCheckConditionStepsActivity;
                    //if (checkCondition != null)
                    //{
                    //    trackingRecord.LoadByPrimaryKey(checkCondition.StepID);
                    //    trackingRecord.CompletedOn = DateTime.Now;
                    //    trackingRecord.Save();
                    //}
                }
                //Schedule 
                ScheduleActivities(context);
            }
            catch (ApplicationException aex)
            {
                //trackingRecord.Status = (int)WFStepStatus.Failed;
                //trackingRecord.Message = aex.Message;
                throw aex;
            }
            catch (Exception ex)
            {
                //trackingRecord.Status = (int)WFStepStatus.Failed;
                //trackingRecord.Message = "Error.";
                throw ex;
            }
            finally
            {
                //if (trackingRecord.RowCount > 0)
                //    trackingRecord.Save();
            }
        }
    }
}

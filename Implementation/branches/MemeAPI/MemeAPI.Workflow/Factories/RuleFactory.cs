﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities.Rules;
using System.CodeDom;

using MemeAPI.Model.DTO;


namespace MemeAPI.Workflow
{
    public class RuleFactory
    {
        public static RuleFactory Current = new RuleFactory();

        public RuleSet BuildRuleSet(List<WFSettingStepConditionDTO> conditions, System.Activities.NativeActivityContext context, string findtype)
        {
            Rule result = new Rule("Check");
            result.ElseActions.Clear();
            result.ThenActions.Clear();
            CodeBinaryOperatorExpression Cond = new CodeBinaryOperatorExpression();
            Cond.Operator = CodeBinaryOperatorType.BooleanAnd;
            Cond.Right = new CodePrimitiveExpression(true);
            CodeThisReferenceExpression thisRef = new CodeThisReferenceExpression();
            for (int i = 0; i < conditions.Count; i++)
            {
                //Left Operand       
                WFSettingStepConditionDTO condition = conditions[i];


                CodeMethodInvokeExpression WFEntity = new CodeMethodInvokeExpression(thisRef, findtype, new CodeExpression[] {});
                CodeMethodInvokeExpression GetType = new CodeMethodInvokeExpression(WFEntity, "GetType", new CodeExpression[] { });
                CodeMethodInvokeExpression GetProp = new CodeMethodInvokeExpression(GetType, "GetProperty", new CodeExpression[] { new CodePrimitiveExpression(condition.WFEntityFieldCode) });
                CodeMethodInvokeExpression GetValue = new CodeMethodInvokeExpression(GetProp, "GetValue", new CodeExpression[] { WFEntity, new CodePrimitiveExpression(null) });
                CodeExpression LeftOperand = new CodeCastExpression(GetCorrectType(condition.DataType), GetValue);

                if (condition.DataType == "System.DateTime")
                {
                    LeftOperand = new CodePropertyReferenceExpression(LeftOperand, "Date");
                }
                string value = condition.Value;
                object RightValue = GetPropertyValueWithCorrectType(condition.DataType, value.Trim());
                CodePrimitiveExpression rightValue = new CodePrimitiveExpression(RightValue);

                CodeExpression cond1 = GetOperatorExpression(LeftOperand, rightValue, condition.WFOperatorCode);

                if (i == 0)
                {
                    Cond.Left = cond1;
                }
                else
                {
                    CodeBinaryOperatorExpression cond2 = new CodeBinaryOperatorExpression();
                    cond2.Left = Cond;
                    cond2.Operator = GetOperator(condition.LogicalOperator);
                    cond2.Right = cond1;
                    Cond = cond2;
                }
            }

            CodePropertyReferenceExpression resultRef = new CodePropertyReferenceExpression(thisRef, "EvaluatePassed");
            CodePrimitiveExpression trueExpression = new CodePrimitiveExpression(true);
            CodePrimitiveExpression falseExpression = new CodePrimitiveExpression(false);
            CodeAssignStatement assignTrue = new CodeAssignStatement();
            assignTrue.Left = resultRef;
            assignTrue.Right = trueExpression;
            CodeAssignStatement assignFalse = new CodeAssignStatement();
            assignFalse.Left = resultRef;
            assignFalse.Right = falseExpression;

            result.Condition = new RuleExpressionCondition(Cond);
            result.ThenActions.Add(new RuleStatementAction(assignTrue));
            result.ElseActions.Add(new RuleStatementAction(assignFalse));

            RuleSet ruleset = new RuleSet();

            ruleset.ChainingBehavior = RuleChainingBehavior.None;
            ruleset.Rules.Add(result);

            return ruleset;
        }

        private CodeExpression GetOperatorExpression(CodeExpression LeftOperand, CodeExpression RightValue, string Operator)
        {
            CodeExpression result = null;
            CodePrimitiveExpression falseExpression = new CodePrimitiveExpression(false);
            switch (Operator)
            {
                //for string,int,datetime
                case "==":
                //for int,datetime
                case ">":
                case "<":
                case "<=":
                case ">=":
                    result = new CodeBinaryOperatorExpression(LeftOperand, GetOperator(Operator), RightValue);
                    break;
                //for string
                case "Contains":
                    result = new CodeMethodInvokeExpression(LeftOperand, Operator, RightValue);
                    break;
                case "!Contains":
                    CodeMethodInvokeExpression ContainsExpression = new CodeMethodInvokeExpression(LeftOperand, "Contains", RightValue);
                    result = new CodeBinaryOperatorExpression(ContainsExpression, CodeBinaryOperatorType.ValueEquality, falseExpression);
                    break;
                //for string,int,datetime
                case "!=":
                    CodeBinaryOperatorExpression EqualsExpression = new CodeBinaryOperatorExpression(LeftOperand, CodeBinaryOperatorType.ValueEquality, RightValue);
                    result = new CodeBinaryOperatorExpression(EqualsExpression, CodeBinaryOperatorType.ValueEquality, falseExpression);
                    break;
                //for object type
                case "HasId":
                    result = new CodeMethodInvokeExpression(LeftOperand, Operator, RightValue);
                    break;
                case "!HasId":
                    CodeMethodInvokeExpression HasIdExpression = new CodeMethodInvokeExpression(LeftOperand, "HasId", RightValue);
                    result = new CodeBinaryOperatorExpression(HasIdExpression, CodeBinaryOperatorType.ValueEquality, falseExpression);
                    break;
                case "Has":
                    result = new CodeMethodInvokeExpression(LeftOperand, Operator, RightValue);
                    break;
                case "!Has":
                    CodeMethodInvokeExpression HasExpression = new CodeMethodInvokeExpression(LeftOperand, "Has", RightValue);
                    result = new CodeBinaryOperatorExpression(HasExpression, CodeBinaryOperatorType.ValueEquality, falseExpression);
                    break;
            }
            return result;
        }

        private CodeBinaryOperatorType GetOperator(string Operator)
        {
            CodeBinaryOperatorType oper = CodeBinaryOperatorType.ValueEquality;

            if (!string.IsNullOrEmpty(Operator))
            {
                switch (Operator)
                {
                    case "==":
                        oper = CodeBinaryOperatorType.ValueEquality; break;
                    case "!=":
                        oper = CodeBinaryOperatorType.ValueEquality; break;
                    case "<":
                        oper = CodeBinaryOperatorType.LessThan; break;
                    case "<=":
                        oper = CodeBinaryOperatorType.LessThanOrEqual; break;
                    case ">":
                        oper = CodeBinaryOperatorType.GreaterThan; break;
                    case ">=":
                        oper = CodeBinaryOperatorType.GreaterThanOrEqual; break;
                    case "*":
                        oper = CodeBinaryOperatorType.Multiply; break;
                    case "||":
                        oper = CodeBinaryOperatorType.BooleanOr; break;
                    case "&&":
                        oper = CodeBinaryOperatorType.BooleanAnd; break;
                    case "+":
                        oper = CodeBinaryOperatorType.Add; break;
                    case "-":
                        oper = CodeBinaryOperatorType.Subtract; break;
                    case "/":
                        oper = CodeBinaryOperatorType.Divide; break;
                    case "%":
                        oper = CodeBinaryOperatorType.Modulus; break;
                }
            }
            return oper;
        }

        private static string GetCorrectType(string type)
        {
            switch (type)
            {
                case "System.String":
                case "String":
                    return ("System.String");
                    break;
                case "System.Decimal":
                case "Decimal":
                    return ("System.Decimal");
                    break;
                case "System.Double":
                case "Double":
                    return ("System.Double");
                    break;
                case "System.Int32":
                case "Int":
                case "YesNo":
                    return ("System.Int32");
                    break;
                case "System.DateTime":
                case "DateTime":
                    return ("System.DateTime");
                    break;
                case "System.Boolean":
                case "Boolean":
                    return ("System.Boolean");
                    break;

            }
            return ("System.String");
        }

        private static Type GetSystemType(string type)
        {
            switch (type)
            {
                case "System.String":
                case "String":
                    return Type.GetType("System.String");
                    break;
                case "System.Decimal":
                case "Decimal":
                    return Type.GetType("System.Decimal");
                    break;
                case "System.Double":
                case "Double":
                    return Type.GetType("System.Double");
                    break;
                case "System.Int32":
                case "Int":
                case "YesNo":
                    return Type.GetType("System.Int32");
                    break;
                case "System.DateTime":
                case "DateTime":
                    return Type.GetType("System.DateTime");
                    break;
                case "System.Boolean":
                case "Boolean":
                    return Type.GetType("System.Boolean");
                    break;

            }
            return Type.GetType("System.String");
        }

        private static object GetPropertyValueWithCorrectType(string type, string value)
        {
            object objvalue = null;
            switch (type)
            {
                case "System.String": 
                case "String":
                    objvalue = value;
                    break;
                case "System.Decimal":
                case "Decimal":
                    objvalue = Convert.ToDecimal(value);
                    break;
                case "System.Double":
                case "Double":
                    objvalue = Convert.ToDouble(value);
                    break;
                case "System.Int32":
                case "Int":
                    objvalue = Convert.ToInt32(value);
                    break;
                case "System.DateTime":
                case "DateTime":
                    objvalue = Convert.ToDateTime(value, Utilities.GetDefaultDateFormat()).Date;
                    break;
                case "Domain.BusinessBase":
                case "BusinessBase":
                    objvalue = value;
                    break;
                case "System.Collections.IList":
                case "IList":
                    objvalue = value;
                    break;
                case "System.Boolean":
                case "Boolean":
                    objvalue = Convert.ToBoolean(value);
                    break;
                case "YesNo":
                    if (value != null)
                    {
                        if ((value.Equals("Yes") )|| (value.Equals("1")))
                        {
                            objvalue = 1;
                        }
                        else objvalue = 0;
                    }
                    else objvalue = 0;
                    break;
                    
            }

            return objvalue;
        }
    }
}

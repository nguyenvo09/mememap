﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Activities;
using System.IO;
using System.Activities.XamlIntegration;
using System.Xaml;
using System.Activities.Statements;

using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WorkflowFactory
    {
        public static WorkflowFactory Current = new WorkflowFactory();

        
        public WFStepsActivity BuildWorkflow(WorkFlowInstanceDTO workflowinstance, int PrimaryEntityID)
        {
            WFStepsActivity Workflow = new WFInstance(workflowinstance.WorkFlowInstanceID, PrimaryEntityID);

            WFSettingStepDTO StepDTO = new WFSettingStepDTO();
            StepDTO.WFSettingStepID = 0;
            Workflow.StepDTO = StepDTO;
           // log4net.ILog log = log4net.LogManager.GetLogger("BuildWorkflow");
           // //log.Info("Start Build WorkFlow Steps");
            BuildSteps(workflowinstance.WorkFlowInstanceID, workflowinstance.WFSetting.WFSettingSteps, Workflow);
            ////log.Info("Finish Build WorkFlow Steps");

            return Workflow;
        }

        private void BuildSteps(int WorkflowInstanceID,List<WFSettingStepDTO> WFSettingSteps, WFStepsActivity ParentStep)
        {
            //log4net.ILog log = log4net.LogManager.GetLogger("BuildSteps");
            if (WFSettingSteps != null && WFSettingSteps.Count > 0)
            {
                foreach (WFSettingStepDTO step in WFSettingSteps)
                {                    
                    WFStepsActivity thisStep = WFStepFactory.Current.BuildSteps(WorkflowInstanceID, step);

                    thisStep.ParentActivity = ParentStep;
                    ParentStep.ChildActivities.Add(thisStep);
                    ////log.Info(step.WFSettingStepTypeName);
                    BuildSteps(WorkflowInstanceID, step.Children, thisStep);
                }
            }
            return;

            //List<AddedParam> addedParams = new List<AddedParam>();

            //AddedParam param1 = new AddedParam();
            //param1.Key = "WorkflowInstanceID"; param1.Value = WorkflowInstanceID;
            //addedParams.Add(param1);

            //if (parent != null)
            //{
            //    AddedParam param2 = new AddedParam();
            //    param2.Key = "ParentID"; param2.Value = ParentID;
            //    addedParams.Add(param2);
            //}

            //List<WFSettingStepDTO> rObj = LogicLayerInstance.Current.wfsettingsteplogic.GetMany(addedParams).ToList<WFSettingStepDTO>();

            

            
            //if (rObj.Count==0)
            //    return ;

            //for (int i = 0; i < rObj.Count; i++)
            //{
            //    WFStepsActivity thisStep = WFStepFactory.Current.BuildSteps(WorkflowID, rObj[i].WFSettingStepID);

            //    thisStep.ParentActivity = ParentStep;
            //    ParentStep.ChildActivities.Add(thisStep);

            //    BuildSteps(WorkflowID, rObj[i].WFSettingStepID, thisStep);
            //}
        }  
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFStepFactory
    {
        public static WFStepFactory Current = new WFStepFactory();



        private WFStepsActivity BuildStepsByStepType(WFSettingStepDTO step)
        {

            //WFSettingStepTypeDTO stepType = LogicLayerInstance.Current.wfsettingsteptypelogic.Get(step.WFSettingStepTypeID);

            Type StepType = Type.GetType(step.WFSettingStepTypeCode);

            WFStepsActivity result = Activator.CreateInstance(StepType) as WFStepsActivity;

            return result;
        }

        public WFStepsActivity BuildSteps(int WorkflowInstanceID, WFSettingStepDTO step)
        {
            
            WFStepsActivity result = null;
            if (step!=null)
            {
                result = BuildStepsByStepType(step);
                result.StepDTO = step;
                result.WorkflowInstanceID = WorkflowInstanceID;
            }
            return result;
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Web.UI;
using System.Workflow.Activities.Rules;
using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class Utilities
    {
        public const string IsOpen = "Open";
        public const string IsClosed = "Closed";

        public static string DisplayStatus(object data)
        {
            if (!string.IsNullOrEmpty(data.ToString()))
            {
                return (Convert.ToBoolean(data.ToString())) ? IsOpen : IsClosed;
            }
            return string.Empty;
        }


        public static System.Globalization.DateTimeFormatInfo GetDefaultDateFormat()
        {
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "dd/MM/yyyy";
            return dateInfo;
        }


        /// <summary>
        /// Decode string. Viet(22/11/2011) 3:10PM
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DecodeString(string value)
        {
            string result = string.Empty;
            result = value.Replace("%7E", "~").Replace("%21", "!").Replace("%23", "#");
            result = result.Replace("%5E", "^").Replace("%28", "(").Replace("%29", ")");
            result = result.Replace("%7B", "{").Replace("%7D", "}").Replace("%5B", "[").Replace("%5D", "]");
            result = result.Replace("%3D", "=").Replace("%7C", "|").Replace("%5C", "\\");
            result = result.Replace("%27", "'").Replace("%22", "\"");
            result = result.Replace("%3B", ";").Replace("%3A", ":").Replace("%3F", "?");
            result = result.Replace("%2C", ",").Replace("%3C", "<").Replace("%3E", ">");
            result = result.Replace("%24", "$").Replace("%25", "%").Replace("%26", "&");
            result = result.Replace("%20", " ").Replace("%A0", " ");

            return result;
        }

        public object returnDateTimeNowIfNull(object strConvert)
        {
            return (strConvert != DBNull.Value) ? strConvert : null;
        }

        public static DateTime RequestDatetimeNow(object strConvert)
        {
            if (!string.IsNullOrEmpty(strConvert.ToString()))
            {
                return Convert.ToDateTime(strConvert);
            }
            return DateTime.Now;
        }

        
       


        public static string GetDurationUnit(string code)
        {
            switch (code)
            {
                case "D":
                    return "Days";
                case "W":
                    return "Weeks";
                case "H":
                    return "Hours";

            }
            return string.Empty;
        }

        public static int GetInteger(object strNum)
        {
            if (strNum != null)
                try
                {
                    return Convert.ToInt32(strNum);
                }
                catch { return 0; }
            return 0;
        }

        public static byte GetByte(object strNum)
        {
            if (strNum != null)
                try
                {
                    return Convert.ToByte(strNum);
                }
                catch { return 0; }
            return 0;
        }


        public static bool GetBoolean(object strNum)
        {
            if (strNum != null)
                try
                {
                    return Convert.ToBoolean(strNum);
                }
                catch { return false; }
            return false;
        }

        
        public static string ApplicationPath
        {
            get
            {
                return System.IO.Path.Combine(HttpContext.Current.Request.Url.GetLeftPart(System.UriPartial.Authority),
                    HttpContext.Current.Request.ApplicationPath.TrimEnd(new char[] { '/' }));
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Tracking;

namespace MemeAPI.Workflow
{
    public class WFStepsTrackingParticipant : TrackingParticipant
    {
        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            WorkflowInstanceRecord instance = record as WorkflowInstanceRecord;
            if (instance != null)
            {
                
            }
            CustomTrackingRecord custom = record as CustomTrackingRecord;
            if (custom != null)
            {

            }
        }
    }
}

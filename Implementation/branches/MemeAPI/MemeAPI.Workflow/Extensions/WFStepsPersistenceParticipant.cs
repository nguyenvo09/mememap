﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Persistence;
using System.Xml.Linq;

using System.Transactions;
using System.Data;
using System.Data.SqlClient;

using MemeAPI.Model.DTO;
using MemeAPI.Logic;

namespace MemeAPI.Workflow
{
    public class WFStepsPersistenceParticipant : PersistenceParticipant
    {
        public enum ReferenceType
        {
            Entity = 1,
            Step = 2,
            ApprovalEntity = 3
        }

        public bool WorklflowCompleted = false;

        private RequestInstanceDTO _RequestInstance;
        public RequestInstanceDTO RequestInstance
        {
            get { return _RequestInstance; }
            set { _RequestInstance = value; }
        }


        


        private Guid _ApprovalGuid;
        public Guid ApprovalGuid
        {
            get { return _ApprovalGuid; }
            set { _ApprovalGuid = value; }
        }

        private int _PrimaryEntityID;

        public int PrimaryEntityID
        {
            get { return _PrimaryEntityID; }
            set { _PrimaryEntityID = value; }
        }

        private int _WorkflowInstanceID;

        public int WorkflowInstanceID
        {
            get { return _WorkflowInstanceID; }
            set { _WorkflowInstanceID = value; }
        }


        private IDictionary<int, bool> _CheckConditionStatuses = new Dictionary<int, bool>();

        public IDictionary<int, bool> CheckConditionStatuses
        {
            get { return _CheckConditionStatuses; }
            set { _CheckConditionStatuses = value; }
        }

        private int _ApproveStatus;

        public int ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }

        protected override void CollectValues(out IDictionary<XName, object> readWriteValues, out IDictionary<XName, object> writeOnlyValues)
        {
            readWriteValues = new Dictionary<XName, object>(6)
                {
                    {"RequestInstance",RequestInstance},{"WorkflowInstanceID",_WorkflowInstanceID},{"ApprovalGuid",_ApprovalGuid},
                    {"PrimaryEntityID",_PrimaryEntityID}, {"CheckConditionStatuses", CheckConditionStatuses}, {"ApproveStatus", _ApproveStatus}
                };

            writeOnlyValues = null;

            //try
            //{
            //    if (RequestInstance != null)
            //    {
            //        if (LogicLayerInstance.Current.requestinstancelogic != null)
            //        {
            //            LogicLayerInstance.Current.requestinstancelogic.SaveOrUpdate(RequestInstance);
            //        }
            //    }
               
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        protected override void PublishValues(IDictionary<XName, object> readWriteValues)
        {
            object WFInstanceID;
            if (readWriteValues.TryGetValue("WorkflowInstanceID", out WFInstanceID))
            {
                this.WorkflowInstanceID = (int)WFInstanceID;
            }
            object ApprovalGuid;
            if (readWriteValues.TryGetValue("ApprovalGuid", out ApprovalGuid))
            {
                this.ApprovalGuid = (Guid)ApprovalGuid;
            }

            object PrimaryEntityID;
            if (readWriteValues.TryGetValue("PrimaryEntityID", out PrimaryEntityID))
            {
                this.PrimaryEntityID = (int)PrimaryEntityID;
            }
            object RequestInstance;
            if (readWriteValues.TryGetValue("RequestInstance", out RequestInstance))
            {
                this.RequestInstance = (RequestInstanceDTO)RequestInstance;
            }
            object checkConditionStatuses;
            if (readWriteValues.TryGetValue("CheckConditionStatuses", out checkConditionStatuses))
            {
                CheckConditionStatuses = (IDictionary<int, bool>)checkConditionStatuses;
            }

            object approveStatus;
            if (readWriteValues.TryGetValue("ApproveStatus", out approveStatus))
            {
                ApproveStatus = (int)approveStatus;
            }

            RefreshEntity();
        }

        public void RefreshEntity()
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.DurableInstancing;
using System.Runtime.DurableInstancing;
using System.Configuration;

namespace MemeAPI.Workflow
{
    public class WFInstanceStore
    {
        public enum WFEntity
        {
            Request=1
        }

        public static SqlWorkflowInstanceStore GetInstanceStore()
        {
            string sqlPersistenceDBConnectionString = ConfigurationManager.ConnectionStrings["WFPersistenceStore"].ConnectionString;
            SqlWorkflowInstanceStore sqlWFInstanceStore = new SqlWorkflowInstanceStore(sqlPersistenceDBConnectionString);
            InstanceHandle handle = sqlWFInstanceStore.CreateInstanceHandle();
            InstanceView view = sqlWFInstanceStore.Execute(handle, new CreateWorkflowOwnerCommand(), TimeSpan.FromSeconds(25));
            handle.Free();
            sqlWFInstanceStore.DefaultInstanceOwner = view.InstanceOwner;
            return sqlWFInstanceStore;
        }
    }
}

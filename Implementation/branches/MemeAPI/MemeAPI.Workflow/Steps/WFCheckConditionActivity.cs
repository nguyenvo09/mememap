﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Workflow.Activities.Rules;

using MemeAPI.Model.DTO;
using BaseAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFCheckConditionActivity : WFStepsActivity
    {
        public bool Passed;
        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            
            ScheduleActivities(context);
        }

        protected override void OnActivityCompleted(System.Activities.NativeActivityContext context, System.Activities.ActivityInstance completedInstance)
        {
            WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();
            int activityCounter = context.GetValue(Counter) - 1;
            WFConditionalBranchStepsActivity currentBranch = ChildActivities[activityCounter] as WFConditionalBranchStepsActivity;
            if (currentBranch != null)
            {
                currentBranch.Passed = persist.CheckConditionStatuses.ContainsKey(currentBranch.StepDTO.WFSettingStepID) ? persist.CheckConditionStatuses[currentBranch.StepDTO.WFSettingStepID] : false;
                if (!currentBranch.Passed)
                    ScheduleActivities(context);
            }
        }
    }
}

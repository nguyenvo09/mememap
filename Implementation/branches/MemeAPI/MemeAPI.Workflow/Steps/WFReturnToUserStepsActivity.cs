﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities.Rules;
using MemeAPI.Model.DTO;
using BaseAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFReturnToUserStepsActivity : WFStepsActivity
    {
        public bool Passed;
        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            try
            {
                WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();
                RequestInstanceDTO requestinstance = persist.RequestInstance;
                if (requestinstance != null)
                {
                    List<AddedParam> param1 = new List<AddedParam>();
                    param1.Add(new AddedParam { Key = "RequestInstanceID", Value = persist.RequestInstance.RequestInstanceID });
                    WFApprovalTrackingDTO apptracking = LogicLayerInstance.Current.approvaltrackinglogic.GetMany(param1).OrderByDescending(u => u.WFApprovalTrackingID).FirstOrDefault();

                    List<AddedParam> param = new List<AddedParam>();
                    requestinstance.RequestStatusID = 25;
                    param.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
                    param.Add(new AddedParam { Key = "RequestStatusID", Value = 25 });
                    LogicLayerInstance.Current.requestinstancelogic.Update(param);
                    if (requestinstance.RequestID != null)
                    {
                        RequestDTO request = LogicLayerInstance.Current.requestlogic.Get(Convert.ToInt32(requestinstance.RequestID));
                        request.Version = (Convert.ToInt32(request.Version) + 1).ToString();
                        param = new List<AddedParam>();
                        param.Add(new AddedParam { Key = "RequestID", Value = request.RequestID });
                        param.Add(new AddedParam { Key = "Version", Value = request.Version });
                        LogicLayerInstance.Current.requestlogic.Update(param);
                        SendEmail(requestinstance, apptracking);
                    }
                    else
                    {
                    }
                }

            }
            catch (ApplicationException aex)
            {

                throw aex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        protected bool SendEmail(RequestInstanceDTO requestinstance, WFApprovalTrackingDTO apptracking)
        {
            QueueDTO queue = new QueueDTO();
            string email = "";

            RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);
            email = request.RequestorEmail;

            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Code", Value = "RTU" });
            WFEmailTemplateDTO emailtemplate = LogicLayerInstance.Current.wfemailtemplatelogic.Get(param);
            if (emailtemplate != null && request != null && requestinstance != null)
            {
                queue.Body = FormatEmailBody(emailtemplate.Body, requestinstance, request, apptracking);
                queue.Subject = FormatEmailTitle(emailtemplate.Title, requestinstance, request);
                queue.Recipient = email;
                LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
            }
            return true;
        }

        protected string FormatEmailBody(string emailtemplate, RequestInstanceDTO requestinstance, RequestDTO request, WFApprovalTrackingDTO workflowtracking)
        {
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                AddedParam param1 = new AddedParam();
                param1.Key = "RequestID"; param1.Value = (int)requestinstance.RequestID;
                addedparames.Add(param1);

                ViewPDRequestDTO viewpdrequest = LogicLayerInstance.Current.viewpdrequestlogic.Get(addedparames);

                emailtemplate = emailtemplate.Replace("{RequestorName}", request.RequestorName);
                emailtemplate = emailtemplate.Replace("{LevelName}", workflowtracking.Name);
                emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequest.RequestNo);
                emailtemplate = emailtemplate.Replace("{RequestDate}", request.CreatedDate.ToString(BaseAPI.Common.CommonData.DateFormat));
                emailtemplate = emailtemplate.Replace("{Version}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{ApproverName}", workflowtracking.ApproverName);
                emailtemplate = emailtemplate.Replace("{ApproverComments}", DecodeString(workflowtracking.Comments));
            }
            return emailtemplate;
        }
        protected string FormatEmailTitle(string emailtitle, RequestInstanceDTO requestinstance, RequestDTO request)
        {
            emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
            emailtitle = emailtitle.Replace("{Version}", request.Version);
            return emailtitle;
        }

    }
}

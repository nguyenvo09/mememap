﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Workflow.Activities.Rules;

using MemeAPI.Model.DTO;
using BaseAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFConditionalBranchStepsActivity : WFStepsActivity
    {
        public bool Passed;

        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            
            //List<AddedParam> listparams= new List<AddedParam>();
            //AddedParam param = new AddedParam();
            //param.Key = "WFSettingStepID";
            //param.Value = StepID;
            List<WFSettingStepConditionDTO> conditions = StepDTO.WFSettingStepConditions;// LogicLayerInstance.Current.wfsettingstepconditionlogic.GetMany(listparams).ToList<WFSettingStepConditionDTO>();
            WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();

            if (conditions.Count > 0 && conditions[0].LogicalOperator.Equals("Approval"))
            {
                if (conditions[0].Value.Equals("Approved"))
                {
                    persist.CheckConditionStatuses[StepDTO.WFSettingStepID] = (persist.ApproveStatus==0);
                }
                else if (conditions[0].Value.Equals("Rejected"))
                {
                    persist.CheckConditionStatuses[StepDTO.WFSettingStepID] = (persist.ApproveStatus == 1);
                }
                else //Returned
                {
                    persist.CheckConditionStatuses[StepDTO.WFSettingStepID] = (persist.ApproveStatus == 2);
                }
                if (persist.CheckConditionStatuses[StepDTO.WFSettingStepID])
                {
                    ScheduleActivities(context);
                }
            }
            else
            {
                if (conditions.Count ==0)
                {
                    persist.CheckConditionStatuses[StepDTO.WFSettingStepID] = Passed;
                    
                    ScheduleActivities(context);
                }
                else
                {
                    EvaluationInput input = new EvaluationInput(persist.RequestInstance);

                    string findtype = "FindRequestTravel";
                    if (persist.RequestInstance.RequestID != null)
                    {
                        findtype = "FindRequest";
                    }

                    RuleSet ruleSet = RuleFactory.Current.BuildRuleSet(conditions, context, findtype);
                    RuleValidation validation = new RuleValidation(input.GetType(), null);
                    RuleExecution execution = new RuleExecution(validation, input);
                    if (ruleSet.Validate(validation))
                        ruleSet.Execute(execution);

                    Passed = input.EvaluatePassed;
                    persist.CheckConditionStatuses[StepDTO.WFSettingStepID] = Passed;
                    if (input.EvaluatePassed)
                    {

                        ScheduleActivities(context);
                    }
                }
            }
        }
    }
}

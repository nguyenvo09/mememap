﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFSendEmailStepsActivity : WFStepsActivity
    {
        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            // SUppose to send email here
            try
            {
                WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();

                RequestInstanceDTO requestinstance = persist.RequestInstance;
                List<WFSettingStepSendEmailDTO> sendemails = StepDTO.WFSettingStepSendEmails; 
                foreach (WFSettingStepSendEmailDTO sendemail in sendemails)
                {
                    QueueDTO queue = new QueueDTO();
                    string email="";
                    if (!sendemail.SendToRequester.Equals("Other"))
                    {
                        BaseAPI.UserModel.DTO.UserInstanceDTO user = LogicLayerInstance.Current.userinstancelogic.Find(requestinstance.CreatedBy);
                        email = user.Email;
                    }
                    else
                    {
                        email = sendemail.UserEmail;
                    }
                  
                    WFEmailTemplateDTO emailtemplate = LogicLayerInstance.Current.wfemailtemplatelogic.Get((int)sendemail.WFEmailTemplateID);

                    queue.Body = FormatEmailBody(emailtemplate.Body, requestinstance);
                    queue.Subject = FormatEmailTitle(emailtemplate.Title, requestinstance);
                    queue.Recipient = email;
                    LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
                }

            }
            catch (ApplicationException aex)
            {

                throw aex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        protected string FormatEmailBody(string emailtemplate, RequestInstanceDTO requestinstance)
        {
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                AddedParam param1 = new AddedParam();
                param1.Key = "RequestID"; param1.Value = (int)requestinstance.RequestID;
                addedparames.Add(param1);

                ViewPDRequestDTO viewpdrequest = LogicLayerInstance.Current.viewpdrequestlogic.Get(addedparames);

                emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequest.RequestNo);
                emailtemplate = emailtemplate.Replace("{Version}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{CustomerName}", viewpdrequest.CustomerName);
                emailtemplate = emailtemplate.Replace("{DealerName}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{Reason}", viewpdrequest.RequestReasonName);

                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);

                emailtemplate = emailtemplate.Replace("{ValueCUR}", request.DiscountCUR != null ? request.DiscountCUR.ToString() : "0");
                emailtemplate = emailtemplate.Replace("{ValueSEK}", request.DiscountSEK != null ? request.DiscountSEK.ToString() : "0");
            }

            return emailtemplate;
        }

        protected string FormatEmailTitle(string emailtitle, RequestInstanceDTO requestinstance)
        {
            if (requestinstance.RequestID != null)
            {
                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);

                emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
                emailtitle = emailtitle.Replace("{Version}", request.Version);

            }
            return emailtitle;
        }
    }
}

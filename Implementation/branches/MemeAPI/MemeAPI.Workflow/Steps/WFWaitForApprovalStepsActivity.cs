﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;
using Newtonsoft.Json;
using System.Workflow.Activities.Rules;
using System.Configuration;

namespace MemeAPI.Workflow
{
    public class WFWaitForApprovalStepsActivity : WFStepsActivity
    {
        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            try
            {
                WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();

                RequestInstanceDTO requestinstance = persist.RequestInstance;

                Guid genid = Guid.NewGuid();

                WFSettingStepApprovalDTO approval = StepDTO.WFSettingStepApprovals.FirstOrDefault();

                WorkFlowInstanceDTO workflowinstance = LogicLayerInstance.Current.wfinstancelogic.Get(persist.WorkflowInstanceID);

                persist.ApproveStatus = 1; //Default Condition Status use for WaitForApproval is 1

                if (approval != null)
                {
                    List<AddedParam> param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "RequestInstanceID", Value = persist.RequestInstance.RequestInstanceID });
                    WFApprovalTrackingDTO apptracking = LogicLayerInstance.Current.approvaltrackinglogic.GetMany(param).OrderByDescending(u => u.WFApprovalTrackingID).FirstOrDefault();
                    if (apptracking != null)
                    {
                        RequestInstanceDTO requestinstances = persist.RequestInstance;
                        //Send to requestor
                        if (apptracking.Status != "Returned")
                            SendEmail(requestinstance, apptracking);
                    }
                    WFApprovalTrackingDTO approvaltracking = new WFApprovalTrackingDTO();
                    approvaltracking.RowGuid = genid;
                    approvaltracking.WFSettingID = workflowinstance.WFSettingID;

                    approvaltracking.RequestInstanceID = persist.RequestInstance.RequestInstanceID;
                    approvaltracking.WFInstanceSystemID = context.WorkflowInstanceId;
                    approvaltracking.WFInstanceID = workflowinstance.WorkFlowInstanceID;
                    approvaltracking.Status = "Pending";

                    approvaltracking.ApproverID = approval.ApproverID;
                    approvaltracking.ApproverName = approval.ApproverName;
                    approvaltracking.ApprovalDate = DateTime.Now;
                    approvaltracking.Name = approval.WFSettingStepApprovalName;
                    approvaltracking.WFSettingStepID = StepDTO.WFSettingStepID;
                    #region Check if the approver is away
                    List<AddedParam> addedparames = new List<AddedParam>();
                    addedparames.Add(new AddedParam { Key = "UserInstanceID", Value = approval.ApproverID });
                    ProfileDTO approverProfile = LogicLayerInstance.Current.profilelogic.Get(addedparames);

                    #region check effective delegate without profile away
                    if (approverProfile != null)
                    {
                        if (approverProfile.AwayStart.HasValue && approverProfile.AwayEnd.HasValue)
                        {
                            if (approverProfile.AwayStart.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= approverProfile.AwayEnd.Value.Date)
                            {
                                DelegateApproverDTO delegateapprover = approval.DelegateApprovers.FirstOrDefault();
                                
                                if (delegateapprover != null)
                                {
                                    approval.ApproverEmail = delegateapprover.DelegateUserEmail;
                                    approvaltracking.DelegateUserID = delegateapprover.DelegateUserID;
                                    approval.ApproverID = Convert.ToInt32(delegateapprover.DelegateUserID);

                                    approvaltracking.DelegateName = delegateapprover.DelegateUserName;
                                    AlternativeDelegateDTO selectedAlt = null;
                                    #region check if delegator is away then select alternative delegate
                                    addedparames = new List<AddedParam>();
                                    addedparames.Add(new AddedParam { Key = "DelegateApproverID", Value = delegateapprover.DelegateApproverID });
                                    List<AlternativeDelegateDTO> listAlt = LogicLayerInstance.Current.alternativedelegatelogic.GetMany(addedparames).OrderByDescending(u => u.EffectiveDate).ToList();
                                    for (int i = 0; i < listAlt.Count && selectedAlt == null; i++)
                                    {
                                        var alt = listAlt[i];
                                        if (alt.EffectiveDate != null && alt.EffectiveDate.Value <= DateTime.Now.Date && alt.EffectiveDate.Value >= approverProfile.AwayStart.Value.Date && alt.EffectiveDate.Value <= approverProfile.AwayEnd.Value.Date)
                                        {
                                            selectedAlt = alt;
                                        }
                                    }
                                    #endregion
                                    if (selectedAlt != null)
                                    {
                                        approval.ApproverEmail = selectedAlt.DelegateUserEmail;
                                        approval.ApproverID = Convert.ToInt32(selectedAlt.DelegateUserID);
                                        approvaltracking.DelegateUserID = selectedAlt.DelegateUserID;
                                        approvaltracking.DelegateName = selectedAlt.DelegateUserName;
                                    }
                                }
                                if (string.IsNullOrWhiteSpace(approval.ApproverEmail)) //make sure that if no delegate is selected the approver will be set to the setting approver 
                                {
                                    approval.ApproverEmail = approverProfile.Email;
                                    approval.ApproverID = approverProfile.UserInstanceID;
                                    approvaltracking.ApproverID =   approval.ApproverID;
                                    approvaltracking.ApproverName = approval.ApproverName;
                                }
                            }
                        }
                    }
                    #endregion
                    #endregion
                    int approvaltrackingid = LogicLayerInstance.Current.approvaltrackinglogic.SaveOrUpdate(approvaltracking);

                    #region Send mail to Approver get template from workflow setting (APPINFO)
                    if (approval.WFEmailTemplate != null)
                    {
                        
                        QueueDTO queue = new QueueDTO();
                        queue.Body = FormatEmailBody(approval.WFEmailTemplate.Body, requestinstance, genid, context.WorkflowInstanceId, approvaltracking.ApproverName, approval.ApproverID);
                        queue.Subject = FormatEmailTitle(approval.WFEmailTemplate.Title, requestinstance, approval.WFSettingStepApprovalName);
                        queue.Recipient = approval.ApproverEmail;
                        queue.WFApprovalTrackingID = approvaltrackingid;
                        LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
                    }
                    #endregion
                }
                context.CreateBookmark(context.WorkflowInstanceId.ToString(), new System.Activities.BookmarkCallback(OnResumeBookmark));
            }
            catch (ApplicationException aex)
            {

                throw aex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        protected bool SendEmail(RequestInstanceDTO requestinstance, WFApprovalTrackingDTO workflowtracking)
        {
            QueueDTO queue = new QueueDTO();
            string email = "";
            RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);
            email = request.RequestorEmail;
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Code", Value = "APPR" });
            WFEmailTemplateDTO emailtemplate = LogicLayerInstance.Current.wfemailtemplatelogic.Get(param);
            if (emailtemplate != null && request != null && requestinstance != null)
            {
                queue.Body = FormatEmailBody(emailtemplate.Body, requestinstance, request, workflowtracking);
                queue.Subject = FormatEmailTitle(emailtemplate.Title, requestinstance, request);
                queue.Recipient = email;
                LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
            }
            return true;
        }
        protected string FormatEmailBody(string emailtemplate, RequestInstanceDTO requestinstance, RequestDTO request, WFApprovalTrackingDTO workflowtracking)
        {
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                AddedParam param1 = new AddedParam();
                param1.Key = "RequestID"; param1.Value = (int)requestinstance.RequestID;
                addedparames.Add(param1);

                ViewPDRequestDTO viewpdrequest = LogicLayerInstance.Current.viewpdrequestlogic.Get(addedparames);

                emailtemplate = emailtemplate.Replace("{RequestorName}", request.RequestorName);
                emailtemplate = emailtemplate.Replace("{LevelName}", workflowtracking.Name);
                emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequest.RequestNo);
                emailtemplate = emailtemplate.Replace("{RequestDate}", request.CreatedDate.ToString(BaseAPI.Common.CommonData.DateFormat));
                emailtemplate = emailtemplate.Replace("{Version}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{ApproverName}", workflowtracking.ApproverName);
                emailtemplate = emailtemplate.Replace("{ApproverComments}", DecodeString(workflowtracking.Comments));
            }
            return emailtemplate;
        }
        protected string FormatEmailTitle(string emailtitle, RequestInstanceDTO requestinstance, RequestDTO request)
        {
            emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
            emailtitle = emailtitle.Replace("{Version}", request.Version);
            return emailtitle;
        }

        protected string FormatEmailBody(string emailtemplate, RequestInstanceDTO requestinstance, Guid id, Guid? WFInstanceSystemID, string approvername,int approverid)
        {
            //wait for approval
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                addedparames.Add(new AddedParam { Key = "RequestID", Value = requestinstance.RequestID });
                emailtemplate = emailtemplate.Replace("{ApproverName}", approvername);

                List<ViewPDRequestDTO> viewpdrequests = LogicLayerInstance.Current.viewpdrequestlogic.GetMany(addedparames).ToList<ViewPDRequestDTO>();
                if (viewpdrequests.Count > 0)
                {
                    emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequests[0].RequestNo);
                    emailtemplate = emailtemplate.Replace("{Version}", viewpdrequests[0].Version);
                    emailtemplate = emailtemplate.Replace("{CustomerName}", viewpdrequests[0].CustomerName);
                    emailtemplate = emailtemplate.Replace("{DealerName}", viewpdrequests[0].DealerName);
                    emailtemplate = emailtemplate.Replace("{Reason}", viewpdrequests[0].RequestReasonName);

                    emailtemplate = emailtemplate.Replace("{RequestorRemark}", viewpdrequests[0].Remark);

                    /*Calculating avg grossMargin*/
                    double margin = 0; int count = 0; int hasvaluecount=0;
                    foreach (ViewPDRequestDTO viewpdrequest in viewpdrequests)
                    {
                        if (viewpdrequest.GrossMargin.HasValue)
                        {
                            margin += viewpdrequest.GrossMargin.Value;
                            count++;
                            hasvaluecount++;
                        }
                        else
                        {
                            count++;
                        }
                    }
                    if (hasvaluecount > 0)
                    {
                        margin = margin / count;
                        emailtemplate = emailtemplate.Replace("{GrossMargin}", Math.Round(margin, 2, MidpointRounding.AwayFromZero).ToString() + " %");
                    }
                    else
                    {
                        emailtemplate = emailtemplate.Replace("{GrossMargin}", "N.A");
                    }
                }

                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);

                emailtemplate = emailtemplate.Replace("{ValueCUR}", request.DiscountCUR != null ? request.DiscountCUR.ToString() : "0");
                emailtemplate = emailtemplate.Replace("{ValueSEK}", request.DiscountSEK != null ? request.DiscountSEK.ToString() : "0");
                emailtemplate = emailtemplate.Replace("{RequestDate}", request.CreatedDate.ToString(BaseAPI.Common.CommonData.DateFormat));

                ViewPDRequestDTO viewpdrequest1 = viewpdrequests.FirstOrDefault();
                if (viewpdrequest1 != null && viewpdrequest1.RequestReasonName == "Others")
                {
                    emailtemplate = emailtemplate.Replace("{OtherReason}", request.OtherRequestReason).Replace("#", "");
                }
                else
                {
                    emailtemplate = emailtemplate.Replace("#Other Reason: {OtherReason} <br/>#", "");
                }

                List<BaseAPI.UserModel.DTO.AddedParam> addedparames2 = new List<BaseAPI.UserModel.DTO.AddedParam>();
                BaseAPI.UserModel.DTO.AddedParam param2 = new BaseAPI.UserModel.DTO.AddedParam();
                param2.Key = "UserInstanceID"; param2.Value = request.CreatedBy;
                addedparames2.Add(param2);

                BaseAPI.UserModel.DTO.UserInstanceDTO user = LogicLayerInstance.Current.userinstancelogic.GetMany(addedparames2).ToList()[0];
                emailtemplate = emailtemplate.Replace("{RequestorName}", user.FirstName + " " + user.LastName);

                string requestcontent = "Action Options:";
                string approvalpart = "";
                string trackingid = id.ToString();
                if (ConfigurationManager.AppSettings["UseSMTP"] == "false")
                {
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["ExEmailDefault"] + "?subject=Approve-Request-" + trackingid + "-" + approverid + "]" + "\">Approve the Request</a>";
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["ExEmailDefault"] + "?subject=Reject-Request-" + trackingid + "-" + approverid + "\">Reject the Request</a>";
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["ExEmailDefault"] + "?subject=Return-Request-" + trackingid + "-" + approverid + "\">Return to Requestor</a>";
                }
                else
                {
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["SMTPEmailDefault"] + "?subject=Approve-Request-" + trackingid +"-"+approverid+ "\">Approve the Request</a>";
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["SMTPEmailDefault"] + "?subject=Reject-Request-" + trackingid + "-" + approverid + "\">Reject the Request</a>";
                    approvalpart += "<br/>" + "<a href=\"mailto:" + ConfigurationManager.AppSettings["SMTPEmailDefault"] + "?subject=Return-Request-" + trackingid + "-" + approverid + "\">Return to Requestor</a>";
                }
                emailtemplate += requestcontent + approvalpart;
            }
            return emailtemplate;
        }

        protected string FormatEmailTitle(string emailtitle, RequestInstanceDTO requestinstance, string stepname)
        {
            if (requestinstance.RequestID != null)
            {
                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);

                emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
                emailtitle = emailtitle.Replace("{Version}", request.Version);

            }
            return emailtitle + " " + stepname;
        }

        protected string FormatNotifyEmailBody(string emailtemplate, RequestInstanceDTO requestinstance, Guid id, Guid? WFInstanceSystemID, string approvername, string approverremark)
        {
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                AddedParam param1 = new AddedParam();
                param1.Key = "RequestID"; param1.Value = (int)requestinstance.RequestID;
                addedparames.Add(param1);

                ViewPDRequestDTO viewpdrequest = LogicLayerInstance.Current.viewpdrequestlogic.Get(addedparames);

                emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequest.RequestNo);
                emailtemplate = emailtemplate.Replace("{Version}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{CustomerName}", viewpdrequest.CustomerName);
                emailtemplate = emailtemplate.Replace("{DealerName}", viewpdrequest.DealerName);
                emailtemplate = emailtemplate.Replace("{Reason}", viewpdrequest.RequestReasonName);
                emailtemplate = emailtemplate.Replace("{ApproverName}", approvername);
                emailtemplate = emailtemplate.Replace("{Remark}", approverremark);

                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);


                emailtemplate = emailtemplate.Replace("{ValueCUR}", request.DiscountCUR != null ? request.DiscountCUR.ToString() : "0");
                emailtemplate = emailtemplate.Replace("{ValueSEK}", request.DiscountSEK != null ? request.DiscountSEK.ToString() : "0");
                emailtemplate = emailtemplate.Replace("{RequestDate}", request.CreatedDate.ToString(BaseAPI.Common.CommonData.DateFormat));

                List<BaseAPI.UserModel.DTO.AddedParam> addedparames2 = new List<BaseAPI.UserModel.DTO.AddedParam>();
                BaseAPI.UserModel.DTO.AddedParam param2 = new BaseAPI.UserModel.DTO.AddedParam();
                param2.Key = "UserInstanceID"; param2.Value = request.CreatedBy;
                addedparames2.Add(param2);

                BaseAPI.UserModel.DTO.UserInstanceDTO user = LogicLayerInstance.Current.userinstancelogic.GetMany(addedparames2).ToList()[0];
                emailtemplate = emailtemplate.Replace("{RequestorName}", user.FirstName + " " + user.LastName);

            }
            return emailtemplate;
        }

        protected string FormatNotifyEmailTitle(string emailtitle, RequestInstanceDTO requestinstance, string stepname)
        {
            if (requestinstance.RequestID != null)
            {
                RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);

                emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
                emailtitle = emailtitle.Replace("{Version}", request.Version);

            }
            return emailtitle + " " + stepname;
        }

        protected override bool CanInduceIdle
        {
            get
            {
                { return true; }
            }
        }

        public void OnResumeBookmark(
            NativeActivityContext context,
            Bookmark bookmark,
            object obj)
        {

            try
            {
                WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();
                WFApprovalTrackingDTO trackingdto = (WFApprovalTrackingDTO)obj;

                if (trackingdto.Status.Equals("Approved"))
                {
                    persist.ApproveStatus = 0;
                }//CheckConditionStatuses[0]
                else if (trackingdto.Status.Equals("Rejected"))
                {
                    persist.ApproveStatus = 1;
                }
                else
                {
                    persist.ApproveStatus = 2;
                }
            }
            catch (ApplicationException aex)
            {

                throw aex;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }


        private int GetTheNextStepID(int currentStepID)
        {
            for (int i = 0; i < this.ParentActivity.ChildActivities.Count; i++)
            {
                WFStepsActivity step = this.ParentActivity.ChildActivities[i] as WFStepsActivity;
                if (step.StepDTO.WFSettingStepID == currentStepID)
                    return (this.ParentActivity.ChildActivities[i + 1] as WFStepsActivity).StepDTO.WFSettingStepID;
                else continue;
            }
            return 0;
        }

        private int GetThePreviousStepID(int currentStepID)
        {
            for (int i = 0; i < this.ParentActivity.ChildActivities.Count; i++)
            {
                WFStepsActivity step = this.ParentActivity.ChildActivities[i] as WFStepsActivity;
                if (step.StepDTO.WFSettingStepID == currentStepID && i == 0)
                    return -1;
                if (step.StepDTO.WFSettingStepID == currentStepID && i >= 1)
                    return (this.ParentActivity.ChildActivities[i - 1] as WFStepsActivity).StepDTO.WFSettingStepID;
                else continue;
            }
            return -1;
        }

        public string BuildWFProcessData(WFSettingStepDTO wfsettingstep, RequestInstanceDTO requestinstance)
        {
            List<WFApprovalInfo> listApp = new List<WFApprovalInfo>();
            BuildApprovalProcess(wfsettingstep, requestinstance, listApp);
            JsonSerializer serializer = new JsonSerializer();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            serializer.Serialize(writer, listApp);
            return sb.ToString();
        }
        private string BuildApprovalProcess(WFSettingStepDTO wfstep, RequestInstanceDTO requestinstance, List<WFApprovalInfo> ltracking)
        {

            if (wfstep.WFSettingStepApprovals != null && wfstep.WFSettingStepApprovals.Count > 0)
            {
                WFSettingStepApprovalDTO stepApp = wfstep.WFSettingStepApprovals.First();
                WFApprovalInfo appInf = new WFApprovalInfo();
                appInf.ApproverID = stepApp.ApproverID;
                appInf.ApproverName = stepApp.ApproverName;
                appInf.StepName = stepApp.WFSettingStepApprovalName;
                appInf.WFSettingStepID = wfstep.WFSettingStepID;
                ltracking.Add(appInf);
            }
            if (wfstep.Children != null && wfstep.Children.Count > 0)
            {
                foreach (var wfstep1 in wfstep.Children)
                {
                    string result = string.Empty;
                    if (wfstep1.WFSettingStepTypeName == "Approve" || wfstep1.WFSettingStepTypeName == "Reject" || wfstep1.WFSettingStepTypeName == "Return To User") return wfstep1.WFSettingStepTypeName;
                    if (CheckCondition(wfstep1, requestinstance) || wfstep1.WFSettingStepTypeName == "Wait For Approval")
                    {
                        result = BuildApprovalProcess(wfstep1, requestinstance, ltracking);
                        if (result == "Approve" || result == "Reject" || result == "Return To User") return result;
                    }
                }

            }
            return string.Empty;

        }

        private WFSettingStepDTO GetStep(int wfsettingstepid, List<WFSettingStepDTO> steps)
        {

            WFSettingStepDTO step = steps.FirstOrDefault(u => u.WFSettingStepID == wfsettingstepid);
            if (step != null)
            {
                return step;
            }
            else
            {
                foreach (var step1 in steps)
                {

                    if (step1.Children != null)
                    {
                        step = GetStep(wfsettingstepid, step1.Children);
                        if (step != null) return step;
                    }
                }
                return null;
            }
        }

        private bool CheckCondition(WFSettingStepDTO stepDTO, RequestInstanceDTO requestinstance)
        {
            List<WFSettingStepConditionDTO> conditions = stepDTO.WFSettingStepConditions;
            bool isPass = false;

            if (conditions == null) return true;

            if (conditions.Count > 0 && conditions[0].LogicalOperator.Equals("Approval"))
            {
                isPass = true;
            }
            else
            {
                if (conditions.Count == 0)
                {
                    isPass = true;
                }
                else
                {
                    EvaluationInput input = new EvaluationInput(requestinstance);

                    string findtype = "FindRequestTravel";
                    if (requestinstance.RequestID != null)
                    {
                        findtype = "FindRequest";
                    }
                    RuleSet ruleSet = RuleFactory.Current.BuildRuleSet(conditions, null, findtype);
                    RuleValidation validation = new RuleValidation(input.GetType(), null);
                    RuleExecution execution = new RuleExecution(validation, input);
                    if (ruleSet.Validate(validation))
                        ruleSet.Execute(execution);
                    isPass = input.EvaluatePassed;
                }
            }
            return isPass;
        }
    }
}

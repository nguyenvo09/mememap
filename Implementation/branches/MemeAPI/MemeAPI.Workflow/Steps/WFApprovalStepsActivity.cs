﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Workflow.Activities.Rules;

using MemeAPI.Model.DTO;
using BaseAPI.Model.DTO;

namespace MemeAPI.Workflow
{
    public class WFApprovalStepsActivity : WFStepsActivity
    {
        public bool Passed;
        protected override void Execute(System.Activities.NativeActivityContext context)
        {
            try
            {
                WFStepsPersistenceParticipant persist = context.GetExtension<WFStepsPersistenceParticipant>();
                RequestInstanceDTO requestinstance = persist.RequestInstance;

                if (requestinstance != null)
                {
                    List<AddedParam> param = new List<AddedParam>();
                    requestinstance.RequestStatusID = 3;
                    param.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
                    param.Add(new AddedParam { Key = "RequestStatusID", Value = requestinstance.RequestStatusID });
                    LogicLayerInstance.Current.requestinstancelogic.Update(param);
                    WFApprovalTrackingDTO apptracking = LogicLayerInstance.Current.approvaltrackinglogic.GetMany(param).OrderByDescending(u => u.WFApprovalTrackingID).FirstOrDefault();
                    if (apptracking != null)
                        SendEmail(requestinstance, apptracking);

                    #region Send Notify Email when end approved
                    param = new List<AddedParam>();
                    param.Add(new AddedParam { Key = "RequestInstanceID", Value = requestinstance.RequestInstanceID });
                    WorkFlowInstanceDTO workflowinstance = LogicLayerInstance.Current.wfinstancelogic.Get(param);
                    WFSettingDTO workflowsetting = LogicLayerInstance.Current.wfinstancelogic.DesializeWFSettingData(workflowinstance.WFSettingData);
                    if (!string.IsNullOrWhiteSpace(workflowsetting.EndNotifyEmail))
                    {
                        if (apptracking != null)
                            SendEndNotifyEmail(requestinstance, workflowsetting.EndNotifyEmail, apptracking);
                    }
                    #endregion
                }
            }
            catch (ApplicationException aex)
            {
                throw aex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        protected bool SendEmail(RequestInstanceDTO requestinstance, WFApprovalTrackingDTO workflowtracking)
        {
            QueueDTO queue = new QueueDTO();
            string email = "";
            RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);
            email = request.RequestorEmail;
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Code", Value = "APPR" });
            WFEmailTemplateDTO emailtemplate = LogicLayerInstance.Current.wfemailtemplatelogic.Get(param);
            if (emailtemplate != null && request != null && requestinstance != null)
            {
                queue.Body = FormatEmailBody(emailtemplate.Body, requestinstance, request, workflowtracking);
                queue.Subject = FormatEmailTitle(emailtemplate.Title, requestinstance, request);
                queue.Recipient = email;
                LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
            }
            return true;
        }
        protected string FormatEmailBody(string emailtemplate, RequestInstanceDTO requestinstance, RequestDTO request, WFApprovalTrackingDTO workflowtracking)
        {
            if (requestinstance.RequestID != null)
            {
                List<AddedParam> addedparames = new List<AddedParam>();
                AddedParam param1 = new AddedParam();
                param1.Key = "RequestID"; param1.Value = (int)requestinstance.RequestID;
                addedparames.Add(param1);

                ViewPDRequestDTO viewpdrequest = LogicLayerInstance.Current.viewpdrequestlogic.Get(addedparames);

                emailtemplate = emailtemplate.Replace("{RequestorName}", request.RequestorName);
                emailtemplate = emailtemplate.Replace("{LevelName}", workflowtracking.Name);
                emailtemplate = emailtemplate.Replace("{RequestNumber}", viewpdrequest.RequestNo);
                emailtemplate = emailtemplate.Replace("{RequestDate}", request.CreatedDate.ToString(BaseAPI.Common.CommonData.DateFormat));
                emailtemplate = emailtemplate.Replace("{DealerName}", viewpdrequest.DealerName);
                emailtemplate = emailtemplate.Replace("{Version}", viewpdrequest.Version);
                emailtemplate = emailtemplate.Replace("{ApproverName}", workflowtracking.ApproverName);
                emailtemplate = emailtemplate.Replace("{ApproverComments}", DecodeString(workflowtracking.Comments));
            }
            return emailtemplate;
        }
        protected string FormatEmailTitle(string emailtitle, RequestInstanceDTO requestinstance, RequestDTO request)
        {
            emailtitle = emailtitle.Replace("{RequestNumber}", requestinstance.RequestNumber);
            emailtitle = emailtitle.Replace("{Version}", request.Version);
            return emailtitle;
        }
        protected bool SendEndNotifyEmail(RequestInstanceDTO requestinstance, string endnotifyemail, WFApprovalTrackingDTO workflowtracking)
        {
            QueueDTO queue = new QueueDTO();
            RequestDTO request = LogicLayerInstance.Current.requestlogic.Get((int)requestinstance.RequestID);
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Code", Value = "FIAPPR" });
            WFEmailTemplateDTO emailtemplate = LogicLayerInstance.Current.wfemailtemplatelogic.Get(param);
            if (emailtemplate != null && request != null && requestinstance != null)
            {
                queue.Body = FormatEmailBody(emailtemplate.Body, requestinstance, request, workflowtracking);
                queue.Subject = FormatEmailTitle(emailtemplate.Title, requestinstance, request);
                queue.Recipient = endnotifyemail;
                LogicLayerInstance.Current.queuelogic.SaveOrUpdate(queue);
            }
            return true;
        }
    }

}

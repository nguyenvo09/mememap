using System;
namespace MemeAPI.Model.DTO
{
    public partial class UserDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Salt { get; set; }
        public string NewSalt { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Lock { get; set; }
        public string ActionKey { get; set; }
        public string ProfilePicture { get; set; }
        public string GalleryURL { get; set; }
        public int? Rating { get; set; }
        public bool Active { get; set; }
        public Guid? ValidKey { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string FacebookID { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookAccessToken { get; set; }
        public string TwitterID { get; set; }
        public string TwitterAccessToken { get; set; }
        public string LinkedInID { get; set; }
        public string LinkedInAccessToken { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
        public string FacebookName { get; set; }
        public string TwitterName { get; set; }
        public string LinkedInName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string GoogleID { get; set; }
        public string GoogleAccessToken { get; set; }
    }
}

using System;
namespace MemeAPI.Model.DTO
{
    public partial class UserInstanceDTO
    {
        public int UserInstanceID { get; set; }
        public int UserID { get; set; }
        public Guid Guid { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
        public string UserName { get; set; }
    }
}

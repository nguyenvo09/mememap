using System;
namespace MemeAPI.Model.DTO
{
    public partial class ZoneDTO
    {
        public int ZoneID { get; set; }
        public int MapID { get; set; }
        public string Data { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
        public string MapName { get; set; }
    }
}

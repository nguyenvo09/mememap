using System;
namespace MemeAPI.Model.DTO
{
    public partial class EmailTemplateDTO
    {
        public int EmailTemplateID { get; set; }
        public string EmailTemplateName { get; set; }
        public string Subject { get; set; }
        public string EmailTemplateContent { get; set; }
        public bool? Active { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EditMode { get; set; }
        public string GUID { get; set; }
    }
}

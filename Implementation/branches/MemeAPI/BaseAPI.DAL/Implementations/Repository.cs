﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;

namespace BaseAPI.DAL
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        private MemeAPIContext dataContext;
        private readonly IDbSet<T> dbset;
        protected Repository(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
        }
        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }
        protected MemeAPIContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }
        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }
        public virtual void Update(T entity)
        {
            try
            {
                dbset.Attach(entity);
                dataContext.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                var pkey = entity.GetType().GetProperty("Id").GetValue(entity,null);
                T attachedEntity = dbset.Find(pkey);
                var attachedEntry = dataContext.Entry(attachedEntity);
                attachedEntry.CurrentValues.SetValues(entity);
            }
        }
        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }
        public virtual T GetById(int id)
        {
            return dbset.Find(id);
        }

        public virtual T GetById(long id)
        {
            return dbset.Find(id);
        }

        public virtual T GetById(string id)
        {
            return dbset.Find(id);
        }
        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).ToList();
        }
        public T Get(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault<T>();
        }
        public virtual IQueryable<T> GetManyQueryable(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where);
        }
        public virtual IQueryable<T> GetAllQueryable()
        {
            return dbset;
        }
    }
}

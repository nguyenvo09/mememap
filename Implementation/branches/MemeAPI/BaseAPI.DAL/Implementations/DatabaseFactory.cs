﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseAPI.DAL
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private MemeAPIContext dataContext;
        public MemeAPIContext Get()
        {
            return dataContext ?? (dataContext = new MemeAPIContext());
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace BaseAPI.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseFactory databaseFactory;
        private MemeAPIContext dataContext;
        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        protected MemeAPIContext DataContext
        {
            get { return dataContext ?? (dataContext = databaseFactory.Get()); }
        }
        
        public void Commit()
        {
            DataContext.Commit();
        }
    }
}

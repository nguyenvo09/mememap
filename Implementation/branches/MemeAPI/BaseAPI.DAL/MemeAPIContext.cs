﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using BaseAPI.Model;
using MemeAPI.Model;

namespace BaseAPI.DAL
{
    public class MemeAPIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<MemeAPI.Entities.MemeAPIContext>());

        public MemeAPIContext()
            : base("name=MemeAPIContext")
        {
        }
        public virtual void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                                      validationError.PropertyName, validationError.ErrorMessage);

                    }
                }
            }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region MemeAPI
            //relationship

            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.CreatedProjects)
                .WithRequired(k => k.Creator)
                .HasForeignKey(k => k.CreatedBy);
            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.ModifiedProjects)
                .WithOptional(k => k.Modifier)
                .HasForeignKey(k => k.ModifiedBy);

            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.CreatedMaps)
                .WithRequired(k => k.Creator)
                .HasForeignKey(k => k.CreatedBy);
            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.ModifiedMaps)
                .WithOptional(k => k.Modifier)
                .HasForeignKey(k => k.ModifiedBy);

            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.CreatedZones)
                .WithRequired(k => k.Creator)
                .HasForeignKey(k => k.CreatedBy);
            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.ModifiedZones)
                .WithOptional(k => k.Modifier)
                .HasForeignKey(k => k.ModifiedBy);

            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.CreatedMedias)
                .WithRequired(k => k.Creator)
                .HasForeignKey(k => k.CreatedBy);
            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.ModifiedMedias)
                .WithOptional(k => k.Modifier)
                .HasForeignKey(k => k.ModifiedBy);

            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.CreatedNotes)
                .WithRequired(k => k.Creator)
                .HasForeignKey(k => k.CreatedBy);
            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.ModifiedNotes)
                .WithOptional(k => k.Modifier)
                .HasForeignKey(k => k.ModifiedBy);



            modelBuilder.Entity<User>()
                .HasMany(k => k.UserInstances)
                .WithRequired(k => k.User)
                .HasForeignKey(k => k.UserID);


            modelBuilder.Entity<UserInstance>()
                .HasMany(k => k.Projects)
                .WithRequired(k => k.UserInstance)
                .HasForeignKey(k => k.UserInstanceID);


            modelBuilder.Entity<Project>()
                .HasMany(k => k.Maps)
                .WithRequired(k => k.Project)
                .HasForeignKey(k => k.ProjectID);


            modelBuilder.Entity<Map>()
                .HasMany(k => k.Zones)
                .WithRequired(k => k.Map)
                .HasForeignKey(k => k.MapID);

            modelBuilder.Entity<Map>()
                .HasMany(k => k.Notes)
                .WithRequired(k => k.Map)
                .HasForeignKey(k => k.MapID);

            #endregion
            base.OnModelCreating(modelBuilder);
        }
        #region Base
        public DbSet<HTML> HTMLs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        #endregion


        public DbSet<User> Users { get; set; }
        public DbSet<UserInstance> UserInstances { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<Note> Notes { get; set; }


    }
}
namespace MemeAPI.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sdfsa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                    {
                        EmailTemplateID = c.Int(nullable: false, identity: true),
                        EmailTemplateName = c.String(maxLength: 200),
                        Subject = c.String(maxLength: 200),
                        EmailTemplateContent = c.String(),
                        Active = c.Boolean(),
                        CreatedBy = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EmailTemplateID);
            
            CreateTable(
                "dbo.Entities",
                c => new
                    {
                        EntityID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Property = c.String(),
                        Data = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EntityID)
                .ForeignKey("dbo.UserInstances", t => t.CreatedBy, cascadeDelete: true)
                .ForeignKey("dbo.UserInstances", t => t.ModifiedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.UserInstances",
                c => new
                    {
                        UserInstanceID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        FirstName = c.String(maxLength: 128),
                        LastName = c.String(maxLength: 128),
                        UserName = c.String(maxLength: 128),
                        Email = c.String(maxLength: 128),
                        Guid = c.Guid(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserInstanceID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Maps",
                c => new
                    {
                        MapID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Name = c.String(maxLength: 256),
                        Description = c.String(maxLength: 2048),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.MapID)
                .ForeignKey("dbo.Projects", t => t.ProjectID, cascadeDelete: true)
                .ForeignKey("dbo.UserInstances", t => t.CreatedBy, cascadeDelete: false)
                .ForeignKey("dbo.UserInstances", t => t.ModifiedBy)
                .Index(t => t.ProjectID)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectID = c.Int(nullable: false, identity: true),
                        UserInstanceID = c.Int(nullable: false),
                        Name = c.String(maxLength: 256),
                        Description = c.String(maxLength: 2048),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectID)
                .ForeignKey("dbo.UserInstances", t => t.CreatedBy, cascadeDelete: false)
                .ForeignKey("dbo.UserInstances", t => t.ModifiedBy)
                .ForeignKey("dbo.UserInstances", t => t.UserInstanceID, cascadeDelete: true)
                .Index(t => t.UserInstanceID)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.Zones",
                c => new
                    {
                        ZoneID = c.Int(nullable: false, identity: true),
                        MapID = c.Int(nullable: false),
                        Data = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ZoneID)
                .ForeignKey("dbo.Maps", t => t.MapID, cascadeDelete: true)
                .ForeignKey("dbo.UserInstances", t => t.CreatedBy, cascadeDelete: false)
                .ForeignKey("dbo.UserInstances", t => t.ModifiedBy)
                .Index(t => t.MapID)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.Media",
                c => new
                    {
                        MediaID = c.Int(nullable: false, identity: true),
                        Data = c.String(),
                        Media_Guid = c.Guid(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.MediaID)
                .ForeignKey("dbo.UserInstances", t => t.CreatedBy, cascadeDelete: false)
                .ForeignKey("dbo.UserInstances", t => t.ModifiedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 128),
                        LastName = c.String(maxLength: 128),
                        UserName = c.String(maxLength: 128),
                        Email = c.String(maxLength: 128),
                        Active = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.HTMLs",
                c => new
                    {
                        HTMLID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        HTMLText = c.String(),
                        Active = c.Boolean(),
                        CreatedBy = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.HTMLID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserInstances", "UserID", "dbo.Users");
            DropForeignKey("dbo.Projects", "UserInstanceID", "dbo.UserInstances");
            DropForeignKey("dbo.Zones", "ModifiedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Projects", "ModifiedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Media", "ModifiedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Maps", "ModifiedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Entities", "ModifiedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Zones", "CreatedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Projects", "CreatedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Media", "CreatedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Maps", "CreatedBy", "dbo.UserInstances");
            DropForeignKey("dbo.Zones", "MapID", "dbo.Maps");
            DropForeignKey("dbo.Maps", "ProjectID", "dbo.Projects");
            DropForeignKey("dbo.Entities", "CreatedBy", "dbo.UserInstances");
            DropIndex("dbo.Media", new[] { "ModifiedBy" });
            DropIndex("dbo.Media", new[] { "CreatedBy" });
            DropIndex("dbo.Zones", new[] { "ModifiedBy" });
            DropIndex("dbo.Zones", new[] { "CreatedBy" });
            DropIndex("dbo.Zones", new[] { "MapID" });
            DropIndex("dbo.Projects", new[] { "ModifiedBy" });
            DropIndex("dbo.Projects", new[] { "CreatedBy" });
            DropIndex("dbo.Projects", new[] { "UserInstanceID" });
            DropIndex("dbo.Maps", new[] { "ModifiedBy" });
            DropIndex("dbo.Maps", new[] { "CreatedBy" });
            DropIndex("dbo.Maps", new[] { "ProjectID" });
            DropIndex("dbo.UserInstances", new[] { "UserID" });
            DropIndex("dbo.Entities", new[] { "ModifiedBy" });
            DropIndex("dbo.Entities", new[] { "CreatedBy" });
            DropTable("dbo.HTMLs");
            DropTable("dbo.Users");
            DropTable("dbo.Media");
            DropTable("dbo.Zones");
            DropTable("dbo.Projects");
            DropTable("dbo.Maps");
            DropTable("dbo.UserInstances");
            DropTable("dbo.Entities");
            DropTable("dbo.EmailTemplates");
        }
    }
}

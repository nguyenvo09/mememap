namespace MemeAPI.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sdfsass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Salt", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "NewSalt", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "Password", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "NewPassword", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "DateOfBirth", c => c.DateTime());
            AddColumn("dbo.Users", "ZipCode", c => c.String(maxLength: 50));
            AddColumn("dbo.Users", "Address", c => c.String(maxLength: 200));
            AddColumn("dbo.Users", "Description", c => c.String());
            AddColumn("dbo.Users", "Lock", c => c.Boolean());
            AddColumn("dbo.Users", "ActionKey", c => c.String(maxLength: 512));
            AddColumn("dbo.Users", "ProfilePicture", c => c.String());
            AddColumn("dbo.Users", "GalleryURL", c => c.String());
            AddColumn("dbo.Users", "Rating", c => c.Int());
            AddColumn("dbo.Users", "ValidKey", c => c.Guid());
            AddColumn("dbo.Users", "LastLoginDate", c => c.DateTime());
            AddColumn("dbo.Users", "FacebookID", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "FacebookEmail", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "FacebookAccessToken", c => c.String(maxLength: 256));
            AddColumn("dbo.Users", "TwitterID", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "TwitterAccessToken", c => c.String(maxLength: 256));
            AddColumn("dbo.Users", "LinkedInID", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "LinkedInAccessToken", c => c.String(maxLength: 256));
            DropColumn("dbo.UserInstances", "FirstName");
            DropColumn("dbo.UserInstances", "LastName");
            DropColumn("dbo.UserInstances", "UserName");
            DropColumn("dbo.UserInstances", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserInstances", "Email", c => c.String(maxLength: 128));
            AddColumn("dbo.UserInstances", "UserName", c => c.String(maxLength: 128));
            AddColumn("dbo.UserInstances", "LastName", c => c.String(maxLength: 128));
            AddColumn("dbo.UserInstances", "FirstName", c => c.String(maxLength: 128));
            DropColumn("dbo.Users", "LinkedInAccessToken");
            DropColumn("dbo.Users", "LinkedInID");
            DropColumn("dbo.Users", "TwitterAccessToken");
            DropColumn("dbo.Users", "TwitterID");
            DropColumn("dbo.Users", "FacebookAccessToken");
            DropColumn("dbo.Users", "FacebookEmail");
            DropColumn("dbo.Users", "FacebookID");
            DropColumn("dbo.Users", "LastLoginDate");
            DropColumn("dbo.Users", "ValidKey");
            DropColumn("dbo.Users", "Rating");
            DropColumn("dbo.Users", "GalleryURL");
            DropColumn("dbo.Users", "ProfilePicture");
            DropColumn("dbo.Users", "ActionKey");
            DropColumn("dbo.Users", "Lock");
            DropColumn("dbo.Users", "Description");
            DropColumn("dbo.Users", "Address");
            DropColumn("dbo.Users", "ZipCode");
            DropColumn("dbo.Users", "DateOfBirth");
            DropColumn("dbo.Users", "NewPassword");
            DropColumn("dbo.Users", "Password");
            DropColumn("dbo.Users", "NewSalt");
            DropColumn("dbo.Users", "Salt");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseAPI.DAL
{
    public interface IDatabaseFactory : IDisposable
    {
        MemeAPIContext Get();
    }
}

using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class UserInstanceRepository : BaseRepository<UserInstance>, IUserInstanceRepository
    {
        public UserInstanceRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IUserInstanceRepository : IRepository<UserInstance>
    {
    }
}

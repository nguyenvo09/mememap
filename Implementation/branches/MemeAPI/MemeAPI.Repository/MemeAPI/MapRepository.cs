using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class MapRepository : BaseRepository<Map>, IMapRepository
    {
        public MapRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IMapRepository : IRepository<Map>
    {
    }
}

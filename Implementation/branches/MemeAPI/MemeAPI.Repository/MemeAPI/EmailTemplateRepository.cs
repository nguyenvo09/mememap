using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class EmailTemplateRepository : BaseRepository<EmailTemplate>, IEmailTemplateRepository
    {
        public EmailTemplateRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IEmailTemplateRepository : IRepository<EmailTemplate>
    {
    }
}

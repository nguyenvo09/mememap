using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class NoteRepository : BaseRepository<Note>, INoteRepository
    {
        public NoteRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface INoteRepository : IRepository<Note>
    {
    }
}

using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class EntityRepository : BaseRepository<Entity>, IEntityRepository
    {
        public EntityRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IEntityRepository : IRepository<Entity>
    {
    }
}

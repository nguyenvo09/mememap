using MemeAPI.Model;
using BaseAPI.Repository;
using BaseAPI.DAL;
namespace MemeAPI.Repository
{
    public class MediaRepository : BaseRepository<Media>, IMediaRepository
    {
        public MediaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IMediaRepository : IRepository<Media>
    {
    }
}

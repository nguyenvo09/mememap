using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;
namespace MemeAPI.Controllers
{
    public class UserController : ApiController
    {
        IUserLogic _oLogic;
        public UserController(IUserLogic oLogic)
        {
            _oLogic = oLogic;
        }
        public UserDTO Get(int id)
        {
            UserDTO obj = _oLogic.Get(id);
            return obj;
        }
        public HttpResponseMessage PostOne(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                UserDTO rObj = _oLogic.Get(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Put(int id, UserDTO obj)
        {
            if (ModelState.IsValid && id == obj.UserID)
            {
                try
                {
                    obj = _oLogic.SaveOrUpdate(obj);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Post(UserDTO obj)
        {
            if (ModelState.IsValid)
            {
                obj = _oLogic.SaveOrUpdate(obj);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                UserDTO obj = new UserDTO();
                try
                {
                    obj = _oLogic.Update(addedParams);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostMany(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<UserDTO> rObj = _oLogic.GetMany(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostPage(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<UserDTO> rObj = _oLogic.GetPage(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostScalar(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                Object rObj = _oLogic.GetScalar(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostDelete(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                var param = addedParams.FirstOrDefault(p => p.Key == "UserID");
                if (param != null && param.Value != null)
                {
                    int id = Convert.ToInt32(param.Value);
                    UserDTO obj = _oLogic.Get(id);
                    _oLogic.Delete(id);
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public void Delete(int id)
        {
            UserDTO obj = _oLogic.Get(id);
            _oLogic.Delete(id);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public HttpResponseMessage PostList(List<UserDTO> list)
        {
            if (ModelState.IsValid)
            {
                var result = _oLogic.Update(list);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public bool GetExistEmail(int? userid, string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "Email", Value = email });
            UserDTO user = _oLogic.Get(param);
            if (user != null) {
                if (userid.HasValue)
                {
                    if (userid != user.UserID)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }
            return false;
        }

        public bool GetExistUserName(int? userid, string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return false;
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "UserName", Value = username });
            UserDTO user = _oLogic.Get(param);
            if (user != null)
            {
                if (userid.HasValue)
                {
                    if (userid != user.UserID)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;

        }
    }
}

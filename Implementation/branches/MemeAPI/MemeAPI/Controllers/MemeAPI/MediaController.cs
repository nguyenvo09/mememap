using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;
namespace MemeAPI.Controllers
{
    public class MediaController : ApiController
    {
        IMediaLogic _oLogic;
        public MediaController(IMediaLogic oLogic)
        {
            _oLogic = oLogic;
        }
        public MediaDTO Get(int id)
        {
            MediaDTO obj = _oLogic.Get(id);
            return obj;
        }
        public HttpResponseMessage PostOne(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                MediaDTO rObj = _oLogic.Get(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Put(int id, MediaDTO obj)
        {
            if (ModelState.IsValid && id == obj.MediaID)
            {
                try
                {
                    obj = _oLogic.SaveOrUpdate(obj);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Post(MediaDTO obj)
        {
            if (ModelState.IsValid)
            {
                obj = _oLogic.SaveOrUpdate(obj);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                MediaDTO obj = new MediaDTO();
                try
                {
                    obj = _oLogic.Update(addedParams);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostMany(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<MediaDTO> rObj = _oLogic.GetMany(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostPage(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<MediaDTO> rObj = _oLogic.GetPage(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostScalar(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                Object rObj = _oLogic.GetScalar(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostDelete(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                var param = addedParams.FirstOrDefault(p => p.Key == "MediaID");
                if (param != null && param.Value != null)
                {
                    int id = Convert.ToInt32(param.Value);
                    MediaDTO obj = _oLogic.Get(id);
                    _oLogic.Delete(id);
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public void Delete(int id)
        {
            MediaDTO obj = _oLogic.Get(id);
            _oLogic.Delete(id);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public HttpResponseMessage PostList(List<MediaDTO> list)
        {
            if (ModelState.IsValid)
            {
                var result = _oLogic.Update(list);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}

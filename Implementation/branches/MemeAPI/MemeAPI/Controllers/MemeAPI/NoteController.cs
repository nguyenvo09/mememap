using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BaseAPI.Model.DTO;
using MemeAPI.Logic;
using MemeAPI.Model.DTO;
namespace MemeAPI.Controllers
{
    public class NoteController : ApiController
    {
        INoteLogic _oLogic;
        public NoteController(INoteLogic oLogic)
        {
            _oLogic = oLogic;
        }
        public NoteDTO Get(int id)
        {
            NoteDTO obj = _oLogic.Get(id);
            return obj;
        }
        public HttpResponseMessage PostOne(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                NoteDTO rObj = _oLogic.Get(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Put(int id, NoteDTO obj)
        {
            if (ModelState.IsValid && id == obj.NoteID)
            {
                try
                {
                    obj = _oLogic.SaveOrUpdate(obj);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Post(NoteDTO obj)
        {
            if (ModelState.IsValid)
            {
                obj = _oLogic.SaveOrUpdate(obj);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                NoteDTO obj = new NoteDTO();
                try
                {
                    obj = _oLogic.Update(addedParams);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostMany(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<NoteDTO> rObj = _oLogic.GetMany(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostPage(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<NoteDTO> rObj = _oLogic.GetPage(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostScalar(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                Object rObj = _oLogic.GetScalar(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostDelete(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                var param = addedParams.FirstOrDefault(p => p.Key == "NoteID");
                if (param != null && param.Value != null)
                {
                    int id = Convert.ToInt32(param.Value);
                    NoteDTO obj = _oLogic.Get(id);
                    _oLogic.Delete(id);
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public void Delete(int id)
        {
            NoteDTO obj = _oLogic.Get(id);
            _oLogic.Delete(id);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public HttpResponseMessage PostList(List<NoteDTO> list)
        {
            if (ModelState.IsValid)
            {
                var result = _oLogic.Update(list);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}

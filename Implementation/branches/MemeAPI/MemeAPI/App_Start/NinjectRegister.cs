﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using Ninject.Syntax;
using System.Web.Http.Dependencies;
using Ninject.Activation;
using Ninject.Parameters;
using System.Web.Http;
using BaseAPI.Common.Log;
using Ninject.Modules;
using BaseAPI.DAL;
using BaseAPI.Logic;
using MemeAPI.Repository;
using MemeAPI.Logic;
namespace MemeAPI
{
    public class NinjectRegister
    {
        public static void RegisterServices()
        {
            var modules = new INinjectModule[]
                              {
                                  new ServiceModule()
                              };
            IKernel kernel = new StandardKernel(modules);
            // Tell ASP.NET MVC to use our Ninject DI Container
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
        }
    }
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            #region Base
            Bind<ILogService>().To<LogService>();
            Bind(typeof(BaseAPI.DAL.IRepository<>)).To(typeof(BaseAPI.DAL.Repository<>));
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IDatabaseFactory>().To<DatabaseFactory>().InSingletonScope();
            Bind<IDisposable>().To<BaseAPI.DAL.Disposable>();
            #endregion



            #region Meme
            Bind<IUserLogic>().To<UserLogic>();
            Bind<IUserRepository>().To<UserRepository>();

            Bind<IUserInstanceLogic>().To<UserInstanceLogic>();
            Bind<IUserInstanceRepository>().To<UserInstanceRepository>();

            Bind<IProjectLogic>().To<ProjectLogic>();
            Bind<IProjectRepository>().To<ProjectRepository>();

            Bind<IMapLogic>().To<MapLogic>();
            Bind<IMapRepository>().To<MapRepository>();

            Bind<IZoneLogic>().To<ZoneLogic>();
            Bind<IZoneRepository>().To<ZoneRepository>();

            Bind<INoteLogic>().To<NoteLogic>();
            Bind<INoteRepository>().To<NoteRepository>();

            Bind<IMediaLogic>().To<MediaLogic>();
            Bind<IMediaRepository>().To<MediaRepository>();

            Bind<IEntityLogic>().To<EntityLogic>();
            Bind<IEntityRepository>().To<EntityRepository>();

            Bind<IEmailTemplateLogic>().To<EmailTemplateLogic>();
            Bind<IEmailTemplateRepository>().To<EmailTemplateRepository>();
            #endregion
        }

    }
    public class NinjectResolver : NinjectScope, IDependencyResolver
    {
        private IKernel _kernel;
        public NinjectResolver(IKernel kernel)
            : base(kernel)
        {
            _kernel = kernel;
        }
        public IDependencyScope BeginScope()
        {
            return new NinjectScope(_kernel.BeginBlock());
        }
    }
    public class NinjectScope : IDependencyScope
    {
        protected IResolutionRoot resolutionRoot;
        public NinjectScope(IResolutionRoot kernel)
        {
            resolutionRoot = kernel;
        }
        public object GetService(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).SingleOrDefault();
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).ToList();
        }
        public void Dispose()
        {
            IDisposable disposable = (IDisposable)resolutionRoot;
            if (disposable != null) disposable.Dispose();
            resolutionRoot = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MemeAPI.Model
{
    public partial class UserInstance
    {
        [Key]
        public int UserInstanceID { get; set; }
        public int UserID { get; set; }
        public Guid Guid { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Project> Projects { get; set; }

        public virtual ICollection<Project> CreatedProjects { get; set; }
        public virtual ICollection<Project> ModifiedProjects { get; set; }
        public virtual ICollection<Map> CreatedMaps { get; set; }
        public virtual ICollection<Map> ModifiedMaps { get; set; }
        public virtual ICollection<Zone> CreatedZones { get; set; }
        public virtual ICollection<Zone> ModifiedZones { get; set; }
        public virtual ICollection<Media> CreatedMedias { get; set; }
        public virtual ICollection<Media> ModifiedMedias { get; set; }
        public virtual ICollection<Entity> CreatedEntities { get; set; }
        public virtual ICollection<Entity> ModifiedEntities { get; set; }
        public virtual ICollection<Entity> CreatedNotes { get; set; }
        public virtual ICollection<Entity> ModifiedNotes { get; set; }
    }
}

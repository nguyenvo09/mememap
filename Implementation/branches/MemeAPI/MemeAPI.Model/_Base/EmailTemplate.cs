﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MemeAPI.Model
{
    public class EmailTemplate
    {
        [Key]
        public int EmailTemplateID { get; set; }
        [StringLength(200)]
        public string EmailTemplateName { get; set; }
        [StringLength(200)]
        public string Subject { get; set; }
        public string EmailTemplateContent { get; set; }
        public bool? Active { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BaseAPI.Model;

namespace MemeAPI.Model
{
    public partial class Project
    {
        [Key]
        public int ProjectID { get; set; }
        public int UserInstanceID { get; set; }

        [StringLength(256)]
        public string Name { get; set; }
        [StringLength(2048)]
        public string Description { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual UserInstance UserInstance { get; set; }
        public virtual UserInstance Creator { get; set; }
        public virtual UserInstance Modifier { get; set; }
        public virtual ICollection<Map> Maps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BaseAPI.Model;

namespace MemeAPI.Model
{
    public partial class Note
    {
        [Key]
        public int NoteID { get; set; }
        public int MapID { get; set; }

        [StringLength(100)]
        public string RID { get; set; }
        [StringLength(100)]
        public string Type { get; set; }
        [StringLength(1000)]
        public string Url { get; set; }
        public string Body { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual UserInstance Creator { get; set; }
        public virtual UserInstance Modifier { get; set; }
        public virtual Map Map { get; set; }
    }
}

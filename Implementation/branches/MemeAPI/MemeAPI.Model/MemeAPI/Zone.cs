﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BaseAPI.Model;

namespace MemeAPI.Model
{
    public partial class Zone
    {
        [Key]
        public int ZoneID { get; set; }
        public int MapID { get; set; }

        public string Data { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual UserInstance Creator { get; set; }
        public virtual UserInstance Modifier { get; set; }
        public virtual Map Map { get; set; }
    }
}

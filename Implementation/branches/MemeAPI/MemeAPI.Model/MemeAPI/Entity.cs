﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BaseAPI.Model;

namespace MemeAPI.Model
{
    public partial class Entity
    {
        [Key]
        public int EntityID { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(1024)]
        public string Property { get; set; }

        public string Data { get; set; }

        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual UserInstance Creator { get; set; }
        public virtual UserInstance Modifier { get; set; }        
    }
}

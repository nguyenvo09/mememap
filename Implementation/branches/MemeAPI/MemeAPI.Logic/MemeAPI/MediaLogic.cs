using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class MediaLogic : IMediaLogic
    {
        public IUnitOfWork _unitOfWork;
        IMediaRepository _mediaRepository;
        public MediaLogic(IUnitOfWork unitOfWork, IMediaRepository mediaRepository)
        {
            this._unitOfWork = unitOfWork;
            this._mediaRepository = mediaRepository;
        }
        public IEnumerable<MediaDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _mediaRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    case "Media_Guid":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Media_Guid);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Media_Guid);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.MediaID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.MediaID);
            }
            var data = (from u in query
                        select new MediaDTO
                         {
                             MediaID = u.MediaID,
                             Data = u.Data,
                             Media_Guid = u.Media_Guid,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate
                         });
            return data;
        }
        public ListDTOModel<MediaDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<MediaDTO> rList = new ListDTOModel<MediaDTO>();
            var query = _mediaRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    case "Media_Guid":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Media_Guid);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Media_Guid);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.MediaID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.MediaID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new MediaDTO
                        {
                            MediaID = u.MediaID,
                            Data = u.Data,
                            Media_Guid = u.Media_Guid,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _mediaRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public MediaDTO Get(int id)
        {
            return ConvertToDTO(_mediaRepository.GetById(id));
        }
        public MediaDTO Get(List<AddedParam> addedParams)
        {
            var query = _mediaRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Media item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _mediaRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public MediaDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "MediaID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int mediaid = Convert.ToInt32(addParam.Value);
            Media dataItem = _mediaRepository.GetById(mediaid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "Data":
                        string data = string.Empty;
                        if (p.Value != null)
                        {
                            data = p.Value.ToString();
                        }
                        dataItem.Data = data;
                        break;
                }
            }
            _mediaRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public MediaDTO SaveOrUpdate(MediaDTO item)
        {
            Media dataItem;
            if (item.MediaID != 0)
            {
                //set logic when update Department
                dataItem = _mediaRepository.GetById(item.MediaID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Media();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.Data = item.Data;
            dataItem.Media_Guid = item.Media_Guid;
            if (item.MediaID != 0)
                _mediaRepository.Update(dataItem);
            else
                _mediaRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public MediaDTO Delete(int id)
        {
            Media dataObj = _mediaRepository.GetById(id);
            _mediaRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private MediaDTO ConvertToDTO(Media data)
        {
            if (data != null)
            {
                MediaDTO dto = new MediaDTO();
                dto.MediaID = data.MediaID;
                dto.Data = data.Data;
                dto.Media_Guid = data.Media_Guid;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                return dto;
            }
            return null;
        }
        public List<MediaDTO> Update(List<MediaDTO> list)
        {
            try
            {
                List<MediaDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<MediaDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<MediaDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<Media> listUpdate = new List<Media>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.MediaID).ToArray();
                    _mediaRepository.Delete(u => listids.Contains(u.MediaID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.MediaID).ToArray();
                    List<Media> listData = _mediaRepository.GetMany(u => listids.Contains(u.MediaID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.MediaID == data.MediaID);
                            ConvertDTOToData(dto, data);
                            _mediaRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, Media> dictNew = new Dictionary<string, Media>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new Media();
                        ConvertDTOToData(dto, dataItem);
                        _mediaRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.MediaID = dictNew[item.GUID].MediaID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(MediaDTO dto, Media data)
        {
            data.MediaID = dto.MediaID;
            data.Data = dto.Data;
            data.Media_Guid = dto.Media_Guid;
            if (dto.MediaID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IMediaLogic
    {
        MediaDTO Get(int id);
        MediaDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<MediaDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<MediaDTO> GetMany(List<AddedParam> addedParams);
        MediaDTO SaveOrUpdate(MediaDTO item);
        MediaDTO Update(List<AddedParam> addParams);
        MediaDTO Delete(int id);
        List<MediaDTO> Update(List<MediaDTO> list);
    }
}

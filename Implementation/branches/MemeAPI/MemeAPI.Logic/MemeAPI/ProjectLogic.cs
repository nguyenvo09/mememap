using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class ProjectLogic : IProjectLogic
    {
        public IUnitOfWork _unitOfWork;
        IProjectRepository _projectRepository;
        public ProjectLogic(IUnitOfWork unitOfWork, IProjectRepository projectRepository)
        {
            this._unitOfWork = unitOfWork;
            this._projectRepository = projectRepository;
        }
        public IEnumerable<ProjectDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _projectRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "UserInstanceID":
                            int userinstanceid = 0;
                            if (p.Value != null)
                            {
                                userinstanceid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.UserInstanceID == userinstanceid);
                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "UserInstanceID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserInstanceID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserInstanceID);
                        }
                        break;
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.ProjectID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.ProjectID);
            }
            var data = (from u in query
                        select new ProjectDTO
                         {
                             ProjectID = u.ProjectID,
                             UserInstanceID = u.UserInstanceID,
                             Name = u.Name,
                             Description = u.Description,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate,
                             UserName = u.UserInstance != null ? u.UserInstance.User != null ? u.UserInstance.User.UserName : "" : "",
                             NumOfMap = u.Maps.Count
                         });
            return data;
        }
        public ListDTOModel<ProjectDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<ProjectDTO> rList = new ListDTOModel<ProjectDTO>();
            var query = _projectRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "UserInstanceID":
                            int userinstanceid = 0;
                            if (p.Value != null)
                            {
                                userinstanceid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.UserInstanceID == userinstanceid);
                            break;
                        case "SearchCode":
                            string searchcode = string.Empty;;
                            if (p.Value != null)
                            {
                                searchcode = p.Value.ToString().ToLower();
                                query = query.Where(u => u.Name.ToLower().Contains(searchcode));
                            }
                            
                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "UserInstanceID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserInstanceID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserInstanceID);
                        }
                        break;
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.ProjectID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.ProjectID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new ProjectDTO
                        {
                            ProjectID = u.ProjectID,
                            UserInstanceID = u.UserInstanceID,
                            Name = u.Name,
                            Description = u.Description,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            UserName = u.UserInstance != null ? u.UserInstance.User != null ? u.UserInstance.User.UserName : "" : "",
                            NumOfMap = u.Maps.Count
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _projectRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public ProjectDTO Get(int id)
        {
            Project item = _projectRepository.GetById(id);
            ProjectDTO dto = ConvertToDTO(item);
            item.ModifiedDate = DateTime.Now;
            _projectRepository.Update(item);
            return dto;
        }
        public ProjectDTO Get(List<AddedParam> addedParams)
        {
            var query = _projectRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Project item = query.FirstOrDefault();
            ProjectDTO dto = ConvertToDTO(item);
            item.ModifiedDate = DateTime.Now;
            _projectRepository.Update(item);
            return dto;
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _projectRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "NumOfProject":
                            var userparam = addedParams.FirstOrDefault(u => u.Key == "UserInstanceID");
                            if (userparam == null) return 0;
                            int userinstanceid = Convert.ToInt32(userparam.Value);
                            return query.Count(u => u.CreatedBy == userinstanceid);
                    }
                }
            }
            return query.Count();
        }
        public ProjectDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "ProjectID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int projectid = Convert.ToInt32(addParam.Value);
            Project dataItem = _projectRepository.GetById(projectid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "UserInstanceID":
                        int userinstanceid = 0;
                        if (p.Value != null)
                        {
                            userinstanceid = Convert.ToInt32(p.Value);
                        }
                        dataItem.UserInstanceID = userinstanceid;
                        break;
                    case "Name":
                        string name = string.Empty;
                        if (p.Value != null)
                        {
                            name = p.Value.ToString();
                        }
                        dataItem.Name = name;
                        break;
                    case "Description":
                        string description = string.Empty;
                        if (p.Value != null)
                        {
                            description = p.Value.ToString();
                        }
                        dataItem.Description = description;
                        break;
                }
            }
            _projectRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public ProjectDTO SaveOrUpdate(ProjectDTO item)
        {
            Project dataItem;
            if (item.ProjectID != 0)
            {
                //set logic when update Department
                dataItem = _projectRepository.GetById(item.ProjectID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Project();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.UserInstanceID = item.UserInstanceID;
            dataItem.Name = item.Name;
            dataItem.Description = item.Description;
            if (item.ProjectID != 0)
                _projectRepository.Update(dataItem);
            else
                _projectRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public ProjectDTO Delete(int id)
        {
            Project dataObj = _projectRepository.GetById(id);
            _projectRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private ProjectDTO ConvertToDTO(Project data)
        {
            if (data != null)
            {
                ProjectDTO dto = new ProjectDTO();
                dto.ProjectID = data.ProjectID;
                dto.UserInstanceID = data.UserInstanceID;
                dto.Name = data.Name;
                dto.Description = data.Description;
                dto.UserName = data.UserInstance != null ? data.UserInstance.User != null ? data.UserInstance.User.UserName : "" : "";
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.NumOfMap = data.Maps != null ? data.Maps.Count : 0;
                return dto;
            }
            return null;
        }
        public List<ProjectDTO> Update(List<ProjectDTO> list)
        {
            try
            {
                List<ProjectDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<ProjectDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<ProjectDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<Project> listUpdate = new List<Project>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.ProjectID).ToArray();
                    _projectRepository.Delete(u => listids.Contains(u.ProjectID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.ProjectID).ToArray();
                    List<Project> listData = _projectRepository.GetMany(u => listids.Contains(u.ProjectID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.ProjectID == data.ProjectID);
                            ConvertDTOToData(dto, data);
                            _projectRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, Project> dictNew = new Dictionary<string, Project>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new Project();
                        ConvertDTOToData(dto, dataItem);
                        _projectRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.ProjectID = dictNew[item.GUID].ProjectID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(ProjectDTO dto, Project data)
        {
            data.ProjectID = dto.ProjectID;
            data.UserInstanceID = dto.UserInstanceID;
            data.Name = dto.Name;
            data.Description = dto.Description;
            if (dto.ProjectID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IProjectLogic
    {
        ProjectDTO Get(int id);
        ProjectDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<ProjectDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<ProjectDTO> GetMany(List<AddedParam> addedParams);
        ProjectDTO SaveOrUpdate(ProjectDTO item);
        ProjectDTO Update(List<AddedParam> addParams);
        ProjectDTO Delete(int id);
        List<ProjectDTO> Update(List<ProjectDTO> list);
    }
}

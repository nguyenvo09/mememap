using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class UserInstanceLogic : IUserInstanceLogic
    {
        public IUnitOfWork _unitOfWork;
        IUserInstanceRepository _userinstanceRepository;
        public UserInstanceLogic(IUnitOfWork unitOfWork, IUserInstanceRepository userinstanceRepository)
        {
            this._unitOfWork = unitOfWork;
            this._userinstanceRepository = userinstanceRepository;
        }
        public IEnumerable<UserInstanceDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _userinstanceRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "UserID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserID);
                        }
                        break;
                    case "Guid":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Guid);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Guid);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.UserInstanceID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.UserInstanceID);
            }
            var data = (from u in query
                        select new UserInstanceDTO
                         {
                             UserInstanceID = u.UserInstanceID,
                             UserID = u.UserID,
                             Guid = u.Guid,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate,
                             UserName = u.User != null ? u.User.UserName : ""
                         });
            return data;
        }
        public ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<UserInstanceDTO> rList = new ListDTOModel<UserInstanceDTO>();
            var query = _userinstanceRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "UserID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserID);
                        }
                        break;
                    case "Guid":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Guid);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Guid);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.UserInstanceID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.UserInstanceID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = u.UserInstanceID,
                            UserID = u.UserID,
                            Guid = u.Guid,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            UserName = u.User != null ? u.User.UserName : ""
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _userinstanceRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public UserInstanceDTO Get(int id)
        {
            return ConvertToDTO(_userinstanceRepository.GetById(id));
        }
        public UserInstanceDTO Get(List<AddedParam> addedParams)
        {
            var query = _userinstanceRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "UserID":
                            int userid = -5;
                            if (p.Value != null)
                            {
                                userid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.UserID == userid);
                            break;
                    }
                }
            }
            UserInstance item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _userinstanceRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public UserInstanceDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "UserInstanceID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int userinstanceid = Convert.ToInt32(addParam.Value);
            UserInstance dataItem = _userinstanceRepository.GetById(userinstanceid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "UserID":
                        int userid = 0;
                        if (p.Value != null)
                        {
                            userid = Convert.ToInt32(p.Value);
                        }
                        dataItem.UserID = userid;
                        break;
                }
            }
            _userinstanceRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public UserInstanceDTO SaveOrUpdate(UserInstanceDTO item)
        {
            UserInstance dataItem;
            if (item.UserInstanceID != 0)
            {
                //set logic when update Department
                dataItem = _userinstanceRepository.GetById(item.UserInstanceID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new UserInstance();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.UserID = item.UserID;
            dataItem.Guid = item.Guid;
            if (item.UserInstanceID != 0)
                _userinstanceRepository.Update(dataItem);
            else
                _userinstanceRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public UserInstanceDTO Delete(int id)
        {
            UserInstance dataObj = _userinstanceRepository.GetById(id);
            _userinstanceRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private UserInstanceDTO ConvertToDTO(UserInstance data)
        {
            if (data != null)
            {
                UserInstanceDTO dto = new UserInstanceDTO();
                dto.UserInstanceID = data.UserInstanceID;
                dto.UserID = data.UserID;
                dto.Guid = data.Guid;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.UserName = data.User != null ? data.User.UserName : "";
                return dto;
            }
            return null;
        }
        public List<UserInstanceDTO> Update(List<UserInstanceDTO> list)
        {
            try
            {
                List<UserInstanceDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<UserInstanceDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<UserInstanceDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<UserInstance> listUpdate = new List<UserInstance>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.UserInstanceID).ToArray();
                    _userinstanceRepository.Delete(u => listids.Contains(u.UserInstanceID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.UserInstanceID).ToArray();
                    List<UserInstance> listData = _userinstanceRepository.GetMany(u => listids.Contains(u.UserInstanceID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.UserInstanceID == data.UserInstanceID);
                            ConvertDTOToData(dto, data);
                            _userinstanceRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, UserInstance> dictNew = new Dictionary<string, UserInstance>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new UserInstance();
                        ConvertDTOToData(dto, dataItem);
                        _userinstanceRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.UserInstanceID = dictNew[item.GUID].UserInstanceID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(UserInstanceDTO dto, UserInstance data)
        {
            data.UserInstanceID = dto.UserInstanceID;
            data.UserID = dto.UserID;
            data.Guid = dto.Guid;
            if (dto.UserInstanceID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IUserInstanceLogic
    {
        UserInstanceDTO Get(int id);
        UserInstanceDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<UserInstanceDTO> GetMany(List<AddedParam> addedParams);
        UserInstanceDTO SaveOrUpdate(UserInstanceDTO item);
        UserInstanceDTO Update(List<AddedParam> addParams);
        UserInstanceDTO Delete(int id);
        List<UserInstanceDTO> Update(List<UserInstanceDTO> list);
    }
}

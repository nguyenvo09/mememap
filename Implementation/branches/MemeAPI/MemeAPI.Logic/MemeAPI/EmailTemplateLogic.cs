using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class EmailTemplateLogic : IEmailTemplateLogic
    {
        public IUnitOfWork _unitOfWork;
        IEmailTemplateRepository _emailtemplateRepository;
       public EmailTemplateLogic(IUnitOfWork unitOfWork, IEmailTemplateRepository emailtemplateRepository)
       {
           this._unitOfWork = unitOfWork;
           this._emailtemplateRepository = emailtemplateRepository;
       }
       public IEnumerable<EmailTemplateDTO> GetMany(List<AddedParam> addedParams)
       {
           var query = _emailtemplateRepository.GetAllQueryable();
          if (addedParams != null)
          {
              foreach (AddedParam p in addedParams)
              {
               }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "EmailTemplateName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.EmailTemplateName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.EmailTemplateName);
                        }
                        break;
                    case "Subject":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Subject);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Subject);
                        }
                        break;
                    case "EmailTemplateContent":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.EmailTemplateContent);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.EmailTemplateContent);
                        }
                        break;
                    default:
                          query = query.OrderBy(u => u.EmailTemplateID);
                          break;
                  }
            }
            else
            {
               query = query.OrderBy(u => u.EmailTemplateID);
            }
            var data = (from u in query
                       select new EmailTemplateDTO
                        {
                            EmailTemplateID = u.EmailTemplateID,
                            EmailTemplateName = u.EmailTemplateName,
                            Subject = u.Subject,
                            EmailTemplateContent = u.EmailTemplateContent,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate
                        });
            return data;
        }
        public ListDTOModel<EmailTemplateDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<EmailTemplateDTO> rList = new ListDTOModel<EmailTemplateDTO>();
            var query = _emailtemplateRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null&&pageIndexParam.Value!=null) {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "EmailTemplateName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.EmailTemplateName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.EmailTemplateName);
                        }
                        break;
                    case "Subject":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Subject);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Subject);
                        }
                        break;
                    case "EmailTemplateContent":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.EmailTemplateContent);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.EmailTemplateContent);
                        }
                        break;
                    default:
                          query = query.OrderBy(u => u.EmailTemplateID);
                          break;
                  }
            }
            else
            {
               query = query.OrderBy(u => u.EmailTemplateID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new EmailTemplateDTO
                        {
                            EmailTemplateID = u.EmailTemplateID,
                            EmailTemplateName = u.EmailTemplateName,
                            Subject = u.Subject,
                            EmailTemplateContent = u.EmailTemplateContent,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _emailtemplateRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public EmailTemplateDTO Get(int id)
        {
            return ConvertToDTO(_emailtemplateRepository.GetById(id));
        }
        public EmailTemplateDTO Get(List<AddedParam> addedParams)
        {
            var query = _emailtemplateRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            EmailTemplate item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _emailtemplateRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public EmailTemplateDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "EmailTemplateID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int emailtemplateid = Convert.ToInt32(addParam.Value);
            EmailTemplate dataItem = _emailtemplateRepository.GetById(emailtemplateid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                case "EmailTemplateName":
                    string emailtemplatename = string.Empty;
                    if(p.Value != null)
                    {
                      emailtemplatename =  p.Value.ToString();
                    }
                    dataItem.EmailTemplateName = emailtemplatename;
                    break;
                case "Subject":
                    string subject = string.Empty;
                    if(p.Value != null)
                    {
                      subject =  p.Value.ToString();
                    }
                    dataItem.Subject = subject;
                    break;
                case "EmailTemplateContent":
                    string emailtemplatecontent = string.Empty;
                    if(p.Value != null)
                    {
                      emailtemplatecontent =  p.Value.ToString();
                    }
                    dataItem.EmailTemplateContent = emailtemplatecontent;
                    break;
                }
            }
            _emailtemplateRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public EmailTemplateDTO SaveOrUpdate(EmailTemplateDTO item)
        {
            EmailTemplate dataItem;
            if (item.EmailTemplateID != 0)
            {
                //set logic when update Department
                dataItem = _emailtemplateRepository.GetById(item.EmailTemplateID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new EmailTemplate();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.EmailTemplateName = item.EmailTemplateName;
            dataItem.Subject = item.Subject;
            dataItem.EmailTemplateContent = item.EmailTemplateContent;
            if (item.EmailTemplateID != 0)
                _emailtemplateRepository.Update(dataItem);
            else
                _emailtemplateRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public EmailTemplateDTO Delete(int id)
        {
            EmailTemplate dataObj = _emailtemplateRepository.GetById(id);
            _emailtemplateRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private EmailTemplateDTO ConvertToDTO(EmailTemplate data)
        {
            if (data != null)
            {
                EmailTemplateDTO dto = new EmailTemplateDTO();
                dto.EmailTemplateID = data.EmailTemplateID;
                dto.EmailTemplateName = data.EmailTemplateName;
                dto.Subject = data.Subject;
                dto.EmailTemplateContent = data.EmailTemplateContent;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                return dto;
            }
            return null;
        }
        public List<EmailTemplateDTO> Update(List<EmailTemplateDTO> list)
        {
            try
            {
                List<EmailTemplateDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<EmailTemplateDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<EmailTemplateDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<EmailTemplate> listUpdate = new List<EmailTemplate>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.EmailTemplateID).ToArray();
                    _emailtemplateRepository.Delete(u => listids.Contains(u.EmailTemplateID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.EmailTemplateID).ToArray();
                    List<EmailTemplate> listData = _emailtemplateRepository.GetMany(u => listids.Contains(u.EmailTemplateID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.EmailTemplateID == data.EmailTemplateID);
                            ConvertDTOToData(dto, data);
                            _emailtemplateRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, EmailTemplate> dictNew = new Dictionary<string, EmailTemplate>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new EmailTemplate();
                        ConvertDTOToData(dto, dataItem);
                        _emailtemplateRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.EmailTemplateID = dictNew[item.GUID].EmailTemplateID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(EmailTemplateDTO dto, EmailTemplate data)
        {
                data.EmailTemplateID = dto.EmailTemplateID;
                data.EmailTemplateName = dto.EmailTemplateName;
                data.Subject = dto.Subject;
                data.EmailTemplateContent = dto.EmailTemplateContent;
            if (dto.EmailTemplateID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IEmailTemplateLogic
    {
          EmailTemplateDTO Get(int id);
          EmailTemplateDTO Get(List<AddedParam> addParams);
          bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
          ListDTOModel<EmailTemplateDTO> GetPage(List<AddedParam> addedParams);
          object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
          IEnumerable<EmailTemplateDTO> GetMany(List<AddedParam> addedParams);
          EmailTemplateDTO SaveOrUpdate(EmailTemplateDTO item);
          EmailTemplateDTO Update(List<AddedParam> addParams);
          EmailTemplateDTO Delete(int id);
          List<EmailTemplateDTO> Update(List<EmailTemplateDTO> list);
    }
 }

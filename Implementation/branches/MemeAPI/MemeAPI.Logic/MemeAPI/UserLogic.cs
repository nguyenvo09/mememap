using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class UserLogic : IUserLogic
    {
        public IUnitOfWork _unitOfWork;
        IUserRepository _userRepository;
        IUserInstanceLogic _userinstanceLogic;
        public UserLogic(IUnitOfWork unitOfWork, IUserRepository userRepository,IUserInstanceLogic userinstanceLogic)
        {
            this._unitOfWork = unitOfWork;
            this._userRepository = userRepository;
            this._userinstanceLogic = userinstanceLogic;
        }
        public IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _userRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    //TODO
                    switch (p.Key)
                    {
                        case "SearchCode":
                            string searchcode = string.Empty; ;
                            if (p.Value != null)
                            {
                                searchcode = p.Value.ToString().ToLower();
                                query = query.Where(u => u.Email.ToLower().Contains(searchcode));
                            }

                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "FirstName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FirstName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FirstName);
                        }
                        break;
                    case "LastName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LastName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LastName);
                        }
                        break;
                    case "UserName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserName);
                        }
                        break;
                    case "Salt":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Salt);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Salt);
                        }
                        break;
                    case "NewSalt":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.NewSalt);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.NewSalt);
                        }
                        break;
                    case "Password":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Password);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Password);
                        }
                        break;
                    case "NewPassword":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.NewPassword);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.NewPassword);
                        }
                        break;
                    case "Email":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Email);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Email);
                        }
                        break;
                    case "DateOfBirth":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.DateOfBirth);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.DateOfBirth);
                        }
                        break;
                    case "ZipCode":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ZipCode);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ZipCode);
                        }
                        break;
                    case "Address":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Address);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Address);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    case "Lock":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Lock);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Lock);
                        }
                        break;
                    case "ActionKey":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ActionKey);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ActionKey);
                        }
                        break;
                    case "ProfilePicture":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ProfilePicture);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ProfilePicture);
                        }
                        break;
                    case "GalleryURL":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.GalleryURL);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.GalleryURL);
                        }
                        break;
                    case "Rating":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Rating);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Rating);
                        }
                        break;
                    case "ValidKey":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ValidKey);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ValidKey);
                        }
                        break;
                    case "LastLoginDate":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LastLoginDate);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LastLoginDate);
                        }
                        break;
                    case "FacebookID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookID);
                        }
                        break;
                    case "FacebookEmail":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookEmail);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookEmail);
                        }
                        break;
                    case "FacebookAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookAccessToken);
                        }
                        break;
                    case "TwitterID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.TwitterID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.TwitterID);
                        }
                        break;
                    case "TwitterAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.TwitterAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.TwitterAccessToken);
                        }
                        break;
                    case "LinkedInID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LinkedInID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LinkedInID);
                        }
                        break;
                    case "LinkedInAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LinkedInAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LinkedInAccessToken);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.UserID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.UserID);
            }
            var data = (from u in query
                        select new UserDTO
                         {
                             UserID = u.UserID,
                             FirstName = u.FirstName,
                             LastName = u.LastName,
                             UserName = u.UserName,
                             Salt = u.Salt,
                             NewSalt = u.NewSalt,
                             Password = u.Password,
                             NewPassword = u.NewPassword,
                             Email = u.Email,
                             DateOfBirth = u.DateOfBirth,
                             ZipCode = u.ZipCode,
                             Address = u.Address,
                             Description = u.Description,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate,
                             Lock = u.Lock,
                             ActionKey = u.ActionKey,
                             ProfilePicture = u.ProfilePicture,
                             GalleryURL = u.GalleryURL,
                             Rating = u.Rating,
                             Active = u.Active,
                             ValidKey = u.ValidKey,
                             LastLoginDate = u.LastLoginDate,
                             FacebookID = u.FacebookID,
                             FacebookEmail = u.FacebookEmail,
                             FacebookAccessToken = u.FacebookAccessToken,
                             TwitterID = u.TwitterID,
                             TwitterAccessToken = u.TwitterAccessToken,
                             LinkedInID = u.LinkedInID,
                             LinkedInAccessToken = u.LinkedInAccessToken,
                              GoogleID = u.GoogleID,
                             GoogleAccessToken = u.GoogleAccessToken,
                             City = u.City,
                             Country = u.Country
                         });
            return data;
        }
        public ListDTOModel<UserDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<UserDTO> rList = new ListDTOModel<UserDTO>();
            var query = _userRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "FirstName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FirstName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FirstName);
                        }
                        break;
                    case "LastName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LastName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LastName);
                        }
                        break;
                    case "UserName":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.UserName);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.UserName);
                        }
                        break;
                    case "Salt":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Salt);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Salt);
                        }
                        break;
                    case "NewSalt":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.NewSalt);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.NewSalt);
                        }
                        break;
                    case "Password":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Password);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Password);
                        }
                        break;
                    case "NewPassword":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.NewPassword);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.NewPassword);
                        }
                        break;
                    case "Email":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Email);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Email);
                        }
                        break;
                    case "DateOfBirth":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.DateOfBirth);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.DateOfBirth);
                        }
                        break;
                    case "ZipCode":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ZipCode);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ZipCode);
                        }
                        break;
                    case "Address":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Address);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Address);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    case "Lock":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Lock);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Lock);
                        }
                        break;
                    case "ActionKey":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ActionKey);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ActionKey);
                        }
                        break;
                    case "ProfilePicture":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ProfilePicture);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ProfilePicture);
                        }
                        break;
                    case "GalleryURL":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.GalleryURL);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.GalleryURL);
                        }
                        break;
                    case "Rating":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Rating);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Rating);
                        }
                        break;
                    case "ValidKey":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ValidKey);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ValidKey);
                        }
                        break;
                    case "LastLoginDate":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LastLoginDate);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LastLoginDate);
                        }
                        break;
                    case "FacebookID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookID);
                        }
                        break;
                    case "FacebookEmail":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookEmail);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookEmail);
                        }
                        break;
                    case "FacebookAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.FacebookAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.FacebookAccessToken);
                        }
                        break;
                    case "TwitterID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.TwitterID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.TwitterID);
                        }
                        break;
                    case "TwitterAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.TwitterAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.TwitterAccessToken);
                        }
                        break;
                    case "LinkedInID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LinkedInID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LinkedInID);
                        }
                        break;
                    case "LinkedInAccessToken":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.LinkedInAccessToken);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.LinkedInAccessToken);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.UserID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.UserID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new UserDTO
                        {
                            UserID = u.UserID,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            UserName = u.UserName,
                            Salt = u.Salt,
                            NewSalt = u.NewSalt,
                            Password = u.Password,
                            NewPassword = u.NewPassword,
                            Email = u.Email,
                            DateOfBirth = u.DateOfBirth,
                            ZipCode = u.ZipCode,
                            Address = u.Address,
                            Description = u.Description,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            Lock = u.Lock,
                            ActionKey = u.ActionKey,
                            ProfilePicture = u.ProfilePicture,
                            GalleryURL = u.GalleryURL,
                            Rating = u.Rating,
                            Active = u.Active,
                            ValidKey = u.ValidKey,
                            LastLoginDate = u.LastLoginDate,
                            FacebookID = u.FacebookID,
                            FacebookEmail = u.FacebookEmail,
                            FacebookAccessToken = u.FacebookAccessToken,
                            TwitterID = u.TwitterID,
                            TwitterAccessToken = u.TwitterAccessToken,
                            LinkedInID = u.LinkedInID,
                            LinkedInAccessToken = u.LinkedInAccessToken,
                            GoogleID = u.GoogleID,
                            GoogleAccessToken = u.GoogleAccessToken,
                            City = u.City,
                            Country = u.Country
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _userRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public UserDTO Get(int id)
        {
            return ConvertToDTO(_userRepository.GetById(id));
        }
        public UserDTO Get(List<AddedParam> addedParams)
        {
            var query = _userRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "UserID":
                            int userid = -5;
                            if (p.Value != null)
                            {
                                userid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.UserID == userid);
                            break;

                        case "UserName":
                            string username = string.Empty;
                            if (p.Value != null)
                            {
                                username = p.Value.ToString().ToLower();
                                query = query.Where(u => u.UserName.ToLower() == username);
                            }
                            else return null;
                            break;

                        case "Email":
                            string email = string.Empty;
                            if (p.Value != null)
                            {
                                email = p.Value.ToString().ToLower();
                                query = query.Where(u => u.Email.ToLower() == email);
                            }
                            else return null;
                            break;
                        case "FacebookID":
                            string facebookid = string.Empty;
                            if (p.Value != null)
                            {
                                facebookid = p.Value.ToString();
                                query = query.Where(u => u.FacebookID == facebookid);
                            }
                            else return null;
                            break;
                        case "LinkedInID":
                            string linkedinid = string.Empty;
                            if (p.Value != null)
                            {
                                linkedinid = p.Value.ToString();
                                query = query.Where(u => u.LinkedInID == linkedinid);
                            }
                            else return null;
                            break;
                        case "GoogleID":
                            string googleid = string.Empty;
                            if (p.Value != null)
                            {
                                googleid = p.Value.ToString();
                                query = query.Where(u => u.GoogleID == googleid);
                            }
                            else return null;
                            break;
                    }
                }
            }
            User item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _userRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public UserDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "UserID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int userid = Convert.ToInt32(addParam.Value);
            User dataItem = _userRepository.GetById(userid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "FirstName":
                        string firstname = string.Empty;
                        if (p.Value != null)
                        {
                            firstname = p.Value.ToString();
                        }
                        dataItem.FirstName = firstname;
                        break;
                    case "LastName":
                        string lastname = string.Empty;
                        if (p.Value != null)
                        {
                            lastname = p.Value.ToString();
                        }
                        dataItem.LastName = lastname;
                        break;
                    case "UserName":
                        string username = string.Empty;
                        if (p.Value != null)
                        {
                            username = p.Value.ToString();
                        }
                        dataItem.UserName = username;
                        break;
                    case "Salt":
                        string salt = string.Empty;
                        if (p.Value != null)
                        {
                            salt = p.Value.ToString();
                        }
                        dataItem.Salt = salt;
                        break;
                    case "NewSalt":
                        string newsalt = string.Empty;
                        if (p.Value != null)
                        {
                            newsalt = p.Value.ToString();
                        }
                        dataItem.NewSalt = newsalt;
                        break;
                    case "Password":
                        string password = string.Empty;
                        if (p.Value != null)
                        {
                            password = p.Value.ToString();
                        }
                        dataItem.Password = password;
                        break;
                    case "NewPassword":
                        string newpassword = string.Empty;
                        if (p.Value != null)
                        {
                            newpassword = p.Value.ToString();
                        }
                        dataItem.NewPassword = newpassword;
                        break;
                    case "Email":
                        string email = string.Empty;
                        if (p.Value != null)
                        {
                            email = p.Value.ToString();
                        }
                        dataItem.Email = email;
                        break;
                    case "DateOfBirth":
                        if (p.Value != null)
                        {
                            DateTime dateofbirth = Convert.ToDateTime(p.Value);
                            dataItem.DateOfBirth = dateofbirth;
                        }
                        break;
                    case "ZipCode":
                        string zipcode = string.Empty;
                        if (p.Value != null)
                        {
                            zipcode = p.Value.ToString();
                        }
                        dataItem.ZipCode = zipcode;
                        break;
                    case "Address":
                        string address = string.Empty;
                        if (p.Value != null)
                        {
                            address = p.Value.ToString();
                        }
                        dataItem.Address = address;
                        break;
                    case "Description":
                        string description = string.Empty;
                        if (p.Value != null)
                        {
                            description = p.Value.ToString();
                        }
                        dataItem.Description = description;
                        break;
                    case "ActionKey":
                        string actionkey = string.Empty;
                        if (p.Value != null)
                        {
                            actionkey = p.Value.ToString();
                        }
                        dataItem.ActionKey = actionkey;
                        break;
                    case "ProfilePicture":
                        string profilepicture = string.Empty;
                        if (p.Value != null)
                        {
                            profilepicture = p.Value.ToString();
                        }
                        dataItem.ProfilePicture = profilepicture;
                        break;
                    case "GalleryURL":
                        string galleryurl = string.Empty;
                        if (p.Value != null)
                        {
                            galleryurl = p.Value.ToString();
                        }
                        dataItem.GalleryURL = galleryurl;
                        break;
                    case "Rating":
                        int? rating = null;
                        if (p.Value != null)
                        {
                            rating = Convert.ToInt32(p.Value);
                        }
                        dataItem.Rating = rating;
                        break;
                    case "LastLoginDate":
                        if (p.Value != null)
                        {
                            DateTime lastlogindate = Convert.ToDateTime(p.Value);
                            dataItem.LastLoginDate = lastlogindate;
                        }
                        break;
                    case "FacebookID":
                        string facebookid = string.Empty;
                        if (p.Value != null)
                        {
                            facebookid = p.Value.ToString();
                        }
                        dataItem.FacebookID = facebookid;
                        break;
                    case "FacebookEmail":
                        string facebookemail = string.Empty;
                        if (p.Value != null)
                        {
                            facebookemail = p.Value.ToString();
                        }
                        dataItem.FacebookEmail = facebookemail;
                        break;
                    case "FacebookAccessToken":
                        string facebookaccesstoken = string.Empty;
                        if (p.Value != null)
                        {
                            facebookaccesstoken = p.Value.ToString();
                        }
                        dataItem.FacebookAccessToken = facebookaccesstoken;
                        break;
                    case "TwitterID":
                        string twitterid = string.Empty;
                        if (p.Value != null)
                        {
                            twitterid = p.Value.ToString();
                        }
                        dataItem.TwitterID = twitterid;
                        break;
                    case "TwitterAccessToken":
                        string twitteraccesstoken = string.Empty;
                        if (p.Value != null)
                        {
                            twitteraccesstoken = p.Value.ToString();
                        }
                        dataItem.TwitterAccessToken = twitteraccesstoken;
                        break;
                    case "LinkedInID":
                        string linkedinid = string.Empty;
                        if (p.Value != null)
                        {
                            linkedinid = p.Value.ToString();
                        }
                        dataItem.LinkedInID = linkedinid;
                        break;
                    case "LinkedInAccessToken":
                        string linkedinaccesstoken = string.Empty;
                        if (p.Value != null)
                        {
                            linkedinaccesstoken = p.Value.ToString();
                        }
                        dataItem.LinkedInAccessToken = linkedinaccesstoken;
                        break;
                    case "GoogleID":
                        string Googleid = string.Empty;
                        if (p.Value != null)
                        {
                            Googleid = p.Value.ToString();
                        }
                        dataItem.GoogleID = Googleid;
                        break;
                    case "GoogleAccessToken":
                        string Googleaccesstoken = string.Empty;
                        if (p.Value != null)
                        {
                            Googleaccesstoken = p.Value.ToString();
                        }
                        dataItem.GoogleAccessToken = Googleaccesstoken;
                        break;
                    case "City":
                        string city = string.Empty;
                        if (p.Value != null)
                        {
                            city = p.Value.ToString();
                        }
                        dataItem.City = city;
                        break;
                    case "Country":
                        string country = string.Empty;
                        if (p.Value != null)
                        {
                            country = p.Value.ToString();
                        }
                        dataItem.Country = country;
                        break;
                }
            }
            dataItem.ModifiedDate = DateTime.Now;
            _userRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public UserDTO SaveOrUpdate(UserDTO item)
        {
            User dataItem;
            if (item.UserID != 0)
            {
                //set logic when update Department
                dataItem = _userRepository.GetById(item.UserID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new User();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.FirstName = item.FirstName;
            dataItem.LastName = item.LastName;
            dataItem.UserName = item.UserName;
            dataItem.Salt = item.Salt;
            dataItem.NewSalt = item.NewSalt;
            dataItem.Password = item.Password;
            dataItem.NewPassword = item.NewPassword;
            dataItem.Email = item.Email;
            dataItem.DateOfBirth = item.DateOfBirth;
            dataItem.ZipCode = item.ZipCode;
            dataItem.Address = item.Address;
            dataItem.Description = item.Description;
            dataItem.Lock = item.Lock;
            dataItem.ActionKey = item.ActionKey;
            dataItem.ProfilePicture = item.ProfilePicture;
            dataItem.GalleryURL = item.GalleryURL;
            dataItem.Rating = item.Rating;
            dataItem.ValidKey = item.ValidKey;
            dataItem.LastLoginDate = item.LastLoginDate;
            dataItem.FacebookID = item.FacebookID;
            dataItem.FacebookEmail = item.FacebookEmail;
            dataItem.FacebookAccessToken = item.FacebookAccessToken;
            dataItem.TwitterID = item.TwitterID;
            dataItem.TwitterAccessToken = item.TwitterAccessToken;
            dataItem.LinkedInID = item.LinkedInID;
            dataItem.LinkedInAccessToken = item.LinkedInAccessToken;
            dataItem.GoogleID = item.GoogleID;
            dataItem.GoogleAccessToken = item.GoogleAccessToken;
            dataItem.Country = item.Country;
            dataItem.City = item.City;
            if (item.UserID != 0)
                _userRepository.Update(dataItem);
            else
                _userRepository.Add(dataItem);
            _unitOfWork.Commit();
            #region add user instance
            if (item.UserID == 0) {
                UserInstanceDTO userin = new UserInstanceDTO();
                userin.UserID = dataItem.UserID;
                userin.Guid = Guid.NewGuid();
                _userinstanceLogic.SaveOrUpdate(userin);
            } 
            #endregion
            item = ConvertToDTO(dataItem);
            return item;
        }
        public UserDTO Delete(int id)
        {
            User dataObj = _userRepository.GetById(id);
            _userRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private UserDTO ConvertToDTO(User data)
        {
            if (data != null)
            {
                UserDTO dto = new UserDTO();
                dto.UserID = data.UserID;
                dto.FirstName = data.FirstName;
                dto.LastName = data.LastName;
                dto.UserName = data.UserName;
                dto.Salt = data.Salt;
                dto.NewSalt = data.NewSalt;
                dto.Password = data.Password;
                dto.NewPassword = data.NewPassword;
                dto.Email = data.Email;
                dto.DateOfBirth = data.DateOfBirth;
                dto.ZipCode = data.ZipCode;
                dto.Address = data.Address;
                dto.Description = data.Description;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.Lock = data.Lock;
                dto.ActionKey = data.ActionKey;
                dto.ProfilePicture = data.ProfilePicture;
                dto.GalleryURL = data.GalleryURL;
                dto.Rating = data.Rating;
                dto.Active = data.Active;
                dto.ValidKey = data.ValidKey;
                dto.LastLoginDate = data.LastLoginDate;
                dto.FacebookID = data.FacebookID;
                dto.FacebookEmail = data.FacebookEmail;
                dto.FacebookAccessToken = data.FacebookAccessToken;
                dto.TwitterID = data.TwitterID;
                dto.TwitterAccessToken = data.TwitterAccessToken;
                dto.LinkedInID = data.LinkedInID;
                dto.LinkedInAccessToken = data.LinkedInAccessToken;
                dto.GoogleID = data.GoogleID;
                dto.GoogleAccessToken = data.GoogleAccessToken;
                dto.Country = data.Country;
                dto.City = data.City;
                return dto;
            }
            return null;
        }
        public List<UserDTO> Update(List<UserDTO> list)
        {
            try
            {
                List<UserDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<UserDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<UserDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<User> listUpdate = new List<User>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.UserID).ToArray();
                    _userRepository.Delete(u => listids.Contains(u.UserID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.UserID).ToArray();
                    List<User> listData = _userRepository.GetMany(u => listids.Contains(u.UserID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.UserID == data.UserID);
                            ConvertDTOToData(dto, data);
                            _userRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, User> dictNew = new Dictionary<string, User>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new User();
                        ConvertDTOToData(dto, dataItem);
                        _userRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.UserID = dictNew[item.GUID].UserID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(UserDTO dto, User data)
        {
            data.UserID = dto.UserID;
            data.FirstName = dto.FirstName;
            data.LastName = dto.LastName;
            data.UserName = dto.UserName;
            data.Salt = dto.Salt;
            data.NewSalt = dto.NewSalt;
            data.Password = dto.Password;
            data.NewPassword = dto.NewPassword;
            data.Email = dto.Email;
            data.DateOfBirth = dto.DateOfBirth;
            data.ZipCode = dto.ZipCode;
            data.Address = dto.Address;
            data.Description = dto.Description;
            data.Lock = dto.Lock;
            data.ActionKey = dto.ActionKey;
            data.ProfilePicture = dto.ProfilePicture;
            data.GalleryURL = dto.GalleryURL;
            data.Rating = dto.Rating;
            data.ValidKey = dto.ValidKey;
            data.LastLoginDate = dto.LastLoginDate;
            data.FacebookID = dto.FacebookID;
            data.FacebookEmail = dto.FacebookEmail;
            data.FacebookAccessToken = dto.FacebookAccessToken;
            data.TwitterID = dto.TwitterID;
            data.TwitterAccessToken = dto.TwitterAccessToken;
            data.LinkedInID = dto.LinkedInID;
            data.LinkedInAccessToken = dto.LinkedInAccessToken;
            data.GoogleID = dto.GoogleID;
            data.GoogleAccessToken = dto.GoogleAccessToken;
            data.Country = dto.Country;
            data.City = dto.City;
            if (dto.UserID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IUserLogic
    {
        UserDTO Get(int id);
        UserDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<UserDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams);
        UserDTO SaveOrUpdate(UserDTO item);
        UserDTO Update(List<AddedParam> addParams);
        UserDTO Delete(int id);
        List<UserDTO> Update(List<UserDTO> list);
    }
}
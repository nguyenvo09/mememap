using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class MapLogic : IMapLogic
    {
        public IUnitOfWork _unitOfWork;
        IMapRepository _mapRepository;
        public MapLogic(IUnitOfWork unitOfWork, IMapRepository mapRepository)
        {
            this._unitOfWork = unitOfWork;
            this._mapRepository = mapRepository;
        }
        public IEnumerable<MapDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _mapRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "SearchCode":
                            string searchcode = string.Empty; ;
                            if (p.Value != null)
                            {
                                searchcode = p.Value.ToString().ToLower();
                                query = query.Where(u => u.Name.ToLower().Contains(searchcode));
                            }

                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "ProjectID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ProjectID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ProjectID);
                        }
                        break;
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.MapID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.MapID);
            }
            var data = (from u in query
                        select new MapDTO
                         {
                             MapID = u.MapID,
                             ProjectID = u.ProjectID,
                             Name = u.Name,
                             Description = u.Description,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate,
                             ProjectName = u.Project != null ? u.Project.Name : ""
                         });
            return data;
        }
        public ListDTOModel<MapDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<MapDTO> rList = new ListDTOModel<MapDTO>();
            var query = _mapRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "ProjectID":
                            int projectid = 0;
                            if (p.Value != null)
                            {
                                projectid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.ProjectID == projectid);
                            break;
                        case "SearchCode":
                            string searchcode = string.Empty; ;
                            if (p.Value != null)
                            {
                                searchcode = p.Value.ToString().ToLower();
                                query = query.Where(u => u.Name.ToLower().Contains(searchcode));
                            }

                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "ProjectID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.ProjectID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.ProjectID);
                        }
                        break;
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Description":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Description);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Description);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.MapID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.MapID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new MapDTO
                        {
                            MapID = u.MapID,
                            ProjectID = u.ProjectID,
                            Name = u.Name,
                            Description = u.Description,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            ProjectName = u.Project != null ? u.Project.Name : ""
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _mapRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public MapDTO Get(int id)
        {

            Map item = _mapRepository.GetById(id);
            MapDTO dto = ConvertToDTO(item);
            item.ModifiedDate = DateTime.Now;
            _mapRepository.Update(item);
            return dto;
        }
        public MapDTO Get(List<AddedParam> addedParams)
        {
            var query = _mapRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Map item = query.FirstOrDefault();
            item.ModifiedDate = DateTime.Now;
            _mapRepository.Update(item);
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _mapRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "NumOfMap":
                            var userparam = addedParams.FirstOrDefault(u => u.Key == "UserInstanceID");
                            if (userparam == null) return 0;
                            int userinstanceid = Convert.ToInt32(userparam.Value);
                            return query.Count(u => u.CreatedBy == userinstanceid);
                    }
                }
            }
            return query.Count();
        }
        public MapDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "MapID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int mapid = Convert.ToInt32(addParam.Value);
            Map dataItem = _mapRepository.GetById(mapid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "ProjectID":
                        int projectid = 0;
                        if (p.Value != null)
                        {
                            projectid = Convert.ToInt32(p.Value);
                        }
                        dataItem.ProjectID = projectid;
                        break;
                    case "Name":
                        string name = string.Empty;
                        if (p.Value != null)
                        {
                            name = p.Value.ToString();
                        }
                        dataItem.Name = name;
                        break;
                    case "Description":
                        string description = string.Empty;
                        if (p.Value != null)
                        {
                            description = p.Value.ToString();
                        }
                        dataItem.Description = description;
                        break;
                }
            }
            _mapRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public MapDTO SaveOrUpdate(MapDTO item)
        {
            Map dataItem;
            if (item.MapID != 0)
            {
                //set logic when update Department
                dataItem = _mapRepository.GetById(item.MapID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Map();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.ProjectID = item.ProjectID;
            dataItem.Name = item.Name;
            dataItem.Description = item.Description;
            if (item.MapID != 0)
                _mapRepository.Update(dataItem);
            else
                _mapRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public MapDTO Delete(int id)
        {
            Map dataObj = _mapRepository.GetById(id);
            _mapRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private MapDTO ConvertToDTO(Map data)
        {
            if (data != null)
            {
                MapDTO dto = new MapDTO();
                dto.MapID = data.MapID;
                dto.ProjectID = data.ProjectID;
                dto.Name = data.Name;
                dto.Description = data.Description;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.ProjectName = data.Project != null ? data.Project.Name : "";
                return dto;
            }
            return null;
        }
        public List<MapDTO> Update(List<MapDTO> list)
        {
            try
            {
                List<MapDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<MapDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<MapDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<Map> listUpdate = new List<Map>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.MapID).ToArray();
                    _mapRepository.Delete(u => listids.Contains(u.MapID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.MapID).ToArray();
                    List<Map> listData = _mapRepository.GetMany(u => listids.Contains(u.MapID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.MapID == data.MapID);
                            ConvertDTOToData(dto, data);
                            _mapRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, Map> dictNew = new Dictionary<string, Map>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new Map();
                        ConvertDTOToData(dto, dataItem);
                        _mapRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.MapID = dictNew[item.GUID].MapID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(MapDTO dto, Map data)
        {
            data.MapID = dto.MapID;
            data.ProjectID = dto.ProjectID;
            data.Name = dto.Name;
            data.Description = dto.Description;
            if (dto.MapID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IMapLogic
    {
        MapDTO Get(int id);
        MapDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<MapDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<MapDTO> GetMany(List<AddedParam> addedParams);
        MapDTO SaveOrUpdate(MapDTO item);
        MapDTO Update(List<AddedParam> addParams);
        MapDTO Delete(int id);
        List<MapDTO> Update(List<MapDTO> list);
    }
}

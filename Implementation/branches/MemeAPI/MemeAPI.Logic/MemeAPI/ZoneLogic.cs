using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class ZoneLogic : IZoneLogic
    {
        public IUnitOfWork _unitOfWork;
        IZoneRepository _zoneRepository;
        public ZoneLogic(IUnitOfWork unitOfWork, IZoneRepository zoneRepository)
        {
            this._unitOfWork = unitOfWork;
            this._zoneRepository = zoneRepository;
        }

       
        public IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _zoneRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "MapID":
                            int mapid = 0;
                            if (p.Value != null)
                            {
                                mapid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.MapID == mapid);
                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "MapID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.MapID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.MapID);
                        }
                        break;
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.ZoneID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.ZoneID);
            }
            var data = (from u in query
                        select new ZoneDTO
                         {
                             ZoneID = u.ZoneID,
                             MapID = u.MapID,
                             Data = u.Data,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate,
                             MapName = u.Map != null ? u.Map.Name : ""
                         });
            return data;
        }
        public ListDTOModel<ZoneDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<ZoneDTO> rList = new ListDTOModel<ZoneDTO>();
            var query = _zoneRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "MapID":
                            int mapid = 0;
                            if (p.Value != null)
                            {
                                mapid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.MapID == mapid);
                            break;
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "MapID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.MapID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.MapID);
                        }
                        break;
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.ZoneID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.ZoneID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new ZoneDTO
                        {
                            ZoneID = u.ZoneID,
                            MapID = u.MapID,
                            Data = u.Data,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            MapName = u.Map != null ? u.Map.Name : ""
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _zoneRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public ZoneDTO Get(int id)
        {
            return ConvertToDTO(_zoneRepository.GetById(id));
        }
        public ZoneDTO Get(List<AddedParam> addedParams)
        {
            var query = _zoneRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Zone item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _zoneRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public ZoneDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "ZoneID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int zoneid = Convert.ToInt32(addParam.Value);
            Zone dataItem = _zoneRepository.GetById(zoneid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "MapID":
                        int mapid = 0;
                        if (p.Value != null)
                        {
                            mapid = Convert.ToInt32(p.Value);
                        }
                        dataItem.MapID = mapid;
                        break;
                    case "Data":
                        string data = string.Empty;
                        if (p.Value != null)
                        {
                            data = p.Value.ToString();
                        }
                        dataItem.Data = data;
                        break;
                }
            }
            _zoneRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public ZoneDTO SaveOrUpdate(ZoneDTO item)
        {
            Zone dataItem;
            if (item.ZoneID != 0)
            {
                //set logic when update Department
                dataItem = _zoneRepository.GetById(item.ZoneID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Zone();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.MapID = item.MapID;
            dataItem.Data = item.Data;
            if (item.ZoneID != 0)
                _zoneRepository.Update(dataItem);
            else
                _zoneRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public ZoneDTO Delete(int id)
        {
            Zone dataObj = _zoneRepository.GetById(id);
            _zoneRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private ZoneDTO ConvertToDTO(Zone data)
        {
            if (data != null)
            {
                ZoneDTO dto = new ZoneDTO();
                dto.ZoneID = data.ZoneID;
                dto.MapID = data.MapID;
                dto.Data = data.Data;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.MapName = data.Map != null ? data.Map.Name : "";
                return dto;
            }
            return null;
        }
        public List<ZoneDTO> Update(List<ZoneDTO> list)
        {
            try
            {
                List<ZoneDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<ZoneDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<ZoneDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<Zone> listUpdate = new List<Zone>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.ZoneID).ToArray();
                    _zoneRepository.Delete(u => listids.Contains(u.ZoneID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.ZoneID).ToArray();
                    List<Zone> listData = _zoneRepository.GetMany(u => listids.Contains(u.ZoneID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.ZoneID == data.ZoneID);
                            ConvertDTOToData(dto, data);
                            _zoneRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, Zone> dictNew = new Dictionary<string, Zone>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new Zone();
                        ConvertDTOToData(dto, dataItem);
                        _zoneRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.ZoneID = dictNew[item.GUID].ZoneID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(ZoneDTO dto, Zone data)
        {
            data.ZoneID = dto.ZoneID;
            data.MapID = dto.MapID;
            data.Data = dto.Data;
            if (dto.ZoneID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IZoneLogic
    {
        ZoneDTO Get(int id);
        ZoneDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<ZoneDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<ZoneDTO> GetMany(List<AddedParam> addedParams);
        ZoneDTO SaveOrUpdate(ZoneDTO item);
        ZoneDTO Update(List<AddedParam> addParams);
        ZoneDTO Delete(int id);
        List<ZoneDTO> Update(List<ZoneDTO> list);
    }
}

using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class EntityLogic : IEntityLogic
    {
        public IUnitOfWork _unitOfWork;
        IEntityRepository _entityRepository;
        public EntityLogic(IUnitOfWork unitOfWork, IEntityRepository entityRepository)
        {
            this._unitOfWork = unitOfWork;
            this._entityRepository = entityRepository;
        }
        public IEnumerable<EntityDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _entityRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Property":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Property);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Property);
                        }
                        break;
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.EntityID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.EntityID);
            }
            var data = (from u in query
                        select new EntityDTO
                         {
                             EntityID = u.EntityID,
                             Name = u.Name,
                             Property = u.Property,
                             Data = u.Data,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate
                         });
            return data;
        }
        public ListDTOModel<EntityDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<EntityDTO> rList = new ListDTOModel<EntityDTO>();
            var query = _entityRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "Name":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Name);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Name);
                        }
                        break;
                    case "Property":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Property);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Property);
                        }
                        break;
                    case "Data":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Data);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Data);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.EntityID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.EntityID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new EntityDTO
                        {
                            EntityID = u.EntityID,
                            Name = u.Name,
                            Property = u.Property,
                            Data = u.Data,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _entityRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public EntityDTO Get(int id)
        {
            return ConvertToDTO(_entityRepository.GetById(id));
        }
        public EntityDTO Get(List<AddedParam> addedParams)
        {
            var query = _entityRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Entity item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _entityRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public EntityDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "EntityID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int entityid = Convert.ToInt32(addParam.Value);
            Entity dataItem = _entityRepository.GetById(entityid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "Name":
                        string name = string.Empty;
                        if (p.Value != null)
                        {
                            name = p.Value.ToString();
                        }
                        dataItem.Name = name;
                        break;
                    case "Property":
                        string property = string.Empty;
                        if (p.Value != null)
                        {
                            property = p.Value.ToString();
                        }
                        dataItem.Property = property;
                        break;
                    case "Data":
                        string data = string.Empty;
                        if (p.Value != null)
                        {
                            data = p.Value.ToString();
                        }
                        dataItem.Data = data;
                        break;
                }
            }
            _entityRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public EntityDTO SaveOrUpdate(EntityDTO item)
        {
            Entity dataItem;
            if (item.EntityID != 0)
            {
                //set logic when update Department
                dataItem = _entityRepository.GetById(item.EntityID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Entity();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.Name = item.Name;
            dataItem.Property = item.Property;
            dataItem.Data = item.Data;
            if (item.EntityID != 0)
                _entityRepository.Update(dataItem);
            else
                _entityRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public EntityDTO Delete(int id)
        {
            Entity dataObj = _entityRepository.GetById(id);
            _entityRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private EntityDTO ConvertToDTO(Entity data)
        {
            if (data != null)
            {
                EntityDTO dto = new EntityDTO();
                dto.EntityID = data.EntityID;
                dto.Name = data.Name;
                dto.Property = data.Property;
                dto.Data = data.Data;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                return dto;
            }
            return null;
        }
        public List<EntityDTO> Update(List<EntityDTO> list)
        {
            try
            {
                List<EntityDTO> listDelete = list.Where(u => u.EditMode == "Delete").ToList();
                List<EntityDTO> listNew = list.Where(u => u.EditMode == "New").ToList();
                List<EntityDTO> listEdit = list.Where(u => u.EditMode == "Edit").ToList();
                List<Entity> listUpdate = new List<Entity>();
                if (listDelete != null && listDelete.Count > 0)
                {
                    int[] listids = listDelete.Select(u => u.EntityID).ToArray();
                    _entityRepository.Delete(u => listids.Contains(u.EntityID));
                }
                if (listEdit != null && listEdit.Count > 0)
                {
                    int[] listids = listEdit.Select(u => u.EntityID).ToArray();
                    List<Entity> listData = _entityRepository.GetMany(u => listids.Contains(u.EntityID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = listEdit.First(u => u.EntityID == data.EntityID);
                            ConvertDTOToData(dto, data);
                            _entityRepository.Update(data);
                            dto = ConvertToDTO(data);
                        }
                    }
                }
                IDictionary<string, Entity> dictNew = new Dictionary<string, Entity>();
                if (listNew != null && listNew.Count > 0)
                {
                    foreach (var dto in listNew)
                    {
                        var dataItem = new Entity();
                        ConvertDTOToData(dto, dataItem);
                        _entityRepository.Add(dataItem);
                        dictNew.Add(dto.GUID, dataItem);
                    }
                }
                _unitOfWork.Commit();
                foreach (var item in listNew)
                {
                    item.EntityID = dictNew[item.GUID].EntityID;
                }
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(EntityDTO dto, Entity data)
        {
            data.EntityID = dto.EntityID;
            data.Name = dto.Name;
            data.Property = dto.Property;
            data.Data = dto.Data;
            if (dto.EntityID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface IEntityLogic
    {
        EntityDTO Get(int id);
        EntityDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<EntityDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<EntityDTO> GetMany(List<AddedParam> addedParams);
        EntityDTO SaveOrUpdate(EntityDTO item);
        EntityDTO Update(List<AddedParam> addParams);
        EntityDTO Delete(int id);
        List<EntityDTO> Update(List<EntityDTO> list);
    }
}

using System;
using System.Linq;
using System.Collections.Generic;
using BaseAPI.DAL;
using BaseAPI.Model.DTO;
using MemeAPI.Model.DTO;
using MemeAPI.Repository;
using MemeAPI.Model;
namespace MemeAPI.Logic
{
    public class NoteLogic : INoteLogic
    {
        public IUnitOfWork _unitOfWork;
        INoteRepository _noteRepository;
        public NoteLogic(IUnitOfWork unitOfWork, INoteRepository noteRepository)
        {
            this._unitOfWork = unitOfWork;
            this._noteRepository = noteRepository;
        }

       
        public IEnumerable<NoteDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _noteRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "MapID":
                            int mapid = 0;
                            if (p.Value != null)
                            {
                                mapid = Convert.ToInt32(p.Value);
                            }
                            query = query.Where(u => u.MapID == mapid);
                            break;
                    }
                    
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "MapID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.MapID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.MapID);
                        }
                        break;
                    case "RID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.RID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.RID);
                        }
                        break;
                    case "Type":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Type);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Type);
                        }
                        break;
                    case "Url":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Url);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.NoteID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.NoteID);
            }
            var data = (from u in query
                        select new NoteDTO
                         {
                             NoteID = u.NoteID,
                             MapID = u.MapID,
                             RID = u.RID,
                             Type = u.Type,
                             Url = u.Url,
                             Body = u.Body,
                             Active = u.Active,
                             CreatedBy = u.CreatedBy,
                             CreatedDate = u.CreatedDate,
                             ModifiedBy = u.ModifiedBy,
                             ModifiedDate = u.ModifiedDate
                         });
            return data;
        }
        public ListDTOModel<NoteDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<NoteDTO> rList = new ListDTOModel<NoteDTO>();
            var query = _noteRepository.GetAllQueryable();
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            //sort param
            var sortedfieldparam = addedParams.FirstOrDefault(u => u.Key == "SortedField");
            if (sortedfieldparam != null)
            {
                var sorteddiretionparam = addedParams.FirstOrDefault(u => u.Key == "SortedDirection");
                bool sortdiretion = Convert.ToBoolean(sorteddiretionparam.Value);
                switch (sortedfieldparam.Value.ToString())
                {
                    case "MapID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.MapID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.MapID);
                        }
                        break;
                    case "Url":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Url);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Url);
                        }
                        break;
                    case "RID":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.RID);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.RID);
                        }
                        break;
                    case "Type":
                        if (sortdiretion)
                        {
                            query = query.OrderByDescending(u => u.Type);
                        }
                        else
                        {
                            query = query.OrderBy(u => u.Type);
                        }
                        break;
                    default:
                        query = query.OrderBy(u => u.NoteID);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(u => u.NoteID);
            }
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = (from u in query
                        select new NoteDTO
                        {
                            NoteID = u.NoteID,
                            MapID = u.MapID,
                            RID = u.RID,
                            Url = u.Url,
                            Body = u.Body,
                            Type= u.Type,
                            Active = u.Active,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.Source = data.ToList();
            return rList;
        }
        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _noteRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count() > 0;
        }
        public NoteDTO Get(int id)
        {
            return ConvertToDTO(_noteRepository.GetById(id));
        }
        public NoteDTO Get(List<AddedParam> addedParams)
        {
            var query = _noteRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            Note item = query.FirstOrDefault();
            return ConvertToDTO(item);
        }
        public Object GetScalar(List<AddedParam> addedParams)
        {
            var query = _noteRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                    }
                }
            }
            return query.Count();
        }
        public NoteDTO Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "NoteID");
            if (addParam == null) return null;
            addParams.Remove(addParam);
            int noteid = Convert.ToInt32(addParam.Value);
            Note dataItem = _noteRepository.GetById(noteid);
            if (dataItem == null) return null;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "MapID":
                        int mapid = 0;
                        if (p.Value != null)
                        {
                            mapid = Convert.ToInt32(p.Value);
                        }
                        dataItem.MapID = mapid;
                        break;
                    case "Body":
                        string body = string.Empty;
                        if (p.Value != null)
                        {
                            body = p.Value.ToString();
                        }
                        dataItem.Body = body;
                        break;
                    case "RID":
                        string rid = "";
                        if (p.Value != null)
                        {
                            rid = p.Value.ToString();
                        }
                        dataItem.RID = rid;
                        break;
                    case "Type":
                        string type = string.Empty;
                        if (p.Value != null)
                        {
                            type = p.Value.ToString();
                        }
                        dataItem.Type = type;
                        break;
                }
            }
            _noteRepository.Update(dataItem);
            _unitOfWork.Commit();
            var item = ConvertToDTO(dataItem);
            return item;
        }
        public NoteDTO SaveOrUpdate(NoteDTO item)
        {
            Note dataItem;
            if (item.NoteID != 0)
            {
                //set logic when update Department
                dataItem = _noteRepository.GetById(item.NoteID);
            }
            else
            {
                //set logic when create new Department
                dataItem = new Note();
                dataItem.CreatedBy = item.CreatedBy;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = true;
            }
            //Set common properties of (create and update)
            dataItem.MapID = item.MapID;
            dataItem.Body = item.Body;
            dataItem.Type = item.Type;
            dataItem.Url = item.Url;
            dataItem.RID = item.RID;
            if (item.NoteID != 0)
                _noteRepository.Update(dataItem);
            else
                _noteRepository.Add(dataItem);
            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return item;
        }
        public NoteDTO Delete(int id)
        {
            Note dataObj = _noteRepository.GetById(id);
            _noteRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private NoteDTO ConvertToDTO(Note data)
        {
            if (data != null)
            {
                NoteDTO dto = new NoteDTO();
                dto.NoteID = data.NoteID;
                dto.MapID = data.MapID;
                dto.Body = data.Body;
                dto.RID = data.RID;
                dto.Type = data.Type;
                dto.Active = data.Active;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                return dto;
            }
            return null;
        }
        public List<NoteDTO> Update(List<NoteDTO> list)
        {
            try
            {

                if (list != null && list.Count > 0)
                {
                    int[] listids = list.Select(u => u.NoteID).ToArray();
                    List<Note> listData = _noteRepository.GetMany(u => listids.Contains(u.NoteID)).ToList();
                    if (listData.Count > 0)
                    {
                        foreach (var data in listData)
                        {
                            var dto = list.First(u => u.NoteID == data.NoteID);
                            ConvertDTOToData(dto, data);
                            _noteRepository.Update(data);
                        }
                    }
                }
                
                _unitOfWork.Commit();
                
                return list;
            }
            catch
            {
            }
            return null;
        }
        private void ConvertDTOToData(NoteDTO dto, Note data)
        {
            data.NoteID = dto.NoteID;
            data.MapID = dto.MapID;
            data.Body = dto.Body;
            data.RID = dto.RID;
            data.Type = dto.Type;
            if (dto.NoteID > 0)
            {
                data.ModifiedBy = dto.CreatedBy;
                data.ModifiedDate = DateTime.Now;
            }
            else
            {
                data.Active = true;
                data.CreatedBy = dto.CreatedBy;
                data.CreatedDate = DateTime.Now;
            }
        }
    }
    public interface INoteLogic
    {
        NoteDTO Get(int id);
        NoteDTO Get(List<AddedParam> addParams);
        bool GetExist(System.Collections.Generic.List<AddedParam> addedParams);
        ListDTOModel<NoteDTO> GetPage(List<AddedParam> addedParams);
        object GetScalar(System.Collections.Generic.List<AddedParam> addedParams);
        IEnumerable<NoteDTO> GetMany(List<AddedParam> addedParams);
        NoteDTO SaveOrUpdate(NoteDTO item);
        NoteDTO Update(List<AddedParam> addParams);
        NoteDTO Delete(int id);
        List<NoteDTO> Update(List<NoteDTO> list);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;
using System.Configuration;
using Base.Validation;
using System.Net;

namespace Base.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UserNameEntryValidation : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' ";
        private string _userNameFieldName;
        private string _toggleUserNameFieldName;

        public UserNameEntryValidation(string userNameFieldName, string toggleUserNameFieldName)
            : base(_defaultErrorMessage)
        {
            _userNameFieldName = userNameFieldName;
            _toggleUserNameFieldName = toggleUserNameFieldName;
        }

        public override string FormatErrorMessage(string name)
        {
            if (!string.IsNullOrWhiteSpace(base.ErrorMessage)) return base.ErrorMessage;
            return string.Format("UserName not valid");
        }

        //Override IsValid  
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();

            var username = Convert.ToString(containerType.GetProperty(this._userNameFieldName).GetValue(validationContext.ObjectInstance, null));
            var isUsingUserName = Convert.ToBoolean(containerType.GetProperty(this._toggleUserNameFieldName).GetValue(validationContext.ObjectInstance, null));

            if (isUsingUserName)
            {
                return null;
            }
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^([a-zA-Z])+([a-zA-Z0-9_\-\.])*");

            if (regex.Match(username).Success)
            {
                return null;
            }else{
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }
        }
    }
}

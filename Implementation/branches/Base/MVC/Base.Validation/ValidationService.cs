﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace Base.Validation
{
    public partial class ValidationService
    {
        private string _baseURI;
        private HttpClient httpClient;
        public ValidationService(string baseURI)
        {
            _baseURI = baseURI;
            httpClient = new HttpClient() { BaseAddress = new Uri(_baseURI) };
        }

        #region UserValidation
        public bool GetExistEmail(int? userId, string email, out HttpStatusCode hCode)
        {
            try
            {
                HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetExistEmail?userid={0}&email={1}", userId, email)).Result;
                hCode = response.StatusCode;
                return response.Content.ReadAsAsync<bool>().Result;
            }
            catch
            {
                hCode = HttpStatusCode.RequestTimeout;
                return false;
            }
        }
        public bool GetExistUserName(int? userId, string userName, out HttpStatusCode hCode)
        {
            HttpResponseMessage response = httpClient.GetAsync(string.Format("api/User/GetExistUserName?userid={0}&username={1}", userId, userName)).Result;
            hCode = response.StatusCode;
            return response.Content.ReadAsAsync<bool>().Result;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;
using System.Configuration;
using Base.Validation;
using System.Net;

namespace Base.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class ExistUserNameValidation : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' ";
        private string _usernameFieldName;


        public ExistUserNameValidation(string usernameFieldName)
            : base(_defaultErrorMessage)
        {
            _usernameFieldName = usernameFieldName;
        }

        public override string FormatErrorMessage(string name)
        {
            if (!string.IsNullOrWhiteSpace(base.ErrorMessage)) return base.ErrorMessage;
            return string.Format("UserName not exist in system");
        }

        //Override IsValid  
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            HttpStatusCode hCode;
            ValidationService validationService = new ValidationService(ConfigurationManager.AppSettings["baseURI"]);
            var containerType = validationContext.ObjectInstance.GetType();
            var username = containerType.GetProperty(this._usernameFieldName);
            var usernameValue = username.GetValue(validationContext.ObjectInstance, null).ToString();
            bool exist = validationService.GetExistUserName(null, usernameValue, out hCode);
            if (hCode == HttpStatusCode.RequestTimeout)
                return new ValidationResult("Can't check validation for UserName");
            if (exist ==false)
            {
                return null;
            }
            else
            {
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }
        }
    }
}

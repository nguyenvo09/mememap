﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;
using System.Configuration;
using Base.Validation;
using System.Net;

namespace Base.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class EmailEntryValidation : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' ";
        private string _emailFieldName;
        private string _toggleemailFieldName;

        public EmailEntryValidation(string emailFieldName, string toggleemailFieldName)
            : base(_defaultErrorMessage)
        {
            _emailFieldName = emailFieldName;
            _toggleemailFieldName = toggleemailFieldName;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("Email not valid");
        }

        //Override IsValid  
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();

            var email = Convert.ToString(containerType.GetProperty(this._emailFieldName).GetValue(validationContext.ObjectInstance, null));
            var isUsingEmail = Convert.ToBoolean(containerType.GetProperty(this._toggleemailFieldName).GetValue(validationContext.ObjectInstance, null));

            if (!isUsingEmail)
            {
                return null;
            }
            System.Text.RegularExpressions.Regex regex= new System.Text.RegularExpressions.Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (regex.Match(email).Success)
            {
                return null;
            }else{
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }
        }
    }
}

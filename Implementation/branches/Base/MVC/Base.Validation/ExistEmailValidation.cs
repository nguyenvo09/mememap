﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;
using System.Configuration;
using Base.Validation;
using System.Net;

namespace Base.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class ExistEmailValidation : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' ";
        private string _emailFieldName;


        public ExistEmailValidation(string emailFieldName)
            : base(_defaultErrorMessage)
        {
            _emailFieldName = emailFieldName;
        }

        public override string FormatErrorMessage(string name)
        {
            if (!string.IsNullOrWhiteSpace(base.ErrorMessage)) return base.ErrorMessage;
            return string.Format("Email not exist in system");
        }

        //Override IsValid  
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            HttpStatusCode hCode;
            ValidationService validationService = new ValidationService(ConfigurationManager.AppSettings["baseURI"]);
            var containerType = validationContext.ObjectInstance.GetType();
            var email = containerType.GetProperty(this._emailFieldName);
            var emailValue = email.GetValue(validationContext.ObjectInstance, null).ToString();
            bool exist = validationService.GetExistEmail(null,emailValue, out hCode);
            if (hCode == HttpStatusCode.RequestTimeout)
                return new ValidationResult("Can't check validation for Email");
            if (exist==false)
            {
                return null;
            }
            else
            {
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }
        }
    }
}

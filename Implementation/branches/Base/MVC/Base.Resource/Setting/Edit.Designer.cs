﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Base.Resource.Setting {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Edit {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Edit() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Base.Resource.Setting.Edit", typeof(Edit).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string Button_Close {
            get {
                return ResourceManager.GetString("Button_Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string Button_Save {
            get {
                return ResourceManager.GetString("Button_Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address:.
        /// </summary>
        public static string Lable_Address {
            get {
                return ResourceManager.GetString("Lable_Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of birth:.
        /// </summary>
        public static string Lable_DateOfBirth {
            get {
                return ResourceManager.GetString("Lable_DateOfBirth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description:.
        /// </summary>
        public static string Lable_Description {
            get {
                return ResourceManager.GetString("Lable_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit your profile.
        /// </summary>
        public static string Lable_EditYourProfile {
            get {
                return ResourceManager.GetString("Lable_EditYourProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name:.
        /// </summary>
        public static string Lable_FirstName {
            get {
                return ResourceManager.GetString("Lable_FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name:.
        /// </summary>
        public static string Lable_LastName {
            get {
                return ResourceManager.GetString("Lable_LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string Lable_Password {
            get {
                return ResourceManager.GetString("Lable_Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Name:.
        /// </summary>
        public static string Lable_UserName {
            get {
                return ResourceManager.GetString("Lable_UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip code:.
        /// </summary>
        public static string Lable_ZipCode {
            get {
                return ResourceManager.GetString("Lable_ZipCode", resourceCulture);
            }
        }
    }
}

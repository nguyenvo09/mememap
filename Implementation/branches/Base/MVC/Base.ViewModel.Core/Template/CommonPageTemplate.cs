﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class CommonPageTemplate
    {
        public bool IsRowFluid { get; set; }
        public List<ZoneTemplate> ZoneTree { get; set; }
        public List<MultiTabTemplate> MultiTabs { get; set; }
        public List<FieldMappingPoint> MappingPoints { get; set; }
        public List<ComboBoxTemplate> ComboBoxTemplates { get; set; }
        public List<ScriptRegister> ScriptRegisters { get; set; }
        public List<StyleRegister> StyleRegisters { get; set; }
        public List<ExtraField> ExtraFields { get; set; }
        public CommonPageTemplate()
        {
            ComboBoxTemplates = new List<ComboBoxTemplate>();
            MappingPoints = new List<FieldMappingPoint>();
            ZoneTree = new List<ZoneTemplate>();
            MultiTabs = new List<MultiTabTemplate>();
            ScriptRegisters = new List<ScriptRegister>();
            StyleRegisters = new List<StyleRegister>();
            ExtraFields = new List<ExtraField>();
        }
        public CommonPageTemplate(List<ZoneTemplate> zoneTree, List<FieldMappingPoint> mappingPoints)
        {
            ZoneTree = zoneTree;
            MappingPoints = mappingPoints;
            ComboBoxTemplates = new List<ComboBoxTemplate>();
            MultiTabs = new List<MultiTabTemplate>();
            ScriptRegisters = new List<ScriptRegister>();
            StyleRegisters = new List<StyleRegister>();
            ExtraFields = new List<ExtraField>();
        }
        public CommonPageTemplate(List<ZoneTemplate> zoneTree, List<MultiTabTemplate> multiTabs, List<FieldMappingPoint> mappingPoints)
        {
            ZoneTree = zoneTree;
            MappingPoints = mappingPoints;
            MultiTabs = multiTabs;
            ComboBoxTemplates = new List<ComboBoxTemplate>();
            ScriptRegisters = new List<ScriptRegister>();
            StyleRegisters = new List<StyleRegister>();
            ExtraFields = new List<ExtraField>();
        }
    }
}
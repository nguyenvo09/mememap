﻿namespace Base.ViewModel.Core.Template
{
    public class ScriptRegister
    {
        public string Name { get; set; }
        public string Source { get; set; }
        public ScriptType ScriptType { get; set; }
    }
    public enum ScriptType
    {
        ScriptBlock, ScriptFile
    }
}
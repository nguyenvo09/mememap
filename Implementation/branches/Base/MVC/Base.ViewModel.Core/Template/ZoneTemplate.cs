﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class ZoneTemplate
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int Span { get; set; }
        public int Offset { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public bool IsRowFluid { get; set; }
        public List<ZoneTemplate> Childs { get; set; }
        public ZoneTemplate(string name, string title, int span, int offset, int row, int column, bool isRowFluid)
        {
            Name = name;
            Title = title;
            Span = span;
            Offset = offset;
            Row = row;
            Column = column;
            IsRowFluid = isRowFluid;
            Childs = new List<ZoneTemplate>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class MultiTabTemplate
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public MultiTabType TabType { get; set; }
        public List<MultiTabPanelTemplate> TabPanels { get; set; }
        public MultiTabTemplate(string name, string displayName, MultiTabType tabType, List<MultiTabPanelTemplate> tabPanels)
        {
            Name = name;
            DisplayName = displayName;
            TabPanels = tabPanels;
            TabType = tabType;
        }
        public string GetTabType()
        {
            switch (TabType)
            {
                case MultiTabType.Left:
                    return "tabs-left";
                case MultiTabType.Right:
                    return "tabs-right";
                case MultiTabType.Top:
                    return string.Empty;
                case MultiTabType.Bottom:
                    return "tabs-below";
            }
            return string.Empty;
        }
    }
    public class MultiTabPanelTemplate
    {
        public string PanelName { get; set; }
        public string DisPlayName { get; set; }
        public bool IsActive { get; set; }
        public MultiTabPanelTemplate(string panelName, string displayName, bool isActive)
        {
            PanelName = panelName;
            DisPlayName = displayName;
            IsActive = isActive;
        }
        public string GetIsActive()
        {
            return IsActive ? "active" : "";
        }
    }
    public enum MultiTabType
    {
        Left, Right, Top, Bottom
    }
}
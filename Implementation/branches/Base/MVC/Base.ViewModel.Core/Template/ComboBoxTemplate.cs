﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
using System.Web.Mvc;
namespace Base.ViewModel.Core.Template
{
    public class ComboBoxTemplate
    {
        public string ControlName { get; set; }
        public SelectList DataSource { get; set; }
        public ComboBoxTemplate()
        {

        }
        public ComboBoxTemplate(string controlName, SelectList source)
        {
            ControlName = controlName;
            DataSource = source;
        }
    }
}

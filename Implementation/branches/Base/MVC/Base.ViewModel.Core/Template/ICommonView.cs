﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.ViewModel.Core.Template
{
    interface ICommonView
    {
        Base.ViewModel.Core.Template.CommonPageTemplate PageTemplate { get; set; }
    }
}

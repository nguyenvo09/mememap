﻿namespace Base.ViewModel.Core.Template
{
    public class StyleRegister
    {
        public string Name { get; set; }
        public string Source { get; set; }
        public StyleType StyleType { get; set; }
    }
    public enum StyleType
    {
        StyleBlock, StyleFile
    }
}

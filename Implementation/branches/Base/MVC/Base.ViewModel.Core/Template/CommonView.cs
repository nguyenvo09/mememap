﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.ViewModel.Core.Template
{
    public class CommonView : ICommonView
    {
        public virtual CommonPageTemplate PageTemplate { get; set; }
        public virtual void SettingTemplate()
        { }
        public virtual List<MultiTabTemplate> SettingForMultiTabTemplate()
        {
            return new List<MultiTabTemplate>();
        }
        public virtual List<ZoneTemplate> SettingForZone()
        {
            return new List<ZoneTemplate>();
        }
        public virtual List<FieldMappingPoint> SettingForMappingPoints()
        {
            return new List<FieldMappingPoint>();
        }
    }
}

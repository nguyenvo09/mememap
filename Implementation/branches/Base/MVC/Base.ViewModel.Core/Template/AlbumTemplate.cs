﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class AlbumTemplate
    {
        public string AlbumName { get; set; }
        public List<ImagePanelTemplate> listTemplate { get; set; }
        public AlbumTemplate()
        {
            listTemplate = new List<ImagePanelTemplate>();
        }
    }
    public class ImagePanelTemplate
    {
        public string PanelName { get; set; }
        public bool Active { get; set; }
        public string Type { get; set; }
        public string AlbumUrl { get; set; }
        public string PanelTitle { get; set; }
        public List<string> ListImageName { get; set; }
        public ImagePanelTemplate()
        {
            ListImageName = new List<string>();
        }
    }
}

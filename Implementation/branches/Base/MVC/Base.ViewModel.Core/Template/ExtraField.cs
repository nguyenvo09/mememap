﻿namespace Base.ViewModel.Core.Template
{
    public class ExtraField
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}

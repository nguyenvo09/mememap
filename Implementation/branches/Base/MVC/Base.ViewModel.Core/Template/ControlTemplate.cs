﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public static class DisplayControlTemplate
    {
        public const string BackButton = "BackButton";
        public const string SimpleGrid = "SimpleGrid";
        public const string CancelButton = "CancelButton";
        public const string CancelButton_Dialog = "CancelButton_Dialog";
        public const string AddButton = "AddButton";
        public const string HiddenInput = "HiddenInput";
        public const string Decimal = "Decimal";
        public const string Double = "Double";
        public const string Boolean = "Boolean";
        public const string String = "String";
        public const string Html = "Html";
        public const string MultiTab = "MultiTab";
        public const string DateTime = "DateTime";
        public const string Password = "Password";
        public const string HyperLink = "HyperLink";
        public const string TreeMenu = "TreeMenu";
        public const string Menu = "Menu";
        public const string MultiLineText = "MultiLineText";
        public const string ImageUrl = "ImageUrl";
        public const string ContentValueCollection = "ContentValueCollection";
        public const string ImageUpload = "ImageUpload";
        public const string Gallery = "Gallery";
        public const string ExtraField = "ExtraField";
        public const string TaskComment = "TaskComment";
        public const string SlideShow = "SlideShow";
        public const string BidConversation = "BidConversation";
        public const string OrderConversation = "OrderConversation";
        public const string ListView = "ListView";
    }
    public static class EditorControlTemplate
    {
        public const string SubmitButton = "SubmitButton";
        public const string CancelButton = "CancelButton";
        public const string AddButton = "AddButton";
        public const string CancelButton_Dialog = "CancelButton_Dialog";
        public const string ValidateSummary = "ValidateSummary";
        public const string MultiLineText = "MultiLineText";
        public const string HiddenInput = "HiddenInput";
        public const string MultiTab = "MultiTab";
        public const string String = "String";
        public const string NumbericTextBox = "NumbericTextBox";
        public const string DateTime = "DateTime";
        public const string ComboBox = "ComboBox";
        public const string SimpleGrid = "SimpleGrid";
        public const string Password = "Password";
        public const string Boolean = "Boolean";
        public const string HyperLink = "HyperLink";
        public const string HTMLEditor = "HTMLEditor";
        public const string ContentValueCollection = "ContentValueCollection";
        public const string ImageUpload = "ImageUpload";
        public const string ViewImageUpload = "ViewImageUpload";
        public const string Gallery = "Gallery";
        public const string MultiFileUpload = "MultiFileUpload";
        public const string Html = "Html";
        public const string ExtraField = "ExtraField";
        public const string Album = "Album";

    }
}

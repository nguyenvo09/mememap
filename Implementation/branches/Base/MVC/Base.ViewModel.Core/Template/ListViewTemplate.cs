﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class ListViewTemplate
    {
       public string ControlName { get; set; }
       public List<ListViewItemTemplate> ListItemTemplate { get; set; }
       public int ItemOnRow { get; set; }
       public int PageIndex { get; set; }
       public int PageSize { get; set; }
       public int TotalCount { get; set; }
       public string AjaxSelection { get; set; }
       public ListViewTemplate()
       {
           ListItemTemplate = new List<ListViewItemTemplate>();
       }
    }
    public class ListViewItemTemplate
    {
       public string ItemName { get; set; }
       public string BackGroundImage { get; set; }
       public string DisplayContent { get; set; }
       public string ToolTip { get; set; }
    }
}

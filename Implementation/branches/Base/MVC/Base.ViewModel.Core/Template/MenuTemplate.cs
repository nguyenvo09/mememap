﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public enum XPosition
    {
        Left, Right
    }
    public enum YPosition
    {
        Top, Bottom
    }
    public enum MenuType
    {
        Button,
        HorizonList,
        VerticalList,
        Tree
    }
    public class MenuTemplate
    {
        public List<MenuData> MenuData { get; set; }
        private List<MenuData> TempData { get; set; }
        public MenuType MenuType { get; set; }
        public MenuPosition MenuPosition { get; set; }
        public string MenuName { get; set; }
        public MenuTemplate()
        {
            MenuData = new List<MenuData>();
            TempData = new List<MenuData>();
            MenuPosition = new MenuPosition();
        }
        private List<MenuData> BuildTree(List<MenuData> lInMenu)
        {
            List<MenuData> lOutMenu = new List<MenuData>();
            lOutMenu = lInMenu.Where(m => m.ParentID == null).ToList();
            foreach (var m in lOutMenu)
            {
                BuildChildTree(m, lInMenu);
            }
            return lOutMenu;
        }
        private void BuildChildTree(MenuData parent, List<MenuData> lInMenu)
        {
            parent.ChildMenus = lInMenu.Where(m => m.ParentID == parent.MenuID).ToList();
            foreach (var m in parent.ChildMenus)
            {
                BuildChildTree(m, lInMenu);
            }
        }
        public void AddMenuItem(int row, int col, int menuID, string displayText, string tipText, int? parentID, bool isActive, string iconURL, bool useURL, string actionOrURL)
        {
            MenuData item = new MenuData();
            item.MenuID = menuID;
            item.DisplayText = displayText;
            item.TipText = tipText;
            item.Row = row;
            item.Col = col;
            item.UseURL = useURL;
            item.IsActive = isActive;
            item.IconUrl = iconURL;
            item.URL = actionOrURL;
            item.JavascriptAction = actionOrURL;
            if (parentID.HasValue)
            {
                MenuData parent = this.TempData.FirstOrDefault(m => m.MenuID == parentID.Value);
                if (parent != null)
                {
                    item.MenuLevel = parent.MenuLevel + 1;
                    item.ParentID = parent.MenuID;
                }
                else
                {
                    item.MenuLevel = 0;
                }
            }
            else
            {
                item.MenuLevel = 0;
            }
            this.TempData.Add(item);
            this.MenuData = this.BuildTree(this.TempData);
        }
    }
    public class MenuPosition
    {
        public XPosition xPosition { get; set; }
        public YPosition yPosition { get; set; }
    }
    public class MenuData
    {
        public int MenuID { get; set; }
        public int? ParentID { get; set; }
        public string DisplayText { get; set; }
        public string URL { get; set; }
        public string JavascriptAction { get; set; }
        public string TipText { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool UseURL { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public int MenuLevel { get; set; }
        public string IconUrl { get; set; }
        public List<MenuData> ChildMenus { get; set; }
        public MenuData()
        {
            MenuLevel = 0;
            Row = 0;
            Col = 0;
            ChildMenus = new List<MenuData>();
        }
    }
}
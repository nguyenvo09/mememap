﻿namespace Base.ViewModel.Core.Template
{
    public class FieldMappingPoint
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public int LabelSpan { get; set; }
        public int LabelOffset { get; set; }
        public string LabelText { get; set; }
        public string LabelStyle { get; set; }
        public int FieldSpan { get; set; }
        public int FieldOffset { get; set; }
        public string FieldStyle { get; set; }
        public int ValidationSpan { get; set; }
        public int ValidationOffset { get; set; }
        public string ValidationStyle { get; set; }
        public bool HasValidation { get; set; }
        public bool HasLabel { get; set; }
        public string FieldName { get; set; }
        public string TemplateName { get; set; }
        public string ContainerName { get; set; }
        public object HtmlAttributes { get; set; }
        public FieldMappingPoint()
        {

        }

        public FieldMappingPoint(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName)
        {
            Row = row;
            Column = column;
            HasLabel = hasLabel;
            HasValidation = hasValidation;
            LabelSpan = labelSpan;
            LabelOffset = labelOffset;
            LabelText = labelText;
            FieldSpan = fieldSpan;
            FieldOffset = fieldOffset;
            ValidationSpan = validationSpan;
            ValidationOffset = validationOffset;
            FieldName = fieldName;
            TemplateName = templateName;
            ContainerName = containerName;
        }
        public FieldMappingPoint(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, object htmlAttributes)
        {
            Row = row;
            Column = column;
            HasLabel = hasLabel;
            HasValidation = hasValidation;
            LabelSpan = labelSpan;
            LabelOffset = labelOffset;
            LabelText = labelText;
            FieldSpan = fieldSpan;
            FieldOffset = fieldOffset;
            ValidationSpan = validationSpan;
            ValidationOffset = validationOffset;
            FieldName = fieldName;
            TemplateName = templateName;
            ContainerName = containerName;
            HtmlAttributes = htmlAttributes;
        }

        public FieldMappingPoint(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, string labelStyle, int labelOffset, int fieldSpan, string fieldStyle, int fieldOffset, int validationSpan, string validationStyle, int validationOffset, string fieldName, string templateName, string containerName, object htmlAttributes)
        {
            //FieldMappingPoint(row, column, hasLabel, hasValidation, labelText, labelSpan, labelOffset, fieldSpan, fieldOffset, validationSpan, validationOffset, fieldName, templateName, containerName, htmlAttributes);
            Row = row;
            Column = column;
            HasLabel = hasLabel;
            HasValidation = hasValidation;
            LabelSpan = labelSpan;
            LabelOffset = labelOffset;
            LabelText = labelText;
            FieldSpan = fieldSpan;
            FieldOffset = fieldOffset;
            ValidationSpan = validationSpan;
            ValidationOffset = validationOffset;
            FieldName = fieldName;
            TemplateName = templateName;
            ContainerName = containerName;
            HtmlAttributes = htmlAttributes;
            LabelStyle = labelStyle;
            FieldStyle = fieldStyle;
            ValidationStyle = validationStyle;
        }
    }
}

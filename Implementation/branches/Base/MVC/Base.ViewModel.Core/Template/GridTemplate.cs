﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Kendo.Mvc.UI;
namespace Base.ViewModel.Core.Template
{
    public class GridTemplate
    {
        public string GridName { get; set; }
        public string ControllerName { get; set; }
        public string KeyName { get; set; }
        public string RowTemplate { get; set; }
        private bool enableDelete;
        private string deleteUrl;
        private bool enableEdit;
        private bool enableCheck;
        private string editUrl;
        private bool enableDetail;
        private string detailUrl;
        public string SelectAjaxAction { get; set; }
        public object RoutedValues { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public bool HasSourceBinding { get; set; }
        public bool HasClientBinding { get; set; }
        public bool HasScrollBinding { get; set; }
        public string ClientBindingMethod { get; set; }
        public GridDetailType GridDetailType { get; set; }
        public List<GridColumnSettings> ColumnConfigs
        {
            get;
            set;
        }
        public DataTable datatable { get; set; }
        public DataTable SourceTable { get; set; }
        public GridTemplate()
        {
            RowTemplate = string.Empty;
            ColumnConfigs = new List<GridColumnSettings>();
            datatable = new DataTable();
            RoutedValues = null;
            GridDetailType = GridDetailType.Normal;
            
        }
        public void SetRowTemplate(string template)
        {
            this.RowTemplate = template;
        }
        public GridTemplate(string gridName, string controllerName, string selectAjaxAction, List<GridColumnSettings> columnConfigs, int pageIndex, int pageSize, int totalCount)
        {
            GridName = gridName;
            ControllerName = controllerName;
            SelectAjaxAction = selectAjaxAction;
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            ColumnConfigs = columnConfigs;
            SetTableTemplate();
        }
        private void SetTableTemplate()
        {
            #region Remove col comand before build table template
            GridColumnSettings colCommand = new GridColumnSettings();
            colCommand.Member = "Grid_Command";
            GridColumnSettings col1 = ColumnConfigs.FirstOrDefault(c => c.Member == colCommand.Member);
            if (col1 != null) ColumnConfigs.Remove(col1);
            #endregion
            #region Setting for table template
            datatable = new DataTable();
            foreach (GridColumnSettings col in ColumnConfigs.Where(c => String.IsNullOrWhiteSpace(c.ClientTemplate)))
            {
                datatable.Columns.Add(col.Member, col.MemberType);
            }

            #endregion
            #region Add Template Command
            string sTemp = string.Empty;
            if (this.enableDetail)
            {
                if (GridDetailType == GridDetailType.Normal)
                {
                    sTemp += string.Format(" <a id='btn_grid_detail{1}' href=\"{0}/#= {1} #\"  title=\"View\"><i class=\"icon-zoom-in \"></i></a>", this.detailUrl, this.KeyName);
                }
                else if (GridDetailType == GridDetailType.Dialog)
                {
                    sTemp += string.Format(" <a id='btn_grid_detail{1}' href=\"{0}/#= {1} #\"  title=\"View\" onclick=\"return ViewDialog(this)\"><i class=\"icon-zoom-in \"></i></a>", this.detailUrl, this.KeyName);
                }
            }
            if (this.enableEdit)
            {
                if (GridDetailType == GridDetailType.Normal)
                {
                    sTemp += string.Format(" <a id='btn_grid_edit{1}' href=\"{0}/#= {1} #\"  ><i class=\"glyphicon glyphicon-hand-right \"></i></a>", this.editUrl, this.KeyName);
                }
                else if (GridDetailType == GridDetailType.Dialog)
                {
                    sTemp += string.Format(" <a id='btn_grid_edit{1}' href=\"{0}/#= {1} #\"  onclick=\"return ViewDialog(this)\"><i class=\"glyphicon glyphicon-hand-right \"></i></a>", this.editUrl, this.KeyName);
                }
            }
            if (this.enableDelete)
            {
                if (GridDetailType == GridDetailType.Normal)
                {
                    sTemp += string.Format(" <a id='btn_grid_delete{1}' href=\"{0}/#= {1} #\" title=\"Delete\"><i class=\"icon-trash\"></i></a>", this.deleteUrl, this.KeyName);
                }
                else if (GridDetailType == GridDetailType.Dialog)
                {
                    sTemp += string.Format(" <a id='btn_grid_detail{1}' href=\"{0}/#= {1} #\"  title=\"Delete\" onclick=\"return ViewDialog(this)\"><i class=\"icon-trash\"></i></a>", this.deleteUrl, this.KeyName);
                }
            }

            if (this.enableCheck)
            {
                GridColumnSettings colCheck = new GridColumnSettings();
                colCheck.Member = "Grid_CheckBox";
                GridColumnSettings col2 = ColumnConfigs.FirstOrDefault(c => c.Member == colCheck.Member);
                if (col1 != null) ColumnConfigs.Remove(col2);
                string colTemp = string.Format("<input id='grid_chk<#= {0} #>' type='checkbox' name='checkedRecords' value'<#= {0} #>' />", this.KeyName);
                colCheck.Member = "Grid_CheckBox";
                colCheck.Filterable = false;
                colCheck.Title = "<input type='checkbox' title='check all records' id='checkAllRecords'/>";
                colCheck.Groupable = false;
                colCheck.Sortable = false;
                colCheck.Width = "5%";
                colCheck.ClientTemplate = colTemp;
                colCheck.MemberType = typeof(String);
                colCheck.HtmlAttributes.Add("style", "text-align:center");
                colCheck.HeaderHtmlAttributes.Add("style", "text-align:right");
                ColumnConfigs.Insert(0, colCheck);
            }

            if (this.enableDelete || this.enableDetail || this.enableEdit)
            {
                colCommand.Filterable = false;
                colCommand.Title = "";
                colCommand.Groupable = false;
                colCommand.Sortable = false;
                colCommand.Width = "20px";
                colCommand.ClientTemplate = sTemp;
                colCommand.MemberType = typeof(String);
                ColumnConfigs.Add(colCommand);
            }
            #endregion
        }
        public void SetColumn(GridColumnSettings column)
        {
            if (this.ColumnConfigs == null) this.ColumnConfigs = new List<GridColumnSettings>();
            ColumnConfigs.Add(column);
            SetTableTemplate();
        }
        public void EnableDialogView()
        {
            this.GridDetailType = GridDetailType.Dialog;
            SetTableTemplate();
        }
        public void EnableCheckBox(bool enable)
        {
            this.enableCheck = enable;
            SetTableTemplate();
        }
        public void EnableSourceBinding(DataTable datasource)
        {
            HasSourceBinding = true;
            SourceTable = datasource;
        }
        public void EnableDelete(bool enable, string deleteUrl)
        {
            this.enableDelete = enable;
            this.deleteUrl = deleteUrl;
            SetTableTemplate();
        }
        public void EnableEdit(bool enable, string editUrl)
        {
            this.enableEdit = enable;
            this.editUrl = editUrl;
            SetTableTemplate();
        }
        public void EnableDetail(bool enable, string detailUrl)
        {
            this.enableDetail = enable;
            this.detailUrl = detailUrl;
            SetTableTemplate();
        }
        public void SetHeight(string height)
        {
            Height = height;
        }
        public void EnableClientBinding(bool enable, string bindingMethod)
        {
            this.HasClientBinding = enable;
            this.ClientBindingMethod = bindingMethod;
        }
        public void EnableScrollBinding(bool enable)
        {
            this.HasScrollBinding = enable;
        }
    }
    public enum GridDetailType
    {
        Normal, Dialog
    }
}

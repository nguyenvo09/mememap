﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kendo.Mvc.UI;
using Base.ViewModel.Core.Template;

namespace Base.ViewModel.Core.FrameView
{
    public class IndexTaskFrameModel : CommonView
    {
        #region  Properties
        public string ContainerZone = "ContainerZone";
        public string Child1Zone = "Child1Zone";
        public string Child2Zone = "Child2Zone";
        public string Child3Zone = "Child3Zone";
        public string AddItemURL { set; get; }
        public string AddImage1 { get; set; }
        public string AddImage2 { get; set; }
        public string AddImage3 { get; set; }
        public string AddImage4 { get; set; }
        public string AddImage5 { get; set; }
        public string AddImage6 { get; set; }
        public string FrameTitle { set; get; }
        public GridTemplate Grid { set; get; }
        #endregion
        #region contructor
        public IndexTaskFrameModel()
        {
            Grid = new GridTemplate();
            AddItemURL = "Create";
            SettingTemplate();
        }
        #endregion
        #region overide method
        public override List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listZone = new List<ZoneTemplate>();
            #region Setting for container
            ZoneTemplate container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
            container.Childs = new List<ZoneTemplate>();
            container.Childs.Add(new ZoneTemplate(Child2Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child1Zone, "", 4, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child3Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            #endregion
            listZone.Add(container);
            return listZone;
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "", 0, 0, 0, 0, 0, 0, "AddImage2", DisplayControlTemplate.ImageUrl, Child2Zone));
            lstMap.Add(new FieldMappingPoint(1, 0, false, false, "", 0, 0, 0, 0, 0, 0, "Grid", DisplayControlTemplate.SimpleGrid, Child2Zone));
            lstMap.Add(new FieldMappingPoint(2, 0, false, false, "New Item", 0, 0, 0, 0, 0, 0, "AddItemURL", DisplayControlTemplate.AddButton, Child1Zone));
            lstMap.Add(new FieldMappingPoint(3, 0, false, false, "", 0, 0, 0, 0, 0, 0, "AddImage1", DisplayControlTemplate.ImageUrl, Child1Zone));
            lstMap.Add(new FieldMappingPoint(3, 0, false, false, "", 0, 0, 0, 0, 0, 0, "AddImage3", DisplayControlTemplate.ImageUrl, Child3Zone));
            
            return lstMap;
        }

        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            #endregion
        }
        #endregion
        #region Setting Methods

        public IndexTaskFrameModel AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        #region Grid Action
        public IndexTaskFrameModel GridColumn(string title, string memberName, Type memberType, string width)
        {
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = true;
            col.Groupable = true;
            col.Sortable = true;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            Grid.SetColumn(col);
            return this;
        }
        public IndexTaskFrameModel GridColumn(GridColumnSettings col)
        {
            Grid.SetColumn(col);
            return this;
        }
        public IndexTaskFrameModel GridColumn(Action<IndexTaskFrameModel> col)
        {
            return this;
        }
        public IndexTaskFrameModel GridName(string gridName)
        {
            Grid.GridName = gridName;
            return this;
        }
        public IndexTaskFrameModel GridKeyName(string keyName)
        {
            Grid.KeyName = keyName;
            return this;
        }
        public IndexTaskFrameModel GridController(string controller)
        {
            Grid.ControllerName = controller;
            return this;
        }
        public IndexTaskFrameModel GridAjaxSelectAction(string selectAction)
        {
            Grid.SelectAjaxAction = selectAction;
            return this;
        }
        public IndexTaskFrameModel GridPageIndex(int pageIndex)
        {
            Grid.PageIndex = pageIndex;
            return this;
        }
        public IndexTaskFrameModel GridPageSize(int pageSize)
        {
            Grid.PageSize = pageSize == 0 ? 20 : pageSize;
            return this;
        }
        public IndexTaskFrameModel GridTotalCount(int totalCount)
        {
            Grid.TotalCount = totalCount;
            return this;
        }
        public IndexTaskFrameModel GridEnableDelete(string deleteUrl)
        {
            Grid.EnableDelete(true, deleteUrl);
            return this;
        }
        public IndexTaskFrameModel GridEnableEdit(string editUrl)
        {
            Grid.EnableEdit(true, editUrl);
            return this;
        }
        public IndexTaskFrameModel GridEnableDetail(string detailUrl)
        {
            Grid.EnableDetail(true, detailUrl);
            return this;
        }
        public IndexTaskFrameModel GridHieght(string height)
        {
            Grid.SetHeight(height);
            return this;
        }
        #endregion
        #region Other Action
        public IndexTaskFrameModel CreateLink(string createUri)
        {
            AddItemURL = createUri;
            return this;
        }
        public IndexTaskFrameModel CreateImage(string imageID, string createUri)
        {
            if (imageID == "image1")
                AddImage1 = createUri;
            if (imageID == "image2")
                AddImage2 = createUri;
            if (imageID == "image3")
                AddImage3 = createUri;
            if (imageID == "image4")
                AddImage4 = createUri;
            if (imageID == "image5")
                AddImage5 = createUri;
            if (imageID == "image6")
                AddImage6 = createUri;
            return this;
        }
        public IndexTaskFrameModel CreateLink(string createText, string createUri)
        {
            AddItemURL = createUri;
            this.PageTemplate.MappingPoints.Where(m => m.FieldName == "AddItemURL").First().LabelText = createText;
            return this;
        }

        public IndexTaskFrameModel AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        #endregion
        #endregion
        #region script action
        public IndexTaskFrameModel AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion
    }
}

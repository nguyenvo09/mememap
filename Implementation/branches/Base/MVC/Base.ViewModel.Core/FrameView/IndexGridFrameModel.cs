﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kendo.Mvc.UI;
using Base.ViewModel.Core.Template;
using System.Data;

namespace Base.ViewModel.Core.FrameView
{
    public class IndexGridFrameModel : CommonView
    {
        #region  Properties
        public string ContainerZone = "ContainerZone";
        public string Child1Zone = "Child1Zone";
        public string Child2Zone = "Child2Zone";
        public string Child3Zone = "Child3Zone";
        public string AddItemURL { set; get; }        
        public string FrameTitle { set; get; }
        public string FrameHeader { set; get; } 
        public GridTemplate Grid { set; get; }
        public MenuTemplate LeftTopMenu { set; get; }
        public MenuTemplate RightTopMenu { set; get; }
        public MenuTemplate LeftBottomMenu { set; get; }
        public MenuTemplate RightBottomMenu { set; get; }
        public string Title { set; get; }
        #endregion
        #region contructor
        public IndexGridFrameModel()
        {
            Grid = new GridTemplate();
            AddItemURL = "Create";
            SettingTemplate();
        }

        public IndexGridFrameModel(string title)
        {
            Title = title;
            Grid = new GridTemplate();
            AddItemURL = "Create";
            SettingTemplate();
        }   
        #endregion
        #region overide method
        public override List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listZone = new List<ZoneTemplate>();
            #region Setting for container
            ZoneTemplate container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
            container.Childs = new List<ZoneTemplate>();
            container.Childs.Add(new ZoneTemplate(Child1Zone, "",12, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child2Zone, "",12, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child3Zone, "", 12, 0, 0, 0, container.IsRowFluid));
            #endregion
            listZone.Add(container);
            return listZone;
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "", 0, 0, 12, 0, 0, 0, "FrameHeader", DisplayControlTemplate.Html, Child1Zone));
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid", DisplayControlTemplate.SimpleGrid, Child3Zone));
            //lstMap.Add(new FieldMappingPoint(0, 0, false, false, "New Item",0, 0, 12, 0, 0, 0, "AddItemURL", DisplayControlTemplate.AddButton, Child2Zone));
            return lstMap;
        }
        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            #endregion

        }
        #endregion
        #region Setting Methods
       

        public IndexGridFrameModel AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        #region Grid Action
        public IndexGridFrameModel GridAjaxSelectAction(string selectAction, string controllerName, object routedValues)
        {
            Grid.SelectAjaxAction = selectAction;
            Grid.RoutedValues = routedValues;
            Grid.ControllerName = controllerName;
            return this;
        }
        public IndexGridFrameModel GridColumn(string title, string memberName, Type memberType, string width, string coltemplate)
        {

            //find grid in list<grid>
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = true;
            col.Groupable = true;
            col.Sortable = true;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            col.ClientTemplate = coltemplate;
            Grid.SetColumn(col);
            return this;
        }
        public IndexGridFrameModel GridColumn(string title, string memberName, Type memberType, string width, string coltemplate, bool enableSort, bool enableGroup, bool enableFilter)
        {

            //find grid in list<grid>
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = enableFilter;
            col.Groupable = enableGroup;
            col.Sortable = enableSort;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            col.ClientTemplate = coltemplate;
            Grid.SetColumn(col);
            return this;
        }
        public IndexGridFrameModel GridColumn(string title, string memberName, Type memberType, string width)
        {
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = true;
            col.Groupable = true;
            col.Sortable = true;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            Grid.SetColumn(col);
            return this;
        }

        public IndexGridFrameModel GridColumn(string title, string memberName, Type memberType, string width, bool enableSort, bool enableGroup, bool enableFilter)
        {
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = enableFilter;
            col.Groupable = enableGroup;
            col.Sortable = enableSort;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            Grid.SetColumn(col);
            return this;
        }

        public IndexGridFrameModel GridColumn(GridColumnSettings col)
        {
            Grid.SetColumn(col);
            return this;
        }
        public IndexGridFrameModel GridName(string gridName)
        {
            Grid.GridName = gridName;
            return this;
        }
        public IndexGridFrameModel GridKeyName(string keyName)
        {
            Grid.KeyName = keyName;
            return this;
        }
        public IndexGridFrameModel GridController(string controller)
        {
            Grid.ControllerName = controller;
            return this;
        }
        public IndexGridFrameModel GridAjaxSelectAction(string selectAction)
        {
            Grid.SelectAjaxAction = selectAction;
            return this;
        }
        public IndexGridFrameModel GridPageIndex(int pageIndex)
        {
            Grid.PageIndex = pageIndex;
            return this;
        }
        public IndexGridFrameModel GridPageSize(int pageSize)
        {
            Grid.PageSize = pageSize == 0 ? 20 : pageSize;
            return this;
        }
        public IndexGridFrameModel GridTotalCount(int totalCount)
        {
            Grid.TotalCount = totalCount;
            return this;
        }
        public IndexGridFrameModel GridEnableDelete(string deleteUrl)
        {
            Grid.EnableDelete(true, deleteUrl);
            return this;
        }

        public IndexGridFrameModel GridEnableCheckBox(bool enable)
        {
            Grid.EnableCheckBox(enable);
            return this;
        }

        public IndexGridFrameModel GridEnableEdit(string editUrl)
        {
            Grid.EnableEdit(true, editUrl);
            return this;
        }
        public IndexGridFrameModel GridEnableDetail(string detailUrl)
        {
            Grid.EnableDetail(true, detailUrl);
            return this;
        }
        public IndexGridFrameModel GridHeight(string height)
        {
            Grid.SetHeight(height);
            return this;
        }
        public IndexGridFrameModel GridEnableSourceBinding(DataTable datasource)
        {
            Grid.EnableSourceBinding(datasource);
            return this;
        }
        #endregion
        #region Other Action
        public IndexGridFrameModel CreateLink(string createUri)
        {
            AddItemURL = createUri;
            return this;
        }
        public IndexGridFrameModel CreateLink(string createText, string createUri)
        {
            AddItemURL = createUri;
            this.PageTemplate.MappingPoints.Where(m => m.FieldName == "AddItemURL").First().LabelText = createText;
            return this;
        }
        public IndexGridFrameModel CreateLink(int row, int col,int fieldSpan,string container,string createText, string createUri)
        {
            FieldMappingPoint fMap =  this.PageTemplate.MappingPoints.Where(m => m.FieldName == "AddItemURL").First();
            AddItemURL = createUri;
            fMap.LabelText = createText;
            fMap.Column = col;
            fMap.Row = row;
            fMap.FieldSpan = fieldSpan;
            fMap.ContainerName = container;
            return this;
        }
        public IndexGridFrameModel AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        public IndexGridFrameModel AddFrameHeader(string tilte)
        {
            FrameHeader = tilte;
            return this;
        }
        public IndexGridFrameModel SetGridHeight(string height)
        {
            Grid.SetHeight(height);
            return this;
        }
        public IndexGridFrameModel EnableZone(bool enabled, string zoneName)
        {
            ZoneTemplate container = PageTemplate.ZoneTree.Find(z => z.Name == ContainerZone);
            if (enabled)
            {
                ZoneTemplate zone = container.Childs.FirstOrDefault(z => z.Name == zoneName);
                if (zone == null)
                {
                    container.Childs.Insert(0,new ZoneTemplate(zoneName, "", 12, 0, 0, 0, container.IsRowFluid));
                }
            }
            else 
            {
                ZoneTemplate zone = container.Childs.FirstOrDefault(z => z.Name == zoneName);
                if(zone!=null)
                {
                    container.Childs.Remove(zone);
                }
            }
            return this;
        }
        #endregion
        #region MenuAction
        public IndexGridFrameModel EnableMenu(bool enable,XPosition x, YPosition y,int row,int col,int span,int offset,string container,MenuType menuType)
        {
            MenuTemplate menu = InitMenu(x,y);
            menu.MenuType = menuType;
            FieldMappingPoint fMap = PageTemplate.MappingPoints.FirstOrDefault(c => c.FieldName == menu.MenuName);
            if (enable)
            {
                if (fMap == null)
                {
                    fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, menu.MenuName, DisplayControlTemplate.Menu, container);
                    PageTemplate.MappingPoints.Add(fMap);
                }
                fMap.Row = row;
                fMap.Column = col;
                fMap.FieldSpan = span;
                fMap.FieldOffset = offset;
                fMap.ContainerName = container;
            }
            else
            {
                if (fMap != null) PageTemplate.MappingPoints.Remove(fMap);
            }
            return this;
        }
        public IndexGridFrameModel AddMenuItem(XPosition x,YPosition y,MenuType menuType,List<MenuData> menuData)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.MenuType = menuType;
            menu.MenuData = menuData;
            return this;
        }
        public IndexGridFrameModel AddMenuItem(XPosition x, YPosition y,int row,int col, int menuID,string displayText,string tipText,int? parentID,bool isActive,string iconURL,string url)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.AddMenuItem(row, col, menuID, displayText, tipText, parentID, isActive, iconURL,true,url);
            return this;
        }
        public IndexGridFrameModel AddMenuItem(XPosition x, YPosition y, int row, int col, int menuID, string displayText, string tipText, int? parentID, bool isActive, string iconURL,bool useURL,string actionOrURL)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.AddMenuItem(row, col, menuID, displayText, tipText, parentID, isActive, iconURL,useURL, actionOrURL);
            return this;
        }
        private MenuTemplate InitMenu(XPosition x, YPosition y)
        {
            switch (x)
            {
                case XPosition.Left:
                    switch (y)
                    { 
                        case YPosition.Top:
                            if (LeftTopMenu == null)
                            {
                                LeftTopMenu = new MenuTemplate();
                                LeftTopMenu.MenuPosition.xPosition = x;
                                LeftTopMenu.MenuPosition.yPosition = y;
                                LeftTopMenu.MenuName ="LeftTopMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, LeftTopMenu.MenuName, DisplayControlTemplate.Menu, Child2Zone);
                                this.PageTemplate.MappingPoints.Insert(0,fMap);
                            }
                            return LeftTopMenu;
                        case YPosition.Bottom:
                            if (LeftBottomMenu == null)
                            {
                                LeftBottomMenu = new MenuTemplate();
                                LeftBottomMenu.MenuPosition.xPosition = x;
                                LeftBottomMenu.MenuPosition.yPosition = y;
                                LeftBottomMenu.MenuName = "LeftBottomMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, LeftBottomMenu.MenuName, DisplayControlTemplate.Menu, Child2Zone);
                                this.PageTemplate.MappingPoints.Add(fMap);
                            }
                            return LeftBottomMenu;
                    }
                    break;
                case XPosition.Right:
                    switch (y)
                    {
                        case YPosition.Top:
                            if (RightTopMenu == null)
                            {
                                RightTopMenu = new MenuTemplate();
                                RightTopMenu.MenuPosition.xPosition = x;
                                RightTopMenu.MenuPosition.yPosition = y;
                                RightTopMenu.MenuName = "RightTopMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, RightTopMenu.MenuName, DisplayControlTemplate.Menu, Child2Zone);
                                this.PageTemplate.MappingPoints.Insert(0,fMap);
                            }
                            return RightTopMenu;
                        case YPosition.Bottom:
                            if (RightBottomMenu == null)
                            {
                                RightBottomMenu = new MenuTemplate();
                                RightBottomMenu.MenuPosition.xPosition = x;
                                RightBottomMenu.MenuPosition.yPosition = y;
                                RightBottomMenu.MenuName = "RightBottomMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, RightBottomMenu.MenuName, DisplayControlTemplate.Menu, Child2Zone);
                                this.PageTemplate.MappingPoints.Insert(0,fMap);
                            }
                            return RightBottomMenu;
                    }
                    break;
            }
            return null;
        }
        #endregion
        #endregion
        #region script action
        public IndexGridFrameModel AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion
        #region style action
        public IndexGridFrameModel AddStyleItem(string name, string source, StyleType type)
        {
            StyleRegister item = new StyleRegister();
            item.Name = name;
            item.Source = source;
            item.StyleType = type;
            this.PageTemplate.StyleRegisters.Add(item);
            return this;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.ViewModel.Core.Template;

namespace Base.ViewModel.Core.FrameView
{
    /// <summary>
    /// frame with left zone - right zone - bottom zone
    /// </summary>
    public class DisplayFrame01<T> : CommonView
    {
        #region Properties

        private FrameType FrameType { get; set; }
        public string LeftZone = "LeftZone";
        public string RightZone = "RightZone";
        public string BottomZone = "BottomZone";
        public string FrameTitle { set; get; }
        public T obj { get; set; }
        #endregion

        #region Contructor
        public DisplayFrame01()
        {
            SettingTemplate();
        }
        
        #endregion

        #region Overide methods
        public List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listzone = new List<ZoneTemplate>();
            listzone.Add(new ZoneTemplate(LeftZone, "", 8, 0, 0, 0, false));
            listzone.Add(new ZoneTemplate(RightZone, "", 4, 0, 0, 0, false));
            listzone.Add(new ZoneTemplate(BottomZone, "", 12, 0, 0, 0, false));
            return listzone;
        }

        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> listMap = new List<FieldMappingPoint>();
            listMap.Add(new FieldMappingPoint(0, 0, false, false, "Title", 0, 0, 1, 0, 0, 0, "Frame Title", EditorControlTemplate.HiddenInput, BottomZone));
            return listMap;
        }

        public override void SettingTemplate()
        {
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            FrameType = FrameType.NormalView;
            FrameTitle = string.Empty;
        }
        #endregion

        #region Setting Methods

        public DisplayFrame01<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;            
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;            
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;            
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public DisplayFrame01<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, string labelStyle, string fieldStyle, string validationStyle)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelStyle = labelStyle;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.FieldStyle = fieldStyle;
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;
            mPoint.ValidationStyle = validationStyle;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;            
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public DisplayFrame01<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, object htmlAttributes, string labelStyle, string fieldStyle, string validationStyle)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelStyle = labelStyle;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.FieldStyle = fieldStyle;
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;
            mPoint.ValidationStyle = validationStyle;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            mPoint.HtmlAttributes = htmlAttributes;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }


        public DisplayFrame01<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;            
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;            
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }
        
        public DisplayFrame01<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value, string labelStyle, string fieldStyle)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            
            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.LabelStyle = labelStyle;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.FieldStyle = fieldStyle;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }
        public DisplayFrame01<T> AddFrameType(FrameType frameType)
        {
            FrameType = frameType;
            if (FrameType == FrameType.Dialog)
            {
                PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").TemplateName = EditorControlTemplate.CancelButton_Dialog;
            }
            return this;
        }
        public DisplayFrame01<T> AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        public DisplayFrame01<T> AddSource(T obj)
        {
            this.obj = obj;
            return this;
        }

        public DisplayFrame01<T> EnableZone(bool enabled, string zoneName)
        {
            ZoneTemplate zone = PageTemplate.ZoneTree.Find(z => z.Name == zoneName);
            if (enabled)
            {
                if (zone == null)
                {
                    if (zoneName == this.LeftZone)
                    {
                        PageTemplate.ZoneTree.Insert(0, new ZoneTemplate(zoneName, "", 8, 0, 0, 0, true));
                    }
                    else if (zoneName == this.RightZone)
                    {
                        PageTemplate.ZoneTree.Insert(0, new ZoneTemplate(zoneName, "", 4, 0, 0, 1, true));
                    }
                    else if (zoneName == this.BottomZone)
                    {
                        PageTemplate.ZoneTree.Insert(0, new ZoneTemplate(zoneName, "", 12, 0, 1, 0, true));
                    }
                }
            }
            else
            {
                if (zone != null)
                {
                    PageTemplate.ZoneTree.Remove(zone);
                }
            }
            return this;
        }

        //public DisplayFrame01<T> AddButton(
        //{
            
        //}

        #endregion

        #region MenuAction
        #endregion

        #region script action
        public DisplayFrame01<T> AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion

        #region Style action
        public DisplayFrame01<T> AddStyleItem(string name, string source, StyleType type)
        {
            StyleRegister item = new StyleRegister();
            item.Name = name;
            item.Source = source;
            item.StyleType = type;
            this.PageTemplate.StyleRegisters.Add(item);
            return this;
        }
        #endregion
    }    
}

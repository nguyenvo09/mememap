﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.ViewModel.Core.Template;
using Kendo.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Base.ViewModel.Core.FrameView
{
    public class EditFrameModel<T>:CommonView
    {
        #region  Properties
        public string SubmitButton { set; get; }
        public string CancelButton { set; get; }
        public T obj{get;set;}
        private FrameType FrameType{set;get;}
        public string FrameAction { set; get; }
        public string FrameController { set; get; }
        public string FrameTitle { set; get; }
        public string FrameHeader { set; get; }
        
        public string NextAction { set; get; }
        public string PrevAction { set; get; }
        public string ContainerZone = "ContainerZone";
        public string TopZone = "TopZone";
        public string Child1Zone = "Child1Zone";
        public string Child2Zone = "Child2Zone";

        #endregion
        #region Contructor
        public EditFrameModel()
        {
            SettingTemplate();
        }
        #endregion
        #region Overide methods
        public EditFrameModel<T> DefaultZone()
        {
            PageTemplate.ZoneTree = new List<ZoneTemplate>();
            #region Setting for container
            ZoneTemplate container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
            container.Childs = new List<ZoneTemplate>();
            container.Childs.Add(new ZoneTemplate(TopZone, "", 12, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child1Zone, "", 12, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child2Zone, "", 12, 0, 0, 0, container.IsRowFluid));
            #endregion
            PageTemplate.ZoneTree.Add(container);
            return this;
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "", 0, 0, 12, 0, 0, 0, "FrameHeader", DisplayControlTemplate.Html, TopZone));
            lstMap.Add(new FieldMappingPoint(1, 0, false, false, "", 0, 0, 12, 0, 0, 0, "ValidateSummary", EditorControlTemplate.ValidateSummary, Child1Zone));
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "Update", 0, 0, 1, 0, 0, 0, "SubmitButton", EditorControlTemplate.SubmitButton, Child2Zone));
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "Cancel", 0, 0, 1, 0, 0, 0, "CancelButton", EditorControlTemplate.CancelButton, Child2Zone));
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "title", 0, 0, 1, 0, 0, 0, "FrameTitle", EditorControlTemplate.HiddenInput, Child2Zone));
            return lstMap;
        }
        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.MultiTabs = new List<MultiTabTemplate>();
            PageTemplate.ZoneTree = new List<ZoneTemplate>();
            PageTemplate.MappingPoints = new List<FieldMappingPoint>();
            FrameTitle = string.Empty;
            FrameAction = string.Empty;
            FrameController = string.Empty;
            DefaultMapFields();
            DefaultZone();
            #endregion
        }
        private void DefaultMapFields()
        {
            AddMappingField(0, 0, false, false, "", 0, 0, 12, 0, 0, 0, "FrameHeader", DisplayControlTemplate.Html, TopZone);
            AddMappingField(0, 0, false, false, "Update", 0, 0, 1, 0, 0, 0, "SubmitButton", EditorControlTemplate.SubmitButton, Child2Zone);
            AddMappingField(0, 0, false, false, "Cancel", 0, 0, 1, 0, 0, 0, "CancelButton", EditorControlTemplate.CancelButton, Child2Zone);
            AddMappingField(0, 0, false, false, "Title", 0, 0, 1, 0, 0, 0, "FrameTitle", EditorControlTemplate.HiddenInput, Child2Zone);
            AddMappingField(0, 0, false, false, "NextAction", 0, 0, 1, 0, 0, 0, "NextAction", EditorControlTemplate.HiddenInput, Child2Zone);
            AddMappingField(0, 0, false, false, "PrevAction", 0, 0, 1, 0, 0, 0, "PrevAction", EditorControlTemplate.HiddenInput, Child2Zone);
        }
        #endregion
        #region Setting Methods
        #region Tabs Medthods
        public EditFrameModel<T> EnableTabControl(int row, int col, int fieldSpan, int fieldOffset, string tabControlName, MultiTabType tapType, string containerName)
        {
            MultiTabTemplate tabControl = new MultiTabTemplate(tabControlName, tabControlName, tapType, new List<MultiTabPanelTemplate>());
            this.PageTemplate.MultiTabs.Add(tabControl);
            AddMappingField(row, col, false, "", 0, 0, fieldSpan, fieldOffset, tabControlName, DisplayControlTemplate.MultiTab, containerName);
            return this;
        }
        public EditFrameModel<T> AddTabPanel(int row, int col, string panelName, string displayText, string tabControlName, bool isActive)
        {
            MultiTabTemplate tabControl = this.PageTemplate.MultiTabs.Find(t => t.Name == tabControlName);
            MultiTabPanelTemplate tabPanel = new MultiTabPanelTemplate(panelName, displayText, isActive);
            tabControl.TabPanels.Add(tabPanel);
            return this;
        }
        #endregion
        public EditFrameModel<T> AddMappingField(int row, int column, bool hasLabel, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, string fieldName, string templateName, string containerName)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }


        public EditFrameModel<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public EditFrameModel<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public EditFrameModel<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, string labelStyle, string fieldStyle)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            mPoint.LabelStyle = labelStyle;
            mPoint.FieldStyle = fieldStyle;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public EditFrameModel<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, object htmlAttributes)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = hasValidation;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = validationSpan;
            mPoint.ValidationOffset = validationOffset;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            mPoint.HtmlAttributes = htmlAttributes;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }
        public EditFrameModel<T> AddComboBox(string fieldName, SelectList source)
        {
            PageTemplate.ComboBoxTemplates.Add(new ComboBoxTemplate(fieldName, source));
            return this;
        }
        public EditFrameModel<T> AddSource(T obj)
        {
            this.obj = obj;
            return this;
        }
        public EditFrameModel<T> AddCancelAction(string cancelUri)
        {
            CancelButton = cancelUri;
            return this;
        }
        public EditFrameModel<T> AddCancelAction(string cancelUri,string cancelText)
        {
            CancelButton = cancelUri;
            PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").LabelText = cancelText;
            return this;
        }
        public EditFrameModel<T> AddUpdateText(string updateText)
        {
            PageTemplate.MappingPoints.Find(m => m.FieldName == "SubmitButton").LabelText = updateText;
            return this;
        }
        public EditFrameModel<T> AddFrameType(FrameType frameType)
        {
           FrameType = frameType;
           if(FrameType == FrameType.Dialog)
           {
               PageTemplate.MappingPoints.Find(m=>m.FieldName=="SubmitButton").FieldSpan = 2;
               PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").TemplateName = EditorControlTemplate.CancelButton_Dialog;
           }
           return this;
        }
        public EditFrameModel<T> AddFrameAction(string action, string controller)
        {
            FrameAction = action;
            FrameController = controller;
            return this;
        }
        public EditFrameModel<T> AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }

        public EditFrameModel<T> AddFrameHeader(string tilte)
        {
            FrameHeader = tilte;
            return this;
        }
        public EditFrameModel<T> AddNextAction(string nextUrl)
        {
            NextAction = nextUrl;
            return this;
        }
        public EditFrameModel<T> AddPrevAction(string prevUrl)
        {
            PrevAction = prevUrl;
            return this;
        }
        #endregion
        #region script action
        public EditFrameModel<T> AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.ViewModel.Core.Template;
using Kendo.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Base.ViewModel.Core.FrameView
{
    public class ViewTaskFrameModel<T>:CommonView
    {
        #region  Properties
        public string CancelButton { set; get; }
        public T obj{get;set;}
        private FrameType FrameType { set; get; }
        public string ContainerZone = "ContainerZone";
        public string Child1Zone = "Child1Zone";
        public string Child2Zone = "Child2Zone";
        public string Child3Zone = "Child3Zone";
        public string Child4Zone = "Child4Zone";
        public string AddImage1 { get; set; }
        public string AddImage2 { get; set; }
        public string AddImage3 { get; set; }
        public string AddImage4 { get; set; }
        public string AddImage5 { get; set; }
        public string AddImage6 { get; set; }
        public string FrameTitle{ set; get; }
        public string FrameAction { set; get; }
        public string FrameController { set; get; }
        public string NextAction { set; get; }
        public string PrevAction { set; get; }
        #endregion
        #region Contructor
        public ViewTaskFrameModel()
        {
            SettingTemplate();
        }
        #endregion
        #region Overide methods
        public override List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listZone = new List<ZoneTemplate>();
            #region Setting for container
            ZoneTemplate container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
            container.Childs = new List<ZoneTemplate>();
            container.Childs.Add(new ZoneTemplate(Child3Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child1Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child2Zone, "", 4, 0, 0, 0, container.IsRowFluid));
            container.Childs.Add(new ZoneTemplate(Child4Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            
            #endregion
            listZone.Add(container);
            return listZone;
        }
        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            FrameType = FrameType.NormalView;
            FrameTitle = string.Empty;
            #endregion
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(2, 0, false, false, "NextAction", 0, 0, 1, 0, 0, 0, "NextAction", EditorControlTemplate.HiddenInput, Child1Zone));
            lstMap.Add(new FieldMappingPoint(3, 0, false, false, "PrevAction", 0, 0, 1, 0, 0, 0, "PrevAction", EditorControlTemplate.HiddenInput, Child1Zone));
            lstMap.Add(new FieldMappingPoint(4, 0, false, false, "", 0, 0, 12, 0, 0, 0, "AddImage1", DisplayControlTemplate.ImageUrl, Child2Zone));
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "Back", 0, 0, 0, 0, 0, 0, "CancelButton", DisplayControlTemplate.CancelButton, Child3Zone));
            lstMap.Add(new FieldMappingPoint(1, 0, false, false, "title", 0, 0, 1, 0, 0, 0, "FrameTitle", EditorControlTemplate.HiddenInput, Child3Zone));
            lstMap.Add(new FieldMappingPoint(5, 0, false, false, "", 0, 0, 12, 0, 0, 0, "AddImage2", DisplayControlTemplate.ImageUrl, Child4Zone));
            return lstMap;
        }
        #endregion
        #region Setting Methods

        public ViewTaskFrameModel<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public  ViewTaskFrameModel<T> AddCancelAction(string cancelUri,string cancelText)
        {
            CancelButton = cancelUri;
            PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").LabelText = cancelText;
            return this;
        }
        public ViewTaskFrameModel<T> AddMappingField(int row, int column, bool hasLabel, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, string fieldName, string templateName, string containerName)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }
        public ViewTaskFrameModel<T> CreateImage(string imageID, string createUri)
        {
            if (imageID == "image1")
                AddImage1 = createUri;
            if (imageID == "image2")
                AddImage2 = createUri;
            if (imageID == "image3")
                AddImage3 = createUri;
            if (imageID == "image4")
                AddImage4 = createUri;
            if (imageID == "image5")
                AddImage5 = createUri;
            if (imageID == "image6")
                AddImage6 = createUri;
            return this;
        }
        public ViewTaskFrameModel<T> AddSource(T obj)
        {
            this.obj = obj;
            return this;
        }
        public ViewTaskFrameModel<T> AddFrameType(FrameType frameType)
        {
            FrameType = frameType;
            if (FrameType == FrameType.Dialog)
            {
                PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").TemplateName = EditorControlTemplate.CancelButton_Dialog;
            }
            return this;
        }
        public ViewTaskFrameModel<T> AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        public ViewTaskFrameModel<T> AddFrameAction(string action, string controller)
        {
            FrameAction = action;
            FrameController = controller;
            return this;
        }
        public ViewTaskFrameModel<T> AddNextAction(string nextUrl)
        {
            NextAction = nextUrl;
            return this;
        }
        public ViewTaskFrameModel<T> AddPrevAction(string prevUrl)
        {
            PrevAction = prevUrl;
            return this;
        }
        #endregion
        #region script action
        public ViewTaskFrameModel<T> AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion
    }
}
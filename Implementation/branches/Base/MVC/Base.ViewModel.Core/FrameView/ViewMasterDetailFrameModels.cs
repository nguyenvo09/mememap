﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.ViewModel.Core.Template;
using Kendo.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Base.ViewModel.Core.FrameView
{
        public class ViewMasterDetailFrameModel<T> : CommonView
        {
            public string ContainerZone = "ContainerZone";
            public string Child1Zone = "Child1Zone";
            public string Child2Zone = "Child2Zone";
            #region  Properties
            public string btnCancelMaster { set; get; }
            public string ModelKey { set; get; }
            public string NextAction { set; get; }
            public string PrevAction { set; get; }
            public string FrameTitle { set; get; }
            public T obj { get; set; }
            public GridTemplate GridDetail { set; get; }
            public List<ComboBoxTemplate> ComboBoxTemplates { set; get; }
            #endregion
            #region Contructor
            public ViewMasterDetailFrameModel()
            {
                SettingTemplate();
                btnCancelMaster = "#";
                FrameTitle = string.Empty;
            }
            #endregion
            #region Overide methods
            public ViewMasterDetailFrameModel<T> DefaultZone()
            {
                PageTemplate.ZoneTree = new List<ZoneTemplate>();
                #region Setting for container
                ZoneTemplate container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
                container.Childs = new List<ZoneTemplate>();
                container.Childs.Add(new ZoneTemplate(Child1Zone, Child1Zone, 12, 0, 0, 0, container.IsRowFluid));
                container.Childs.Add(new ZoneTemplate(Child2Zone, Child2Zone, 12, 0, 0, 0, container.IsRowFluid));
                #endregion
                PageTemplate.ZoneTree.Add(container);
                return this;
            }
            public override void SettingTemplate()
            {
                #region CommonPageTemplate
                PageTemplate = new CommonPageTemplate();
                PageTemplate.MultiTabs = new List<MultiTabTemplate>();
                PageTemplate.ZoneTree = new List<ZoneTemplate>();
                PageTemplate.MappingPoints = new List<FieldMappingPoint>();
                ComboBoxTemplates = new List<ComboBoxTemplate>();
                GridDetail = new GridTemplate();
                DefaultMapFields();
                DefaultZone();
                #endregion
            }
            private void DefaultMapFields()
            {
                AddMappingField(0, 0, false, false, "Cancel", 0, 0, 1, 0, 0, 0, "btnCancelMaster", DisplayControlTemplate.CancelButton, Child2Zone);
                AddMappingField(0, 0, false, false, "ModelKey", 0, 0, 1, 0, 0, 0, "ModelKey", DisplayControlTemplate.HiddenInput, Child2Zone);
                AddMappingField(0, 0, false, false, "NextAction", 0, 0, 1, 0, 0, 0, "NextAction", DisplayControlTemplate.HiddenInput, Child2Zone);
                AddMappingField(0, 0, false, false, "PrevAction", 0, 0, 1, 0, 0, 0, "PrevAction", DisplayControlTemplate.HiddenInput, Child2Zone);
            }
            #endregion
            #region Master Setting Methods

            public ViewMasterDetailFrameModel<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
            {
                FieldMappingPoint mPoint = new FieldMappingPoint();

                ExtraField ex = new ExtraField();
                ex.Name = fieldName;
                ex.Value = value;
                PageTemplate.ExtraFields.Add(ex);
                mPoint.Row = row;
                mPoint.Column = column;
                mPoint.HasLabel = false;
                mPoint.HasValidation = false;
                mPoint.LabelSpan = 0;
                mPoint.LabelOffset = 0;
                mPoint.LabelText = string.Empty;
                mPoint.FieldSpan = fieldSpan;
                mPoint.FieldOffset = fieldOffset;
                mPoint.ValidationSpan = 0;
                mPoint.ValidationOffset = 0;
                mPoint.FieldName = fieldName;
                mPoint.TemplateName = EditorControlTemplate.ExtraField;
                mPoint.ContainerName = containerName;
                this.PageTemplate.MappingPoints.Add(mPoint);
                return this;
            }

            public ViewMasterDetailFrameModel<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName)
            {
                FieldMappingPoint mPoint = new FieldMappingPoint();
                mPoint.Row = row;
                mPoint.Column = column;
                mPoint.HasLabel = hasLabel;
                mPoint.HasValidation = hasValidation;
                mPoint.LabelSpan = labelSpan;
                mPoint.LabelOffset = labelOffset;
                mPoint.LabelText = labelText;
                mPoint.FieldSpan = fieldSpan;
                mPoint.FieldOffset = fieldOffset;
                mPoint.ValidationSpan = validationSpan;
                mPoint.ValidationOffset = validationOffset;
                mPoint.FieldName = fieldName;
                mPoint.TemplateName = templateName;
                mPoint.ContainerName = containerName;
                this.PageTemplate.MappingPoints.Add(mPoint);
                return this;
            }

            public ViewMasterDetailFrameModel<T> AddMappingField(int row, int column, bool hasLabel, bool hasValidation, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, int validationSpan, int validationOffset, string fieldName, string templateName, string containerName, object htmlAttributes)
            {
                FieldMappingPoint mPoint = new FieldMappingPoint();
                mPoint.Row = row;
                mPoint.Column = column;
                mPoint.HasLabel = hasLabel;
                mPoint.HasValidation = hasValidation;
                mPoint.LabelSpan = labelSpan;
                mPoint.LabelOffset = labelOffset;
                mPoint.LabelText = labelText;
                mPoint.FieldSpan = fieldSpan;
                mPoint.FieldOffset = fieldOffset;
                mPoint.ValidationSpan = validationSpan;
                mPoint.ValidationOffset = validationOffset;
                mPoint.FieldName = fieldName;
                mPoint.TemplateName = templateName;
                mPoint.ContainerName = containerName;
                mPoint.HtmlAttributes = htmlAttributes;
                this.PageTemplate.MappingPoints.Add(mPoint);
                return this;
            }

            public ViewMasterDetailFrameModel<T> AddComboBox(string fieldName, SelectList source)
            {
                PageTemplate.ComboBoxTemplates.Add(new ComboBoxTemplate(fieldName, source));
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddSource(T obj)
            {
                this.obj = obj;
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddCancelAction(string cancelUri, string cancelText)
            {
                this.btnCancelMaster = cancelUri;
                PageTemplate.MappingPoints.Find(m => m.FieldName == "btnCancelMaster").LabelText = cancelText;
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddModelKeyName(string modelKeyFieldName)
            {
                ModelKey = modelKeyFieldName;
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddNextAction(string nextUrl)
            {
                NextAction = nextUrl;
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddPrevAction(string prevUrl)
            {
                PrevAction = prevUrl;
                return this;
            }
            public ViewMasterDetailFrameModel<T> AddFrameTitle(string tilte)
            {
                FrameTitle = tilte;
                return this;
            }
            #endregion
            #region Detail Setting Methods
            public ViewMasterDetailFrameModel<T> GridColumn(string title, string memberName, Type memberType, string width)
            {
                GridColumnSettings col = new GridColumnSettings();
                col.Filterable = true;
                col.Groupable = true;
                col.Sortable = true;
                col.Title = title;
                col.Member = memberName;
                col.MemberType = memberType;
                col.Width = width;
                GridDetail.SetColumn(col);
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridColumn(string title, string memberName, Type memberType, string width, string coltemplate)
            {

                //find grid in list<grid>
                GridColumnSettings col = new GridColumnSettings();
                col.Filterable = true;
                col.Groupable = true;
                col.Sortable = true;
                col.Title = title;
                col.Member = memberName;
                col.MemberType = memberType;
                col.Width = width;
                col.ClientTemplate = coltemplate;
                GridDetail.SetColumn(col);
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridColumn(GridColumnSettings col)
            {
                GridDetail.SetColumn(col);
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridColumn(Action<ViewMasterDetailFrameModel<T>> col)
            {
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridName(string gridName)
            {
                GridDetail.GridName = gridName;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridKeyName(string keyName)
            {
                GridDetail.KeyName = keyName;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridController(string controller)
            {
                GridDetail.ControllerName = controller;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridAjaxSelectAction(string selectAction)
            {
                GridDetail.SelectAjaxAction = selectAction;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridAjaxSelectAction(string selectAction, string controllerName, object routedValues)
            {
                GridDetail.SelectAjaxAction = selectAction;
                GridDetail.RoutedValues = routedValues;
                GridDetail.ControllerName = controllerName;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridPageIndex(int pageIndex)
            {
                GridDetail.PageIndex = pageIndex;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridPageSize(int pageSize)
            {
                GridDetail.PageSize = pageSize == 0 ? 20 : pageSize;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridTotalCount(int totalCount)
            {
                GridDetail.TotalCount = totalCount;
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridEnableDetail(string detailUrl)
            {
                GridDetail.EnableDetail(true, detailUrl);
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridEnableDialogView()
            {
                GridDetail.EnableDialogView();
                return this;
            }
            public ViewMasterDetailFrameModel<T> GridHieght(string height)
            {
                GridDetail.SetHeight(height);
                return this;
            }
            #endregion
            #region script action
            public ViewMasterDetailFrameModel<T> AddScriptItem(string name, string source, ScriptType type)
            {
                ScriptRegister item = new ScriptRegister();
                item.Name = name;
                item.Source = source;
                item.ScriptType = type;
                this.PageTemplate.ScriptRegisters.Add(item);
                return this;
            }
            #endregion

        }
}
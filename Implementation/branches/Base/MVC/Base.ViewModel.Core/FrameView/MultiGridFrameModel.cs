﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kendo.Mvc.UI;
using Base.ViewModel.Core.Template;

namespace Base.ViewModel.Core.FrameView
{
    public class MultiGridFrameModel : CommonView
    {
        #region  Properties
        public string ContainerZone = "ContainerZone";
        public string Child1Zone = "Child1Zone";
        public string FrameTitle { set; get; }
        ZoneTemplate container;
        public string Grid1Title { set; get; }
        public string Grid2Title { set; get; }
        public string Grid3Title { set; get; }
        
        public GridTemplate Grid1 { set; get; }
        public GridTemplate Grid2 { set; get; }
        public GridTemplate Grid3 { set; get; }
        public GridTemplate Grid4 { set; get; }
        public GridTemplate Grid5 { set; get; }
        public GridTemplate Grid6 { set; get; }
        public GridTemplate Grid7 { set; get; }
        public GridTemplate Grid8 { set; get; }
        public List<GridTemplate> Grids;
        #endregion
        #region contructor
        public MultiGridFrameModel()
        {
            Grids = new List<GridTemplate>();
            SettingTemplate();
        }
        #endregion
        #region overide method
        public override List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listZone = new List<ZoneTemplate>();
            container = new ZoneTemplate(ContainerZone, ContainerZone, 12, 0, 0, 0, true);
            listZone.Add(container);
            container.Childs = new List<ZoneTemplate>();
            container.Childs.Add(new ZoneTemplate(Child1Zone, "", 8, 0, 0, 0, container.IsRowFluid));
            return listZone;
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(0, 0, false, false, "", 0, 0, 0, 0, 0, 0, "AddImage1", DisplayControlTemplate.ImageUrl, Child1Zone));
            return lstMap;
        }
        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            #endregion
        }
        #endregion
        #region Setting Methods

        public MultiGridFrameModel AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = EditorControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        #region Grid Action
        private GridTemplate InitGrid(string gridName)
        {
            try
            {
                if (Grid1 == null)
                {
                    Grid1 = new GridTemplate();
                    Grid1.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(2, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid1", DisplayControlTemplate.SimpleGrid, zone.Name);
                    FieldMappingPoint fMapTitle = new FieldMappingPoint(1, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid1Title", DisplayControlTemplate.Html, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    PageTemplate.MappingPoints.Add(fMapTitle);
                    container.Childs.Add(zone);
                    return Grid1;
                }
                else
                {
                    if (Grid1.GridName == gridName) return Grid1;
                }
                if (Grid2 == null)
                {
                    Grid2 = new GridTemplate();
                    Grid2.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 8, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMapGridTitle = new FieldMappingPoint(3, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid2Title", DisplayControlTemplate.Html, zone.Name);
                    FieldMappingPoint fMap = new FieldMappingPoint(4, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid2", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    PageTemplate.MappingPoints.Add(fMapGridTitle);
                    container.Childs.Add(zone);
                    return Grid2;
                }
                else
                {
                    if (Grid2.GridName == gridName) return Grid2;
                }
                if (Grid3 == null)
                {
                    Grid3 = new GridTemplate();
                    Grid3.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 8, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMapGridTitle = new FieldMappingPoint(5, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid3Title", DisplayControlTemplate.Html, zone.Name);
                    FieldMappingPoint fMap = new FieldMappingPoint(6, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid3", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    PageTemplate.MappingPoints.Add(fMapGridTitle);
                    container.Childs.Add(zone);
                    return Grid3;
                }
                else
                {
                    if (Grid3.GridName == gridName) return Grid3;
                }
                if (Grid4 == null)
                {
                    Grid4 = new GridTemplate();
                    Grid4.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(7, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid4", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    container.Childs.Add(zone);
                    return Grid4;
                }
                else
                {
                    if (Grid4.GridName == gridName) return Grid4;
                }
                if (Grid5 == null)
                {
                    Grid5 = new GridTemplate();
                    Grid5.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(8, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid5", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    container.Childs.Add(zone);
                    return Grid5;
                }
                else
                {
                    if (Grid5.GridName == gridName) return Grid5;
                }
                if (Grid6 == null)
                {
                    Grid6 = new GridTemplate();
                    Grid6.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(9, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid6", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    container.Childs.Add(zone);
                    return Grid6;
                }
                else
                {
                    if (Grid6.GridName == gridName) return Grid6;
                }
                if (Grid7 == null)
                {
                    Grid7 = new GridTemplate();
                    Grid7.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(10, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid7", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    container.Childs.Add(zone);
                    return Grid7;
                }
                else
                {
                    if (Grid7.GridName == gridName) return Grid7;
                }

                if (Grid8 == null)
                {
                    Grid8 = new GridTemplate();
                    Grid8.GridName = gridName;
                    ZoneTemplate zone = new ZoneTemplate(gridName + "_Zone", "", 12, 0, 0, 0, container.IsRowFluid);
                    FieldMappingPoint fMap = new FieldMappingPoint(11, 0, false, false, "", 0, 0, 12, 0, 0, 0, "Grid8", DisplayControlTemplate.SimpleGrid, zone.Name);
                    PageTemplate.MappingPoints.Add(fMap);
                    container.Childs.Add(zone);
                    return Grid8;
                }
                else
                {
                    if (Grid8.GridName == gridName) return Grid8;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public MultiGridFrameModel GridColumn(string gridName, string title, string memberName, Type memberType, string width)
        {
            //find grid in list<grid>
            GridTemplate grid = InitGrid(gridName);
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = true;
            col.Groupable = true;
            col.Sortable = true;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            grid.SetColumn(col);
            return this;
        }
        public MultiGridFrameModel GridColumn(string gridName, string title, string memberName, Type memberType, string width,string coltemplate)
        {
            //find grid in list<grid>
            GridTemplate grid = InitGrid(gridName);
            GridColumnSettings col = new GridColumnSettings();
            col.Filterable = true;
            col.Groupable = true;
            col.Sortable = true;
            col.Title = title;
            col.Member = memberName;
            col.MemberType = memberType;
            col.Width = width;
            col.ClientTemplate = coltemplate;
            grid.SetColumn(col);
            return this;
        }
        public MultiGridFrameModel GridColumn(string gridName, GridColumnSettings col)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.SetColumn(col);
            return this;
        }
       
        public MultiGridFrameModel GridKeyName(string gridName, string keyName)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.KeyName = keyName;
            return this;
        }
        public MultiGridFrameModel GridController(string gridName, string controller)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.ControllerName = controller;
            return this;
        }
        public MultiGridFrameModel GridAjaxSelectAction(string gridName,string selectAction, string controllerName, object routedValues)
        {
           GridTemplate grid = InitGrid(gridName);
           grid.RoutedValues = routedValues;
           grid.ControllerName = controllerName;
           grid.SelectAjaxAction = selectAction;
            return this;
        }
        public MultiGridFrameModel GridAjaxSelectAction(string gridName, string selectAction)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.SelectAjaxAction = selectAction;
            return this;
        }
       
        public MultiGridFrameModel AddGridTitle(string gridName, string title)
        {
            if(gridName == "GirdUser")
            this.Grid1Title = title;
            if (gridName == "MyTask")
                this.Grid2Title = title;
            if(gridName == "NewAvailableTask")
                this.Grid3Title = title;
            return this;
        }
        public MultiGridFrameModel GridPageIndex(string gridName, int pageIndex)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.PageIndex = pageIndex;
            return this;
        }
        public MultiGridFrameModel GridPageSize(string gridName, int pageSize)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.PageSize = pageSize == 0 ? 20 : pageSize;
            return this;
        }
        public MultiGridFrameModel GridTotalCount(string gridName, int totalCount)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.TotalCount = totalCount;
            return this;
        }
        public MultiGridFrameModel GridEnableDelete(string gridName, string deleteUrl)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.EnableDelete(true, deleteUrl);
            return this;
        }
        public MultiGridFrameModel GridEnableEdit(string gridName, string editUrl)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.EnableEdit(true, editUrl);
            return this;
        }
        public MultiGridFrameModel GridEnableDetail(string gridName, string detailUrl)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.EnableDetail(true, detailUrl);
            return this;
        }
        public MultiGridFrameModel GridHieght(string gridName, string height)
        {
            GridTemplate grid = InitGrid(gridName);
            grid.SetHeight(height);
            return this;
        }
        #endregion
        #region Other Action
        public MultiGridFrameModel AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        #endregion
        #endregion
        #region script action
        public MultiGridFrameModel AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.ViewModel.Core.Template;
using Kendo.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Base.ViewModel.Core.FrameView
{
    public class HomeFrameModel<T>:CommonView
    {
        #region  Properties
        private FrameType FrameType { set; get; }
        public string TopZone = "TopZone";
        public string BodyLeftZone = "BodyLeftZone";
        public string BodyRightZone = "BodyRightZone";
        public string FooterZone = "FooterZone";
        public string FrameTitle{ set; get; }
        public List<String> ExtraFields { set; get; }
        public MenuTemplate LeftTopMenu { set; get; }
        public MenuTemplate RightTopMenu { set; get; }
        public MenuTemplate LeftBottomMenu { set; get; }
        public MenuTemplate RightBottomMenu { set; get; }
        public T obj { get; set; }
        #endregion
        #region Contructor
        public HomeFrameModel()
        {
            SettingTemplate();
        }
        #endregion
        #region Overide methods
        public override List<ZoneTemplate> SettingForZone()
        {
            List<ZoneTemplate> listZone = new List<ZoneTemplate>();
            #region Setting for container
            listZone.Add(new ZoneTemplate(TopZone, "", 12, 0, 0, 0, false));
            listZone.Add(new ZoneTemplate(BodyLeftZone, "", 8, 0, 0, 0, false));
            listZone.Add(new ZoneTemplate(BodyRightZone, "", 4, 0, 0, 0, false));
            listZone.Add(new ZoneTemplate(FooterZone, "", 12, 0, 0, 0, false));
            #endregion
            return listZone;
        }
        public override void SettingTemplate()
        {
            #region CommonPageTemplate
            PageTemplate = new CommonPageTemplate();
            PageTemplate.ZoneTree = SettingForZone();
            PageTemplate.MappingPoints = SettingForMappingPoints();
            FrameType = FrameType.NormalView;
            FrameTitle = string.Empty;
            ExtraFields = new List<string>();
            #endregion
        }
        public override List<FieldMappingPoint> SettingForMappingPoints()
        {
            List<FieldMappingPoint> lstMap = new List<FieldMappingPoint>();
            lstMap.Add(new FieldMappingPoint(1, 0, false, false, "title", 0, 0, 1, 0, 0, 0, "FrameTitle", EditorControlTemplate.HiddenInput, FooterZone));
            return lstMap;
        }

       
        #endregion
        #region Tabs Medthods
        public HomeFrameModel<T> EnableTabControl(int row, int col, int fieldSpan, int fieldOffset, string tabControlName, MultiTabType tapType, string containerName)
        {
            MultiTabTemplate tabControl = new MultiTabTemplate(tabControlName, tabControlName, tapType, new List<MultiTabPanelTemplate>());
            this.PageTemplate.MultiTabs.Add(tabControl);
            AddMappingField(row, col, false, "", 0, 0, fieldSpan, fieldOffset, tabControlName, DisplayControlTemplate.MultiTab, containerName);
            return this;
        }
        public HomeFrameModel<T> AddTabPanel(int row, int col, string panelName, string displayText, string tabControlName, bool isActive)
        {
            MultiTabTemplate tabControl = this.PageTemplate.MultiTabs.Find(t => t.Name == tabControlName);
            MultiTabPanelTemplate tabPanel = new MultiTabPanelTemplate(panelName, displayText, isActive);
            tabControl.TabPanels.Add(tabPanel);
            return this;
        }
        #endregion
        #region Setting Methods

        public HomeFrameModel<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = DisplayControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public HomeFrameModel<T> AddExtraField(int row, int column, int fieldSpan, int fieldOffset, string containerName, string fieldName, string value, string labelStyle, string fieldStyle)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();

            ExtraField ex = new ExtraField();
            ex.Name = fieldName;
            ex.Value = value;
            PageTemplate.ExtraFields.Add(ex);
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = false;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = 0;
            mPoint.LabelOffset = 0;
            mPoint.LabelText = string.Empty;
            mPoint.LabelStyle = labelStyle;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.FieldStyle = fieldStyle;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = DisplayControlTemplate.ExtraField;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }
        

        public HomeFrameModel<T> AddMappingField(int row, int column, bool hasLabel, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, string fieldName, string templateName, string containerName)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }

        public HomeFrameModel<T> AddMappingField(int row, int column, bool hasLabel, string labelText, int labelSpan, int labelOffset, int fieldSpan, int fieldOffset, string fieldName, string templateName, string containerName, object htmlAttributes)
        {
            FieldMappingPoint mPoint = new FieldMappingPoint();
            mPoint.Row = row;
            mPoint.Column = column;
            mPoint.HasLabel = hasLabel;
            mPoint.HasValidation = false;
            mPoint.LabelSpan = labelSpan;
            mPoint.LabelOffset = labelOffset;
            mPoint.LabelText = labelText;
            mPoint.FieldSpan = fieldSpan;
            mPoint.FieldOffset = fieldOffset;
            mPoint.ValidationSpan = 0;
            mPoint.ValidationOffset = 0;
            mPoint.FieldName = fieldName;
            mPoint.TemplateName = templateName;
            mPoint.ContainerName = containerName;
            mPoint.HtmlAttributes = htmlAttributes;
            this.PageTemplate.MappingPoints.Add(mPoint);
            return this;
        }       
       
        public HomeFrameModel<T> AddFrameType(FrameType frameType)
        {
            FrameType = frameType;
            if (FrameType == FrameType.Dialog)
            {
                PageTemplate.MappingPoints.Find(m => m.FieldName == "CancelButton").TemplateName = EditorControlTemplate.CancelButton_Dialog;
            }
            return this;
        }
        public HomeFrameModel<T> AddFrameTitle(string tilte)
        {
            FrameTitle = tilte;
            return this;
        }
        public HomeFrameModel<T> AddSource(T obj)
        {
            this.obj = obj;
            return this;
        }

        #endregion
        #region MenuAction
        public HomeFrameModel<T> EnableMenu(bool enable, XPosition x, YPosition y, int row, int col, int span, int offset, string container, MenuType menuType)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.MenuType = menuType;
            FieldMappingPoint fMap = PageTemplate.MappingPoints.FirstOrDefault(c => c.FieldName == menu.MenuName);
            if (enable)
            {
                if (fMap == null)
                {
                    fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, menu.MenuName, DisplayControlTemplate.Menu, container);
                    PageTemplate.MappingPoints.Add(fMap);
                }
                fMap.Row = row;
                fMap.Column = col;
                fMap.FieldSpan = span;
                fMap.FieldOffset = offset;
                fMap.ContainerName = container;
            }
            else
            {
                if (fMap != null) PageTemplate.MappingPoints.Remove(fMap);
            }
            return this;
        }
        public HomeFrameModel<T> AddMenuItem(XPosition x, YPosition y, MenuType menuType, List<MenuData> menuData)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.MenuType = menuType;
            menu.MenuData = menuData;
            return this;
        }
        public HomeFrameModel<T> AddMenuItem(XPosition x, YPosition y, int row, int col, int menuID, string displayText, string tipText, int? parentID, bool isActive, string iconURL, string url)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.AddMenuItem(row, col, menuID, displayText, tipText, parentID, isActive, iconURL, true, url);
            return this;
        }
        public HomeFrameModel<T> AddMenuItem(XPosition x, YPosition y, int row, int col, int menuID, string displayText, string tipText, int? parentID, bool isActive, string iconURL, bool useURL, string actionOrURL)
        {
            MenuTemplate menu = InitMenu(x, y);
            menu.AddMenuItem(row, col, menuID, displayText, tipText, parentID, isActive, iconURL, useURL, actionOrURL);
            return this;
        }
        private MenuTemplate InitMenu(XPosition x, YPosition y)
        {
            switch (x)
            {
                case XPosition.Left:
                    switch (y)
                    {
                        case YPosition.Top:
                            if (LeftTopMenu == null)
                            {
                                LeftTopMenu = new MenuTemplate();
                                LeftTopMenu.MenuPosition.xPosition = x;
                                LeftTopMenu.MenuPosition.yPosition = y;
                                LeftTopMenu.MenuName = "LeftTopMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, LeftTopMenu.MenuName, DisplayControlTemplate.Menu, "");
                                this.PageTemplate.MappingPoints.Insert(0, fMap);
                            }
                            return LeftTopMenu;
                        case YPosition.Bottom:
                            if (LeftBottomMenu == null)
                            {
                                LeftBottomMenu = new MenuTemplate();
                                LeftBottomMenu.MenuPosition.xPosition = x;
                                LeftBottomMenu.MenuPosition.yPosition = y;
                                LeftBottomMenu.MenuName = "LeftBottomMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, LeftBottomMenu.MenuName, DisplayControlTemplate.Menu, "");
                                this.PageTemplate.MappingPoints.Add(fMap);
                            }
                            return LeftBottomMenu;
                    }
                    break;
                case XPosition.Right:
                    switch (y)
                    {
                        case YPosition.Top:
                            if (RightTopMenu == null)
                            {
                                RightTopMenu = new MenuTemplate();
                                RightTopMenu.MenuPosition.xPosition = x;
                                RightTopMenu.MenuPosition.yPosition = y;
                                RightTopMenu.MenuName = "RightTopMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, RightTopMenu.MenuName, DisplayControlTemplate.Menu, "");
                                this.PageTemplate.MappingPoints.Insert(0, fMap);
                            }
                            return RightTopMenu;
                        case YPosition.Bottom:
                            if (RightBottomMenu == null)
                            {
                                RightBottomMenu = new MenuTemplate();
                                RightBottomMenu.MenuPosition.xPosition = x;
                                RightBottomMenu.MenuPosition.yPosition = y;
                                RightBottomMenu.MenuName = "RightBottomMenu";
                                FieldMappingPoint fMap = new FieldMappingPoint(0, 0, false, false, "", 0, 0, 2, 0, 0, 0, RightBottomMenu.MenuName, DisplayControlTemplate.Menu, "");
                                this.PageTemplate.MappingPoints.Insert(0, fMap);
                            }
                            return RightBottomMenu;
                    }
                    break;
            }
            return null;
        }
        #endregion
        #region script action
        public HomeFrameModel<T> AddScriptItem(string name, string source, ScriptType type)
        {
            ScriptRegister item = new ScriptRegister();
            item.Name = name;
            item.Source = source;
            item.ScriptType = type;
            this.PageTemplate.ScriptRegisters.Add(item);
            return this;
        }
        
        #endregion
        #region Style action
        public HomeFrameModel<T> AddStyleItem(string name, string source, StyleType type)
        {
            StyleRegister item = new StyleRegister();
            item.Name = name;
            item.Source = source;
            item.StyleType = type;
            this.PageTemplate.StyleRegisters.Add(item);
            return this;
        }

        #endregion
      
    }
}
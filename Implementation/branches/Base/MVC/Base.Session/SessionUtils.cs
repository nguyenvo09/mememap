﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using Base.Model.DTO;
using MemeMVC.Model.DTO;

namespace Base
{
    public class SessionUtils
    {
        public static UserDTO CurrentUser
        {
            set
            {
                JsonSerializer serializer = new JsonSerializer();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                serializer.Serialize(writer, value);
                HttpContext.Current.Session["SessionUtils.USERINFO"] = sb.ToString();
                
            }

            get
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Session["SessionUtils.USERINFO"] as string))
                {
                    JsonReader reader = new JsonTextReader(new StringReader(HttpContext.Current.Session["SessionUtils.USERINFO"] as string));
                    JsonSerializer serializer = new JsonSerializer();
                    UserDTO user = serializer.Deserialize<UserDTO>(reader);
                    return user;
                }
                return null;
            }
        }
        

       
    }
}
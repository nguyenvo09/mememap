using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.Model.DTO;
using Base.ViewModel.Core.Template;
using MemeMVC.Model.DTO;

namespace Base.ViewModel
{
    public class UserInViewModel
    {
        public string Result {get;set;}
        public string GUID { get; set; }
        public int UserID { get; set; }
        public UserInstanceDTO UserIn { get; set; }
        public UserInViewModel()
        {
            UserIn = new UserInstanceDTO();
        }
    }
    public class UserInsViewModel
    {
        public string Result { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalCount { get; set; }
        public List<UserInstanceDTO> UserIns { get; set; }
        public UserInsViewModel()
        {
            UserIns = new List<UserInstanceDTO>();
        }
    }
}

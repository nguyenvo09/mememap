﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.Model.DTO;

namespace Base.ViewModel
{
    public partial class GUIDGenerateViewModel
    {
        public string Result { get; set; }
        public string GUIDString { get; set; }
        public int MasterID { get; set; }
        public GUIDGenerateViewModel() {
            GUIDString = Guid.NewGuid().ToString();
        }
    }
}

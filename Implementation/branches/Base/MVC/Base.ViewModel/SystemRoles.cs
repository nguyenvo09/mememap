﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Base.ViewModel
{
    public static class  SystemRole
    {
        public const string SystemAdmin = "SystemAdmin";
        public const string Admin = "Admin";
        public const string Member = "Member";
        public const string Guest = "Guest";
    }
}

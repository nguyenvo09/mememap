﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Base.ViewModel
{
    public static class BackupType
    {
        public const string Project = "Project";
        public const string KPI = "KPI";
    }
    public static class Module
    {
        public const string General = "General";
        public const string KPI = "KPI";
        public const string Milestone = "Milestone";
        public const string ActionPlan = "ActionPlan";
        public const string VitalityReport = "VitalityReport";
        public const string Risk = "Risk";
        public const string Document = "Document";
    }

}

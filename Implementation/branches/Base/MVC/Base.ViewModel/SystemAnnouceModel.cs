﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.ViewModel
{
    public class SystemAnnouces {
        public List<SystemAnnouce> Annouces { get; set; }
        public SystemAnnouces()
        {
            Annouces = new List<SystemAnnouce>();
        }
        public SystemAnnouces(SystemAnnouces systemAnouce)
        {
            this.Annouces = systemAnouce.Annouces;
        }
        public SystemAnnouces AddAnnouce(string name,string message,string guideTip,AnnouceType type)
        {
            Annouces.Add(new SystemAnnouce(name, message, guideTip,type));
            return this;
        }
    }
    public class SystemAnnouce {
        public string Name { get; set; }
        public string Message { get; set; }
        public string GuideTip { get; set; }
        public AnnouceType AnnouceType { get; set; }
        public SystemAnnouce() { 
        }
        public SystemAnnouce(string name, string message,string guideTip,AnnouceType type)
        {
            this.Name = name;
            this.Message = message;
            this.GuideTip = guideTip;
            this.AnnouceType = type;
        }

    }
    public enum AnnouceType 
    {
       Error,Information,Warning
    }
}

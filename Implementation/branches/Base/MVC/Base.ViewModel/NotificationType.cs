﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Base.ViewModel
{
    public static class NotificationType
    {
        public const string TagUser = "Tag User";
        public const string AddAction = "Add Action";
        public const string EditAction = "Edit Action";
        public const string Unknown = "Unknown";
    }


}

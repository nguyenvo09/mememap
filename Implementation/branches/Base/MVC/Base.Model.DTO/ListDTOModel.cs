﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Base.Model;
using System.Collections.Generic;
namespace Base.Model.DTO
{
    public class ListDTOModel<T>
    {
        public int PageIndex { get;set; }
        public int PageSize { get; set; }
        public int TotalCount { get;set; }
        public List<T> Source { get; set; }
    }
}

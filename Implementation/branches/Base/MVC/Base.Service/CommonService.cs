﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base.Model.DTO;
using Kendo.Mvc.UI;
using Kendo.Mvc;
using Base.ViewModel;
using System.IO;
using System.Security.Cryptography;
using System.Web.Security;
using Newtonsoft.Json;
namespace Base.Service
{
    public class CommonService
    {
        public static bool HasError { get; set; }
        public static SystemAnnouces SystemAnnouces { get; set; }
        public static void AddSystemAnnouce(string name, string message, string guideTip, AnnouceType type)
        {
            if (SystemAnnouces == null) SystemAnnouces = new SystemAnnouces();
            SystemAnnouces.AddAnnouce(name, message, guideTip, type);
        }

        #region For custom filter
        public static List<CustomGridFilter> CreateCustomFilters(IList<IFilterDescriptor> ft)
        {
            List<CustomGridFilter> rL = new List<CustomGridFilter>();
            for (int i = 0; i < ft.Count; i++)
            {
                CustomGridFilter cgf = new CustomGridFilter();
                FilterDescriptor fd = ft[i] as FilterDescriptor;
                if (fd != null) // only one FilterDescriptor
                {
                    CustomFilter cf = new CustomFilter();
                    cf.ConvertedValue = fd.ConvertedValue;
                    cf.Member = fd.Member;
                    cf.MemberType = fd.MemberType;
                    cf.Operator = fd.Operator;
                    cf.Value = fd.Value;
                    cgf.Filter = cf;
                }
                else // is CompositeFilterDescriptor
                {
                    CompositeFilterDescriptor cfd = ft[i] as CompositeFilterDescriptor;
                    if (cfd != null)
                    {
                        cgf.CompositeFilter = CreateCustomCompositeFilter(cfd);
                    }
                }
                rL.Add(cgf);
            }
            return rL;
        }
        public static CustomCompositeFilter CreateCustomCompositeFilter(CompositeFilterDescriptor cfd)
        {
            CustomCompositeFilter ccf = new CustomCompositeFilter();
            ccf.LogicalOperator = cfd.LogicalOperator;
            ccf.Filters = new List<CustomFilter>();
            ccf.CompositeFilters = new List<CustomCompositeFilter>();
            for (int i = 0; i < cfd.FilterDescriptors.Count; i++)
            {
                FilterDescriptor fd = cfd.FilterDescriptors[i] as FilterDescriptor;
                if (fd != null)
                {
                    CustomFilter cf = new CustomFilter();
                    cf.ConvertedValue = fd.ConvertedValue;
                    cf.Member = fd.Member;
                    cf.MemberType = fd.MemberType;
                    cf.Operator = fd.Operator;
                    cf.Value = fd.Value;
                    ccf.Filters.Add(cf);
                }
                else
                {
                    CompositeFilterDescriptor cfd1 = cfd.FilterDescriptors[i] as CompositeFilterDescriptor;
                    ccf.CompositeFilters.Add(CreateCustomCompositeFilter(cfd1));
                }
            }
            return ccf;
        }
        public static List<CustomGroupDescriptor> CreateCustomGroupDescription(IList<GroupDescriptor> groups)
        {
            List<CustomGroupDescriptor> lcg = new List<CustomGroupDescriptor>();
            foreach (var g in groups)
            {
                CustomGroupDescriptor cg = new CustomGroupDescriptor();
                cg.Member = g.Member;
                cg.SortDirection = g.SortDirection;
                lcg.Add(cg);
            }
            return lcg;
        }
        public static FilterOperator ConvertToFilterOperator(string sOpearator)
        {
            switch (sOpearator)
            {
                //equal ==
                case "eq":
                case "==":
                case "isequalto":
                case "equals":
                case "equalto":
                case "equal":
                    return FilterOperator.IsEqualTo;
                //not equal !=
                case "neq":
                case "!=":
                case "isnotequalto":
                case "notequals":
                case "notequalto":
                case "notequal":
                case "ne":
                    return FilterOperator.IsNotEqualTo;
                // Greater
                case "gt":
                case ">":
                case "isgreaterthan":
                case "greaterthan":
                case "greater":
                    return FilterOperator.IsGreaterThan;
                // Greater or equal
                case "gte":
                case ">=":
                case "isgreaterthanorequalto":
                case "greaterthanequal":
                case "ge":
                    return FilterOperator.IsGreaterThanOrEqualTo;
                // Less
                case "lt":
                case "<":
                case "islessthan":
                case "lessthan":
                case "less":
                    return FilterOperator.IsLessThan;
                // Less or equal
                case "lte":
                case "<=":
                case "islessthanorequalto":
                case "lessthanequal":
                case "le":
                    return FilterOperator.IsLessThanOrEqualTo;
                case "startswith":
                    return FilterOperator.StartsWith;

                case "endswith":
                    return FilterOperator.EndsWith;
                //string.Contains()
                case "contains":
                    return FilterOperator.Contains;
                case "doesnotcontain":
                    return FilterOperator.DoesNotContain;
                default:
                    return FilterOperator.Contains;

            }
        }
        public static FilterCompositionLogicalOperator ConvertToLogicalOperator(string sOpearator)
        {
            switch (sOpearator.ToLower())
            {
                case "and":
                    return FilterCompositionLogicalOperator.And;
                case "or":
                    return FilterCompositionLogicalOperator.Or;
                default:
                    return FilterCompositionLogicalOperator.And;
            }
        }
        public static List<CustomGridFilter> CreateCustomFilters(GFilter ft)
        {
            List<CustomGridFilter> rL = new List<CustomGridFilter>();
            CustomGridFilter cgf = new CustomGridFilter();
            if (string.IsNullOrWhiteSpace(ft.logic)) //only one FilterDescriptor
            {
                CustomFilter cf = new CustomFilter();
                cf.ConvertedValue = ft.value;
                cf.Member = ft.field;
                cf.Operator = ConvertToFilterOperator(ft.@operator);
                cf.Value = ft.value;
                cgf.Filter = cf;
            }
            else // is CompositeFilterDescriptor
            {
                cgf.CompositeFilter = CreateCustomCompositeFilter(ft);
            }
            rL.Add(cgf);
            return rL;
        }
        public static CustomCompositeFilter CreateCustomCompositeFilter(GFilter ft)
        {
            CustomCompositeFilter ccf = new CustomCompositeFilter();
            ccf.LogicalOperator = ConvertToLogicalOperator(ft.logic);
            ccf.Filters = new List<CustomFilter>();
            ccf.CompositeFilters = new List<CustomCompositeFilter>();
            for (int i = 0; i < ft.filters.Count; i++)
            {
                GFilter fd = ft.filters[i];
                if (string.IsNullOrWhiteSpace(fd.logic))
                {
                    CustomFilter cf = new CustomFilter();
                    cf.ConvertedValue = fd.value;
                    cf.Member = fd.field;
                    cf.Operator = ConvertToFilterOperator(fd.@operator);
                    cf.Value = fd.value;
                    ccf.Filters.Add(cf);
                }
                else
                {
                    ccf.CompositeFilters.Add(CreateCustomCompositeFilter(fd));
                }
            }
            return ccf;
        }
        public static void ConvertToCustomGridCommand(CustomGridCommand cCommand, DataSourceRequest request)
        {
            if (cCommand == null)
            {
                cCommand = new CustomGridCommand();
            }
            if (request.Filters != null && request.Filters.Any())
            {
                cCommand.FilterDescriptors = CommonService.CreateCustomFilters(request.Filters);
            }
            cCommand.Page = request.Page;
            cCommand.PageSize = request.PageSize;
            cCommand.SortDescriptors = request.Sorts;
            if (request.Filters != null && request.Filters.Any())
            {
                cCommand.GroupDescriptors = CommonService.CreateCustomGroupDescription(request.Groups);
            }
        }
        public static void ConvertToCustomGridCommand(CustomGridCommand cCommand, GFilter filters)
        {
            if (cCommand == null)
            {
                cCommand = new CustomGridCommand();
            }
            cCommand.FilterDescriptors = CommonService.CreateCustomFilters(filters);
        }
        #endregion

        public static bool CreateFolder(string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                return true;
            }
            catch
            {

            }
            return false;
        }
        /// <summary>
        /// Delete a file
        /// </summary>
        /// <param name="path">EX: Server.MapPath('~/Upload/Image01.png')</param>
        /// <returns>True: delete success False: delete unsuccess</returns>
        public static bool DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }
        /// <summary>
        /// Get list file name of a directory
        /// </summary>
        /// <param name="directoryPath">DirectoryPath EX: Server.MapPath("~/Upload")</param>
        /// <returns></returns>
        public static List<string> GetFileNames(string directoryPath)
        {
            List<string> listFile = new List<string>();
            try
            {
                if (Directory.Exists(directoryPath))
                {
                    string[] ls = Directory.GetFiles(directoryPath);
                    for (int i = 0; i < ls.Length; i++)
                    {
                        ls[i] = Path.GetFileName(ls[i]);
                    }
                    listFile = ls.ToList();
                }
            }
            catch { }
            return listFile;
        }

        #region Hash Password
        //public static string CreateSalt()
        //{
        //    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        //    byte[] buff = new byte[32];
        //    rng.GetBytes(buff);
        //    return Convert.ToBase64String(buff);
        //}
        public static string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "SHA1");
            return hashedPwd;
        }

        public static string GetRandomPassword(int length)
        {
            char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string password = string.Empty;
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                int x = random.Next(1, chars.Length);
                //Don't Allow Repetation of Characters
                if (!password.Contains(chars.GetValue(x).ToString()))
                    password += chars.GetValue(x);
                else
                    i--;
            }
            return password;
        }
        #endregion

        public static string SerializerToJson(object value)
        {
            JsonSerializer serializer = new JsonSerializer();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            serializer.Serialize(writer, value);
            return sb.ToString();
        }

        public static string EncodeString(string value)
        {
            string result = string.Empty;
            result = result.Replace("%", "%25").Replace("$", "%24").Replace("&", "%26");
            result = value.Replace("~", "%7E").Replace("!", "%21").Replace("#", "%23");
            result = result.Replace("^", "%5E").Replace("(", "%28").Replace(")", "%29");
            result = result.Replace("{", "%7B").Replace("}", "%7D").Replace("[", "%5B").Replace("]", "%5D");
            result = result.Replace("=", "%3D").Replace("|", "%7C").Replace("\\", "%5C");
            result = result.Replace("'", "%27").Replace("\"", "%22");
            result = result.Replace(";", "%3B").Replace(":", "%3A").Replace("?", "%3F");
            result = result.Replace(",", "%2C").Replace("<", "%3C").Replace(">", "%3E");
            result = result.Replace(" ", "%20").Replace(" ", "%A0");
            return result;
        }

        public static string DecodeString(string value)
        {
            string result = string.Empty;
            result = result.Replace("%25", "%").Replace("%24", "$").Replace("%26", "&");
            result = value.Replace("%7E", "~").Replace("%21", "!").Replace("%23", "#");
            result = result.Replace("%5E", "^").Replace("%28", "(").Replace("%29", ")");
            result = result.Replace("%7B", "{").Replace("%7D", "}").Replace("%5B", "[").Replace("%5D", "]");
            result = result.Replace("%3D", "=").Replace("%7C", "|").Replace("%5C", "\\");
            result = result.Replace("%27", "'").Replace("%22", "\"");
            result = result.Replace("%3B", ";").Replace("%3A", ":").Replace("%3F", "?");
            result = result.Replace("%2C", ",").Replace("%3C", "<").Replace("%3E", ">");
            result = result.Replace("%20", " ").Replace("%A0", " ");
            return result;
        }

        public static void Logger(String lines)
        {
            System.IO.StreamWriter file;
            try
            {
                file = new System.IO.StreamWriter("C:\\logMeme.txt", true);
            }
            catch
            {
                file = new System.IO.StreamWriter("D:\\logMeme.txt", true);
            }
            file.WriteLine(lines);
            file.Close();
        }
    }
    public class DateConvert
    {
        public static DateTime? ParseStringToDate(string datestring)
        {
            if (string.IsNullOrWhiteSpace(datestring)) return null;
            return DateTime.ParseExact(datestring, CommonData.DateConvertFormat, null);
        }
        public static string ParseDateToString(DateTime date)
        {
            return date.ToString(CommonData.DateConvertFormat);
        }
        public static string ParseDateToString(DateTime? date)
        {
            if (!date.HasValue) return string.Empty;
            return ParseDateToString(date);
        }
    }
}

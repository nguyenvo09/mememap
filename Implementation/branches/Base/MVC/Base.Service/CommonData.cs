﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Base.Service
{
    public class CommonData
    {
        public static string DateControlFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateControlFormat"]) ? ConfigurationManager.AppSettings["DateControlFormat"] : "mm/dd/yy"; }
        }
        public static string DateTelerikControlFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateTelerikControlFormat"]) ? ConfigurationManager.AppSettings["DateTelerikControlFormat"] : "dd/MM/yyyy"; }
        }
        public static string DateDisplayFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateDisplayFormat"]) ? ConfigurationManager.AppSettings["DateDisplayFormat"] : "DD/MM/YYYY"; }
        }
        public static string DateConvertFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateConvertFormat"]) ? ConfigurationManager.AppSettings["DateConvertFormat"] : "MMM DD YYYY"; }
        }
    }
}

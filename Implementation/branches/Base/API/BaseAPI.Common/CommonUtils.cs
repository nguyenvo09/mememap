﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BaseAPI.Common
{
    public class CommonData
    {
        public static string DateControlFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateControlFormat"]) ? ConfigurationManager.AppSettings["DateControlFormat"] : "mm/dd/yy"; }
        }
        public static string DateDisplayFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateDisplayFormat"]) ? ConfigurationManager.AppSettings["DateDisplayFormat"] : "dd/MM/yyyy"; }
        }
        public static string DateConvertFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateConvertFormat"]) ? ConfigurationManager.AppSettings["DateConvertFormat"] : "mm/dd/yy"; }
        }
        public static string DateFormat
        {
            get { return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["DateFormat"]) ? ConfigurationManager.AppSettings["DateFormat"] : "mm/dd/yy"; }
        }

    }
    public class DateConvert
    {
        public static DateTime? ParseStringToDate(string datestring){
            if(string.IsNullOrWhiteSpace(datestring))return null;
            return DateTime.ParseExact(datestring, CommonData.DateConvertFormat,null);
        }
        public static string ParseDateToString(DateTime date)
        {
            return date.ToString(CommonData.DateConvertFormat);
        }
        public static string ParseDateToString(DateTime? date)
        {
            if (!date.HasValue) return string.Empty;
            return ParseDateToString(date.Value);
        }
    }

    public class CommonService
    {
        public static void Logger(String lines)
        {
            System.IO.StreamWriter file;
            try
            {
                file = new System.IO.StreamWriter("C:\\logMeme_api.txt", true);
            }
            catch
            {
                file = new System.IO.StreamWriter("D:\\logMeme_api.txt", true);
            }
            file.WriteLine(lines);
            file.Close();
        }
    }
}

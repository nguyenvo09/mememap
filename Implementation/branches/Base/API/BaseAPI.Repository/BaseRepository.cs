﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseAPI.DAL;
namespace BaseAPI.Repository
{
    public class BaseRepository<T> : Repository<T> where T : class
    {
        private MemeAPIContext dataContext;
        protected IDatabaseFactory DatabaseFactory
        {
            get;
            set;
        }
        public BaseRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
        protected MemeAPIContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }
    }
}

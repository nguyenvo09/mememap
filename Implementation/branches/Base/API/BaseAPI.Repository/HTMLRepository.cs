﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseAPI.Model;
using BaseAPI.DAL;

namespace BaseAPI.Repository
{
    public class HTMLRepository : BaseRepository<HTML>, IHTMLRepository  
    {
        public HTMLRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
    public interface IHTMLRepository : IRepository<HTML>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseAPI.UserModel.DTO
{
    [MetadataType(typeof(UserDTO_Validation))]
    public partial class UserDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Salt { get; set; }
        public string NewSalt { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Lock { get; set; }
        public string ActionKey { get; set; }
        public string ProfilePicture { get; set; }
        public string GalleryURL { get; set; }
        public int? Rating { get; set; }
        public bool? Active { get; set; }
        public string FacebookID { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookAccessToken { get; set; }
        public string TwitterID { get; set; }
        public string TwitterAccessToken { get; set; }
        public string LinkedInID { get; set; }
        public string LinkedInAccessToken { get; set; }
        public Guid? ValidKey { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public PhotoGallery Gallery { get; set; }

    }
    public class UserDTO_Validation
    {
        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }
        [RegularExpression(@"^([a-zA-Z])+([a-zA-Z0-9_\-\.])*", ErrorMessage = "UserName invalid")]
        [Required(ErrorMessage = "*")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage= "Email invalid")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

    }

    public class RegisterDTO
    {
        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z])+([a-zA-Z0-9_\-\.])*", ErrorMessage = "Please enter a valid username")]
        [StringLength(100, ErrorMessage = "Please use at least 3 characters", MinimumLength = 3)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
        public int RoleID { get; set; }
    }

    public class ChangePasswordDTO
    {
        [Required(ErrorMessage = "*")]
        public string OldPassword { get; set; }

        public string NewSalt { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "*")]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage="Confirm password is not the same with Password ")]
        [Required(ErrorMessage = "*")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangeProfileDTO
    {
        public int UserID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [Required]
        public string UserName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string ProfilePicture { get; set; }
    }

    public class LoginDTO
    {
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ForgetPassword { get; set; }
        public string ReturnURL { get; set; }
    }

    public class ForgotPasswordDTO
    {
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
    public class PhotoGallery
    {
        public int AlbumID { get; set; }
        public string Path { get; set; }
        public string FolderName { get; set; }
        public List<string> ListPhoto { get; set; }
        public PhotoGallery()
        {
            ListPhoto = new List<string>();
        }
    }

}

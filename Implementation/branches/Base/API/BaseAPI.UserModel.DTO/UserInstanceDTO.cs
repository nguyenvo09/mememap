﻿using System;
using System.Text;

namespace BaseAPI.UserModel.DTO
{
    public partial class UserInstanceDTO
    {
        public int UserInstanceID { get; set; }
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public Guid Guid { get; set; }
        public string HubNames { get; set; }
        public string KFANames { get; set; }
        public int? KPIRoleID { get; set; }
        public string RegionName { get; set; }
        public string KPIRoleName { get; set; }
        public string GeographicLevel { get; set; }

        public bool ApplySubKPIRole { get; set; }
        public string SubHubNames { get; set; }
        public string SubRegionName { get; set; }
        public string SubKFANames { get; set; }
        public int? SubKPIRoleID { get; set; }
        public string SubKPIRoleName { get; set; }
        public string SubGeographicLevel { get; set; }

        public bool? Active { get; set; }     
    }
    
}


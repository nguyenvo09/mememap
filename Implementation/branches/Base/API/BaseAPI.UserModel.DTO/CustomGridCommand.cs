﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using BaseAPI.UserModel;
using System.Collections.Generic;
using Kendo.Mvc;
using System.Xml.Linq;
using System.ComponentModel;
namespace BaseAPI.UserModel.DTO
{
    public class CustomGridCommand
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public List<CustomGridFilter> FilterDescriptors { get; set; }
        public IList<SortDescriptor> SortDescriptors { get; set; }
        public IList<CustomGroupDescriptor> GroupDescriptors { get; set; }
        public List<AddedParam> AddedParams { get; set; }
        public CustomGridCommand()
        {
            Page = 0;
            PageSize = 20;
            FilterDescriptors = new List<CustomGridFilter>();
            AddedParams = new List<AddedParam>();
        }
        #region Setting method
        public CustomGridCommand SetAddedParam(string key,object value) {
            AddedParam p = new AddedParam();
            p.Key = key;
            p.Value = value;
            this.AddedParams.Add(p);
            return this;
        }
        public CustomGridCommand SetFilterDescriptors(List<CustomGridFilter> filters)
        {
            this.FilterDescriptors = filters;
            return this;
        }
        public CustomGridCommand SetGroupDescriptors(IList<CustomGroupDescriptor> groups)
        {
            this.GroupDescriptors = groups;
            return this;
        }
        public CustomGridCommand SetSortDescriptors(IList<SortDescriptor> sorts)
        {
            this.SortDescriptors = sorts;
            return this;
        }
        #endregion
    }
    public class CustomGroupDescriptor
    {
        public ListSortDirection SortDirection { set; get; }
        public String Member { set; get; }
    }
    public class CustomGridFilter
    {
        public CustomFilter Filter{get;set;}
        public CustomCompositeFilter CompositeFilter { get; set; }
    }
    public class CustomFilter{
        public object ConvertedValue { get; set; }
        public string Member { get; set; }
        public Type MemberType { get; set; }
        public FilterOperator Operator { get; set; }
        public object Value { get; set; }
    }
    public class CustomCompositeFilter
    {
        public FilterCompositionLogicalOperator LogicalOperator { get; set; }
        public List<CustomCompositeFilter> CompositeFilters { get; set; } 
        public List<CustomFilter> Filters { get; set; }
    }
    public class AddedParam {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}

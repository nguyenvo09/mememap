﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using BaseAPI.UserModel;
using System.Collections.Generic;
namespace BaseAPI.UserModel.DTO
{
    public class ListDTOModel<T>
    {
        public int PageIndex { get;set; }
        public int PageSize { get; set; }
        public int TotalCount { get;set; }
        public List<T> source { get; set; }
    }
}

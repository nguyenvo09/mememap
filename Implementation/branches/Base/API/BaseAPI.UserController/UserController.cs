﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using BaseAPI.Common.Log;
using BaseAPI.UserModel.DTO;
using BaseAPI.UserLogic;
namespace BaseAPI.UserControllers
{

    public class UserController : ApiController
    {
        IUserLogic _oLogic;        
        public UserController(IUserLogic oLogic)
        {
            _oLogic = oLogic;
        }

        // POST api/User/PostGridCommand
        public HttpResponseMessage PostGridCommand(CustomGridCommand command)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<UserDTO> rObj = _oLogic.GetGridData(command);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        // GET api/User/GetUser
        public UserDTO GetUser(int id)
        {
            UserDTO obj = _oLogic.Find(id);
            if (obj == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return obj;
        }
        // GET api/User/GetUser
        public UserDTO GetUser(string email)
        {
            UserDTO user = _oLogic.FindEmail(email);
            if (user == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            _oLogic.UpdateDateLogin(email);
            return user;
        }

        // GET api/User/GetUser
        public IEnumerable<UserDTO> GetAllUser()
        {
            return _oLogic.GetAll();
        }

        // PUT api/User/PutUser
        public HttpResponseMessage PutUser(int id, UserDTO obj)
        {
            if (ModelState.IsValid && id == obj.UserID)
            {
                try
                {
                    obj.UserID = _oLogic.SaveOrUpdate(obj);
                }
                catch 
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        // PUT api/User/PutUser
        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid )
            {
                int returnID = 0;
                try
                {
                    returnID = _oLogic.Update(addedParams);
                }
                catch 
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, returnID);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/User/PutUserProfile
        public HttpResponseMessage PutUserProfile(int id, ChangeProfileDTO obj)
        {
            if (ModelState.IsValid && id == obj.UserID)
            {
                try
                {
                    obj.UserID = _oLogic.UpdateUserProfile(obj);
                }
                catch 
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        #region forgot password
        public bool GetChangePassword(string email, string newPassword, string newSalt)
        {
            UserDTO obj = _oLogic.FindEmail(email);
            if (obj == null)
            {
                obj = _oLogic.FindUserName(email);
            }
            if (obj != null)
            {
                _oLogic.ChangePassword(obj, newPassword, newSalt);
                return true;
            }
            return false;
        }
        public bool GetForgotPassword(string email, string newPassword, string newSalt, string key)
        {
            UserDTO obj = _oLogic.FindEmail(email);
            if (obj != null)
            {
                _oLogic.ForgotPassword(obj, newPassword, newSalt, key);
                return true;
            }
            return false;
        }
        public bool GetChangeForgotPassword(string email, string key)
        {
            UserDTO obj = _oLogic.FindEmail(email);
            if (obj != null)
            {
                return _oLogic.ChangeForgotPassword(email, key);
            }
            return false;
        }
        #endregion

        #region active user
        public bool GetSendEmailActive(string email, string key)
        {
            UserDTO obj = _oLogic.FindEmail(email);
            if (obj != null)
            {
                return _oLogic.SendEmailActive(obj, key);                
            }
            return false;
        }

        public bool GetActiveUser(string email,string key)
        {
            UserDTO obj = _oLogic.FindEmail(email);
            if (obj != null)
            {
                return _oLogic.ActiveUser(email, key);                
            }
            return false;
        }
        #endregion


        // POST api/User/PostUser
        public HttpResponseMessage PostUser(UserDTO obj)
        {
            if (ModelState.IsValid)
            {
                obj.UserID = _oLogic.SaveOrUpdate(obj);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE /api/User/Delete/5
        public void Delete(int id)
        {
            UserDTO obj = _oLogic.Find(id);
            if (obj == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }            
            _oLogic.Delete(id);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        //get info user by email
        public UserDTO GetUserByEmail(string email)
        {            
            UserDTO obj = _oLogic.FindEmail(email);
            return obj;
        }

        public UserDTO GetUserByUserName(string username)
        {
            UserDTO obj = _oLogic.FindUserName(username);
            return obj;
        }

        public bool GetExistEmail(string email)
        {
            return _oLogic.ExistEmail(email);
        }

        public bool GetExistEmail(int? userId, string email)
        {
            return _oLogic.ExistEmail(userId, email);
        }

        public bool GetExistUserName(int? userId, string userName)
        {
            return _oLogic.ExistUserName(userId, userName);

        }
        // POST api/Column/PostParams
        public HttpResponseMessage PostParams(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<UserDTO> rObj = _oLogic.GetMany(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using BaseAPI.UserModel;
using BaseAPI.Common.Log;
using Kendo.Mvc;
using BaseAPI.UserModel.DTO;
using BaseAPI.UserLogic;
namespace BaseAPI.UserControllers
{

    public class UserInstanceController : ApiController
    {
        IUserInstanceLogic _cLogic;
        public UserInstanceController(IUserInstanceLogic cLogic)
        {
            _cLogic = cLogic;
        }

        // POST api/UserInstance/PostGridCommand
        public HttpResponseMessage PostGridCommand(CustomGridCommand command)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<UserInstanceDTO> rObj = _cLogic.GetGridData(command);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        // POST api/UserInstance/PostParams
        public HttpResponseMessage PostParams(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<UserInstanceDTO> rObj = _cLogic.GetMany(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        public UserInstanceDTO GetUserInstanceByUserID(int id)
        {
            List<AddedParam> param = new List<AddedParam>();
            param.Add(new AddedParam { Key = "UserID", Value = id });
            UserInstanceDTO obj = _cLogic.GetMany(param).FirstOrDefault();
            return obj;
        }

        // GET api/UserInstance/Get
        public UserInstanceDTO Get(int id)
        {
            UserInstanceDTO obj = _cLogic.Find(id);
            return obj;
        }
        // GET api/UserInstance/GetAll
        public IEnumerable<UserInstanceDTO> GetAll()
        {
            return _cLogic.GetAll();
        }


        // PUT api/UserInstance/Put
        public HttpResponseMessage Put(int id, UserInstanceDTO obj)
        {
            if (ModelState.IsValid && id == obj.UserInstanceID)
            {
                try
                {
                    obj.UserInstanceID = _cLogic.SaveOrUpdate(obj);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        // POST api/UserInstance/Post
        public HttpResponseMessage Post(UserInstanceDTO obj)
        {
            if (ModelState.IsValid)
            {
                obj.UserInstanceID = _cLogic.SaveOrUpdate(obj);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, obj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostUpdate(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                int returnID = 0;
                try
                {
                    returnID = _cLogic.Update(addedParams);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, returnID);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage PostExist(List<AddedParam> addedParams)
        {

            //Must be contain and ID field
            if (ModelState.IsValid)
            {
                bool rObj = _cLogic.GetExist(addedParams);

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage PostPage(List<AddedParam> addedParams)
        {
            if (ModelState.IsValid)
            {
                ListDTOModel<UserInstanceDTO> rObj = _cLogic.GetPage(addedParams);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rObj);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE /api/UserInstance/Delete/5
        public void Delete(int id)
        {
            UserInstanceDTO obj = _cLogic.Find(id);
            if (obj != null)
                _cLogic.Delete(id);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace BaseAPI.Model
{
    public class HTML
    {
        [Key]
        public int HTMLID { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [MaxLength]
        public string HTMLText { get; set; }
        public bool? Active { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}

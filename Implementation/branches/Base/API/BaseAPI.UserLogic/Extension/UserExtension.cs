﻿using System.Collections.Generic;
using System.Linq;
using Kendo.Mvc;
using BaseAPI.UserModel;
using System.ComponentModel;
using System.Dynamic;
using BaseAPI.UserModel.DTO;
namespace BaseAPI.UserLogic
{
    public static partial class CustomBindingExtensions
    {
        public static IQueryable<User> ApplyFiltering(this IQueryable<User> data, List<CustomGridFilter> filterDescriptors)
        {
            if (filterDescriptors!=null&&filterDescriptors.Any())
            {
                data = data.Where(ExpressionBuilder.Expression<User>(CreateFilterDescriptor(filterDescriptors)));
            }
            return data;
        }
        public static IQueryable<User> ApplyPaging(this IQueryable<User> data, int currentPage, int pageSize)
        {
            if (pageSize > 0 && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize);
            }

            data = data.Take(pageSize);
            return data;
        }
        public static IEnumerable<User> ApplyPaging(this IEnumerable<User> data, int currentPage, int pageSize)
        {
            if (pageSize > 0 && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize);
            }

            data = data.Take(pageSize);
            return data;
        }
        public static IQueryable<User> ApplySorting(this IQueryable<User> data, IList<SortDescriptor> sortDescriptors)
        {
            if (sortDescriptors != null && sortDescriptors.Any())
            {
                foreach (SortDescriptor sortDescriptor in sortDescriptors)
                {
                    data = AddSortExpression(data, sortDescriptor.SortDirection, sortDescriptor.Member);
                }
            }
            return data;
        }
        private static IQueryable<User> AddSortExpression(IQueryable<User> data, ListSortDirection sortDirection, string memberName)
        {
            if (sortDirection == ListSortDirection.Ascending)
            {
                switch (memberName)
                {
                    case "UserID":
                        data = data.OrderBy(o => o.UserID);
                        break;
                    case "FirstName":
                        data = data.OrderBy(o => o.FirstName);
                        break;
                    case "LastName":
                        data = data.OrderBy(o => o.LastName);
                        break;
                    case "UserName":
                        data = data.OrderBy(o => o.UserName);
                        break;
                    case "Password":
                        data = data.OrderBy(o => o.Password);
                        break;
                    case "Email":
                        data = data.OrderBy(o => o.Email);
                        break;
                    case "Description":
                        data = data.OrderBy(o => o.Description);
                        break;
                    case "ZipCode":
                        data = data.OrderBy(o => o.ZipCode);
                        break;
                    case "Address":
                        data = data.OrderBy(o => o.Address);
                        break;
                    case "CreatedBy":
                        data = data.OrderBy(o => o.CreatedBy);
                        break;
                    case "CreatedDate":
                        data = data.OrderBy(o => o.CreatedDate);
                        break;
                    case "ModifiedBy":
                        data = data.OrderBy(o => o.ModifiedBy);
                        break;
                    case "ModifiedDate":
                        data = data.OrderBy(o => o.ModifiedDate);
                        break;
                    case "Active":
                        data = data.OrderBy(o => o.Active);
                        break;
                    case "LastLoginDate":
                        data = data.OrderBy(o => o.LastLoginDate);
                        break;                    
                    case "DateOfBirth":
                        data = data.OrderBy(o => o.DateOfBirth);
                        break;
                    case "Lock":
                        data = data.OrderBy(o => o.Lock);
                        break;
                    case "ActionKey":
                        data = data.OrderBy(o => o.ActionKey);
                        break;
                }
            }
            else
            {
                switch (memberName)
                {
                    case "UserID":
                        data = data.OrderByDescending(o => o.UserID);
                        break;
                    case "FirstName":
                        data = data.OrderByDescending(o => o.FirstName);
                        break;
                    case "LastName":
                        data = data.OrderByDescending(o => o.LastName);
                        break;
                    case "UserName":
                        data = data.OrderByDescending(o => o.UserName);
                        break;
                    case "Password":
                        data = data.OrderByDescending(o => o.Password);
                        break;
                    case "Email":
                        data = data.OrderByDescending(o => o.Email);
                        break;
                    case "Description":
                        data = data.OrderByDescending(o => o.Description);
                        break;                    
                    case "ZipCode":
                        data = data.OrderByDescending(o => o.ZipCode);
                        break;
                    case "Address":
                        data = data.OrderByDescending(o => o.Address);
                        break;
                    case "CreatedBy":
                        data = data.OrderByDescending(o => o.CreatedBy);
                        break;
                    case "CreatedDate":
                        data = data.OrderByDescending(o => o.CreatedDate);
                        break;
                    case "ModifiedBy":
                        data = data.OrderByDescending(o => o.ModifiedBy);
                        break;
                    case "ModifiedDate":
                        data = data.OrderByDescending(o => o.ModifiedDate);
                        break;
                    case "Active":
                        data = data.OrderByDescending(o => o.Active);
                        break;
                    case "LastLoginDate":
                        data = data.OrderByDescending(o => o.LastLoginDate);
                        break;                    
                    case "DateOfBirth":
                        data = data.OrderByDescending(o => o.DateOfBirth);
                        break;
                    case "Lock":
                        data = data.OrderByDescending(o => o.Lock);
                        break;
                    case "ActionKey":
                        data = data.OrderByDescending(o => o.ActionKey);
                        break;
                }
            }
            return data;
        }

    }
}

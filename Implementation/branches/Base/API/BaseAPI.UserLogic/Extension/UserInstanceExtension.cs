﻿using System.Collections.Generic;
using System.Linq;
using Kendo.Mvc;
using BaseAPI.UserModel;
using System.ComponentModel;
using System.Dynamic;
using BaseAPI.UserModel.DTO;
namespace BaseAPI.UserLogic
{
    public static partial class CustomBindingExtensions
    {
        public static IQueryable<UserInstance> ApplyFiltering(this IQueryable<UserInstance> data, List<CustomGridFilter> filterDescriptors)
        {
            if (filterDescriptors != null && filterDescriptors.Any())
            {
                data = data.Where(ExpressionBuilder.Expression<UserInstance>(BaseAPI.UserLogic.CustomBindingExtensions.CreateFilterDescriptor(filterDescriptors)));
            }
            return data;
        }
        public static IQueryable<UserInstance> ApplyPaging(this IQueryable<UserInstance> data, int currentPageIndex, int pageSize)
        {
            if (pageSize > 0 && currentPageIndex > 0)
            {
                data = data.Skip((currentPageIndex - 1) * pageSize);
            }

            data = data.Take(pageSize);
            return data;
        }
        public static IEnumerable<UserInstance> ApplyPaging(this IEnumerable<UserInstance> data, int currentPageIndex, int pageSize)
        {
            if (pageSize > 0 && currentPageIndex > 0)
            {
                data = data.Skip((currentPageIndex - 1) * pageSize);
            }

            data = data.Take(pageSize);
            return data;
        }
        public static IQueryable<UserInstance> ApplySorting(this IQueryable<UserInstance> data, IList<SortDescriptor> sortDescriptors)
        {
            if (sortDescriptors != null && sortDescriptors.Any())
            {
                foreach (SortDescriptor sortDescriptor in sortDescriptors)
                {
                    data = AddSortExpression(data, sortDescriptor.SortDirection, sortDescriptor.Member);
                }
            }
            return data;
        }
        private static IQueryable<UserInstance> AddSortExpression(IQueryable<UserInstance> data, ListSortDirection sortDirection, string memberName)
        {
            if (sortDirection == ListSortDirection.Ascending)
            {
                switch (memberName)
                {
                    case "UserInstanceID":
                        data = data.OrderBy(o => o.UserInstanceID);
                        break;

                    case "FirstName":
                        data = data.OrderBy(o => o.User.FirstName);
                        break;
                    case "LastName":
                        data = data.OrderBy(o => o.User.LastName);
                        break;
                    case "UserName":
                        data = data.OrderBy(o => o.User.UserName);
                        break;
                    case "Email":
                        data = data.OrderBy(o => o.User.Email);
                        break;
                    case "RegionName":
                        data = data.OrderBy(o => o.RegionName);
                        break;
                    case "KPIRoleName":
                        data = data.OrderBy(o => o.KPIRoleName);
                        break;                    
                }
            }
            else
            {
                switch (memberName)
                {
                    case "UserInstanceID":
                        data = data.OrderByDescending(o => o.UserInstanceID);
                        break;
                    case "LastName":
                        data = data.OrderByDescending(o => o.User.LastName);
                        break;
                    case "FirstName":
                        data = data.OrderByDescending(o => o.User.FirstName);
                        break;
                    case "UserName":
                        data = data.OrderByDescending(o => o.User.UserName);
                        break;
                    case "Email":
                        data = data.OrderByDescending(o => o.User.Email);
                        break;
                    case "RegionName":
                        data = data.OrderByDescending(o => o.RegionName);
                        break;
                    case "KPIRoleName":
                        data = data.OrderByDescending(o => o.KPIRoleName);
                        break;                    
                }
            }
            return data;
        }
    }
}

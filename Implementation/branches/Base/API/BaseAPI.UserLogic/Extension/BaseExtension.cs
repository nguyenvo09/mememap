﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kendo.Mvc;
using BaseAPI.UserModel.DTO;

namespace BaseAPI.UserLogic
{
    public static partial class CustomBindingExtensions
    {
        public static List<IFilterDescriptor> CreateFilterDescriptor(List<CustomGridFilter> filterDescriptors)
        {
            List<IFilterDescriptor> lstFilter = new List<IFilterDescriptor>();
            foreach (CustomGridFilter cgf in filterDescriptors)
            {
                if (cgf.Filter != null)
                {
                    FilterDescriptor fd = new FilterDescriptor();
                    fd.Value = cgf.Filter.ConvertedValue;
                    fd.Operator = cgf.Filter.Operator;
                    fd.MemberType = cgf.Filter.MemberType;
                    fd.Member = cgf.Filter.Member;
                    lstFilter.Add(fd);
                }
                if (cgf.CompositeFilter != null)
                {
                    lstFilter.Add(CreateCompositeFilterDescriptor(cgf.CompositeFilter));
                }
            }
            return lstFilter;
        }
        public static CompositeFilterDescriptor CreateCompositeFilterDescriptor(CustomCompositeFilter ccf)
        {
            CompositeFilterDescriptor cfd = new CompositeFilterDescriptor();
            cfd.LogicalOperator = ccf.LogicalOperator;
            foreach (CustomFilter cf in ccf.Filters)
            {
                FilterDescriptor fd = new FilterDescriptor();
                fd.Value = cf.ConvertedValue;
                fd.Operator = cf.Operator;
                fd.MemberType = cf.MemberType;
                fd.Member = cf.Member;
                cfd.FilterDescriptors.Add(fd);
            }
            foreach (CustomCompositeFilter ccf1 in ccf.CompositeFilters)
            {
                cfd.FilterDescriptors.Add(CreateCompositeFilterDescriptor(ccf1));
            }
            return cfd;
        }
    }
}

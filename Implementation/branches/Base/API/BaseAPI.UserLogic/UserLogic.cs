﻿using System.Linq;
using BaseAPI.UserDAL;
using BaseAPI.UserRepository;
using BaseAPI.UserModel;
using System.Collections;
using System.Collections.Generic;
using BaseAPI.UserModel.DTO;
using System;

namespace BaseAPI.UserLogic
{
    public class UserLogic : BaseAPI.UserLogic.IUserLogic
    {
        public BaseAPI.DAL.IUnitOfWork _unitOfWork;
        IUserRepository _objRepository;

        public UserLogic(BaseAPI.DAL.IUnitOfWork unitOfWork, IUserRepository UserRepository)
        {
            this._unitOfWork = unitOfWork;
            this._objRepository = UserRepository;
        }
        public ListDTOModel<UserDTO> GetGridData(CustomGridCommand command)
        {
            int total = 0;
            ListDTOModel<UserDTO> rObj = new ListDTOModel<UserDTO>();
            rObj.PageIndex = command.Page;
            rObj.PageSize = command.PageSize;
            rObj.source = GetData(command, out total).ToList();// need to implement caching or something so that not have to reload on every request
            rObj.TotalCount = total;
            return rObj;
        }
        

        public IEnumerable<UserDTO> GetData(CustomGridCommand command, out int count)
        {
            var query = _objRepository.GetAllQueryable().Where(o => o.Active == true && o.UserID > 0);
            query = query.ApplyFiltering(command.FilterDescriptors);
            count = query.Count();
            if (count > 0)
            {
                if (command.SortDescriptors != null && command.SortDescriptors.Count > 0)
                {
                    query = query.ApplySorting(command.SortDescriptors);
                }
                else
                {
                    query = query.OrderByDescending(kg => kg.UserID);
                }
                query = query.ApplyPaging(command.Page, command.PageSize);
            }
            var data = (from u in query
                        select new UserDTO
                            {
                                UserID = u.UserID,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                UserName = u.UserName,
                                Salt = u.Salt,
                                NewSalt = u.NewSalt,
                                Password = u.Password,
                                NewPassword = u.NewPassword,
                                Email = u.Email,
                                DateOfBirth = u.DateOfBirth,
                                ZipCode = u.ZipCode,
                                Address = u.Address,                                
                                Description = u.Description,
                                CreatedBy = u.CreatedBy,
                                CreatedDate = u.CreatedDate,
                                ModifiedBy = u.ModifiedBy,
                                ModifiedDate = u.ModifiedDate,
                                Active = u.Active,
                                LastLoginDate = u.LastLoginDate,
                                ActionKey = u.ActionKey,
                                Lock = u.Lock,
                                ProfilePicture = u.ProfilePicture,
                                GalleryURL = u.GalleryURL,
                                Rating = u.Rating,
                                ValidKey = u.ValidKey,
                                FacebookID = u.FacebookID,
                                FacebookEmail = u.FacebookEmail,
                                FacebookAccessToken = u.LinkedInAccessToken,
                                TwitterID = u.TwitterID,
                                TwitterAccessToken = u.TwitterAccessToken,
                                LinkedInID = u.LinkedInID,
                                LinkedInAccessToken = u.LinkedInAccessToken
                                
                            }).AsQueryable();
            return data;
        }
        public IEnumerable<UserDTO> GetAll()
        {
            var query = _objRepository.GetAllQueryable().Where(u => u.UserID > 0);
            var data = (from u in query
                        select new UserDTO
                        {
                            UserID = u.UserID,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            UserName = u.UserName,
                            Salt = u.Salt,
                            NewSalt = u.NewSalt,
                            NewPassword = u.NewPassword,
                            Password = u.Password,
                            Email = u.Email,
                            DateOfBirth = u.DateOfBirth,
                            ZipCode = u.ZipCode,
                            Address = u.Address,                            
                            Description = u.Description,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            Active = u.Active,
                            LastLoginDate = u.LastLoginDate,
                            ActionKey = u.ActionKey,
                            Lock = u.Lock,
                            ProfilePicture = u.ProfilePicture,
                            GalleryURL = u.GalleryURL,
                            Rating = u.Rating,
                            ValidKey = u.ValidKey,
                            FacebookID = u.FacebookID,
                            FacebookEmail = u.FacebookEmail,
                            FacebookAccessToken = u.LinkedInAccessToken,
                            TwitterID = u.TwitterID,
                            TwitterAccessToken = u.TwitterAccessToken,
                            LinkedInID = u.LinkedInID,
                            LinkedInAccessToken = u.LinkedInAccessToken
                        });
            return data;
        }

        public int Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "UserID");
            if (addParam == null) return 0;
            addParams.Remove(addParam);
            int userID = Convert.ToInt32(addParam.Value);
            User dataItem = _objRepository.GetById(userID);
            if (dataItem == null) return 0;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "ModifiedBy":
                        dataItem.ModifiedBy = Convert.ToInt32(p.Value);
                        break;
                    case "ProfilePicture":
                        string profile = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.ProfilePicture = profile;
                        break;
                    case "LastName":
                        string lastName = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.LastName = lastName;
                        break;
                    case "FirstName":
                        string firstName = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.FirstName = firstName;
                        break;
                    case "Address":
                        string address = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.Address = address;
                        break;
                    case "DateOfBirth":
                        DateTime date = Convert.ToDateTime(p.Value);
                        dataItem.DateOfBirth = date;
                        break;
                    case "Description":
                        string description = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.Description = description;
                        break;
                    case "ZipCode":
                        string zipcode = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.ZipCode = zipcode;
                        break;
                    case "Salt":
                        string salt = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.Salt = salt;
                        break;
                    case "NewSalt":
                        string newsalt = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.NewSalt = newsalt;
                        break;
                    case "Password":
                        string password = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.Password = password;
                        break;
                    case "NewPassword":
                        string newpassword = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.NewPassword = newpassword;
                        break;
                    case "Active":
                        dataItem.Active = Convert.ToBoolean(p.Value);
                        break;
                }
            }
            dataItem.ValidKey = Guid.NewGuid();
            dataItem.ModifiedDate = DateTime.Now;
            _objRepository.Update(dataItem);

            _unitOfWork.Commit();

            return dataItem.UserID;
        
        }
        public int SaveOrUpdate(UserDTO item)
        {            
            User dataItem;
            if (item.UserID != 0)
            {
                dataItem = _objRepository.GetById(item.UserID);
                dataItem.ModifiedBy = item.ModifiedBy.Value;
                dataItem.ModifiedDate = DateTime.Now;
            }
            else
            {
                dataItem = new User();
                dataItem.FacebookID = item.FacebookID;
                dataItem.TwitterID = item.TwitterID;
                dataItem.CreatedBy = item.CreatedBy.Value;
                dataItem.CreatedDate = DateTime.Now;
                dataItem.Active = item.Active;
                dataItem.Lock = false;
                dataItem.ValidKey = Guid.NewGuid();
            }
            //Set properties of object
            if (!string.IsNullOrWhiteSpace(item.TwitterID)) dataItem.TwitterID = item.TwitterID;
            if (!string.IsNullOrWhiteSpace(item.TwitterAccessToken)) dataItem.TwitterAccessToken = item.TwitterAccessToken;
            if (!string.IsNullOrWhiteSpace(item.FacebookID)) dataItem.FacebookID = item.FacebookID;
            if (!string.IsNullOrWhiteSpace(item.FacebookEmail)) dataItem.FacebookEmail = item.FacebookEmail;
            if (!string.IsNullOrWhiteSpace(item.FacebookAccessToken)) dataItem.FacebookAccessToken = item.FacebookAccessToken;
            if (!string.IsNullOrWhiteSpace(item.LinkedInID)) dataItem.LinkedInID = item.LinkedInID;
            if (!string.IsNullOrWhiteSpace(item.LinkedInAccessToken)) dataItem.LinkedInAccessToken = item.LinkedInAccessToken;
            dataItem.FirstName = item.FirstName;
            dataItem.LastName = item.LastName;
            dataItem.UserName = item.UserName;
            dataItem.Salt = item.Salt;
            dataItem.NewSalt = item.NewSalt;
            dataItem.Password = item.Password;
            dataItem.NewPassword = item.NewPassword;
            dataItem.Email = item.Email;
            dataItem.DateOfBirth = item.DateOfBirth;
            dataItem.ZipCode = item.ZipCode;
            dataItem.Address = item.Address;            
            dataItem.Description = item.Description;
            dataItem.ProfilePicture = item.ProfilePicture;
            dataItem.GalleryURL = item.GalleryURL;
            if (item.UserID != 0)
            {
                _objRepository.Update(dataItem);
                _unitOfWork.Commit();
            }
            else
            {
                _objRepository.Add(dataItem);
                _unitOfWork.Commit();
            }

            item = ConvertToDTO(dataItem);
            return dataItem.UserID;
        }

        public int UpdateUserProfile(ChangeProfileDTO item)
        {
            User dataItem;
            dataItem = _objRepository.GetById(item.UserID);

            //Set properties of object            
            dataItem.FirstName = item.FirstName;
            dataItem.LastName = item.LastName;
            dataItem.UserName = item.UserName;
            dataItem.DateOfBirth = item.DateOfBirth;
            dataItem.ZipCode = item.ZipCode;
            dataItem.Address = item.Address;            
            dataItem.Description = item.Description;
            dataItem.ProfilePicture = item.ProfilePicture;

            _objRepository.Update(dataItem);
            _unitOfWork.Commit();
            return dataItem.UserID;
        }

        public UserDTO Delete(int id)
        {
            User dataObj = _objRepository.GetById(id);
            _objRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private UserDTO ConvertToDTO(User data)
        {
            if (data != null)
            {
                UserDTO dto = new UserDTO();
                dto.UserID = data.UserID;
                dto.FirstName = data.FirstName;
                dto.LastName = data.LastName;
                dto.UserName = data.UserName;
                dto.Salt = data.Salt;
                dto.NewSalt = data.NewSalt;
                dto.Password = data.Password;
                dto.NewPassword = data.NewPassword;
                dto.Email = data.Email;
                dto.DateOfBirth = data.DateOfBirth;
                dto.ZipCode = data.ZipCode;
                dto.Address = data.Address;
                dto.Active = data.Active;                
                dto.Description = data.Description;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
                dto.ModifiedBy = data.ModifiedBy;
                dto.ModifiedDate = data.ModifiedDate;
                dto.Active = data.Active;
                dto.LastLoginDate = data.LastLoginDate;
                dto.ActionKey = data.ActionKey;
                dto.Lock = data.Lock;
                dto.ProfilePicture = data.ProfilePicture;
                dto.GalleryURL = data.GalleryURL;
                dto.Rating = data.Rating;
                dto.ValidKey = data.ValidKey;
                dto.FacebookID = data.FacebookID;
                dto.FacebookEmail = data.FacebookEmail;
                dto.FacebookAccessToken = data.FacebookAccessToken;
                dto.TwitterID = data.TwitterID;
                dto.TwitterAccessToken = data.TwitterAccessToken;
                dto.LinkedInID = data.LinkedInID;
                dto.LinkedInAccessToken = data.LinkedInAccessToken;
                return dto;
            }
            return null;
        }


        #region forgot password
        public int ChangePassword(UserDTO item, string newPassword, string newSalt)
        {
            User dataItem = new User();
            if (item.UserID != 0)
            {
                dataItem = _objRepository.GetById(item.UserID);
                dataItem.Salt = newSalt;
                dataItem.Password = newPassword;
                dataItem.ValidKey = Guid.NewGuid();
            }
            _objRepository.Update(dataItem);
            _unitOfWork.Commit();
            return dataItem.UserID;
        }
        public int ForgotPassword(UserDTO item, string newPassword, string newSalt, string key)
        {
            User dataItem = new User();
            if (item.UserID != 0)
            {
                dataItem = _objRepository.GetById(item.UserID);
                dataItem.NewSalt = newSalt;
                dataItem.NewPassword= newPassword;
                dataItem.ActionKey = key;
                dataItem.ValidKey = Guid.NewGuid();
            }
            _objRepository.Update(dataItem);
            _unitOfWork.Commit();
            return dataItem.UserID;
        }
        public bool ChangeForgotPassword(string email, string key)
        {
            string actionKey = email.ToLower() + "key=" + key.ToLower();
            User dataItem = _objRepository.Get(u => u.NewPassword != null && u.ActionKey.ToLower() == actionKey);
            if (dataItem != null)
            {
                dataItem.Salt = dataItem.NewSalt;
                dataItem.Password = dataItem.NewPassword;
                dataItem.NewSalt = null;
                dataItem.NewPassword = null;
                dataItem.ValidKey = Guid.NewGuid();
                _objRepository.Update(dataItem);
                _unitOfWork.Commit();
                return true;
            }
            return false;
        }

        #endregion

        #region active user
        public bool SendEmailActive(UserDTO item, string key)
        {
            User dataItem = new User();
            if (item.UserID != 0)
            {
                dataItem = _objRepository.GetById(item.UserID);
                dataItem.ActionKey = key;
                _objRepository.Update(dataItem);
                _unitOfWork.Commit();
                return true;
            }
            return false;            
        }
        public bool ActiveUser(string email, string key)
        {
            User dataItem = new User();
            dataItem = _objRepository.Get(u => u.Active == false && u.ActionKey.ToLower() == (email.ToLower()) + "key=" + key.ToLower());
            if (dataItem != null)
            {
                dataItem.Active = true;
                _objRepository.Update(dataItem);
                _unitOfWork.Commit();
                return true;
            }
            return false;
        }
        #endregion


        //get userinfo by email
        public UserDTO Find(int id)
        {
            return ConvertToDTO(_objRepository.GetById(id));
        }

        public UserDTO FindEmail(string email)
        {
            var lsUser = _objRepository.GetMany(c => c.Email.ToLower() == email.ToLower());
            if (lsUser.Count() > 0)
                return ConvertToDTO(lsUser.First());
            
            return null;
        }

        public UserDTO FindUserName(string username)
        {
            var lsUser = _objRepository.GetMany(c => c.UserName.ToLower() == username.ToLower());
            if (lsUser.Count() > 0)
                return ConvertToDTO(lsUser.First());

            return null;
        }

        public bool ExistUserName(int? userId, string userName)
        {
            if (userId == null)
                userId = -1;
            var data = _objRepository.GetManyQueryable(u => u.UserName.ToLower() == userName.ToLower() && u.UserID != userId.Value);
            return (data.Count() > 0);                
        }

        public bool ExistEmail(int? userId, string email)
        {
            if (userId == null)
                userId = -1;
            var data = _objRepository.GetManyQueryable(u => u.Email.ToLower() == email.ToLower() && u.UserID != userId.Value);
            return (data.Count() > 0);
        }

        public bool ExistEmail(string email)
        {
            var data = _objRepository.GetManyQueryable(u => u.Email.ToLower() == email.ToLower());
            return (data.Count() > 0);
        }


        //update last date login
        public void UpdateDateLogin(string email)
        {
            User dataItem = new User();
            if (!string.IsNullOrEmpty(email))
            {
                dataItem = _objRepository.Get(u => u.Email.ToLower() == email.ToLower());
                dataItem.LastLoginDate = DateTime.Now;
            }
            _objRepository.Update(dataItem);
            _unitOfWork.Commit();
        }
        public IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _objRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "TwitterID":
                            string twitterID = p.Value.ToString();
                            query = query.Where(u => u.TwitterID == twitterID);
                            break;
                        case "FacebookID":
                            string facebookID = p.Value.ToString();
                            query = query.Where(u => u.FacebookID == facebookID);
                            break;
                        case "LinkedInID":
                            string linkedInID = p.Value.ToString();
                            query = query.Where(u => u.LinkedInID == linkedInID);
                            break;
                    }
                }
            }
            var data = (from u in query
                        select new UserDTO
                        {
                            UserID = u.UserID,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            UserName = u.UserName,
                            Salt = u.Salt,
                            NewSalt = u.NewSalt,
                            NewPassword = u.NewPassword,
                            Password = u.Password,
                            Email = u.Email,
                            DateOfBirth = u.DateOfBirth,
                            ZipCode = u.ZipCode,
                            Address = u.Address,
                            Description = u.Description,
                            CreatedBy = u.CreatedBy,
                            CreatedDate = u.CreatedDate,
                            ModifiedBy = u.ModifiedBy,
                            ModifiedDate = u.ModifiedDate,
                            Active = u.Active,
                            LastLoginDate = u.LastLoginDate,
                            ActionKey = u.ActionKey,
                            Lock = u.Lock,
                            ProfilePicture = u.ProfilePicture,
                            GalleryURL = u.GalleryURL,
                            Rating = u.Rating,
                            ValidKey = u.ValidKey,
                            FacebookID = u.FacebookID,
                            FacebookEmail = u.FacebookEmail,
                            FacebookAccessToken = u.LinkedInAccessToken,
                            TwitterID = u.TwitterID,
                            TwitterAccessToken = u.TwitterAccessToken,
                            LinkedInID = u.LinkedInID,
                            LinkedInAccessToken = u.LinkedInAccessToken
                        });
            return data;
        }
        
    }
}

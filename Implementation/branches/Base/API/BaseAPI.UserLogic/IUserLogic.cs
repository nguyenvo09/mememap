﻿using System;
using System.Collections.Generic;
using BaseAPI.UserModel.DTO;
namespace BaseAPI.UserLogic
{
    public interface IUserLogic
    {
        UserDTO Delete(int id);
        UserDTO Find(int id);
        UserDTO FindEmail(string email);
        UserDTO FindUserName(string username);
        System.Collections.Generic.IEnumerable<BaseAPI.UserModel.DTO.UserDTO> GetData(BaseAPI.UserModel.DTO.CustomGridCommand command, out int count);
        BaseAPI.UserModel.DTO.ListDTOModel<BaseAPI.UserModel.DTO.UserDTO> GetGridData(BaseAPI.UserModel.DTO.CustomGridCommand command);
        int SaveOrUpdate(BaseAPI.UserModel.DTO.UserDTO item);
        int Update(List<AddedParam> addedParams);
        int UpdateUserProfile(ChangeProfileDTO item);
        System.Collections.Generic.IEnumerable<BaseAPI.UserModel.DTO.UserDTO> GetAll();
        int ChangePassword(UserDTO item, string newPassword, string newSalt);
        int ForgotPassword(UserDTO item, string newPassword, string newSalt, string key);
        void UpdateDateLogin(string email);
        bool ChangeForgotPassword(string email, string key);
        bool ExistUserName(int? userId, string userName);
        bool ExistEmail(int? userId, string email);
        bool SendEmailActive(UserDTO item, string key);
        bool ActiveUser(string email, string key);
        bool ExistEmail(string email);
        IEnumerable<UserDTO> GetMany(List<AddedParam> addedParams);
    }
}

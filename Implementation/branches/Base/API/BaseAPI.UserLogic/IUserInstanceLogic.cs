﻿using System;
using System.Collections.Generic;
using BaseAPI.UserModel.DTO;
namespace BaseAPI.UserLogic
{
    public interface IUserInstanceLogic
    {
        BaseAPI.UserModel.DTO.UserInstanceDTO Delete(int id);
        BaseAPI.UserModel.DTO.UserInstanceDTO Find(int id);
        System.Collections.Generic.IEnumerable<BaseAPI.UserModel.DTO.UserInstanceDTO> GetAll();
        System.Collections.Generic.IEnumerable<BaseAPI.UserModel.DTO.UserInstanceDTO> GetAllActiveUserInstance();
        BaseAPI.UserModel.DTO.ListDTOModel<BaseAPI.UserModel.DTO.UserInstanceDTO> GetGridData(BaseAPI.UserModel.DTO.CustomGridCommand command);
        System.Collections.Generic.IEnumerable<BaseAPI.UserModel.DTO.UserInstanceDTO> GetMany(System.Collections.Generic.List<BaseAPI.UserModel.DTO.AddedParam> addedParams);
        int SaveOrUpdate(BaseAPI.UserModel.DTO.UserInstanceDTO item);
        int Update(System.Collections.Generic.List<BaseAPI.UserModel.DTO.AddedParam> addParams);
        bool GetExist(List<AddedParam> addedParams);
        UserInstanceDTO GetUserInstanceByUserInstanceID(int userInstanceID);
        ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams);
    }
}

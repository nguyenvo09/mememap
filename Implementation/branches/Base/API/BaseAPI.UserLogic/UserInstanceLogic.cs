﻿using System.Linq;
using BaseAPI.UserRepository;
using BaseAPI.UserModel;
using System.Collections;
using System.Collections.Generic;
using BaseAPI.UserModel.DTO;
using System;
using BaseAPI.UserDAL;

namespace BaseAPI.UserLogic
{
    public class UserInstanceLogic : BaseAPI.UserLogic.IUserInstanceLogic
    {
        public BaseAPI.DAL.IUnitOfWork _unitOfWork;
        IUserInstanceRepository _userInstanceRepository;

        public UserInstanceLogic(BaseAPI.DAL.IUnitOfWork unitOfWork, IUserInstanceRepository UserInstanceRepository)
        {
            this._unitOfWork = unitOfWork;
            this._userInstanceRepository = UserInstanceRepository;
        }
        public ListDTOModel<UserInstanceDTO> GetGridData(CustomGridCommand command)
        {
            int total = 0;
            ListDTOModel<UserInstanceDTO> rObj = new ListDTOModel<UserInstanceDTO>();
            rObj.PageIndex = command.Page;
            rObj.PageSize = command.PageSize;
            rObj.source = GetGridData(command, out total).ToList();
            rObj.TotalCount = total;
            return rObj;
        }
        public IEnumerable<UserInstanceDTO> GetGridData(CustomGridCommand command, out int count)
        {
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserID > 0);
            if (command.AddedParams.Count > 0)
            {
                foreach (var p in command.AddedParams)
                {
                    switch (p.Key)
                    {
                        case "ExceptSystemAdmin":
                            string value = p.Value.ToString();
                            query = query.Where(u => u.User.UserName != value);
                            break;
                        case "RegionName":
                            string region = p.Value.ToString();
                            query = query.Where(u => u.RegionName == region);
                            break;
                    }
                }
            }
            query = query.ApplyFiltering(command.FilterDescriptors);
            count = query.Count();
            if (count > 0)
            {
                if (command.SortDescriptors != null && command.SortDescriptors.Count > 0)
                {
                    query = query.ApplySorting(command.SortDescriptors);
                }
                else
                {
                    query = query.OrderByDescending(obj => obj.UserInstanceID);
                }
                query = query.ApplyPaging(command.Page, command.PageSize);
            }
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID,
                            UserID = c.UserID,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName,
                            Email = c.User.Email,
                            Guid = c.Guid,
                            KFANames = c.KFANames,
                            HubNames = c.HubNames,
                            KPIRoleID = c.KPIRoleID,
                            RegionName = c.RegionName,
                            GeographicLevel = c.GeographicLevel,
                            KPIRoleName = c.KPIRoleName,
                            SubKFANames = c.SubKFANames,
                            SubHubNames = c.SubHubNames,
                            SubKPIRoleID = c.SubKPIRoleID,
                            SubRegionName = c.SubRegionName,
                            SubGeographicLevel = c.SubGeographicLevel,
                            SubKPIRoleName = c.SubKPIRoleName,
                            ApplySubKPIRole = c.ApplySubKPIRole,
                            Active = c.User.Active
                        }).AsQueryable();
            return data;
        }
        public IEnumerable<UserInstanceDTO> GetAll()
        {
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserID > 0);
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID,
                            UserID = c.UserID,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName,
                            Email = c.User.Email,
                            Guid = c.Guid,
                            KFANames = c.KFANames,
                            HubNames = c.HubNames,
                            KPIRoleID = c.KPIRoleID,
                            RegionName = c.RegionName,
                            GeographicLevel = c.GeographicLevel,
                            KPIRoleName = c.KPIRoleName,
                            SubKFANames = c.SubKFANames,
                            SubHubNames = c.SubHubNames,
                            SubKPIRoleID = c.SubKPIRoleID,
                            SubRegionName = c.SubRegionName,
                            SubGeographicLevel = c.SubGeographicLevel,
                            SubKPIRoleName = c.SubKPIRoleName,
                            ApplySubKPIRole = c.ApplySubKPIRole,
                            Active = c.User.Active
                        });
            return data;
        }
        public IEnumerable<UserInstanceDTO> GetAllActiveUserInstance()
        {
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserID > 0);
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID,
                            UserID = c.UserID,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName,
                            Email = c.User.Email,
                            Guid = c.Guid,
                            KFANames = c.KFANames,
                            HubNames = c.HubNames,
                            KPIRoleID = c.KPIRoleID,
                            RegionName = c.RegionName,
                            GeographicLevel = c.GeographicLevel,
                            KPIRoleName = c.KPIRoleName,
                            SubKFANames = c.SubKFANames,
                            SubHubNames = c.SubHubNames,
                            SubKPIRoleID = c.SubKPIRoleID,
                            SubRegionName = c.SubRegionName,
                            SubGeographicLevel = c.SubGeographicLevel,
                            SubKPIRoleName = c.SubKPIRoleName,
                            ApplySubKPIRole = c.ApplySubKPIRole,
                            Active = c.User.Active
                        });
            return data;
        }
        public IEnumerable<UserInstanceDTO> GetMany(List<AddedParam> addedParams)
        {
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserID > 0);
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "ExceptSystemAdmin":
                            string value = p.Value.ToString();
                            query = query.Where(u => u.User.UserName != value);
                            break;
                        case "UserID":
                            int userID = Convert.ToInt32(p.Value);
                            query = query.Where(u => u.UserID == userID);
                            break;
                        case "UserInstanceID":
                            int userInstanceID = Convert.ToInt32(p.Value);
                            query = query.Where(u => u.UserInstanceID == userInstanceID);
                            break;
                        case "GreaterThan":
                            int smallerValue = Convert.ToInt32(p.Value);
                            query = query.Where(a => a.UserInstanceID > smallerValue);
                            break;
                        case "Active":
                            bool active = Convert.ToBoolean(p.Value);
                            query = query.Where(a => a.User.Active == active);
                            break;
                        case "KPIRoleID":
                            int kpiRoleID = Convert.ToInt32(p.Value);
                            query = query.Where(u => u.KPIRoleID == kpiRoleID);
                            break;
                        case "KFAName":
                            if (p.Value != null)
                            {
                                string kfaName = p.Value.ToString();
                                query = query.Where(u => u.KFANames.Contains(kfaName));
                            }
                            break;
                    }
                }
            }
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID,
                            UserID = c.UserID,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName,
                            Email = c.User.Email,
                            Guid = c.Guid,
                            KFANames = c.KFANames,
                            HubNames = c.HubNames,
                            KPIRoleID = c.KPIRoleID,
                            RegionName = c.RegionName,
                            GeographicLevel = c.GeographicLevel,
                            KPIRoleName = c.KPIRoleName,
                            SubKFANames = c.SubKFANames,
                            SubHubNames = c.SubHubNames,
                            SubKPIRoleID = c.SubKPIRoleID,
                            SubRegionName = c.SubRegionName,
                            SubGeographicLevel = c.SubGeographicLevel,
                            SubKPIRoleName = c.SubKPIRoleName,
                            ApplySubKPIRole = c.ApplySubKPIRole,
                            Active = c.User.Active

                        });
            return data;
        }

        public ListDTOModel<UserInstanceDTO> GetPage(List<AddedParam> addedParams)
        {
            ListDTOModel<UserInstanceDTO> rList = new ListDTOModel<UserInstanceDTO>();
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserInstanceID > 1);
            int pageIndex = 1;
            int pageSize = 20;
            var pageIndexParam = addedParams.FirstOrDefault(u => u.Key == "PageIndex");
            var pageSizeParam = addedParams.FirstOrDefault(u => u.Key == "PageSize");
            if (pageIndexParam != null && pageIndexParam.Value != null)
            {
                pageIndex = Convert.ToInt32(pageIndexParam.Value);
            }
            if (pageSizeParam != null && pageSizeParam.Value != null)
            {
                pageSize = Convert.ToInt32(pageSizeParam.Value);
            }
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    switch (p.Key)
                    {
                        case "SearchVolvoID":
                            if (p.Value != null)
                            {
                                string searchvolvoid = p.Value.ToString();
                                query = query.Where(u => u.User.UserName == searchvolvoid);
                            }
                            break;
                        case "SearchCode":
                            if (p.Value != null)
                            {
                                string searchcode = p.Value.ToString();
                                query = query.Where(u => u.User.FirstName.Contains(searchcode) || u.User.LastName.Contains(searchcode));
                            }
                            break;
                    }
                }
            }
            //sort default            
            query = query.OrderBy(u => u.UserInstanceID);
            rList.TotalCount = query.Count();
            // paging
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var data = query.ToList().Select(u =>
                        new UserInstanceDTO
                        {
                            UserInstanceID = u.UserInstanceID,
                            UserID = u.UserID,
                            UserName = u.User != null ? u.User.UserName : "",
                            FirstName = u.User != null ? u.User.FirstName : "",
                            LastName = u.User != null ? u.User.LastName : "",
                            Email = u.User != null ? u.User.Email : "",
                            KPIRoleName = u.KPIRoleName,
                            RegionName = u.RegionName,
                            Active = u.User != null ? u.User.Active : false
                        });
            rList.PageIndex = pageIndex;
            rList.PageSize = pageSize;
            rList.source = data.ToList();
            return rList;
        }

        public bool GetExist(List<AddedParam> addedParams)
        {
            var query = _userInstanceRepository.GetAllQueryable();
            if (addedParams != null)
            {
                foreach (AddedParam p in addedParams)
                {
                    //add logic for query here
                    switch (p.Key)
                    {
                        case "UserInstanceID":
                            int userInstanceId = Convert.ToInt32(p.Value);
                            query = query.Where(o => o.UserInstanceID != userInstanceId);
                            break;
                        case "UserID":
                            int id = Convert.ToInt32(p.Value);
                            query = query.Where(o => o.UserID == id);
                            break;
                    }
                }
            }
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID
                        });
            return data.Count() > 0;

        }

        public UserInstanceDTO Find(int id)
        {
            return ConvertToDTO(_userInstanceRepository.GetById(id));
        }

        public UserInstanceDTO GetUserInstanceByUserInstanceID(int userInstanceID)
        {
            var query = _userInstanceRepository.GetAllQueryable().Where(u => u.UserInstanceID == userInstanceID);
            var data = (from c in query
                        select new UserInstanceDTO
                        {
                            UserInstanceID = c.UserInstanceID,
                            UserID = c.UserID,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName,
                            Email = c.User.Email,
                            Guid = c.Guid,
                            KFANames = c.KFANames,
                            HubNames = c.HubNames,
                            KPIRoleID = c.KPIRoleID,
                            RegionName = c.RegionName,
                            GeographicLevel = c.GeographicLevel,
                            KPIRoleName = c.KPIRoleName,
                            SubKFANames = c.SubKFANames,
                            SubHubNames = c.SubHubNames,
                            SubKPIRoleID = c.SubKPIRoleID,
                            SubRegionName = c.SubRegionName,
                            SubGeographicLevel = c.SubGeographicLevel,
                            SubKPIRoleName = c.SubKPIRoleName,
                            ApplySubKPIRole = c.ApplySubKPIRole,
                            Active = c.User.Active

                        });
            return data.FirstOrDefault();
        }
        public int SaveOrUpdate(UserInstanceDTO item)
        {
            UserInstance dataItem;

            if (item.UserInstanceID != 0)
            {
                //set logic when update UserInstance
                dataItem = _userInstanceRepository.GetById(item.UserInstanceID);
            }
            else
            {
                //set logic when create new UserInstance
                dataItem = new UserInstance();
                dataItem.Guid = Guid.NewGuid();

            }
            //Set common properties of UserInstance(create and update)
            dataItem.UserID = item.UserID;
            dataItem.HubNames = item.HubNames;
            dataItem.KFANames = item.KFANames;
            dataItem.KPIRoleID = item.KPIRoleID;
            dataItem.GeographicLevel = item.GeographicLevel;
            dataItem.RegionName = item.RegionName;
            dataItem.SubKFANames = item.SubKFANames;
            dataItem.SubHubNames = item.SubHubNames;
            dataItem.SubKPIRoleID = item.SubKPIRoleID;
            dataItem.SubRegionName = item.SubRegionName;
            dataItem.SubGeographicLevel = item.SubGeographicLevel;
            dataItem.ApplySubKPIRole = item.ApplySubKPIRole;
            if (item.UserInstanceID != 0)
                _userInstanceRepository.Update(dataItem);
            else
                _userInstanceRepository.Add(dataItem);

            _unitOfWork.Commit();
            item = ConvertToDTO(dataItem);
            return dataItem.UserInstanceID;
        }
        public int Update(List<AddedParam> addParams)
        {
            AddedParam addParam = addParams.FirstOrDefault(a => a.Key == "UserInstanceID");
            if (addParam == null) return 0;
            addParams.Remove(addParam);
            int userInstanceID = Convert.ToInt32(addParam.Value);
            UserInstance dataItem = _userInstanceRepository.GetById(userInstanceID);
            if (dataItem == null) return 0;
            //update field
            foreach (AddedParam p in addParams)
            {
                switch (p.Key)
                {
                    case "KPIRoleID":
                        int roleID = Convert.ToInt32(p.Value);
                        dataItem.KPIRoleID = roleID;
                        break;
                    case "HubNames":
                        string hubNames = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.HubNames = hubNames;
                        break;
                    case "KFANames":
                        string kfaIDs = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.KFANames = kfaIDs;
                        break;
                    case "GeographicLevel":
                        string geographicLevel = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.GeographicLevel = geographicLevel;
                        break;
                    case "RegionName":
                        string regionName = p.Value == null ? "" : p.Value.ToString();
                        dataItem.RegionName = regionName;
                        break;


                    case "SubKPIRoleID":
                        int? subkpiroleid = null;
                        if (p.Value != null)
                        {
                            subkpiroleid = Convert.ToInt32(p.Value);
                        }
                        dataItem.SubKPIRoleID = subkpiroleid;
                        break;
                    case "SubHubNames":
                        string subHubNames = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.SubHubNames = subHubNames;
                        break;
                    case "SubKFANames":
                        string subKFANames = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.SubKFANames = subKFANames;
                        break;
                    case "SubGeographicLevel":
                        string subGeographicLevel = p.Value != null ? p.Value.ToString() : string.Empty;
                        dataItem.SubGeographicLevel = subGeographicLevel;
                        break;
                    case "SubRegionName":
                        string subRegionName = p.Value == null ? "" : p.Value.ToString();
                        dataItem.SubRegionName = subRegionName;
                        break;
                    case "ApplySubKPIRole":
                        if (p.Value != null)
                        {
                            bool applysubkpirole = Convert.ToBoolean(p.Value);
                            dataItem.ApplySubKPIRole = applysubkpirole;
                        }
                        break;

                }
            }

            _userInstanceRepository.Update(dataItem);

            _unitOfWork.Commit();

            return dataItem.UserID;

        }
        public UserInstanceDTO Delete(int id)
        {
            UserInstance dataObj = _userInstanceRepository.GetById(id);
            _userInstanceRepository.Delete(dataObj);
            _unitOfWork.Commit();
            return ConvertToDTO(dataObj);
        }
        private UserInstanceDTO ConvertToDTO(UserInstance data)
        {
            if (data != null)
            {
                UserInstanceDTO dto = new UserInstanceDTO();
                dto.UserInstanceID = data.UserInstanceID;
                dto.UserID = data.UserID;
                dto.FirstName = data.User != null ? data.User.FirstName : string.Empty;
                dto.LastName = data.User != null ? data.User.LastName : string.Empty;
                dto.UserName = data.User != null ? data.User.UserName : string.Empty;
                dto.Email = data.User != null ? data.User.Email : string.Empty;
                dto.Guid = data.Guid;
                dto.HubNames = data.HubNames;
                dto.KFANames = data.KFANames;
                dto.KPIRoleID = data.KPIRoleID;
                dto.RegionName = data.RegionName;
                dto.KPIRoleName = data.KPIRoleName;
                dto.GeographicLevel = data.GeographicLevel;
                dto.SubHubNames = data.SubHubNames;
                dto.SubKFANames = data.SubKFANames;
                dto.SubKPIRoleID = data.SubKPIRoleID;
                dto.SubRegionName = data.SubRegionName;
                dto.SubKPIRoleName = data.SubKPIRoleName;
                dto.SubGeographicLevel = data.SubGeographicLevel;
                dto.ApplySubKPIRole = data.ApplySubKPIRole;
                dto.Active = data.User != null ? data.User.Active : null;
                return dto;
            }
            return null;
        }
    }
}
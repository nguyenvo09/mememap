﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseAPI.UserModel;
using BaseAPI.UserDAL;

namespace BaseAPI.UserRepository
{
    public class UserInstanceRepository : BaseUserRepository<UserInstance>, IUserInstanceRepository  
    {
        public UserInstanceRepository(IUserDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            UserDatabaseFactory = databaseFactory;
        }
    }
    public interface IUserInstanceRepository : IRepository<UserInstance>
    {
    }
}

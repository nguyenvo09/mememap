﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseAPI.UserModel;
using BaseAPI.UserDAL;

namespace BaseAPI.UserRepository
{
    public class UserRepository : BaseUserRepository<User>, IUserRepository  
    {
        public UserRepository(IUserDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            UserDatabaseFactory = databaseFactory;
        }
    }
    public interface IUserRepository : IRepository<User>
    {
    }
}

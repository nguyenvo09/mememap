﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseAPI.UserDAL;
namespace BaseAPI.UserRepository
{
    public class BaseUserRepository<T> : UserRepository<T> where T : class
    {
        private UserAPIContext dataContext;
        protected IUserDatabaseFactory UserDatabaseFactory
        {
            get;
            set;
        }
        public BaseUserRepository(IUserDatabaseFactory userDatabaseFactory)
            : base(userDatabaseFactory)
        {
            UserDatabaseFactory = userDatabaseFactory;
        }
        protected UserAPIContext DataContext
        {
            get { return dataContext ?? (dataContext = UserDatabaseFactory.Get()); }
        }
    }
}

﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
//using BaseAPI.UserModel;

namespace BaseAPI.UserDAL
{
    public class UserAPIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<MemeAPI.Entities.MemeAPIContext>());

        public UserAPIContext()
            : base("name=UserAPIContext")
        {
        }
        public virtual void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                                      validationError.PropertyName, validationError.ErrorMessage);

                    }
                }
            }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BaseAPI.UserModel.User>()
                .HasMany(k => k.UserInstances)
                .WithRequired(k => k.User)
                .HasForeignKey(k => k.UserID);

            base.OnModelCreating(modelBuilder);
        }
        #region Base
        public DbSet<BaseAPI.UserModel.User> Users { get; set; }
        public DbSet<BaseAPI.UserModel.UserInstance> UserInstances { get; set; }



        #endregion
    }
}
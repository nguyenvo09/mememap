﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseAPI.UserDAL
{
    public interface IUserDatabaseFactory : IDisposable
    {
        UserAPIContext Get();
    }
}

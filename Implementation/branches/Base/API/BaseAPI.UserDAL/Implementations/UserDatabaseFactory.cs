﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseAPI.UserDAL
{
    public class UserDatabaseFactory : Disposable, IUserDatabaseFactory
    {
        private UserAPIContext dataContext;
        public UserAPIContext Get()
        {
            return dataContext ?? (dataContext = new UserAPIContext());
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}

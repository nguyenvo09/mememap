﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace BaseAPI.UserModel
{
    public partial class User
    {
        [Key]
        public int UserID { get; set; }
        [StringLength(128)]
        public string FirstName { get; set; }
        [StringLength(128)]
        public string LastName { get; set; }
        [StringLength(128)]
        public string UserName { get; set; }
        [StringLength(128)]
        public string Salt { get; set; }
        [StringLength(128)]
        public string NewSalt { get; set; }
        [StringLength(128)]
        public string Password { get; set; }
        [StringLength(128)]
        public string NewPassword { get; set; }
        [StringLength(128)]
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [StringLength(50)]
        public string ZipCode { get; set; }
        [StringLength(200)]
        public string Address { get; set; }
        [StringLength(5000)]
        public string Description { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Lock { get; set; }
        [StringLength(512)]
        public string ActionKey { get; set; }
        public string ProfilePicture { get; set; }
        public string GalleryURL { get; set; }
        public int? Rating { get; set; }
        public bool? Active { get; set; }
        public Guid? ValidKey { get; set; }
        public DateTime? LastLoginDate { get; set; }
        [StringLength(128)]
        public string FacebookID { get; set; }
        [StringLength(128)]
        public string FacebookEmail { get; set; }
        [StringLength(256)]
        public string FacebookAccessToken { get; set; }
        [StringLength(128)]
        public string TwitterID { get; set; }
        [StringLength(256)]
        public string TwitterAccessToken { get; set; }
        [StringLength(128)]
        public string LinkedInID { get; set; }
        [StringLength(256)]
        public string LinkedInAccessToken { get; set; }

       public virtual ICollection<UserInstance> UserInstances { get; set; }
    }
}

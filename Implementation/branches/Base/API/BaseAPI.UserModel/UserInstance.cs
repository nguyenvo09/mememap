﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using MemeAPI.Model;

namespace BaseAPI.UserModel
{
    public partial class UserInstance
    {
        public int UserInstanceID { get; set; }
        public int UserID { get; set; }
        public Guid Guid { get; set; }
        public string RegionName { get; set; }
        public string KFANames { get; set; }
        public string GeographicLevel { get; set; }
        public string HubNames { get; set; }
        public int? KPIRoleID { get; set; }
        public string KPIRoleName { get; private set; }

        public bool ApplySubKPIRole { get; set; }
        public string SubHubNames { get; set; }
        public string SubRegionName { get; set; }
        public string SubKFANames { get; set; }
        public int? SubKPIRoleID { get; set; }
        public string SubKPIRoleName { get; set; }
        public string SubGeographicLevel { get; set; }
        
        public virtual User User { get; set; }
    }
}
